                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : FreeWare ANSI-C Compiler
                              3 ; Version 2.6.0 #4309 (Jul 28 2006)
                              4 ; This file generated Tue Nov 14 20:10:57 2017
                              5 ;--------------------------------------------------------
                              6 	.module main
                              7 	.optsdcc -mmcs51 --model-large
                              8 	
                              9 ;--------------------------------------------------------
                             10 ; Public variables in this module
                             11 ;--------------------------------------------------------
                             12 	.globl _main
                             13 	.globl _serial_isr
                             14 	.globl _timer0_isr
                             15 	.globl _SerialInitialize
                             16 	.globl _P5_7
                             17 	.globl _P5_6
                             18 	.globl _P5_5
                             19 	.globl _P5_4
                             20 	.globl _P5_3
                             21 	.globl _P5_2
                             22 	.globl _P5_1
                             23 	.globl _P5_0
                             24 	.globl _P4_7
                             25 	.globl _P4_6
                             26 	.globl _P4_5
                             27 	.globl _P4_4
                             28 	.globl _P4_3
                             29 	.globl _P4_2
                             30 	.globl _P4_1
                             31 	.globl _P4_0
                             32 	.globl _PX0L
                             33 	.globl _PT0L
                             34 	.globl _PX1L
                             35 	.globl _PT1L
                             36 	.globl _PLS
                             37 	.globl _PT2L
                             38 	.globl _PPCL
                             39 	.globl _EC
                             40 	.globl _CCF0
                             41 	.globl _CCF1
                             42 	.globl _CCF2
                             43 	.globl _CCF3
                             44 	.globl _CCF4
                             45 	.globl _CR
                             46 	.globl _CF
                             47 	.globl _TF2
                             48 	.globl _EXF2
                             49 	.globl _RCLK
                             50 	.globl _TCLK
                             51 	.globl _EXEN2
                             52 	.globl _TR2
                             53 	.globl _C_T2
                             54 	.globl _CP_RL2
                             55 	.globl _T2CON_7
                             56 	.globl _T2CON_6
                             57 	.globl _T2CON_5
                             58 	.globl _T2CON_4
                             59 	.globl _T2CON_3
                             60 	.globl _T2CON_2
                             61 	.globl _T2CON_1
                             62 	.globl _T2CON_0
                             63 	.globl _PT2
                             64 	.globl _ET2
                             65 	.globl _CY
                             66 	.globl _AC
                             67 	.globl _F0
                             68 	.globl _RS1
                             69 	.globl _RS0
                             70 	.globl _OV
                             71 	.globl _F1
                             72 	.globl _P
                             73 	.globl _PS
                             74 	.globl _PT1
                             75 	.globl _PX1
                             76 	.globl _PT0
                             77 	.globl _PX0
                             78 	.globl _RD
                             79 	.globl _WR
                             80 	.globl _T1
                             81 	.globl _T0
                             82 	.globl _INT1
                             83 	.globl _INT0
                             84 	.globl _TXD
                             85 	.globl _RXD
                             86 	.globl _P3_7
                             87 	.globl _P3_6
                             88 	.globl _P3_5
                             89 	.globl _P3_4
                             90 	.globl _P3_3
                             91 	.globl _P3_2
                             92 	.globl _P3_1
                             93 	.globl _P3_0
                             94 	.globl _EA
                             95 	.globl _ES
                             96 	.globl _ET1
                             97 	.globl _EX1
                             98 	.globl _ET0
                             99 	.globl _EX0
                            100 	.globl _P2_7
                            101 	.globl _P2_6
                            102 	.globl _P2_5
                            103 	.globl _P2_4
                            104 	.globl _P2_3
                            105 	.globl _P2_2
                            106 	.globl _P2_1
                            107 	.globl _P2_0
                            108 	.globl _SM0
                            109 	.globl _SM1
                            110 	.globl _SM2
                            111 	.globl _REN
                            112 	.globl _TB8
                            113 	.globl _RB8
                            114 	.globl _TI
                            115 	.globl _RI
                            116 	.globl _P1_7
                            117 	.globl _P1_6
                            118 	.globl _P1_5
                            119 	.globl _P1_4
                            120 	.globl _P1_3
                            121 	.globl _P1_2
                            122 	.globl _P1_1
                            123 	.globl _P1_0
                            124 	.globl _TF1
                            125 	.globl _TR1
                            126 	.globl _TF0
                            127 	.globl _TR0
                            128 	.globl _IE1
                            129 	.globl _IT1
                            130 	.globl _IE0
                            131 	.globl _IT0
                            132 	.globl _P0_7
                            133 	.globl _P0_6
                            134 	.globl _P0_5
                            135 	.globl _P0_4
                            136 	.globl _P0_3
                            137 	.globl _P0_2
                            138 	.globl _P0_1
                            139 	.globl _P0_0
                            140 	.globl _EECON
                            141 	.globl _KBF
                            142 	.globl _KBE
                            143 	.globl _KBLS
                            144 	.globl _BRL
                            145 	.globl _BDRCON
                            146 	.globl _T2MOD
                            147 	.globl _SPDAT
                            148 	.globl _SPSTA
                            149 	.globl _SPCON
                            150 	.globl _SADEN
                            151 	.globl _SADDR
                            152 	.globl _WDTPRG
                            153 	.globl _WDTRST
                            154 	.globl _P5
                            155 	.globl _P4
                            156 	.globl _IPH1
                            157 	.globl _IPL1
                            158 	.globl _IPH0
                            159 	.globl _IPL0
                            160 	.globl _IEN1
                            161 	.globl _IEN0
                            162 	.globl _CMOD
                            163 	.globl _CL
                            164 	.globl _CH
                            165 	.globl _CCON
                            166 	.globl _CCAPM4
                            167 	.globl _CCAPM3
                            168 	.globl _CCAPM2
                            169 	.globl _CCAPM1
                            170 	.globl _CCAPM0
                            171 	.globl _CCAP4L
                            172 	.globl _CCAP3L
                            173 	.globl _CCAP2L
                            174 	.globl _CCAP1L
                            175 	.globl _CCAP0L
                            176 	.globl _CCAP4H
                            177 	.globl _CCAP3H
                            178 	.globl _CCAP2H
                            179 	.globl _CCAP1H
                            180 	.globl _CCAP0H
                            181 	.globl _CKCKON1
                            182 	.globl _CKCKON0
                            183 	.globl _CKRL
                            184 	.globl _AUXR1
                            185 	.globl _AUXR
                            186 	.globl _TH2
                            187 	.globl _TL2
                            188 	.globl _RCAP2H
                            189 	.globl _RCAP2L
                            190 	.globl _T2CON
                            191 	.globl _B
                            192 	.globl _ACC
                            193 	.globl _PSW
                            194 	.globl _IP
                            195 	.globl _P3
                            196 	.globl _IE
                            197 	.globl _P2
                            198 	.globl _SBUF
                            199 	.globl _SCON
                            200 	.globl _P1
                            201 	.globl _TH1
                            202 	.globl _TH0
                            203 	.globl _TL1
                            204 	.globl _TL0
                            205 	.globl _TMOD
                            206 	.globl _TCON
                            207 	.globl _PCON
                            208 	.globl _DPH
                            209 	.globl _DPL
                            210 	.globl _SP
                            211 	.globl _P0
                            212 	.globl _putchar
                            213 	.globl _getchar
                            214 ;--------------------------------------------------------
                            215 ; special function registers
                            216 ;--------------------------------------------------------
                            217 	.area RSEG    (DATA)
                    0080    218 _P0	=	0x0080
                    0081    219 _SP	=	0x0081
                    0082    220 _DPL	=	0x0082
                    0083    221 _DPH	=	0x0083
                    0087    222 _PCON	=	0x0087
                    0088    223 _TCON	=	0x0088
                    0089    224 _TMOD	=	0x0089
                    008A    225 _TL0	=	0x008a
                    008B    226 _TL1	=	0x008b
                    008C    227 _TH0	=	0x008c
                    008D    228 _TH1	=	0x008d
                    0090    229 _P1	=	0x0090
                    0098    230 _SCON	=	0x0098
                    0099    231 _SBUF	=	0x0099
                    00A0    232 _P2	=	0x00a0
                    00A8    233 _IE	=	0x00a8
                    00B0    234 _P3	=	0x00b0
                    00B8    235 _IP	=	0x00b8
                    00D0    236 _PSW	=	0x00d0
                    00E0    237 _ACC	=	0x00e0
                    00F0    238 _B	=	0x00f0
                    00C8    239 _T2CON	=	0x00c8
                    00CA    240 _RCAP2L	=	0x00ca
                    00CB    241 _RCAP2H	=	0x00cb
                    00CC    242 _TL2	=	0x00cc
                    00CD    243 _TH2	=	0x00cd
                    008E    244 _AUXR	=	0x008e
                    00A2    245 _AUXR1	=	0x00a2
                    0097    246 _CKRL	=	0x0097
                    008F    247 _CKCKON0	=	0x008f
                    008F    248 _CKCKON1	=	0x008f
                    00FA    249 _CCAP0H	=	0x00fa
                    00FB    250 _CCAP1H	=	0x00fb
                    00FC    251 _CCAP2H	=	0x00fc
                    00FD    252 _CCAP3H	=	0x00fd
                    00FE    253 _CCAP4H	=	0x00fe
                    00EA    254 _CCAP0L	=	0x00ea
                    00EB    255 _CCAP1L	=	0x00eb
                    00EC    256 _CCAP2L	=	0x00ec
                    00ED    257 _CCAP3L	=	0x00ed
                    00EE    258 _CCAP4L	=	0x00ee
                    00DA    259 _CCAPM0	=	0x00da
                    00DB    260 _CCAPM1	=	0x00db
                    00DC    261 _CCAPM2	=	0x00dc
                    00DD    262 _CCAPM3	=	0x00dd
                    00DE    263 _CCAPM4	=	0x00de
                    00D8    264 _CCON	=	0x00d8
                    00F9    265 _CH	=	0x00f9
                    00E9    266 _CL	=	0x00e9
                    00D9    267 _CMOD	=	0x00d9
                    00A8    268 _IEN0	=	0x00a8
                    00B1    269 _IEN1	=	0x00b1
                    00B8    270 _IPL0	=	0x00b8
                    00B7    271 _IPH0	=	0x00b7
                    00B2    272 _IPL1	=	0x00b2
                    00B3    273 _IPH1	=	0x00b3
                    00C0    274 _P4	=	0x00c0
                    00D8    275 _P5	=	0x00d8
                    00A6    276 _WDTRST	=	0x00a6
                    00A7    277 _WDTPRG	=	0x00a7
                    00A9    278 _SADDR	=	0x00a9
                    00B9    279 _SADEN	=	0x00b9
                    00C3    280 _SPCON	=	0x00c3
                    00C4    281 _SPSTA	=	0x00c4
                    00C5    282 _SPDAT	=	0x00c5
                    00C9    283 _T2MOD	=	0x00c9
                    009B    284 _BDRCON	=	0x009b
                    009A    285 _BRL	=	0x009a
                    009C    286 _KBLS	=	0x009c
                    009D    287 _KBE	=	0x009d
                    009E    288 _KBF	=	0x009e
                    00D2    289 _EECON	=	0x00d2
                            290 ;--------------------------------------------------------
                            291 ; special function bits
                            292 ;--------------------------------------------------------
                            293 	.area RSEG    (DATA)
                    0080    294 _P0_0	=	0x0080
                    0081    295 _P0_1	=	0x0081
                    0082    296 _P0_2	=	0x0082
                    0083    297 _P0_3	=	0x0083
                    0084    298 _P0_4	=	0x0084
                    0085    299 _P0_5	=	0x0085
                    0086    300 _P0_6	=	0x0086
                    0087    301 _P0_7	=	0x0087
                    0088    302 _IT0	=	0x0088
                    0089    303 _IE0	=	0x0089
                    008A    304 _IT1	=	0x008a
                    008B    305 _IE1	=	0x008b
                    008C    306 _TR0	=	0x008c
                    008D    307 _TF0	=	0x008d
                    008E    308 _TR1	=	0x008e
                    008F    309 _TF1	=	0x008f
                    0090    310 _P1_0	=	0x0090
                    0091    311 _P1_1	=	0x0091
                    0092    312 _P1_2	=	0x0092
                    0093    313 _P1_3	=	0x0093
                    0094    314 _P1_4	=	0x0094
                    0095    315 _P1_5	=	0x0095
                    0096    316 _P1_6	=	0x0096
                    0097    317 _P1_7	=	0x0097
                    0098    318 _RI	=	0x0098
                    0099    319 _TI	=	0x0099
                    009A    320 _RB8	=	0x009a
                    009B    321 _TB8	=	0x009b
                    009C    322 _REN	=	0x009c
                    009D    323 _SM2	=	0x009d
                    009E    324 _SM1	=	0x009e
                    009F    325 _SM0	=	0x009f
                    00A0    326 _P2_0	=	0x00a0
                    00A1    327 _P2_1	=	0x00a1
                    00A2    328 _P2_2	=	0x00a2
                    00A3    329 _P2_3	=	0x00a3
                    00A4    330 _P2_4	=	0x00a4
                    00A5    331 _P2_5	=	0x00a5
                    00A6    332 _P2_6	=	0x00a6
                    00A7    333 _P2_7	=	0x00a7
                    00A8    334 _EX0	=	0x00a8
                    00A9    335 _ET0	=	0x00a9
                    00AA    336 _EX1	=	0x00aa
                    00AB    337 _ET1	=	0x00ab
                    00AC    338 _ES	=	0x00ac
                    00AF    339 _EA	=	0x00af
                    00B0    340 _P3_0	=	0x00b0
                    00B1    341 _P3_1	=	0x00b1
                    00B2    342 _P3_2	=	0x00b2
                    00B3    343 _P3_3	=	0x00b3
                    00B4    344 _P3_4	=	0x00b4
                    00B5    345 _P3_5	=	0x00b5
                    00B6    346 _P3_6	=	0x00b6
                    00B7    347 _P3_7	=	0x00b7
                    00B0    348 _RXD	=	0x00b0
                    00B1    349 _TXD	=	0x00b1
                    00B2    350 _INT0	=	0x00b2
                    00B3    351 _INT1	=	0x00b3
                    00B4    352 _T0	=	0x00b4
                    00B5    353 _T1	=	0x00b5
                    00B6    354 _WR	=	0x00b6
                    00B7    355 _RD	=	0x00b7
                    00B8    356 _PX0	=	0x00b8
                    00B9    357 _PT0	=	0x00b9
                    00BA    358 _PX1	=	0x00ba
                    00BB    359 _PT1	=	0x00bb
                    00BC    360 _PS	=	0x00bc
                    00D0    361 _P	=	0x00d0
                    00D1    362 _F1	=	0x00d1
                    00D2    363 _OV	=	0x00d2
                    00D3    364 _RS0	=	0x00d3
                    00D4    365 _RS1	=	0x00d4
                    00D5    366 _F0	=	0x00d5
                    00D6    367 _AC	=	0x00d6
                    00D7    368 _CY	=	0x00d7
                    00AD    369 _ET2	=	0x00ad
                    00BD    370 _PT2	=	0x00bd
                    00C8    371 _T2CON_0	=	0x00c8
                    00C9    372 _T2CON_1	=	0x00c9
                    00CA    373 _T2CON_2	=	0x00ca
                    00CB    374 _T2CON_3	=	0x00cb
                    00CC    375 _T2CON_4	=	0x00cc
                    00CD    376 _T2CON_5	=	0x00cd
                    00CE    377 _T2CON_6	=	0x00ce
                    00CF    378 _T2CON_7	=	0x00cf
                    00C8    379 _CP_RL2	=	0x00c8
                    00C9    380 _C_T2	=	0x00c9
                    00CA    381 _TR2	=	0x00ca
                    00CB    382 _EXEN2	=	0x00cb
                    00CC    383 _TCLK	=	0x00cc
                    00CD    384 _RCLK	=	0x00cd
                    00CE    385 _EXF2	=	0x00ce
                    00CF    386 _TF2	=	0x00cf
                    00DF    387 _CF	=	0x00df
                    00DE    388 _CR	=	0x00de
                    00DC    389 _CCF4	=	0x00dc
                    00DB    390 _CCF3	=	0x00db
                    00DA    391 _CCF2	=	0x00da
                    00D9    392 _CCF1	=	0x00d9
                    00D8    393 _CCF0	=	0x00d8
                    00AE    394 _EC	=	0x00ae
                    00BE    395 _PPCL	=	0x00be
                    00BD    396 _PT2L	=	0x00bd
                    00BC    397 _PLS	=	0x00bc
                    00BB    398 _PT1L	=	0x00bb
                    00BA    399 _PX1L	=	0x00ba
                    00B9    400 _PT0L	=	0x00b9
                    00B8    401 _PX0L	=	0x00b8
                    00C0    402 _P4_0	=	0x00c0
                    00C1    403 _P4_1	=	0x00c1
                    00C2    404 _P4_2	=	0x00c2
                    00C3    405 _P4_3	=	0x00c3
                    00C4    406 _P4_4	=	0x00c4
                    00C5    407 _P4_5	=	0x00c5
                    00C6    408 _P4_6	=	0x00c6
                    00C7    409 _P4_7	=	0x00c7
                    00D8    410 _P5_0	=	0x00d8
                    00D9    411 _P5_1	=	0x00d9
                    00DA    412 _P5_2	=	0x00da
                    00DB    413 _P5_3	=	0x00db
                    00DC    414 _P5_4	=	0x00dc
                    00DD    415 _P5_5	=	0x00dd
                    00DE    416 _P5_6	=	0x00de
                    00DF    417 _P5_7	=	0x00df
                            418 ;--------------------------------------------------------
                            419 ; overlayable register banks
                            420 ;--------------------------------------------------------
                            421 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     422 	.ds 8
                            423 	.area REG_BANK_1	(REL,OVR,DATA)
   0008                     424 	.ds 8
                            425 ;--------------------------------------------------------
                            426 ; internal ram data
                            427 ;--------------------------------------------------------
                            428 	.area DSEG    (DATA)
                            429 ;--------------------------------------------------------
                            430 ; overlayable items in internal ram 
                            431 ;--------------------------------------------------------
                            432 	.area OSEG    (OVR,DATA)
                            433 ;--------------------------------------------------------
                            434 ; Stack segment in internal ram 
                            435 ;--------------------------------------------------------
                            436 	.area	SSEG	(DATA)
   0010                     437 __start__stack:
   0010                     438 	.ds	1
                            439 
                            440 ;--------------------------------------------------------
                            441 ; indirectly addressable internal ram data
                            442 ;--------------------------------------------------------
                            443 	.area ISEG    (DATA)
                            444 ;--------------------------------------------------------
                            445 ; bit data
                            446 ;--------------------------------------------------------
                            447 	.area BSEG    (BIT)
                            448 ;--------------------------------------------------------
                            449 ; paged external ram data
                            450 ;--------------------------------------------------------
                            451 	.area PSEG    (PAG,XDATA)
                            452 ;--------------------------------------------------------
                            453 ; external ram data
                            454 ;--------------------------------------------------------
                            455 	.area XSEG    (XDATA)
   0000                     456 _putchar_x_1_1:
   0000                     457 	.ds 1
                            458 ;--------------------------------------------------------
                            459 ; external initialized ram data
                            460 ;--------------------------------------------------------
                            461 	.area XISEG   (XDATA)
                            462 	.area HOME    (CODE)
                            463 	.area GSINIT0 (CODE)
                            464 	.area GSINIT1 (CODE)
                            465 	.area GSINIT2 (CODE)
                            466 	.area GSINIT3 (CODE)
                            467 	.area GSINIT4 (CODE)
                            468 	.area GSINIT5 (CODE)
                            469 	.area GSINIT  (CODE)
                            470 	.area GSFINAL (CODE)
                            471 	.area CSEG    (CODE)
                            472 ;--------------------------------------------------------
                            473 ; interrupt vector 
                            474 ;--------------------------------------------------------
                            475 	.area HOME    (CODE)
   0000                     476 __interrupt_vect:
   0000 02 00 26            477 	ljmp	__sdcc_gsinit_startup
   0003 32                  478 	reti
   0004                     479 	.ds	7
   000B 02 00 B2            480 	ljmp	_timer0_isr
   000E                     481 	.ds	5
   0013 32                  482 	reti
   0014                     483 	.ds	7
   001B 32                  484 	reti
   001C                     485 	.ds	7
   0023 02 00 CC            486 	ljmp	_serial_isr
                            487 ;--------------------------------------------------------
                            488 ; global & static initialisations
                            489 ;--------------------------------------------------------
                            490 	.area HOME    (CODE)
                            491 	.area GSINIT  (CODE)
                            492 	.area GSFINAL (CODE)
                            493 	.area GSINIT  (CODE)
                            494 	.globl __sdcc_gsinit_startup
                            495 	.globl __sdcc_program_startup
                            496 	.globl __start__stack
                            497 	.globl __mcs51_genXINIT
                            498 	.globl __mcs51_genXRAMCLEAR
                            499 	.globl __mcs51_genRAMCLEAR
                            500 	.area GSFINAL (CODE)
   007F 02 00 82            501 	ljmp	__sdcc_program_startup
                            502 ;--------------------------------------------------------
                            503 ; Home
                            504 ;--------------------------------------------------------
                            505 	.area HOME    (CODE)
                            506 	.area CSEG    (CODE)
   0082                     507 __sdcc_program_startup:
   0082 12 00 CD            508 	lcall	_main
                            509 ;	return from main will lock up
   0085 80 FE               510 	sjmp .
                            511 ;--------------------------------------------------------
                            512 ; code
                            513 ;--------------------------------------------------------
                            514 	.area CSEG    (CODE)
                            515 ;------------------------------------------------------------
                            516 ;Allocation info for local variables in function 'SerialInitialize'
                            517 ;------------------------------------------------------------
                            518 ;------------------------------------------------------------
                            519 ;	main.c:11: void SerialInitialize()
                            520 ;	-----------------------------------------
                            521 ;	 function SerialInitialize
                            522 ;	-----------------------------------------
   0087                     523 _SerialInitialize:
                    0002    524 	ar2 = 0x02
                    0003    525 	ar3 = 0x03
                    0004    526 	ar4 = 0x04
                    0005    527 	ar5 = 0x05
                    0006    528 	ar6 = 0x06
                    0007    529 	ar7 = 0x07
                    0000    530 	ar0 = 0x00
                    0001    531 	ar1 = 0x01
                            532 ;	main.c:13: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
                            533 ;	genAssign
   0087 75 89 20            534 	mov	_TMOD,#0x20
                            535 ;	main.c:14: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
                            536 ;	genAssign
   008A 75 8D FD            537 	mov	_TH1,#0xFD
                            538 ;	main.c:15: SCON=0x50;
                            539 ;	genAssign
   008D 75 98 50            540 	mov	_SCON,#0x50
                            541 ;	main.c:16: TR1=1;  // to start timer
                            542 ;	genAssign
   0090 D2 8E               543 	setb	_TR1
                            544 ;	main.c:17: IE=0x90; // to make serial interrupt interrupt enable
                            545 ;	genAssign
   0092 75 A8 90            546 	mov	_IE,#0x90
                            547 ;	Peephole 300	removed redundant label 00101$
   0095 22                  548 	ret
                            549 ;------------------------------------------------------------
                            550 ;Allocation info for local variables in function 'putchar'
                            551 ;------------------------------------------------------------
                            552 ;x                         Allocated with name '_putchar_x_1_1'
                            553 ;------------------------------------------------------------
                            554 ;	main.c:20: void putchar(char x)
                            555 ;	-----------------------------------------
                            556 ;	 function putchar
                            557 ;	-----------------------------------------
   0096                     558 _putchar:
                            559 ;	genReceive
   0096 E5 82               560 	mov	a,dpl
   0098 90 00 00            561 	mov	dptr,#_putchar_x_1_1
   009B F0                  562 	movx	@dptr,a
                            563 ;	main.c:22: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
                            564 ;	genAssign
   009C 90 00 00            565 	mov	dptr,#_putchar_x_1_1
   009F E0                  566 	movx	a,@dptr
   00A0 F5 99               567 	mov	_SBUF,a
                            568 ;	main.c:23: while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
   00A2                     569 00101$:
                            570 ;	genIfx
                            571 ;	genIfxJump
                            572 ;	Peephole 108.d	removed ljmp by inverse jump logic
                            573 ;	main.c:24: TI=0; // make TI bit to zero for next transmission
                            574 ;	genAssign
                            575 ;	Peephole 250.a	using atomic test and clear
   00A2 10 99 02            576 	jbc	_TI,00108$
   00A5 80 FB               577 	sjmp	00101$
   00A7                     578 00108$:
                            579 ;	Peephole 300	removed redundant label 00104$
   00A7 22                  580 	ret
                            581 ;------------------------------------------------------------
                            582 ;Allocation info for local variables in function 'getchar'
                            583 ;------------------------------------------------------------
                            584 ;rec                       Allocated with name '_getchar_rec_1_1'
                            585 ;------------------------------------------------------------
                            586 ;	main.c:28: char getchar()
                            587 ;	-----------------------------------------
                            588 ;	 function getchar
                            589 ;	-----------------------------------------
   00A8                     590 _getchar:
                            591 ;	main.c:32: while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
   00A8                     592 00101$:
                            593 ;	genIfx
                            594 ;	genIfxJump
                            595 ;	Peephole 108.d	removed ljmp by inverse jump logic
   00A8 30 98 FD            596 	jnb	_RI,00101$
                            597 ;	Peephole 300	removed redundant label 00108$
                            598 ;	main.c:33: rec=SBUF;   // data is received in SBUF register present in controller during receiving
                            599 ;	genAssign
   00AB AA 99               600 	mov	r2,_SBUF
                            601 ;	main.c:34: RI=0;  // make RI bit to 0 so that next data is received
                            602 ;	genAssign
   00AD C2 98               603 	clr	_RI
                            604 ;	main.c:35: return rec;  // return rec where function is called
                            605 ;	genRet
   00AF 8A 82               606 	mov	dpl,r2
                            607 ;	Peephole 300	removed redundant label 00104$
   00B1 22                  608 	ret
                            609 ;------------------------------------------------------------
                            610 ;Allocation info for local variables in function 'timer0_isr'
                            611 ;------------------------------------------------------------
                            612 ;i                         Allocated with name '_timer0_isr_i_1_1'
                            613 ;------------------------------------------------------------
                            614 ;	main.c:37: void timer0_isr(void) interrupt 1 __using(1)
                            615 ;	-----------------------------------------
                            616 ;	 function timer0_isr
                            617 ;	-----------------------------------------
   00B2                     618 _timer0_isr:
                    000A    619 	ar2 = 0x0a
                    000B    620 	ar3 = 0x0b
                    000C    621 	ar4 = 0x0c
                    000D    622 	ar5 = 0x0d
                    000E    623 	ar6 = 0x0e
                    000F    624 	ar7 = 0x0f
                    0008    625 	ar0 = 0x08
                    0009    626 	ar1 = 0x09
   00B2 C0 D0               627 	push	psw
   00B4 75 D0 08            628 	mov	psw,#0x08
                            629 ;	main.c:40: TR0=0;
                            630 ;	genAssign
   00B7 C2 8C               631 	clr	_TR0
                            632 ;	main.c:41: P1_6 = 0;
                            633 ;	genAssign
   00B9 C2 96               634 	clr	_P1_6
                            635 ;	main.c:42: for (i=0; i<0xFF; i++)
                            636 ;	genAssign
   00BB 7A FF               637 	mov	r2,#0xFF
   00BD                     638 00103$:
                            639 ;	genDjnz
                            640 ;	Peephole 112.b	changed ljmp to sjmp
                            641 ;	Peephole 205	optimized misc jump sequence
   00BD DA FE               642 	djnz	r2,00103$
                            643 ;	Peephole 300	removed redundant label 00109$
                            644 ;	Peephole 300	removed redundant label 00110$
                            645 ;	main.c:44: P1_6 = 1;
                            646 ;	genAssign
   00BF D2 96               647 	setb	_P1_6
                            648 ;	main.c:45: TH0 = 0x4C;
                            649 ;	genAssign
   00C1 75 8C 4C            650 	mov	_TH0,#0x4C
                            651 ;	main.c:46: TL0 = 0x08;
                            652 ;	genAssign
   00C4 75 8A 08            653 	mov	_TL0,#0x08
                            654 ;	main.c:47: TR0=1;
                            655 ;	genAssign
   00C7 D2 8C               656 	setb	_TR0
                            657 ;	Peephole 300	removed redundant label 00104$
   00C9 D0 D0               658 	pop	psw
   00CB 32                  659 	reti
                            660 ;	eliminated unneeded push/pop dpl
                            661 ;	eliminated unneeded push/pop dph
                            662 ;	eliminated unneeded push/pop b
                            663 ;	eliminated unneeded push/pop acc
                            664 ;------------------------------------------------------------
                            665 ;Allocation info for local variables in function 'serial_isr'
                            666 ;------------------------------------------------------------
                            667 ;------------------------------------------------------------
                            668 ;	main.c:50: void serial_isr(void) interrupt 4
                            669 ;	-----------------------------------------
                            670 ;	 function serial_isr
                            671 ;	-----------------------------------------
   00CC                     672 _serial_isr:
                    0002    673 	ar2 = 0x02
                    0003    674 	ar3 = 0x03
                    0004    675 	ar4 = 0x04
                    0005    676 	ar5 = 0x05
                    0006    677 	ar6 = 0x06
                    0007    678 	ar7 = 0x07
                    0000    679 	ar0 = 0x00
                    0001    680 	ar1 = 0x01
                            681 ;	main.c:53: }
                            682 ;	Peephole 300	removed redundant label 00101$
   00CC 32                  683 	reti
                            684 ;	eliminated unneeded push/pop psw
                            685 ;	eliminated unneeded push/pop dpl
                            686 ;	eliminated unneeded push/pop dph
                            687 ;	eliminated unneeded push/pop b
                            688 ;	eliminated unneeded push/pop acc
                            689 ;------------------------------------------------------------
                            690 ;Allocation info for local variables in function 'main'
                            691 ;------------------------------------------------------------
                            692 ;in                        Allocated with name '_main_in_1_1'
                            693 ;------------------------------------------------------------
                            694 ;	main.c:54: void main(void)
                            695 ;	-----------------------------------------
                            696 ;	 function main
                            697 ;	-----------------------------------------
   00CD                     698 _main:
                            699 ;	main.c:57: SerialInitialize();
                            700 ;	genCall
   00CD 12 00 87            701 	lcall	_SerialInitialize
                            702 ;	main.c:58: puts("hello");
                            703 ;	genCall
                            704 ;	Peephole 182.a	used 16 bit load of DPTR
   00D0 90 01 98            705 	mov	dptr,#__str_0
   00D3 75 F0 80            706 	mov	b,#0x80
   00D6 12 00 FA            707 	lcall	_puts
                            708 ;	main.c:59: TMOD &= 0x00;
                            709 ;	genDummyRead
   00D9 E5 89               710 	mov	a,_TMOD
                            711 ;	genAssign
   00DB 75 89 00            712 	mov	_TMOD,#0x00
                            713 ;	main.c:60: TMOD |= 0x01;
                            714 ;	genOr
   00DE 43 89 01            715 	orl	_TMOD,#0x01
                            716 ;	main.c:61: TH0 = 0x4C;
                            717 ;	genAssign
   00E1 75 8C 4C            718 	mov	_TH0,#0x4C
                            719 ;	main.c:62: TL0 = 0x08;
                            720 ;	genAssign
   00E4 75 8A 08            721 	mov	_TL0,#0x08
                            722 ;	main.c:64: ET0=1;
                            723 ;	genAssign
   00E7 D2 A9               724 	setb	_ET0
                            725 ;	main.c:65: EA=1;
                            726 ;	genAssign
   00E9 D2 AF               727 	setb	_EA
                            728 ;	main.c:66: ACC=0x01;
                            729 ;	genAssign
   00EB 75 E0 01            730 	mov	_ACC,#0x01
                            731 ;	main.c:67: TR0 = 1;
                            732 ;	genAssign
   00EE D2 8C               733 	setb	_TR0
                            734 ;	main.c:70: while(1)
   00F0                     735 00102$:
                            736 ;	main.c:72: in=getchar();
                            737 ;	genCall
   00F0 12 00 A8            738 	lcall	_getchar
                            739 ;	main.c:73: putchar(in);
                            740 ;	genCall
   00F3 AA 82               741 	mov  r2,dpl
                            742 ;	Peephole 177.a	removed redundant mov
   00F5 12 00 96            743 	lcall	_putchar
                            744 ;	Peephole 112.b	changed ljmp to sjmp
   00F8 80 F6               745 	sjmp	00102$
                            746 ;	Peephole 259.a	removed redundant label 00104$ and ret
                            747 ;
                            748 	.area CSEG    (CODE)
                            749 	.area CONST   (CODE)
   0198                     750 __str_0:
   0198 68 65 6C 6C 6F      751 	.ascii "hello"
   019D 00                  752 	.db 0x00
                            753 	.area XINIT   (CODE)
