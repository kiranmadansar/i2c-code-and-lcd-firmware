/*
 */

/* INCLUDE ALL THE HEADER FILE NEEDED*/
#include <mcs51/8051.h>
#include <at89c51ed2.h>
#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>

void SerialInitialize()
{
    TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
    TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
    SCON=0x50;
    TR1=1;  // to start timer
    IE=0x90; // to make serial interrupt interrupt enable
}

void putchar(char x)
{
    SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
    while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
    TI=0; // make TI bit to zero for next transmission
}


char getchar()
{
    char rec;
    /* RI bit is present in SCON register*/
    while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
    rec=SBUF;   // data is received in SBUF register present in controller during receiving
    RI=0;  // make RI bit to 0 so that next data is received
    return rec;  // return rec where function is called
}
void timer0_isr(void) interrupt 1 __using(1)
{
    unsigned char i;
    TR0=0;
    P1_6 = 0;
    for (i=0; i<0xFF; i++)
        ;
    P1_6 = 1;
    TH0 = 0x4C;
    TL0 = 0x08;
    TR0=1;
}

void serial_isr(void) interrupt 4
{
    ;
}
void main(void)
{
    unsigned char in;
    SerialInitialize();
    puts("hello");
    TMOD &= 0x00;
    TMOD |= 0x01;
    TH0 = 0x4C;
    TL0 = 0x08;
    //IE = 0x82;
    ET0=1;
    EA=1;
    ACC=0x01;
    TR0 = 1;
    // Insert code

    while(1)
        {
            in=getchar();
            putchar(in);
        }

}
