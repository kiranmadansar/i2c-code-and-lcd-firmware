/*

UNIVERSITY OF COLORADO BOULDER

C PROGRAM WHICH DEALS WITH I2C COMMUNICATION WITH EEPROM 24LC16B
AND WITH LCD MODULE.
PROVIDES A FRIENDLY COMMAND LINE INTERFACE FOR THE USER

WRITTEN BY KIRAN NARAYANA HEGDE (kihe6592@colorado.edu)

SUBMITTED AS PART OF THE LAB4 REQUIRED ELEMENT SUBMISSION ECEN5613 - EMBEDDED SYSTEM DESIGN

SPECIAL THANKS TO SDCC LIBRARY FOR PROVIDING REFERNCES ON I2C DRIVER

*/

/* INCLUDE ALL THE HEADER FILE NEEDED*/
#include <mcs51/8051.h>
#include <at89c51ed2.h>
#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>

/*DEFINING PINS FOR I2C COMMUNICATION*/
#define SDA P1.4
#define SCL P1.3
#define SDA_C P1_4
#define SCL_C P1_3

/* DEFINING THE MACRO FOR MEMORY MAPPED PERIPHERAL COMMUNICATION */
#define INST_WRITE ((xdata unsigned char *)0xF000)
#define DATA_WRITE ((xdata unsigned char *)0xF400)
#define DATA_READ ((xdata unsigned char *)0xFC00)
#define INST_READ ((xdata unsigned char *)0xF800)

/* DEFINING THE BASE ADDRESS FOR LCD ROWS */
#define BASE_ADDRROW0 0x00
#define BASE_ADDRROW1 0x40
#define BASE_ADDRROW2 0x10
#define BASE_ADDRROW3 0x50

#define BUSYFLAG_MASK_BITS 0x80
#define CURSORADDR_MASK_BITS 0x7F
#define DDRAM_WRITE_MASK_BITS 0x80
#define DDRAM_ADDR_MASK_BITS 0x7F
#define CGRAM_PIXEL_MASK_BITS 0x1F
#define CGRAM_ADDR_MASK_BITS 0x3F
#define I2C_BLOCKADDR_MASK_BITS 0x700
#define I2C_ADDR_MASK_BITS 0x7FF
#define I2C_READ_MASK_BITS 0x01
#define I2C_WRITE_MASK_BITS 0xFE
#define I2C_CONTROLBYTE_BITS 0xA0
#define I2C_BLOCKADD_SHIFT_BITS 7
#define READ_ENTER 13
#define READ_BACKSPACE 127
#define CGRAM_FIRSTCHAR_ADDR 0x00
#define CGRAM_SECONDCHAR_ADDR 0X08
#define CGRAM_THIRDCHAR_ADDR 0X10
#define CGRAM_FOURTHCHAR_ADDR 0X18
#define CGRAM_FIFTHCHAR_ADDR 0X20
#define CGRAM_SIXTHCHAR_ADDR 0X28
#define CGRAM_SEVENTHCHAR_ADDR 0X30
#define CGRAM_EIGHTHCHAR_ADDR 0X38
#define UNLOCK_COMMAND 0x30
#define FUNCTION_SET_LINES_FONT 0x38
#define DISPLAY_OFF 0x08
#define DISPLAY_ON 0x0C
#define ENTRY_MODE_SET 0x06
#define CLEARSCREEN_CURSOR_HOME 0x01
#define CLOCK_CGRAM_BASE_ADDR 0x30
#define IOEXP_CB_BITS 0x70
#define IOEXP_INIT_BITS 0x01


#pragma nooverlay
/*DEFINING GLOBAL VARIABLES */
unsigned volatile int t;
//unsigned char rec;
unsigned char array[8];
volatile unsigned char u;
volatile unsigned char MS;
volatile unsigned char S, SS;
volatile unsigned char M, MM;
volatile unsigned char CLOCK_ENABLE_FLAG=1;
register count_flag;

unsigned int confirm=1;

/* DELAY FUNCTION DEFINED FOR LCD COMMUNICATION */
void MSDelay(unsigned int value)
{
    unsigned int x, zz;
    for(zz=0;zz<value;zz++)
    {
        for(x=0;x<126;x++)
        {
            ;
        }
    }

}

  /* DISPLAY STIRING ON THE SERIAL TERMINAL */
void putstr(char *ptr)
{
        while(*ptr)
        {
            putchar(*ptr);
            ptr++;
        }

}
  /* Description: Polls the LCD busy flag. Function does not return
  until the LCD controller is ready to accept another command */
void lcdbusywait()
{
    unsigned volatile int i;
    EA=0;
    i=*INST_READ;
    EA=1;
    MSDelay(1);
    while((i&BUSYFLAG_MASK_BITS)==BUSYFLAG_MASK_BITS)
    {
        EA=0;
        i = *INST_READ;
        EA=1;
        MSDelay(1);
    }
}

/* FUNCTION TO GET THE USER INPUT FROM SERIAL TERMINAL */
unsigned char input_value()
{
    unsigned volatile char l=0, m=0;
    do
    {
        l=getchar();
        if(l>='0' && l<='9')
        {
            m=((m*10)+(l-'0'));
            putchar(l);                 /* CONVERTING IT TO DECIMAL VALUE*/
            //k++;
        }
        else if(l==READ_BACKSPACE)
        {
            m=m/10;
        }
        else if(l!=READ_ENTER)
            {putstr("\n\rEnter Numbers\n\r");}
    }while(l!=READ_ENTER);
    return m;                                   /* UNTIL ENTER IS PRESSES*/
}

void lcdclockgotoaddr(unsigned char addr)
{
    if(addr>=0x59 && addr<=0x5F)
    {
        unsigned char temp;
        temp = DDRAM_WRITE_MASK_BITS;
        temp |= addr;
        *INST_WRITE = temp;
        lcdbusywait();
    }
}

/*GOTO ADDRESS FOR THE EEPROM. USER HAS TO INPUT THE ADDRESS*/
void lcdgotoaddr(unsigned char addr)
{
    if(addr>=0x00 && addr<=0x58)
    {
        unsigned char temp;
        temp = DDRAM_WRITE_MASK_BITS;
        temp |= addr;
        EA=0;
        *INST_WRITE = temp;
        EA=1;
        lcdbusywait();
    }
    else
        putstr("\n\rCan't move to given position\n\r");
}

/* GO TO THE X ROW, Y COLUMN CURSOR POSITION */
void lcdgotoxy(unsigned char row, unsigned char column)
{
        switch(row)
        {
            case 0: lcdgotoaddr((BASE_ADDRROW0+column));
                    break;
            case 1: lcdgotoaddr((BASE_ADDRROW1+column));
                    break;
            case 2: lcdgotoaddr((BASE_ADDRROW2+column));
                    break;
            case 3: lcdgotoaddr((BASE_ADDRROW3+column));
                    break;
        }
}

/* CLEARS THE LCD AND MOVES THE CURSOR TO HOME POSITION*/
void lcdclear()
{
    EA=0;
    *INST_WRITE = CLEARSCREEN_CURSOR_HOME;
    EA=1;
    lcdbusywait();
    putstr("Clearing the LCD completed\n\r");
}

/* DISPLAY A CHARACTER IN THE LCD */
void lcdputch(unsigned char cc)
{
    unsigned volatile int aa, tempput;
    EA=0;
    *DATA_WRITE = cc;
    EA=1;
    lcdbusywait();
    EA=0;
    aa=*INST_READ;
    EA=1;
        tempput = aa&CURSORADDR_MASK_BITS;              /* THIS LOOP HANDLES THE ROW ADDRESS MISMATCH OF THE LCD*/
        if(tempput==0x10)
        {
            lcdgotoxy(1,0);
        }
        else if(tempput==0x50)
        {
            lcdgotoxy(2,0);
        }
        else if(tempput==0x20)
        {
            lcdgotoxy(3,0);
        }
        else if(tempput==0x59)
        {
            lcdgotoxy(0,0);//lcdgotoaddr(temp);
        }
        else
        {
            ;
        }
}


/* dISPLAY STRING ON THE LCD*/
void lcdputstr(unsigned char *ptr)
{
    while(*ptr)
    {
        lcdputch(*ptr);
        ptr++;
    }
}

/*-----------------------------------------------------------------------------------------------*/
/* Name: lcdinit()
 Description: Initializes the LCD (see Figure25 on page 212 )
  of the HD44780U data sheet)
*/
void lcdinit()
{
    MSDelay(20);
   *INST_WRITE = UNLOCK_COMMAND;
   MSDelay(8);
   *INST_WRITE = UNLOCK_COMMAND;
   MSDelay(3);
   *INST_WRITE = UNLOCK_COMMAND;
   lcdbusywait();
   *INST_WRITE = FUNCTION_SET_LINES_FONT;
   lcdbusywait();
   *INST_WRITE = DISPLAY_OFF;
   lcdbusywait();
   *INST_WRITE = DISPLAY_ON;
   lcdbusywait();
   *INST_WRITE = ENTRY_MODE_SET;
   lcdbusywait();
   *INST_WRITE = CLEARSCREEN_CURSOR_HOME;
   lcdbusywait();
}

/* DISPLAY THE HEX VALUES OF CGRAM DATA*/
void cgramdump()
{
    unsigned int count_row=0;
    unsigned int count=0;
    unsigned char temp_val=0x40, k;
    EA=0;
    k=*INST_READ;
    //*INST_WRITE = temp_val;
    EA=1;
    lcdbusywait();
    putstr("\n\r");
    putstr("CGRAM values\n\r");
    putstr("\n\rCCODE |  Values\n\r");
        for (count_row=0; count_row<=7; count_row++)
        {
            printf("0x%04x:  ", (temp_val&CGRAM_ADDR_MASK_BITS));
            for(count=0; count<=7; count++)
            {
                ET0=0;
                *INST_WRITE = temp_val;
                lcdbusywait();
                printf("%02x  ",*DATA_READ);
                ET0=1;
                temp_val++;
                lcdbusywait();
            }
            putstr("\n\r");
        }
        putstr("\n\r");
        EA=0;
        *INST_WRITE=(k|DDRAM_WRITE_MASK_BITS);
        EA=1;
        lcdbusywait();

}

/* CREATE A NEW CUSTOM CHARACTER */
void lcdcreatechar(unsigned char ccode, unsigned char row_vals[])
{
    unsigned char n, y;
    EA=0;
    y=*INST_READ;
    EA=1;
    lcdbusywait();
    ccode |= 0x40;
    for (n=0; n<=7; n++)
    {
        ET0=0;
        *INST_WRITE = ccode;
        lcdbusywait();
        *DATA_WRITE = (row_vals[n]&CGRAM_PIXEL_MASK_BITS);
        ET0=1;
        lcdbusywait();
        ccode++;
    }
    EA=0;
    *INST_WRITE=(y|DDRAM_WRITE_MASK_BITS);
    EA=1;
    lcdbusywait();
}

/* DISPLAY THE HEX VALUES OF DDRAM DATA*/
void ddramdump()
{
    unsigned int count_row=0;
    unsigned int count=0;
    unsigned char temp_val=0, k;
    EA=0;
    k=*INST_READ;
    EA=1;
    putstr("\n\r");
    putstr("DDRAM Values\n\r");
    putstr("\n\rADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r\n\r");
        for (count_row=0; count_row<=3; count_row++)
        {
            if(count_row==0)
                temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW0);
            if(count_row==1)
                temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW1);
            if(count_row==2)
                temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW2);
            if(count_row==3)
                temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW3);
            printf("0x%02x:  ", (temp_val&DDRAM_ADDR_MASK_BITS));
            for(count=0; count<=15; count++)
            {
                EA=0;
                *INST_WRITE = temp_val;
                EA=1;
                lcdbusywait();
                temp_val++;
                EA=0;
                printf("%02x  ",*DATA_READ);
                EA=1;
                lcdbusywait();
            }
            putstr("\n\r");
        }
        putstr("\n\r");
        EA=0;
        *INST_WRITE=(k|DDRAM_WRITE_MASK_BITS);
        EA=1;
        lcdbusywait();
}

/* READS THE BINARY INPUT FOR THE CUSTOM CHARACTER GENERATION FUNCTION*/
unsigned char readinput()
{
    unsigned char value=0, i, j=0;
    do
    {
        i=getchar();
        if(i>='0'&&i<='1')          /* works only if its a number */
        {
            value=((value*2)+(i-'0')); /* Convert character to integers */
            putchar(i);
            j++;
        }
        else if(i==READ_BACKSPACE)
        {
            value=value/2;
        }
        else if(i!=READ_ENTER)
            {putstr("\n\rEnter Valid Numbers\n\r");}
    }while(i!=READ_ENTER);
    j=0;
    return value;
}

/* cREATES A CUSTOM CHARACTER */
void addcharacter()
{
    unsigned char temp, y;
    if(u<8)
    {
        putstr("\n\rEnter eight pixel values\n\r");
        putstr("Enter five binary values for every pixel\n\r");
    //if(add_val<=64)
    //{
        for(temp=0; temp<=7; temp++)
        {
            y=readinput();
            if(y>0x1F)
            {
                putstr("Invalid Number. Enter Again\n\r");
                temp--;
            }
            else
            {
              array[temp]=y;
            putstr("\n\r");
            }
        }
        if(u==0)
            lcdcreatechar(CGRAM_FIRSTCHAR_ADDR, array);
        else if(u==1)
            lcdcreatechar(CGRAM_SECONDCHAR_ADDR, array);
        else if(u==2)
            lcdcreatechar(CGRAM_THIRDCHAR_ADDR, array);
        else if(u==3)
            lcdcreatechar(CGRAM_FOURTHCHAR_ADDR, array);
        else if(u==4)
            lcdcreatechar(CGRAM_FIFTHCHAR_ADDR, array);
        else if(u==5)
            lcdcreatechar(CGRAM_SIXTHCHAR_ADDR, array);
        else if(u==6)
            lcdcreatechar(CGRAM_SEVENTHCHAR_ADDR, array);
        else if(u==7)
            lcdcreatechar(CGRAM_EIGHTHCHAR_ADDR, array);
        u++;
    }
    else
    {
        putstr("Eight custom character created. No more custom character please\n\r");
    }

}
 /* DISPLAY THE CUSTOM CHARACTER */
void display()
{
    unsigned char hey=0;
    putstr("Look at the LCD\n\r");
    while(hey<0x8)
    {
        lcdputch(hey);
        hey++;
    }
}

/* CONVERT HEX VALUES FROM THE HEX STRING
STACKOVERFLOW: https://stackoverflow.com/questions/10324/convert-a-hexadecimal-string-to-an-integer-efficiently-in-c
WRITTEN BY MARKUS SAFAR
*/
unsigned long hex2int(char *a, unsigned int len)
{
    int i;
    unsigned long val = 0;

    for(i=0;i<len;i++)
       if(a[i] <= 57)
        val += (a[i]-48)*(1<<(4*(len-1-i)));
       else
        val += (a[i]-55)*(1<<(4*(len-1-i)));

    return val;
}

uint16_t ReadVariable()
{
    uint16_t l=0, m=0, n, k=0;
    unsigned char ptr[]="";
    do
    {
        l=getchar();
        if(l!=READ_ENTER)
        {
            if(l!=READ_BACKSPACE)
            {
                ptr[k]=l;
                k++;
                putchar(l);
            }
            else
            {
                k--;
            }
        }
    }while(l!=READ_ENTER);
    n=hex2int(ptr,k);
    return n;
}

/* DYNAMICALLY PRITING THE DATA ON LCD */
void interactive()
{
    unsigned char co, i;
    EA=0;
    i=*INST_READ;
    EA=1;
    i=i&DDRAM_WRITE_MASK_BITS;
    EA=0;
    *INST_WRITE=i;
    EA=1;
    putstr("Enter the character to display. Hit enter to stop\n\r");
    do
    {   co=getchar();
        putchar(co);
        if(co!=READ_ENTER)
            lcdputch(co);
    }while(co!=READ_ENTER);
}

void gotoxy()
{
    unsigned char temp=10, temp1=20;
    putstr("\n\rEnter row number\n\r");
    do
    {
        temp=input_value();
        if(temp>3)
            putstr("\n\rEnter correct row\n\r");
    }while(temp>3);
    putstr("\n\rEnter column number\n\r");
    do
    {
        temp1=input_value();
        if(temp1>15)
            putstr("\n\rEnter correct column number\n\r");
    }while(temp1>15);
    lcdgotoxy(temp, temp1);
}

void gotoaddress()
{
    unsigned char temp=0x66;
    putstr("Enter the address in hex\n\r");
    while((temp>0x1F && temp<0x40)||(temp>=0x5F))
    {
        temp=ReadVariable();
        if((temp>0x1F && temp<0x40)||(temp>=0x5F))
            putstr("\n\rEnter correct address values in hex\n\r");
    }
    lcdgotoaddr(temp);
    putstr("\n\r");
}

_sdcc_external_startup()
{
    AUXR |= 0x0C;
    return 0;
}



void delay()
{
_asm
    DELAY:MOV R5, #5
    DEL1: MOV R4, #10
    DEL: NOP
        DJNZ r4, DEL
         DJNZ R5, DEL1
         ret
_endasm ;
}

void SerialInitialize()
{
    TMOD|=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
    TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
    TL1=0xFD;
    SCON=0x50;
    TR1=1;  // to start timer
    //IE |=0x80; // to make serial interrupt interrupt enable
}

void putchar(char x)
{
    SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
    while(!TI); // wait until transmission is completed. when transmission is completed TI bit become 1
    TI=0; // make TI bit to zero for next transmission
}


char getchar()
{
    char rec;
    /* RI bit is present in SCON register*/
    while(!RI);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
    rec=SBUF;   // data is received in SBUF register present in controller during receiving
    RI=0;  // make RI bit to 0 so that next data is received
    return rec;  // return rec where function is called
}

/* START BIT CONDITION FOR THE I2C*/
void startbit()
{
    _asm
        SETB SDA
        SETB SCL
        ACALL DELAY
        CLR SDA
        ACALL DELAY
        CLR SCL
        ACALL DELAY
    _endasm ;
}

/* STOP BIT CONDITION FOR THE I2C*/
void stopbit()
{
    _asm
        ACALL DELAY
        CLR SDA
        ACALL DELAY
        SETB SCL
        ACALL DELAY
        SETB SDA
        ACALL DELAY
    _endasm;
}

/*ACKNOWLEDGEMENT CONDITION FOR RECEIVING */
void ackbit()
{
    _asm
        CLR C
        ACALL DELAY
        SETB SDA
        ACALL DELAY
        SETB SCL
        ACALL DELAY
        MOV C, SDA
        ACALL DELAY
        CLR SCL
        ACALL DELAY
    _endasm;
    //return SDA_C
}

/* SENDING ACKNOWLEDGEMENT TO SLAVE*/
void Master_ack()
{
    _asm
        ACALL DELAY
        ACALL DELAY
        CLR SDA
        ACALL DELAY
        SETB SCL
        ACALL DELAY
        ACALL DELAY
        CLR SCL
        nop
        setb SDA
    _endasm;
}

/* SENDING DATA TO ASSEMBLY */
void send_data_assembly()
{
    _asm
        ACALL DELAY
        MOV R1, #8
        HERE:CLR C
            RLC A
            JNC NEXT
            SETB SDA
            AJMP NEXT1
        NEXT: CLR SDA
        NEXT1:
            ACALL DELAY
            SETB SCL
            ACALL DELAY
            CLR SCL
            ACALL DELAY
            DJNZ R1, HERE
    _endasm;
}

int send_data(unsigned char value)
{
    int status_send=1;
    ACC = value;
    CY=1;
    send_data_assembly();
    ackbit();
    ACC = 0x00;
    return status_send;
}

void ninthbit()
{
_asm
    ACALL DELAY
    SETB SDA
    ACALL DELAY
    SETB SCL
    ACALL DELAY
    ACALL DELAY
    CLR SCL
    ACALL DELAY
_endasm ;
}

/* TOGGLES THE CLOCK FOR DATAREAD*/
void clock_func()
{
    _asm
            MOV A, #0
            MOV R7, #8
        LOOP2: NOP
                ACALL DELAY
                clr SCL
                ACALL DELAY
                setb SCL
                MOV C, SDA
                RLC A
                DJNZ R7, LOOP2
                ACALL DELAY
                clr SCL
                ACALL DELAY
                //SETB SCL
    _endasm;
}

/* WRITING A BYTE OF DATA */
int eebytew(uint16_t addr, unsigned char databyte)  // write byte, returns status
{
    uint16_t address;
    int result, result1, result2;
    unsigned char temp, tempaddr;
    printf("\n\rWriting 0x%03X: 0x%02X\n\r",addr, databyte);
    temp = I2C_CONTROLBYTE_BITS;
    address=addr;
    address &=I2C_BLOCKADDR_MASK_BITS;
    address >>=I2C_BLOCKADD_SHIFT_BITS;
    temp |= address;
    temp &= I2C_WRITE_MASK_BITS;
    tempaddr=addr;
    startbit();
    result = send_data(temp);
    result1 = send_data(tempaddr);
    result2 = send_data(databyte);
    stopbit();
    confirm=1;
    return (result&result1);
}

/* READING A BYTE OF DATA*/
int eebyter(uint16_t addr)
{
    uint16_t address;
    unsigned char temp, temp1, tempaddr, backup;
    temp = I2C_CONTROLBYTE_BITS;
    address=addr;
    address &=I2C_BLOCKADDR_MASK_BITS;
    address >>=I2C_BLOCKADD_SHIFT_BITS;
    temp |= address;
    temp1=(temp&I2C_WRITE_MASK_BITS);
    tempaddr=addr;
    startbit();
    send_data(temp1);
    send_data(tempaddr);
    startbit();
    temp=temp | I2C_READ_MASK_BITS;
    send_data(temp);
    clock_func();
    stopbit();
    backup=ACC;
    printf("\n\r0x%03X: 0x%02X\n\r", addr, backup);
    return 0;
}

/* SEQUENTIAL READ FUNCTION */
void readall_function()
{
    uint16_t address;
    unsigned char temp, temp1, tempaddr, backup, i;
    uint16_t tempvalue, tempvalue1;
    putstr("Enter the address\n\r");
    do
    {
        tempvalue = ReadVariable();
        if(tempvalue>I2C_ADDR_MASK_BITS)
            putstr("Enter the correct value\n\r");
    }while(tempvalue>I2C_ADDR_MASK_BITS);
    putstr("\n\rEnter the second higher address\n\r");
    do
    {
        tempvalue1 = ReadVariable();
        if(tempvalue1>I2C_ADDR_MASK_BITS)
            putstr("Enter the correct value\n\r");
    }while(tempvalue1>I2C_ADDR_MASK_BITS);

    temp = I2C_CONTROLBYTE_BITS;
    address=tempvalue;
    address &=I2C_BLOCKADDR_MASK_BITS;
    address >>=I2C_BLOCKADD_SHIFT_BITS;
    temp |= address;
    temp1=(temp&I2C_WRITE_MASK_BITS);
    tempaddr=tempvalue;
    startbit();
    send_data(temp1);
    send_data(tempaddr);
    startbit();
    temp=temp | I2C_READ_MASK_BITS;
    send_data(temp);
    putstr("\n\r");
    putstr("\n\r");
    putstr("EEPROM data\n\r");
    putstr("ADDR |  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r");
    putstr("\n\r");
    while(tempvalue<=tempvalue1)
    {
        printf("0x%03X: ", tempvalue);
        for(i=0; i<=15; i++)
        {
            clock_func();
            backup=ACC;
            printf("%02X  ", backup);
            if(tempvalue!=tempvalue1)
            {
                Master_ack();
                tempvalue++;
            }
            else
            {
                tempvalue++;
                break;
            }
        }
        i=0;
        putstr("\n\r");
    }
    stopbit();

}

/* WRITE THE DATA TO EEPROM*/
void write_function()
{
    uint16_t temp=0x8000, temp1=0x100;
    putstr("Enter the address\n\r");
    do
    {
        temp = ReadVariable();
        if(temp>I2C_ADDR_MASK_BITS)
            putstr("Enter the correct value\n\r");
    }while(temp>I2C_ADDR_MASK_BITS);
    putstr("\n\rEnter the Value\n\r");
    do
    {
        temp1=ReadVariable();
        if(temp1>I2C_ADDR_MASK_BITS)
            putstr("Enter the correct value\n\r");
    }while(temp1>I2C_ADDR_MASK_BITS);

    eebytew(temp, temp1);
}

/* HELP MENU*/
void help_menu()
{
    putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    putstr("EEPROM Control\n\r");
    putstr("Press 'R' to read a byte from the user input address\n\r");
    putstr("Press 'W' to write a byte data to user input address\n\r");
    putstr("Press 'S' for sequential read between the adresses\n\r");
    putstr("Press 'T' for EEPROM software reset\n\r");
    putstr("LCD Control\n\r");
    putstr("Press 'C' for CGRAM HEX dump\n\r");
    putstr("Press 'D' for DDRAM HEX dump\n\r");
    putstr("Press 'E' for Clearing the LCD\n\r");
    putstr("Press 'P' to display the custom character\n\r");
    putstr("Press 'N' to add custom character\n\r");
    putstr("Press 'I' for printing letters on LCD\n\r");
    putstr("Press 'K' for displaying the logo\n\r");
    putstr("Press 'X' for moving the cursor to user input x,y position\n\r");
    putstr("Press 'G' for moving the cursor to user input address\n\r");
    putstr("Clock Control\n\r");
    putstr("Press '0' for pausing the clock\n\r");
    putstr("Press '1' to resume the clock \n\r");
    putstr("Press '2' to reset the clock to zero\n\r");
    putstr("IO Expander control\n\r");
    putstr("Press 'B' for writing data to IO Expander\n\r");
    putstr("Press 'F' to read data from IO Expander\n\r");
    putstr("Press 'H' for configuring the individual GPIO pin\n\r");
    putstr("Press 'J' for configuring IO expander's one pin as input and one as output\n\r");
    putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
}

void read_function()
{
    uint16_t temp1;
    putstr("Enter the address in hex\n\r");
    do
    {
        temp1=ReadVariable();
        if(temp1>I2C_ADDR_MASK_BITS)
            putstr("Enter the correct value\n\r");
    }while(temp1>I2C_ADDR_MASK_BITS);
    eebyter(temp1);
}
/*CREATING A LOGO */
void custom_character()
{
    unsigned char in_array[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c};
    unsigned char in_array5[] = {0x0f, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0f};
    unsigned char in_array1[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x1f, 0x1f, 0x1f};
    unsigned char in_array2[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x03};
    unsigned char in_array3[] = {0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f};
    unsigned char in_array4[] = {0x1f, 0x00, 0x04, 0x0A, 0x0A, 0x04, 0x00, 0x1f};
    unsigned char in_array6[] = {0x1f, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x1f};
    unsigned char in_array7[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x018, 0x18};
    unsigned char kk;
    EA=0;
    kk=*INST_READ;
    EA=1;
    kk &= CURSORADDR_MASK_BITS;
    lcdclear();
    putstr("Look at LCD\n\r");
    lcdcreatechar(CGRAM_FIRSTCHAR_ADDR, in_array);
    lcdcreatechar(CGRAM_SECONDCHAR_ADDR, in_array1);
    lcdcreatechar(CGRAM_THIRDCHAR_ADDR, in_array2);
    lcdcreatechar(CGRAM_FOURTHCHAR_ADDR, in_array3);
    lcdcreatechar(CGRAM_FIFTHCHAR_ADDR, in_array4);
    lcdcreatechar(CGRAM_SIXTHCHAR_ADDR, in_array5);
    lcdcreatechar(CGRAM_SEVENTHCHAR_ADDR, in_array6);
    lcdcreatechar(CGRAM_EIGHTHCHAR_ADDR, in_array7);
    lcdgotoxy(3,2);
    lcdputch(0x01);
    lcdgotoxy(2,2);
    lcdputch(0x00);
    lcdgotoxy(1,2);
    lcdputch(0x00);
    lcdgotoxy(0,2);
    lcdputch(0x05);
    lcdgotoxy(3,1);
    lcdputch(0x02);
    lcdgotoxy(3,3);
    lcdputch(0x07);
    lcdgotoxy(0,3);
    lcdputch(0x03);
    lcdgotoxy(0,4);
    lcdputch(0x04);
    lcdgotoxy(0,5);
    lcdputch(0x06);
    *INST_WRITE=(kk|DDRAM_WRITE_MASK_BITS);
    lcdbusywait();

}

void Timer0Initialize()
{
    TMOD |= 0x09;
    TH0 = 0x4C;
    TL0 = 0x20;
    IE = 0x82;
    TR0=1;
}

void lcdtime()
{
    lcdclockgotoaddr(0x59);
    lcdputstr("00:00.0");
}

unsigned char read_ioexpander()
{
    unsigned char io_tem;
    io_tem=(IOEXP_CB_BITS | I2C_READ_MASK_BITS);
    startbit();
    send_data(io_tem);
    clock_func();
    stopbit();
    io_tem = ACC;
    return io_tem;
}

unsigned char invert_ioexpander(unsigned char value_io)
{
    value_io &= IOEXP_INIT_BITS;
    value_io<<=1;
    value_io ^= 0xFF;
    value_io &= 0x02;
    value_io |= 0x01;
    return value_io;
}

void write_ioexpander(unsigned char send_bits)
{
    unsigned char io_temp;
    io_temp=(IOEXP_CB_BITS&I2C_WRITE_MASK_BITS);
    startbit();
    send_data(io_temp);
    send_data(send_bits);
    stopbit();
}

void external_isr(void) __interrupt(2) __using(2)
{
    unsigned char io_data, io_inver;
    EX1=0;
    io_data=read_ioexpander();
    io_inver=invert_ioexpander(io_data);
    write_ioexpander(io_inver);
    EX1=1;
}

void timer_isr(void) __critical __interrupt(1)__using(1)
{
    unsigned char interr;
    unsigned volatile int bb;
    TF0=0;
    TH0 = 0x4C;
    TL0 = 0x20;
    TR0 = 1;
    if(CLOCK_ENABLE_FLAG)
    {
        if(count_flag==0x00)
        {
            P1_6 = 0;
            P1_6=1;
            interr=*INST_READ;
            interr= interr&CURSORADDR_MASK_BITS;
            count_flag=0x02;
            MS++;
            lcdclockgotoaddr(0x5F);
            if(MS>9)
            {
                MS=0;
                S++;
            }
            lcdputch(MS+CLOCK_CGRAM_BASE_ADDR);
            if(S>9)
            {
                S=0;
                SS++;
            }
            lcdclockgotoaddr(0x5D);
            lcdputch(S+CLOCK_CGRAM_BASE_ADDR);
            if(SS>5)
            {
                SS=0;
                M++;
            }
            lcdclockgotoaddr(0x5C);
            lcdputch(SS+CLOCK_CGRAM_BASE_ADDR);
            if(M>9)
            {
                M=0;
                MM++;
            }
            lcdclockgotoaddr(0x5A);
            lcdputch(M+CLOCK_CGRAM_BASE_ADDR);
            lcdclockgotoaddr(0x59);
            lcdputch(MM+CLOCK_CGRAM_BASE_ADDR);
            lcdclockgotoaddr(0x5E);
            lcdputch('.');
            lcdclockgotoaddr(0x5B);
            lcdputch(':');
            *INST_WRITE=(interr | DDRAM_WRITE_MASK_BITS);
            bb=*INST_READ;
            MSDelay(1);
            while((bb&BUSYFLAG_MASK_BITS)==BUSYFLAG_MASK_BITS)
            {
                bb= *INST_READ;
                MSDelay(1);
            }
        }
        count_flag--;
    }
}

void clock_reset()
{
    EA=0;
    MS=0;
    S=0; SS=0;
    M=0; MM=0;
    lcdtime();
    EA=1;
}

// Name: eereset()
// Description: Performs a software reset of the I2C EEPROM using an
// algorithm that conforms to Microchip application note AN709.
void eereset()
{
    ACC=0xFF;
    startbit();
    send_data_assembly();
    ninthbit();
    startbit();
    stopbit();
}

void IOexpInit()
{
    IT1=1;
    IE |=0x84;
    write_ioexpander(0x01);
}

void ioexpander_writedata()
{
    uint16_t ioexp_write;
    putstr("Enter the value to write\n\r");
    do
    {
        ioexp_write=ReadVariable();
        if(ioexp_write>0xFF)
            putstr("Enter proper value\n\r");
    }while(ioexp_write>0xFF);
    write_ioexpander(0x00);
    write_ioexpander(ioexp_write);
}

void ioexpander_readdata()
{
    unsigned char ioexp_read;
    write_ioexpander(0xFF);
    ioexp_read = read_ioexpander();
    putstr("Read input value from the IO Expander is ");
    printf("0x%2x\n\r", ioexp_read);
}

void ioexpander_config()
{
    uint16_t io_con;
    putstr("Press 1 for input and 0 for output. Enter eight values for eight port pins and hit enter\n\r");
    do
    {
        io_con=readinput();
        if(io_con>0xFF)
            putstr("Enter proper value\n\r");
    }while(io_con>0xFF);
    write_ioexpander(io_con);
}

void main(void)
{
    unsigned char in;
    MS=0; S=0; SS=0; M=0; MM=0;
    count_flag=0x02;
    Timer0Initialize();
    SerialInitialize();
    lcdinit();
    eereset();
    lcdtime();
    IOexpInit();
    lcdgotoxy(1,0);
    //lcdputstr("YOU KNOW NOTHING");
    lcdgotoxy(2,4);
    //lcdputstr("JON SNOW");
    EA=0;
    *INST_WRITE=(DDRAM_WRITE_MASK_BITS);
    EA=1;
    lcdbusywait();
    putstr("Welcome\n\r");
    putstr("Press '>' for help menu\n\r");
    putstr("Enter the character\n\r");
    while(1)
    {
        putstr("\n\rEnter the character\n\r");
        in=getchar();
        switch(in)
        {
            case '0' : putstr("The clock is paused\n\r");
                    CLOCK_ENABLE_FLAG=0;
                        break;
            case '1' : putstr("The clock is resuming\n\r");
                        CLOCK_ENABLE_FLAG=1;
                        break;
            case '2' : putstr("Clock is reset to zero\n\r");
                        clock_reset();
                        break;
            case 'W' : write_function();
                        break;
            case 'R' : read_function();
                        break;
            case 'S' : readall_function();
                        break;
            case '>' : help_menu();
                        break;
            case 'X' : gotoxy();
                        break;
            case 'G' : gotoaddress();
                        break;
            case 'C' : cgramdump();
                        break;
            case 'D' : ddramdump();
                        break;
            case 'E' : lcdclear();
                        break;
            case 'N' : addcharacter();
                        break;
            case 'P' : display();
                        break;
            case 'I' : interactive();
                        break;
            case 'K' :  u=0;
                        custom_character();
                        break;
            case 'T' : eereset();
                        break;
            case 'B' : ioexpander_writedata();
                        break;
            case 'F' : ioexpander_readdata();
                        break;
            case 'H' : ioexpander_config();
                        break;
            case 'J' : IOexpInit();
                        break;
            default: putstr("Enter the correct option\n\r");
        }
    }
}
