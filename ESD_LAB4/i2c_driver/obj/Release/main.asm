;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.6.0 #4309 (Jul 28 2006)
; This file generated Fri Nov 17 00:59:16 2017
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _ioexpander_config
	.globl _ioexpander_readdata
	.globl _ioexpander_writedata
	.globl _IOexpInit
	.globl _eereset
	.globl _clock_reset
	.globl _timer_isr
	.globl _external_isr
	.globl _write_ioexpander
	.globl _invert_ioexpander
	.globl _read_ioexpander
	.globl _lcdtime
	.globl _Timer0Initialize
	.globl _custom_character
	.globl _read_function
	.globl _help_menu
	.globl _write_function
	.globl _readall_function
	.globl _eebyter
	.globl _eebytew
	.globl _clock_func
	.globl _ninthbit
	.globl _send_data
	.globl _send_data_assembly
	.globl _Master_ack
	.globl _ackbit
	.globl _stopbit
	.globl _startbit
	.globl _SerialInitialize
	.globl _delay
	.globl __sdcc_external_startup
	.globl _gotoaddress
	.globl _gotoxy
	.globl _interactive
	.globl _ReadVariable
	.globl _hex2int
	.globl _display
	.globl _addcharacter
	.globl _readinput
	.globl _ddramdump
	.globl _lcdcreatechar
	.globl _cgramdump
	.globl _lcdinit
	.globl _lcdputstr
	.globl _lcdputch
	.globl _lcdclear
	.globl _lcdgotoxy
	.globl _lcdgotoaddr
	.globl _lcdclockgotoaddr
	.globl _input_value
	.globl _lcdbusywait
	.globl _putstr
	.globl _MSDelay
	.globl _P5_7
	.globl _P5_6
	.globl _P5_5
	.globl _P5_4
	.globl _P5_3
	.globl _P5_2
	.globl _P5_1
	.globl _P5_0
	.globl _P4_7
	.globl _P4_6
	.globl _P4_5
	.globl _P4_4
	.globl _P4_3
	.globl _P4_2
	.globl _P4_1
	.globl _P4_0
	.globl _PX0L
	.globl _PT0L
	.globl _PX1L
	.globl _PT1L
	.globl _PLS
	.globl _PT2L
	.globl _PPCL
	.globl _EC
	.globl _CCF0
	.globl _CCF1
	.globl _CCF2
	.globl _CCF3
	.globl _CCF4
	.globl _CR
	.globl _CF
	.globl _TF2
	.globl _EXF2
	.globl _RCLK
	.globl _TCLK
	.globl _EXEN2
	.globl _TR2
	.globl _C_T2
	.globl _CP_RL2
	.globl _T2CON_7
	.globl _T2CON_6
	.globl _T2CON_5
	.globl _T2CON_4
	.globl _T2CON_3
	.globl _T2CON_2
	.globl _T2CON_1
	.globl _T2CON_0
	.globl _PT2
	.globl _ET2
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _PS
	.globl _PT1
	.globl _PX1
	.globl _PT0
	.globl _PX0
	.globl _RD
	.globl _WR
	.globl _T1
	.globl _T0
	.globl _INT1
	.globl _INT0
	.globl _TXD
	.globl _RXD
	.globl _P3_7
	.globl _P3_6
	.globl _P3_5
	.globl _P3_4
	.globl _P3_3
	.globl _P3_2
	.globl _P3_1
	.globl _P3_0
	.globl _EA
	.globl _ES
	.globl _ET1
	.globl _EX1
	.globl _ET0
	.globl _EX0
	.globl _P2_7
	.globl _P2_6
	.globl _P2_5
	.globl _P2_4
	.globl _P2_3
	.globl _P2_2
	.globl _P2_1
	.globl _P2_0
	.globl _SM0
	.globl _SM1
	.globl _SM2
	.globl _REN
	.globl _TB8
	.globl _RB8
	.globl _TI
	.globl _RI
	.globl _P1_7
	.globl _P1_6
	.globl _P1_5
	.globl _P1_4
	.globl _P1_3
	.globl _P1_2
	.globl _P1_1
	.globl _P1_0
	.globl _TF1
	.globl _TR1
	.globl _TF0
	.globl _TR0
	.globl _IE1
	.globl _IT1
	.globl _IE0
	.globl _IT0
	.globl _P0_7
	.globl _P0_6
	.globl _P0_5
	.globl _P0_4
	.globl _P0_3
	.globl _P0_2
	.globl _P0_1
	.globl _P0_0
	.globl _EECON
	.globl _KBF
	.globl _KBE
	.globl _KBLS
	.globl _BRL
	.globl _BDRCON
	.globl _T2MOD
	.globl _SPDAT
	.globl _SPSTA
	.globl _SPCON
	.globl _SADEN
	.globl _SADDR
	.globl _WDTPRG
	.globl _WDTRST
	.globl _P5
	.globl _P4
	.globl _IPH1
	.globl _IPL1
	.globl _IPH0
	.globl _IPL0
	.globl _IEN1
	.globl _IEN0
	.globl _CMOD
	.globl _CL
	.globl _CH
	.globl _CCON
	.globl _CCAPM4
	.globl _CCAPM3
	.globl _CCAPM2
	.globl _CCAPM1
	.globl _CCAPM0
	.globl _CCAP4L
	.globl _CCAP3L
	.globl _CCAP2L
	.globl _CCAP1L
	.globl _CCAP0L
	.globl _CCAP4H
	.globl _CCAP3H
	.globl _CCAP2H
	.globl _CCAP1H
	.globl _CCAP0H
	.globl _CKCKON1
	.globl _CKCKON0
	.globl _CKRL
	.globl _AUXR1
	.globl _AUXR
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T2CON
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _IP
	.globl _P3
	.globl _IE
	.globl _P2
	.globl _SBUF
	.globl _SCON
	.globl _P1
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _P0
	.globl _confirm
	.globl _CLOCK_ENABLE_FLAG
	.globl _eebytew_PARM_2
	.globl _hex2int_PARM_2
	.globl _lcdcreatechar_PARM_2
	.globl _lcdgotoxy_PARM_2
	.globl _count_flag
	.globl _MM
	.globl _M
	.globl _SS
	.globl _S
	.globl _MS
	.globl _u
	.globl _array
	.globl _t
	.globl _putchar
	.globl _getchar
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (DATA)
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
_T2CON	=	0x00c8
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_TL2	=	0x00cc
_TH2	=	0x00cd
_AUXR	=	0x008e
_AUXR1	=	0x00a2
_CKRL	=	0x0097
_CKCKON0	=	0x008f
_CKCKON1	=	0x008f
_CCAP0H	=	0x00fa
_CCAP1H	=	0x00fb
_CCAP2H	=	0x00fc
_CCAP3H	=	0x00fd
_CCAP4H	=	0x00fe
_CCAP0L	=	0x00ea
_CCAP1L	=	0x00eb
_CCAP2L	=	0x00ec
_CCAP3L	=	0x00ed
_CCAP4L	=	0x00ee
_CCAPM0	=	0x00da
_CCAPM1	=	0x00db
_CCAPM2	=	0x00dc
_CCAPM3	=	0x00dd
_CCAPM4	=	0x00de
_CCON	=	0x00d8
_CH	=	0x00f9
_CL	=	0x00e9
_CMOD	=	0x00d9
_IEN0	=	0x00a8
_IEN1	=	0x00b1
_IPL0	=	0x00b8
_IPH0	=	0x00b7
_IPL1	=	0x00b2
_IPH1	=	0x00b3
_P4	=	0x00c0
_P5	=	0x00d8
_WDTRST	=	0x00a6
_WDTPRG	=	0x00a7
_SADDR	=	0x00a9
_SADEN	=	0x00b9
_SPCON	=	0x00c3
_SPSTA	=	0x00c4
_SPDAT	=	0x00c5
_T2MOD	=	0x00c9
_BDRCON	=	0x009b
_BRL	=	0x009a
_KBLS	=	0x009c
_KBE	=	0x009d
_KBF	=	0x009e
_EECON	=	0x00d2
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (DATA)
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
_ET2	=	0x00ad
_PT2	=	0x00bd
_T2CON_0	=	0x00c8
_T2CON_1	=	0x00c9
_T2CON_2	=	0x00ca
_T2CON_3	=	0x00cb
_T2CON_4	=	0x00cc
_T2CON_5	=	0x00cd
_T2CON_6	=	0x00ce
_T2CON_7	=	0x00cf
_CP_RL2	=	0x00c8
_C_T2	=	0x00c9
_TR2	=	0x00ca
_EXEN2	=	0x00cb
_TCLK	=	0x00cc
_RCLK	=	0x00cd
_EXF2	=	0x00ce
_TF2	=	0x00cf
_CF	=	0x00df
_CR	=	0x00de
_CCF4	=	0x00dc
_CCF3	=	0x00db
_CCF2	=	0x00da
_CCF1	=	0x00d9
_CCF0	=	0x00d8
_EC	=	0x00ae
_PPCL	=	0x00be
_PT2L	=	0x00bd
_PLS	=	0x00bc
_PT1L	=	0x00bb
_PX1L	=	0x00ba
_PT0L	=	0x00b9
_PX0L	=	0x00b8
_P4_0	=	0x00c0
_P4_1	=	0x00c1
_P4_2	=	0x00c2
_P4_3	=	0x00c3
_P4_4	=	0x00c4
_P4_5	=	0x00c5
_P4_6	=	0x00c6
_P4_7	=	0x00c7
_P5_0	=	0x00d8
_P5_1	=	0x00d9
_P5_2	=	0x00da
_P5_3	=	0x00db
_P5_4	=	0x00dc
_P5_5	=	0x00dd
_P5_6	=	0x00de
_P5_7	=	0x00df
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
	.area REG_BANK_1	(REL,OVR,DATA)
	.ds 8
	.area REG_BANK_2	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_lcdcreatechar_sloc0_1_0:
	.ds 2
_hex2int_sloc0_1_0:
	.ds 1
_hex2int_sloc1_1_0:
	.ds 2
_hex2int_sloc2_1_0:
	.ds 4
_hex2int_sloc3_1_0:
	.ds 4
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_t::
	.ds 2
_array::
	.ds 8
_u::
	.ds 1
_MS::
	.ds 1
_S::
	.ds 1
_SS::
	.ds 1
_M::
	.ds 1
_MM::
	.ds 1
_count_flag::
	.ds 2
_MSDelay_value_1_1:
	.ds 2
_putstr_ptr_1_1:
	.ds 3
_lcdbusywait_i_1_1:
	.ds 2
_input_value_l_1_1:
	.ds 1
_input_value_m_1_1:
	.ds 1
_lcdclockgotoaddr_addr_1_1:
	.ds 1
_lcdgotoaddr_addr_1_1:
	.ds 1
_lcdgotoxy_PARM_2:
	.ds 1
_lcdgotoxy_row_1_1:
	.ds 1
_lcdputch_cc_1_1:
	.ds 1
_lcdputch_aa_1_1:
	.ds 2
_lcdputch_tempput_1_1:
	.ds 2
_lcdputstr_ptr_1_1:
	.ds 3
_cgramdump_temp_val_1_1:
	.ds 1
_lcdcreatechar_PARM_2:
	.ds 3
_lcdcreatechar_ccode_1_1:
	.ds 1
_ddramdump_temp_val_1_1:
	.ds 1
_readinput_value_1_1:
	.ds 1
_addcharacter_temp_1_1:
	.ds 1
_hex2int_PARM_2:
	.ds 2
_hex2int_a_1_1:
	.ds 3
_hex2int_val_1_1:
	.ds 4
_ReadVariable_k_1_1:
	.ds 2
_ReadVariable_ptr_1_1:
	.ds 1
_gotoaddress_temp_1_1:
	.ds 1
_putchar_x_1_1:
	.ds 1
_send_data_value_1_1:
	.ds 1
_eebytew_PARM_2:
	.ds 1
_eebytew_addr_1_1:
	.ds 2
_eebyter_addr_1_1:
	.ds 2
_eebyter_backup_1_1:
	.ds 1
_readall_function_backup_1_1:
	.ds 1
_readall_function_tempvalue_1_1:
	.ds 2
_custom_character_in_array_1_1:
	.ds 8
_custom_character_in_array5_1_1:
	.ds 8
_custom_character_in_array1_1_1:
	.ds 8
_custom_character_in_array2_1_1:
	.ds 8
_custom_character_in_array3_1_1:
	.ds 8
_custom_character_in_array4_1_1:
	.ds 8
_custom_character_in_array6_1_1:
	.ds 8
_custom_character_in_array7_1_1:
	.ds 8
_read_ioexpander_io_tem_1_1:
	.ds 1
_invert_ioexpander_value_io_1_1:
	.ds 1
_write_ioexpander_send_bits_1_1:
	.ds 1
_timer_isr_bb_1_1:
	.ds 2
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
_CLOCK_ENABLE_FLAG::
	.ds 1
_confirm::
	.ds 2
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	ljmp	_timer_isr
	.ds	5
	ljmp	_external_isr
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'MSDelay'
;------------------------------------------------------------
;value                     Allocated with name '_MSDelay_value_1_1'
;x                         Allocated with name '_MSDelay_x_1_1'
;zz                        Allocated with name '_MSDelay_zz_1_1'
;------------------------------------------------------------
;	main.c:90: void MSDelay(unsigned int value)
;	-----------------------------------------
;	 function MSDelay
;	-----------------------------------------
_MSDelay:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_MSDelay_value_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:93: for(zz=0;zz<value;zz++)
;	genAssign
	mov	dptr,#_MSDelay_value_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	r4,#0x00
	mov	r5,#0x00
00104$:
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,r2
	mov	a,r5
	subb	a,r3
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00108$
;	Peephole 300	removed redundant label 00116$
;	main.c:95: for(x=0;x<126;x++)
;	genAssign
	mov	r6,#0x7E
	mov	r7,#0x00
00103$:
;	genMinus
;	genMinusDec
	dec	r6
	cjne	r6,#0xff,00117$
	dec	r7
00117$:
;	genIfx
	mov	a,r6
	orl	a,r7
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00118$
;	main.c:93: for(zz=0;zz<value;zz++)
;	genPlus
;     genPlusIncr
;	tail increment optimized (range 7)
	inc	r4
	cjne	r4,#0x00,00104$
	inc	r5
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00104$
00108$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putstr'
;------------------------------------------------------------
;ptr                       Allocated with name '_putstr_ptr_1_1'
;------------------------------------------------------------
;	main.c:104: void putstr(char *ptr)
;	-----------------------------------------
;	 function putstr
;	-----------------------------------------
_putstr:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_putstr_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:106: while(*ptr)
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
	mov	r5,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:108: putchar(*ptr);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_putchar
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:109: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00110$
	inc	r3
00110$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00108$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdbusywait'
;------------------------------------------------------------
;i                         Allocated with name '_lcdbusywait_i_1_1'
;------------------------------------------------------------
;	main.c:115: void lcdbusywait()
;	-----------------------------------------
;	 function lcdbusywait
;	-----------------------------------------
_lcdbusywait:
;	main.c:118: EA=0;
;	genAssign
	clr	_EA
;	main.c:119: i=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_lcdbusywait_i_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:120: EA=1;
;	genAssign
	setb	_EA
;	main.c:121: MSDelay(1);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
	lcall	_MSDelay
;	main.c:122: while((i&BUSYFLAG_MASK_BITS)==BUSYFLAG_MASK_BITS)
00101$:
;	genAssign
	mov	dptr,#_lcdbusywait_i_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAnd
	anl	ar2,#0x80
	mov	r3,#0x00
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x80,00104$
	cjne	r3,#0x00,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:124: EA=0;
;	genAssign
	clr	_EA
;	main.c:125: i = *INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_lcdbusywait_i_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:126: EA=1;
;	genAssign
	setb	_EA
;	main.c:127: MSDelay(1);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
	lcall	_MSDelay
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'input_value'
;------------------------------------------------------------
;l                         Allocated with name '_input_value_l_1_1'
;m                         Allocated with name '_input_value_m_1_1'
;------------------------------------------------------------
;	main.c:132: unsigned char input_value()
;	-----------------------------------------
;	 function input_value
;	-----------------------------------------
_input_value:
;	main.c:134: unsigned volatile char l=0, m=0;
;	genAssign
	mov	dptr,#_input_value_l_1_1
;	Peephole 181	changed mov to clr
;	genAssign
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#_input_value_m_1_1
	movx	@dptr,a
;	main.c:135: do
00110$:
;	main.c:137: l=getchar();
;	genCall
	lcall	_getchar
	mov	a,dpl
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	@dptr,a
;	main.c:138: if(l>='0' && l<='9')
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x30,00121$
00121$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00122$
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x39
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00123$
;	main.c:140: m=((m*10)+(l-'0'));
;	genAssign
	mov	dptr,#_input_value_m_1_1
	movx	a,@dptr
;	genMult
;	genMultOneByte
	mov	r2,a
;	Peephole 105	removed redundant mov
	mov	b,#0x0A
	mul	ab
	mov	r2,a
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
;	genMinus
	mov	r3,a
;	Peephole 105	removed redundant mov
	add	a,#0xd0
;	genPlus
	mov	dptr,#_input_value_m_1_1
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	main.c:141: putchar(l);                 /* CONVERTING IT TO DECIMAL VALUE*/
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
	lcall	_putchar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00107$:
;	main.c:144: else if(l==READ_BACKSPACE)
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x7F,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00124$
;	Peephole 300	removed redundant label 00125$
;	main.c:146: m=m/10;
;	genAssign
	mov	dptr,#_input_value_m_1_1
	movx	a,@dptr
	mov	r2,a
;	genDiv
	mov	dptr,#_input_value_m_1_1
;     genDivOneByte
	mov	b,#0x0A
	mov	a,r2
	div	ab
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00104$:
;	main.c:148: else if(l!=READ_ENTER)
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x0D,00126$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00126$:
;	main.c:149: {putstr("\n\rEnter Numbers\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_0
	mov	b,#0x80
	lcall	_putstr
00111$:
;	main.c:150: }while(l!=READ_ENTER);
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x0D,00127$
	sjmp	00128$
00127$:
	ljmp	00110$
00128$:
;	main.c:151: return m;                                   /* UNTIL ENTER IS PRESSES*/
;	genAssign
	mov	dptr,#_input_value_m_1_1
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdclockgotoaddr'
;------------------------------------------------------------
;addr                      Allocated with name '_lcdclockgotoaddr_addr_1_1'
;temp                      Allocated with name '_lcdclockgotoaddr_temp_2_2'
;------------------------------------------------------------
;	main.c:154: void lcdclockgotoaddr(unsigned char addr)
;	-----------------------------------------
;	 function lcdclockgotoaddr
;	-----------------------------------------
_lcdclockgotoaddr:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdclockgotoaddr_addr_1_1
	movx	@dptr,a
;	main.c:156: if(addr>=0x59 && addr<=0x5F)
;	genAssign
	mov	dptr,#_lcdclockgotoaddr_addr_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x59,00108$
00108$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00104$
;	Peephole 300	removed redundant label 00109$
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r2
	add	a,#0xff - 0x5F
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00104$
;	Peephole 300	removed redundant label 00110$
;	main.c:160: temp |= addr;
;	genOr
	orl	ar2,#0x80
;	main.c:161: *INST_WRITE = temp;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:162: lcdbusywait();
;	genCall
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	_lcdbusywait
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdgotoaddr'
;------------------------------------------------------------
;addr                      Allocated with name '_lcdgotoaddr_addr_1_1'
;temp                      Allocated with name '_lcdgotoaddr_temp_2_2'
;------------------------------------------------------------
;	main.c:167: void lcdgotoaddr(unsigned char addr)
;	-----------------------------------------
;	 function lcdgotoaddr
;	-----------------------------------------
_lcdgotoaddr:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdgotoaddr_addr_1_1
	movx	@dptr,a
;	main.c:169: if(addr>=0x00 && addr<=0x58)
;	genAssign
	mov	dptr,#_lcdgotoaddr_addr_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x58
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00102$
;	Peephole 300	removed redundant label 00108$
;	main.c:173: temp |= addr;
;	genOr
	orl	ar2,#0x80
;	main.c:174: EA=0;
;	genAssign
	clr	_EA
;	main.c:175: *INST_WRITE = temp;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:176: EA=1;
;	genAssign
	setb	_EA
;	main.c:177: lcdbusywait();
;	genCall
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
00102$:
;	main.c:180: putstr("\n\rCan't move to given position\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdgotoxy'
;------------------------------------------------------------
;column                    Allocated with name '_lcdgotoxy_PARM_2'
;row                       Allocated with name '_lcdgotoxy_row_1_1'
;------------------------------------------------------------
;	main.c:184: void lcdgotoxy(unsigned char row, unsigned char column)
;	-----------------------------------------
;	 function lcdgotoxy
;	-----------------------------------------
_lcdgotoxy:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdgotoxy_row_1_1
	movx	@dptr,a
;	main.c:186: switch(row)
;	genAssign
	mov	dptr,#_lcdgotoxy_row_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x03
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00106$
;	Peephole 300	removed redundant label 00109$
;	genJumpTab
	mov	a,r2
;	Peephole 254	optimized left shift
	add	a,r2
	add	a,r2
	mov	dptr,#00110$
	jmp	@a+dptr
00110$:
	ljmp	00101$
	ljmp	00102$
	ljmp	00103$
	ljmp	00104$
;	main.c:188: case 0: lcdgotoaddr((BASE_ADDRROW0+column));
00101$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:189: break;
;	main.c:190: case 1: lcdgotoaddr((BASE_ADDRROW1+column));
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoaddr
00102$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genPlus
;     genPlusIncr
	mov	a,#0x40
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:191: break;
;	main.c:192: case 2: lcdgotoaddr((BASE_ADDRROW2+column));
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoaddr
00103$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genPlus
;     genPlusIncr
	mov	a,#0x10
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:193: break;
;	main.c:194: case 3: lcdgotoaddr((BASE_ADDRROW3+column));
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoaddr
00104$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genPlus
;     genPlusIncr
	mov	a,#0x50
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:196: }
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	_lcdgotoaddr
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdclear'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:200: void lcdclear()
;	-----------------------------------------
;	 function lcdclear
;	-----------------------------------------
_lcdclear:
;	main.c:202: EA=0;
;	genAssign
	clr	_EA
;	main.c:203: *INST_WRITE = CLEARSCREEN_CURSOR_HOME;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x01
	movx	@dptr,a
;	main.c:204: EA=1;
;	genAssign
	setb	_EA
;	main.c:205: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:206: putstr("Clearing the LCD completed\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdputch'
;------------------------------------------------------------
;cc                        Allocated with name '_lcdputch_cc_1_1'
;aa                        Allocated with name '_lcdputch_aa_1_1'
;tempput                   Allocated with name '_lcdputch_tempput_1_1'
;------------------------------------------------------------
;	main.c:210: void lcdputch(unsigned char cc)
;	-----------------------------------------
;	 function lcdputch
;	-----------------------------------------
_lcdputch:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdputch_cc_1_1
	movx	@dptr,a
;	main.c:213: EA=0;
;	genAssign
	clr	_EA
;	main.c:214: *DATA_WRITE = cc;
;	genAssign
	mov	r2,#0x00
	mov	r3,#0xF4
;	genAssign
	mov	dptr,#_lcdputch_cc_1_1
	movx	a,@dptr
;	genPointerSet
;     genFarPointerSet
	mov	r4,a
	mov	dpl,r2
	mov	dph,r3
;	Peephole 136	removed redundant move
	movx	@dptr,a
;	main.c:215: EA=1;
;	genAssign
	setb	_EA
;	main.c:216: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:217: EA=0;
;	genAssign
	clr	_EA
;	main.c:218: aa=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_lcdputch_aa_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:219: EA=1;
;	genAssign
	setb	_EA
;	main.c:220: tempput = aa&CURSORADDR_MASK_BITS;              /* THIS LOOP HANDLES THE ROW ADDRESS MISMATCH OF THE LCD*/
;	genAssign
	mov	dptr,#_lcdputch_aa_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAnd
	mov	dptr,#_lcdputch_tempput_1_1
	mov	a,#0x7F
	anl	a,r2
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:221: if(tempput==0x10)
;	genAssign
	mov	dptr,#_lcdputch_tempput_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x10,00110$
	cjne	r3,#0x00,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00118$
;	Peephole 300	removed redundant label 00119$
;	main.c:223: lcdgotoxy(1,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x01
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
00110$:
;	main.c:225: else if(tempput==0x50)
;	genAssign
	mov	dptr,#_lcdputch_tempput_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x50,00107$
	cjne	r3,#0x00,00107$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00120$
;	Peephole 300	removed redundant label 00121$
;	main.c:227: lcdgotoxy(2,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x02
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
00107$:
;	main.c:229: else if(tempput==0x20)
;	genAssign
	mov	dptr,#_lcdputch_tempput_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x20,00104$
	cjne	r3,#0x00,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00122$
;	Peephole 300	removed redundant label 00123$
;	main.c:231: lcdgotoxy(3,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
00104$:
;	main.c:233: else if(tempput==0x59)
;	genAssign
	mov	dptr,#_lcdputch_tempput_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x59,00112$
	cjne	r3,#0x00,00112$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00124$
;	Peephole 300	removed redundant label 00125$
;	main.c:235: lcdgotoxy(0,0);//lcdgotoaddr(temp);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	_lcdgotoxy
00112$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdputstr'
;------------------------------------------------------------
;ptr                       Allocated with name '_lcdputstr_ptr_1_1'
;------------------------------------------------------------
;	main.c:245: void lcdputstr(unsigned char *ptr)
;	-----------------------------------------
;	 function lcdputstr
;	-----------------------------------------
_lcdputstr:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_lcdputstr_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:247: while(*ptr)
;	genAssign
	mov	dptr,#_lcdputstr_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
	mov	r5,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:249: lcdputch(*ptr);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_lcdputch
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:250: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00110$
	inc	r3
00110$:
;	genAssign
	mov	dptr,#_lcdputstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00108$:
;	genAssign
	mov	dptr,#_lcdputstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdinit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:259: void lcdinit()
;	-----------------------------------------
;	 function lcdinit
;	-----------------------------------------
_lcdinit:
;	main.c:261: MSDelay(20);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0014
	lcall	_MSDelay
;	main.c:262: *INST_WRITE = UNLOCK_COMMAND;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x30
	movx	@dptr,a
;	main.c:263: MSDelay(8);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0008
	lcall	_MSDelay
;	main.c:264: *INST_WRITE = UNLOCK_COMMAND;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x30
	movx	@dptr,a
;	main.c:265: MSDelay(3);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0003
	lcall	_MSDelay
;	main.c:266: *INST_WRITE = UNLOCK_COMMAND;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x30
	movx	@dptr,a
;	main.c:267: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:268: *INST_WRITE = FUNCTION_SET_LINES_FONT;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x38
	movx	@dptr,a
;	main.c:269: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:270: *INST_WRITE = DISPLAY_OFF;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x08
	movx	@dptr,a
;	main.c:271: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:272: *INST_WRITE = DISPLAY_ON;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x0C
	movx	@dptr,a
;	main.c:273: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:274: *INST_WRITE = ENTRY_MODE_SET;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x06
	movx	@dptr,a
;	main.c:275: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:276: *INST_WRITE = CLEARSCREEN_CURSOR_HOME;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x01
	movx	@dptr,a
;	main.c:277: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'cgramdump'
;------------------------------------------------------------
;count_row                 Allocated with name '_cgramdump_count_row_1_1'
;count                     Allocated with name '_cgramdump_count_1_1'
;temp_val                  Allocated with name '_cgramdump_temp_val_1_1'
;k                         Allocated with name '_cgramdump_k_1_1'
;------------------------------------------------------------
;	main.c:281: void cgramdump()
;	-----------------------------------------
;	 function cgramdump
;	-----------------------------------------
_cgramdump:
;	main.c:285: unsigned char temp_val=0x40, k;
;	genAssign
	mov	dptr,#_cgramdump_temp_val_1_1
	mov	a,#0x40
	movx	@dptr,a
;	main.c:286: EA=0;
;	genAssign
	clr	_EA
;	main.c:287: k=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:289: EA=1;
;	genAssign
	setb	_EA
;	main.c:290: lcdbusywait();
;	genCall
	push	ar2
	lcall	_lcdbusywait
	pop	ar2
;	main.c:291: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:292: putstr("CGRAM values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_4
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:293: putstr("\n\rCCODE |  Values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_5
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:294: for (count_row=0; count_row<=7; count_row++)
;	genAssign
	mov	r3,#0x00
	mov	r4,#0x00
00105$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x07
	subb	a,r3
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r4
;	genIfxJump
	jnc	00117$
	ljmp	00108$
00117$:
;	main.c:296: printf("0x%04x:  ", (temp_val&CGRAM_ADDR_MASK_BITS));
;	genAssign
	mov	dptr,#_cgramdump_temp_val_1_1
	movx	a,@dptr
	mov	r5,a
;	genAnd
	mov	a,#0x3F
	anl	a,r5
	mov	r6,a
;	genCast
	mov	r7,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_6
	push	acc
	mov	a,#(__str_6 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:297: for(count=0; count<=7; count++)
;	genAssign
;	genAssign
	mov	r6,#0x00
	mov	r7,#0x00
00101$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x07
	subb	a,r6
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r7
;	genIfxJump
	jnc	00118$
	ljmp	00115$
00118$:
;	main.c:299: ET0=0;
;	genAssign
	clr	_ET0
;	main.c:300: *INST_WRITE = temp_val;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r5
	movx	@dptr,a
;	main.c:301: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:302: printf("%02x  ",*DATA_READ);
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xFC00
	movx	a,@dptr
	mov	r0,a
;	genCast
	mov	r1,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
;	genIpush
	mov	a,#__str_7
	push	acc
	mov	a,#(__str_7 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:303: ET0=1;
;	genAssign
	setb	_ET0
;	main.c:304: temp_val++;
;	genPlus
;     genPlusIncr
	inc	r5
;	main.c:305: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:297: for(count=0; count<=7; count++)
;	genPlus
;     genPlusIncr
	inc	r6
	cjne	r6,#0x00,00119$
	inc	r7
00119$:
	ljmp	00101$
00115$:
;	genAssign
	mov	dptr,#_cgramdump_temp_val_1_1
	mov	a,r5
	movx	@dptr,a
;	main.c:307: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:294: for (count_row=0; count_row<=7; count_row++)
;	genPlus
;     genPlusIncr
	inc	r3
	cjne	r3,#0x00,00120$
	inc	r4
00120$:
	ljmp	00105$
00108$:
;	main.c:309: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:310: EA=0;
;	genAssign
	clr	_EA
;	main.c:311: *INST_WRITE=(k|DDRAM_WRITE_MASK_BITS);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:312: EA=1;
;	genAssign
	setb	_EA
;	main.c:313: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdcreatechar'
;------------------------------------------------------------
;sloc0                     Allocated with name '_lcdcreatechar_sloc0_1_0'
;row_vals                  Allocated with name '_lcdcreatechar_PARM_2'
;ccode                     Allocated with name '_lcdcreatechar_ccode_1_1'
;n                         Allocated with name '_lcdcreatechar_n_1_1'
;y                         Allocated with name '_lcdcreatechar_y_1_1'
;------------------------------------------------------------
;	main.c:318: void lcdcreatechar(unsigned char ccode, unsigned char row_vals[])
;	-----------------------------------------
;	 function lcdcreatechar
;	-----------------------------------------
_lcdcreatechar:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdcreatechar_ccode_1_1
	movx	@dptr,a
;	main.c:321: EA=0;
;	genAssign
	clr	_EA
;	main.c:322: y=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:323: EA=1;
;	genAssign
	setb	_EA
;	main.c:324: lcdbusywait();
;	genCall
	push	ar2
	lcall	_lcdbusywait
	pop	ar2
;	main.c:325: ccode |= 0x40;
;	genAssign
;	genOr
	mov	dptr,#_lcdcreatechar_ccode_1_1
	movx	a,@dptr
	mov	r3,a
;	Peephole 248.a	optimized or to xdata
	orl	a,#0x40
	movx	@dptr,a
;	main.c:326: for (n=0; n<=7; n++)
;	genAssign
	mov	dptr,#_lcdcreatechar_PARM_2
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genAssign
	mov	dptr,#_lcdcreatechar_ccode_1_1
	movx	a,@dptr
	mov	r6,a
;	genAssign
	mov	r7,#0x00
00101$:
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r7
	add	a,#0xff - 0x07
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00109$
;	Peephole 300	removed redundant label 00110$
;	main.c:328: ET0=0;
;	genIpush
	push	ar2
;	genAssign
	clr	_ET0
;	main.c:329: *INST_WRITE = ccode;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r6
	movx	@dptr,a
;	main.c:330: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:331: *DATA_WRITE = (row_vals[n]&CGRAM_PIXEL_MASK_BITS);
;	genAssign
	mov	_lcdcreatechar_sloc0_1_0,#0x00
	mov	(_lcdcreatechar_sloc0_1_0 + 1),#0xF4
;	genPlus
;	Peephole 236.g	used r7 instead of ar7
	mov	a,r7
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	mov	r2,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	mov	r0,a
	mov	ar1,r5
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r0
	mov	b,r1
	lcall	__gptrget
	mov	r2,a
;	genAnd
	anl	ar2,#0x1F
;	genPointerSet
;     genFarPointerSet
	mov	dpl,_lcdcreatechar_sloc0_1_0
	mov	dph,(_lcdcreatechar_sloc0_1_0 + 1)
	mov	a,r2
	movx	@dptr,a
;	main.c:332: ET0=1;
;	genAssign
	setb	_ET0
;	main.c:333: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:334: ccode++;
;	genPlus
;     genPlusIncr
	inc	r6
;	genAssign
	mov	dptr,#_lcdcreatechar_ccode_1_1
	mov	a,r6
	movx	@dptr,a
;	main.c:326: for (n=0; n<=7; n++)
;	genPlus
;     genPlusIncr
	inc	r7
;	genIpop
	pop	ar2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00109$:
;	genAssign
	mov	dptr,#_lcdcreatechar_ccode_1_1
	mov	a,r6
	movx	@dptr,a
;	main.c:336: EA=0;
;	genAssign
	clr	_EA
;	main.c:337: *INST_WRITE=(y|DDRAM_WRITE_MASK_BITS);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:338: EA=1;
;	genAssign
	setb	_EA
;	main.c:339: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'ddramdump'
;------------------------------------------------------------
;count_row                 Allocated with name '_ddramdump_count_row_1_1'
;count                     Allocated with name '_ddramdump_count_1_1'
;temp_val                  Allocated with name '_ddramdump_temp_val_1_1'
;k                         Allocated with name '_ddramdump_k_1_1'
;------------------------------------------------------------
;	main.c:343: void ddramdump()
;	-----------------------------------------
;	 function ddramdump
;	-----------------------------------------
_ddramdump:
;	main.c:347: unsigned char temp_val=0, k;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:348: EA=0;
;	genAssign
	clr	_EA
;	main.c:349: k=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:350: EA=1;
;	genAssign
	setb	_EA
;	main.c:351: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:352: putstr("DDRAM Values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_8
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:353: putstr("\n\rADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_9
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:354: for (count_row=0; count_row<=3; count_row++)
;	genAssign
	mov	r3,#0x00
	mov	r4,#0x00
00113$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x03
	subb	a,r3
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r4
;	genIfxJump
	jnc	00129$
	ljmp	00116$
00129$:
;	main.c:356: if(count_row==0)
;	genIfx
	mov	a,r3
	orl	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00102$
;	Peephole 300	removed redundant label 00130$
;	main.c:357: temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW0);
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0x80
	movx	@dptr,a
00102$:
;	main.c:358: if(count_row==1)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r3,#0x01,00104$
	cjne	r4,#0x00,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00131$
;	Peephole 300	removed redundant label 00132$
;	main.c:359: temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW1);
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0xC0
	movx	@dptr,a
00104$:
;	main.c:360: if(count_row==2)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r3,#0x02,00106$
	cjne	r4,#0x00,00106$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00133$
;	Peephole 300	removed redundant label 00134$
;	main.c:361: temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW2);
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0x90
	movx	@dptr,a
00106$:
;	main.c:362: if(count_row==3)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r3,#0x03,00108$
	cjne	r4,#0x00,00108$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00135$
;	Peephole 300	removed redundant label 00136$
;	main.c:363: temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW3);
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0xD0
	movx	@dptr,a
00108$:
;	main.c:364: printf("0x%02x:  ", (temp_val&DDRAM_ADDR_MASK_BITS));
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	movx	a,@dptr
	mov	r5,a
;	genAnd
	mov	a,#0x7F
	anl	a,r5
	mov	r6,a
;	genCast
	mov	r7,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_10
	push	acc
	mov	a,#(__str_10 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:365: for(count=0; count<=15; count++)
;	genAssign
;	genAssign
	mov	r6,#0x00
	mov	r7,#0x00
00109$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x0F
	subb	a,r6
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r7
;	genIfxJump
	jnc	00137$
	ljmp	00127$
00137$:
;	main.c:367: EA=0;
;	genAssign
	clr	_EA
;	main.c:368: *INST_WRITE = temp_val;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r5
	movx	@dptr,a
;	main.c:369: EA=1;
;	genAssign
	setb	_EA
;	main.c:370: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:371: temp_val++;
;	genPlus
;     genPlusIncr
	inc	r5
;	main.c:372: EA=0;
;	genAssign
	clr	_EA
;	main.c:373: printf("%02x  ",*DATA_READ);
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xFC00
	movx	a,@dptr
	mov	r0,a
;	genCast
	mov	r1,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
;	genIpush
	mov	a,#__str_7
	push	acc
	mov	a,#(__str_7 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:374: EA=1;
;	genAssign
	setb	_EA
;	main.c:375: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:365: for(count=0; count<=15; count++)
;	genPlus
;     genPlusIncr
	inc	r6
	cjne	r6,#0x00,00138$
	inc	r7
00138$:
	ljmp	00109$
00127$:
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,r5
	movx	@dptr,a
;	main.c:377: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:354: for (count_row=0; count_row<=3; count_row++)
;	genPlus
;     genPlusIncr
	inc	r3
	cjne	r3,#0x00,00139$
	inc	r4
00139$:
	ljmp	00113$
00116$:
;	main.c:379: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:380: EA=0;
;	genAssign
	clr	_EA
;	main.c:381: *INST_WRITE=(k|DDRAM_WRITE_MASK_BITS);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:382: EA=1;
;	genAssign
	setb	_EA
;	main.c:383: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'readinput'
;------------------------------------------------------------
;value                     Allocated with name '_readinput_value_1_1'
;i                         Allocated with name '_readinput_i_1_1'
;j                         Allocated with name '_readinput_j_1_1'
;------------------------------------------------------------
;	main.c:387: unsigned char readinput()
;	-----------------------------------------
;	 function readinput
;	-----------------------------------------
_readinput:
;	main.c:389: unsigned char value=0, i, j=0;
;	genAssign
	mov	dptr,#_readinput_value_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:390: do
;	genAssign
	mov	r2,#0x00
00110$:
;	main.c:392: i=getchar();
;	genCall
	push	ar2
	lcall	_getchar
	mov	r3,dpl
	pop	ar2
;	main.c:393: if(i>='0'&&i<='1')          /* works only if its a number */
;	genAssign
	mov	ar4,r3
;	genCmpLt
;	genCmp
	cjne	r4,#0x30,00122$
00122$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00123$
;	genAssign
	mov	ar4,r3
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r4
	add	a,#0xff - 0x31
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00124$
;	main.c:395: value=((value*2)+(i-'0')); /* Convert character to integers */
;	genAssign
	mov	dptr,#_readinput_value_1_1
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
;	Peephole 105	removed redundant mov
;	Peephole 204	removed redundant mov
	add	a,acc
	mov	r4,a
;	genMinus
	mov	a,r3
	add	a,#0xd0
;	genPlus
	mov	dptr,#_readinput_value_1_1
;	Peephole 236.a	used r4 instead of ar4
	add	a,r4
	movx	@dptr,a
;	main.c:396: putchar(i);
;	genCall
	mov	dpl,r3
	push	ar2
	push	ar3
	lcall	_putchar
	pop	ar3
	pop	ar2
;	main.c:397: j++;
;	genPlus
;     genPlusIncr
	inc	r2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00107$:
;	main.c:399: else if(i==READ_BACKSPACE)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r3,#0x7F,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00125$
;	Peephole 300	removed redundant label 00126$
;	main.c:401: value=value/2;
;	genAssign
	mov	dptr,#_readinput_value_1_1
	movx	a,@dptr
;	genRightShift
;	genRightShiftLiteral
;	genrshOne
	mov	r4,a
;	Peephole 105	removed redundant mov
	clr	c
	rrc	a
;	genAssign
	mov	r4,a
	mov	dptr,#_readinput_value_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00104$:
;	main.c:403: else if(i!=READ_ENTER)
;	genCmpEq
;	gencjneshort
	cjne	r3,#0x0D,00127$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00127$:
;	main.c:404: {putstr("\n\rEnter Valid Numbers\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
00111$:
;	main.c:405: }while(i!=READ_ENTER);
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r3,#0x0D,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00128$
;	Peephole 300	removed redundant label 00129$
;	main.c:407: return value;
;	genAssign
	mov	dptr,#_readinput_value_1_1
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'addcharacter'
;------------------------------------------------------------
;temp                      Allocated with name '_addcharacter_temp_1_1'
;y                         Allocated with name '_addcharacter_y_1_1'
;------------------------------------------------------------
;	main.c:411: void addcharacter()
;	-----------------------------------------
;	 function addcharacter
;	-----------------------------------------
_addcharacter:
;	main.c:414: if(u<8)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x08,00148$
00148$:
;	genIfxJump
	jc	00149$
	ljmp	00128$
00149$:
;	main.c:416: putstr("\n\rEnter eight pixel values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_12
	mov	b,#0x80
	lcall	_putstr
;	main.c:417: putstr("Enter five binary values for every pixel\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_13
	mov	b,#0x80
	lcall	_putstr
;	main.c:420: for(temp=0; temp<=7; temp++)
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
00130$:
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x07
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00133$
;	Peephole 300	removed redundant label 00150$
;	main.c:422: y=readinput();
;	genCall
	push	ar2
	lcall	_readinput
	mov	r3,dpl
	pop	ar2
;	main.c:423: if(y>0x1F)
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x1F
	jnc	00102$
;	Peephole 300	removed redundant label 00151$
;	main.c:425: putstr("Invalid Number. Enter Again\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_14
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:426: temp--;
;	genMinus
;	genMinusDec
	mov	a,r2
	dec	a
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00132$
00102$:
;	main.c:430: array[temp]=y;
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_array
	mov	dpl,a
;	Peephole 181	changed mov to clr
	clr	a
	addc	a,#(_array >> 8)
	mov	dph,a
;	genPointerSet
;     genFarPointerSet
	mov	a,r3
	movx	@dptr,a
;	main.c:431: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
00132$:
;	main.c:420: for(temp=0; temp<=7; temp++)
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
	movx	a,@dptr
	mov	r2,a
;	genPlus
	mov	dptr,#_addcharacter_temp_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00130$
00133$:
;	main.c:434: if(u==0)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
;	genIfx
	mov	r2,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00125$
;	Peephole 300	removed redundant label 00152$
;	main.c:435: lcdcreatechar(CGRAM_FIRSTCHAR_ADDR, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	lcall	_lcdcreatechar
	ljmp	00126$
00125$:
;	main.c:436: else if(u==1)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x01,00122$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00153$
;	Peephole 300	removed redundant label 00154$
;	main.c:437: lcdcreatechar(CGRAM_SECONDCHAR_ADDR, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x08
	lcall	_lcdcreatechar
	ljmp	00126$
00122$:
;	main.c:438: else if(u==2)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x02,00119$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00155$
;	Peephole 300	removed redundant label 00156$
;	main.c:439: lcdcreatechar(CGRAM_THIRDCHAR_ADDR, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x10
	lcall	_lcdcreatechar
	ljmp	00126$
00119$:
;	main.c:440: else if(u==3)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x03,00116$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00157$
;	Peephole 300	removed redundant label 00158$
;	main.c:441: lcdcreatechar(CGRAM_FOURTHCHAR_ADDR, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x18
	lcall	_lcdcreatechar
	ljmp	00126$
00116$:
;	main.c:442: else if(u==4)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x04,00113$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00159$
;	Peephole 300	removed redundant label 00160$
;	main.c:443: lcdcreatechar(CGRAM_FIFTHCHAR_ADDR, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x20
	lcall	_lcdcreatechar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00126$
00113$:
;	main.c:444: else if(u==5)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x05,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00161$
;	Peephole 300	removed redundant label 00162$
;	main.c:445: lcdcreatechar(CGRAM_SIXTHCHAR_ADDR, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x28
	lcall	_lcdcreatechar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00126$
00110$:
;	main.c:446: else if(u==6)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x06,00107$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00163$
;	Peephole 300	removed redundant label 00164$
;	main.c:447: lcdcreatechar(CGRAM_SEVENTHCHAR_ADDR, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x30
	lcall	_lcdcreatechar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00126$
00107$:
;	main.c:448: else if(u==7)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x07,00126$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00165$
;	Peephole 300	removed redundant label 00166$
;	main.c:449: lcdcreatechar(CGRAM_EIGHTHCHAR_ADDR, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x38
	lcall	_lcdcreatechar
00126$:
;	main.c:450: u++;
;	genPlus
	mov	dptr,#_u
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
	ret
00128$:
;	main.c:454: putstr("Eight custom character created. No more custom character please\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_15
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'display'
;------------------------------------------------------------
;hey                       Allocated with name '_display_hey_1_1'
;------------------------------------------------------------
;	main.c:459: void display()
;	-----------------------------------------
;	 function display
;	-----------------------------------------
_display:
;	main.c:462: putstr("Look at the LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_16
	mov	b,#0x80
	lcall	_putstr
;	main.c:463: while(hey<0x8)
;	genAssign
	mov	r2,#0x00
00101$:
;	genCmpLt
;	genCmp
	cjne	r2,#0x08,00109$
00109$:
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00104$
;	Peephole 300	removed redundant label 00110$
;	main.c:465: lcdputch(hey);
;	genCall
	mov	dpl,r2
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:466: hey++;
;	genPlus
;     genPlusIncr
	inc	r2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'hex2int'
;------------------------------------------------------------
;sloc0                     Allocated with name '_hex2int_sloc0_1_0'
;sloc1                     Allocated with name '_hex2int_sloc1_1_0'
;sloc2                     Allocated with name '_hex2int_sloc2_1_0'
;sloc3                     Allocated with name '_hex2int_sloc3_1_0'
;len                       Allocated with name '_hex2int_PARM_2'
;a                         Allocated with name '_hex2int_a_1_1'
;i                         Allocated with name '_hex2int_i_1_1'
;val                       Allocated with name '_hex2int_val_1_1'
;------------------------------------------------------------
;	main.c:474: unsigned long hex2int(char *a, unsigned int len)
;	-----------------------------------------
;	 function hex2int
;	-----------------------------------------
_hex2int:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_hex2int_a_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:477: unsigned long val = 0;
;	genAssign
	mov	dptr,#_hex2int_val_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:479: for(i=0;i<len;i++)
;	genAssign
	mov	dptr,#_hex2int_a_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genAssign
	mov	dptr,#_hex2int_PARM_2
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genAssign
	mov	r7,#0x00
	mov	r0,#0x00
00104$:
;	genIpush
	push	ar2
	push	ar3
	push	ar4
;	genAssign
	mov	ar1,r7
	mov	ar2,r0
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r1
	subb	a,r5
	mov	a,r2
	subb	a,r6
;	genIpop
;	genIfx
;	genIfxJump
;	Peephole 129.d	optimized condition
	pop	ar4
	pop	ar3
	pop	ar2
	jc	00114$
	ljmp	00107$
00114$:
;	main.c:480: if(a[i] <= 57)
;	genIpush
	push	ar5
	push	ar6
;	genPlus
;	Peephole 236.g	used r7 instead of ar7
	mov	a,r7
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	mov	r1,a
;	Peephole 236.g	used r0 instead of ar0
	mov	a,r0
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	mov	r5,a
	mov	ar6,r4
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r1
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
	mov	_hex2int_sloc0_1_0,a
;	genCmpGt
;	genCmp
	clr	c
;	Peephole 159	avoided xrl during execution
	mov	a,#(0x39 ^ 0x80)
	mov	b,_hex2int_sloc0_1_0
	xrl	b,#0x80
	subb	a,b
	clr	a
	rlc	a
;	genIpop
	pop	ar6
	pop	ar5
;	genIfx
;	genIfxJump
	jz	00115$
	ljmp	00102$
00115$:
;	main.c:481: val += (a[i]-48)*(1<<(4*(len-1-i)));
;	genIpush
	push	ar2
	push	ar3
	push	ar4
;	genCast
	mov	r1,_hex2int_sloc0_1_0
	mov	a,_hex2int_sloc0_1_0
	rlc	a
	subb	a,acc
	mov	r2,a
;	genMinus
	mov	a,r1
	add	a,#0xd0
	mov	r1,a
	mov	a,r2
	addc	a,#0xff
	mov	r2,a
;	genMinus
;	genMinusDec
	mov	a,r5
	add	a,#0xff
	mov	r3,a
	mov	a,r6
	addc	a,#0xff
	mov	r4,a
;	genMinus
	mov	a,r3
	clr	c
;	Peephole 236.l	used r7 instead of ar7
	subb	a,r7
	mov	r3,a
	mov	a,r4
;	Peephole 236.l	used r0 instead of ar0
	subb	a,r0
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r4,a
;	Peephole 105	removed redundant mov
	xch	a,r3
	add	a,acc
	xch	a,r3
	rlc	a
	xch	a,r3
	add	a,acc
	xch	a,r3
	rlc	a
	mov	r4,a
;	genLeftShift
	mov	b,r3
	inc	b
	mov	r3,#0x01
	mov	r4,#0x00
	sjmp	00117$
00116$:
	mov	a,r3
;	Peephole 254	optimized left shift
	add	a,r3
	mov	r3,a
	mov	a,r4
	rlc	a
	mov	r4,a
00117$:
	djnz	b,00116$
;	genAssign
	mov	dptr,#__mulint_PARM_2
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	genCall
	mov	dpl,r1
	mov	dph,r2
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	lcall	__mulint
	mov	_hex2int_sloc1_1_0,dpl
	mov	(_hex2int_sloc1_1_0 + 1),dph
	pop	ar0
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	dptr,#_hex2int_val_1_1
	movx	a,@dptr
	mov	_hex2int_sloc2_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc2_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc2_1_0 + 2),a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc2_1_0 + 3),a
;	genCast
	mov	r2,_hex2int_sloc1_1_0
	mov	r3,(_hex2int_sloc1_1_0 + 1)
	mov	r4,#0x00
	mov	r1,#0x00
;	genPlus
	mov	dptr,#_hex2int_val_1_1
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,_hex2int_sloc2_1_0
	movx	@dptr,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,(_hex2int_sloc2_1_0 + 1)
	inc	dptr
	movx	@dptr,a
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	addc	a,(_hex2int_sloc2_1_0 + 2)
	inc	dptr
	movx	@dptr,a
;	Peephole 236.g	used r1 instead of ar1
	mov	a,r1
	addc	a,(_hex2int_sloc2_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	genIpop
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00106$
00102$:
;	main.c:483: val += (a[i]-55)*(1<<(4*(len-1-i)));
;	genIpush
	push	ar2
	push	ar3
	push	ar4
;	genCast
	mov	r1,_hex2int_sloc0_1_0
	mov	a,_hex2int_sloc0_1_0
	rlc	a
	subb	a,acc
	mov	r2,a
;	genMinus
	mov	a,r1
	add	a,#0xc9
	mov	r1,a
	mov	a,r2
	addc	a,#0xff
	mov	r2,a
;	genMinus
;	genMinusDec
	mov	a,r5
	add	a,#0xff
	mov	r3,a
	mov	a,r6
	addc	a,#0xff
	mov	r4,a
;	genMinus
	mov	a,r3
	clr	c
;	Peephole 236.l	used r7 instead of ar7
	subb	a,r7
	mov	r3,a
	mov	a,r4
;	Peephole 236.l	used r0 instead of ar0
	subb	a,r0
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r4,a
;	Peephole 105	removed redundant mov
	xch	a,r3
	add	a,acc
	xch	a,r3
	rlc	a
	xch	a,r3
	add	a,acc
	xch	a,r3
	rlc	a
	mov	r4,a
;	genLeftShift
	mov	b,r3
	inc	b
	mov	r3,#0x01
	mov	r4,#0x00
	sjmp	00119$
00118$:
	mov	a,r3
;	Peephole 254	optimized left shift
	add	a,r3
	mov	r3,a
	mov	a,r4
	rlc	a
	mov	r4,a
00119$:
	djnz	b,00118$
;	genAssign
	mov	dptr,#__mulint_PARM_2
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	genCall
	mov	dpl,r1
	mov	dph,r2
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	lcall	__mulint
	mov	_hex2int_sloc2_1_0,dpl
	mov	(_hex2int_sloc2_1_0 + 1),dph
	pop	ar0
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	dptr,#_hex2int_val_1_1
	movx	a,@dptr
	mov	_hex2int_sloc3_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc3_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc3_1_0 + 2),a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc3_1_0 + 3),a
;	genCast
	mov	r2,_hex2int_sloc2_1_0
	mov	r3,(_hex2int_sloc2_1_0 + 1)
	mov	r4,#0x00
	mov	r1,#0x00
;	genPlus
	mov	dptr,#_hex2int_val_1_1
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,_hex2int_sloc3_1_0
	movx	@dptr,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,(_hex2int_sloc3_1_0 + 1)
	inc	dptr
	movx	@dptr,a
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	addc	a,(_hex2int_sloc3_1_0 + 2)
	inc	dptr
	movx	@dptr,a
;	Peephole 236.g	used r1 instead of ar1
	mov	a,r1
	addc	a,(_hex2int_sloc3_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	main.c:485: return val;
;	genIpop
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:483: val += (a[i]-55)*(1<<(4*(len-1-i)));
00106$:
;	main.c:479: for(i=0;i<len;i++)
;	genPlus
;     genPlusIncr
	inc	r7
	cjne	r7,#0x00,00120$
	inc	r0
00120$:
	ljmp	00104$
00107$:
;	main.c:485: return val;
;	genAssign
	mov	dptr,#_hex2int_val_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
;	genRet
	mov	r5,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 191	removed redundant mov
;	Peephole 300	removed redundant label 00108$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ReadVariable'
;------------------------------------------------------------
;l                         Allocated with name '_ReadVariable_l_1_1'
;m                         Allocated with name '_ReadVariable_m_1_1'
;n                         Allocated with name '_ReadVariable_n_1_1'
;k                         Allocated with name '_ReadVariable_k_1_1'
;ptr                       Allocated with name '_ReadVariable_ptr_1_1'
;------------------------------------------------------------
;	main.c:488: uint16_t ReadVariable()
;	-----------------------------------------
;	 function ReadVariable
;	-----------------------------------------
_ReadVariable:
;	main.c:490: uint16_t l=0, m=0, n, k=0;
;	genAssign
	mov	dptr,#_ReadVariable_k_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:491: unsigned char ptr[]="";
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_ReadVariable_ptr_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:492: do
00106$:
;	main.c:494: l=getchar();
;	genCall
	lcall	_getchar
;	genCast
;	peephole 177.g	optimized mov sequence
	mov	a,dpl
	mov	r2,a
	rlc	a
	subb	a,acc
	mov	r3,a
;	main.c:495: if(l!=READ_ENTER)
;	genCmpEq
;	gencjne
;	gencjneshort
;	Peephole 241.c	optimized compare
	clr	a
	cjne	r2,#0x0D,00115$
	cjne	r3,#0x00,00115$
	inc	a
00115$:
;	Peephole 300	removed redundant label 00116$
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00107$
;	Peephole 300	removed redundant label 00117$
;	main.c:497: if(l!=READ_BACKSPACE)
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x7F,00118$
	cjne	r3,#0x00,00118$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00102$
00118$:
;	main.c:499: ptr[k]=l;
;	genAssign
	mov	dptr,#_ReadVariable_k_1_1
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genPlus
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	add	a,#_ReadVariable_ptr_1_1
	mov	dpl,a
;	Peephole 236.g	used r6 instead of ar6
	mov	a,r6
	addc	a,#(_ReadVariable_ptr_1_1 >> 8)
	mov	dph,a
;	genCast
	mov	ar7,r2
;	genPointerSet
;     genFarPointerSet
	mov	a,r7
	movx	@dptr,a
;	main.c:500: k++;
;	genPlus
	mov	dptr,#_ReadVariable_k_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r5 instead of ar5
	add	a,r5
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r6 instead of ar6
	addc	a,r6
	inc	dptr
	movx	@dptr,a
;	main.c:501: putchar(l);
;	genCast
;	genCall
	mov	dpl,r2
	push	ar4
	lcall	_putchar
	pop	ar4
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00107$
00102$:
;	main.c:505: k--;
;	genAssign
	mov	dptr,#_ReadVariable_k_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genMinus
;	genMinusDec
	dec	r2
	cjne	r2,#0xff,00119$
	dec	r3
00119$:
;	genAssign
	mov	dptr,#_ReadVariable_k_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
00107$:
;	main.c:508: }while(l!=READ_ENTER);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00106$
;	Peephole 300	removed redundant label 00120$
;	main.c:509: n=hex2int(ptr,k);
;	genAssign
	mov	dptr,#_ReadVariable_k_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_hex2int_PARM_2
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#_ReadVariable_ptr_1_1
	mov	b,#0x00
;	genCast
;	main.c:510: return n;
;	genRet
;	Peephole 150.h	removed misc moves via dph, dpl, b, a before return
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_hex2int
;
;------------------------------------------------------------
;Allocation info for local variables in function 'interactive'
;------------------------------------------------------------
;co                        Allocated with name '_interactive_co_1_1'
;i                         Allocated with name '_interactive_i_1_1'
;------------------------------------------------------------
;	main.c:514: void interactive()
;	-----------------------------------------
;	 function interactive
;	-----------------------------------------
_interactive:
;	main.c:517: EA=0;
;	genAssign
	clr	_EA
;	main.c:518: i=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:519: EA=1;
;	genAssign
	setb	_EA
;	main.c:520: i=i&DDRAM_WRITE_MASK_BITS;
;	genAnd
	anl	ar2,#0x80
;	main.c:521: EA=0;
;	genAssign
	clr	_EA
;	main.c:522: *INST_WRITE=i;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:523: EA=1;
;	genAssign
	setb	_EA
;	main.c:524: putstr("Enter the character to display. Hit enter to stop\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_18
	mov	b,#0x80
	lcall	_putstr
;	main.c:525: do
00103$:
;	main.c:526: {   co=getchar();
;	genCall
	lcall	_getchar
;	main.c:527: putchar(co);
;	genCall
	mov  r2,dpl
;	Peephole 177.a	removed redundant mov
	push	ar2
	lcall	_putchar
	pop	ar2
;	main.c:528: if(co!=READ_ENTER)
;	genCmpEq
;	gencjne
;	gencjneshort
;	Peephole 241.d	optimized compare
	clr	a
	cjne	r2,#0x0D,00110$
	inc	a
00110$:
;	Peephole 300	removed redundant label 00111$
;	genIfx
	mov	r3,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00104$
;	Peephole 300	removed redundant label 00112$
;	main.c:529: lcdputch(co);
;	genCall
	mov	dpl,r2
	push	ar3
	lcall	_lcdputch
	pop	ar3
00104$:
;	main.c:530: }while(co!=READ_ENTER);
;	genIfx
	mov	a,r3
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00103$
;	Peephole 300	removed redundant label 00113$
;	Peephole 300	removed redundant label 00106$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'gotoxy'
;------------------------------------------------------------
;temp                      Allocated with name '_gotoxy_temp_1_1'
;temp1                     Allocated with name '_gotoxy_temp1_1_1'
;------------------------------------------------------------
;	main.c:533: void gotoxy()
;	-----------------------------------------
;	 function gotoxy
;	-----------------------------------------
_gotoxy:
;	main.c:536: putstr("\n\rEnter row number\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_19
	mov	b,#0x80
	lcall	_putstr
;	main.c:537: do
00103$:
;	main.c:539: temp=input_value();
;	genCall
	lcall	_input_value
	mov	r2,dpl
;	main.c:540: if(temp>3)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x03
	subb	a,r2
	clr	a
	rlc	a
;	genIfx
	mov	r3,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00119$
;	main.c:541: putstr("\n\rEnter correct row\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_20
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
00104$:
;	main.c:542: }while(temp>3);
;	genIfx
	mov	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00120$
;	main.c:543: putstr("\n\rEnter column number\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_21
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:544: do
00108$:
;	main.c:546: temp1=input_value();
;	genCall
	push	ar2
	lcall	_input_value
	mov	r3,dpl
	pop	ar2
;	main.c:547: if(temp1>15)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x0F
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00109$
;	Peephole 300	removed redundant label 00121$
;	main.c:548: putstr("\n\rEnter correct column number\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_22
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00109$:
;	main.c:549: }while(temp1>15);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00108$
;	Peephole 300	removed redundant label 00122$
;	main.c:550: lcdgotoxy(temp, temp1);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,r3
	movx	@dptr,a
;	genCall
	mov	dpl,r2
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
;
;------------------------------------------------------------
;Allocation info for local variables in function 'gotoaddress'
;------------------------------------------------------------
;temp                      Allocated with name '_gotoaddress_temp_1_1'
;------------------------------------------------------------
;	main.c:553: void gotoaddress()
;	-----------------------------------------
;	 function gotoaddress
;	-----------------------------------------
_gotoaddress:
;	main.c:555: unsigned char temp=0x66;
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	mov	a,#0x66
	movx	@dptr,a
;	main.c:556: putstr("Enter the address in hex\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_23
	mov	b,#0x80
	lcall	_putstr
;	main.c:557: while((temp>0x1F && temp<0x40)||(temp>=0x5F))
00107$:
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x1F
	jnc	00106$
;	Peephole 300	removed redundant label 00115$
;	genCmpLt
;	genCmp
	cjne	r2,#0x40,00116$
00116$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00108$
;	Peephole 300	removed redundant label 00117$
00106$:
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x5F,00118$
00118$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00109$
;	Peephole 300	removed redundant label 00119$
00108$:
;	main.c:559: temp=ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r3,dpl
	mov	r4,dph
;	genCast
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	mov	a,r3
	movx	@dptr,a
;	main.c:560: if((temp>0x1F && temp<0x40)||(temp>=0x5F))
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x1F
	jnc	00104$
;	Peephole 300	removed redundant label 00120$
;	genCmpLt
;	genCmp
	cjne	r3,#0x40,00121$
00121$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00101$
;	Peephole 300	removed redundant label 00122$
00104$:
;	genCmpLt
;	genCmp
	cjne	r3,#0x5F,00123$
00123$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00124$
00101$:
;	main.c:561: putstr("\n\rEnter correct address values in hex\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_24
	mov	b,#0x80
	lcall	_putstr
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00107$
00109$:
;	main.c:563: lcdgotoaddr(temp);
;	genCall
	mov	dpl,r2
	lcall	_lcdgotoaddr
;	main.c:564: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function '_sdcc_external_startup'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:567: _sdcc_external_startup()
;	-----------------------------------------
;	 function _sdcc_external_startup
;	-----------------------------------------
__sdcc_external_startup:
;	main.c:569: AUXR |= 0x0C;
;	genOr
	orl	_AUXR,#0x0C
;	main.c:570: return 0;
;	genRet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0000
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'delay'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:575: void delay()
;	-----------------------------------------
;	 function delay
;	-----------------------------------------
_delay:
;	main.c:584: _endasm ;
;	genInline
	    DELAY:MOV R5, #5
    DEL1:
	MOV R4, #10
    DEL:
	NOP
	        DJNZ r4, DEL
	         DJNZ R5, DEL1
	         ret
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'SerialInitialize'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:587: void SerialInitialize()
;	-----------------------------------------
;	 function SerialInitialize
;	-----------------------------------------
_SerialInitialize:
;	main.c:589: TMOD|=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
;	genOr
	orl	_TMOD,#0x20
;	main.c:590: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
;	genAssign
	mov	_TH1,#0xFD
;	main.c:591: TL1=0xFD;
;	genAssign
	mov	_TL1,#0xFD
;	main.c:592: SCON=0x50;
;	genAssign
	mov	_SCON,#0x50
;	main.c:593: TR1=1;  // to start timer
;	genAssign
	setb	_TR1
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
;x                         Allocated with name '_putchar_x_1_1'
;------------------------------------------------------------
;	main.c:597: void putchar(char x)
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
;	genReceive
	mov	a,dpl
	mov	dptr,#_putchar_x_1_1
	movx	@dptr,a
;	main.c:599: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
;	genAssign
	mov	dptr,#_putchar_x_1_1
	movx	a,@dptr
	mov	_SBUF,a
;	main.c:600: while(!TI); // wait until transmission is completed. when transmission is completed TI bit become 1
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
;	main.c:601: TI=0; // make TI bit to zero for next transmission
;	genAssign
;	Peephole 250.a	using atomic test and clear
	jbc	_TI,00108$
	sjmp	00101$
00108$:
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getchar'
;------------------------------------------------------------
;rec                       Allocated with name '_getchar_rec_1_1'
;------------------------------------------------------------
;	main.c:605: char getchar()
;	-----------------------------------------
;	 function getchar
;	-----------------------------------------
_getchar:
;	main.c:609: while(!RI);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
	jnb	_RI,00101$
;	Peephole 300	removed redundant label 00108$
;	main.c:610: rec=SBUF;   // data is received in SBUF register present in controller during receiving
;	genAssign
	mov	r2,_SBUF
;	main.c:611: RI=0;  // make RI bit to 0 so that next data is received
;	genAssign
	clr	_RI
;	main.c:612: return rec;  // return rec where function is called
;	genRet
	mov	dpl,r2
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'startbit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:616: void startbit()
;	-----------------------------------------
;	 function startbit
;	-----------------------------------------
_startbit:
;	main.c:626: _endasm ;
;	genInline
	        SETB P1.4
	        SETB P1.3
	        ACALL DELAY
	        CLR P1.4
	        ACALL DELAY
	        CLR P1.3
	        ACALL DELAY
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'stopbit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:630: void stopbit()
;	-----------------------------------------
;	 function stopbit
;	-----------------------------------------
_stopbit:
;	main.c:640: _endasm;
;	genInline
	        ACALL DELAY
	        CLR P1.4
	        ACALL DELAY
	        SETB P1.3
	        ACALL DELAY
	        SETB P1.4
	        ACALL DELAY
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ackbit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:644: void ackbit()
;	-----------------------------------------
;	 function ackbit
;	-----------------------------------------
_ackbit:
;	main.c:657: _endasm;
;	genInline
	        CLR C
	        ACALL DELAY
	        SETB P1.4
	        ACALL DELAY
	        SETB P1.3
	        ACALL DELAY
	        MOV C, P1.4
	        ACALL DELAY
	        CLR P1.3
	        ACALL DELAY
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Master_ack'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:662: void Master_ack()
;	-----------------------------------------
;	 function Master_ack
;	-----------------------------------------
_Master_ack:
;	main.c:675: _endasm;
;	genInline
	        ACALL DELAY
	        ACALL DELAY
	        CLR P1.4
	        ACALL DELAY
	        SETB P1.3
	        ACALL DELAY
	        ACALL DELAY
	        CLR P1.3
	        nop
	        setb P1.4
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'send_data_assembly'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:679: void send_data_assembly()
;	-----------------------------------------
;	 function send_data_assembly
;	-----------------------------------------
_send_data_assembly:
;	main.c:697: _endasm;
;	genInline
	        ACALL DELAY
	        MOV R1, #8
	        HERE:CLR C
	            RLC A
	            JNC NEXT
	            SETB P1.4
	            AJMP NEXT1
        NEXT:
	CLR P1.4
        NEXT1:
	            ACALL DELAY
	            SETB P1.3
	            ACALL DELAY
	            CLR P1.3
	            ACALL DELAY
	            DJNZ R1, HERE
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'send_data'
;------------------------------------------------------------
;value                     Allocated with name '_send_data_value_1_1'
;status_send               Allocated with name '_send_data_status_send_1_1'
;------------------------------------------------------------
;	main.c:700: int send_data(unsigned char value)
;	-----------------------------------------
;	 function send_data
;	-----------------------------------------
_send_data:
;	genReceive
	mov	a,dpl
	mov	dptr,#_send_data_value_1_1
	movx	@dptr,a
;	main.c:703: ACC = value;
;	genAssign
	mov	dptr,#_send_data_value_1_1
	movx	a,@dptr
	mov	_ACC,a
;	main.c:704: CY=1;
;	genAssign
	setb	_CY
;	main.c:705: send_data_assembly();
;	genCall
	lcall	_send_data_assembly
;	main.c:706: ackbit();
;	genCall
	lcall	_ackbit
;	main.c:707: ACC = 0x00;
;	genAssign
	mov	_ACC,#0x00
;	main.c:708: return status_send;
;	genRet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ninthbit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:711: void ninthbit()
;	-----------------------------------------
;	 function ninthbit
;	-----------------------------------------
_ninthbit:
;	main.c:722: _endasm ;
;	genInline
	    ACALL DELAY
	    SETB P1.4
	    ACALL DELAY
	    SETB P1.3
	    ACALL DELAY
	    ACALL DELAY
	    CLR P1.3
	    ACALL DELAY
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'clock_func'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:726: void clock_func()
;	-----------------------------------------
;	 function clock_func
;	-----------------------------------------
_clock_func:
;	main.c:743: _endasm;
;	genInline
	            MOV A, #0
	            MOV R7, #8
        LOOP2:
	NOP
	                ACALL DELAY
	                clr P1.3
	                ACALL DELAY
	                setb P1.3
	                MOV C, P1.4
	                RLC A
	                DJNZ R7, LOOP2
	                ACALL DELAY
	                clr P1.3
	                ACALL DELAY
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'eebytew'
;------------------------------------------------------------
;databyte                  Allocated with name '_eebytew_PARM_2'
;addr                      Allocated with name '_eebytew_addr_1_1'
;address                   Allocated with name '_eebytew_address_1_1'
;result                    Allocated with name '_eebytew_result_1_1'
;result1                   Allocated with name '_eebytew_result1_1_1'
;result2                   Allocated with name '_eebytew_result2_1_1'
;temp                      Allocated with name '_eebytew_temp_1_1'
;tempaddr                  Allocated with name '_eebytew_tempaddr_1_1'
;------------------------------------------------------------
;	main.c:747: int eebytew(uint16_t addr, unsigned char databyte)  // write byte, returns status
;	-----------------------------------------
;	 function eebytew
;	-----------------------------------------
_eebytew:
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_eebytew_addr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:752: printf("\n\rWriting 0x%03X: 0x%02X\n\r",addr, databyte);
;	genAssign
	mov	dptr,#_eebytew_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genCast
	mov	ar3,r2
	mov	r4,#0x00
;	genAssign
	mov	dptr,#_eebytew_addr_1_1
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genIpush
	push	ar2
	push	ar5
	push	ar6
	push	ar3
	push	ar4
;	genIpush
	push	ar5
	push	ar6
;	genIpush
	mov	a,#__str_25
	push	acc
	mov	a,#(__str_25 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xf9
	mov	sp,a
	pop	ar6
	pop	ar5
	pop	ar2
;	main.c:755: address &=I2C_BLOCKADDR_MASK_BITS;
;	genAnd
	mov	r3,#0x00
	mov	a,#0x07
	anl	a,r6
;	main.c:756: address >>=I2C_BLOCKADD_SHIFT_BITS;
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	r4,a
;	Peephole 105	removed redundant mov
	mov	c,acc.7
	xch	a,r3
	rlc	a
	xch	a,r3
	rlc	a
	xch	a,r3
	anl	a,#0x01
	mov	r4,a
;	main.c:757: temp |= address;
;	genOr
	orl	ar3,#0xA0
;	genCast
;	main.c:758: temp &= I2C_WRITE_MASK_BITS;
;	genAnd
	anl	ar3,#0xFE
;	main.c:759: tempaddr=addr;
;	genCast
;	main.c:760: startbit();
;	genCall
	push	ar2
	push	ar3
	push	ar5
	lcall	_startbit
	pop	ar5
	pop	ar3
	pop	ar2
;	main.c:761: result = send_data(temp);
;	genCall
	mov	dpl,r3
	push	ar2
	push	ar5
	lcall	_send_data
	mov	r3,dpl
	mov	r4,dph
	pop	ar5
	pop	ar2
;	main.c:762: result1 = send_data(tempaddr);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_send_data
	mov	r5,dpl
	mov	r6,dph
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:763: result2 = send_data(databyte);
;	genCall
	mov	dpl,r2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_send_data
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
;	main.c:764: stopbit();
;	genCall
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_stopbit
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
;	main.c:765: confirm=1;
;	genAssign
	mov	dptr,#_confirm
	mov	a,#0x01
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	main.c:766: return (result&result1);
;	genAnd
	mov	a,r5
	anl	ar3,a
	mov	a,r6
	anl	ar4,a
;	genRet
	mov	dpl,r3
	mov	dph,r4
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'eebyter'
;------------------------------------------------------------
;addr                      Allocated with name '_eebyter_addr_1_1'
;address                   Allocated with name '_eebyter_address_1_1'
;temp                      Allocated with name '_eebyter_temp_1_1'
;temp1                     Allocated with name '_eebyter_temp1_1_1'
;tempaddr                  Allocated with name '_eebyter_tempaddr_1_1'
;backup                    Allocated with name '_eebyter_backup_1_1'
;------------------------------------------------------------
;	main.c:770: int eebyter(uint16_t addr)
;	-----------------------------------------
;	 function eebyter
;	-----------------------------------------
_eebyter:
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_eebyter_addr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:775: address=addr;
;	genAssign
	mov	dptr,#_eebyter_addr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	main.c:776: address &=I2C_BLOCKADDR_MASK_BITS;
;	genAnd
	mov	r4,#0x00
	mov	a,#0x07
	anl	a,r3
;	main.c:777: address >>=I2C_BLOCKADD_SHIFT_BITS;
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	r5,a
;	Peephole 105	removed redundant mov
	mov	c,acc.7
	xch	a,r4
	rlc	a
	xch	a,r4
	rlc	a
	xch	a,r4
	anl	a,#0x01
	mov	r5,a
;	main.c:778: temp |= address;
;	genOr
	orl	ar4,#0xA0
;	genCast
;	main.c:779: temp1=(temp&I2C_WRITE_MASK_BITS);
;	genAnd
	mov	a,#0xFE
	anl	a,r4
	mov	r5,a
;	main.c:780: tempaddr=addr;
;	genCast
	mov	ar6,r2
;	main.c:781: startbit();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_startbit
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:782: send_data(temp1);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	push	ar6
	lcall	_send_data
	pop	ar6
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:783: send_data(tempaddr);
;	genCall
	mov	dpl,r6
	push	ar2
	push	ar3
	push	ar4
	lcall	_send_data
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:784: startbit();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_startbit
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:785: temp=temp | I2C_READ_MASK_BITS;
;	genOr
	orl	ar4,#0x01
;	main.c:786: send_data(temp);
;	genCall
	mov	dpl,r4
	push	ar2
	push	ar3
	lcall	_send_data
	pop	ar3
	pop	ar2
;	main.c:787: clock_func();
;	genCall
	push	ar2
	push	ar3
	lcall	_clock_func
	pop	ar3
	pop	ar2
;	main.c:788: stopbit();
;	genCall
	push	ar2
	push	ar3
	lcall	_stopbit
	pop	ar3
	pop	ar2
;	main.c:789: backup=ACC;
;	genAssign
;	main.c:790: printf("\n\r0x%03X: 0x%02X\n\r", addr, backup);
;	genAssign
	mov	dptr,#_eebyter_backup_1_1
	mov	a,_ACC
	movx	@dptr,a
;	Peephole 180.a	removed redundant mov to dptr
	movx	a,@dptr
	mov	r4,a
;	genCast
	mov	r5,#0x00
;	genIpush
	push	ar4
	push	ar5
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_26
	push	acc
	mov	a,#(__str_26 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xf9
	mov	sp,a
;	main.c:791: return 0;
;	genRet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0000
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'readall_function'
;------------------------------------------------------------
;address                   Allocated with name '_readall_function_address_1_1'
;temp                      Allocated with name '_readall_function_temp_1_1'
;temp1                     Allocated with name '_readall_function_temp1_1_1'
;tempaddr                  Allocated with name '_readall_function_tempaddr_1_1'
;backup                    Allocated with name '_readall_function_backup_1_1'
;i                         Allocated with name '_readall_function_i_1_1'
;tempvalue                 Allocated with name '_readall_function_tempvalue_1_1'
;tempvalue1                Allocated with name '_readall_function_tempvalue1_1_1'
;------------------------------------------------------------
;	main.c:795: void readall_function()
;	-----------------------------------------
;	 function readall_function
;	-----------------------------------------
_readall_function:
;	main.c:800: putstr("Enter the address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
	lcall	_putstr
;	main.c:801: do
00103$:
;	main.c:803: tempvalue = ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r2,dpl
	mov	r3,dph
;	genAssign
	mov	dptr,#_readall_function_tempvalue_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:804: if(tempvalue>I2C_ADDR_MASK_BITS)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r2
	mov	a,#0x07
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00134$
;	main.c:805: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_28
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00104$:
;	main.c:806: }while(tempvalue>I2C_ADDR_MASK_BITS);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00135$
;	main.c:807: putstr("\n\rEnter the second higher address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_29
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
;	main.c:808: do
00108$:
;	main.c:810: tempvalue1 = ReadVariable();
;	genCall
	push	ar2
	push	ar3
	lcall	_ReadVariable
	mov	r4,dpl
	mov	r5,dph
	pop	ar3
	pop	ar2
;	main.c:811: if(tempvalue1>I2C_ADDR_MASK_BITS)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r4
	mov	a,#0x07
	subb	a,r5
	clr	a
	rlc	a
;	genIfx
	mov	r6,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00109$
;	Peephole 300	removed redundant label 00136$
;	main.c:812: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_28
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_putstr
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
00109$:
;	main.c:813: }while(tempvalue1>I2C_ADDR_MASK_BITS);
;	genIfx
	mov	a,r6
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
;	main.c:817: address &=I2C_BLOCKADDR_MASK_BITS;
;	genAnd
	jnz	00108$
;	Peephole 300	removed redundant label 00137$
;	Peephole 256.c	loading r6 with zero from a
	mov	r6,a
	mov	a,#0x07
	anl	a,r3
;	main.c:818: address >>=I2C_BLOCKADD_SHIFT_BITS;
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	r7,a
;	Peephole 105	removed redundant mov
	mov	c,acc.7
	xch	a,r6
	rlc	a
	xch	a,r6
	rlc	a
	xch	a,r6
	anl	a,#0x01
	mov	r7,a
;	main.c:819: temp |= address;
;	genOr
	orl	ar6,#0xA0
;	genCast
;	main.c:820: temp1=(temp&I2C_WRITE_MASK_BITS);
;	genAnd
	mov	a,#0xFE
	anl	a,r6
	mov	r7,a
;	main.c:821: tempaddr=tempvalue;
;	genCast
;	main.c:822: startbit();
;	genCall
	push	ar2
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_startbit
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar2
;	main.c:823: send_data(temp1);
;	genCall
	mov	dpl,r7
	push	ar2
	push	ar4
	push	ar5
	push	ar6
	lcall	_send_data
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar2
;	main.c:824: send_data(tempaddr);
;	genCall
	mov	dpl,r2
	push	ar4
	push	ar5
	push	ar6
	lcall	_send_data
	pop	ar6
	pop	ar5
	pop	ar4
;	main.c:825: startbit();
;	genCall
	push	ar4
	push	ar5
	push	ar6
	lcall	_startbit
	pop	ar6
	pop	ar5
	pop	ar4
;	main.c:826: temp=temp | I2C_READ_MASK_BITS;
;	genOr
	orl	ar6,#0x01
;	main.c:827: send_data(temp);
;	genCall
	mov	dpl,r6
	push	ar4
	push	ar5
	lcall	_send_data
	pop	ar5
	pop	ar4
;	main.c:828: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:829: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:830: putstr("EEPROM data\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_30
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:831: putstr("ADDR |  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_31
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:832: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:833: while(tempvalue<=tempvalue1)
00118$:
;	genAssign
	mov	dptr,#_readall_function_tempvalue_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpGt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,r2
	mov	a,r5
	subb	a,r3
;	genIfxJump
	jnc	00138$
	ljmp	00120$
00138$:
;	main.c:835: printf("0x%03X: ", tempvalue);
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_32
	push	acc
	mov	a,#(__str_32 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:836: for(i=0; i<=15; i++)
;	genAssign
;	genAssign
	mov	r6,#0x00
00114$:
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r6
	add	a,#0xff - 0x0F
	jnc	00139$
	ljmp	00117$
00139$:
;	main.c:838: clock_func();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_clock_func
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:839: backup=ACC;
;	genAssign
;	main.c:840: printf("%02X  ", backup);
;	genAssign
	mov	dptr,#_readall_function_backup_1_1
	mov	a,_ACC
	movx	@dptr,a
;	Peephole 180.a	removed redundant mov to dptr
	movx	a,@dptr
	mov	r7,a
;	genCast
	mov	r0,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
;	genIpush
	mov	a,#__str_33
	push	acc
	mov	a,#(__str_33 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:841: if(tempvalue!=tempvalue1)
;	genCmpEq
;	gencjneshort
	mov	a,r2
	cjne	a,ar4,00140$
	mov	a,r3
	cjne	a,ar5,00140$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00112$
00140$:
;	main.c:843: Master_ack();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_Master_ack
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:844: tempvalue++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00141$
	inc	r3
00141$:
;	genAssign
	mov	dptr,#_readall_function_tempvalue_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00116$
00112$:
;	main.c:848: tempvalue++;
;	genPlus
	mov	dptr,#_readall_function_tempvalue_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	main.c:849: break;
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00116$:
;	main.c:836: for(i=0; i<=15; i++)
;	genPlus
;     genPlusIncr
	inc	r6
	ljmp	00114$
00117$:
;	main.c:853: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
	ljmp	00118$
00120$:
;	main.c:855: stopbit();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_stopbit
;
;------------------------------------------------------------
;Allocation info for local variables in function 'write_function'
;------------------------------------------------------------
;temp                      Allocated with name '_write_function_temp_1_1'
;temp1                     Allocated with name '_write_function_temp1_1_1'
;------------------------------------------------------------
;	main.c:860: void write_function()
;	-----------------------------------------
;	 function write_function
;	-----------------------------------------
_write_function:
;	main.c:863: putstr("Enter the address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
	lcall	_putstr
;	main.c:864: do
00103$:
;	main.c:866: temp = ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r2,dpl
	mov	r3,dph
;	main.c:867: if(temp>I2C_ADDR_MASK_BITS)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r2
	mov	a,#0x07
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00119$
;	main.c:868: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_28
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00104$:
;	main.c:869: }while(temp>I2C_ADDR_MASK_BITS);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00120$
;	main.c:870: putstr("\n\rEnter the Value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_34
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
;	main.c:871: do
00108$:
;	main.c:873: temp1=ReadVariable();
;	genCall
	push	ar2
	push	ar3
	lcall	_ReadVariable
	mov	r4,dpl
	mov	r5,dph
	pop	ar3
	pop	ar2
;	main.c:874: if(temp1>I2C_ADDR_MASK_BITS)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r4
	mov	a,#0x07
	subb	a,r5
	clr	a
	rlc	a
;	genIfx
	mov	r6,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00109$
;	Peephole 300	removed redundant label 00121$
;	main.c:875: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_28
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_putstr
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
00109$:
;	main.c:876: }while(temp1>I2C_ADDR_MASK_BITS);
;	genIfx
	mov	a,r6
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00108$
;	Peephole 300	removed redundant label 00122$
;	main.c:878: eebytew(temp, temp1);
;	genCast
	mov	dptr,#_eebytew_PARM_2
	mov	a,r4
	movx	@dptr,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_eebytew
;
;------------------------------------------------------------
;Allocation info for local variables in function 'help_menu'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:882: void help_menu()
;	-----------------------------------------
;	 function help_menu
;	-----------------------------------------
_help_menu:
;	main.c:884: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_35
	mov	b,#0x80
	lcall	_putstr
;	main.c:885: putstr("EEPROM Control\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_36
	mov	b,#0x80
	lcall	_putstr
;	main.c:886: putstr("Press 'R' to read a byte from the user input address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_37
	mov	b,#0x80
	lcall	_putstr
;	main.c:887: putstr("Press 'W' to write a byte data to user input address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_38
	mov	b,#0x80
	lcall	_putstr
;	main.c:888: putstr("Press 'S' for sequential read between the adresses\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_39
	mov	b,#0x80
	lcall	_putstr
;	main.c:889: putstr("Press 'T' for EEPROM software reset\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_40
	mov	b,#0x80
	lcall	_putstr
;	main.c:890: putstr("LCD Control\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_41
	mov	b,#0x80
	lcall	_putstr
;	main.c:891: putstr("Press 'C' for CGRAM HEX dump\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_42
	mov	b,#0x80
	lcall	_putstr
;	main.c:892: putstr("Press 'D' for DDRAM HEX dump\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_43
	mov	b,#0x80
	lcall	_putstr
;	main.c:893: putstr("Press 'E' for Clearing the LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_44
	mov	b,#0x80
	lcall	_putstr
;	main.c:894: putstr("Press 'P' to display the custom character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_45
	mov	b,#0x80
	lcall	_putstr
;	main.c:895: putstr("Press 'N' to add custom character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_46
	mov	b,#0x80
	lcall	_putstr
;	main.c:896: putstr("Press 'I' for printing letters on LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_47
	mov	b,#0x80
	lcall	_putstr
;	main.c:897: putstr("Press 'K' for displaying the logo\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_48
	mov	b,#0x80
	lcall	_putstr
;	main.c:898: putstr("Press 'X' for moving the cursor to user input x,y position\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_49
	mov	b,#0x80
	lcall	_putstr
;	main.c:899: putstr("Press 'G' for moving the cursor to user input address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_50
	mov	b,#0x80
	lcall	_putstr
;	main.c:900: putstr("Clock Control\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_51
	mov	b,#0x80
	lcall	_putstr
;	main.c:901: putstr("Press '0' for pausing the clock\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_52
	mov	b,#0x80
	lcall	_putstr
;	main.c:902: putstr("Press '1' to resume the clock \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_53
	mov	b,#0x80
	lcall	_putstr
;	main.c:903: putstr("Press '2' to reset the clock to zero\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_54
	mov	b,#0x80
	lcall	_putstr
;	main.c:904: putstr("IO Expander control\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_55
	mov	b,#0x80
	lcall	_putstr
;	main.c:905: putstr("Press 'B' for writing data to IO Expander\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_56
	mov	b,#0x80
	lcall	_putstr
;	main.c:906: putstr("Press 'F' to read data from IO Expander\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_57
	mov	b,#0x80
	lcall	_putstr
;	main.c:907: putstr("Press 'H' for configuring the individual GPIO pin\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_58
	mov	b,#0x80
	lcall	_putstr
;	main.c:908: putstr("Press 'J' for configuring IO expander's one pin as input and one as output\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_59
	mov	b,#0x80
	lcall	_putstr
;	main.c:909: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_35
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'read_function'
;------------------------------------------------------------
;temp1                     Allocated with name '_read_function_temp1_1_1'
;------------------------------------------------------------
;	main.c:912: void read_function()
;	-----------------------------------------
;	 function read_function
;	-----------------------------------------
_read_function:
;	main.c:915: putstr("Enter the address in hex\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_23
	mov	b,#0x80
	lcall	_putstr
;	main.c:916: do
00103$:
;	main.c:918: temp1=ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r2,dpl
	mov	r3,dph
;	main.c:919: if(temp1>I2C_ADDR_MASK_BITS)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r2
	mov	a,#0x07
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00111$
;	main.c:920: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_28
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00104$:
;	main.c:921: }while(temp1>I2C_ADDR_MASK_BITS);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00112$
;	main.c:922: eebyter(temp1);
;	genCall
	mov	dpl,r2
	mov	dph,r3
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_eebyter
;
;------------------------------------------------------------
;Allocation info for local variables in function 'custom_character'
;------------------------------------------------------------
;in_array                  Allocated with name '_custom_character_in_array_1_1'
;in_array5                 Allocated with name '_custom_character_in_array5_1_1'
;in_array1                 Allocated with name '_custom_character_in_array1_1_1'
;in_array2                 Allocated with name '_custom_character_in_array2_1_1'
;in_array3                 Allocated with name '_custom_character_in_array3_1_1'
;in_array4                 Allocated with name '_custom_character_in_array4_1_1'
;in_array6                 Allocated with name '_custom_character_in_array6_1_1'
;in_array7                 Allocated with name '_custom_character_in_array7_1_1'
;kk                        Allocated with name '_custom_character_kk_1_1'
;------------------------------------------------------------
;	main.c:925: void custom_character()
;	-----------------------------------------
;	 function custom_character
;	-----------------------------------------
_custom_character:
;	main.c:927: unsigned char in_array[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array_1_1
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0001)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0002)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0003)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0004)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0005)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0006)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0007)
	mov	a,#0x0C
	movx	@dptr,a
;	main.c:928: unsigned char in_array5[] = {0x0f, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array5_1_1
	mov	a,#0x0F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0001)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0002)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0003)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0004)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0005)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0006)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0007)
	mov	a,#0x0F
	movx	@dptr,a
;	main.c:929: unsigned char in_array1[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x1f, 0x1f, 0x1f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array1_1_1
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0001)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0002)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0003)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0004)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0005)
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0006)
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0007)
	mov	a,#0x1F
	movx	@dptr,a
;	main.c:930: unsigned char in_array2[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x03};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array2_1_1
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0001)
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0002)
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0003)
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0004)
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0005)
	mov	a,#0x03
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0006)
	mov	a,#0x03
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0007)
	mov	a,#0x03
	movx	@dptr,a
;	main.c:931: unsigned char in_array3[] = {0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array3_1_1
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0001)
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0002)
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0003)
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0004)
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0005)
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0006)
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0007)
	mov	a,#0x1F
	movx	@dptr,a
;	main.c:932: unsigned char in_array4[] = {0x1f, 0x00, 0x04, 0x0A, 0x0A, 0x04, 0x00, 0x1f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array4_1_1
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0001)
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0002)
	mov	a,#0x04
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0003)
	mov	a,#0x0A
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0004)
	mov	a,#0x0A
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0005)
	mov	a,#0x04
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0006)
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0007)
	mov	a,#0x1F
	movx	@dptr,a
;	main.c:933: unsigned char in_array6[] = {0x1f, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x1f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array6_1_1
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0001)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0002)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0003)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0004)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0005)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0006)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0007)
	mov	a,#0x1F
	movx	@dptr,a
;	main.c:934: unsigned char in_array7[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x018, 0x18};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array7_1_1
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0001)
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0002)
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0003)
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0004)
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0005)
	mov	a,#0x18
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0006)
	mov	a,#0x18
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0007)
	mov	a,#0x18
	movx	@dptr,a
;	main.c:936: EA=0;
;	genAssign
	clr	_EA
;	main.c:937: kk=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:938: EA=1;
;	genAssign
	setb	_EA
;	main.c:939: kk &= CURSORADDR_MASK_BITS;
;	genAnd
	anl	ar2,#0x7F
;	main.c:940: lcdclear();
;	genCall
	push	ar2
	lcall	_lcdclear
	pop	ar2
;	main.c:941: putstr("Look at LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_60
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:942: lcdcreatechar(CGRAM_FIRSTCHAR_ADDR, in_array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:943: lcdcreatechar(CGRAM_SECONDCHAR_ADDR, in_array1);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array1_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array1_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x08
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:944: lcdcreatechar(CGRAM_THIRDCHAR_ADDR, in_array2);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array2_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array2_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x10
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:945: lcdcreatechar(CGRAM_FOURTHCHAR_ADDR, in_array3);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array3_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array3_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x18
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:946: lcdcreatechar(CGRAM_FIFTHCHAR_ADDR, in_array4);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array4_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array4_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x20
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:947: lcdcreatechar(CGRAM_SIXTHCHAR_ADDR, in_array5);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array5_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array5_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x28
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:948: lcdcreatechar(CGRAM_SEVENTHCHAR_ADDR, in_array6);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array6_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array6_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x30
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:949: lcdcreatechar(CGRAM_EIGHTHCHAR_ADDR, in_array7);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array7_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array7_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x38
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:950: lcdgotoxy(3,2);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x02
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:951: lcdputch(0x01);
;	genCall
	mov	dpl,#0x01
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:952: lcdgotoxy(2,2);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x02
	movx	@dptr,a
;	genCall
	mov	dpl,#0x02
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:953: lcdputch(0x00);
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:954: lcdgotoxy(1,2);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x02
	movx	@dptr,a
;	genCall
	mov	dpl,#0x01
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:955: lcdputch(0x00);
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:956: lcdgotoxy(0,2);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x02
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:957: lcdputch(0x05);
;	genCall
	mov	dpl,#0x05
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:958: lcdgotoxy(3,1);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x01
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:959: lcdputch(0x02);
;	genCall
	mov	dpl,#0x02
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:960: lcdgotoxy(3,3);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x03
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:961: lcdputch(0x07);
;	genCall
	mov	dpl,#0x07
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:962: lcdgotoxy(0,3);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x03
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:963: lcdputch(0x03);
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:964: lcdgotoxy(0,4);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x04
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:965: lcdputch(0x04);
;	genCall
	mov	dpl,#0x04
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:966: lcdgotoxy(0,5);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x05
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:967: lcdputch(0x06);
;	genCall
	mov	dpl,#0x06
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:968: *INST_WRITE=(kk|DDRAM_WRITE_MASK_BITS);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:969: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'Timer0Initialize'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:973: void Timer0Initialize()
;	-----------------------------------------
;	 function Timer0Initialize
;	-----------------------------------------
_Timer0Initialize:
;	main.c:975: TMOD |= 0x09;
;	genOr
	orl	_TMOD,#0x09
;	main.c:976: TH0 = 0x4C;
;	genAssign
	mov	_TH0,#0x4C
;	main.c:977: TL0 = 0x20;
;	genAssign
	mov	_TL0,#0x20
;	main.c:978: IE = 0x82;
;	genAssign
	mov	_IE,#0x82
;	main.c:979: TR0=1;
;	genAssign
	setb	_TR0
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdtime'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:982: void lcdtime()
;	-----------------------------------------
;	 function lcdtime
;	-----------------------------------------
_lcdtime:
;	main.c:984: lcdclockgotoaddr(0x59);
;	genCall
	mov	dpl,#0x59
	lcall	_lcdclockgotoaddr
;	main.c:985: lcdputstr("00:00.0");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_61
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdputstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'read_ioexpander'
;------------------------------------------------------------
;io_tem                    Allocated with name '_read_ioexpander_io_tem_1_1'
;------------------------------------------------------------
;	main.c:988: unsigned char read_ioexpander()
;	-----------------------------------------
;	 function read_ioexpander
;	-----------------------------------------
_read_ioexpander:
;	main.c:992: startbit();
;	genCall
	lcall	_startbit
;	main.c:993: send_data(io_tem);
;	genCall
	mov	dpl,#0x71
	lcall	_send_data
;	main.c:994: clock_func();
;	genCall
	lcall	_clock_func
;	main.c:995: stopbit();
;	genCall
	lcall	_stopbit
;	main.c:996: io_tem = ACC;
;	genAssign
;	main.c:997: return io_tem;
;	genAssign
	mov	dptr,#_read_ioexpander_io_tem_1_1
	mov	a,_ACC
	movx	@dptr,a
;	Peephole 180.a	removed redundant mov to dptr
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'invert_ioexpander'
;------------------------------------------------------------
;value_io                  Allocated with name '_invert_ioexpander_value_io_1_1'
;------------------------------------------------------------
;	main.c:1000: unsigned char invert_ioexpander(unsigned char value_io)
;	-----------------------------------------
;	 function invert_ioexpander
;	-----------------------------------------
_invert_ioexpander:
;	genReceive
	mov	a,dpl
	mov	dptr,#_invert_ioexpander_value_io_1_1
	movx	@dptr,a
;	main.c:1002: value_io &= IOEXP_INIT_BITS;
;	genAssign
;	genAnd
	mov	dptr,#_invert_ioexpander_value_io_1_1
	movx	a,@dptr
	mov	r2,a
;	Peephole 248.b	optimized and to xdata
	anl	a,#0x01
	movx	@dptr,a
;	main.c:1003: value_io<<=1;
;	genAssign
	mov	dptr,#_invert_ioexpander_value_io_1_1
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
;	Peephole 105	removed redundant mov
;	genAssign
;	Peephole 204	removed redundant mov
	add	a,acc
	mov	r2,a
	mov	dptr,#_invert_ioexpander_value_io_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
;	main.c:1004: value_io ^= 0xFF;
;	genAssign
;	genXor
;	Peephole 236.n	used r2 instead of ar2
	mov	dptr,#_invert_ioexpander_value_io_1_1
	movx	a,@dptr
	mov	r2,a
;	Peephole 248.c	optimized xor to xdata
	xrl	a,#0xFF
	movx	@dptr,a
;	main.c:1005: value_io &= 0x02;
;	genAssign
;	genAnd
;	Peephole 248.b	optimized and to xdata
;	main.c:1006: value_io |= 0x01;
;	genAssign
;	genOr
;	Peephole 248.a	optimized or to xdata
	mov	dptr,#_invert_ioexpander_value_io_1_1
	movx	a,@dptr
;	Peephole 248.g	optimized and/or to volatile xdata
	anl	a,#0x02
	movx	@dptr,a
	movx	a,@dptr
	mov	r2,a
	orl	a,#0x01
	movx	@dptr,a
;	main.c:1007: return value_io;
;	genAssign
	mov	dptr,#_invert_ioexpander_value_io_1_1
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'write_ioexpander'
;------------------------------------------------------------
;send_bits                 Allocated with name '_write_ioexpander_send_bits_1_1'
;io_temp                   Allocated with name '_write_ioexpander_io_temp_1_1'
;------------------------------------------------------------
;	main.c:1010: void write_ioexpander(unsigned char send_bits)
;	-----------------------------------------
;	 function write_ioexpander
;	-----------------------------------------
_write_ioexpander:
;	genReceive
	mov	a,dpl
	mov	dptr,#_write_ioexpander_send_bits_1_1
	movx	@dptr,a
;	main.c:1014: startbit();
;	genCall
	lcall	_startbit
;	main.c:1015: send_data(io_temp);
;	genCall
	mov	dpl,#0x70
	lcall	_send_data
;	main.c:1016: send_data(send_bits);
;	genAssign
	mov	dptr,#_write_ioexpander_send_bits_1_1
	movx	a,@dptr
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
	lcall	_send_data
;	main.c:1017: stopbit();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_stopbit
;
;------------------------------------------------------------
;Allocation info for local variables in function 'external_isr'
;------------------------------------------------------------
;io_data                   Allocated with name '_external_isr_io_data_1_1'
;io_inver                  Allocated with name '_external_isr_io_inver_1_1'
;------------------------------------------------------------
;	main.c:1020: void external_isr(void) __interrupt(2) __using(2)
;	-----------------------------------------
;	 function external_isr
;	-----------------------------------------
_external_isr:
	ar2 = 0x12
	ar3 = 0x13
	ar4 = 0x14
	ar5 = 0x15
	ar6 = 0x16
	ar7 = 0x17
	ar0 = 0x10
	ar1 = 0x11
	push	acc
	push	b
	push	dpl
	push	dph
	push	(0+2)
	push	(0+3)
	push	(0+4)
	push	(0+5)
	push	(0+6)
	push	(0+7)
	push	(0+0)
	push	(0+1)
	push	psw
	mov	psw,#0x10
;	main.c:1023: EX1=0;
;	genAssign
	clr	_EX1
;	main.c:1024: io_data=read_ioexpander();
;	genCall
	mov	psw,#0x00
	lcall	_read_ioexpander
	mov	psw,#0x10
;	main.c:1025: io_inver=invert_ioexpander(io_data);
;	genCall
	mov  r2,dpl
;	Peephole 177.a	removed redundant mov
	mov	psw,#0x00
	lcall	_invert_ioexpander
	mov	psw,#0x10
;	main.c:1026: write_ioexpander(io_inver);
;	genCall
	mov  r2,dpl
;	Peephole 177.a	removed redundant mov
	mov	psw,#0x00
	lcall	_write_ioexpander
	mov	psw,#0x10
;	main.c:1027: EX1=1;
;	genAssign
	setb	_EX1
;	Peephole 300	removed redundant label 00101$
	pop	psw
	pop	(0+1)
	pop	(0+0)
	pop	(0+7)
	pop	(0+6)
	pop	(0+5)
	pop	(0+4)
	pop	(0+3)
	pop	(0+2)
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	reti
;------------------------------------------------------------
;Allocation info for local variables in function 'timer_isr'
;------------------------------------------------------------
;interr                    Allocated with name '_timer_isr_interr_1_1'
;bb                        Allocated with name '_timer_isr_bb_1_1'
;------------------------------------------------------------
;	main.c:1030: void timer_isr(void) __critical __interrupt(1)__using(1)
;	-----------------------------------------
;	 function timer_isr
;	-----------------------------------------
_timer_isr:
	ar2 = 0x0a
	ar3 = 0x0b
	ar4 = 0x0c
	ar5 = 0x0d
	ar6 = 0x0e
	ar7 = 0x0f
	ar0 = 0x08
	ar1 = 0x09
	push	acc
	push	b
	push	dpl
	push	dph
	push	(0+2)
	push	(0+3)
	push	(0+4)
	push	(0+5)
	push	(0+6)
	push	(0+7)
	push	(0+0)
	push	(0+1)
	push	psw
	mov	psw,#0x08
	setb	c
	jbc	ea,00126$
	clr	c
00126$:
	push	psw
;	main.c:1034: TF0=0;
;	genAssign
	clr	_TF0
;	main.c:1035: TH0 = 0x4C;
;	genAssign
	mov	_TH0,#0x4C
;	main.c:1036: TL0 = 0x20;
;	genAssign
	mov	_TL0,#0x20
;	main.c:1037: TR0 = 1;
;	genAssign
	setb	_TR0
;	main.c:1038: if(CLOCK_ENABLE_FLAG)
;	genAssign
	mov	dptr,#_CLOCK_ENABLE_FLAG
	movx	a,@dptr
;	genIfx
	mov	r2,a
;	Peephole 105	removed redundant mov
;	genIfxJump
	jnz	00127$
	ljmp	00116$
00127$:
;	main.c:1040: if(count_flag==0x00)
;	genAssign
	mov	dptr,#_count_flag
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r3,a
;	Peephole 135	removed redundant mov
	orl	a,r2
;	genIfxJump
	jz	00128$
	ljmp	00113$
00128$:
;	main.c:1042: P1_6 = 0;
;	genAssign
	clr	_P1_6
;	main.c:1043: P1_6=1;
;	genAssign
	setb	_P1_6
;	main.c:1044: interr=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:1045: interr= interr&CURSORADDR_MASK_BITS;
;	genAnd
	anl	ar2,#0x7F
;	main.c:1046: count_flag=0x02;
;	genAssign
	mov	dptr,#_count_flag
	mov	a,#0x02
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	main.c:1047: MS++;
;	genPlus
	mov	dptr,#_MS
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
;	main.c:1048: lcdclockgotoaddr(0x5F);
;	genCall
	mov	dpl,#0x5F
	push	ar2
	mov	psw,#0x00
	lcall	_lcdclockgotoaddr
	mov	psw,#0x08
	pop	ar2
;	main.c:1049: if(MS>9)
;	genAssign
	mov	dptr,#_MS
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r3,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x09
	jnc	00102$
;	Peephole 300	removed redundant label 00129$
;	main.c:1051: MS=0;
;	genAssign
	mov	dptr,#_MS
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1052: S++;
;	genPlus
	mov	dptr,#_S
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
00102$:
;	main.c:1054: lcdputch(MS+CLOCK_CGRAM_BASE_ADDR);
;	genAssign
	mov	dptr,#_MS
	movx	a,@dptr
	mov	r3,a
;	genPlus
;     genPlusIncr
	mov	a,#0x30
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
;	genCall
	mov	r3,a
;	Peephole 244.c	loading dpl from a instead of r3
	mov	dpl,a
	push	ar2
	mov	psw,#0x00
	lcall	_lcdputch
	mov	psw,#0x08
	pop	ar2
;	main.c:1055: if(S>9)
;	genAssign
	mov	dptr,#_S
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r3,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x09
	jnc	00104$
;	Peephole 300	removed redundant label 00130$
;	main.c:1057: S=0;
;	genAssign
	mov	dptr,#_S
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1058: SS++;
;	genPlus
	mov	dptr,#_SS
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
00104$:
;	main.c:1060: lcdclockgotoaddr(0x5D);
;	genCall
	mov	dpl,#0x5D
	push	ar2
	mov	psw,#0x00
	lcall	_lcdclockgotoaddr
	mov	psw,#0x08
	pop	ar2
;	main.c:1061: lcdputch(S+CLOCK_CGRAM_BASE_ADDR);
;	genAssign
	mov	dptr,#_S
	movx	a,@dptr
	mov	r3,a
;	genPlus
;     genPlusIncr
	mov	a,#0x30
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
;	genCall
	mov	r3,a
;	Peephole 244.c	loading dpl from a instead of r3
	mov	dpl,a
	push	ar2
	mov	psw,#0x00
	lcall	_lcdputch
	mov	psw,#0x08
	pop	ar2
;	main.c:1062: if(SS>5)
;	genAssign
	mov	dptr,#_SS
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r3,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x05
	jnc	00106$
;	Peephole 300	removed redundant label 00131$
;	main.c:1064: SS=0;
;	genAssign
	mov	dptr,#_SS
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1065: M++;
;	genPlus
	mov	dptr,#_M
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
00106$:
;	main.c:1067: lcdclockgotoaddr(0x5C);
;	genCall
	mov	dpl,#0x5C
	push	ar2
	mov	psw,#0x00
	lcall	_lcdclockgotoaddr
	mov	psw,#0x08
	pop	ar2
;	main.c:1068: lcdputch(SS+CLOCK_CGRAM_BASE_ADDR);
;	genAssign
	mov	dptr,#_SS
	movx	a,@dptr
	mov	r3,a
;	genPlus
;     genPlusIncr
	mov	a,#0x30
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
;	genCall
	mov	r3,a
;	Peephole 244.c	loading dpl from a instead of r3
	mov	dpl,a
	push	ar2
	mov	psw,#0x00
	lcall	_lcdputch
	mov	psw,#0x08
	pop	ar2
;	main.c:1069: if(M>9)
;	genAssign
	mov	dptr,#_M
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r3,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x09
	jnc	00108$
;	Peephole 300	removed redundant label 00132$
;	main.c:1071: M=0;
;	genAssign
	mov	dptr,#_M
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1072: MM++;
;	genPlus
	mov	dptr,#_MM
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
00108$:
;	main.c:1074: lcdclockgotoaddr(0x5A);
;	genCall
	mov	dpl,#0x5A
	push	ar2
	mov	psw,#0x00
	lcall	_lcdclockgotoaddr
	mov	psw,#0x08
	pop	ar2
;	main.c:1075: lcdputch(M+CLOCK_CGRAM_BASE_ADDR);
;	genAssign
	mov	dptr,#_M
	movx	a,@dptr
	mov	r3,a
;	genPlus
;     genPlusIncr
	mov	a,#0x30
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
;	genCall
	mov	r3,a
;	Peephole 244.c	loading dpl from a instead of r3
	mov	dpl,a
	push	ar2
	mov	psw,#0x00
	lcall	_lcdputch
	mov	psw,#0x08
	pop	ar2
;	main.c:1076: lcdclockgotoaddr(0x59);
;	genCall
	mov	dpl,#0x59
	push	ar2
	mov	psw,#0x00
	lcall	_lcdclockgotoaddr
	mov	psw,#0x08
	pop	ar2
;	main.c:1077: lcdputch(MM+CLOCK_CGRAM_BASE_ADDR);
;	genAssign
	mov	dptr,#_MM
	movx	a,@dptr
	mov	r3,a
;	genPlus
;     genPlusIncr
	mov	a,#0x30
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
;	genCall
	mov	r3,a
;	Peephole 244.c	loading dpl from a instead of r3
	mov	dpl,a
	push	ar2
	mov	psw,#0x00
	lcall	_lcdputch
	mov	psw,#0x08
	pop	ar2
;	main.c:1078: lcdclockgotoaddr(0x5E);
;	genCall
	mov	dpl,#0x5E
	push	ar2
	mov	psw,#0x00
	lcall	_lcdclockgotoaddr
	mov	psw,#0x08
	pop	ar2
;	main.c:1079: lcdputch('.');
;	genCall
	mov	dpl,#0x2E
	push	ar2
	mov	psw,#0x00
	lcall	_lcdputch
	mov	psw,#0x08
	pop	ar2
;	main.c:1080: lcdclockgotoaddr(0x5B);
;	genCall
	mov	dpl,#0x5B
	push	ar2
	mov	psw,#0x00
	lcall	_lcdclockgotoaddr
	mov	psw,#0x08
	pop	ar2
;	main.c:1081: lcdputch(':');
;	genCall
	mov	dpl,#0x3A
	push	ar2
	mov	psw,#0x00
	lcall	_lcdputch
	mov	psw,#0x08
	pop	ar2
;	main.c:1082: *INST_WRITE=(interr | DDRAM_WRITE_MASK_BITS);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:1083: bb=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_timer_isr_bb_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1084: MSDelay(1);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
	mov	psw,#0x00
	lcall	_MSDelay
	mov	psw,#0x08
;	main.c:1085: while((bb&BUSYFLAG_MASK_BITS)==BUSYFLAG_MASK_BITS)
00109$:
;	genAssign
	mov	dptr,#_timer_isr_bb_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAnd
	anl	ar2,#0x80
	mov	r3,#0x00
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x80,00113$
	cjne	r3,#0x00,00113$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00133$
;	Peephole 300	removed redundant label 00134$
;	main.c:1087: bb= *INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_timer_isr_bb_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1088: MSDelay(1);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
	mov	psw,#0x00
	lcall	_MSDelay
	mov	psw,#0x08
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00109$
00113$:
;	main.c:1091: count_flag--;
;	genAssign
	mov	dptr,#_count_flag
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genMinus
;	genMinusDec
	dec	r2
	cjne	r2,#0xff,00135$
	dec	r3
00135$:
;	genAssign
	mov	dptr,#_count_flag
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
00116$:
	pop	psw
	mov	ea,c
	pop	psw
	pop	(0+1)
	pop	(0+0)
	pop	(0+7)
	pop	(0+6)
	pop	(0+5)
	pop	(0+4)
	pop	(0+3)
	pop	(0+2)
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	reti
;------------------------------------------------------------
;Allocation info for local variables in function 'clock_reset'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:1095: void clock_reset()
;	-----------------------------------------
;	 function clock_reset
;	-----------------------------------------
_clock_reset:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	main.c:1097: EA=0;
;	genAssign
	clr	_EA
;	main.c:1098: MS=0;
;	genAssign
	mov	dptr,#_MS
;	Peephole 181	changed mov to clr
;	main.c:1099: S=0; SS=0;
;	genAssign
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genAssign
;	Peephole 181	changed mov to clr
;	main.c:1100: M=0; MM=0;
;	genAssign
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#_S
	movx	@dptr,a
	mov	dptr,#_SS
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#_M
	movx	@dptr,a
;	genAssign
	mov	dptr,#_MM
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1101: lcdtime();
;	genCall
	lcall	_lcdtime
;	main.c:1102: EA=1;
;	genAssign
	setb	_EA
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'eereset'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:1108: void eereset()
;	-----------------------------------------
;	 function eereset
;	-----------------------------------------
_eereset:
;	main.c:1110: ACC=0xFF;
;	genAssign
	mov	_ACC,#0xFF
;	main.c:1111: startbit();
;	genCall
	lcall	_startbit
;	main.c:1112: send_data_assembly();
;	genCall
	lcall	_send_data_assembly
;	main.c:1113: ninthbit();
;	genCall
	lcall	_ninthbit
;	main.c:1114: startbit();
;	genCall
	lcall	_startbit
;	main.c:1115: stopbit();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_stopbit
;
;------------------------------------------------------------
;Allocation info for local variables in function 'IOexpInit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:1118: void IOexpInit()
;	-----------------------------------------
;	 function IOexpInit
;	-----------------------------------------
_IOexpInit:
;	main.c:1120: IT1=1;
;	genAssign
	setb	_IT1
;	main.c:1121: IE |=0x84;
;	genOr
	orl	_IE,#0x84
;	main.c:1122: write_ioexpander(0x01);
;	genCall
	mov	dpl,#0x01
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_write_ioexpander
;
;------------------------------------------------------------
;Allocation info for local variables in function 'ioexpander_writedata'
;------------------------------------------------------------
;ioexp_write               Allocated with name '_ioexpander_writedata_ioexp_write_1_1'
;------------------------------------------------------------
;	main.c:1125: void ioexpander_writedata()
;	-----------------------------------------
;	 function ioexpander_writedata
;	-----------------------------------------
_ioexpander_writedata:
;	main.c:1128: putstr("Enter the value to write\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_62
	mov	b,#0x80
	lcall	_putstr
;	main.c:1129: do
00103$:
;	main.c:1131: ioexp_write=ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r2,dpl
	mov	r3,dph
;	main.c:1132: if(ioexp_write>0xFF)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r2
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00111$
;	main.c:1133: putstr("Enter proper value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_63
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00104$:
;	main.c:1134: }while(ioexp_write>0xFF);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
;	main.c:1135: write_ioexpander(0x00);
;	genCall
	jnz	00103$
;	Peephole 300	removed redundant label 00112$
;	Peephole 256.c	loading dpl with zero from a
	mov	dpl,a
	push	ar2
	push	ar3
	lcall	_write_ioexpander
	pop	ar3
	pop	ar2
;	main.c:1136: write_ioexpander(ioexp_write);
;	genCast
;	genCall
	mov	dpl,r2
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_write_ioexpander
;
;------------------------------------------------------------
;Allocation info for local variables in function 'ioexpander_readdata'
;------------------------------------------------------------
;ioexp_read                Allocated with name '_ioexpander_readdata_ioexp_read_1_1'
;------------------------------------------------------------
;	main.c:1139: void ioexpander_readdata()
;	-----------------------------------------
;	 function ioexpander_readdata
;	-----------------------------------------
_ioexpander_readdata:
;	main.c:1142: write_ioexpander(0xFF);
;	genCall
	mov	dpl,#0xFF
	lcall	_write_ioexpander
;	main.c:1143: ioexp_read = read_ioexpander();
;	genCall
	lcall	_read_ioexpander
	mov	r2,dpl
;	main.c:1144: putstr("Read input value from the IO Expander is ");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_64
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:1145: printf("0x%2x\n\r", ioexp_read);
;	genCast
	mov	r3,#0x00
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_65
	push	acc
	mov	a,#(__str_65 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ioexpander_config'
;------------------------------------------------------------
;io_con                    Allocated with name '_ioexpander_config_io_con_1_1'
;------------------------------------------------------------
;	main.c:1148: void ioexpander_config()
;	-----------------------------------------
;	 function ioexpander_config
;	-----------------------------------------
_ioexpander_config:
;	main.c:1151: putstr("Press 1 for input and 0 for output. Enter eight values for eight port pins and hit enter\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_66
	mov	b,#0x80
	lcall	_putstr
;	main.c:1152: do
00103$:
;	main.c:1154: io_con=readinput();
;	genCall
	lcall	_readinput
	mov	r2,dpl
;	genCast
	mov	r3,#0x00
;	main.c:1155: if(io_con>0xFF)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r2
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00111$
;	main.c:1156: putstr("Enter proper value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_63
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00104$:
;	main.c:1157: }while(io_con>0xFF);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00112$
;	main.c:1158: write_ioexpander(io_con);
;	genCast
;	genCall
	mov	dpl,r2
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_write_ioexpander
;
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;in                        Allocated with name '_main_in_1_1'
;------------------------------------------------------------
;	main.c:1161: void main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	main.c:1164: MS=0; S=0; SS=0; M=0; MM=0;
;	genAssign
	mov	dptr,#_MS
;	Peephole 181	changed mov to clr
;	genAssign
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genAssign
;	Peephole 181	changed mov to clr
;	genAssign
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#_S
	movx	@dptr,a
	mov	dptr,#_SS
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#_M
	movx	@dptr,a
;	genAssign
	mov	dptr,#_MM
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1165: count_flag=0x02;
;	genAssign
	mov	dptr,#_count_flag
	mov	a,#0x02
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	main.c:1166: Timer0Initialize();
;	genCall
	lcall	_Timer0Initialize
;	main.c:1167: SerialInitialize();
;	genCall
	lcall	_SerialInitialize
;	main.c:1168: lcdinit();
;	genCall
	lcall	_lcdinit
;	main.c:1169: eereset();
;	genCall
	lcall	_eereset
;	main.c:1170: lcdtime();
;	genCall
	lcall	_lcdtime
;	main.c:1171: IOexpInit();
;	genCall
	lcall	_IOexpInit
;	main.c:1172: lcdgotoxy(1,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x01
	lcall	_lcdgotoxy
;	main.c:1174: lcdgotoxy(2,4);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x04
	movx	@dptr,a
;	genCall
	mov	dpl,#0x02
	lcall	_lcdgotoxy
;	main.c:1176: EA=0;
;	genAssign
	clr	_EA
;	main.c:1177: *INST_WRITE=(DDRAM_WRITE_MASK_BITS);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x80
	movx	@dptr,a
;	main.c:1178: EA=1;
;	genAssign
	setb	_EA
;	main.c:1179: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:1180: putstr("Welcome\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_67
	mov	b,#0x80
	lcall	_putstr
;	main.c:1181: putstr("Press '>' for help menu\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_68
	mov	b,#0x80
	lcall	_putstr
;	main.c:1182: putstr("Enter the character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_69
	mov	b,#0x80
	lcall	_putstr
;	main.c:1183: while(1)
00125$:
;	main.c:1185: putstr("\n\rEnter the character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_70
	mov	b,#0x80
	lcall	_putstr
;	main.c:1186: in=getchar();
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	main.c:1187: switch(in)
;	genAssign
	mov	ar3,r2
;	genCmpLt
;	genCmp
	cjne	r3,#0x30,00132$
00132$:
;	genIfxJump
	jnc	00133$
	ljmp	00122$
00133$:
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x58
	jnc	00134$
	ljmp	00122$
00134$:
;	genMinus
	mov	a,r2
	add	a,#0xd0
;	genJumpTab
	mov	r2,a
;	Peephole 105	removed redundant mov
	add	a,#(00135$-3-.)
	movc	a,@a+pc
	push	acc
	mov	a,r2
	add	a,#(00136$-3-.)
	movc	a,@a+pc
	push	acc
	ret
00135$:
	.db	00101$
	.db	00102$
	.db	00103$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00107$
	.db	00122$
	.db	00122$
	.db	00122$
	.db	00118$
	.db	00110$
	.db	00111$
	.db	00112$
	.db	00119$
	.db	00109$
	.db	00120$
	.db	00115$
	.db	00121$
	.db	00116$
	.db	00122$
	.db	00122$
	.db	00113$
	.db	00122$
	.db	00114$
	.db	00122$
	.db	00105$
	.db	00106$
	.db	00117$
	.db	00122$
	.db	00122$
	.db	00104$
	.db	00108$
00136$:
	.db	00101$>>8
	.db	00102$>>8
	.db	00103$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00107$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00118$>>8
	.db	00110$>>8
	.db	00111$>>8
	.db	00112$>>8
	.db	00119$>>8
	.db	00109$>>8
	.db	00120$>>8
	.db	00115$>>8
	.db	00121$>>8
	.db	00116$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00113$>>8
	.db	00122$>>8
	.db	00114$>>8
	.db	00122$>>8
	.db	00105$>>8
	.db	00106$>>8
	.db	00117$>>8
	.db	00122$>>8
	.db	00122$>>8
	.db	00104$>>8
	.db	00108$>>8
;	main.c:1189: case '0' : putstr("The clock is paused\n\r");
00101$:
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_71
	mov	b,#0x80
	lcall	_putstr
;	main.c:1190: CLOCK_ENABLE_FLAG=0;
;	genAssign
	mov	dptr,#_CLOCK_ENABLE_FLAG
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1191: break;
	ljmp	00125$
;	main.c:1192: case '1' : putstr("The clock is resuming\n\r");
00102$:
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_72
	mov	b,#0x80
	lcall	_putstr
;	main.c:1193: CLOCK_ENABLE_FLAG=1;
;	genAssign
	mov	dptr,#_CLOCK_ENABLE_FLAG
	mov	a,#0x01
	movx	@dptr,a
;	main.c:1194: break;
	ljmp	00125$
;	main.c:1195: case '2' : putstr("Clock is reset to zero\n\r");
00103$:
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_73
	mov	b,#0x80
	lcall	_putstr
;	main.c:1196: clock_reset();
;	genCall
	lcall	_clock_reset
;	main.c:1197: break;
	ljmp	00125$
;	main.c:1198: case 'W' : write_function();
00104$:
;	genCall
	lcall	_write_function
;	main.c:1199: break;
	ljmp	00125$
;	main.c:1200: case 'R' : read_function();
00105$:
;	genCall
	lcall	_read_function
;	main.c:1201: break;
	ljmp	00125$
;	main.c:1202: case 'S' : readall_function();
00106$:
;	genCall
	lcall	_readall_function
;	main.c:1203: break;
	ljmp	00125$
;	main.c:1204: case '>' : help_menu();
00107$:
;	genCall
	lcall	_help_menu
;	main.c:1205: break;
	ljmp	00125$
;	main.c:1206: case 'X' : gotoxy();
00108$:
;	genCall
	lcall	_gotoxy
;	main.c:1207: break;
	ljmp	00125$
;	main.c:1208: case 'G' : gotoaddress();
00109$:
;	genCall
	lcall	_gotoaddress
;	main.c:1209: break;
	ljmp	00125$
;	main.c:1210: case 'C' : cgramdump();
00110$:
;	genCall
	lcall	_cgramdump
;	main.c:1211: break;
	ljmp	00125$
;	main.c:1212: case 'D' : ddramdump();
00111$:
;	genCall
	lcall	_ddramdump
;	main.c:1213: break;
	ljmp	00125$
;	main.c:1214: case 'E' : lcdclear();
00112$:
;	genCall
	lcall	_lcdclear
;	main.c:1215: break;
	ljmp	00125$
;	main.c:1216: case 'N' : addcharacter();
00113$:
;	genCall
	lcall	_addcharacter
;	main.c:1217: break;
	ljmp	00125$
;	main.c:1218: case 'P' : display();
00114$:
;	genCall
	lcall	_display
;	main.c:1219: break;
	ljmp	00125$
;	main.c:1220: case 'I' : interactive();
00115$:
;	genCall
	lcall	_interactive
;	main.c:1221: break;
	ljmp	00125$
;	main.c:1222: case 'K' :  u=0;
00116$:
;	genAssign
	mov	dptr,#_u
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:1223: custom_character();
;	genCall
	lcall	_custom_character
;	main.c:1224: break;
	ljmp	00125$
;	main.c:1225: case 'T' : eereset();
00117$:
;	genCall
	lcall	_eereset
;	main.c:1226: break;
	ljmp	00125$
;	main.c:1227: case 'B' : ioexpander_writedata();
00118$:
;	genCall
	lcall	_ioexpander_writedata
;	main.c:1228: break;
	ljmp	00125$
;	main.c:1229: case 'F' : ioexpander_readdata();
00119$:
;	genCall
	lcall	_ioexpander_readdata
;	main.c:1230: break;
	ljmp	00125$
;	main.c:1231: case 'H' : ioexpander_config();
00120$:
;	genCall
	lcall	_ioexpander_config
;	main.c:1232: break;
	ljmp	00125$
;	main.c:1233: case 'J' : IOexpInit();
00121$:
;	genCall
	lcall	_IOexpInit
;	main.c:1234: break;
	ljmp	00125$
;	main.c:1235: default: putstr("Enter the correct option\n\r");
00122$:
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_74
	mov	b,#0x80
	lcall	_putstr
;	main.c:1236: }
	ljmp	00125$
;	Peephole 259.b	removed redundant label 00127$ and ret
;
	.area CSEG    (CODE)
	.area CONST   (CODE)
__str_0:
	.db 0x0A
	.db 0x0D
	.ascii "Enter Numbers"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_1:
	.db 0x0A
	.db 0x0D
	.ascii "Can't move to given position"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_2:
	.ascii "Clearing the LCD completed"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_3:
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_4:
	.ascii "CGRAM values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_5:
	.db 0x0A
	.db 0x0D
	.ascii "CCODE |  Values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_6:
	.ascii "0x%04x:  "
	.db 0x00
__str_7:
	.ascii "%02x  "
	.db 0x00
__str_8:
	.ascii "DDRAM Values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_9:
	.db 0x0A
	.db 0x0D
	.ascii "ADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C "
	.ascii " +D  +E  +F"
	.db 0x0A
	.db 0x0D
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_10:
	.ascii "0x%02x:  "
	.db 0x00
__str_11:
	.db 0x0A
	.db 0x0D
	.ascii "Enter Valid Numbers"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_12:
	.db 0x0A
	.db 0x0D
	.ascii "Enter eight pixel values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_13:
	.ascii "Enter five binary values for every pixel"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_14:
	.ascii "Invalid Number. Enter Again"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_15:
	.ascii "Eight custom character created. No more custom character ple"
	.ascii "ase"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_16:
	.ascii "Look at the LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_18:
	.ascii "Enter the character to display. Hit enter to stop"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_19:
	.db 0x0A
	.db 0x0D
	.ascii "Enter row number"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_20:
	.db 0x0A
	.db 0x0D
	.ascii "Enter correct row"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_21:
	.db 0x0A
	.db 0x0D
	.ascii "Enter column number"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_22:
	.db 0x0A
	.db 0x0D
	.ascii "Enter correct column number"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_23:
	.ascii "Enter the address in hex"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_24:
	.db 0x0A
	.db 0x0D
	.ascii "Enter correct address values in hex"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_25:
	.db 0x0A
	.db 0x0D
	.ascii "Writing 0x%03X: 0x%02X"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_26:
	.db 0x0A
	.db 0x0D
	.ascii "0x%03X: 0x%02X"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_27:
	.ascii "Enter the address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_28:
	.ascii "Enter the correct value"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_29:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the second higher address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_30:
	.ascii "EEPROM data"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_31:
	.ascii "ADDR |  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  "
	.ascii "+D  +E  +F"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_32:
	.ascii "0x%03X: "
	.db 0x00
__str_33:
	.ascii "%02X  "
	.db 0x00
__str_34:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the Value"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_35:
	.db 0x0A
	.db 0x0D
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_36:
	.ascii "EEPROM Control"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_37:
	.ascii "Press 'R' to read a byte from the user input address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_38:
	.ascii "Press 'W' to write a byte data to user input address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_39:
	.ascii "Press 'S' for sequential read between the adresses"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_40:
	.ascii "Press 'T' for EEPROM software reset"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_41:
	.ascii "LCD Control"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_42:
	.ascii "Press 'C' for CGRAM HEX dump"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_43:
	.ascii "Press 'D' for DDRAM HEX dump"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_44:
	.ascii "Press 'E' for Clearing the LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_45:
	.ascii "Press 'P' to display the custom character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_46:
	.ascii "Press 'N' to add custom character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_47:
	.ascii "Press 'I' for printing letters on LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_48:
	.ascii "Press 'K' for displaying the logo"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_49:
	.ascii "Press 'X' for moving the cursor to user input x,y position"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_50:
	.ascii "Press 'G' for moving the cursor to user input address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_51:
	.ascii "Clock Control"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_52:
	.ascii "Press '0' for pausing the clock"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_53:
	.ascii "Press '1' to resume the clock "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_54:
	.ascii "Press '2' to reset the clock to zero"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_55:
	.ascii "IO Expander control"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_56:
	.ascii "Press 'B' for writing data to IO Expander"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_57:
	.ascii "Press 'F' to read data from IO Expander"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_58:
	.ascii "Press 'H' for configuring the individual GPIO pin"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_59:
	.ascii "Press 'J' for configuring IO expander's one pin as input and"
	.ascii " one as output"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_60:
	.ascii "Look at LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_61:
	.ascii "00:00.0"
	.db 0x00
__str_62:
	.ascii "Enter the value to write"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_63:
	.ascii "Enter proper value"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_64:
	.ascii "Read input value from the IO Expander is "
	.db 0x00
__str_65:
	.ascii "0x%2x"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_66:
	.ascii "Press 1 for input and 0 for output. Enter eight values for e"
	.ascii "ight port pins and hit enter"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_67:
	.ascii "Welcome"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_68:
	.ascii "Press '>' for help menu"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_69:
	.ascii "Enter the character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_70:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_71:
	.ascii "The clock is paused"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_72:
	.ascii "The clock is resuming"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_73:
	.ascii "Clock is reset to zero"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_74:
	.ascii "Enter the correct option"
	.db 0x0A
	.db 0x0D
	.db 0x00
	.area XINIT   (CODE)
__xinit__CLOCK_ENABLE_FLAG:
	.db #0x01
__xinit__confirm:
	.byte #0x01,#0x00
