                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : FreeWare ANSI-C Compiler
                              3 ; Version 2.6.0 #4309 (Jul 28 2006)
                              4 ; This file generated Fri Nov 17 00:59:16 2017
                              5 ;--------------------------------------------------------
                              6 	.module main
                              7 	.optsdcc -mmcs51 --model-large
                              8 	
                              9 ;--------------------------------------------------------
                             10 ; Public variables in this module
                             11 ;--------------------------------------------------------
                             12 	.globl _main
                             13 	.globl _ioexpander_config
                             14 	.globl _ioexpander_readdata
                             15 	.globl _ioexpander_writedata
                             16 	.globl _IOexpInit
                             17 	.globl _eereset
                             18 	.globl _clock_reset
                             19 	.globl _timer_isr
                             20 	.globl _external_isr
                             21 	.globl _write_ioexpander
                             22 	.globl _invert_ioexpander
                             23 	.globl _read_ioexpander
                             24 	.globl _lcdtime
                             25 	.globl _Timer0Initialize
                             26 	.globl _custom_character
                             27 	.globl _read_function
                             28 	.globl _help_menu
                             29 	.globl _write_function
                             30 	.globl _readall_function
                             31 	.globl _eebyter
                             32 	.globl _eebytew
                             33 	.globl _clock_func
                             34 	.globl _ninthbit
                             35 	.globl _send_data
                             36 	.globl _send_data_assembly
                             37 	.globl _Master_ack
                             38 	.globl _ackbit
                             39 	.globl _stopbit
                             40 	.globl _startbit
                             41 	.globl _SerialInitialize
                             42 	.globl _delay
                             43 	.globl __sdcc_external_startup
                             44 	.globl _gotoaddress
                             45 	.globl _gotoxy
                             46 	.globl _interactive
                             47 	.globl _ReadVariable
                             48 	.globl _hex2int
                             49 	.globl _display
                             50 	.globl _addcharacter
                             51 	.globl _readinput
                             52 	.globl _ddramdump
                             53 	.globl _lcdcreatechar
                             54 	.globl _cgramdump
                             55 	.globl _lcdinit
                             56 	.globl _lcdputstr
                             57 	.globl _lcdputch
                             58 	.globl _lcdclear
                             59 	.globl _lcdgotoxy
                             60 	.globl _lcdgotoaddr
                             61 	.globl _lcdclockgotoaddr
                             62 	.globl _input_value
                             63 	.globl _lcdbusywait
                             64 	.globl _putstr
                             65 	.globl _MSDelay
                             66 	.globl _P5_7
                             67 	.globl _P5_6
                             68 	.globl _P5_5
                             69 	.globl _P5_4
                             70 	.globl _P5_3
                             71 	.globl _P5_2
                             72 	.globl _P5_1
                             73 	.globl _P5_0
                             74 	.globl _P4_7
                             75 	.globl _P4_6
                             76 	.globl _P4_5
                             77 	.globl _P4_4
                             78 	.globl _P4_3
                             79 	.globl _P4_2
                             80 	.globl _P4_1
                             81 	.globl _P4_0
                             82 	.globl _PX0L
                             83 	.globl _PT0L
                             84 	.globl _PX1L
                             85 	.globl _PT1L
                             86 	.globl _PLS
                             87 	.globl _PT2L
                             88 	.globl _PPCL
                             89 	.globl _EC
                             90 	.globl _CCF0
                             91 	.globl _CCF1
                             92 	.globl _CCF2
                             93 	.globl _CCF3
                             94 	.globl _CCF4
                             95 	.globl _CR
                             96 	.globl _CF
                             97 	.globl _TF2
                             98 	.globl _EXF2
                             99 	.globl _RCLK
                            100 	.globl _TCLK
                            101 	.globl _EXEN2
                            102 	.globl _TR2
                            103 	.globl _C_T2
                            104 	.globl _CP_RL2
                            105 	.globl _T2CON_7
                            106 	.globl _T2CON_6
                            107 	.globl _T2CON_5
                            108 	.globl _T2CON_4
                            109 	.globl _T2CON_3
                            110 	.globl _T2CON_2
                            111 	.globl _T2CON_1
                            112 	.globl _T2CON_0
                            113 	.globl _PT2
                            114 	.globl _ET2
                            115 	.globl _CY
                            116 	.globl _AC
                            117 	.globl _F0
                            118 	.globl _RS1
                            119 	.globl _RS0
                            120 	.globl _OV
                            121 	.globl _F1
                            122 	.globl _P
                            123 	.globl _PS
                            124 	.globl _PT1
                            125 	.globl _PX1
                            126 	.globl _PT0
                            127 	.globl _PX0
                            128 	.globl _RD
                            129 	.globl _WR
                            130 	.globl _T1
                            131 	.globl _T0
                            132 	.globl _INT1
                            133 	.globl _INT0
                            134 	.globl _TXD
                            135 	.globl _RXD
                            136 	.globl _P3_7
                            137 	.globl _P3_6
                            138 	.globl _P3_5
                            139 	.globl _P3_4
                            140 	.globl _P3_3
                            141 	.globl _P3_2
                            142 	.globl _P3_1
                            143 	.globl _P3_0
                            144 	.globl _EA
                            145 	.globl _ES
                            146 	.globl _ET1
                            147 	.globl _EX1
                            148 	.globl _ET0
                            149 	.globl _EX0
                            150 	.globl _P2_7
                            151 	.globl _P2_6
                            152 	.globl _P2_5
                            153 	.globl _P2_4
                            154 	.globl _P2_3
                            155 	.globl _P2_2
                            156 	.globl _P2_1
                            157 	.globl _P2_0
                            158 	.globl _SM0
                            159 	.globl _SM1
                            160 	.globl _SM2
                            161 	.globl _REN
                            162 	.globl _TB8
                            163 	.globl _RB8
                            164 	.globl _TI
                            165 	.globl _RI
                            166 	.globl _P1_7
                            167 	.globl _P1_6
                            168 	.globl _P1_5
                            169 	.globl _P1_4
                            170 	.globl _P1_3
                            171 	.globl _P1_2
                            172 	.globl _P1_1
                            173 	.globl _P1_0
                            174 	.globl _TF1
                            175 	.globl _TR1
                            176 	.globl _TF0
                            177 	.globl _TR0
                            178 	.globl _IE1
                            179 	.globl _IT1
                            180 	.globl _IE0
                            181 	.globl _IT0
                            182 	.globl _P0_7
                            183 	.globl _P0_6
                            184 	.globl _P0_5
                            185 	.globl _P0_4
                            186 	.globl _P0_3
                            187 	.globl _P0_2
                            188 	.globl _P0_1
                            189 	.globl _P0_0
                            190 	.globl _EECON
                            191 	.globl _KBF
                            192 	.globl _KBE
                            193 	.globl _KBLS
                            194 	.globl _BRL
                            195 	.globl _BDRCON
                            196 	.globl _T2MOD
                            197 	.globl _SPDAT
                            198 	.globl _SPSTA
                            199 	.globl _SPCON
                            200 	.globl _SADEN
                            201 	.globl _SADDR
                            202 	.globl _WDTPRG
                            203 	.globl _WDTRST
                            204 	.globl _P5
                            205 	.globl _P4
                            206 	.globl _IPH1
                            207 	.globl _IPL1
                            208 	.globl _IPH0
                            209 	.globl _IPL0
                            210 	.globl _IEN1
                            211 	.globl _IEN0
                            212 	.globl _CMOD
                            213 	.globl _CL
                            214 	.globl _CH
                            215 	.globl _CCON
                            216 	.globl _CCAPM4
                            217 	.globl _CCAPM3
                            218 	.globl _CCAPM2
                            219 	.globl _CCAPM1
                            220 	.globl _CCAPM0
                            221 	.globl _CCAP4L
                            222 	.globl _CCAP3L
                            223 	.globl _CCAP2L
                            224 	.globl _CCAP1L
                            225 	.globl _CCAP0L
                            226 	.globl _CCAP4H
                            227 	.globl _CCAP3H
                            228 	.globl _CCAP2H
                            229 	.globl _CCAP1H
                            230 	.globl _CCAP0H
                            231 	.globl _CKCKON1
                            232 	.globl _CKCKON0
                            233 	.globl _CKRL
                            234 	.globl _AUXR1
                            235 	.globl _AUXR
                            236 	.globl _TH2
                            237 	.globl _TL2
                            238 	.globl _RCAP2H
                            239 	.globl _RCAP2L
                            240 	.globl _T2CON
                            241 	.globl _B
                            242 	.globl _ACC
                            243 	.globl _PSW
                            244 	.globl _IP
                            245 	.globl _P3
                            246 	.globl _IE
                            247 	.globl _P2
                            248 	.globl _SBUF
                            249 	.globl _SCON
                            250 	.globl _P1
                            251 	.globl _TH1
                            252 	.globl _TH0
                            253 	.globl _TL1
                            254 	.globl _TL0
                            255 	.globl _TMOD
                            256 	.globl _TCON
                            257 	.globl _PCON
                            258 	.globl _DPH
                            259 	.globl _DPL
                            260 	.globl _SP
                            261 	.globl _P0
                            262 	.globl _confirm
                            263 	.globl _CLOCK_ENABLE_FLAG
                            264 	.globl _eebytew_PARM_2
                            265 	.globl _hex2int_PARM_2
                            266 	.globl _lcdcreatechar_PARM_2
                            267 	.globl _lcdgotoxy_PARM_2
                            268 	.globl _count_flag
                            269 	.globl _MM
                            270 	.globl _M
                            271 	.globl _SS
                            272 	.globl _S
                            273 	.globl _MS
                            274 	.globl _u
                            275 	.globl _array
                            276 	.globl _t
                            277 	.globl _putchar
                            278 	.globl _getchar
                            279 ;--------------------------------------------------------
                            280 ; special function registers
                            281 ;--------------------------------------------------------
                            282 	.area RSEG    (DATA)
                    0080    283 _P0	=	0x0080
                    0081    284 _SP	=	0x0081
                    0082    285 _DPL	=	0x0082
                    0083    286 _DPH	=	0x0083
                    0087    287 _PCON	=	0x0087
                    0088    288 _TCON	=	0x0088
                    0089    289 _TMOD	=	0x0089
                    008A    290 _TL0	=	0x008a
                    008B    291 _TL1	=	0x008b
                    008C    292 _TH0	=	0x008c
                    008D    293 _TH1	=	0x008d
                    0090    294 _P1	=	0x0090
                    0098    295 _SCON	=	0x0098
                    0099    296 _SBUF	=	0x0099
                    00A0    297 _P2	=	0x00a0
                    00A8    298 _IE	=	0x00a8
                    00B0    299 _P3	=	0x00b0
                    00B8    300 _IP	=	0x00b8
                    00D0    301 _PSW	=	0x00d0
                    00E0    302 _ACC	=	0x00e0
                    00F0    303 _B	=	0x00f0
                    00C8    304 _T2CON	=	0x00c8
                    00CA    305 _RCAP2L	=	0x00ca
                    00CB    306 _RCAP2H	=	0x00cb
                    00CC    307 _TL2	=	0x00cc
                    00CD    308 _TH2	=	0x00cd
                    008E    309 _AUXR	=	0x008e
                    00A2    310 _AUXR1	=	0x00a2
                    0097    311 _CKRL	=	0x0097
                    008F    312 _CKCKON0	=	0x008f
                    008F    313 _CKCKON1	=	0x008f
                    00FA    314 _CCAP0H	=	0x00fa
                    00FB    315 _CCAP1H	=	0x00fb
                    00FC    316 _CCAP2H	=	0x00fc
                    00FD    317 _CCAP3H	=	0x00fd
                    00FE    318 _CCAP4H	=	0x00fe
                    00EA    319 _CCAP0L	=	0x00ea
                    00EB    320 _CCAP1L	=	0x00eb
                    00EC    321 _CCAP2L	=	0x00ec
                    00ED    322 _CCAP3L	=	0x00ed
                    00EE    323 _CCAP4L	=	0x00ee
                    00DA    324 _CCAPM0	=	0x00da
                    00DB    325 _CCAPM1	=	0x00db
                    00DC    326 _CCAPM2	=	0x00dc
                    00DD    327 _CCAPM3	=	0x00dd
                    00DE    328 _CCAPM4	=	0x00de
                    00D8    329 _CCON	=	0x00d8
                    00F9    330 _CH	=	0x00f9
                    00E9    331 _CL	=	0x00e9
                    00D9    332 _CMOD	=	0x00d9
                    00A8    333 _IEN0	=	0x00a8
                    00B1    334 _IEN1	=	0x00b1
                    00B8    335 _IPL0	=	0x00b8
                    00B7    336 _IPH0	=	0x00b7
                    00B2    337 _IPL1	=	0x00b2
                    00B3    338 _IPH1	=	0x00b3
                    00C0    339 _P4	=	0x00c0
                    00D8    340 _P5	=	0x00d8
                    00A6    341 _WDTRST	=	0x00a6
                    00A7    342 _WDTPRG	=	0x00a7
                    00A9    343 _SADDR	=	0x00a9
                    00B9    344 _SADEN	=	0x00b9
                    00C3    345 _SPCON	=	0x00c3
                    00C4    346 _SPSTA	=	0x00c4
                    00C5    347 _SPDAT	=	0x00c5
                    00C9    348 _T2MOD	=	0x00c9
                    009B    349 _BDRCON	=	0x009b
                    009A    350 _BRL	=	0x009a
                    009C    351 _KBLS	=	0x009c
                    009D    352 _KBE	=	0x009d
                    009E    353 _KBF	=	0x009e
                    00D2    354 _EECON	=	0x00d2
                            355 ;--------------------------------------------------------
                            356 ; special function bits
                            357 ;--------------------------------------------------------
                            358 	.area RSEG    (DATA)
                    0080    359 _P0_0	=	0x0080
                    0081    360 _P0_1	=	0x0081
                    0082    361 _P0_2	=	0x0082
                    0083    362 _P0_3	=	0x0083
                    0084    363 _P0_4	=	0x0084
                    0085    364 _P0_5	=	0x0085
                    0086    365 _P0_6	=	0x0086
                    0087    366 _P0_7	=	0x0087
                    0088    367 _IT0	=	0x0088
                    0089    368 _IE0	=	0x0089
                    008A    369 _IT1	=	0x008a
                    008B    370 _IE1	=	0x008b
                    008C    371 _TR0	=	0x008c
                    008D    372 _TF0	=	0x008d
                    008E    373 _TR1	=	0x008e
                    008F    374 _TF1	=	0x008f
                    0090    375 _P1_0	=	0x0090
                    0091    376 _P1_1	=	0x0091
                    0092    377 _P1_2	=	0x0092
                    0093    378 _P1_3	=	0x0093
                    0094    379 _P1_4	=	0x0094
                    0095    380 _P1_5	=	0x0095
                    0096    381 _P1_6	=	0x0096
                    0097    382 _P1_7	=	0x0097
                    0098    383 _RI	=	0x0098
                    0099    384 _TI	=	0x0099
                    009A    385 _RB8	=	0x009a
                    009B    386 _TB8	=	0x009b
                    009C    387 _REN	=	0x009c
                    009D    388 _SM2	=	0x009d
                    009E    389 _SM1	=	0x009e
                    009F    390 _SM0	=	0x009f
                    00A0    391 _P2_0	=	0x00a0
                    00A1    392 _P2_1	=	0x00a1
                    00A2    393 _P2_2	=	0x00a2
                    00A3    394 _P2_3	=	0x00a3
                    00A4    395 _P2_4	=	0x00a4
                    00A5    396 _P2_5	=	0x00a5
                    00A6    397 _P2_6	=	0x00a6
                    00A7    398 _P2_7	=	0x00a7
                    00A8    399 _EX0	=	0x00a8
                    00A9    400 _ET0	=	0x00a9
                    00AA    401 _EX1	=	0x00aa
                    00AB    402 _ET1	=	0x00ab
                    00AC    403 _ES	=	0x00ac
                    00AF    404 _EA	=	0x00af
                    00B0    405 _P3_0	=	0x00b0
                    00B1    406 _P3_1	=	0x00b1
                    00B2    407 _P3_2	=	0x00b2
                    00B3    408 _P3_3	=	0x00b3
                    00B4    409 _P3_4	=	0x00b4
                    00B5    410 _P3_5	=	0x00b5
                    00B6    411 _P3_6	=	0x00b6
                    00B7    412 _P3_7	=	0x00b7
                    00B0    413 _RXD	=	0x00b0
                    00B1    414 _TXD	=	0x00b1
                    00B2    415 _INT0	=	0x00b2
                    00B3    416 _INT1	=	0x00b3
                    00B4    417 _T0	=	0x00b4
                    00B5    418 _T1	=	0x00b5
                    00B6    419 _WR	=	0x00b6
                    00B7    420 _RD	=	0x00b7
                    00B8    421 _PX0	=	0x00b8
                    00B9    422 _PT0	=	0x00b9
                    00BA    423 _PX1	=	0x00ba
                    00BB    424 _PT1	=	0x00bb
                    00BC    425 _PS	=	0x00bc
                    00D0    426 _P	=	0x00d0
                    00D1    427 _F1	=	0x00d1
                    00D2    428 _OV	=	0x00d2
                    00D3    429 _RS0	=	0x00d3
                    00D4    430 _RS1	=	0x00d4
                    00D5    431 _F0	=	0x00d5
                    00D6    432 _AC	=	0x00d6
                    00D7    433 _CY	=	0x00d7
                    00AD    434 _ET2	=	0x00ad
                    00BD    435 _PT2	=	0x00bd
                    00C8    436 _T2CON_0	=	0x00c8
                    00C9    437 _T2CON_1	=	0x00c9
                    00CA    438 _T2CON_2	=	0x00ca
                    00CB    439 _T2CON_3	=	0x00cb
                    00CC    440 _T2CON_4	=	0x00cc
                    00CD    441 _T2CON_5	=	0x00cd
                    00CE    442 _T2CON_6	=	0x00ce
                    00CF    443 _T2CON_7	=	0x00cf
                    00C8    444 _CP_RL2	=	0x00c8
                    00C9    445 _C_T2	=	0x00c9
                    00CA    446 _TR2	=	0x00ca
                    00CB    447 _EXEN2	=	0x00cb
                    00CC    448 _TCLK	=	0x00cc
                    00CD    449 _RCLK	=	0x00cd
                    00CE    450 _EXF2	=	0x00ce
                    00CF    451 _TF2	=	0x00cf
                    00DF    452 _CF	=	0x00df
                    00DE    453 _CR	=	0x00de
                    00DC    454 _CCF4	=	0x00dc
                    00DB    455 _CCF3	=	0x00db
                    00DA    456 _CCF2	=	0x00da
                    00D9    457 _CCF1	=	0x00d9
                    00D8    458 _CCF0	=	0x00d8
                    00AE    459 _EC	=	0x00ae
                    00BE    460 _PPCL	=	0x00be
                    00BD    461 _PT2L	=	0x00bd
                    00BC    462 _PLS	=	0x00bc
                    00BB    463 _PT1L	=	0x00bb
                    00BA    464 _PX1L	=	0x00ba
                    00B9    465 _PT0L	=	0x00b9
                    00B8    466 _PX0L	=	0x00b8
                    00C0    467 _P4_0	=	0x00c0
                    00C1    468 _P4_1	=	0x00c1
                    00C2    469 _P4_2	=	0x00c2
                    00C3    470 _P4_3	=	0x00c3
                    00C4    471 _P4_4	=	0x00c4
                    00C5    472 _P4_5	=	0x00c5
                    00C6    473 _P4_6	=	0x00c6
                    00C7    474 _P4_7	=	0x00c7
                    00D8    475 _P5_0	=	0x00d8
                    00D9    476 _P5_1	=	0x00d9
                    00DA    477 _P5_2	=	0x00da
                    00DB    478 _P5_3	=	0x00db
                    00DC    479 _P5_4	=	0x00dc
                    00DD    480 _P5_5	=	0x00dd
                    00DE    481 _P5_6	=	0x00de
                    00DF    482 _P5_7	=	0x00df
                            483 ;--------------------------------------------------------
                            484 ; overlayable register banks
                            485 ;--------------------------------------------------------
                            486 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     487 	.ds 8
                            488 	.area REG_BANK_1	(REL,OVR,DATA)
   0008                     489 	.ds 8
                            490 	.area REG_BANK_2	(REL,OVR,DATA)
   0010                     491 	.ds 8
                            492 ;--------------------------------------------------------
                            493 ; internal ram data
                            494 ;--------------------------------------------------------
                            495 	.area DSEG    (DATA)
   0022                     496 _lcdcreatechar_sloc0_1_0:
   0022                     497 	.ds 2
   0024                     498 _hex2int_sloc0_1_0:
   0024                     499 	.ds 1
   0025                     500 _hex2int_sloc1_1_0:
   0025                     501 	.ds 2
   0027                     502 _hex2int_sloc2_1_0:
   0027                     503 	.ds 4
   002B                     504 _hex2int_sloc3_1_0:
   002B                     505 	.ds 4
                            506 ;--------------------------------------------------------
                            507 ; overlayable items in internal ram 
                            508 ;--------------------------------------------------------
                            509 	.area OSEG    (OVR,DATA)
                            510 ;--------------------------------------------------------
                            511 ; Stack segment in internal ram 
                            512 ;--------------------------------------------------------
                            513 	.area	SSEG	(DATA)
   002F                     514 __start__stack:
   002F                     515 	.ds	1
                            516 
                            517 ;--------------------------------------------------------
                            518 ; indirectly addressable internal ram data
                            519 ;--------------------------------------------------------
                            520 	.area ISEG    (DATA)
                            521 ;--------------------------------------------------------
                            522 ; bit data
                            523 ;--------------------------------------------------------
                            524 	.area BSEG    (BIT)
                            525 ;--------------------------------------------------------
                            526 ; paged external ram data
                            527 ;--------------------------------------------------------
                            528 	.area PSEG    (PAG,XDATA)
                            529 ;--------------------------------------------------------
                            530 ; external ram data
                            531 ;--------------------------------------------------------
                            532 	.area XSEG    (XDATA)
   0000                     533 _t::
   0000                     534 	.ds 2
   0002                     535 _array::
   0002                     536 	.ds 8
   000A                     537 _u::
   000A                     538 	.ds 1
   000B                     539 _MS::
   000B                     540 	.ds 1
   000C                     541 _S::
   000C                     542 	.ds 1
   000D                     543 _SS::
   000D                     544 	.ds 1
   000E                     545 _M::
   000E                     546 	.ds 1
   000F                     547 _MM::
   000F                     548 	.ds 1
   0010                     549 _count_flag::
   0010                     550 	.ds 2
   0012                     551 _MSDelay_value_1_1:
   0012                     552 	.ds 2
   0014                     553 _putstr_ptr_1_1:
   0014                     554 	.ds 3
   0017                     555 _lcdbusywait_i_1_1:
   0017                     556 	.ds 2
   0019                     557 _input_value_l_1_1:
   0019                     558 	.ds 1
   001A                     559 _input_value_m_1_1:
   001A                     560 	.ds 1
   001B                     561 _lcdclockgotoaddr_addr_1_1:
   001B                     562 	.ds 1
   001C                     563 _lcdgotoaddr_addr_1_1:
   001C                     564 	.ds 1
   001D                     565 _lcdgotoxy_PARM_2:
   001D                     566 	.ds 1
   001E                     567 _lcdgotoxy_row_1_1:
   001E                     568 	.ds 1
   001F                     569 _lcdputch_cc_1_1:
   001F                     570 	.ds 1
   0020                     571 _lcdputch_aa_1_1:
   0020                     572 	.ds 2
   0022                     573 _lcdputch_tempput_1_1:
   0022                     574 	.ds 2
   0024                     575 _lcdputstr_ptr_1_1:
   0024                     576 	.ds 3
   0027                     577 _cgramdump_temp_val_1_1:
   0027                     578 	.ds 1
   0028                     579 _lcdcreatechar_PARM_2:
   0028                     580 	.ds 3
   002B                     581 _lcdcreatechar_ccode_1_1:
   002B                     582 	.ds 1
   002C                     583 _ddramdump_temp_val_1_1:
   002C                     584 	.ds 1
   002D                     585 _readinput_value_1_1:
   002D                     586 	.ds 1
   002E                     587 _addcharacter_temp_1_1:
   002E                     588 	.ds 1
   002F                     589 _hex2int_PARM_2:
   002F                     590 	.ds 2
   0031                     591 _hex2int_a_1_1:
   0031                     592 	.ds 3
   0034                     593 _hex2int_val_1_1:
   0034                     594 	.ds 4
   0038                     595 _ReadVariable_k_1_1:
   0038                     596 	.ds 2
   003A                     597 _ReadVariable_ptr_1_1:
   003A                     598 	.ds 1
   003B                     599 _gotoaddress_temp_1_1:
   003B                     600 	.ds 1
   003C                     601 _putchar_x_1_1:
   003C                     602 	.ds 1
   003D                     603 _send_data_value_1_1:
   003D                     604 	.ds 1
   003E                     605 _eebytew_PARM_2:
   003E                     606 	.ds 1
   003F                     607 _eebytew_addr_1_1:
   003F                     608 	.ds 2
   0041                     609 _eebyter_addr_1_1:
   0041                     610 	.ds 2
   0043                     611 _eebyter_backup_1_1:
   0043                     612 	.ds 1
   0044                     613 _readall_function_backup_1_1:
   0044                     614 	.ds 1
   0045                     615 _readall_function_tempvalue_1_1:
   0045                     616 	.ds 2
   0047                     617 _custom_character_in_array_1_1:
   0047                     618 	.ds 8
   004F                     619 _custom_character_in_array5_1_1:
   004F                     620 	.ds 8
   0057                     621 _custom_character_in_array1_1_1:
   0057                     622 	.ds 8
   005F                     623 _custom_character_in_array2_1_1:
   005F                     624 	.ds 8
   0067                     625 _custom_character_in_array3_1_1:
   0067                     626 	.ds 8
   006F                     627 _custom_character_in_array4_1_1:
   006F                     628 	.ds 8
   0077                     629 _custom_character_in_array6_1_1:
   0077                     630 	.ds 8
   007F                     631 _custom_character_in_array7_1_1:
   007F                     632 	.ds 8
   0087                     633 _read_ioexpander_io_tem_1_1:
   0087                     634 	.ds 1
   0088                     635 _invert_ioexpander_value_io_1_1:
   0088                     636 	.ds 1
   0089                     637 _write_ioexpander_send_bits_1_1:
   0089                     638 	.ds 1
   008A                     639 _timer_isr_bb_1_1:
   008A                     640 	.ds 2
                            641 ;--------------------------------------------------------
                            642 ; external initialized ram data
                            643 ;--------------------------------------------------------
                            644 	.area XISEG   (XDATA)
   00BC                     645 _CLOCK_ENABLE_FLAG::
   00BC                     646 	.ds 1
   00BD                     647 _confirm::
   00BD                     648 	.ds 2
                            649 	.area HOME    (CODE)
                            650 	.area GSINIT0 (CODE)
                            651 	.area GSINIT1 (CODE)
                            652 	.area GSINIT2 (CODE)
                            653 	.area GSINIT3 (CODE)
                            654 	.area GSINIT4 (CODE)
                            655 	.area GSINIT5 (CODE)
                            656 	.area GSINIT  (CODE)
                            657 	.area GSFINAL (CODE)
                            658 	.area CSEG    (CODE)
                            659 ;--------------------------------------------------------
                            660 ; interrupt vector 
                            661 ;--------------------------------------------------------
                            662 	.area HOME    (CODE)
   0000                     663 __interrupt_vect:
   0000 02 00 16            664 	ljmp	__sdcc_gsinit_startup
   0003 32                  665 	reti
   0004                     666 	.ds	7
   000B 02 17 3C            667 	ljmp	_timer_isr
   000E                     668 	.ds	5
   0013 02 16 E1            669 	ljmp	_external_isr
                            670 ;--------------------------------------------------------
                            671 ; global & static initialisations
                            672 ;--------------------------------------------------------
                            673 	.area HOME    (CODE)
                            674 	.area GSINIT  (CODE)
                            675 	.area GSFINAL (CODE)
                            676 	.area GSINIT  (CODE)
                            677 	.globl __sdcc_gsinit_startup
                            678 	.globl __sdcc_program_startup
                            679 	.globl __start__stack
                            680 	.globl __mcs51_genXINIT
                            681 	.globl __mcs51_genXRAMCLEAR
                            682 	.globl __mcs51_genRAMCLEAR
                            683 	.area GSFINAL (CODE)
   006F 02 00 72            684 	ljmp	__sdcc_program_startup
                            685 ;--------------------------------------------------------
                            686 ; Home
                            687 ;--------------------------------------------------------
                            688 	.area HOME    (CODE)
                            689 	.area CSEG    (CODE)
   0072                     690 __sdcc_program_startup:
   0072 12 1A 67            691 	lcall	_main
                            692 ;	return from main will lock up
   0075 80 FE               693 	sjmp .
                            694 ;--------------------------------------------------------
                            695 ; code
                            696 ;--------------------------------------------------------
                            697 	.area CSEG    (CODE)
                            698 ;------------------------------------------------------------
                            699 ;Allocation info for local variables in function 'MSDelay'
                            700 ;------------------------------------------------------------
                            701 ;value                     Allocated with name '_MSDelay_value_1_1'
                            702 ;x                         Allocated with name '_MSDelay_x_1_1'
                            703 ;zz                        Allocated with name '_MSDelay_zz_1_1'
                            704 ;------------------------------------------------------------
                            705 ;	main.c:90: void MSDelay(unsigned int value)
                            706 ;	-----------------------------------------
                            707 ;	 function MSDelay
                            708 ;	-----------------------------------------
   0077                     709 _MSDelay:
                    0002    710 	ar2 = 0x02
                    0003    711 	ar3 = 0x03
                    0004    712 	ar4 = 0x04
                    0005    713 	ar5 = 0x05
                    0006    714 	ar6 = 0x06
                    0007    715 	ar7 = 0x07
                    0000    716 	ar0 = 0x00
                    0001    717 	ar1 = 0x01
                            718 ;	genReceive
   0077 AA 83               719 	mov	r2,dph
   0079 E5 82               720 	mov	a,dpl
   007B 90 00 12            721 	mov	dptr,#_MSDelay_value_1_1
   007E F0                  722 	movx	@dptr,a
   007F A3                  723 	inc	dptr
   0080 EA                  724 	mov	a,r2
   0081 F0                  725 	movx	@dptr,a
                            726 ;	main.c:93: for(zz=0;zz<value;zz++)
                            727 ;	genAssign
   0082 90 00 12            728 	mov	dptr,#_MSDelay_value_1_1
   0085 E0                  729 	movx	a,@dptr
   0086 FA                  730 	mov	r2,a
   0087 A3                  731 	inc	dptr
   0088 E0                  732 	movx	a,@dptr
   0089 FB                  733 	mov	r3,a
                            734 ;	genAssign
   008A 7C 00               735 	mov	r4,#0x00
   008C 7D 00               736 	mov	r5,#0x00
   008E                     737 00104$:
                            738 ;	genCmpLt
                            739 ;	genCmp
   008E C3                  740 	clr	c
   008F EC                  741 	mov	a,r4
   0090 9A                  742 	subb	a,r2
   0091 ED                  743 	mov	a,r5
   0092 9B                  744 	subb	a,r3
                            745 ;	genIfxJump
                            746 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0093 50 14               747 	jnc	00108$
                            748 ;	Peephole 300	removed redundant label 00116$
                            749 ;	main.c:95: for(x=0;x<126;x++)
                            750 ;	genAssign
   0095 7E 7E               751 	mov	r6,#0x7E
   0097 7F 00               752 	mov	r7,#0x00
   0099                     753 00103$:
                            754 ;	genMinus
                            755 ;	genMinusDec
   0099 1E                  756 	dec	r6
   009A BE FF 01            757 	cjne	r6,#0xff,00117$
   009D 1F                  758 	dec	r7
   009E                     759 00117$:
                            760 ;	genIfx
   009E EE                  761 	mov	a,r6
   009F 4F                  762 	orl	a,r7
                            763 ;	genIfxJump
                            764 ;	Peephole 108.b	removed ljmp by inverse jump logic
   00A0 70 F7               765 	jnz	00103$
                            766 ;	Peephole 300	removed redundant label 00118$
                            767 ;	main.c:93: for(zz=0;zz<value;zz++)
                            768 ;	genPlus
                            769 ;     genPlusIncr
                            770 ;	tail increment optimized (range 7)
   00A2 0C                  771 	inc	r4
   00A3 BC 00 E8            772 	cjne	r4,#0x00,00104$
   00A6 0D                  773 	inc	r5
                            774 ;	Peephole 112.b	changed ljmp to sjmp
   00A7 80 E5               775 	sjmp	00104$
   00A9                     776 00108$:
   00A9 22                  777 	ret
                            778 ;------------------------------------------------------------
                            779 ;Allocation info for local variables in function 'putstr'
                            780 ;------------------------------------------------------------
                            781 ;ptr                       Allocated with name '_putstr_ptr_1_1'
                            782 ;------------------------------------------------------------
                            783 ;	main.c:104: void putstr(char *ptr)
                            784 ;	-----------------------------------------
                            785 ;	 function putstr
                            786 ;	-----------------------------------------
   00AA                     787 _putstr:
                            788 ;	genReceive
   00AA AA F0               789 	mov	r2,b
   00AC AB 83               790 	mov	r3,dph
   00AE E5 82               791 	mov	a,dpl
   00B0 90 00 14            792 	mov	dptr,#_putstr_ptr_1_1
   00B3 F0                  793 	movx	@dptr,a
   00B4 A3                  794 	inc	dptr
   00B5 EB                  795 	mov	a,r3
   00B6 F0                  796 	movx	@dptr,a
   00B7 A3                  797 	inc	dptr
   00B8 EA                  798 	mov	a,r2
   00B9 F0                  799 	movx	@dptr,a
                            800 ;	main.c:106: while(*ptr)
                            801 ;	genAssign
   00BA 90 00 14            802 	mov	dptr,#_putstr_ptr_1_1
   00BD E0                  803 	movx	a,@dptr
   00BE FA                  804 	mov	r2,a
   00BF A3                  805 	inc	dptr
   00C0 E0                  806 	movx	a,@dptr
   00C1 FB                  807 	mov	r3,a
   00C2 A3                  808 	inc	dptr
   00C3 E0                  809 	movx	a,@dptr
   00C4 FC                  810 	mov	r4,a
   00C5                     811 00101$:
                            812 ;	genPointerGet
                            813 ;	genGenPointerGet
   00C5 8A 82               814 	mov	dpl,r2
   00C7 8B 83               815 	mov	dph,r3
   00C9 8C F0               816 	mov	b,r4
   00CB 12 25 13            817 	lcall	__gptrget
                            818 ;	genIfx
   00CE FD                  819 	mov	r5,a
                            820 ;	Peephole 105	removed redundant mov
                            821 ;	genIfxJump
                            822 ;	Peephole 108.c	removed ljmp by inverse jump logic
   00CF 60 23               823 	jz	00108$
                            824 ;	Peephole 300	removed redundant label 00109$
                            825 ;	main.c:108: putchar(*ptr);
                            826 ;	genCall
   00D1 8D 82               827 	mov	dpl,r5
   00D3 C0 02               828 	push	ar2
   00D5 C0 03               829 	push	ar3
   00D7 C0 04               830 	push	ar4
   00D9 12 0C DF            831 	lcall	_putchar
   00DC D0 04               832 	pop	ar4
   00DE D0 03               833 	pop	ar3
   00E0 D0 02               834 	pop	ar2
                            835 ;	main.c:109: ptr++;
                            836 ;	genPlus
                            837 ;     genPlusIncr
   00E2 0A                  838 	inc	r2
   00E3 BA 00 01            839 	cjne	r2,#0x00,00110$
   00E6 0B                  840 	inc	r3
   00E7                     841 00110$:
                            842 ;	genAssign
   00E7 90 00 14            843 	mov	dptr,#_putstr_ptr_1_1
   00EA EA                  844 	mov	a,r2
   00EB F0                  845 	movx	@dptr,a
   00EC A3                  846 	inc	dptr
   00ED EB                  847 	mov	a,r3
   00EE F0                  848 	movx	@dptr,a
   00EF A3                  849 	inc	dptr
   00F0 EC                  850 	mov	a,r4
   00F1 F0                  851 	movx	@dptr,a
                            852 ;	Peephole 112.b	changed ljmp to sjmp
   00F2 80 D1               853 	sjmp	00101$
   00F4                     854 00108$:
                            855 ;	genAssign
   00F4 90 00 14            856 	mov	dptr,#_putstr_ptr_1_1
   00F7 EA                  857 	mov	a,r2
   00F8 F0                  858 	movx	@dptr,a
   00F9 A3                  859 	inc	dptr
   00FA EB                  860 	mov	a,r3
   00FB F0                  861 	movx	@dptr,a
   00FC A3                  862 	inc	dptr
   00FD EC                  863 	mov	a,r4
   00FE F0                  864 	movx	@dptr,a
                            865 ;	Peephole 300	removed redundant label 00104$
   00FF 22                  866 	ret
                            867 ;------------------------------------------------------------
                            868 ;Allocation info for local variables in function 'lcdbusywait'
                            869 ;------------------------------------------------------------
                            870 ;i                         Allocated with name '_lcdbusywait_i_1_1'
                            871 ;------------------------------------------------------------
                            872 ;	main.c:115: void lcdbusywait()
                            873 ;	-----------------------------------------
                            874 ;	 function lcdbusywait
                            875 ;	-----------------------------------------
   0100                     876 _lcdbusywait:
                            877 ;	main.c:118: EA=0;
                            878 ;	genAssign
   0100 C2 AF               879 	clr	_EA
                            880 ;	main.c:119: i=*INST_READ;
                            881 ;	genPointerGet
                            882 ;	genFarPointerGet
                            883 ;	Peephole 182.b	used 16 bit load of dptr
   0102 90 F8 00            884 	mov	dptr,#0xF800
   0105 E0                  885 	movx	a,@dptr
                            886 ;	genCast
   0106 FA                  887 	mov	r2,a
   0107 90 00 17            888 	mov	dptr,#_lcdbusywait_i_1_1
                            889 ;	Peephole 100	removed redundant mov
   010A F0                  890 	movx	@dptr,a
   010B A3                  891 	inc	dptr
                            892 ;	Peephole 181	changed mov to clr
   010C E4                  893 	clr	a
   010D F0                  894 	movx	@dptr,a
                            895 ;	main.c:120: EA=1;
                            896 ;	genAssign
   010E D2 AF               897 	setb	_EA
                            898 ;	main.c:121: MSDelay(1);
                            899 ;	genCall
                            900 ;	Peephole 182.b	used 16 bit load of dptr
   0110 90 00 01            901 	mov	dptr,#0x0001
   0113 12 00 77            902 	lcall	_MSDelay
                            903 ;	main.c:122: while((i&BUSYFLAG_MASK_BITS)==BUSYFLAG_MASK_BITS)
   0116                     904 00101$:
                            905 ;	genAssign
   0116 90 00 17            906 	mov	dptr,#_lcdbusywait_i_1_1
   0119 E0                  907 	movx	a,@dptr
   011A FA                  908 	mov	r2,a
   011B A3                  909 	inc	dptr
   011C E0                  910 	movx	a,@dptr
   011D FB                  911 	mov	r3,a
                            912 ;	genAnd
   011E 53 02 80            913 	anl	ar2,#0x80
   0121 7B 00               914 	mov	r3,#0x00
                            915 ;	genCmpEq
                            916 ;	gencjneshort
                            917 ;	Peephole 112.b	changed ljmp to sjmp
                            918 ;	Peephole 198.a	optimized misc jump sequence
   0123 BA 80 1B            919 	cjne	r2,#0x80,00104$
   0126 BB 00 18            920 	cjne	r3,#0x00,00104$
                            921 ;	Peephole 200.b	removed redundant sjmp
                            922 ;	Peephole 300	removed redundant label 00108$
                            923 ;	Peephole 300	removed redundant label 00109$
                            924 ;	main.c:124: EA=0;
                            925 ;	genAssign
   0129 C2 AF               926 	clr	_EA
                            927 ;	main.c:125: i = *INST_READ;
                            928 ;	genPointerGet
                            929 ;	genFarPointerGet
                            930 ;	Peephole 182.b	used 16 bit load of dptr
   012B 90 F8 00            931 	mov	dptr,#0xF800
   012E E0                  932 	movx	a,@dptr
                            933 ;	genCast
   012F FA                  934 	mov	r2,a
   0130 90 00 17            935 	mov	dptr,#_lcdbusywait_i_1_1
                            936 ;	Peephole 100	removed redundant mov
   0133 F0                  937 	movx	@dptr,a
   0134 A3                  938 	inc	dptr
                            939 ;	Peephole 181	changed mov to clr
   0135 E4                  940 	clr	a
   0136 F0                  941 	movx	@dptr,a
                            942 ;	main.c:126: EA=1;
                            943 ;	genAssign
   0137 D2 AF               944 	setb	_EA
                            945 ;	main.c:127: MSDelay(1);
                            946 ;	genCall
                            947 ;	Peephole 182.b	used 16 bit load of dptr
   0139 90 00 01            948 	mov	dptr,#0x0001
   013C 12 00 77            949 	lcall	_MSDelay
                            950 ;	Peephole 112.b	changed ljmp to sjmp
   013F 80 D5               951 	sjmp	00101$
   0141                     952 00104$:
   0141 22                  953 	ret
                            954 ;------------------------------------------------------------
                            955 ;Allocation info for local variables in function 'input_value'
                            956 ;------------------------------------------------------------
                            957 ;l                         Allocated with name '_input_value_l_1_1'
                            958 ;m                         Allocated with name '_input_value_m_1_1'
                            959 ;------------------------------------------------------------
                            960 ;	main.c:132: unsigned char input_value()
                            961 ;	-----------------------------------------
                            962 ;	 function input_value
                            963 ;	-----------------------------------------
   0142                     964 _input_value:
                            965 ;	main.c:134: unsigned volatile char l=0, m=0;
                            966 ;	genAssign
   0142 90 00 19            967 	mov	dptr,#_input_value_l_1_1
                            968 ;	Peephole 181	changed mov to clr
                            969 ;	genAssign
                            970 ;	Peephole 181	changed mov to clr
                            971 ;	Peephole 219.a	removed redundant clear
   0145 E4                  972 	clr	a
   0146 F0                  973 	movx	@dptr,a
   0147 90 00 1A            974 	mov	dptr,#_input_value_m_1_1
   014A F0                  975 	movx	@dptr,a
                            976 ;	main.c:135: do
   014B                     977 00110$:
                            978 ;	main.c:137: l=getchar();
                            979 ;	genCall
   014B 12 0C F1            980 	lcall	_getchar
   014E E5 82               981 	mov	a,dpl
                            982 ;	genAssign
   0150 90 00 19            983 	mov	dptr,#_input_value_l_1_1
   0153 F0                  984 	movx	@dptr,a
                            985 ;	main.c:138: if(l>='0' && l<='9')
                            986 ;	genAssign
   0154 90 00 19            987 	mov	dptr,#_input_value_l_1_1
   0157 E0                  988 	movx	a,@dptr
   0158 FA                  989 	mov	r2,a
                            990 ;	genCmpLt
                            991 ;	genCmp
   0159 BA 30 00            992 	cjne	r2,#0x30,00121$
   015C                     993 00121$:
                            994 ;	genIfxJump
                            995 ;	Peephole 112.b	changed ljmp to sjmp
                            996 ;	Peephole 160.a	removed sjmp by inverse jump logic
   015C 40 2B               997 	jc	00107$
                            998 ;	Peephole 300	removed redundant label 00122$
                            999 ;	genAssign
   015E 90 00 19           1000 	mov	dptr,#_input_value_l_1_1
   0161 E0                 1001 	movx	a,@dptr
                           1002 ;	genCmpGt
                           1003 ;	genCmp
                           1004 ;	genIfxJump
                           1005 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   0162 FA                 1006 	mov  r2,a
                           1007 ;	Peephole 177.a	removed redundant mov
   0163 24 C6              1008 	add	a,#0xff - 0x39
                           1009 ;	Peephole 112.b	changed ljmp to sjmp
                           1010 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0165 40 22              1011 	jc	00107$
                           1012 ;	Peephole 300	removed redundant label 00123$
                           1013 ;	main.c:140: m=((m*10)+(l-'0'));
                           1014 ;	genAssign
   0167 90 00 1A           1015 	mov	dptr,#_input_value_m_1_1
   016A E0                 1016 	movx	a,@dptr
                           1017 ;	genMult
                           1018 ;	genMultOneByte
   016B FA                 1019 	mov	r2,a
                           1020 ;	Peephole 105	removed redundant mov
   016C 75 F0 0A           1021 	mov	b,#0x0A
   016F A4                 1022 	mul	ab
   0170 FA                 1023 	mov	r2,a
                           1024 ;	genAssign
   0171 90 00 19           1025 	mov	dptr,#_input_value_l_1_1
   0174 E0                 1026 	movx	a,@dptr
                           1027 ;	genMinus
   0175 FB                 1028 	mov	r3,a
                           1029 ;	Peephole 105	removed redundant mov
   0176 24 D0              1030 	add	a,#0xd0
                           1031 ;	genPlus
   0178 90 00 1A           1032 	mov	dptr,#_input_value_m_1_1
                           1033 ;	Peephole 236.a	used r2 instead of ar2
   017B 2A                 1034 	add	a,r2
   017C F0                 1035 	movx	@dptr,a
                           1036 ;	main.c:141: putchar(l);                 /* CONVERTING IT TO DECIMAL VALUE*/
                           1037 ;	genAssign
   017D 90 00 19           1038 	mov	dptr,#_input_value_l_1_1
   0180 E0                 1039 	movx	a,@dptr
                           1040 ;	genCall
   0181 FA                 1041 	mov	r2,a
                           1042 ;	Peephole 244.c	loading dpl from a instead of r2
   0182 F5 82              1043 	mov	dpl,a
   0184 12 0C DF           1044 	lcall	_putchar
                           1045 ;	Peephole 112.b	changed ljmp to sjmp
   0187 80 2B              1046 	sjmp	00111$
   0189                    1047 00107$:
                           1048 ;	main.c:144: else if(l==READ_BACKSPACE)
                           1049 ;	genAssign
   0189 90 00 19           1050 	mov	dptr,#_input_value_l_1_1
   018C E0                 1051 	movx	a,@dptr
   018D FA                 1052 	mov	r2,a
                           1053 ;	genCmpEq
                           1054 ;	gencjneshort
                           1055 ;	Peephole 112.b	changed ljmp to sjmp
                           1056 ;	Peephole 198.b	optimized misc jump sequence
   018E BA 7F 10           1057 	cjne	r2,#0x7F,00104$
                           1058 ;	Peephole 200.b	removed redundant sjmp
                           1059 ;	Peephole 300	removed redundant label 00124$
                           1060 ;	Peephole 300	removed redundant label 00125$
                           1061 ;	main.c:146: m=m/10;
                           1062 ;	genAssign
   0191 90 00 1A           1063 	mov	dptr,#_input_value_m_1_1
   0194 E0                 1064 	movx	a,@dptr
   0195 FA                 1065 	mov	r2,a
                           1066 ;	genDiv
   0196 90 00 1A           1067 	mov	dptr,#_input_value_m_1_1
                           1068 ;     genDivOneByte
   0199 75 F0 0A           1069 	mov	b,#0x0A
   019C EA                 1070 	mov	a,r2
   019D 84                 1071 	div	ab
   019E F0                 1072 	movx	@dptr,a
                           1073 ;	Peephole 112.b	changed ljmp to sjmp
   019F 80 13              1074 	sjmp	00111$
   01A1                    1075 00104$:
                           1076 ;	main.c:148: else if(l!=READ_ENTER)
                           1077 ;	genAssign
   01A1 90 00 19           1078 	mov	dptr,#_input_value_l_1_1
   01A4 E0                 1079 	movx	a,@dptr
   01A5 FA                 1080 	mov	r2,a
                           1081 ;	genCmpEq
                           1082 ;	gencjneshort
   01A6 BA 0D 02           1083 	cjne	r2,#0x0D,00126$
                           1084 ;	Peephole 112.b	changed ljmp to sjmp
   01A9 80 09              1085 	sjmp	00111$
   01AB                    1086 00126$:
                           1087 ;	main.c:149: {putstr("\n\rEnter Numbers\n\r");}
                           1088 ;	genCall
                           1089 ;	Peephole 182.a	used 16 bit load of DPTR
   01AB 90 25 4C           1090 	mov	dptr,#__str_0
   01AE 75 F0 80           1091 	mov	b,#0x80
   01B1 12 00 AA           1092 	lcall	_putstr
   01B4                    1093 00111$:
                           1094 ;	main.c:150: }while(l!=READ_ENTER);
                           1095 ;	genAssign
   01B4 90 00 19           1096 	mov	dptr,#_input_value_l_1_1
   01B7 E0                 1097 	movx	a,@dptr
   01B8 FA                 1098 	mov	r2,a
                           1099 ;	genCmpEq
                           1100 ;	gencjneshort
   01B9 BA 0D 02           1101 	cjne	r2,#0x0D,00127$
   01BC 80 03              1102 	sjmp	00128$
   01BE                    1103 00127$:
   01BE 02 01 4B           1104 	ljmp	00110$
   01C1                    1105 00128$:
                           1106 ;	main.c:151: return m;                                   /* UNTIL ENTER IS PRESSES*/
                           1107 ;	genAssign
   01C1 90 00 1A           1108 	mov	dptr,#_input_value_m_1_1
   01C4 E0                 1109 	movx	a,@dptr
                           1110 ;	genRet
                           1111 ;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
   01C5 F5 82              1112 	mov	dpl,a
                           1113 ;	Peephole 300	removed redundant label 00113$
   01C7 22                 1114 	ret
                           1115 ;------------------------------------------------------------
                           1116 ;Allocation info for local variables in function 'lcdclockgotoaddr'
                           1117 ;------------------------------------------------------------
                           1118 ;addr                      Allocated with name '_lcdclockgotoaddr_addr_1_1'
                           1119 ;temp                      Allocated with name '_lcdclockgotoaddr_temp_2_2'
                           1120 ;------------------------------------------------------------
                           1121 ;	main.c:154: void lcdclockgotoaddr(unsigned char addr)
                           1122 ;	-----------------------------------------
                           1123 ;	 function lcdclockgotoaddr
                           1124 ;	-----------------------------------------
   01C8                    1125 _lcdclockgotoaddr:
                           1126 ;	genReceive
   01C8 E5 82              1127 	mov	a,dpl
   01CA 90 00 1B           1128 	mov	dptr,#_lcdclockgotoaddr_addr_1_1
   01CD F0                 1129 	movx	@dptr,a
                           1130 ;	main.c:156: if(addr>=0x59 && addr<=0x5F)
                           1131 ;	genAssign
   01CE 90 00 1B           1132 	mov	dptr,#_lcdclockgotoaddr_addr_1_1
   01D1 E0                 1133 	movx	a,@dptr
   01D2 FA                 1134 	mov	r2,a
                           1135 ;	genCmpLt
                           1136 ;	genCmp
   01D3 BA 59 00           1137 	cjne	r2,#0x59,00108$
   01D6                    1138 00108$:
                           1139 ;	genIfxJump
                           1140 ;	Peephole 112.b	changed ljmp to sjmp
                           1141 ;	Peephole 160.a	removed sjmp by inverse jump logic
   01D6 40 10              1142 	jc	00104$
                           1143 ;	Peephole 300	removed redundant label 00109$
                           1144 ;	genCmpGt
                           1145 ;	genCmp
                           1146 ;	genIfxJump
                           1147 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   01D8 EA                 1148 	mov	a,r2
   01D9 24 A0              1149 	add	a,#0xff - 0x5F
                           1150 ;	Peephole 112.b	changed ljmp to sjmp
                           1151 ;	Peephole 160.a	removed sjmp by inverse jump logic
   01DB 40 0B              1152 	jc	00104$
                           1153 ;	Peephole 300	removed redundant label 00110$
                           1154 ;	main.c:160: temp |= addr;
                           1155 ;	genOr
   01DD 43 02 80           1156 	orl	ar2,#0x80
                           1157 ;	main.c:161: *INST_WRITE = temp;
                           1158 ;	genAssign
                           1159 ;	Peephole 182.b	used 16 bit load of dptr
   01E0 90 F0 00           1160 	mov	dptr,#0xF000
                           1161 ;	genPointerSet
                           1162 ;     genFarPointerSet
   01E3 EA                 1163 	mov	a,r2
   01E4 F0                 1164 	movx	@dptr,a
                           1165 ;	main.c:162: lcdbusywait();
                           1166 ;	genCall
                           1167 ;	Peephole 253.c	replaced lcall with ljmp
   01E5 02 01 00           1168 	ljmp	_lcdbusywait
   01E8                    1169 00104$:
   01E8 22                 1170 	ret
                           1171 ;------------------------------------------------------------
                           1172 ;Allocation info for local variables in function 'lcdgotoaddr'
                           1173 ;------------------------------------------------------------
                           1174 ;addr                      Allocated with name '_lcdgotoaddr_addr_1_1'
                           1175 ;temp                      Allocated with name '_lcdgotoaddr_temp_2_2'
                           1176 ;------------------------------------------------------------
                           1177 ;	main.c:167: void lcdgotoaddr(unsigned char addr)
                           1178 ;	-----------------------------------------
                           1179 ;	 function lcdgotoaddr
                           1180 ;	-----------------------------------------
   01E9                    1181 _lcdgotoaddr:
                           1182 ;	genReceive
   01E9 E5 82              1183 	mov	a,dpl
   01EB 90 00 1C           1184 	mov	dptr,#_lcdgotoaddr_addr_1_1
   01EE F0                 1185 	movx	@dptr,a
                           1186 ;	main.c:169: if(addr>=0x00 && addr<=0x58)
                           1187 ;	genAssign
   01EF 90 00 1C           1188 	mov	dptr,#_lcdgotoaddr_addr_1_1
   01F2 E0                 1189 	movx	a,@dptr
                           1190 ;	genCmpGt
                           1191 ;	genCmp
                           1192 ;	genIfxJump
                           1193 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   01F3 FA                 1194 	mov  r2,a
                           1195 ;	Peephole 177.a	removed redundant mov
   01F4 24 A7              1196 	add	a,#0xff - 0x58
                           1197 ;	Peephole 112.b	changed ljmp to sjmp
                           1198 ;	Peephole 160.a	removed sjmp by inverse jump logic
   01F6 40 0F              1199 	jc	00102$
                           1200 ;	Peephole 300	removed redundant label 00108$
                           1201 ;	main.c:173: temp |= addr;
                           1202 ;	genOr
   01F8 43 02 80           1203 	orl	ar2,#0x80
                           1204 ;	main.c:174: EA=0;
                           1205 ;	genAssign
   01FB C2 AF              1206 	clr	_EA
                           1207 ;	main.c:175: *INST_WRITE = temp;
                           1208 ;	genAssign
                           1209 ;	Peephole 182.b	used 16 bit load of dptr
   01FD 90 F0 00           1210 	mov	dptr,#0xF000
                           1211 ;	genPointerSet
                           1212 ;     genFarPointerSet
   0200 EA                 1213 	mov	a,r2
   0201 F0                 1214 	movx	@dptr,a
                           1215 ;	main.c:176: EA=1;
                           1216 ;	genAssign
   0202 D2 AF              1217 	setb	_EA
                           1218 ;	main.c:177: lcdbusywait();
                           1219 ;	genCall
                           1220 ;	Peephole 112.b	changed ljmp to sjmp
                           1221 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1222 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0204 02 01 00           1223 	ljmp	_lcdbusywait
   0207                    1224 00102$:
                           1225 ;	main.c:180: putstr("\n\rCan't move to given position\n\r");
                           1226 ;	genCall
                           1227 ;	Peephole 182.a	used 16 bit load of DPTR
   0207 90 25 5E           1228 	mov	dptr,#__str_1
   020A 75 F0 80           1229 	mov	b,#0x80
                           1230 ;	Peephole 253.b	replaced lcall/ret with ljmp
   020D 02 00 AA           1231 	ljmp	_putstr
                           1232 ;
                           1233 ;------------------------------------------------------------
                           1234 ;Allocation info for local variables in function 'lcdgotoxy'
                           1235 ;------------------------------------------------------------
                           1236 ;column                    Allocated with name '_lcdgotoxy_PARM_2'
                           1237 ;row                       Allocated with name '_lcdgotoxy_row_1_1'
                           1238 ;------------------------------------------------------------
                           1239 ;	main.c:184: void lcdgotoxy(unsigned char row, unsigned char column)
                           1240 ;	-----------------------------------------
                           1241 ;	 function lcdgotoxy
                           1242 ;	-----------------------------------------
   0210                    1243 _lcdgotoxy:
                           1244 ;	genReceive
   0210 E5 82              1245 	mov	a,dpl
   0212 90 00 1E           1246 	mov	dptr,#_lcdgotoxy_row_1_1
   0215 F0                 1247 	movx	@dptr,a
                           1248 ;	main.c:186: switch(row)
                           1249 ;	genAssign
   0216 90 00 1E           1250 	mov	dptr,#_lcdgotoxy_row_1_1
   0219 E0                 1251 	movx	a,@dptr
                           1252 ;	genCmpGt
                           1253 ;	genCmp
                           1254 ;	genIfxJump
                           1255 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   021A FA                 1256 	mov  r2,a
                           1257 ;	Peephole 177.a	removed redundant mov
   021B 24 FC              1258 	add	a,#0xff - 0x03
                           1259 ;	Peephole 112.b	changed ljmp to sjmp
                           1260 ;	Peephole 160.a	removed sjmp by inverse jump logic
   021D 40 47              1261 	jc	00106$
                           1262 ;	Peephole 300	removed redundant label 00109$
                           1263 ;	genJumpTab
   021F EA                 1264 	mov	a,r2
                           1265 ;	Peephole 254	optimized left shift
   0220 2A                 1266 	add	a,r2
   0221 2A                 1267 	add	a,r2
   0222 90 02 26           1268 	mov	dptr,#00110$
   0225 73                 1269 	jmp	@a+dptr
   0226                    1270 00110$:
   0226 02 02 32           1271 	ljmp	00101$
   0229 02 02 3C           1272 	ljmp	00102$
   022C 02 02 4A           1273 	ljmp	00103$
   022F 02 02 58           1274 	ljmp	00104$
                           1275 ;	main.c:188: case 0: lcdgotoaddr((BASE_ADDRROW0+column));
   0232                    1276 00101$:
                           1277 ;	genAssign
   0232 90 00 1D           1278 	mov	dptr,#_lcdgotoxy_PARM_2
   0235 E0                 1279 	movx	a,@dptr
                           1280 ;	genCall
   0236 FA                 1281 	mov	r2,a
                           1282 ;	Peephole 244.c	loading dpl from a instead of r2
   0237 F5 82              1283 	mov	dpl,a
                           1284 ;	main.c:189: break;
                           1285 ;	main.c:190: case 1: lcdgotoaddr((BASE_ADDRROW1+column));
                           1286 ;	Peephole 112.b	changed ljmp to sjmp
                           1287 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1288 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0239 02 01 E9           1289 	ljmp	_lcdgotoaddr
   023C                    1290 00102$:
                           1291 ;	genAssign
   023C 90 00 1D           1292 	mov	dptr,#_lcdgotoxy_PARM_2
   023F E0                 1293 	movx	a,@dptr
   0240 FA                 1294 	mov	r2,a
                           1295 ;	genPlus
                           1296 ;     genPlusIncr
   0241 74 40              1297 	mov	a,#0x40
                           1298 ;	Peephole 236.a	used r2 instead of ar2
   0243 2A                 1299 	add	a,r2
                           1300 ;	genCall
   0244 FA                 1301 	mov	r2,a
                           1302 ;	Peephole 244.c	loading dpl from a instead of r2
   0245 F5 82              1303 	mov	dpl,a
                           1304 ;	main.c:191: break;
                           1305 ;	main.c:192: case 2: lcdgotoaddr((BASE_ADDRROW2+column));
                           1306 ;	Peephole 112.b	changed ljmp to sjmp
                           1307 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1308 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0247 02 01 E9           1309 	ljmp	_lcdgotoaddr
   024A                    1310 00103$:
                           1311 ;	genAssign
   024A 90 00 1D           1312 	mov	dptr,#_lcdgotoxy_PARM_2
   024D E0                 1313 	movx	a,@dptr
   024E FA                 1314 	mov	r2,a
                           1315 ;	genPlus
                           1316 ;     genPlusIncr
   024F 74 10              1317 	mov	a,#0x10
                           1318 ;	Peephole 236.a	used r2 instead of ar2
   0251 2A                 1319 	add	a,r2
                           1320 ;	genCall
   0252 FA                 1321 	mov	r2,a
                           1322 ;	Peephole 244.c	loading dpl from a instead of r2
   0253 F5 82              1323 	mov	dpl,a
                           1324 ;	main.c:193: break;
                           1325 ;	main.c:194: case 3: lcdgotoaddr((BASE_ADDRROW3+column));
                           1326 ;	Peephole 112.b	changed ljmp to sjmp
                           1327 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1328 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0255 02 01 E9           1329 	ljmp	_lcdgotoaddr
   0258                    1330 00104$:
                           1331 ;	genAssign
   0258 90 00 1D           1332 	mov	dptr,#_lcdgotoxy_PARM_2
   025B E0                 1333 	movx	a,@dptr
   025C FA                 1334 	mov	r2,a
                           1335 ;	genPlus
                           1336 ;     genPlusIncr
   025D 74 50              1337 	mov	a,#0x50
                           1338 ;	Peephole 236.a	used r2 instead of ar2
   025F 2A                 1339 	add	a,r2
                           1340 ;	genCall
   0260 FA                 1341 	mov	r2,a
                           1342 ;	Peephole 244.c	loading dpl from a instead of r2
   0261 F5 82              1343 	mov	dpl,a
                           1344 ;	main.c:196: }
                           1345 ;	Peephole 253.c	replaced lcall with ljmp
   0263 02 01 E9           1346 	ljmp	_lcdgotoaddr
   0266                    1347 00106$:
   0266 22                 1348 	ret
                           1349 ;------------------------------------------------------------
                           1350 ;Allocation info for local variables in function 'lcdclear'
                           1351 ;------------------------------------------------------------
                           1352 ;------------------------------------------------------------
                           1353 ;	main.c:200: void lcdclear()
                           1354 ;	-----------------------------------------
                           1355 ;	 function lcdclear
                           1356 ;	-----------------------------------------
   0267                    1357 _lcdclear:
                           1358 ;	main.c:202: EA=0;
                           1359 ;	genAssign
   0267 C2 AF              1360 	clr	_EA
                           1361 ;	main.c:203: *INST_WRITE = CLEARSCREEN_CURSOR_HOME;
                           1362 ;	genAssign
                           1363 ;	Peephole 182.b	used 16 bit load of dptr
   0269 90 F0 00           1364 	mov	dptr,#0xF000
                           1365 ;	genPointerSet
                           1366 ;     genFarPointerSet
   026C 74 01              1367 	mov	a,#0x01
   026E F0                 1368 	movx	@dptr,a
                           1369 ;	main.c:204: EA=1;
                           1370 ;	genAssign
   026F D2 AF              1371 	setb	_EA
                           1372 ;	main.c:205: lcdbusywait();
                           1373 ;	genCall
   0271 12 01 00           1374 	lcall	_lcdbusywait
                           1375 ;	main.c:206: putstr("Clearing the LCD completed\n\r");
                           1376 ;	genCall
                           1377 ;	Peephole 182.a	used 16 bit load of DPTR
   0274 90 25 7F           1378 	mov	dptr,#__str_2
   0277 75 F0 80           1379 	mov	b,#0x80
                           1380 ;	Peephole 253.b	replaced lcall/ret with ljmp
   027A 02 00 AA           1381 	ljmp	_putstr
                           1382 ;
                           1383 ;------------------------------------------------------------
                           1384 ;Allocation info for local variables in function 'lcdputch'
                           1385 ;------------------------------------------------------------
                           1386 ;cc                        Allocated with name '_lcdputch_cc_1_1'
                           1387 ;aa                        Allocated with name '_lcdputch_aa_1_1'
                           1388 ;tempput                   Allocated with name '_lcdputch_tempput_1_1'
                           1389 ;------------------------------------------------------------
                           1390 ;	main.c:210: void lcdputch(unsigned char cc)
                           1391 ;	-----------------------------------------
                           1392 ;	 function lcdputch
                           1393 ;	-----------------------------------------
   027D                    1394 _lcdputch:
                           1395 ;	genReceive
   027D E5 82              1396 	mov	a,dpl
   027F 90 00 1F           1397 	mov	dptr,#_lcdputch_cc_1_1
   0282 F0                 1398 	movx	@dptr,a
                           1399 ;	main.c:213: EA=0;
                           1400 ;	genAssign
   0283 C2 AF              1401 	clr	_EA
                           1402 ;	main.c:214: *DATA_WRITE = cc;
                           1403 ;	genAssign
   0285 7A 00              1404 	mov	r2,#0x00
   0287 7B F4              1405 	mov	r3,#0xF4
                           1406 ;	genAssign
   0289 90 00 1F           1407 	mov	dptr,#_lcdputch_cc_1_1
   028C E0                 1408 	movx	a,@dptr
                           1409 ;	genPointerSet
                           1410 ;     genFarPointerSet
   028D FC                 1411 	mov	r4,a
   028E 8A 82              1412 	mov	dpl,r2
   0290 8B 83              1413 	mov	dph,r3
                           1414 ;	Peephole 136	removed redundant move
   0292 F0                 1415 	movx	@dptr,a
                           1416 ;	main.c:215: EA=1;
                           1417 ;	genAssign
   0293 D2 AF              1418 	setb	_EA
                           1419 ;	main.c:216: lcdbusywait();
                           1420 ;	genCall
   0295 12 01 00           1421 	lcall	_lcdbusywait
                           1422 ;	main.c:217: EA=0;
                           1423 ;	genAssign
   0298 C2 AF              1424 	clr	_EA
                           1425 ;	main.c:218: aa=*INST_READ;
                           1426 ;	genPointerGet
                           1427 ;	genFarPointerGet
                           1428 ;	Peephole 182.b	used 16 bit load of dptr
   029A 90 F8 00           1429 	mov	dptr,#0xF800
   029D E0                 1430 	movx	a,@dptr
                           1431 ;	genCast
   029E FA                 1432 	mov	r2,a
   029F 90 00 20           1433 	mov	dptr,#_lcdputch_aa_1_1
                           1434 ;	Peephole 100	removed redundant mov
   02A2 F0                 1435 	movx	@dptr,a
   02A3 A3                 1436 	inc	dptr
                           1437 ;	Peephole 181	changed mov to clr
   02A4 E4                 1438 	clr	a
   02A5 F0                 1439 	movx	@dptr,a
                           1440 ;	main.c:219: EA=1;
                           1441 ;	genAssign
   02A6 D2 AF              1442 	setb	_EA
                           1443 ;	main.c:220: tempput = aa&CURSORADDR_MASK_BITS;              /* THIS LOOP HANDLES THE ROW ADDRESS MISMATCH OF THE LCD*/
                           1444 ;	genAssign
   02A8 90 00 20           1445 	mov	dptr,#_lcdputch_aa_1_1
   02AB E0                 1446 	movx	a,@dptr
   02AC FA                 1447 	mov	r2,a
   02AD A3                 1448 	inc	dptr
   02AE E0                 1449 	movx	a,@dptr
   02AF FB                 1450 	mov	r3,a
                           1451 ;	genAnd
   02B0 90 00 22           1452 	mov	dptr,#_lcdputch_tempput_1_1
   02B3 74 7F              1453 	mov	a,#0x7F
   02B5 5A                 1454 	anl	a,r2
   02B6 F0                 1455 	movx	@dptr,a
   02B7 A3                 1456 	inc	dptr
                           1457 ;	Peephole 181	changed mov to clr
   02B8 E4                 1458 	clr	a
   02B9 F0                 1459 	movx	@dptr,a
                           1460 ;	main.c:221: if(tempput==0x10)
                           1461 ;	genAssign
   02BA 90 00 22           1462 	mov	dptr,#_lcdputch_tempput_1_1
   02BD E0                 1463 	movx	a,@dptr
   02BE FA                 1464 	mov	r2,a
   02BF A3                 1465 	inc	dptr
   02C0 E0                 1466 	movx	a,@dptr
   02C1 FB                 1467 	mov	r3,a
                           1468 ;	genCmpEq
                           1469 ;	gencjneshort
                           1470 ;	Peephole 112.b	changed ljmp to sjmp
                           1471 ;	Peephole 198.a	optimized misc jump sequence
   02C2 BA 10 0E           1472 	cjne	r2,#0x10,00110$
   02C5 BB 00 0B           1473 	cjne	r3,#0x00,00110$
                           1474 ;	Peephole 200.b	removed redundant sjmp
                           1475 ;	Peephole 300	removed redundant label 00118$
                           1476 ;	Peephole 300	removed redundant label 00119$
                           1477 ;	main.c:223: lcdgotoxy(1,0);
                           1478 ;	genAssign
   02C8 90 00 1D           1479 	mov	dptr,#_lcdgotoxy_PARM_2
                           1480 ;	Peephole 181	changed mov to clr
   02CB E4                 1481 	clr	a
   02CC F0                 1482 	movx	@dptr,a
                           1483 ;	genCall
   02CD 75 82 01           1484 	mov	dpl,#0x01
                           1485 ;	Peephole 112.b	changed ljmp to sjmp
                           1486 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1487 ;	Peephole 253.a	replaced lcall/ret with ljmp
   02D0 02 02 10           1488 	ljmp	_lcdgotoxy
   02D3                    1489 00110$:
                           1490 ;	main.c:225: else if(tempput==0x50)
                           1491 ;	genAssign
   02D3 90 00 22           1492 	mov	dptr,#_lcdputch_tempput_1_1
   02D6 E0                 1493 	movx	a,@dptr
   02D7 FA                 1494 	mov	r2,a
   02D8 A3                 1495 	inc	dptr
   02D9 E0                 1496 	movx	a,@dptr
   02DA FB                 1497 	mov	r3,a
                           1498 ;	genCmpEq
                           1499 ;	gencjneshort
                           1500 ;	Peephole 112.b	changed ljmp to sjmp
                           1501 ;	Peephole 198.a	optimized misc jump sequence
   02DB BA 50 0E           1502 	cjne	r2,#0x50,00107$
   02DE BB 00 0B           1503 	cjne	r3,#0x00,00107$
                           1504 ;	Peephole 200.b	removed redundant sjmp
                           1505 ;	Peephole 300	removed redundant label 00120$
                           1506 ;	Peephole 300	removed redundant label 00121$
                           1507 ;	main.c:227: lcdgotoxy(2,0);
                           1508 ;	genAssign
   02E1 90 00 1D           1509 	mov	dptr,#_lcdgotoxy_PARM_2
                           1510 ;	Peephole 181	changed mov to clr
   02E4 E4                 1511 	clr	a
   02E5 F0                 1512 	movx	@dptr,a
                           1513 ;	genCall
   02E6 75 82 02           1514 	mov	dpl,#0x02
                           1515 ;	Peephole 112.b	changed ljmp to sjmp
                           1516 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1517 ;	Peephole 253.a	replaced lcall/ret with ljmp
   02E9 02 02 10           1518 	ljmp	_lcdgotoxy
   02EC                    1519 00107$:
                           1520 ;	main.c:229: else if(tempput==0x20)
                           1521 ;	genAssign
   02EC 90 00 22           1522 	mov	dptr,#_lcdputch_tempput_1_1
   02EF E0                 1523 	movx	a,@dptr
   02F0 FA                 1524 	mov	r2,a
   02F1 A3                 1525 	inc	dptr
   02F2 E0                 1526 	movx	a,@dptr
   02F3 FB                 1527 	mov	r3,a
                           1528 ;	genCmpEq
                           1529 ;	gencjneshort
                           1530 ;	Peephole 112.b	changed ljmp to sjmp
                           1531 ;	Peephole 198.a	optimized misc jump sequence
   02F4 BA 20 0E           1532 	cjne	r2,#0x20,00104$
   02F7 BB 00 0B           1533 	cjne	r3,#0x00,00104$
                           1534 ;	Peephole 200.b	removed redundant sjmp
                           1535 ;	Peephole 300	removed redundant label 00122$
                           1536 ;	Peephole 300	removed redundant label 00123$
                           1537 ;	main.c:231: lcdgotoxy(3,0);
                           1538 ;	genAssign
   02FA 90 00 1D           1539 	mov	dptr,#_lcdgotoxy_PARM_2
                           1540 ;	Peephole 181	changed mov to clr
   02FD E4                 1541 	clr	a
   02FE F0                 1542 	movx	@dptr,a
                           1543 ;	genCall
   02FF 75 82 03           1544 	mov	dpl,#0x03
                           1545 ;	Peephole 112.b	changed ljmp to sjmp
                           1546 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1547 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0302 02 02 10           1548 	ljmp	_lcdgotoxy
   0305                    1549 00104$:
                           1550 ;	main.c:233: else if(tempput==0x59)
                           1551 ;	genAssign
   0305 90 00 22           1552 	mov	dptr,#_lcdputch_tempput_1_1
   0308 E0                 1553 	movx	a,@dptr
   0309 FA                 1554 	mov	r2,a
   030A A3                 1555 	inc	dptr
   030B E0                 1556 	movx	a,@dptr
   030C FB                 1557 	mov	r3,a
                           1558 ;	genCmpEq
                           1559 ;	gencjneshort
                           1560 ;	Peephole 112.b	changed ljmp to sjmp
                           1561 ;	Peephole 198.a	optimized misc jump sequence
   030D BA 59 0E           1562 	cjne	r2,#0x59,00112$
   0310 BB 00 0B           1563 	cjne	r3,#0x00,00112$
                           1564 ;	Peephole 200.b	removed redundant sjmp
                           1565 ;	Peephole 300	removed redundant label 00124$
                           1566 ;	Peephole 300	removed redundant label 00125$
                           1567 ;	main.c:235: lcdgotoxy(0,0);//lcdgotoaddr(temp);
                           1568 ;	genAssign
   0313 90 00 1D           1569 	mov	dptr,#_lcdgotoxy_PARM_2
                           1570 ;	Peephole 181	changed mov to clr
   0316 E4                 1571 	clr	a
   0317 F0                 1572 	movx	@dptr,a
                           1573 ;	genCall
   0318 75 82 00           1574 	mov	dpl,#0x00
                           1575 ;	Peephole 253.c	replaced lcall with ljmp
   031B 02 02 10           1576 	ljmp	_lcdgotoxy
   031E                    1577 00112$:
   031E 22                 1578 	ret
                           1579 ;------------------------------------------------------------
                           1580 ;Allocation info for local variables in function 'lcdputstr'
                           1581 ;------------------------------------------------------------
                           1582 ;ptr                       Allocated with name '_lcdputstr_ptr_1_1'
                           1583 ;------------------------------------------------------------
                           1584 ;	main.c:245: void lcdputstr(unsigned char *ptr)
                           1585 ;	-----------------------------------------
                           1586 ;	 function lcdputstr
                           1587 ;	-----------------------------------------
   031F                    1588 _lcdputstr:
                           1589 ;	genReceive
   031F AA F0              1590 	mov	r2,b
   0321 AB 83              1591 	mov	r3,dph
   0323 E5 82              1592 	mov	a,dpl
   0325 90 00 24           1593 	mov	dptr,#_lcdputstr_ptr_1_1
   0328 F0                 1594 	movx	@dptr,a
   0329 A3                 1595 	inc	dptr
   032A EB                 1596 	mov	a,r3
   032B F0                 1597 	movx	@dptr,a
   032C A3                 1598 	inc	dptr
   032D EA                 1599 	mov	a,r2
   032E F0                 1600 	movx	@dptr,a
                           1601 ;	main.c:247: while(*ptr)
                           1602 ;	genAssign
   032F 90 00 24           1603 	mov	dptr,#_lcdputstr_ptr_1_1
   0332 E0                 1604 	movx	a,@dptr
   0333 FA                 1605 	mov	r2,a
   0334 A3                 1606 	inc	dptr
   0335 E0                 1607 	movx	a,@dptr
   0336 FB                 1608 	mov	r3,a
   0337 A3                 1609 	inc	dptr
   0338 E0                 1610 	movx	a,@dptr
   0339 FC                 1611 	mov	r4,a
   033A                    1612 00101$:
                           1613 ;	genPointerGet
                           1614 ;	genGenPointerGet
   033A 8A 82              1615 	mov	dpl,r2
   033C 8B 83              1616 	mov	dph,r3
   033E 8C F0              1617 	mov	b,r4
   0340 12 25 13           1618 	lcall	__gptrget
                           1619 ;	genIfx
   0343 FD                 1620 	mov	r5,a
                           1621 ;	Peephole 105	removed redundant mov
                           1622 ;	genIfxJump
                           1623 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0344 60 23              1624 	jz	00108$
                           1625 ;	Peephole 300	removed redundant label 00109$
                           1626 ;	main.c:249: lcdputch(*ptr);
                           1627 ;	genCall
   0346 8D 82              1628 	mov	dpl,r5
   0348 C0 02              1629 	push	ar2
   034A C0 03              1630 	push	ar3
   034C C0 04              1631 	push	ar4
   034E 12 02 7D           1632 	lcall	_lcdputch
   0351 D0 04              1633 	pop	ar4
   0353 D0 03              1634 	pop	ar3
   0355 D0 02              1635 	pop	ar2
                           1636 ;	main.c:250: ptr++;
                           1637 ;	genPlus
                           1638 ;     genPlusIncr
   0357 0A                 1639 	inc	r2
   0358 BA 00 01           1640 	cjne	r2,#0x00,00110$
   035B 0B                 1641 	inc	r3
   035C                    1642 00110$:
                           1643 ;	genAssign
   035C 90 00 24           1644 	mov	dptr,#_lcdputstr_ptr_1_1
   035F EA                 1645 	mov	a,r2
   0360 F0                 1646 	movx	@dptr,a
   0361 A3                 1647 	inc	dptr
   0362 EB                 1648 	mov	a,r3
   0363 F0                 1649 	movx	@dptr,a
   0364 A3                 1650 	inc	dptr
   0365 EC                 1651 	mov	a,r4
   0366 F0                 1652 	movx	@dptr,a
                           1653 ;	Peephole 112.b	changed ljmp to sjmp
   0367 80 D1              1654 	sjmp	00101$
   0369                    1655 00108$:
                           1656 ;	genAssign
   0369 90 00 24           1657 	mov	dptr,#_lcdputstr_ptr_1_1
   036C EA                 1658 	mov	a,r2
   036D F0                 1659 	movx	@dptr,a
   036E A3                 1660 	inc	dptr
   036F EB                 1661 	mov	a,r3
   0370 F0                 1662 	movx	@dptr,a
   0371 A3                 1663 	inc	dptr
   0372 EC                 1664 	mov	a,r4
   0373 F0                 1665 	movx	@dptr,a
                           1666 ;	Peephole 300	removed redundant label 00104$
   0374 22                 1667 	ret
                           1668 ;------------------------------------------------------------
                           1669 ;Allocation info for local variables in function 'lcdinit'
                           1670 ;------------------------------------------------------------
                           1671 ;------------------------------------------------------------
                           1672 ;	main.c:259: void lcdinit()
                           1673 ;	-----------------------------------------
                           1674 ;	 function lcdinit
                           1675 ;	-----------------------------------------
   0375                    1676 _lcdinit:
                           1677 ;	main.c:261: MSDelay(20);
                           1678 ;	genCall
                           1679 ;	Peephole 182.b	used 16 bit load of dptr
   0375 90 00 14           1680 	mov	dptr,#0x0014
   0378 12 00 77           1681 	lcall	_MSDelay
                           1682 ;	main.c:262: *INST_WRITE = UNLOCK_COMMAND;
                           1683 ;	genAssign
                           1684 ;	Peephole 182.b	used 16 bit load of dptr
   037B 90 F0 00           1685 	mov	dptr,#0xF000
                           1686 ;	genPointerSet
                           1687 ;     genFarPointerSet
   037E 74 30              1688 	mov	a,#0x30
   0380 F0                 1689 	movx	@dptr,a
                           1690 ;	main.c:263: MSDelay(8);
                           1691 ;	genCall
                           1692 ;	Peephole 182.b	used 16 bit load of dptr
   0381 90 00 08           1693 	mov	dptr,#0x0008
   0384 12 00 77           1694 	lcall	_MSDelay
                           1695 ;	main.c:264: *INST_WRITE = UNLOCK_COMMAND;
                           1696 ;	genAssign
                           1697 ;	Peephole 182.b	used 16 bit load of dptr
   0387 90 F0 00           1698 	mov	dptr,#0xF000
                           1699 ;	genPointerSet
                           1700 ;     genFarPointerSet
   038A 74 30              1701 	mov	a,#0x30
   038C F0                 1702 	movx	@dptr,a
                           1703 ;	main.c:265: MSDelay(3);
                           1704 ;	genCall
                           1705 ;	Peephole 182.b	used 16 bit load of dptr
   038D 90 00 03           1706 	mov	dptr,#0x0003
   0390 12 00 77           1707 	lcall	_MSDelay
                           1708 ;	main.c:266: *INST_WRITE = UNLOCK_COMMAND;
                           1709 ;	genAssign
                           1710 ;	Peephole 182.b	used 16 bit load of dptr
   0393 90 F0 00           1711 	mov	dptr,#0xF000
                           1712 ;	genPointerSet
                           1713 ;     genFarPointerSet
   0396 74 30              1714 	mov	a,#0x30
   0398 F0                 1715 	movx	@dptr,a
                           1716 ;	main.c:267: lcdbusywait();
                           1717 ;	genCall
   0399 12 01 00           1718 	lcall	_lcdbusywait
                           1719 ;	main.c:268: *INST_WRITE = FUNCTION_SET_LINES_FONT;
                           1720 ;	genAssign
                           1721 ;	Peephole 182.b	used 16 bit load of dptr
   039C 90 F0 00           1722 	mov	dptr,#0xF000
                           1723 ;	genPointerSet
                           1724 ;     genFarPointerSet
   039F 74 38              1725 	mov	a,#0x38
   03A1 F0                 1726 	movx	@dptr,a
                           1727 ;	main.c:269: lcdbusywait();
                           1728 ;	genCall
   03A2 12 01 00           1729 	lcall	_lcdbusywait
                           1730 ;	main.c:270: *INST_WRITE = DISPLAY_OFF;
                           1731 ;	genAssign
                           1732 ;	Peephole 182.b	used 16 bit load of dptr
   03A5 90 F0 00           1733 	mov	dptr,#0xF000
                           1734 ;	genPointerSet
                           1735 ;     genFarPointerSet
   03A8 74 08              1736 	mov	a,#0x08
   03AA F0                 1737 	movx	@dptr,a
                           1738 ;	main.c:271: lcdbusywait();
                           1739 ;	genCall
   03AB 12 01 00           1740 	lcall	_lcdbusywait
                           1741 ;	main.c:272: *INST_WRITE = DISPLAY_ON;
                           1742 ;	genAssign
                           1743 ;	Peephole 182.b	used 16 bit load of dptr
   03AE 90 F0 00           1744 	mov	dptr,#0xF000
                           1745 ;	genPointerSet
                           1746 ;     genFarPointerSet
   03B1 74 0C              1747 	mov	a,#0x0C
   03B3 F0                 1748 	movx	@dptr,a
                           1749 ;	main.c:273: lcdbusywait();
                           1750 ;	genCall
   03B4 12 01 00           1751 	lcall	_lcdbusywait
                           1752 ;	main.c:274: *INST_WRITE = ENTRY_MODE_SET;
                           1753 ;	genAssign
                           1754 ;	Peephole 182.b	used 16 bit load of dptr
   03B7 90 F0 00           1755 	mov	dptr,#0xF000
                           1756 ;	genPointerSet
                           1757 ;     genFarPointerSet
   03BA 74 06              1758 	mov	a,#0x06
   03BC F0                 1759 	movx	@dptr,a
                           1760 ;	main.c:275: lcdbusywait();
                           1761 ;	genCall
   03BD 12 01 00           1762 	lcall	_lcdbusywait
                           1763 ;	main.c:276: *INST_WRITE = CLEARSCREEN_CURSOR_HOME;
                           1764 ;	genAssign
                           1765 ;	Peephole 182.b	used 16 bit load of dptr
   03C0 90 F0 00           1766 	mov	dptr,#0xF000
                           1767 ;	genPointerSet
                           1768 ;     genFarPointerSet
   03C3 74 01              1769 	mov	a,#0x01
   03C5 F0                 1770 	movx	@dptr,a
                           1771 ;	main.c:277: lcdbusywait();
                           1772 ;	genCall
                           1773 ;	Peephole 253.b	replaced lcall/ret with ljmp
   03C6 02 01 00           1774 	ljmp	_lcdbusywait
                           1775 ;
                           1776 ;------------------------------------------------------------
                           1777 ;Allocation info for local variables in function 'cgramdump'
                           1778 ;------------------------------------------------------------
                           1779 ;count_row                 Allocated with name '_cgramdump_count_row_1_1'
                           1780 ;count                     Allocated with name '_cgramdump_count_1_1'
                           1781 ;temp_val                  Allocated with name '_cgramdump_temp_val_1_1'
                           1782 ;k                         Allocated with name '_cgramdump_k_1_1'
                           1783 ;------------------------------------------------------------
                           1784 ;	main.c:281: void cgramdump()
                           1785 ;	-----------------------------------------
                           1786 ;	 function cgramdump
                           1787 ;	-----------------------------------------
   03C9                    1788 _cgramdump:
                           1789 ;	main.c:285: unsigned char temp_val=0x40, k;
                           1790 ;	genAssign
   03C9 90 00 27           1791 	mov	dptr,#_cgramdump_temp_val_1_1
   03CC 74 40              1792 	mov	a,#0x40
   03CE F0                 1793 	movx	@dptr,a
                           1794 ;	main.c:286: EA=0;
                           1795 ;	genAssign
   03CF C2 AF              1796 	clr	_EA
                           1797 ;	main.c:287: k=*INST_READ;
                           1798 ;	genPointerGet
                           1799 ;	genFarPointerGet
                           1800 ;	Peephole 182.b	used 16 bit load of dptr
   03D1 90 F8 00           1801 	mov	dptr,#0xF800
   03D4 E0                 1802 	movx	a,@dptr
   03D5 FA                 1803 	mov	r2,a
                           1804 ;	main.c:289: EA=1;
                           1805 ;	genAssign
   03D6 D2 AF              1806 	setb	_EA
                           1807 ;	main.c:290: lcdbusywait();
                           1808 ;	genCall
   03D8 C0 02              1809 	push	ar2
   03DA 12 01 00           1810 	lcall	_lcdbusywait
   03DD D0 02              1811 	pop	ar2
                           1812 ;	main.c:291: putstr("\n\r");
                           1813 ;	genCall
                           1814 ;	Peephole 182.a	used 16 bit load of DPTR
   03DF 90 25 9C           1815 	mov	dptr,#__str_3
   03E2 75 F0 80           1816 	mov	b,#0x80
   03E5 C0 02              1817 	push	ar2
   03E7 12 00 AA           1818 	lcall	_putstr
   03EA D0 02              1819 	pop	ar2
                           1820 ;	main.c:292: putstr("CGRAM values\n\r");
                           1821 ;	genCall
                           1822 ;	Peephole 182.a	used 16 bit load of DPTR
   03EC 90 25 9F           1823 	mov	dptr,#__str_4
   03EF 75 F0 80           1824 	mov	b,#0x80
   03F2 C0 02              1825 	push	ar2
   03F4 12 00 AA           1826 	lcall	_putstr
   03F7 D0 02              1827 	pop	ar2
                           1828 ;	main.c:293: putstr("\n\rCCODE |  Values\n\r");
                           1829 ;	genCall
                           1830 ;	Peephole 182.a	used 16 bit load of DPTR
   03F9 90 25 AE           1831 	mov	dptr,#__str_5
   03FC 75 F0 80           1832 	mov	b,#0x80
   03FF C0 02              1833 	push	ar2
   0401 12 00 AA           1834 	lcall	_putstr
   0404 D0 02              1835 	pop	ar2
                           1836 ;	main.c:294: for (count_row=0; count_row<=7; count_row++)
                           1837 ;	genAssign
   0406 7B 00              1838 	mov	r3,#0x00
   0408 7C 00              1839 	mov	r4,#0x00
   040A                    1840 00105$:
                           1841 ;	genCmpGt
                           1842 ;	genCmp
   040A C3                 1843 	clr	c
   040B 74 07              1844 	mov	a,#0x07
   040D 9B                 1845 	subb	a,r3
                           1846 ;	Peephole 181	changed mov to clr
   040E E4                 1847 	clr	a
   040F 9C                 1848 	subb	a,r4
                           1849 ;	genIfxJump
   0410 50 03              1850 	jnc	00117$
   0412 02 04 FA           1851 	ljmp	00108$
   0415                    1852 00117$:
                           1853 ;	main.c:296: printf("0x%04x:  ", (temp_val&CGRAM_ADDR_MASK_BITS));
                           1854 ;	genAssign
   0415 90 00 27           1855 	mov	dptr,#_cgramdump_temp_val_1_1
   0418 E0                 1856 	movx	a,@dptr
   0419 FD                 1857 	mov	r5,a
                           1858 ;	genAnd
   041A 74 3F              1859 	mov	a,#0x3F
   041C 5D                 1860 	anl	a,r5
   041D FE                 1861 	mov	r6,a
                           1862 ;	genCast
   041E 7F 00              1863 	mov	r7,#0x00
                           1864 ;	genIpush
   0420 C0 02              1865 	push	ar2
   0422 C0 03              1866 	push	ar3
   0424 C0 04              1867 	push	ar4
   0426 C0 05              1868 	push	ar5
   0428 C0 06              1869 	push	ar6
   042A C0 07              1870 	push	ar7
                           1871 ;	genIpush
   042C 74 C2              1872 	mov	a,#__str_6
   042E C0 E0              1873 	push	acc
   0430 74 25              1874 	mov	a,#(__str_6 >> 8)
   0432 C0 E0              1875 	push	acc
   0434 74 80              1876 	mov	a,#0x80
   0436 C0 E0              1877 	push	acc
                           1878 ;	genCall
   0438 12 1C 74           1879 	lcall	_printf
   043B E5 81              1880 	mov	a,sp
   043D 24 FB              1881 	add	a,#0xfb
   043F F5 81              1882 	mov	sp,a
   0441 D0 05              1883 	pop	ar5
   0443 D0 04              1884 	pop	ar4
   0445 D0 03              1885 	pop	ar3
   0447 D0 02              1886 	pop	ar2
                           1887 ;	main.c:297: for(count=0; count<=7; count++)
                           1888 ;	genAssign
                           1889 ;	genAssign
   0449 7E 00              1890 	mov	r6,#0x00
   044B 7F 00              1891 	mov	r7,#0x00
   044D                    1892 00101$:
                           1893 ;	genCmpGt
                           1894 ;	genCmp
   044D C3                 1895 	clr	c
   044E 74 07              1896 	mov	a,#0x07
   0450 9E                 1897 	subb	a,r6
                           1898 ;	Peephole 181	changed mov to clr
   0451 E4                 1899 	clr	a
   0452 9F                 1900 	subb	a,r7
                           1901 ;	genIfxJump
   0453 50 03              1902 	jnc	00118$
   0455 02 04 D8           1903 	ljmp	00115$
   0458                    1904 00118$:
                           1905 ;	main.c:299: ET0=0;
                           1906 ;	genAssign
   0458 C2 A9              1907 	clr	_ET0
                           1908 ;	main.c:300: *INST_WRITE = temp_val;
                           1909 ;	genAssign
                           1910 ;	Peephole 182.b	used 16 bit load of dptr
   045A 90 F0 00           1911 	mov	dptr,#0xF000
                           1912 ;	genPointerSet
                           1913 ;     genFarPointerSet
   045D ED                 1914 	mov	a,r5
   045E F0                 1915 	movx	@dptr,a
                           1916 ;	main.c:301: lcdbusywait();
                           1917 ;	genCall
   045F C0 02              1918 	push	ar2
   0461 C0 03              1919 	push	ar3
   0463 C0 04              1920 	push	ar4
   0465 C0 05              1921 	push	ar5
   0467 C0 06              1922 	push	ar6
   0469 C0 07              1923 	push	ar7
   046B 12 01 00           1924 	lcall	_lcdbusywait
   046E D0 07              1925 	pop	ar7
   0470 D0 06              1926 	pop	ar6
   0472 D0 05              1927 	pop	ar5
   0474 D0 04              1928 	pop	ar4
   0476 D0 03              1929 	pop	ar3
   0478 D0 02              1930 	pop	ar2
                           1931 ;	main.c:302: printf("%02x  ",*DATA_READ);
                           1932 ;	genPointerGet
                           1933 ;	genFarPointerGet
                           1934 ;	Peephole 182.b	used 16 bit load of dptr
   047A 90 FC 00           1935 	mov	dptr,#0xFC00
   047D E0                 1936 	movx	a,@dptr
   047E F8                 1937 	mov	r0,a
                           1938 ;	genCast
   047F 79 00              1939 	mov	r1,#0x00
                           1940 ;	genIpush
   0481 C0 02              1941 	push	ar2
   0483 C0 03              1942 	push	ar3
   0485 C0 04              1943 	push	ar4
   0487 C0 05              1944 	push	ar5
   0489 C0 06              1945 	push	ar6
   048B C0 07              1946 	push	ar7
   048D C0 00              1947 	push	ar0
   048F C0 01              1948 	push	ar1
                           1949 ;	genIpush
   0491 74 CC              1950 	mov	a,#__str_7
   0493 C0 E0              1951 	push	acc
   0495 74 25              1952 	mov	a,#(__str_7 >> 8)
   0497 C0 E0              1953 	push	acc
   0499 74 80              1954 	mov	a,#0x80
   049B C0 E0              1955 	push	acc
                           1956 ;	genCall
   049D 12 1C 74           1957 	lcall	_printf
   04A0 E5 81              1958 	mov	a,sp
   04A2 24 FB              1959 	add	a,#0xfb
   04A4 F5 81              1960 	mov	sp,a
   04A6 D0 07              1961 	pop	ar7
   04A8 D0 06              1962 	pop	ar6
   04AA D0 05              1963 	pop	ar5
   04AC D0 04              1964 	pop	ar4
   04AE D0 03              1965 	pop	ar3
   04B0 D0 02              1966 	pop	ar2
                           1967 ;	main.c:303: ET0=1;
                           1968 ;	genAssign
   04B2 D2 A9              1969 	setb	_ET0
                           1970 ;	main.c:304: temp_val++;
                           1971 ;	genPlus
                           1972 ;     genPlusIncr
   04B4 0D                 1973 	inc	r5
                           1974 ;	main.c:305: lcdbusywait();
                           1975 ;	genCall
   04B5 C0 02              1976 	push	ar2
   04B7 C0 03              1977 	push	ar3
   04B9 C0 04              1978 	push	ar4
   04BB C0 05              1979 	push	ar5
   04BD C0 06              1980 	push	ar6
   04BF C0 07              1981 	push	ar7
   04C1 12 01 00           1982 	lcall	_lcdbusywait
   04C4 D0 07              1983 	pop	ar7
   04C6 D0 06              1984 	pop	ar6
   04C8 D0 05              1985 	pop	ar5
   04CA D0 04              1986 	pop	ar4
   04CC D0 03              1987 	pop	ar3
   04CE D0 02              1988 	pop	ar2
                           1989 ;	main.c:297: for(count=0; count<=7; count++)
                           1990 ;	genPlus
                           1991 ;     genPlusIncr
   04D0 0E                 1992 	inc	r6
   04D1 BE 00 01           1993 	cjne	r6,#0x00,00119$
   04D4 0F                 1994 	inc	r7
   04D5                    1995 00119$:
   04D5 02 04 4D           1996 	ljmp	00101$
   04D8                    1997 00115$:
                           1998 ;	genAssign
   04D8 90 00 27           1999 	mov	dptr,#_cgramdump_temp_val_1_1
   04DB ED                 2000 	mov	a,r5
   04DC F0                 2001 	movx	@dptr,a
                           2002 ;	main.c:307: putstr("\n\r");
                           2003 ;	genCall
                           2004 ;	Peephole 182.a	used 16 bit load of DPTR
   04DD 90 25 9C           2005 	mov	dptr,#__str_3
   04E0 75 F0 80           2006 	mov	b,#0x80
   04E3 C0 02              2007 	push	ar2
   04E5 C0 03              2008 	push	ar3
   04E7 C0 04              2009 	push	ar4
   04E9 12 00 AA           2010 	lcall	_putstr
   04EC D0 04              2011 	pop	ar4
   04EE D0 03              2012 	pop	ar3
   04F0 D0 02              2013 	pop	ar2
                           2014 ;	main.c:294: for (count_row=0; count_row<=7; count_row++)
                           2015 ;	genPlus
                           2016 ;     genPlusIncr
   04F2 0B                 2017 	inc	r3
   04F3 BB 00 01           2018 	cjne	r3,#0x00,00120$
   04F6 0C                 2019 	inc	r4
   04F7                    2020 00120$:
   04F7 02 04 0A           2021 	ljmp	00105$
   04FA                    2022 00108$:
                           2023 ;	main.c:309: putstr("\n\r");
                           2024 ;	genCall
                           2025 ;	Peephole 182.a	used 16 bit load of DPTR
   04FA 90 25 9C           2026 	mov	dptr,#__str_3
   04FD 75 F0 80           2027 	mov	b,#0x80
   0500 C0 02              2028 	push	ar2
   0502 12 00 AA           2029 	lcall	_putstr
   0505 D0 02              2030 	pop	ar2
                           2031 ;	main.c:310: EA=0;
                           2032 ;	genAssign
   0507 C2 AF              2033 	clr	_EA
                           2034 ;	main.c:311: *INST_WRITE=(k|DDRAM_WRITE_MASK_BITS);
                           2035 ;	genAssign
                           2036 ;	Peephole 182.b	used 16 bit load of dptr
   0509 90 F0 00           2037 	mov	dptr,#0xF000
                           2038 ;	genOr
   050C 43 02 80           2039 	orl	ar2,#0x80
                           2040 ;	genPointerSet
                           2041 ;     genFarPointerSet
   050F EA                 2042 	mov	a,r2
   0510 F0                 2043 	movx	@dptr,a
                           2044 ;	main.c:312: EA=1;
                           2045 ;	genAssign
   0511 D2 AF              2046 	setb	_EA
                           2047 ;	main.c:313: lcdbusywait();
                           2048 ;	genCall
                           2049 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0513 02 01 00           2050 	ljmp	_lcdbusywait
                           2051 ;
                           2052 ;------------------------------------------------------------
                           2053 ;Allocation info for local variables in function 'lcdcreatechar'
                           2054 ;------------------------------------------------------------
                           2055 ;sloc0                     Allocated with name '_lcdcreatechar_sloc0_1_0'
                           2056 ;row_vals                  Allocated with name '_lcdcreatechar_PARM_2'
                           2057 ;ccode                     Allocated with name '_lcdcreatechar_ccode_1_1'
                           2058 ;n                         Allocated with name '_lcdcreatechar_n_1_1'
                           2059 ;y                         Allocated with name '_lcdcreatechar_y_1_1'
                           2060 ;------------------------------------------------------------
                           2061 ;	main.c:318: void lcdcreatechar(unsigned char ccode, unsigned char row_vals[])
                           2062 ;	-----------------------------------------
                           2063 ;	 function lcdcreatechar
                           2064 ;	-----------------------------------------
   0516                    2065 _lcdcreatechar:
                           2066 ;	genReceive
   0516 E5 82              2067 	mov	a,dpl
   0518 90 00 2B           2068 	mov	dptr,#_lcdcreatechar_ccode_1_1
   051B F0                 2069 	movx	@dptr,a
                           2070 ;	main.c:321: EA=0;
                           2071 ;	genAssign
   051C C2 AF              2072 	clr	_EA
                           2073 ;	main.c:322: y=*INST_READ;
                           2074 ;	genPointerGet
                           2075 ;	genFarPointerGet
                           2076 ;	Peephole 182.b	used 16 bit load of dptr
   051E 90 F8 00           2077 	mov	dptr,#0xF800
   0521 E0                 2078 	movx	a,@dptr
   0522 FA                 2079 	mov	r2,a
                           2080 ;	main.c:323: EA=1;
                           2081 ;	genAssign
   0523 D2 AF              2082 	setb	_EA
                           2083 ;	main.c:324: lcdbusywait();
                           2084 ;	genCall
   0525 C0 02              2085 	push	ar2
   0527 12 01 00           2086 	lcall	_lcdbusywait
   052A D0 02              2087 	pop	ar2
                           2088 ;	main.c:325: ccode |= 0x40;
                           2089 ;	genAssign
                           2090 ;	genOr
   052C 90 00 2B           2091 	mov	dptr,#_lcdcreatechar_ccode_1_1
   052F E0                 2092 	movx	a,@dptr
   0530 FB                 2093 	mov	r3,a
                           2094 ;	Peephole 248.a	optimized or to xdata
   0531 44 40              2095 	orl	a,#0x40
   0533 F0                 2096 	movx	@dptr,a
                           2097 ;	main.c:326: for (n=0; n<=7; n++)
                           2098 ;	genAssign
   0534 90 00 28           2099 	mov	dptr,#_lcdcreatechar_PARM_2
   0537 E0                 2100 	movx	a,@dptr
   0538 FB                 2101 	mov	r3,a
   0539 A3                 2102 	inc	dptr
   053A E0                 2103 	movx	a,@dptr
   053B FC                 2104 	mov	r4,a
   053C A3                 2105 	inc	dptr
   053D E0                 2106 	movx	a,@dptr
   053E FD                 2107 	mov	r5,a
                           2108 ;	genAssign
   053F 90 00 2B           2109 	mov	dptr,#_lcdcreatechar_ccode_1_1
   0542 E0                 2110 	movx	a,@dptr
   0543 FE                 2111 	mov	r6,a
                           2112 ;	genAssign
   0544 7F 00              2113 	mov	r7,#0x00
   0546                    2114 00101$:
                           2115 ;	genCmpGt
                           2116 ;	genCmp
                           2117 ;	genIfxJump
                           2118 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   0546 EF                 2119 	mov	a,r7
   0547 24 F8              2120 	add	a,#0xff - 0x07
                           2121 ;	Peephole 112.b	changed ljmp to sjmp
                           2122 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0549 40 6F              2123 	jc	00109$
                           2124 ;	Peephole 300	removed redundant label 00110$
                           2125 ;	main.c:328: ET0=0;
                           2126 ;	genIpush
   054B C0 02              2127 	push	ar2
                           2128 ;	genAssign
   054D C2 A9              2129 	clr	_ET0
                           2130 ;	main.c:329: *INST_WRITE = ccode;
                           2131 ;	genAssign
                           2132 ;	Peephole 182.b	used 16 bit load of dptr
   054F 90 F0 00           2133 	mov	dptr,#0xF000
                           2134 ;	genPointerSet
                           2135 ;     genFarPointerSet
   0552 EE                 2136 	mov	a,r6
   0553 F0                 2137 	movx	@dptr,a
                           2138 ;	main.c:330: lcdbusywait();
                           2139 ;	genCall
   0554 C0 02              2140 	push	ar2
   0556 C0 03              2141 	push	ar3
   0558 C0 04              2142 	push	ar4
   055A C0 05              2143 	push	ar5
   055C C0 06              2144 	push	ar6
   055E C0 07              2145 	push	ar7
   0560 12 01 00           2146 	lcall	_lcdbusywait
   0563 D0 07              2147 	pop	ar7
   0565 D0 06              2148 	pop	ar6
   0567 D0 05              2149 	pop	ar5
   0569 D0 04              2150 	pop	ar4
   056B D0 03              2151 	pop	ar3
   056D D0 02              2152 	pop	ar2
                           2153 ;	main.c:331: *DATA_WRITE = (row_vals[n]&CGRAM_PIXEL_MASK_BITS);
                           2154 ;	genAssign
   056F 75 22 00           2155 	mov	_lcdcreatechar_sloc0_1_0,#0x00
   0572 75 23 F4           2156 	mov	(_lcdcreatechar_sloc0_1_0 + 1),#0xF4
                           2157 ;	genPlus
                           2158 ;	Peephole 236.g	used r7 instead of ar7
   0575 EF                 2159 	mov	a,r7
                           2160 ;	Peephole 236.a	used r3 instead of ar3
   0576 2B                 2161 	add	a,r3
   0577 FA                 2162 	mov	r2,a
                           2163 ;	Peephole 181	changed mov to clr
   0578 E4                 2164 	clr	a
                           2165 ;	Peephole 236.b	used r4 instead of ar4
   0579 3C                 2166 	addc	a,r4
   057A F8                 2167 	mov	r0,a
   057B 8D 01              2168 	mov	ar1,r5
                           2169 ;	genPointerGet
                           2170 ;	genGenPointerGet
   057D 8A 82              2171 	mov	dpl,r2
   057F 88 83              2172 	mov	dph,r0
   0581 89 F0              2173 	mov	b,r1
   0583 12 25 13           2174 	lcall	__gptrget
   0586 FA                 2175 	mov	r2,a
                           2176 ;	genAnd
   0587 53 02 1F           2177 	anl	ar2,#0x1F
                           2178 ;	genPointerSet
                           2179 ;     genFarPointerSet
   058A 85 22 82           2180 	mov	dpl,_lcdcreatechar_sloc0_1_0
   058D 85 23 83           2181 	mov	dph,(_lcdcreatechar_sloc0_1_0 + 1)
   0590 EA                 2182 	mov	a,r2
   0591 F0                 2183 	movx	@dptr,a
                           2184 ;	main.c:332: ET0=1;
                           2185 ;	genAssign
   0592 D2 A9              2186 	setb	_ET0
                           2187 ;	main.c:333: lcdbusywait();
                           2188 ;	genCall
   0594 C0 02              2189 	push	ar2
   0596 C0 03              2190 	push	ar3
   0598 C0 04              2191 	push	ar4
   059A C0 05              2192 	push	ar5
   059C C0 06              2193 	push	ar6
   059E C0 07              2194 	push	ar7
   05A0 12 01 00           2195 	lcall	_lcdbusywait
   05A3 D0 07              2196 	pop	ar7
   05A5 D0 06              2197 	pop	ar6
   05A7 D0 05              2198 	pop	ar5
   05A9 D0 04              2199 	pop	ar4
   05AB D0 03              2200 	pop	ar3
   05AD D0 02              2201 	pop	ar2
                           2202 ;	main.c:334: ccode++;
                           2203 ;	genPlus
                           2204 ;     genPlusIncr
   05AF 0E                 2205 	inc	r6
                           2206 ;	genAssign
   05B0 90 00 2B           2207 	mov	dptr,#_lcdcreatechar_ccode_1_1
   05B3 EE                 2208 	mov	a,r6
   05B4 F0                 2209 	movx	@dptr,a
                           2210 ;	main.c:326: for (n=0; n<=7; n++)
                           2211 ;	genPlus
                           2212 ;     genPlusIncr
   05B5 0F                 2213 	inc	r7
                           2214 ;	genIpop
   05B6 D0 02              2215 	pop	ar2
                           2216 ;	Peephole 112.b	changed ljmp to sjmp
   05B8 80 8C              2217 	sjmp	00101$
   05BA                    2218 00109$:
                           2219 ;	genAssign
   05BA 90 00 2B           2220 	mov	dptr,#_lcdcreatechar_ccode_1_1
   05BD EE                 2221 	mov	a,r6
   05BE F0                 2222 	movx	@dptr,a
                           2223 ;	main.c:336: EA=0;
                           2224 ;	genAssign
   05BF C2 AF              2225 	clr	_EA
                           2226 ;	main.c:337: *INST_WRITE=(y|DDRAM_WRITE_MASK_BITS);
                           2227 ;	genAssign
                           2228 ;	Peephole 182.b	used 16 bit load of dptr
   05C1 90 F0 00           2229 	mov	dptr,#0xF000
                           2230 ;	genOr
   05C4 43 02 80           2231 	orl	ar2,#0x80
                           2232 ;	genPointerSet
                           2233 ;     genFarPointerSet
   05C7 EA                 2234 	mov	a,r2
   05C8 F0                 2235 	movx	@dptr,a
                           2236 ;	main.c:338: EA=1;
                           2237 ;	genAssign
   05C9 D2 AF              2238 	setb	_EA
                           2239 ;	main.c:339: lcdbusywait();
                           2240 ;	genCall
                           2241 ;	Peephole 253.b	replaced lcall/ret with ljmp
   05CB 02 01 00           2242 	ljmp	_lcdbusywait
                           2243 ;
                           2244 ;------------------------------------------------------------
                           2245 ;Allocation info for local variables in function 'ddramdump'
                           2246 ;------------------------------------------------------------
                           2247 ;count_row                 Allocated with name '_ddramdump_count_row_1_1'
                           2248 ;count                     Allocated with name '_ddramdump_count_1_1'
                           2249 ;temp_val                  Allocated with name '_ddramdump_temp_val_1_1'
                           2250 ;k                         Allocated with name '_ddramdump_k_1_1'
                           2251 ;------------------------------------------------------------
                           2252 ;	main.c:343: void ddramdump()
                           2253 ;	-----------------------------------------
                           2254 ;	 function ddramdump
                           2255 ;	-----------------------------------------
   05CE                    2256 _ddramdump:
                           2257 ;	main.c:347: unsigned char temp_val=0, k;
                           2258 ;	genAssign
   05CE 90 00 2C           2259 	mov	dptr,#_ddramdump_temp_val_1_1
                           2260 ;	Peephole 181	changed mov to clr
   05D1 E4                 2261 	clr	a
   05D2 F0                 2262 	movx	@dptr,a
                           2263 ;	main.c:348: EA=0;
                           2264 ;	genAssign
   05D3 C2 AF              2265 	clr	_EA
                           2266 ;	main.c:349: k=*INST_READ;
                           2267 ;	genPointerGet
                           2268 ;	genFarPointerGet
                           2269 ;	Peephole 182.b	used 16 bit load of dptr
   05D5 90 F8 00           2270 	mov	dptr,#0xF800
   05D8 E0                 2271 	movx	a,@dptr
   05D9 FA                 2272 	mov	r2,a
                           2273 ;	main.c:350: EA=1;
                           2274 ;	genAssign
   05DA D2 AF              2275 	setb	_EA
                           2276 ;	main.c:351: putstr("\n\r");
                           2277 ;	genCall
                           2278 ;	Peephole 182.a	used 16 bit load of DPTR
   05DC 90 25 9C           2279 	mov	dptr,#__str_3
   05DF 75 F0 80           2280 	mov	b,#0x80
   05E2 C0 02              2281 	push	ar2
   05E4 12 00 AA           2282 	lcall	_putstr
   05E7 D0 02              2283 	pop	ar2
                           2284 ;	main.c:352: putstr("DDRAM Values\n\r");
                           2285 ;	genCall
                           2286 ;	Peephole 182.a	used 16 bit load of DPTR
   05E9 90 25 D3           2287 	mov	dptr,#__str_8
   05EC 75 F0 80           2288 	mov	b,#0x80
   05EF C0 02              2289 	push	ar2
   05F1 12 00 AA           2290 	lcall	_putstr
   05F4 D0 02              2291 	pop	ar2
                           2292 ;	main.c:353: putstr("\n\rADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r\n\r");
                           2293 ;	genCall
                           2294 ;	Peephole 182.a	used 16 bit load of DPTR
   05F6 90 25 E2           2295 	mov	dptr,#__str_9
   05F9 75 F0 80           2296 	mov	b,#0x80
   05FC C0 02              2297 	push	ar2
   05FE 12 00 AA           2298 	lcall	_putstr
   0601 D0 02              2299 	pop	ar2
                           2300 ;	main.c:354: for (count_row=0; count_row<=3; count_row++)
                           2301 ;	genAssign
   0603 7B 00              2302 	mov	r3,#0x00
   0605 7C 00              2303 	mov	r4,#0x00
   0607                    2304 00113$:
                           2305 ;	genCmpGt
                           2306 ;	genCmp
   0607 C3                 2307 	clr	c
   0608 74 03              2308 	mov	a,#0x03
   060A 9B                 2309 	subb	a,r3
                           2310 ;	Peephole 181	changed mov to clr
   060B E4                 2311 	clr	a
   060C 9C                 2312 	subb	a,r4
                           2313 ;	genIfxJump
   060D 50 03              2314 	jnc	00129$
   060F 02 07 29           2315 	ljmp	00116$
   0612                    2316 00129$:
                           2317 ;	main.c:356: if(count_row==0)
                           2318 ;	genIfx
   0612 EB                 2319 	mov	a,r3
   0613 4C                 2320 	orl	a,r4
                           2321 ;	genIfxJump
                           2322 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0614 70 06              2323 	jnz	00102$
                           2324 ;	Peephole 300	removed redundant label 00130$
                           2325 ;	main.c:357: temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW0);
                           2326 ;	genAssign
   0616 90 00 2C           2327 	mov	dptr,#_ddramdump_temp_val_1_1
   0619 74 80              2328 	mov	a,#0x80
   061B F0                 2329 	movx	@dptr,a
   061C                    2330 00102$:
                           2331 ;	main.c:358: if(count_row==1)
                           2332 ;	genCmpEq
                           2333 ;	gencjneshort
                           2334 ;	Peephole 112.b	changed ljmp to sjmp
                           2335 ;	Peephole 198.a	optimized misc jump sequence
   061C BB 01 09           2336 	cjne	r3,#0x01,00104$
   061F BC 00 06           2337 	cjne	r4,#0x00,00104$
                           2338 ;	Peephole 200.b	removed redundant sjmp
                           2339 ;	Peephole 300	removed redundant label 00131$
                           2340 ;	Peephole 300	removed redundant label 00132$
                           2341 ;	main.c:359: temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW1);
                           2342 ;	genAssign
   0622 90 00 2C           2343 	mov	dptr,#_ddramdump_temp_val_1_1
   0625 74 C0              2344 	mov	a,#0xC0
   0627 F0                 2345 	movx	@dptr,a
   0628                    2346 00104$:
                           2347 ;	main.c:360: if(count_row==2)
                           2348 ;	genCmpEq
                           2349 ;	gencjneshort
                           2350 ;	Peephole 112.b	changed ljmp to sjmp
                           2351 ;	Peephole 198.a	optimized misc jump sequence
   0628 BB 02 09           2352 	cjne	r3,#0x02,00106$
   062B BC 00 06           2353 	cjne	r4,#0x00,00106$
                           2354 ;	Peephole 200.b	removed redundant sjmp
                           2355 ;	Peephole 300	removed redundant label 00133$
                           2356 ;	Peephole 300	removed redundant label 00134$
                           2357 ;	main.c:361: temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW2);
                           2358 ;	genAssign
   062E 90 00 2C           2359 	mov	dptr,#_ddramdump_temp_val_1_1
   0631 74 90              2360 	mov	a,#0x90
   0633 F0                 2361 	movx	@dptr,a
   0634                    2362 00106$:
                           2363 ;	main.c:362: if(count_row==3)
                           2364 ;	genCmpEq
                           2365 ;	gencjneshort
                           2366 ;	Peephole 112.b	changed ljmp to sjmp
                           2367 ;	Peephole 198.a	optimized misc jump sequence
   0634 BB 03 09           2368 	cjne	r3,#0x03,00108$
   0637 BC 00 06           2369 	cjne	r4,#0x00,00108$
                           2370 ;	Peephole 200.b	removed redundant sjmp
                           2371 ;	Peephole 300	removed redundant label 00135$
                           2372 ;	Peephole 300	removed redundant label 00136$
                           2373 ;	main.c:363: temp_val=(DDRAM_WRITE_MASK_BITS | BASE_ADDRROW3);
                           2374 ;	genAssign
   063A 90 00 2C           2375 	mov	dptr,#_ddramdump_temp_val_1_1
   063D 74 D0              2376 	mov	a,#0xD0
   063F F0                 2377 	movx	@dptr,a
   0640                    2378 00108$:
                           2379 ;	main.c:364: printf("0x%02x:  ", (temp_val&DDRAM_ADDR_MASK_BITS));
                           2380 ;	genAssign
   0640 90 00 2C           2381 	mov	dptr,#_ddramdump_temp_val_1_1
   0643 E0                 2382 	movx	a,@dptr
   0644 FD                 2383 	mov	r5,a
                           2384 ;	genAnd
   0645 74 7F              2385 	mov	a,#0x7F
   0647 5D                 2386 	anl	a,r5
   0648 FE                 2387 	mov	r6,a
                           2388 ;	genCast
   0649 7F 00              2389 	mov	r7,#0x00
                           2390 ;	genIpush
   064B C0 02              2391 	push	ar2
   064D C0 03              2392 	push	ar3
   064F C0 04              2393 	push	ar4
   0651 C0 05              2394 	push	ar5
   0653 C0 06              2395 	push	ar6
   0655 C0 07              2396 	push	ar7
                           2397 ;	genIpush
   0657 74 2E              2398 	mov	a,#__str_10
   0659 C0 E0              2399 	push	acc
   065B 74 26              2400 	mov	a,#(__str_10 >> 8)
   065D C0 E0              2401 	push	acc
   065F 74 80              2402 	mov	a,#0x80
   0661 C0 E0              2403 	push	acc
                           2404 ;	genCall
   0663 12 1C 74           2405 	lcall	_printf
   0666 E5 81              2406 	mov	a,sp
   0668 24 FB              2407 	add	a,#0xfb
   066A F5 81              2408 	mov	sp,a
   066C D0 05              2409 	pop	ar5
   066E D0 04              2410 	pop	ar4
   0670 D0 03              2411 	pop	ar3
   0672 D0 02              2412 	pop	ar2
                           2413 ;	main.c:365: for(count=0; count<=15; count++)
                           2414 ;	genAssign
                           2415 ;	genAssign
   0674 7E 00              2416 	mov	r6,#0x00
   0676 7F 00              2417 	mov	r7,#0x00
   0678                    2418 00109$:
                           2419 ;	genCmpGt
                           2420 ;	genCmp
   0678 C3                 2421 	clr	c
   0679 74 0F              2422 	mov	a,#0x0F
   067B 9E                 2423 	subb	a,r6
                           2424 ;	Peephole 181	changed mov to clr
   067C E4                 2425 	clr	a
   067D 9F                 2426 	subb	a,r7
                           2427 ;	genIfxJump
   067E 50 03              2428 	jnc	00137$
   0680 02 07 07           2429 	ljmp	00127$
   0683                    2430 00137$:
                           2431 ;	main.c:367: EA=0;
                           2432 ;	genAssign
   0683 C2 AF              2433 	clr	_EA
                           2434 ;	main.c:368: *INST_WRITE = temp_val;
                           2435 ;	genAssign
                           2436 ;	Peephole 182.b	used 16 bit load of dptr
   0685 90 F0 00           2437 	mov	dptr,#0xF000
                           2438 ;	genPointerSet
                           2439 ;     genFarPointerSet
   0688 ED                 2440 	mov	a,r5
   0689 F0                 2441 	movx	@dptr,a
                           2442 ;	main.c:369: EA=1;
                           2443 ;	genAssign
   068A D2 AF              2444 	setb	_EA
                           2445 ;	main.c:370: lcdbusywait();
                           2446 ;	genCall
   068C C0 02              2447 	push	ar2
   068E C0 03              2448 	push	ar3
   0690 C0 04              2449 	push	ar4
   0692 C0 05              2450 	push	ar5
   0694 C0 06              2451 	push	ar6
   0696 C0 07              2452 	push	ar7
   0698 12 01 00           2453 	lcall	_lcdbusywait
   069B D0 07              2454 	pop	ar7
   069D D0 06              2455 	pop	ar6
   069F D0 05              2456 	pop	ar5
   06A1 D0 04              2457 	pop	ar4
   06A3 D0 03              2458 	pop	ar3
   06A5 D0 02              2459 	pop	ar2
                           2460 ;	main.c:371: temp_val++;
                           2461 ;	genPlus
                           2462 ;     genPlusIncr
   06A7 0D                 2463 	inc	r5
                           2464 ;	main.c:372: EA=0;
                           2465 ;	genAssign
   06A8 C2 AF              2466 	clr	_EA
                           2467 ;	main.c:373: printf("%02x  ",*DATA_READ);
                           2468 ;	genPointerGet
                           2469 ;	genFarPointerGet
                           2470 ;	Peephole 182.b	used 16 bit load of dptr
   06AA 90 FC 00           2471 	mov	dptr,#0xFC00
   06AD E0                 2472 	movx	a,@dptr
   06AE F8                 2473 	mov	r0,a
                           2474 ;	genCast
   06AF 79 00              2475 	mov	r1,#0x00
                           2476 ;	genIpush
   06B1 C0 02              2477 	push	ar2
   06B3 C0 03              2478 	push	ar3
   06B5 C0 04              2479 	push	ar4
   06B7 C0 05              2480 	push	ar5
   06B9 C0 06              2481 	push	ar6
   06BB C0 07              2482 	push	ar7
   06BD C0 00              2483 	push	ar0
   06BF C0 01              2484 	push	ar1
                           2485 ;	genIpush
   06C1 74 CC              2486 	mov	a,#__str_7
   06C3 C0 E0              2487 	push	acc
   06C5 74 25              2488 	mov	a,#(__str_7 >> 8)
   06C7 C0 E0              2489 	push	acc
   06C9 74 80              2490 	mov	a,#0x80
   06CB C0 E0              2491 	push	acc
                           2492 ;	genCall
   06CD 12 1C 74           2493 	lcall	_printf
   06D0 E5 81              2494 	mov	a,sp
   06D2 24 FB              2495 	add	a,#0xfb
   06D4 F5 81              2496 	mov	sp,a
   06D6 D0 07              2497 	pop	ar7
   06D8 D0 06              2498 	pop	ar6
   06DA D0 05              2499 	pop	ar5
   06DC D0 04              2500 	pop	ar4
   06DE D0 03              2501 	pop	ar3
   06E0 D0 02              2502 	pop	ar2
                           2503 ;	main.c:374: EA=1;
                           2504 ;	genAssign
   06E2 D2 AF              2505 	setb	_EA
                           2506 ;	main.c:375: lcdbusywait();
                           2507 ;	genCall
   06E4 C0 02              2508 	push	ar2
   06E6 C0 03              2509 	push	ar3
   06E8 C0 04              2510 	push	ar4
   06EA C0 05              2511 	push	ar5
   06EC C0 06              2512 	push	ar6
   06EE C0 07              2513 	push	ar7
   06F0 12 01 00           2514 	lcall	_lcdbusywait
   06F3 D0 07              2515 	pop	ar7
   06F5 D0 06              2516 	pop	ar6
   06F7 D0 05              2517 	pop	ar5
   06F9 D0 04              2518 	pop	ar4
   06FB D0 03              2519 	pop	ar3
   06FD D0 02              2520 	pop	ar2
                           2521 ;	main.c:365: for(count=0; count<=15; count++)
                           2522 ;	genPlus
                           2523 ;     genPlusIncr
   06FF 0E                 2524 	inc	r6
   0700 BE 00 01           2525 	cjne	r6,#0x00,00138$
   0703 0F                 2526 	inc	r7
   0704                    2527 00138$:
   0704 02 06 78           2528 	ljmp	00109$
   0707                    2529 00127$:
                           2530 ;	genAssign
   0707 90 00 2C           2531 	mov	dptr,#_ddramdump_temp_val_1_1
   070A ED                 2532 	mov	a,r5
   070B F0                 2533 	movx	@dptr,a
                           2534 ;	main.c:377: putstr("\n\r");
                           2535 ;	genCall
                           2536 ;	Peephole 182.a	used 16 bit load of DPTR
   070C 90 25 9C           2537 	mov	dptr,#__str_3
   070F 75 F0 80           2538 	mov	b,#0x80
   0712 C0 02              2539 	push	ar2
   0714 C0 03              2540 	push	ar3
   0716 C0 04              2541 	push	ar4
   0718 12 00 AA           2542 	lcall	_putstr
   071B D0 04              2543 	pop	ar4
   071D D0 03              2544 	pop	ar3
   071F D0 02              2545 	pop	ar2
                           2546 ;	main.c:354: for (count_row=0; count_row<=3; count_row++)
                           2547 ;	genPlus
                           2548 ;     genPlusIncr
   0721 0B                 2549 	inc	r3
   0722 BB 00 01           2550 	cjne	r3,#0x00,00139$
   0725 0C                 2551 	inc	r4
   0726                    2552 00139$:
   0726 02 06 07           2553 	ljmp	00113$
   0729                    2554 00116$:
                           2555 ;	main.c:379: putstr("\n\r");
                           2556 ;	genCall
                           2557 ;	Peephole 182.a	used 16 bit load of DPTR
   0729 90 25 9C           2558 	mov	dptr,#__str_3
   072C 75 F0 80           2559 	mov	b,#0x80
   072F C0 02              2560 	push	ar2
   0731 12 00 AA           2561 	lcall	_putstr
   0734 D0 02              2562 	pop	ar2
                           2563 ;	main.c:380: EA=0;
                           2564 ;	genAssign
   0736 C2 AF              2565 	clr	_EA
                           2566 ;	main.c:381: *INST_WRITE=(k|DDRAM_WRITE_MASK_BITS);
                           2567 ;	genAssign
                           2568 ;	Peephole 182.b	used 16 bit load of dptr
   0738 90 F0 00           2569 	mov	dptr,#0xF000
                           2570 ;	genOr
   073B 43 02 80           2571 	orl	ar2,#0x80
                           2572 ;	genPointerSet
                           2573 ;     genFarPointerSet
   073E EA                 2574 	mov	a,r2
   073F F0                 2575 	movx	@dptr,a
                           2576 ;	main.c:382: EA=1;
                           2577 ;	genAssign
   0740 D2 AF              2578 	setb	_EA
                           2579 ;	main.c:383: lcdbusywait();
                           2580 ;	genCall
                           2581 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0742 02 01 00           2582 	ljmp	_lcdbusywait
                           2583 ;
                           2584 ;------------------------------------------------------------
                           2585 ;Allocation info for local variables in function 'readinput'
                           2586 ;------------------------------------------------------------
                           2587 ;value                     Allocated with name '_readinput_value_1_1'
                           2588 ;i                         Allocated with name '_readinput_i_1_1'
                           2589 ;j                         Allocated with name '_readinput_j_1_1'
                           2590 ;------------------------------------------------------------
                           2591 ;	main.c:387: unsigned char readinput()
                           2592 ;	-----------------------------------------
                           2593 ;	 function readinput
                           2594 ;	-----------------------------------------
   0745                    2595 _readinput:
                           2596 ;	main.c:389: unsigned char value=0, i, j=0;
                           2597 ;	genAssign
   0745 90 00 2D           2598 	mov	dptr,#_readinput_value_1_1
                           2599 ;	Peephole 181	changed mov to clr
   0748 E4                 2600 	clr	a
   0749 F0                 2601 	movx	@dptr,a
                           2602 ;	main.c:390: do
                           2603 ;	genAssign
   074A 7A 00              2604 	mov	r2,#0x00
   074C                    2605 00110$:
                           2606 ;	main.c:392: i=getchar();
                           2607 ;	genCall
   074C C0 02              2608 	push	ar2
   074E 12 0C F1           2609 	lcall	_getchar
   0751 AB 82              2610 	mov	r3,dpl
   0753 D0 02              2611 	pop	ar2
                           2612 ;	main.c:393: if(i>='0'&&i<='1')          /* works only if its a number */
                           2613 ;	genAssign
   0755 8B 04              2614 	mov	ar4,r3
                           2615 ;	genCmpLt
                           2616 ;	genCmp
   0757 BC 30 00           2617 	cjne	r4,#0x30,00122$
   075A                    2618 00122$:
                           2619 ;	genIfxJump
                           2620 ;	Peephole 112.b	changed ljmp to sjmp
                           2621 ;	Peephole 160.a	removed sjmp by inverse jump logic
   075A 40 26              2622 	jc	00107$
                           2623 ;	Peephole 300	removed redundant label 00123$
                           2624 ;	genAssign
   075C 8B 04              2625 	mov	ar4,r3
                           2626 ;	genCmpGt
                           2627 ;	genCmp
                           2628 ;	genIfxJump
                           2629 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   075E EC                 2630 	mov	a,r4
   075F 24 CE              2631 	add	a,#0xff - 0x31
                           2632 ;	Peephole 112.b	changed ljmp to sjmp
                           2633 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0761 40 1F              2634 	jc	00107$
                           2635 ;	Peephole 300	removed redundant label 00124$
                           2636 ;	main.c:395: value=((value*2)+(i-'0')); /* Convert character to integers */
                           2637 ;	genAssign
   0763 90 00 2D           2638 	mov	dptr,#_readinput_value_1_1
   0766 E0                 2639 	movx	a,@dptr
                           2640 ;	genLeftShift
                           2641 ;	genLeftShiftLiteral
                           2642 ;	genlshOne
                           2643 ;	Peephole 105	removed redundant mov
                           2644 ;	Peephole 204	removed redundant mov
   0767 25 E0              2645 	add	a,acc
   0769 FC                 2646 	mov	r4,a
                           2647 ;	genMinus
   076A EB                 2648 	mov	a,r3
   076B 24 D0              2649 	add	a,#0xd0
                           2650 ;	genPlus
   076D 90 00 2D           2651 	mov	dptr,#_readinput_value_1_1
                           2652 ;	Peephole 236.a	used r4 instead of ar4
   0770 2C                 2653 	add	a,r4
   0771 F0                 2654 	movx	@dptr,a
                           2655 ;	main.c:396: putchar(i);
                           2656 ;	genCall
   0772 8B 82              2657 	mov	dpl,r3
   0774 C0 02              2658 	push	ar2
   0776 C0 03              2659 	push	ar3
   0778 12 0C DF           2660 	lcall	_putchar
   077B D0 03              2661 	pop	ar3
   077D D0 02              2662 	pop	ar2
                           2663 ;	main.c:397: j++;
                           2664 ;	genPlus
                           2665 ;     genPlusIncr
   077F 0A                 2666 	inc	r2
                           2667 ;	Peephole 112.b	changed ljmp to sjmp
   0780 80 27              2668 	sjmp	00111$
   0782                    2669 00107$:
                           2670 ;	main.c:399: else if(i==READ_BACKSPACE)
                           2671 ;	genCmpEq
                           2672 ;	gencjneshort
                           2673 ;	Peephole 112.b	changed ljmp to sjmp
                           2674 ;	Peephole 198.b	optimized misc jump sequence
   0782 BB 7F 0E           2675 	cjne	r3,#0x7F,00104$
                           2676 ;	Peephole 200.b	removed redundant sjmp
                           2677 ;	Peephole 300	removed redundant label 00125$
                           2678 ;	Peephole 300	removed redundant label 00126$
                           2679 ;	main.c:401: value=value/2;
                           2680 ;	genAssign
   0785 90 00 2D           2681 	mov	dptr,#_readinput_value_1_1
   0788 E0                 2682 	movx	a,@dptr
                           2683 ;	genRightShift
                           2684 ;	genRightShiftLiteral
                           2685 ;	genrshOne
   0789 FC                 2686 	mov	r4,a
                           2687 ;	Peephole 105	removed redundant mov
   078A C3                 2688 	clr	c
   078B 13                 2689 	rrc	a
                           2690 ;	genAssign
   078C FC                 2691 	mov	r4,a
   078D 90 00 2D           2692 	mov	dptr,#_readinput_value_1_1
                           2693 ;	Peephole 100	removed redundant mov
   0790 F0                 2694 	movx	@dptr,a
                           2695 ;	Peephole 112.b	changed ljmp to sjmp
   0791 80 16              2696 	sjmp	00111$
   0793                    2697 00104$:
                           2698 ;	main.c:403: else if(i!=READ_ENTER)
                           2699 ;	genCmpEq
                           2700 ;	gencjneshort
   0793 BB 0D 02           2701 	cjne	r3,#0x0D,00127$
                           2702 ;	Peephole 112.b	changed ljmp to sjmp
   0796 80 11              2703 	sjmp	00111$
   0798                    2704 00127$:
                           2705 ;	main.c:404: {putstr("\n\rEnter Valid Numbers\n\r");}
                           2706 ;	genCall
                           2707 ;	Peephole 182.a	used 16 bit load of DPTR
   0798 90 26 38           2708 	mov	dptr,#__str_11
   079B 75 F0 80           2709 	mov	b,#0x80
   079E C0 02              2710 	push	ar2
   07A0 C0 03              2711 	push	ar3
   07A2 12 00 AA           2712 	lcall	_putstr
   07A5 D0 03              2713 	pop	ar3
   07A7 D0 02              2714 	pop	ar2
   07A9                    2715 00111$:
                           2716 ;	main.c:405: }while(i!=READ_ENTER);
                           2717 ;	genCmpEq
                           2718 ;	gencjneshort
                           2719 ;	Peephole 112.b	changed ljmp to sjmp
                           2720 ;	Peephole 198.b	optimized misc jump sequence
   07A9 BB 0D A0           2721 	cjne	r3,#0x0D,00110$
                           2722 ;	Peephole 200.b	removed redundant sjmp
                           2723 ;	Peephole 300	removed redundant label 00128$
                           2724 ;	Peephole 300	removed redundant label 00129$
                           2725 ;	main.c:407: return value;
                           2726 ;	genAssign
   07AC 90 00 2D           2727 	mov	dptr,#_readinput_value_1_1
   07AF E0                 2728 	movx	a,@dptr
                           2729 ;	genRet
                           2730 ;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
   07B0 F5 82              2731 	mov	dpl,a
                           2732 ;	Peephole 300	removed redundant label 00113$
   07B2 22                 2733 	ret
                           2734 ;------------------------------------------------------------
                           2735 ;Allocation info for local variables in function 'addcharacter'
                           2736 ;------------------------------------------------------------
                           2737 ;temp                      Allocated with name '_addcharacter_temp_1_1'
                           2738 ;y                         Allocated with name '_addcharacter_y_1_1'
                           2739 ;------------------------------------------------------------
                           2740 ;	main.c:411: void addcharacter()
                           2741 ;	-----------------------------------------
                           2742 ;	 function addcharacter
                           2743 ;	-----------------------------------------
   07B3                    2744 _addcharacter:
                           2745 ;	main.c:414: if(u<8)
                           2746 ;	genAssign
   07B3 90 00 0A           2747 	mov	dptr,#_u
   07B6 E0                 2748 	movx	a,@dptr
   07B7 FA                 2749 	mov	r2,a
                           2750 ;	genCmpLt
                           2751 ;	genCmp
   07B8 BA 08 00           2752 	cjne	r2,#0x08,00148$
   07BB                    2753 00148$:
                           2754 ;	genIfxJump
   07BB 40 03              2755 	jc	00149$
   07BD 02 09 1F           2756 	ljmp	00128$
   07C0                    2757 00149$:
                           2758 ;	main.c:416: putstr("\n\rEnter eight pixel values\n\r");
                           2759 ;	genCall
                           2760 ;	Peephole 182.a	used 16 bit load of DPTR
   07C0 90 26 50           2761 	mov	dptr,#__str_12
   07C3 75 F0 80           2762 	mov	b,#0x80
   07C6 12 00 AA           2763 	lcall	_putstr
                           2764 ;	main.c:417: putstr("Enter five binary values for every pixel\n\r");
                           2765 ;	genCall
                           2766 ;	Peephole 182.a	used 16 bit load of DPTR
   07C9 90 26 6D           2767 	mov	dptr,#__str_13
   07CC 75 F0 80           2768 	mov	b,#0x80
   07CF 12 00 AA           2769 	lcall	_putstr
                           2770 ;	main.c:420: for(temp=0; temp<=7; temp++)
                           2771 ;	genAssign
   07D2 90 00 2E           2772 	mov	dptr,#_addcharacter_temp_1_1
                           2773 ;	Peephole 181	changed mov to clr
   07D5 E4                 2774 	clr	a
   07D6 F0                 2775 	movx	@dptr,a
   07D7                    2776 00130$:
                           2777 ;	genAssign
   07D7 90 00 2E           2778 	mov	dptr,#_addcharacter_temp_1_1
   07DA E0                 2779 	movx	a,@dptr
                           2780 ;	genCmpGt
                           2781 ;	genCmp
                           2782 ;	genIfxJump
                           2783 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   07DB FA                 2784 	mov  r2,a
                           2785 ;	Peephole 177.a	removed redundant mov
   07DC 24 F8              2786 	add	a,#0xff - 0x07
                           2787 ;	Peephole 112.b	changed ljmp to sjmp
                           2788 ;	Peephole 160.a	removed sjmp by inverse jump logic
   07DE 40 46              2789 	jc	00133$
                           2790 ;	Peephole 300	removed redundant label 00150$
                           2791 ;	main.c:422: y=readinput();
                           2792 ;	genCall
   07E0 C0 02              2793 	push	ar2
   07E2 12 07 45           2794 	lcall	_readinput
   07E5 AB 82              2795 	mov	r3,dpl
   07E7 D0 02              2796 	pop	ar2
                           2797 ;	main.c:423: if(y>0x1F)
                           2798 ;	genCmpGt
                           2799 ;	genCmp
                           2800 ;	genIfxJump
                           2801 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           2802 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   07E9 EB                 2803 	mov	a,r3
   07EA 24 E0              2804 	add	a,#0xff - 0x1F
   07EC 50 15              2805 	jnc	00102$
                           2806 ;	Peephole 300	removed redundant label 00151$
                           2807 ;	main.c:425: putstr("Invalid Number. Enter Again\n\r");
                           2808 ;	genCall
                           2809 ;	Peephole 182.a	used 16 bit load of DPTR
   07EE 90 26 98           2810 	mov	dptr,#__str_14
   07F1 75 F0 80           2811 	mov	b,#0x80
   07F4 C0 02              2812 	push	ar2
   07F6 12 00 AA           2813 	lcall	_putstr
   07F9 D0 02              2814 	pop	ar2
                           2815 ;	main.c:426: temp--;
                           2816 ;	genMinus
                           2817 ;	genMinusDec
   07FB EA                 2818 	mov	a,r2
   07FC 14                 2819 	dec	a
                           2820 ;	genAssign
   07FD 90 00 2E           2821 	mov	dptr,#_addcharacter_temp_1_1
   0800 F0                 2822 	movx	@dptr,a
                           2823 ;	Peephole 112.b	changed ljmp to sjmp
   0801 80 15              2824 	sjmp	00132$
   0803                    2825 00102$:
                           2826 ;	main.c:430: array[temp]=y;
                           2827 ;	genPlus
                           2828 ;	Peephole 236.g	used r2 instead of ar2
   0803 EA                 2829 	mov	a,r2
   0804 24 02              2830 	add	a,#_array
   0806 F5 82              2831 	mov	dpl,a
                           2832 ;	Peephole 181	changed mov to clr
   0808 E4                 2833 	clr	a
   0809 34 00              2834 	addc	a,#(_array >> 8)
   080B F5 83              2835 	mov	dph,a
                           2836 ;	genPointerSet
                           2837 ;     genFarPointerSet
   080D EB                 2838 	mov	a,r3
   080E F0                 2839 	movx	@dptr,a
                           2840 ;	main.c:431: putstr("\n\r");
                           2841 ;	genCall
                           2842 ;	Peephole 182.a	used 16 bit load of DPTR
   080F 90 25 9C           2843 	mov	dptr,#__str_3
   0812 75 F0 80           2844 	mov	b,#0x80
   0815 12 00 AA           2845 	lcall	_putstr
   0818                    2846 00132$:
                           2847 ;	main.c:420: for(temp=0; temp<=7; temp++)
                           2848 ;	genAssign
   0818 90 00 2E           2849 	mov	dptr,#_addcharacter_temp_1_1
   081B E0                 2850 	movx	a,@dptr
   081C FA                 2851 	mov	r2,a
                           2852 ;	genPlus
   081D 90 00 2E           2853 	mov	dptr,#_addcharacter_temp_1_1
                           2854 ;     genPlusIncr
   0820 74 01              2855 	mov	a,#0x01
                           2856 ;	Peephole 236.a	used r2 instead of ar2
   0822 2A                 2857 	add	a,r2
   0823 F0                 2858 	movx	@dptr,a
                           2859 ;	Peephole 112.b	changed ljmp to sjmp
   0824 80 B1              2860 	sjmp	00130$
   0826                    2861 00133$:
                           2862 ;	main.c:434: if(u==0)
                           2863 ;	genAssign
   0826 90 00 0A           2864 	mov	dptr,#_u
   0829 E0                 2865 	movx	a,@dptr
                           2866 ;	genIfx
   082A FA                 2867 	mov	r2,a
                           2868 ;	Peephole 105	removed redundant mov
                           2869 ;	genIfxJump
                           2870 ;	Peephole 108.b	removed ljmp by inverse jump logic
   082B 70 17              2871 	jnz	00125$
                           2872 ;	Peephole 300	removed redundant label 00152$
                           2873 ;	main.c:435: lcdcreatechar(CGRAM_FIRSTCHAR_ADDR, array);
                           2874 ;	genCast
   082D 90 00 28           2875 	mov	dptr,#_lcdcreatechar_PARM_2
   0830 74 02              2876 	mov	a,#_array
   0832 F0                 2877 	movx	@dptr,a
   0833 A3                 2878 	inc	dptr
   0834 74 00              2879 	mov	a,#(_array >> 8)
   0836 F0                 2880 	movx	@dptr,a
   0837 A3                 2881 	inc	dptr
   0838 74 00              2882 	mov	a,#0x0
   083A F0                 2883 	movx	@dptr,a
                           2884 ;	genCall
   083B 75 82 00           2885 	mov	dpl,#0x00
   083E 12 05 16           2886 	lcall	_lcdcreatechar
   0841 02 09 17           2887 	ljmp	00126$
   0844                    2888 00125$:
                           2889 ;	main.c:436: else if(u==1)
                           2890 ;	genAssign
   0844 90 00 0A           2891 	mov	dptr,#_u
   0847 E0                 2892 	movx	a,@dptr
   0848 FA                 2893 	mov	r2,a
                           2894 ;	genCmpEq
                           2895 ;	gencjneshort
                           2896 ;	Peephole 112.b	changed ljmp to sjmp
                           2897 ;	Peephole 198.b	optimized misc jump sequence
   0849 BA 01 17           2898 	cjne	r2,#0x01,00122$
                           2899 ;	Peephole 200.b	removed redundant sjmp
                           2900 ;	Peephole 300	removed redundant label 00153$
                           2901 ;	Peephole 300	removed redundant label 00154$
                           2902 ;	main.c:437: lcdcreatechar(CGRAM_SECONDCHAR_ADDR, array);
                           2903 ;	genCast
   084C 90 00 28           2904 	mov	dptr,#_lcdcreatechar_PARM_2
   084F 74 02              2905 	mov	a,#_array
   0851 F0                 2906 	movx	@dptr,a
   0852 A3                 2907 	inc	dptr
   0853 74 00              2908 	mov	a,#(_array >> 8)
   0855 F0                 2909 	movx	@dptr,a
   0856 A3                 2910 	inc	dptr
   0857 74 00              2911 	mov	a,#0x0
   0859 F0                 2912 	movx	@dptr,a
                           2913 ;	genCall
   085A 75 82 08           2914 	mov	dpl,#0x08
   085D 12 05 16           2915 	lcall	_lcdcreatechar
   0860 02 09 17           2916 	ljmp	00126$
   0863                    2917 00122$:
                           2918 ;	main.c:438: else if(u==2)
                           2919 ;	genAssign
   0863 90 00 0A           2920 	mov	dptr,#_u
   0866 E0                 2921 	movx	a,@dptr
   0867 FA                 2922 	mov	r2,a
                           2923 ;	genCmpEq
                           2924 ;	gencjneshort
                           2925 ;	Peephole 112.b	changed ljmp to sjmp
                           2926 ;	Peephole 198.b	optimized misc jump sequence
   0868 BA 02 17           2927 	cjne	r2,#0x02,00119$
                           2928 ;	Peephole 200.b	removed redundant sjmp
                           2929 ;	Peephole 300	removed redundant label 00155$
                           2930 ;	Peephole 300	removed redundant label 00156$
                           2931 ;	main.c:439: lcdcreatechar(CGRAM_THIRDCHAR_ADDR, array);
                           2932 ;	genCast
   086B 90 00 28           2933 	mov	dptr,#_lcdcreatechar_PARM_2
   086E 74 02              2934 	mov	a,#_array
   0870 F0                 2935 	movx	@dptr,a
   0871 A3                 2936 	inc	dptr
   0872 74 00              2937 	mov	a,#(_array >> 8)
   0874 F0                 2938 	movx	@dptr,a
   0875 A3                 2939 	inc	dptr
   0876 74 00              2940 	mov	a,#0x0
   0878 F0                 2941 	movx	@dptr,a
                           2942 ;	genCall
   0879 75 82 10           2943 	mov	dpl,#0x10
   087C 12 05 16           2944 	lcall	_lcdcreatechar
   087F 02 09 17           2945 	ljmp	00126$
   0882                    2946 00119$:
                           2947 ;	main.c:440: else if(u==3)
                           2948 ;	genAssign
   0882 90 00 0A           2949 	mov	dptr,#_u
   0885 E0                 2950 	movx	a,@dptr
   0886 FA                 2951 	mov	r2,a
                           2952 ;	genCmpEq
                           2953 ;	gencjneshort
                           2954 ;	Peephole 112.b	changed ljmp to sjmp
                           2955 ;	Peephole 198.b	optimized misc jump sequence
   0887 BA 03 17           2956 	cjne	r2,#0x03,00116$
                           2957 ;	Peephole 200.b	removed redundant sjmp
                           2958 ;	Peephole 300	removed redundant label 00157$
                           2959 ;	Peephole 300	removed redundant label 00158$
                           2960 ;	main.c:441: lcdcreatechar(CGRAM_FOURTHCHAR_ADDR, array);
                           2961 ;	genCast
   088A 90 00 28           2962 	mov	dptr,#_lcdcreatechar_PARM_2
   088D 74 02              2963 	mov	a,#_array
   088F F0                 2964 	movx	@dptr,a
   0890 A3                 2965 	inc	dptr
   0891 74 00              2966 	mov	a,#(_array >> 8)
   0893 F0                 2967 	movx	@dptr,a
   0894 A3                 2968 	inc	dptr
   0895 74 00              2969 	mov	a,#0x0
   0897 F0                 2970 	movx	@dptr,a
                           2971 ;	genCall
   0898 75 82 18           2972 	mov	dpl,#0x18
   089B 12 05 16           2973 	lcall	_lcdcreatechar
   089E 02 09 17           2974 	ljmp	00126$
   08A1                    2975 00116$:
                           2976 ;	main.c:442: else if(u==4)
                           2977 ;	genAssign
   08A1 90 00 0A           2978 	mov	dptr,#_u
   08A4 E0                 2979 	movx	a,@dptr
   08A5 FA                 2980 	mov	r2,a
                           2981 ;	genCmpEq
                           2982 ;	gencjneshort
                           2983 ;	Peephole 112.b	changed ljmp to sjmp
                           2984 ;	Peephole 198.b	optimized misc jump sequence
   08A6 BA 04 16           2985 	cjne	r2,#0x04,00113$
                           2986 ;	Peephole 200.b	removed redundant sjmp
                           2987 ;	Peephole 300	removed redundant label 00159$
                           2988 ;	Peephole 300	removed redundant label 00160$
                           2989 ;	main.c:443: lcdcreatechar(CGRAM_FIFTHCHAR_ADDR, array);
                           2990 ;	genCast
   08A9 90 00 28           2991 	mov	dptr,#_lcdcreatechar_PARM_2
   08AC 74 02              2992 	mov	a,#_array
   08AE F0                 2993 	movx	@dptr,a
   08AF A3                 2994 	inc	dptr
   08B0 74 00              2995 	mov	a,#(_array >> 8)
   08B2 F0                 2996 	movx	@dptr,a
   08B3 A3                 2997 	inc	dptr
   08B4 74 00              2998 	mov	a,#0x0
   08B6 F0                 2999 	movx	@dptr,a
                           3000 ;	genCall
   08B7 75 82 20           3001 	mov	dpl,#0x20
   08BA 12 05 16           3002 	lcall	_lcdcreatechar
                           3003 ;	Peephole 112.b	changed ljmp to sjmp
   08BD 80 58              3004 	sjmp	00126$
   08BF                    3005 00113$:
                           3006 ;	main.c:444: else if(u==5)
                           3007 ;	genAssign
   08BF 90 00 0A           3008 	mov	dptr,#_u
   08C2 E0                 3009 	movx	a,@dptr
   08C3 FA                 3010 	mov	r2,a
                           3011 ;	genCmpEq
                           3012 ;	gencjneshort
                           3013 ;	Peephole 112.b	changed ljmp to sjmp
                           3014 ;	Peephole 198.b	optimized misc jump sequence
   08C4 BA 05 16           3015 	cjne	r2,#0x05,00110$
                           3016 ;	Peephole 200.b	removed redundant sjmp
                           3017 ;	Peephole 300	removed redundant label 00161$
                           3018 ;	Peephole 300	removed redundant label 00162$
                           3019 ;	main.c:445: lcdcreatechar(CGRAM_SIXTHCHAR_ADDR, array);
                           3020 ;	genCast
   08C7 90 00 28           3021 	mov	dptr,#_lcdcreatechar_PARM_2
   08CA 74 02              3022 	mov	a,#_array
   08CC F0                 3023 	movx	@dptr,a
   08CD A3                 3024 	inc	dptr
   08CE 74 00              3025 	mov	a,#(_array >> 8)
   08D0 F0                 3026 	movx	@dptr,a
   08D1 A3                 3027 	inc	dptr
   08D2 74 00              3028 	mov	a,#0x0
   08D4 F0                 3029 	movx	@dptr,a
                           3030 ;	genCall
   08D5 75 82 28           3031 	mov	dpl,#0x28
   08D8 12 05 16           3032 	lcall	_lcdcreatechar
                           3033 ;	Peephole 112.b	changed ljmp to sjmp
   08DB 80 3A              3034 	sjmp	00126$
   08DD                    3035 00110$:
                           3036 ;	main.c:446: else if(u==6)
                           3037 ;	genAssign
   08DD 90 00 0A           3038 	mov	dptr,#_u
   08E0 E0                 3039 	movx	a,@dptr
   08E1 FA                 3040 	mov	r2,a
                           3041 ;	genCmpEq
                           3042 ;	gencjneshort
                           3043 ;	Peephole 112.b	changed ljmp to sjmp
                           3044 ;	Peephole 198.b	optimized misc jump sequence
   08E2 BA 06 16           3045 	cjne	r2,#0x06,00107$
                           3046 ;	Peephole 200.b	removed redundant sjmp
                           3047 ;	Peephole 300	removed redundant label 00163$
                           3048 ;	Peephole 300	removed redundant label 00164$
                           3049 ;	main.c:447: lcdcreatechar(CGRAM_SEVENTHCHAR_ADDR, array);
                           3050 ;	genCast
   08E5 90 00 28           3051 	mov	dptr,#_lcdcreatechar_PARM_2
   08E8 74 02              3052 	mov	a,#_array
   08EA F0                 3053 	movx	@dptr,a
   08EB A3                 3054 	inc	dptr
   08EC 74 00              3055 	mov	a,#(_array >> 8)
   08EE F0                 3056 	movx	@dptr,a
   08EF A3                 3057 	inc	dptr
   08F0 74 00              3058 	mov	a,#0x0
   08F2 F0                 3059 	movx	@dptr,a
                           3060 ;	genCall
   08F3 75 82 30           3061 	mov	dpl,#0x30
   08F6 12 05 16           3062 	lcall	_lcdcreatechar
                           3063 ;	Peephole 112.b	changed ljmp to sjmp
   08F9 80 1C              3064 	sjmp	00126$
   08FB                    3065 00107$:
                           3066 ;	main.c:448: else if(u==7)
                           3067 ;	genAssign
   08FB 90 00 0A           3068 	mov	dptr,#_u
   08FE E0                 3069 	movx	a,@dptr
   08FF FA                 3070 	mov	r2,a
                           3071 ;	genCmpEq
                           3072 ;	gencjneshort
                           3073 ;	Peephole 112.b	changed ljmp to sjmp
                           3074 ;	Peephole 198.b	optimized misc jump sequence
   0900 BA 07 14           3075 	cjne	r2,#0x07,00126$
                           3076 ;	Peephole 200.b	removed redundant sjmp
                           3077 ;	Peephole 300	removed redundant label 00165$
                           3078 ;	Peephole 300	removed redundant label 00166$
                           3079 ;	main.c:449: lcdcreatechar(CGRAM_EIGHTHCHAR_ADDR, array);
                           3080 ;	genCast
   0903 90 00 28           3081 	mov	dptr,#_lcdcreatechar_PARM_2
   0906 74 02              3082 	mov	a,#_array
   0908 F0                 3083 	movx	@dptr,a
   0909 A3                 3084 	inc	dptr
   090A 74 00              3085 	mov	a,#(_array >> 8)
   090C F0                 3086 	movx	@dptr,a
   090D A3                 3087 	inc	dptr
   090E 74 00              3088 	mov	a,#0x0
   0910 F0                 3089 	movx	@dptr,a
                           3090 ;	genCall
   0911 75 82 38           3091 	mov	dpl,#0x38
   0914 12 05 16           3092 	lcall	_lcdcreatechar
   0917                    3093 00126$:
                           3094 ;	main.c:450: u++;
                           3095 ;	genPlus
   0917 90 00 0A           3096 	mov	dptr,#_u
   091A E0                 3097 	movx	a,@dptr
   091B 24 01              3098 	add	a,#0x01
   091D F0                 3099 	movx	@dptr,a
                           3100 ;	Peephole 112.b	changed ljmp to sjmp
                           3101 ;	Peephole 251.b	replaced sjmp to ret with ret
   091E 22                 3102 	ret
   091F                    3103 00128$:
                           3104 ;	main.c:454: putstr("Eight custom character created. No more custom character please\n\r");
                           3105 ;	genCall
                           3106 ;	Peephole 182.a	used 16 bit load of DPTR
   091F 90 26 B6           3107 	mov	dptr,#__str_15
   0922 75 F0 80           3108 	mov	b,#0x80
                           3109 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0925 02 00 AA           3110 	ljmp	_putstr
                           3111 ;
                           3112 ;------------------------------------------------------------
                           3113 ;Allocation info for local variables in function 'display'
                           3114 ;------------------------------------------------------------
                           3115 ;hey                       Allocated with name '_display_hey_1_1'
                           3116 ;------------------------------------------------------------
                           3117 ;	main.c:459: void display()
                           3118 ;	-----------------------------------------
                           3119 ;	 function display
                           3120 ;	-----------------------------------------
   0928                    3121 _display:
                           3122 ;	main.c:462: putstr("Look at the LCD\n\r");
                           3123 ;	genCall
                           3124 ;	Peephole 182.a	used 16 bit load of DPTR
   0928 90 26 F8           3125 	mov	dptr,#__str_16
   092B 75 F0 80           3126 	mov	b,#0x80
   092E 12 00 AA           3127 	lcall	_putstr
                           3128 ;	main.c:463: while(hey<0x8)
                           3129 ;	genAssign
   0931 7A 00              3130 	mov	r2,#0x00
   0933                    3131 00101$:
                           3132 ;	genCmpLt
                           3133 ;	genCmp
   0933 BA 08 00           3134 	cjne	r2,#0x08,00109$
   0936                    3135 00109$:
                           3136 ;	genIfxJump
                           3137 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0936 50 0C              3138 	jnc	00104$
                           3139 ;	Peephole 300	removed redundant label 00110$
                           3140 ;	main.c:465: lcdputch(hey);
                           3141 ;	genCall
   0938 8A 82              3142 	mov	dpl,r2
   093A C0 02              3143 	push	ar2
   093C 12 02 7D           3144 	lcall	_lcdputch
   093F D0 02              3145 	pop	ar2
                           3146 ;	main.c:466: hey++;
                           3147 ;	genPlus
                           3148 ;     genPlusIncr
   0941 0A                 3149 	inc	r2
                           3150 ;	Peephole 112.b	changed ljmp to sjmp
   0942 80 EF              3151 	sjmp	00101$
   0944                    3152 00104$:
   0944 22                 3153 	ret
                           3154 ;------------------------------------------------------------
                           3155 ;Allocation info for local variables in function 'hex2int'
                           3156 ;------------------------------------------------------------
                           3157 ;sloc0                     Allocated with name '_hex2int_sloc0_1_0'
                           3158 ;sloc1                     Allocated with name '_hex2int_sloc1_1_0'
                           3159 ;sloc2                     Allocated with name '_hex2int_sloc2_1_0'
                           3160 ;sloc3                     Allocated with name '_hex2int_sloc3_1_0'
                           3161 ;len                       Allocated with name '_hex2int_PARM_2'
                           3162 ;a                         Allocated with name '_hex2int_a_1_1'
                           3163 ;i                         Allocated with name '_hex2int_i_1_1'
                           3164 ;val                       Allocated with name '_hex2int_val_1_1'
                           3165 ;------------------------------------------------------------
                           3166 ;	main.c:474: unsigned long hex2int(char *a, unsigned int len)
                           3167 ;	-----------------------------------------
                           3168 ;	 function hex2int
                           3169 ;	-----------------------------------------
   0945                    3170 _hex2int:
                           3171 ;	genReceive
   0945 AA F0              3172 	mov	r2,b
   0947 AB 83              3173 	mov	r3,dph
   0949 E5 82              3174 	mov	a,dpl
   094B 90 00 31           3175 	mov	dptr,#_hex2int_a_1_1
   094E F0                 3176 	movx	@dptr,a
   094F A3                 3177 	inc	dptr
   0950 EB                 3178 	mov	a,r3
   0951 F0                 3179 	movx	@dptr,a
   0952 A3                 3180 	inc	dptr
   0953 EA                 3181 	mov	a,r2
   0954 F0                 3182 	movx	@dptr,a
                           3183 ;	main.c:477: unsigned long val = 0;
                           3184 ;	genAssign
   0955 90 00 34           3185 	mov	dptr,#_hex2int_val_1_1
   0958 E4                 3186 	clr	a
   0959 F0                 3187 	movx	@dptr,a
   095A A3                 3188 	inc	dptr
   095B F0                 3189 	movx	@dptr,a
   095C A3                 3190 	inc	dptr
   095D F0                 3191 	movx	@dptr,a
   095E A3                 3192 	inc	dptr
   095F F0                 3193 	movx	@dptr,a
                           3194 ;	main.c:479: for(i=0;i<len;i++)
                           3195 ;	genAssign
   0960 90 00 31           3196 	mov	dptr,#_hex2int_a_1_1
   0963 E0                 3197 	movx	a,@dptr
   0964 FA                 3198 	mov	r2,a
   0965 A3                 3199 	inc	dptr
   0966 E0                 3200 	movx	a,@dptr
   0967 FB                 3201 	mov	r3,a
   0968 A3                 3202 	inc	dptr
   0969 E0                 3203 	movx	a,@dptr
   096A FC                 3204 	mov	r4,a
                           3205 ;	genAssign
   096B 90 00 2F           3206 	mov	dptr,#_hex2int_PARM_2
   096E E0                 3207 	movx	a,@dptr
   096F FD                 3208 	mov	r5,a
   0970 A3                 3209 	inc	dptr
   0971 E0                 3210 	movx	a,@dptr
   0972 FE                 3211 	mov	r6,a
                           3212 ;	genAssign
   0973 7F 00              3213 	mov	r7,#0x00
   0975 78 00              3214 	mov	r0,#0x00
   0977                    3215 00104$:
                           3216 ;	genIpush
   0977 C0 02              3217 	push	ar2
   0979 C0 03              3218 	push	ar3
   097B C0 04              3219 	push	ar4
                           3220 ;	genAssign
   097D 8F 01              3221 	mov	ar1,r7
   097F 88 02              3222 	mov	ar2,r0
                           3223 ;	genCmpLt
                           3224 ;	genCmp
   0981 C3                 3225 	clr	c
   0982 E9                 3226 	mov	a,r1
   0983 9D                 3227 	subb	a,r5
   0984 EA                 3228 	mov	a,r2
   0985 9E                 3229 	subb	a,r6
                           3230 ;	genIpop
                           3231 ;	genIfx
                           3232 ;	genIfxJump
                           3233 ;	Peephole 129.d	optimized condition
   0986 D0 04              3234 	pop	ar4
   0988 D0 03              3235 	pop	ar3
   098A D0 02              3236 	pop	ar2
   098C 40 03              3237 	jc	00114$
   098E 02 0B 1D           3238 	ljmp	00107$
   0991                    3239 00114$:
                           3240 ;	main.c:480: if(a[i] <= 57)
                           3241 ;	genIpush
   0991 C0 05              3242 	push	ar5
   0993 C0 06              3243 	push	ar6
                           3244 ;	genPlus
                           3245 ;	Peephole 236.g	used r7 instead of ar7
   0995 EF                 3246 	mov	a,r7
                           3247 ;	Peephole 236.a	used r2 instead of ar2
   0996 2A                 3248 	add	a,r2
   0997 F9                 3249 	mov	r1,a
                           3250 ;	Peephole 236.g	used r0 instead of ar0
   0998 E8                 3251 	mov	a,r0
                           3252 ;	Peephole 236.b	used r3 instead of ar3
   0999 3B                 3253 	addc	a,r3
   099A FD                 3254 	mov	r5,a
   099B 8C 06              3255 	mov	ar6,r4
                           3256 ;	genPointerGet
                           3257 ;	genGenPointerGet
   099D 89 82              3258 	mov	dpl,r1
   099F 8D 83              3259 	mov	dph,r5
   09A1 8E F0              3260 	mov	b,r6
   09A3 12 25 13           3261 	lcall	__gptrget
   09A6 F5 24              3262 	mov	_hex2int_sloc0_1_0,a
                           3263 ;	genCmpGt
                           3264 ;	genCmp
   09A8 C3                 3265 	clr	c
                           3266 ;	Peephole 159	avoided xrl during execution
   09A9 74 B9              3267 	mov	a,#(0x39 ^ 0x80)
   09AB 85 24 F0           3268 	mov	b,_hex2int_sloc0_1_0
   09AE 63 F0 80           3269 	xrl	b,#0x80
   09B1 95 F0              3270 	subb	a,b
   09B3 E4                 3271 	clr	a
   09B4 33                 3272 	rlc	a
                           3273 ;	genIpop
   09B5 D0 06              3274 	pop	ar6
   09B7 D0 05              3275 	pop	ar5
                           3276 ;	genIfx
                           3277 ;	genIfxJump
   09B9 60 03              3278 	jz	00115$
   09BB 02 0A 6B           3279 	ljmp	00102$
   09BE                    3280 00115$:
                           3281 ;	main.c:481: val += (a[i]-48)*(1<<(4*(len-1-i)));
                           3282 ;	genIpush
   09BE C0 02              3283 	push	ar2
   09C0 C0 03              3284 	push	ar3
   09C2 C0 04              3285 	push	ar4
                           3286 ;	genCast
   09C4 A9 24              3287 	mov	r1,_hex2int_sloc0_1_0
   09C6 E5 24              3288 	mov	a,_hex2int_sloc0_1_0
   09C8 33                 3289 	rlc	a
   09C9 95 E0              3290 	subb	a,acc
   09CB FA                 3291 	mov	r2,a
                           3292 ;	genMinus
   09CC E9                 3293 	mov	a,r1
   09CD 24 D0              3294 	add	a,#0xd0
   09CF F9                 3295 	mov	r1,a
   09D0 EA                 3296 	mov	a,r2
   09D1 34 FF              3297 	addc	a,#0xff
   09D3 FA                 3298 	mov	r2,a
                           3299 ;	genMinus
                           3300 ;	genMinusDec
   09D4 ED                 3301 	mov	a,r5
   09D5 24 FF              3302 	add	a,#0xff
   09D7 FB                 3303 	mov	r3,a
   09D8 EE                 3304 	mov	a,r6
   09D9 34 FF              3305 	addc	a,#0xff
   09DB FC                 3306 	mov	r4,a
                           3307 ;	genMinus
   09DC EB                 3308 	mov	a,r3
   09DD C3                 3309 	clr	c
                           3310 ;	Peephole 236.l	used r7 instead of ar7
   09DE 9F                 3311 	subb	a,r7
   09DF FB                 3312 	mov	r3,a
   09E0 EC                 3313 	mov	a,r4
                           3314 ;	Peephole 236.l	used r0 instead of ar0
   09E1 98                 3315 	subb	a,r0
                           3316 ;	genLeftShift
                           3317 ;	genLeftShiftLiteral
                           3318 ;	genlshTwo
   09E2 FC                 3319 	mov	r4,a
                           3320 ;	Peephole 105	removed redundant mov
   09E3 CB                 3321 	xch	a,r3
   09E4 25 E0              3322 	add	a,acc
   09E6 CB                 3323 	xch	a,r3
   09E7 33                 3324 	rlc	a
   09E8 CB                 3325 	xch	a,r3
   09E9 25 E0              3326 	add	a,acc
   09EB CB                 3327 	xch	a,r3
   09EC 33                 3328 	rlc	a
   09ED FC                 3329 	mov	r4,a
                           3330 ;	genLeftShift
   09EE 8B F0              3331 	mov	b,r3
   09F0 05 F0              3332 	inc	b
   09F2 7B 01              3333 	mov	r3,#0x01
   09F4 7C 00              3334 	mov	r4,#0x00
   09F6 80 06              3335 	sjmp	00117$
   09F8                    3336 00116$:
   09F8 EB                 3337 	mov	a,r3
                           3338 ;	Peephole 254	optimized left shift
   09F9 2B                 3339 	add	a,r3
   09FA FB                 3340 	mov	r3,a
   09FB EC                 3341 	mov	a,r4
   09FC 33                 3342 	rlc	a
   09FD FC                 3343 	mov	r4,a
   09FE                    3344 00117$:
   09FE D5 F0 F7           3345 	djnz	b,00116$
                           3346 ;	genAssign
   0A01 90 00 8C           3347 	mov	dptr,#__mulint_PARM_2
   0A04 EB                 3348 	mov	a,r3
   0A05 F0                 3349 	movx	@dptr,a
   0A06 A3                 3350 	inc	dptr
   0A07 EC                 3351 	mov	a,r4
   0A08 F0                 3352 	movx	@dptr,a
                           3353 ;	genCall
   0A09 89 82              3354 	mov	dpl,r1
   0A0B 8A 83              3355 	mov	dph,r2
   0A0D C0 02              3356 	push	ar2
   0A0F C0 03              3357 	push	ar3
   0A11 C0 04              3358 	push	ar4
   0A13 C0 05              3359 	push	ar5
   0A15 C0 06              3360 	push	ar6
   0A17 C0 07              3361 	push	ar7
   0A19 C0 00              3362 	push	ar0
   0A1B 12 1C 08           3363 	lcall	__mulint
   0A1E 85 82 25           3364 	mov	_hex2int_sloc1_1_0,dpl
   0A21 85 83 26           3365 	mov	(_hex2int_sloc1_1_0 + 1),dph
   0A24 D0 00              3366 	pop	ar0
   0A26 D0 07              3367 	pop	ar7
   0A28 D0 06              3368 	pop	ar6
   0A2A D0 05              3369 	pop	ar5
   0A2C D0 04              3370 	pop	ar4
   0A2E D0 03              3371 	pop	ar3
   0A30 D0 02              3372 	pop	ar2
                           3373 ;	genAssign
   0A32 90 00 34           3374 	mov	dptr,#_hex2int_val_1_1
   0A35 E0                 3375 	movx	a,@dptr
   0A36 F5 27              3376 	mov	_hex2int_sloc2_1_0,a
   0A38 A3                 3377 	inc	dptr
   0A39 E0                 3378 	movx	a,@dptr
   0A3A F5 28              3379 	mov	(_hex2int_sloc2_1_0 + 1),a
   0A3C A3                 3380 	inc	dptr
   0A3D E0                 3381 	movx	a,@dptr
   0A3E F5 29              3382 	mov	(_hex2int_sloc2_1_0 + 2),a
   0A40 A3                 3383 	inc	dptr
   0A41 E0                 3384 	movx	a,@dptr
   0A42 F5 2A              3385 	mov	(_hex2int_sloc2_1_0 + 3),a
                           3386 ;	genCast
   0A44 AA 25              3387 	mov	r2,_hex2int_sloc1_1_0
   0A46 AB 26              3388 	mov	r3,(_hex2int_sloc1_1_0 + 1)
   0A48 7C 00              3389 	mov	r4,#0x00
   0A4A 79 00              3390 	mov	r1,#0x00
                           3391 ;	genPlus
   0A4C 90 00 34           3392 	mov	dptr,#_hex2int_val_1_1
                           3393 ;	Peephole 236.g	used r2 instead of ar2
   0A4F EA                 3394 	mov	a,r2
   0A50 25 27              3395 	add	a,_hex2int_sloc2_1_0
   0A52 F0                 3396 	movx	@dptr,a
                           3397 ;	Peephole 236.g	used r3 instead of ar3
   0A53 EB                 3398 	mov	a,r3
   0A54 35 28              3399 	addc	a,(_hex2int_sloc2_1_0 + 1)
   0A56 A3                 3400 	inc	dptr
   0A57 F0                 3401 	movx	@dptr,a
                           3402 ;	Peephole 236.g	used r4 instead of ar4
   0A58 EC                 3403 	mov	a,r4
   0A59 35 29              3404 	addc	a,(_hex2int_sloc2_1_0 + 2)
   0A5B A3                 3405 	inc	dptr
   0A5C F0                 3406 	movx	@dptr,a
                           3407 ;	Peephole 236.g	used r1 instead of ar1
   0A5D E9                 3408 	mov	a,r1
   0A5E 35 2A              3409 	addc	a,(_hex2int_sloc2_1_0 + 3)
   0A60 A3                 3410 	inc	dptr
   0A61 F0                 3411 	movx	@dptr,a
                           3412 ;	genIpop
   0A62 D0 04              3413 	pop	ar4
   0A64 D0 03              3414 	pop	ar3
   0A66 D0 02              3415 	pop	ar2
   0A68 02 0B 15           3416 	ljmp	00106$
   0A6B                    3417 00102$:
                           3418 ;	main.c:483: val += (a[i]-55)*(1<<(4*(len-1-i)));
                           3419 ;	genIpush
   0A6B C0 02              3420 	push	ar2
   0A6D C0 03              3421 	push	ar3
   0A6F C0 04              3422 	push	ar4
                           3423 ;	genCast
   0A71 A9 24              3424 	mov	r1,_hex2int_sloc0_1_0
   0A73 E5 24              3425 	mov	a,_hex2int_sloc0_1_0
   0A75 33                 3426 	rlc	a
   0A76 95 E0              3427 	subb	a,acc
   0A78 FA                 3428 	mov	r2,a
                           3429 ;	genMinus
   0A79 E9                 3430 	mov	a,r1
   0A7A 24 C9              3431 	add	a,#0xc9
   0A7C F9                 3432 	mov	r1,a
   0A7D EA                 3433 	mov	a,r2
   0A7E 34 FF              3434 	addc	a,#0xff
   0A80 FA                 3435 	mov	r2,a
                           3436 ;	genMinus
                           3437 ;	genMinusDec
   0A81 ED                 3438 	mov	a,r5
   0A82 24 FF              3439 	add	a,#0xff
   0A84 FB                 3440 	mov	r3,a
   0A85 EE                 3441 	mov	a,r6
   0A86 34 FF              3442 	addc	a,#0xff
   0A88 FC                 3443 	mov	r4,a
                           3444 ;	genMinus
   0A89 EB                 3445 	mov	a,r3
   0A8A C3                 3446 	clr	c
                           3447 ;	Peephole 236.l	used r7 instead of ar7
   0A8B 9F                 3448 	subb	a,r7
   0A8C FB                 3449 	mov	r3,a
   0A8D EC                 3450 	mov	a,r4
                           3451 ;	Peephole 236.l	used r0 instead of ar0
   0A8E 98                 3452 	subb	a,r0
                           3453 ;	genLeftShift
                           3454 ;	genLeftShiftLiteral
                           3455 ;	genlshTwo
   0A8F FC                 3456 	mov	r4,a
                           3457 ;	Peephole 105	removed redundant mov
   0A90 CB                 3458 	xch	a,r3
   0A91 25 E0              3459 	add	a,acc
   0A93 CB                 3460 	xch	a,r3
   0A94 33                 3461 	rlc	a
   0A95 CB                 3462 	xch	a,r3
   0A96 25 E0              3463 	add	a,acc
   0A98 CB                 3464 	xch	a,r3
   0A99 33                 3465 	rlc	a
   0A9A FC                 3466 	mov	r4,a
                           3467 ;	genLeftShift
   0A9B 8B F0              3468 	mov	b,r3
   0A9D 05 F0              3469 	inc	b
   0A9F 7B 01              3470 	mov	r3,#0x01
   0AA1 7C 00              3471 	mov	r4,#0x00
   0AA3 80 06              3472 	sjmp	00119$
   0AA5                    3473 00118$:
   0AA5 EB                 3474 	mov	a,r3
                           3475 ;	Peephole 254	optimized left shift
   0AA6 2B                 3476 	add	a,r3
   0AA7 FB                 3477 	mov	r3,a
   0AA8 EC                 3478 	mov	a,r4
   0AA9 33                 3479 	rlc	a
   0AAA FC                 3480 	mov	r4,a
   0AAB                    3481 00119$:
   0AAB D5 F0 F7           3482 	djnz	b,00118$
                           3483 ;	genAssign
   0AAE 90 00 8C           3484 	mov	dptr,#__mulint_PARM_2
   0AB1 EB                 3485 	mov	a,r3
   0AB2 F0                 3486 	movx	@dptr,a
   0AB3 A3                 3487 	inc	dptr
   0AB4 EC                 3488 	mov	a,r4
   0AB5 F0                 3489 	movx	@dptr,a
                           3490 ;	genCall
   0AB6 89 82              3491 	mov	dpl,r1
   0AB8 8A 83              3492 	mov	dph,r2
   0ABA C0 02              3493 	push	ar2
   0ABC C0 03              3494 	push	ar3
   0ABE C0 04              3495 	push	ar4
   0AC0 C0 05              3496 	push	ar5
   0AC2 C0 06              3497 	push	ar6
   0AC4 C0 07              3498 	push	ar7
   0AC6 C0 00              3499 	push	ar0
   0AC8 12 1C 08           3500 	lcall	__mulint
   0ACB 85 82 27           3501 	mov	_hex2int_sloc2_1_0,dpl
   0ACE 85 83 28           3502 	mov	(_hex2int_sloc2_1_0 + 1),dph
   0AD1 D0 00              3503 	pop	ar0
   0AD3 D0 07              3504 	pop	ar7
   0AD5 D0 06              3505 	pop	ar6
   0AD7 D0 05              3506 	pop	ar5
   0AD9 D0 04              3507 	pop	ar4
   0ADB D0 03              3508 	pop	ar3
   0ADD D0 02              3509 	pop	ar2
                           3510 ;	genAssign
   0ADF 90 00 34           3511 	mov	dptr,#_hex2int_val_1_1
   0AE2 E0                 3512 	movx	a,@dptr
   0AE3 F5 2B              3513 	mov	_hex2int_sloc3_1_0,a
   0AE5 A3                 3514 	inc	dptr
   0AE6 E0                 3515 	movx	a,@dptr
   0AE7 F5 2C              3516 	mov	(_hex2int_sloc3_1_0 + 1),a
   0AE9 A3                 3517 	inc	dptr
   0AEA E0                 3518 	movx	a,@dptr
   0AEB F5 2D              3519 	mov	(_hex2int_sloc3_1_0 + 2),a
   0AED A3                 3520 	inc	dptr
   0AEE E0                 3521 	movx	a,@dptr
   0AEF F5 2E              3522 	mov	(_hex2int_sloc3_1_0 + 3),a
                           3523 ;	genCast
   0AF1 AA 27              3524 	mov	r2,_hex2int_sloc2_1_0
   0AF3 AB 28              3525 	mov	r3,(_hex2int_sloc2_1_0 + 1)
   0AF5 7C 00              3526 	mov	r4,#0x00
   0AF7 79 00              3527 	mov	r1,#0x00
                           3528 ;	genPlus
   0AF9 90 00 34           3529 	mov	dptr,#_hex2int_val_1_1
                           3530 ;	Peephole 236.g	used r2 instead of ar2
   0AFC EA                 3531 	mov	a,r2
   0AFD 25 2B              3532 	add	a,_hex2int_sloc3_1_0
   0AFF F0                 3533 	movx	@dptr,a
                           3534 ;	Peephole 236.g	used r3 instead of ar3
   0B00 EB                 3535 	mov	a,r3
   0B01 35 2C              3536 	addc	a,(_hex2int_sloc3_1_0 + 1)
   0B03 A3                 3537 	inc	dptr
   0B04 F0                 3538 	movx	@dptr,a
                           3539 ;	Peephole 236.g	used r4 instead of ar4
   0B05 EC                 3540 	mov	a,r4
   0B06 35 2D              3541 	addc	a,(_hex2int_sloc3_1_0 + 2)
   0B08 A3                 3542 	inc	dptr
   0B09 F0                 3543 	movx	@dptr,a
                           3544 ;	Peephole 236.g	used r1 instead of ar1
   0B0A E9                 3545 	mov	a,r1
   0B0B 35 2E              3546 	addc	a,(_hex2int_sloc3_1_0 + 3)
   0B0D A3                 3547 	inc	dptr
   0B0E F0                 3548 	movx	@dptr,a
                           3549 ;	main.c:485: return val;
                           3550 ;	genIpop
   0B0F D0 04              3551 	pop	ar4
   0B11 D0 03              3552 	pop	ar3
   0B13 D0 02              3553 	pop	ar2
                           3554 ;	main.c:483: val += (a[i]-55)*(1<<(4*(len-1-i)));
   0B15                    3555 00106$:
                           3556 ;	main.c:479: for(i=0;i<len;i++)
                           3557 ;	genPlus
                           3558 ;     genPlusIncr
   0B15 0F                 3559 	inc	r7
   0B16 BF 00 01           3560 	cjne	r7,#0x00,00120$
   0B19 08                 3561 	inc	r0
   0B1A                    3562 00120$:
   0B1A 02 09 77           3563 	ljmp	00104$
   0B1D                    3564 00107$:
                           3565 ;	main.c:485: return val;
                           3566 ;	genAssign
   0B1D 90 00 34           3567 	mov	dptr,#_hex2int_val_1_1
   0B20 E0                 3568 	movx	a,@dptr
   0B21 FA                 3569 	mov	r2,a
   0B22 A3                 3570 	inc	dptr
   0B23 E0                 3571 	movx	a,@dptr
   0B24 FB                 3572 	mov	r3,a
   0B25 A3                 3573 	inc	dptr
   0B26 E0                 3574 	movx	a,@dptr
   0B27 FC                 3575 	mov	r4,a
   0B28 A3                 3576 	inc	dptr
   0B29 E0                 3577 	movx	a,@dptr
                           3578 ;	genRet
   0B2A FD                 3579 	mov	r5,a
   0B2B 8A 82              3580 	mov	dpl,r2
   0B2D 8B 83              3581 	mov	dph,r3
   0B2F 8C F0              3582 	mov	b,r4
                           3583 ;	Peephole 191	removed redundant mov
                           3584 ;	Peephole 300	removed redundant label 00108$
   0B31 22                 3585 	ret
                           3586 ;------------------------------------------------------------
                           3587 ;Allocation info for local variables in function 'ReadVariable'
                           3588 ;------------------------------------------------------------
                           3589 ;l                         Allocated with name '_ReadVariable_l_1_1'
                           3590 ;m                         Allocated with name '_ReadVariable_m_1_1'
                           3591 ;n                         Allocated with name '_ReadVariable_n_1_1'
                           3592 ;k                         Allocated with name '_ReadVariable_k_1_1'
                           3593 ;ptr                       Allocated with name '_ReadVariable_ptr_1_1'
                           3594 ;------------------------------------------------------------
                           3595 ;	main.c:488: uint16_t ReadVariable()
                           3596 ;	-----------------------------------------
                           3597 ;	 function ReadVariable
                           3598 ;	-----------------------------------------
   0B32                    3599 _ReadVariable:
                           3600 ;	main.c:490: uint16_t l=0, m=0, n, k=0;
                           3601 ;	genAssign
   0B32 90 00 38           3602 	mov	dptr,#_ReadVariable_k_1_1
   0B35 E4                 3603 	clr	a
   0B36 F0                 3604 	movx	@dptr,a
   0B37 A3                 3605 	inc	dptr
   0B38 F0                 3606 	movx	@dptr,a
                           3607 ;	main.c:491: unsigned char ptr[]="";
                           3608 ;	genPointerSet
                           3609 ;     genFarPointerSet
   0B39 90 00 3A           3610 	mov	dptr,#_ReadVariable_ptr_1_1
                           3611 ;	Peephole 181	changed mov to clr
   0B3C E4                 3612 	clr	a
   0B3D F0                 3613 	movx	@dptr,a
                           3614 ;	main.c:492: do
   0B3E                    3615 00106$:
                           3616 ;	main.c:494: l=getchar();
                           3617 ;	genCall
   0B3E 12 0C F1           3618 	lcall	_getchar
                           3619 ;	genCast
                           3620 ;	peephole 177.g	optimized mov sequence
   0B41 E5 82              3621 	mov	a,dpl
   0B43 FA                 3622 	mov	r2,a
   0B44 33                 3623 	rlc	a
   0B45 95 E0              3624 	subb	a,acc
   0B47 FB                 3625 	mov	r3,a
                           3626 ;	main.c:495: if(l!=READ_ENTER)
                           3627 ;	genCmpEq
                           3628 ;	gencjne
                           3629 ;	gencjneshort
                           3630 ;	Peephole 241.c	optimized compare
   0B48 E4                 3631 	clr	a
   0B49 BA 0D 04           3632 	cjne	r2,#0x0D,00115$
   0B4C BB 00 01           3633 	cjne	r3,#0x00,00115$
   0B4F 04                 3634 	inc	a
   0B50                    3635 00115$:
                           3636 ;	Peephole 300	removed redundant label 00116$
                           3637 ;	genIfx
   0B50 FC                 3638 	mov	r4,a
                           3639 ;	Peephole 105	removed redundant mov
                           3640 ;	genIfxJump
                           3641 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0B51 70 49              3642 	jnz	00107$
                           3643 ;	Peephole 300	removed redundant label 00117$
                           3644 ;	main.c:497: if(l!=READ_BACKSPACE)
                           3645 ;	genCmpEq
                           3646 ;	gencjneshort
   0B53 BA 7F 05           3647 	cjne	r2,#0x7F,00118$
   0B56 BB 00 02           3648 	cjne	r3,#0x00,00118$
                           3649 ;	Peephole 112.b	changed ljmp to sjmp
   0B59 80 2C              3650 	sjmp	00102$
   0B5B                    3651 00118$:
                           3652 ;	main.c:499: ptr[k]=l;
                           3653 ;	genAssign
   0B5B 90 00 38           3654 	mov	dptr,#_ReadVariable_k_1_1
   0B5E E0                 3655 	movx	a,@dptr
   0B5F FD                 3656 	mov	r5,a
   0B60 A3                 3657 	inc	dptr
   0B61 E0                 3658 	movx	a,@dptr
   0B62 FE                 3659 	mov	r6,a
                           3660 ;	genPlus
                           3661 ;	Peephole 236.g	used r5 instead of ar5
   0B63 ED                 3662 	mov	a,r5
   0B64 24 3A              3663 	add	a,#_ReadVariable_ptr_1_1
   0B66 F5 82              3664 	mov	dpl,a
                           3665 ;	Peephole 236.g	used r6 instead of ar6
   0B68 EE                 3666 	mov	a,r6
   0B69 34 00              3667 	addc	a,#(_ReadVariable_ptr_1_1 >> 8)
   0B6B F5 83              3668 	mov	dph,a
                           3669 ;	genCast
   0B6D 8A 07              3670 	mov	ar7,r2
                           3671 ;	genPointerSet
                           3672 ;     genFarPointerSet
   0B6F EF                 3673 	mov	a,r7
   0B70 F0                 3674 	movx	@dptr,a
                           3675 ;	main.c:500: k++;
                           3676 ;	genPlus
   0B71 90 00 38           3677 	mov	dptr,#_ReadVariable_k_1_1
                           3678 ;     genPlusIncr
   0B74 74 01              3679 	mov	a,#0x01
                           3680 ;	Peephole 236.a	used r5 instead of ar5
   0B76 2D                 3681 	add	a,r5
   0B77 F0                 3682 	movx	@dptr,a
                           3683 ;	Peephole 181	changed mov to clr
   0B78 E4                 3684 	clr	a
                           3685 ;	Peephole 236.b	used r6 instead of ar6
   0B79 3E                 3686 	addc	a,r6
   0B7A A3                 3687 	inc	dptr
   0B7B F0                 3688 	movx	@dptr,a
                           3689 ;	main.c:501: putchar(l);
                           3690 ;	genCast
                           3691 ;	genCall
   0B7C 8A 82              3692 	mov	dpl,r2
   0B7E C0 04              3693 	push	ar4
   0B80 12 0C DF           3694 	lcall	_putchar
   0B83 D0 04              3695 	pop	ar4
                           3696 ;	Peephole 112.b	changed ljmp to sjmp
   0B85 80 15              3697 	sjmp	00107$
   0B87                    3698 00102$:
                           3699 ;	main.c:505: k--;
                           3700 ;	genAssign
   0B87 90 00 38           3701 	mov	dptr,#_ReadVariable_k_1_1
   0B8A E0                 3702 	movx	a,@dptr
   0B8B FA                 3703 	mov	r2,a
   0B8C A3                 3704 	inc	dptr
   0B8D E0                 3705 	movx	a,@dptr
   0B8E FB                 3706 	mov	r3,a
                           3707 ;	genMinus
                           3708 ;	genMinusDec
   0B8F 1A                 3709 	dec	r2
   0B90 BA FF 01           3710 	cjne	r2,#0xff,00119$
   0B93 1B                 3711 	dec	r3
   0B94                    3712 00119$:
                           3713 ;	genAssign
   0B94 90 00 38           3714 	mov	dptr,#_ReadVariable_k_1_1
   0B97 EA                 3715 	mov	a,r2
   0B98 F0                 3716 	movx	@dptr,a
   0B99 A3                 3717 	inc	dptr
   0B9A EB                 3718 	mov	a,r3
   0B9B F0                 3719 	movx	@dptr,a
   0B9C                    3720 00107$:
                           3721 ;	main.c:508: }while(l!=READ_ENTER);
                           3722 ;	genIfx
   0B9C EC                 3723 	mov	a,r4
                           3724 ;	genIfxJump
                           3725 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0B9D 60 9F              3726 	jz	00106$
                           3727 ;	Peephole 300	removed redundant label 00120$
                           3728 ;	main.c:509: n=hex2int(ptr,k);
                           3729 ;	genAssign
   0B9F 90 00 38           3730 	mov	dptr,#_ReadVariable_k_1_1
   0BA2 E0                 3731 	movx	a,@dptr
   0BA3 FA                 3732 	mov	r2,a
   0BA4 A3                 3733 	inc	dptr
   0BA5 E0                 3734 	movx	a,@dptr
   0BA6 FB                 3735 	mov	r3,a
                           3736 ;	genAssign
   0BA7 90 00 2F           3737 	mov	dptr,#_hex2int_PARM_2
   0BAA EA                 3738 	mov	a,r2
   0BAB F0                 3739 	movx	@dptr,a
   0BAC A3                 3740 	inc	dptr
   0BAD EB                 3741 	mov	a,r3
   0BAE F0                 3742 	movx	@dptr,a
                           3743 ;	genCall
                           3744 ;	Peephole 182.a	used 16 bit load of DPTR
   0BAF 90 00 3A           3745 	mov	dptr,#_ReadVariable_ptr_1_1
   0BB2 75 F0 00           3746 	mov	b,#0x00
                           3747 ;	genCast
                           3748 ;	main.c:510: return n;
                           3749 ;	genRet
                           3750 ;	Peephole 150.h	removed misc moves via dph, dpl, b, a before return
                           3751 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0BB5 02 09 45           3752 	ljmp	_hex2int
                           3753 ;
                           3754 ;------------------------------------------------------------
                           3755 ;Allocation info for local variables in function 'interactive'
                           3756 ;------------------------------------------------------------
                           3757 ;co                        Allocated with name '_interactive_co_1_1'
                           3758 ;i                         Allocated with name '_interactive_i_1_1'
                           3759 ;------------------------------------------------------------
                           3760 ;	main.c:514: void interactive()
                           3761 ;	-----------------------------------------
                           3762 ;	 function interactive
                           3763 ;	-----------------------------------------
   0BB8                    3764 _interactive:
                           3765 ;	main.c:517: EA=0;
                           3766 ;	genAssign
   0BB8 C2 AF              3767 	clr	_EA
                           3768 ;	main.c:518: i=*INST_READ;
                           3769 ;	genPointerGet
                           3770 ;	genFarPointerGet
                           3771 ;	Peephole 182.b	used 16 bit load of dptr
   0BBA 90 F8 00           3772 	mov	dptr,#0xF800
   0BBD E0                 3773 	movx	a,@dptr
   0BBE FA                 3774 	mov	r2,a
                           3775 ;	main.c:519: EA=1;
                           3776 ;	genAssign
   0BBF D2 AF              3777 	setb	_EA
                           3778 ;	main.c:520: i=i&DDRAM_WRITE_MASK_BITS;
                           3779 ;	genAnd
   0BC1 53 02 80           3780 	anl	ar2,#0x80
                           3781 ;	main.c:521: EA=0;
                           3782 ;	genAssign
   0BC4 C2 AF              3783 	clr	_EA
                           3784 ;	main.c:522: *INST_WRITE=i;
                           3785 ;	genAssign
                           3786 ;	Peephole 182.b	used 16 bit load of dptr
   0BC6 90 F0 00           3787 	mov	dptr,#0xF000
                           3788 ;	genPointerSet
                           3789 ;     genFarPointerSet
   0BC9 EA                 3790 	mov	a,r2
   0BCA F0                 3791 	movx	@dptr,a
                           3792 ;	main.c:523: EA=1;
                           3793 ;	genAssign
   0BCB D2 AF              3794 	setb	_EA
                           3795 ;	main.c:524: putstr("Enter the character to display. Hit enter to stop\n\r");
                           3796 ;	genCall
                           3797 ;	Peephole 182.a	used 16 bit load of DPTR
   0BCD 90 27 0A           3798 	mov	dptr,#__str_18
   0BD0 75 F0 80           3799 	mov	b,#0x80
   0BD3 12 00 AA           3800 	lcall	_putstr
                           3801 ;	main.c:525: do
   0BD6                    3802 00103$:
                           3803 ;	main.c:526: {   co=getchar();
                           3804 ;	genCall
   0BD6 12 0C F1           3805 	lcall	_getchar
                           3806 ;	main.c:527: putchar(co);
                           3807 ;	genCall
   0BD9 AA 82              3808 	mov  r2,dpl
                           3809 ;	Peephole 177.a	removed redundant mov
   0BDB C0 02              3810 	push	ar2
   0BDD 12 0C DF           3811 	lcall	_putchar
   0BE0 D0 02              3812 	pop	ar2
                           3813 ;	main.c:528: if(co!=READ_ENTER)
                           3814 ;	genCmpEq
                           3815 ;	gencjne
                           3816 ;	gencjneshort
                           3817 ;	Peephole 241.d	optimized compare
   0BE2 E4                 3818 	clr	a
   0BE3 BA 0D 01           3819 	cjne	r2,#0x0D,00110$
   0BE6 04                 3820 	inc	a
   0BE7                    3821 00110$:
                           3822 ;	Peephole 300	removed redundant label 00111$
                           3823 ;	genIfx
   0BE7 FB                 3824 	mov	r3,a
                           3825 ;	Peephole 105	removed redundant mov
                           3826 ;	genIfxJump
                           3827 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0BE8 70 09              3828 	jnz	00104$
                           3829 ;	Peephole 300	removed redundant label 00112$
                           3830 ;	main.c:529: lcdputch(co);
                           3831 ;	genCall
   0BEA 8A 82              3832 	mov	dpl,r2
   0BEC C0 03              3833 	push	ar3
   0BEE 12 02 7D           3834 	lcall	_lcdputch
   0BF1 D0 03              3835 	pop	ar3
   0BF3                    3836 00104$:
                           3837 ;	main.c:530: }while(co!=READ_ENTER);
                           3838 ;	genIfx
   0BF3 EB                 3839 	mov	a,r3
                           3840 ;	genIfxJump
                           3841 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0BF4 60 E0              3842 	jz	00103$
                           3843 ;	Peephole 300	removed redundant label 00113$
                           3844 ;	Peephole 300	removed redundant label 00106$
   0BF6 22                 3845 	ret
                           3846 ;------------------------------------------------------------
                           3847 ;Allocation info for local variables in function 'gotoxy'
                           3848 ;------------------------------------------------------------
                           3849 ;temp                      Allocated with name '_gotoxy_temp_1_1'
                           3850 ;temp1                     Allocated with name '_gotoxy_temp1_1_1'
                           3851 ;------------------------------------------------------------
                           3852 ;	main.c:533: void gotoxy()
                           3853 ;	-----------------------------------------
                           3854 ;	 function gotoxy
                           3855 ;	-----------------------------------------
   0BF7                    3856 _gotoxy:
                           3857 ;	main.c:536: putstr("\n\rEnter row number\n\r");
                           3858 ;	genCall
                           3859 ;	Peephole 182.a	used 16 bit load of DPTR
   0BF7 90 27 3E           3860 	mov	dptr,#__str_19
   0BFA 75 F0 80           3861 	mov	b,#0x80
   0BFD 12 00 AA           3862 	lcall	_putstr
                           3863 ;	main.c:537: do
   0C00                    3864 00103$:
                           3865 ;	main.c:539: temp=input_value();
                           3866 ;	genCall
   0C00 12 01 42           3867 	lcall	_input_value
   0C03 AA 82              3868 	mov	r2,dpl
                           3869 ;	main.c:540: if(temp>3)
                           3870 ;	genCmpGt
                           3871 ;	genCmp
   0C05 C3                 3872 	clr	c
   0C06 74 03              3873 	mov	a,#0x03
   0C08 9A                 3874 	subb	a,r2
   0C09 E4                 3875 	clr	a
   0C0A 33                 3876 	rlc	a
                           3877 ;	genIfx
   0C0B FB                 3878 	mov	r3,a
                           3879 ;	Peephole 105	removed redundant mov
                           3880 ;	genIfxJump
                           3881 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0C0C 60 11              3882 	jz	00104$
                           3883 ;	Peephole 300	removed redundant label 00119$
                           3884 ;	main.c:541: putstr("\n\rEnter correct row\n\r");
                           3885 ;	genCall
                           3886 ;	Peephole 182.a	used 16 bit load of DPTR
   0C0E 90 27 53           3887 	mov	dptr,#__str_20
   0C11 75 F0 80           3888 	mov	b,#0x80
   0C14 C0 02              3889 	push	ar2
   0C16 C0 03              3890 	push	ar3
   0C18 12 00 AA           3891 	lcall	_putstr
   0C1B D0 03              3892 	pop	ar3
   0C1D D0 02              3893 	pop	ar2
   0C1F                    3894 00104$:
                           3895 ;	main.c:542: }while(temp>3);
                           3896 ;	genIfx
   0C1F EB                 3897 	mov	a,r3
                           3898 ;	genIfxJump
                           3899 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0C20 70 DE              3900 	jnz	00103$
                           3901 ;	Peephole 300	removed redundant label 00120$
                           3902 ;	main.c:543: putstr("\n\rEnter column number\n\r");
                           3903 ;	genCall
                           3904 ;	Peephole 182.a	used 16 bit load of DPTR
   0C22 90 27 69           3905 	mov	dptr,#__str_21
   0C25 75 F0 80           3906 	mov	b,#0x80
   0C28 C0 02              3907 	push	ar2
   0C2A 12 00 AA           3908 	lcall	_putstr
   0C2D D0 02              3909 	pop	ar2
                           3910 ;	main.c:544: do
   0C2F                    3911 00108$:
                           3912 ;	main.c:546: temp1=input_value();
                           3913 ;	genCall
   0C2F C0 02              3914 	push	ar2
   0C31 12 01 42           3915 	lcall	_input_value
   0C34 AB 82              3916 	mov	r3,dpl
   0C36 D0 02              3917 	pop	ar2
                           3918 ;	main.c:547: if(temp1>15)
                           3919 ;	genCmpGt
                           3920 ;	genCmp
   0C38 C3                 3921 	clr	c
   0C39 74 0F              3922 	mov	a,#0x0F
   0C3B 9B                 3923 	subb	a,r3
   0C3C E4                 3924 	clr	a
   0C3D 33                 3925 	rlc	a
                           3926 ;	genIfx
   0C3E FC                 3927 	mov	r4,a
                           3928 ;	Peephole 105	removed redundant mov
                           3929 ;	genIfxJump
                           3930 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0C3F 60 15              3931 	jz	00109$
                           3932 ;	Peephole 300	removed redundant label 00121$
                           3933 ;	main.c:548: putstr("\n\rEnter correct column number\n\r");
                           3934 ;	genCall
                           3935 ;	Peephole 182.a	used 16 bit load of DPTR
   0C41 90 27 81           3936 	mov	dptr,#__str_22
   0C44 75 F0 80           3937 	mov	b,#0x80
   0C47 C0 02              3938 	push	ar2
   0C49 C0 03              3939 	push	ar3
   0C4B C0 04              3940 	push	ar4
   0C4D 12 00 AA           3941 	lcall	_putstr
   0C50 D0 04              3942 	pop	ar4
   0C52 D0 03              3943 	pop	ar3
   0C54 D0 02              3944 	pop	ar2
   0C56                    3945 00109$:
                           3946 ;	main.c:549: }while(temp1>15);
                           3947 ;	genIfx
   0C56 EC                 3948 	mov	a,r4
                           3949 ;	genIfxJump
                           3950 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0C57 70 D6              3951 	jnz	00108$
                           3952 ;	Peephole 300	removed redundant label 00122$
                           3953 ;	main.c:550: lcdgotoxy(temp, temp1);
                           3954 ;	genAssign
   0C59 90 00 1D           3955 	mov	dptr,#_lcdgotoxy_PARM_2
   0C5C EB                 3956 	mov	a,r3
   0C5D F0                 3957 	movx	@dptr,a
                           3958 ;	genCall
   0C5E 8A 82              3959 	mov	dpl,r2
                           3960 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0C60 02 02 10           3961 	ljmp	_lcdgotoxy
                           3962 ;
                           3963 ;------------------------------------------------------------
                           3964 ;Allocation info for local variables in function 'gotoaddress'
                           3965 ;------------------------------------------------------------
                           3966 ;temp                      Allocated with name '_gotoaddress_temp_1_1'
                           3967 ;------------------------------------------------------------
                           3968 ;	main.c:553: void gotoaddress()
                           3969 ;	-----------------------------------------
                           3970 ;	 function gotoaddress
                           3971 ;	-----------------------------------------
   0C63                    3972 _gotoaddress:
                           3973 ;	main.c:555: unsigned char temp=0x66;
                           3974 ;	genAssign
   0C63 90 00 3B           3975 	mov	dptr,#_gotoaddress_temp_1_1
   0C66 74 66              3976 	mov	a,#0x66
   0C68 F0                 3977 	movx	@dptr,a
                           3978 ;	main.c:556: putstr("Enter the address in hex\n\r");
                           3979 ;	genCall
                           3980 ;	Peephole 182.a	used 16 bit load of DPTR
   0C69 90 27 A1           3981 	mov	dptr,#__str_23
   0C6C 75 F0 80           3982 	mov	b,#0x80
   0C6F 12 00 AA           3983 	lcall	_putstr
                           3984 ;	main.c:557: while((temp>0x1F && temp<0x40)||(temp>=0x5F))
   0C72                    3985 00107$:
                           3986 ;	genAssign
   0C72 90 00 3B           3987 	mov	dptr,#_gotoaddress_temp_1_1
   0C75 E0                 3988 	movx	a,@dptr
                           3989 ;	genCmpGt
                           3990 ;	genCmp
                           3991 ;	genIfxJump
                           3992 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           3993 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   0C76 FA                 3994 	mov  r2,a
                           3995 ;	Peephole 177.a	removed redundant mov
   0C77 24 E0              3996 	add	a,#0xff - 0x1F
   0C79 50 05              3997 	jnc	00106$
                           3998 ;	Peephole 300	removed redundant label 00115$
                           3999 ;	genCmpLt
                           4000 ;	genCmp
   0C7B BA 40 00           4001 	cjne	r2,#0x40,00116$
   0C7E                    4002 00116$:
                           4003 ;	genIfxJump
                           4004 ;	Peephole 112.b	changed ljmp to sjmp
                           4005 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0C7E 40 0A              4006 	jc	00108$
                           4007 ;	Peephole 300	removed redundant label 00117$
   0C80                    4008 00106$:
                           4009 ;	genAssign
   0C80 90 00 3B           4010 	mov	dptr,#_gotoaddress_temp_1_1
   0C83 E0                 4011 	movx	a,@dptr
   0C84 FA                 4012 	mov	r2,a
                           4013 ;	genCmpLt
                           4014 ;	genCmp
   0C85 BA 5F 00           4015 	cjne	r2,#0x5F,00118$
   0C88                    4016 00118$:
                           4017 ;	genIfxJump
                           4018 ;	Peephole 112.b	changed ljmp to sjmp
                           4019 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0C88 40 26              4020 	jc	00109$
                           4021 ;	Peephole 300	removed redundant label 00119$
   0C8A                    4022 00108$:
                           4023 ;	main.c:559: temp=ReadVariable();
                           4024 ;	genCall
   0C8A 12 0B 32           4025 	lcall	_ReadVariable
   0C8D AB 82              4026 	mov	r3,dpl
   0C8F AC 83              4027 	mov	r4,dph
                           4028 ;	genCast
                           4029 ;	genAssign
   0C91 90 00 3B           4030 	mov	dptr,#_gotoaddress_temp_1_1
   0C94 EB                 4031 	mov	a,r3
   0C95 F0                 4032 	movx	@dptr,a
                           4033 ;	main.c:560: if((temp>0x1F && temp<0x40)||(temp>=0x5F))
                           4034 ;	genCmpGt
                           4035 ;	genCmp
                           4036 ;	genIfxJump
                           4037 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           4038 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   0C96 EB                 4039 	mov	a,r3
   0C97 24 E0              4040 	add	a,#0xff - 0x1F
   0C99 50 05              4041 	jnc	00104$
                           4042 ;	Peephole 300	removed redundant label 00120$
                           4043 ;	genCmpLt
                           4044 ;	genCmp
   0C9B BB 40 00           4045 	cjne	r3,#0x40,00121$
   0C9E                    4046 00121$:
                           4047 ;	genIfxJump
                           4048 ;	Peephole 112.b	changed ljmp to sjmp
                           4049 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0C9E 40 05              4050 	jc	00101$
                           4051 ;	Peephole 300	removed redundant label 00122$
   0CA0                    4052 00104$:
                           4053 ;	genCmpLt
                           4054 ;	genCmp
   0CA0 BB 5F 00           4055 	cjne	r3,#0x5F,00123$
   0CA3                    4056 00123$:
                           4057 ;	genIfxJump
                           4058 ;	Peephole 112.b	changed ljmp to sjmp
                           4059 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0CA3 40 CD              4060 	jc	00107$
                           4061 ;	Peephole 300	removed redundant label 00124$
   0CA5                    4062 00101$:
                           4063 ;	main.c:561: putstr("\n\rEnter correct address values in hex\n\r");
                           4064 ;	genCall
                           4065 ;	Peephole 182.a	used 16 bit load of DPTR
   0CA5 90 27 BC           4066 	mov	dptr,#__str_24
   0CA8 75 F0 80           4067 	mov	b,#0x80
   0CAB 12 00 AA           4068 	lcall	_putstr
                           4069 ;	Peephole 112.b	changed ljmp to sjmp
   0CAE 80 C2              4070 	sjmp	00107$
   0CB0                    4071 00109$:
                           4072 ;	main.c:563: lcdgotoaddr(temp);
                           4073 ;	genCall
   0CB0 8A 82              4074 	mov	dpl,r2
   0CB2 12 01 E9           4075 	lcall	_lcdgotoaddr
                           4076 ;	main.c:564: putstr("\n\r");
                           4077 ;	genCall
                           4078 ;	Peephole 182.a	used 16 bit load of DPTR
   0CB5 90 25 9C           4079 	mov	dptr,#__str_3
   0CB8 75 F0 80           4080 	mov	b,#0x80
                           4081 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0CBB 02 00 AA           4082 	ljmp	_putstr
                           4083 ;
                           4084 ;------------------------------------------------------------
                           4085 ;Allocation info for local variables in function '_sdcc_external_startup'
                           4086 ;------------------------------------------------------------
                           4087 ;------------------------------------------------------------
                           4088 ;	main.c:567: _sdcc_external_startup()
                           4089 ;	-----------------------------------------
                           4090 ;	 function _sdcc_external_startup
                           4091 ;	-----------------------------------------
   0CBE                    4092 __sdcc_external_startup:
                           4093 ;	main.c:569: AUXR |= 0x0C;
                           4094 ;	genOr
   0CBE 43 8E 0C           4095 	orl	_AUXR,#0x0C
                           4096 ;	main.c:570: return 0;
                           4097 ;	genRet
                           4098 ;	Peephole 182.b	used 16 bit load of dptr
   0CC1 90 00 00           4099 	mov	dptr,#0x0000
                           4100 ;	Peephole 300	removed redundant label 00101$
   0CC4 22                 4101 	ret
                           4102 ;------------------------------------------------------------
                           4103 ;Allocation info for local variables in function 'delay'
                           4104 ;------------------------------------------------------------
                           4105 ;------------------------------------------------------------
                           4106 ;	main.c:575: void delay()
                           4107 ;	-----------------------------------------
                           4108 ;	 function delay
                           4109 ;	-----------------------------------------
   0CC5                    4110 _delay:
                           4111 ;	main.c:584: _endasm ;
                           4112 ;	genInline
   0CC5 7D 05              4113 	    DELAY:MOV R5, #5
   0CC7                    4114     DEL1:
   0CC7 7C 0A              4115 	MOV R4, #10
   0CC9                    4116     DEL:
   0CC9 00                 4117 	NOP
   0CCA DC FD              4118 	        DJNZ r4, DEL
   0CCC DD F9              4119 	         DJNZ R5, DEL1
   0CCE 22                 4120 	         ret
                           4121 ;	Peephole 300	removed redundant label 00101$
   0CCF 22                 4122 	ret
                           4123 ;------------------------------------------------------------
                           4124 ;Allocation info for local variables in function 'SerialInitialize'
                           4125 ;------------------------------------------------------------
                           4126 ;------------------------------------------------------------
                           4127 ;	main.c:587: void SerialInitialize()
                           4128 ;	-----------------------------------------
                           4129 ;	 function SerialInitialize
                           4130 ;	-----------------------------------------
   0CD0                    4131 _SerialInitialize:
                           4132 ;	main.c:589: TMOD|=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
                           4133 ;	genOr
   0CD0 43 89 20           4134 	orl	_TMOD,#0x20
                           4135 ;	main.c:590: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
                           4136 ;	genAssign
   0CD3 75 8D FD           4137 	mov	_TH1,#0xFD
                           4138 ;	main.c:591: TL1=0xFD;
                           4139 ;	genAssign
   0CD6 75 8B FD           4140 	mov	_TL1,#0xFD
                           4141 ;	main.c:592: SCON=0x50;
                           4142 ;	genAssign
   0CD9 75 98 50           4143 	mov	_SCON,#0x50
                           4144 ;	main.c:593: TR1=1;  // to start timer
                           4145 ;	genAssign
   0CDC D2 8E              4146 	setb	_TR1
                           4147 ;	Peephole 300	removed redundant label 00101$
   0CDE 22                 4148 	ret
                           4149 ;------------------------------------------------------------
                           4150 ;Allocation info for local variables in function 'putchar'
                           4151 ;------------------------------------------------------------
                           4152 ;x                         Allocated with name '_putchar_x_1_1'
                           4153 ;------------------------------------------------------------
                           4154 ;	main.c:597: void putchar(char x)
                           4155 ;	-----------------------------------------
                           4156 ;	 function putchar
                           4157 ;	-----------------------------------------
   0CDF                    4158 _putchar:
                           4159 ;	genReceive
   0CDF E5 82              4160 	mov	a,dpl
   0CE1 90 00 3C           4161 	mov	dptr,#_putchar_x_1_1
   0CE4 F0                 4162 	movx	@dptr,a
                           4163 ;	main.c:599: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
                           4164 ;	genAssign
   0CE5 90 00 3C           4165 	mov	dptr,#_putchar_x_1_1
   0CE8 E0                 4166 	movx	a,@dptr
   0CE9 F5 99              4167 	mov	_SBUF,a
                           4168 ;	main.c:600: while(!TI); // wait until transmission is completed. when transmission is completed TI bit become 1
   0CEB                    4169 00101$:
                           4170 ;	genIfx
                           4171 ;	genIfxJump
                           4172 ;	Peephole 108.d	removed ljmp by inverse jump logic
                           4173 ;	main.c:601: TI=0; // make TI bit to zero for next transmission
                           4174 ;	genAssign
                           4175 ;	Peephole 250.a	using atomic test and clear
   0CEB 10 99 02           4176 	jbc	_TI,00108$
   0CEE 80 FB              4177 	sjmp	00101$
   0CF0                    4178 00108$:
                           4179 ;	Peephole 300	removed redundant label 00104$
   0CF0 22                 4180 	ret
                           4181 ;------------------------------------------------------------
                           4182 ;Allocation info for local variables in function 'getchar'
                           4183 ;------------------------------------------------------------
                           4184 ;rec                       Allocated with name '_getchar_rec_1_1'
                           4185 ;------------------------------------------------------------
                           4186 ;	main.c:605: char getchar()
                           4187 ;	-----------------------------------------
                           4188 ;	 function getchar
                           4189 ;	-----------------------------------------
   0CF1                    4190 _getchar:
                           4191 ;	main.c:609: while(!RI);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
   0CF1                    4192 00101$:
                           4193 ;	genIfx
                           4194 ;	genIfxJump
                           4195 ;	Peephole 108.d	removed ljmp by inverse jump logic
   0CF1 30 98 FD           4196 	jnb	_RI,00101$
                           4197 ;	Peephole 300	removed redundant label 00108$
                           4198 ;	main.c:610: rec=SBUF;   // data is received in SBUF register present in controller during receiving
                           4199 ;	genAssign
   0CF4 AA 99              4200 	mov	r2,_SBUF
                           4201 ;	main.c:611: RI=0;  // make RI bit to 0 so that next data is received
                           4202 ;	genAssign
   0CF6 C2 98              4203 	clr	_RI
                           4204 ;	main.c:612: return rec;  // return rec where function is called
                           4205 ;	genRet
   0CF8 8A 82              4206 	mov	dpl,r2
                           4207 ;	Peephole 300	removed redundant label 00104$
   0CFA 22                 4208 	ret
                           4209 ;------------------------------------------------------------
                           4210 ;Allocation info for local variables in function 'startbit'
                           4211 ;------------------------------------------------------------
                           4212 ;------------------------------------------------------------
                           4213 ;	main.c:616: void startbit()
                           4214 ;	-----------------------------------------
                           4215 ;	 function startbit
                           4216 ;	-----------------------------------------
   0CFB                    4217 _startbit:
                           4218 ;	main.c:626: _endasm ;
                           4219 ;	genInline
   0CFB D2 94              4220 	        SETB P1.4
   0CFD D2 93              4221 	        SETB P1.3
   0CFF 91 C5              4222 	        ACALL DELAY
   0D01 C2 94              4223 	        CLR P1.4
   0D03 91 C5              4224 	        ACALL DELAY
   0D05 C2 93              4225 	        CLR P1.3
   0D07 91 C5              4226 	        ACALL DELAY
                           4227 ;	Peephole 300	removed redundant label 00101$
   0D09 22                 4228 	ret
                           4229 ;------------------------------------------------------------
                           4230 ;Allocation info for local variables in function 'stopbit'
                           4231 ;------------------------------------------------------------
                           4232 ;------------------------------------------------------------
                           4233 ;	main.c:630: void stopbit()
                           4234 ;	-----------------------------------------
                           4235 ;	 function stopbit
                           4236 ;	-----------------------------------------
   0D0A                    4237 _stopbit:
                           4238 ;	main.c:640: _endasm;
                           4239 ;	genInline
   0D0A 91 C5              4240 	        ACALL DELAY
   0D0C C2 94              4241 	        CLR P1.4
   0D0E 91 C5              4242 	        ACALL DELAY
   0D10 D2 93              4243 	        SETB P1.3
   0D12 91 C5              4244 	        ACALL DELAY
   0D14 D2 94              4245 	        SETB P1.4
   0D16 91 C5              4246 	        ACALL DELAY
                           4247 ;	Peephole 300	removed redundant label 00101$
   0D18 22                 4248 	ret
                           4249 ;------------------------------------------------------------
                           4250 ;Allocation info for local variables in function 'ackbit'
                           4251 ;------------------------------------------------------------
                           4252 ;------------------------------------------------------------
                           4253 ;	main.c:644: void ackbit()
                           4254 ;	-----------------------------------------
                           4255 ;	 function ackbit
                           4256 ;	-----------------------------------------
   0D19                    4257 _ackbit:
                           4258 ;	main.c:657: _endasm;
                           4259 ;	genInline
   0D19 C3                 4260 	        CLR C
   0D1A 91 C5              4261 	        ACALL DELAY
   0D1C D2 94              4262 	        SETB P1.4
   0D1E 91 C5              4263 	        ACALL DELAY
   0D20 D2 93              4264 	        SETB P1.3
   0D22 91 C5              4265 	        ACALL DELAY
   0D24 A2 94              4266 	        MOV C, P1.4
   0D26 91 C5              4267 	        ACALL DELAY
   0D28 C2 93              4268 	        CLR P1.3
   0D2A 91 C5              4269 	        ACALL DELAY
                           4270 ;	Peephole 300	removed redundant label 00101$
   0D2C 22                 4271 	ret
                           4272 ;------------------------------------------------------------
                           4273 ;Allocation info for local variables in function 'Master_ack'
                           4274 ;------------------------------------------------------------
                           4275 ;------------------------------------------------------------
                           4276 ;	main.c:662: void Master_ack()
                           4277 ;	-----------------------------------------
                           4278 ;	 function Master_ack
                           4279 ;	-----------------------------------------
   0D2D                    4280 _Master_ack:
                           4281 ;	main.c:675: _endasm;
                           4282 ;	genInline
   0D2D 91 C5              4283 	        ACALL DELAY
   0D2F 91 C5              4284 	        ACALL DELAY
   0D31 C2 94              4285 	        CLR P1.4
   0D33 91 C5              4286 	        ACALL DELAY
   0D35 D2 93              4287 	        SETB P1.3
   0D37 91 C5              4288 	        ACALL DELAY
   0D39 91 C5              4289 	        ACALL DELAY
   0D3B C2 93              4290 	        CLR P1.3
   0D3D 00                 4291 	        nop
   0D3E D2 94              4292 	        setb P1.4
                           4293 ;	Peephole 300	removed redundant label 00101$
   0D40 22                 4294 	ret
                           4295 ;------------------------------------------------------------
                           4296 ;Allocation info for local variables in function 'send_data_assembly'
                           4297 ;------------------------------------------------------------
                           4298 ;------------------------------------------------------------
                           4299 ;	main.c:679: void send_data_assembly()
                           4300 ;	-----------------------------------------
                           4301 ;	 function send_data_assembly
                           4302 ;	-----------------------------------------
   0D41                    4303 _send_data_assembly:
                           4304 ;	main.c:697: _endasm;
                           4305 ;	genInline
   0D41 91 C5              4306 	        ACALL DELAY
   0D43 79 08              4307 	        MOV R1, #8
   0D45 C3                 4308 	        HERE:CLR C
   0D46 33                 4309 	            RLC A
   0D47 50 04              4310 	            JNC NEXT
   0D49 D2 94              4311 	            SETB P1.4
   0D4B A1 4F              4312 	            AJMP NEXT1
   0D4D                    4313         NEXT:
   0D4D C2 94              4314 	CLR P1.4
   0D4F                    4315         NEXT1:
   0D4F 91 C5              4316 	            ACALL DELAY
   0D51 D2 93              4317 	            SETB P1.3
   0D53 91 C5              4318 	            ACALL DELAY
   0D55 C2 93              4319 	            CLR P1.3
   0D57 91 C5              4320 	            ACALL DELAY
   0D59 D9 EA              4321 	            DJNZ R1, HERE
                           4322 ;	Peephole 300	removed redundant label 00101$
   0D5B 22                 4323 	ret
                           4324 ;------------------------------------------------------------
                           4325 ;Allocation info for local variables in function 'send_data'
                           4326 ;------------------------------------------------------------
                           4327 ;value                     Allocated with name '_send_data_value_1_1'
                           4328 ;status_send               Allocated with name '_send_data_status_send_1_1'
                           4329 ;------------------------------------------------------------
                           4330 ;	main.c:700: int send_data(unsigned char value)
                           4331 ;	-----------------------------------------
                           4332 ;	 function send_data
                           4333 ;	-----------------------------------------
   0D5C                    4334 _send_data:
                           4335 ;	genReceive
   0D5C E5 82              4336 	mov	a,dpl
   0D5E 90 00 3D           4337 	mov	dptr,#_send_data_value_1_1
   0D61 F0                 4338 	movx	@dptr,a
                           4339 ;	main.c:703: ACC = value;
                           4340 ;	genAssign
   0D62 90 00 3D           4341 	mov	dptr,#_send_data_value_1_1
   0D65 E0                 4342 	movx	a,@dptr
   0D66 F5 E0              4343 	mov	_ACC,a
                           4344 ;	main.c:704: CY=1;
                           4345 ;	genAssign
   0D68 D2 D7              4346 	setb	_CY
                           4347 ;	main.c:705: send_data_assembly();
                           4348 ;	genCall
   0D6A 12 0D 41           4349 	lcall	_send_data_assembly
                           4350 ;	main.c:706: ackbit();
                           4351 ;	genCall
   0D6D 12 0D 19           4352 	lcall	_ackbit
                           4353 ;	main.c:707: ACC = 0x00;
                           4354 ;	genAssign
   0D70 75 E0 00           4355 	mov	_ACC,#0x00
                           4356 ;	main.c:708: return status_send;
                           4357 ;	genRet
                           4358 ;	Peephole 182.b	used 16 bit load of dptr
   0D73 90 00 01           4359 	mov	dptr,#0x0001
                           4360 ;	Peephole 300	removed redundant label 00101$
   0D76 22                 4361 	ret
                           4362 ;------------------------------------------------------------
                           4363 ;Allocation info for local variables in function 'ninthbit'
                           4364 ;------------------------------------------------------------
                           4365 ;------------------------------------------------------------
                           4366 ;	main.c:711: void ninthbit()
                           4367 ;	-----------------------------------------
                           4368 ;	 function ninthbit
                           4369 ;	-----------------------------------------
   0D77                    4370 _ninthbit:
                           4371 ;	main.c:722: _endasm ;
                           4372 ;	genInline
   0D77 91 C5              4373 	    ACALL DELAY
   0D79 D2 94              4374 	    SETB P1.4
   0D7B 91 C5              4375 	    ACALL DELAY
   0D7D D2 93              4376 	    SETB P1.3
   0D7F 91 C5              4377 	    ACALL DELAY
   0D81 91 C5              4378 	    ACALL DELAY
   0D83 C2 93              4379 	    CLR P1.3
   0D85 91 C5              4380 	    ACALL DELAY
                           4381 ;	Peephole 300	removed redundant label 00101$
   0D87 22                 4382 	ret
                           4383 ;------------------------------------------------------------
                           4384 ;Allocation info for local variables in function 'clock_func'
                           4385 ;------------------------------------------------------------
                           4386 ;------------------------------------------------------------
                           4387 ;	main.c:726: void clock_func()
                           4388 ;	-----------------------------------------
                           4389 ;	 function clock_func
                           4390 ;	-----------------------------------------
   0D88                    4391 _clock_func:
                           4392 ;	main.c:743: _endasm;
                           4393 ;	genInline
   0D88 74 00              4394 	            MOV A, #0
   0D8A 7F 08              4395 	            MOV R7, #8
   0D8C                    4396         LOOP2:
   0D8C 00                 4397 	NOP
   0D8D 91 C5              4398 	                ACALL DELAY
   0D8F C2 93              4399 	                clr P1.3
   0D91 91 C5              4400 	                ACALL DELAY
   0D93 D2 93              4401 	                setb P1.3
   0D95 A2 94              4402 	                MOV C, P1.4
   0D97 33                 4403 	                RLC A
   0D98 DF F2              4404 	                DJNZ R7, LOOP2
   0D9A 91 C5              4405 	                ACALL DELAY
   0D9C C2 93              4406 	                clr P1.3
   0D9E 91 C5              4407 	                ACALL DELAY
                           4408 ;	Peephole 300	removed redundant label 00101$
   0DA0 22                 4409 	ret
                           4410 ;------------------------------------------------------------
                           4411 ;Allocation info for local variables in function 'eebytew'
                           4412 ;------------------------------------------------------------
                           4413 ;databyte                  Allocated with name '_eebytew_PARM_2'
                           4414 ;addr                      Allocated with name '_eebytew_addr_1_1'
                           4415 ;address                   Allocated with name '_eebytew_address_1_1'
                           4416 ;result                    Allocated with name '_eebytew_result_1_1'
                           4417 ;result1                   Allocated with name '_eebytew_result1_1_1'
                           4418 ;result2                   Allocated with name '_eebytew_result2_1_1'
                           4419 ;temp                      Allocated with name '_eebytew_temp_1_1'
                           4420 ;tempaddr                  Allocated with name '_eebytew_tempaddr_1_1'
                           4421 ;------------------------------------------------------------
                           4422 ;	main.c:747: int eebytew(uint16_t addr, unsigned char databyte)  // write byte, returns status
                           4423 ;	-----------------------------------------
                           4424 ;	 function eebytew
                           4425 ;	-----------------------------------------
   0DA1                    4426 _eebytew:
                           4427 ;	genReceive
   0DA1 AA 83              4428 	mov	r2,dph
   0DA3 E5 82              4429 	mov	a,dpl
   0DA5 90 00 3F           4430 	mov	dptr,#_eebytew_addr_1_1
   0DA8 F0                 4431 	movx	@dptr,a
   0DA9 A3                 4432 	inc	dptr
   0DAA EA                 4433 	mov	a,r2
   0DAB F0                 4434 	movx	@dptr,a
                           4435 ;	main.c:752: printf("\n\rWriting 0x%03X: 0x%02X\n\r",addr, databyte);
                           4436 ;	genAssign
   0DAC 90 00 3E           4437 	mov	dptr,#_eebytew_PARM_2
   0DAF E0                 4438 	movx	a,@dptr
   0DB0 FA                 4439 	mov	r2,a
                           4440 ;	genCast
   0DB1 8A 03              4441 	mov	ar3,r2
   0DB3 7C 00              4442 	mov	r4,#0x00
                           4443 ;	genAssign
   0DB5 90 00 3F           4444 	mov	dptr,#_eebytew_addr_1_1
   0DB8 E0                 4445 	movx	a,@dptr
   0DB9 FD                 4446 	mov	r5,a
   0DBA A3                 4447 	inc	dptr
   0DBB E0                 4448 	movx	a,@dptr
   0DBC FE                 4449 	mov	r6,a
                           4450 ;	genIpush
   0DBD C0 02              4451 	push	ar2
   0DBF C0 05              4452 	push	ar5
   0DC1 C0 06              4453 	push	ar6
   0DC3 C0 03              4454 	push	ar3
   0DC5 C0 04              4455 	push	ar4
                           4456 ;	genIpush
   0DC7 C0 05              4457 	push	ar5
   0DC9 C0 06              4458 	push	ar6
                           4459 ;	genIpush
   0DCB 74 E4              4460 	mov	a,#__str_25
   0DCD C0 E0              4461 	push	acc
   0DCF 74 27              4462 	mov	a,#(__str_25 >> 8)
   0DD1 C0 E0              4463 	push	acc
   0DD3 74 80              4464 	mov	a,#0x80
   0DD5 C0 E0              4465 	push	acc
                           4466 ;	genCall
   0DD7 12 1C 74           4467 	lcall	_printf
   0DDA E5 81              4468 	mov	a,sp
   0DDC 24 F9              4469 	add	a,#0xf9
   0DDE F5 81              4470 	mov	sp,a
   0DE0 D0 06              4471 	pop	ar6
   0DE2 D0 05              4472 	pop	ar5
   0DE4 D0 02              4473 	pop	ar2
                           4474 ;	main.c:755: address &=I2C_BLOCKADDR_MASK_BITS;
                           4475 ;	genAnd
   0DE6 7B 00              4476 	mov	r3,#0x00
   0DE8 74 07              4477 	mov	a,#0x07
   0DEA 5E                 4478 	anl	a,r6
                           4479 ;	main.c:756: address >>=I2C_BLOCKADD_SHIFT_BITS;
                           4480 ;	genRightShift
                           4481 ;	genRightShiftLiteral
                           4482 ;	genrshTwo
   0DEB FC                 4483 	mov	r4,a
                           4484 ;	Peephole 105	removed redundant mov
   0DEC A2 E7              4485 	mov	c,acc.7
   0DEE CB                 4486 	xch	a,r3
   0DEF 33                 4487 	rlc	a
   0DF0 CB                 4488 	xch	a,r3
   0DF1 33                 4489 	rlc	a
   0DF2 CB                 4490 	xch	a,r3
   0DF3 54 01              4491 	anl	a,#0x01
   0DF5 FC                 4492 	mov	r4,a
                           4493 ;	main.c:757: temp |= address;
                           4494 ;	genOr
   0DF6 43 03 A0           4495 	orl	ar3,#0xA0
                           4496 ;	genCast
                           4497 ;	main.c:758: temp &= I2C_WRITE_MASK_BITS;
                           4498 ;	genAnd
   0DF9 53 03 FE           4499 	anl	ar3,#0xFE
                           4500 ;	main.c:759: tempaddr=addr;
                           4501 ;	genCast
                           4502 ;	main.c:760: startbit();
                           4503 ;	genCall
   0DFC C0 02              4504 	push	ar2
   0DFE C0 03              4505 	push	ar3
   0E00 C0 05              4506 	push	ar5
   0E02 12 0C FB           4507 	lcall	_startbit
   0E05 D0 05              4508 	pop	ar5
   0E07 D0 03              4509 	pop	ar3
   0E09 D0 02              4510 	pop	ar2
                           4511 ;	main.c:761: result = send_data(temp);
                           4512 ;	genCall
   0E0B 8B 82              4513 	mov	dpl,r3
   0E0D C0 02              4514 	push	ar2
   0E0F C0 05              4515 	push	ar5
   0E11 12 0D 5C           4516 	lcall	_send_data
   0E14 AB 82              4517 	mov	r3,dpl
   0E16 AC 83              4518 	mov	r4,dph
   0E18 D0 05              4519 	pop	ar5
   0E1A D0 02              4520 	pop	ar2
                           4521 ;	main.c:762: result1 = send_data(tempaddr);
                           4522 ;	genCall
   0E1C 8D 82              4523 	mov	dpl,r5
   0E1E C0 02              4524 	push	ar2
   0E20 C0 03              4525 	push	ar3
   0E22 C0 04              4526 	push	ar4
   0E24 12 0D 5C           4527 	lcall	_send_data
   0E27 AD 82              4528 	mov	r5,dpl
   0E29 AE 83              4529 	mov	r6,dph
   0E2B D0 04              4530 	pop	ar4
   0E2D D0 03              4531 	pop	ar3
   0E2F D0 02              4532 	pop	ar2
                           4533 ;	main.c:763: result2 = send_data(databyte);
                           4534 ;	genCall
   0E31 8A 82              4535 	mov	dpl,r2
   0E33 C0 03              4536 	push	ar3
   0E35 C0 04              4537 	push	ar4
   0E37 C0 05              4538 	push	ar5
   0E39 C0 06              4539 	push	ar6
   0E3B 12 0D 5C           4540 	lcall	_send_data
   0E3E D0 06              4541 	pop	ar6
   0E40 D0 05              4542 	pop	ar5
   0E42 D0 04              4543 	pop	ar4
   0E44 D0 03              4544 	pop	ar3
                           4545 ;	main.c:764: stopbit();
                           4546 ;	genCall
   0E46 C0 03              4547 	push	ar3
   0E48 C0 04              4548 	push	ar4
   0E4A C0 05              4549 	push	ar5
   0E4C C0 06              4550 	push	ar6
   0E4E 12 0D 0A           4551 	lcall	_stopbit
   0E51 D0 06              4552 	pop	ar6
   0E53 D0 05              4553 	pop	ar5
   0E55 D0 04              4554 	pop	ar4
   0E57 D0 03              4555 	pop	ar3
                           4556 ;	main.c:765: confirm=1;
                           4557 ;	genAssign
   0E59 90 00 BD           4558 	mov	dptr,#_confirm
   0E5C 74 01              4559 	mov	a,#0x01
   0E5E F0                 4560 	movx	@dptr,a
   0E5F E4                 4561 	clr	a
   0E60 A3                 4562 	inc	dptr
   0E61 F0                 4563 	movx	@dptr,a
                           4564 ;	main.c:766: return (result&result1);
                           4565 ;	genAnd
   0E62 ED                 4566 	mov	a,r5
   0E63 52 03              4567 	anl	ar3,a
   0E65 EE                 4568 	mov	a,r6
   0E66 52 04              4569 	anl	ar4,a
                           4570 ;	genRet
   0E68 8B 82              4571 	mov	dpl,r3
   0E6A 8C 83              4572 	mov	dph,r4
                           4573 ;	Peephole 300	removed redundant label 00101$
   0E6C 22                 4574 	ret
                           4575 ;------------------------------------------------------------
                           4576 ;Allocation info for local variables in function 'eebyter'
                           4577 ;------------------------------------------------------------
                           4578 ;addr                      Allocated with name '_eebyter_addr_1_1'
                           4579 ;address                   Allocated with name '_eebyter_address_1_1'
                           4580 ;temp                      Allocated with name '_eebyter_temp_1_1'
                           4581 ;temp1                     Allocated with name '_eebyter_temp1_1_1'
                           4582 ;tempaddr                  Allocated with name '_eebyter_tempaddr_1_1'
                           4583 ;backup                    Allocated with name '_eebyter_backup_1_1'
                           4584 ;------------------------------------------------------------
                           4585 ;	main.c:770: int eebyter(uint16_t addr)
                           4586 ;	-----------------------------------------
                           4587 ;	 function eebyter
                           4588 ;	-----------------------------------------
   0E6D                    4589 _eebyter:
                           4590 ;	genReceive
   0E6D AA 83              4591 	mov	r2,dph
   0E6F E5 82              4592 	mov	a,dpl
   0E71 90 00 41           4593 	mov	dptr,#_eebyter_addr_1_1
   0E74 F0                 4594 	movx	@dptr,a
   0E75 A3                 4595 	inc	dptr
   0E76 EA                 4596 	mov	a,r2
   0E77 F0                 4597 	movx	@dptr,a
                           4598 ;	main.c:775: address=addr;
                           4599 ;	genAssign
   0E78 90 00 41           4600 	mov	dptr,#_eebyter_addr_1_1
   0E7B E0                 4601 	movx	a,@dptr
   0E7C FA                 4602 	mov	r2,a
   0E7D A3                 4603 	inc	dptr
   0E7E E0                 4604 	movx	a,@dptr
   0E7F FB                 4605 	mov	r3,a
                           4606 ;	main.c:776: address &=I2C_BLOCKADDR_MASK_BITS;
                           4607 ;	genAnd
   0E80 7C 00              4608 	mov	r4,#0x00
   0E82 74 07              4609 	mov	a,#0x07
   0E84 5B                 4610 	anl	a,r3
                           4611 ;	main.c:777: address >>=I2C_BLOCKADD_SHIFT_BITS;
                           4612 ;	genRightShift
                           4613 ;	genRightShiftLiteral
                           4614 ;	genrshTwo
   0E85 FD                 4615 	mov	r5,a
                           4616 ;	Peephole 105	removed redundant mov
   0E86 A2 E7              4617 	mov	c,acc.7
   0E88 CC                 4618 	xch	a,r4
   0E89 33                 4619 	rlc	a
   0E8A CC                 4620 	xch	a,r4
   0E8B 33                 4621 	rlc	a
   0E8C CC                 4622 	xch	a,r4
   0E8D 54 01              4623 	anl	a,#0x01
   0E8F FD                 4624 	mov	r5,a
                           4625 ;	main.c:778: temp |= address;
                           4626 ;	genOr
   0E90 43 04 A0           4627 	orl	ar4,#0xA0
                           4628 ;	genCast
                           4629 ;	main.c:779: temp1=(temp&I2C_WRITE_MASK_BITS);
                           4630 ;	genAnd
   0E93 74 FE              4631 	mov	a,#0xFE
   0E95 5C                 4632 	anl	a,r4
   0E96 FD                 4633 	mov	r5,a
                           4634 ;	main.c:780: tempaddr=addr;
                           4635 ;	genCast
   0E97 8A 06              4636 	mov	ar6,r2
                           4637 ;	main.c:781: startbit();
                           4638 ;	genCall
   0E99 C0 02              4639 	push	ar2
   0E9B C0 03              4640 	push	ar3
   0E9D C0 04              4641 	push	ar4
   0E9F C0 05              4642 	push	ar5
   0EA1 C0 06              4643 	push	ar6
   0EA3 12 0C FB           4644 	lcall	_startbit
   0EA6 D0 06              4645 	pop	ar6
   0EA8 D0 05              4646 	pop	ar5
   0EAA D0 04              4647 	pop	ar4
   0EAC D0 03              4648 	pop	ar3
   0EAE D0 02              4649 	pop	ar2
                           4650 ;	main.c:782: send_data(temp1);
                           4651 ;	genCall
   0EB0 8D 82              4652 	mov	dpl,r5
   0EB2 C0 02              4653 	push	ar2
   0EB4 C0 03              4654 	push	ar3
   0EB6 C0 04              4655 	push	ar4
   0EB8 C0 06              4656 	push	ar6
   0EBA 12 0D 5C           4657 	lcall	_send_data
   0EBD D0 06              4658 	pop	ar6
   0EBF D0 04              4659 	pop	ar4
   0EC1 D0 03              4660 	pop	ar3
   0EC3 D0 02              4661 	pop	ar2
                           4662 ;	main.c:783: send_data(tempaddr);
                           4663 ;	genCall
   0EC5 8E 82              4664 	mov	dpl,r6
   0EC7 C0 02              4665 	push	ar2
   0EC9 C0 03              4666 	push	ar3
   0ECB C0 04              4667 	push	ar4
   0ECD 12 0D 5C           4668 	lcall	_send_data
   0ED0 D0 04              4669 	pop	ar4
   0ED2 D0 03              4670 	pop	ar3
   0ED4 D0 02              4671 	pop	ar2
                           4672 ;	main.c:784: startbit();
                           4673 ;	genCall
   0ED6 C0 02              4674 	push	ar2
   0ED8 C0 03              4675 	push	ar3
   0EDA C0 04              4676 	push	ar4
   0EDC 12 0C FB           4677 	lcall	_startbit
   0EDF D0 04              4678 	pop	ar4
   0EE1 D0 03              4679 	pop	ar3
   0EE3 D0 02              4680 	pop	ar2
                           4681 ;	main.c:785: temp=temp | I2C_READ_MASK_BITS;
                           4682 ;	genOr
   0EE5 43 04 01           4683 	orl	ar4,#0x01
                           4684 ;	main.c:786: send_data(temp);
                           4685 ;	genCall
   0EE8 8C 82              4686 	mov	dpl,r4
   0EEA C0 02              4687 	push	ar2
   0EEC C0 03              4688 	push	ar3
   0EEE 12 0D 5C           4689 	lcall	_send_data
   0EF1 D0 03              4690 	pop	ar3
   0EF3 D0 02              4691 	pop	ar2
                           4692 ;	main.c:787: clock_func();
                           4693 ;	genCall
   0EF5 C0 02              4694 	push	ar2
   0EF7 C0 03              4695 	push	ar3
   0EF9 12 0D 88           4696 	lcall	_clock_func
   0EFC D0 03              4697 	pop	ar3
   0EFE D0 02              4698 	pop	ar2
                           4699 ;	main.c:788: stopbit();
                           4700 ;	genCall
   0F00 C0 02              4701 	push	ar2
   0F02 C0 03              4702 	push	ar3
   0F04 12 0D 0A           4703 	lcall	_stopbit
   0F07 D0 03              4704 	pop	ar3
   0F09 D0 02              4705 	pop	ar2
                           4706 ;	main.c:789: backup=ACC;
                           4707 ;	genAssign
                           4708 ;	main.c:790: printf("\n\r0x%03X: 0x%02X\n\r", addr, backup);
                           4709 ;	genAssign
   0F0B 90 00 43           4710 	mov	dptr,#_eebyter_backup_1_1
   0F0E E5 E0              4711 	mov	a,_ACC
   0F10 F0                 4712 	movx	@dptr,a
                           4713 ;	Peephole 180.a	removed redundant mov to dptr
   0F11 E0                 4714 	movx	a,@dptr
   0F12 FC                 4715 	mov	r4,a
                           4716 ;	genCast
   0F13 7D 00              4717 	mov	r5,#0x00
                           4718 ;	genIpush
   0F15 C0 04              4719 	push	ar4
   0F17 C0 05              4720 	push	ar5
                           4721 ;	genIpush
   0F19 C0 02              4722 	push	ar2
   0F1B C0 03              4723 	push	ar3
                           4724 ;	genIpush
   0F1D 74 FF              4725 	mov	a,#__str_26
   0F1F C0 E0              4726 	push	acc
   0F21 74 27              4727 	mov	a,#(__str_26 >> 8)
   0F23 C0 E0              4728 	push	acc
   0F25 74 80              4729 	mov	a,#0x80
   0F27 C0 E0              4730 	push	acc
                           4731 ;	genCall
   0F29 12 1C 74           4732 	lcall	_printf
   0F2C E5 81              4733 	mov	a,sp
   0F2E 24 F9              4734 	add	a,#0xf9
   0F30 F5 81              4735 	mov	sp,a
                           4736 ;	main.c:791: return 0;
                           4737 ;	genRet
                           4738 ;	Peephole 182.b	used 16 bit load of dptr
   0F32 90 00 00           4739 	mov	dptr,#0x0000
                           4740 ;	Peephole 300	removed redundant label 00101$
   0F35 22                 4741 	ret
                           4742 ;------------------------------------------------------------
                           4743 ;Allocation info for local variables in function 'readall_function'
                           4744 ;------------------------------------------------------------
                           4745 ;address                   Allocated with name '_readall_function_address_1_1'
                           4746 ;temp                      Allocated with name '_readall_function_temp_1_1'
                           4747 ;temp1                     Allocated with name '_readall_function_temp1_1_1'
                           4748 ;tempaddr                  Allocated with name '_readall_function_tempaddr_1_1'
                           4749 ;backup                    Allocated with name '_readall_function_backup_1_1'
                           4750 ;i                         Allocated with name '_readall_function_i_1_1'
                           4751 ;tempvalue                 Allocated with name '_readall_function_tempvalue_1_1'
                           4752 ;tempvalue1                Allocated with name '_readall_function_tempvalue1_1_1'
                           4753 ;------------------------------------------------------------
                           4754 ;	main.c:795: void readall_function()
                           4755 ;	-----------------------------------------
                           4756 ;	 function readall_function
                           4757 ;	-----------------------------------------
   0F36                    4758 _readall_function:
                           4759 ;	main.c:800: putstr("Enter the address\n\r");
                           4760 ;	genCall
                           4761 ;	Peephole 182.a	used 16 bit load of DPTR
   0F36 90 28 12           4762 	mov	dptr,#__str_27
   0F39 75 F0 80           4763 	mov	b,#0x80
   0F3C 12 00 AA           4764 	lcall	_putstr
                           4765 ;	main.c:801: do
   0F3F                    4766 00103$:
                           4767 ;	main.c:803: tempvalue = ReadVariable();
                           4768 ;	genCall
   0F3F 12 0B 32           4769 	lcall	_ReadVariable
   0F42 AA 82              4770 	mov	r2,dpl
   0F44 AB 83              4771 	mov	r3,dph
                           4772 ;	genAssign
   0F46 90 00 45           4773 	mov	dptr,#_readall_function_tempvalue_1_1
   0F49 EA                 4774 	mov	a,r2
   0F4A F0                 4775 	movx	@dptr,a
   0F4B A3                 4776 	inc	dptr
   0F4C EB                 4777 	mov	a,r3
   0F4D F0                 4778 	movx	@dptr,a
                           4779 ;	main.c:804: if(tempvalue>I2C_ADDR_MASK_BITS)
                           4780 ;	genCmpGt
                           4781 ;	genCmp
   0F4E C3                 4782 	clr	c
   0F4F 74 FF              4783 	mov	a,#0xFF
   0F51 9A                 4784 	subb	a,r2
   0F52 74 07              4785 	mov	a,#0x07
   0F54 9B                 4786 	subb	a,r3
   0F55 E4                 4787 	clr	a
   0F56 33                 4788 	rlc	a
                           4789 ;	genIfx
   0F57 FC                 4790 	mov	r4,a
                           4791 ;	Peephole 105	removed redundant mov
                           4792 ;	genIfxJump
                           4793 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0F58 60 15              4794 	jz	00104$
                           4795 ;	Peephole 300	removed redundant label 00134$
                           4796 ;	main.c:805: putstr("Enter the correct value\n\r");
                           4797 ;	genCall
                           4798 ;	Peephole 182.a	used 16 bit load of DPTR
   0F5A 90 28 26           4799 	mov	dptr,#__str_28
   0F5D 75 F0 80           4800 	mov	b,#0x80
   0F60 C0 02              4801 	push	ar2
   0F62 C0 03              4802 	push	ar3
   0F64 C0 04              4803 	push	ar4
   0F66 12 00 AA           4804 	lcall	_putstr
   0F69 D0 04              4805 	pop	ar4
   0F6B D0 03              4806 	pop	ar3
   0F6D D0 02              4807 	pop	ar2
   0F6F                    4808 00104$:
                           4809 ;	main.c:806: }while(tempvalue>I2C_ADDR_MASK_BITS);
                           4810 ;	genIfx
   0F6F EC                 4811 	mov	a,r4
                           4812 ;	genIfxJump
                           4813 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0F70 70 CD              4814 	jnz	00103$
                           4815 ;	Peephole 300	removed redundant label 00135$
                           4816 ;	main.c:807: putstr("\n\rEnter the second higher address\n\r");
                           4817 ;	genCall
                           4818 ;	Peephole 182.a	used 16 bit load of DPTR
   0F72 90 28 40           4819 	mov	dptr,#__str_29
   0F75 75 F0 80           4820 	mov	b,#0x80
   0F78 C0 02              4821 	push	ar2
   0F7A C0 03              4822 	push	ar3
   0F7C 12 00 AA           4823 	lcall	_putstr
   0F7F D0 03              4824 	pop	ar3
   0F81 D0 02              4825 	pop	ar2
                           4826 ;	main.c:808: do
   0F83                    4827 00108$:
                           4828 ;	main.c:810: tempvalue1 = ReadVariable();
                           4829 ;	genCall
   0F83 C0 02              4830 	push	ar2
   0F85 C0 03              4831 	push	ar3
   0F87 12 0B 32           4832 	lcall	_ReadVariable
   0F8A AC 82              4833 	mov	r4,dpl
   0F8C AD 83              4834 	mov	r5,dph
   0F8E D0 03              4835 	pop	ar3
   0F90 D0 02              4836 	pop	ar2
                           4837 ;	main.c:811: if(tempvalue1>I2C_ADDR_MASK_BITS)
                           4838 ;	genCmpGt
                           4839 ;	genCmp
   0F92 C3                 4840 	clr	c
   0F93 74 FF              4841 	mov	a,#0xFF
   0F95 9C                 4842 	subb	a,r4
   0F96 74 07              4843 	mov	a,#0x07
   0F98 9D                 4844 	subb	a,r5
   0F99 E4                 4845 	clr	a
   0F9A 33                 4846 	rlc	a
                           4847 ;	genIfx
   0F9B FE                 4848 	mov	r6,a
                           4849 ;	Peephole 105	removed redundant mov
                           4850 ;	genIfxJump
                           4851 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0F9C 60 1D              4852 	jz	00109$
                           4853 ;	Peephole 300	removed redundant label 00136$
                           4854 ;	main.c:812: putstr("Enter the correct value\n\r");
                           4855 ;	genCall
                           4856 ;	Peephole 182.a	used 16 bit load of DPTR
   0F9E 90 28 26           4857 	mov	dptr,#__str_28
   0FA1 75 F0 80           4858 	mov	b,#0x80
   0FA4 C0 02              4859 	push	ar2
   0FA6 C0 03              4860 	push	ar3
   0FA8 C0 04              4861 	push	ar4
   0FAA C0 05              4862 	push	ar5
   0FAC C0 06              4863 	push	ar6
   0FAE 12 00 AA           4864 	lcall	_putstr
   0FB1 D0 06              4865 	pop	ar6
   0FB3 D0 05              4866 	pop	ar5
   0FB5 D0 04              4867 	pop	ar4
   0FB7 D0 03              4868 	pop	ar3
   0FB9 D0 02              4869 	pop	ar2
   0FBB                    4870 00109$:
                           4871 ;	main.c:813: }while(tempvalue1>I2C_ADDR_MASK_BITS);
                           4872 ;	genIfx
   0FBB EE                 4873 	mov	a,r6
                           4874 ;	genIfxJump
                           4875 ;	Peephole 108.b	removed ljmp by inverse jump logic
                           4876 ;	main.c:817: address &=I2C_BLOCKADDR_MASK_BITS;
                           4877 ;	genAnd
   0FBC 70 C5              4878 	jnz	00108$
                           4879 ;	Peephole 300	removed redundant label 00137$
                           4880 ;	Peephole 256.c	loading r6 with zero from a
   0FBE FE                 4881 	mov	r6,a
   0FBF 74 07              4882 	mov	a,#0x07
   0FC1 5B                 4883 	anl	a,r3
                           4884 ;	main.c:818: address >>=I2C_BLOCKADD_SHIFT_BITS;
                           4885 ;	genRightShift
                           4886 ;	genRightShiftLiteral
                           4887 ;	genrshTwo
   0FC2 FF                 4888 	mov	r7,a
                           4889 ;	Peephole 105	removed redundant mov
   0FC3 A2 E7              4890 	mov	c,acc.7
   0FC5 CE                 4891 	xch	a,r6
   0FC6 33                 4892 	rlc	a
   0FC7 CE                 4893 	xch	a,r6
   0FC8 33                 4894 	rlc	a
   0FC9 CE                 4895 	xch	a,r6
   0FCA 54 01              4896 	anl	a,#0x01
   0FCC FF                 4897 	mov	r7,a
                           4898 ;	main.c:819: temp |= address;
                           4899 ;	genOr
   0FCD 43 06 A0           4900 	orl	ar6,#0xA0
                           4901 ;	genCast
                           4902 ;	main.c:820: temp1=(temp&I2C_WRITE_MASK_BITS);
                           4903 ;	genAnd
   0FD0 74 FE              4904 	mov	a,#0xFE
   0FD2 5E                 4905 	anl	a,r6
   0FD3 FF                 4906 	mov	r7,a
                           4907 ;	main.c:821: tempaddr=tempvalue;
                           4908 ;	genCast
                           4909 ;	main.c:822: startbit();
                           4910 ;	genCall
   0FD4 C0 02              4911 	push	ar2
   0FD6 C0 04              4912 	push	ar4
   0FD8 C0 05              4913 	push	ar5
   0FDA C0 06              4914 	push	ar6
   0FDC C0 07              4915 	push	ar7
   0FDE 12 0C FB           4916 	lcall	_startbit
   0FE1 D0 07              4917 	pop	ar7
   0FE3 D0 06              4918 	pop	ar6
   0FE5 D0 05              4919 	pop	ar5
   0FE7 D0 04              4920 	pop	ar4
   0FE9 D0 02              4921 	pop	ar2
                           4922 ;	main.c:823: send_data(temp1);
                           4923 ;	genCall
   0FEB 8F 82              4924 	mov	dpl,r7
   0FED C0 02              4925 	push	ar2
   0FEF C0 04              4926 	push	ar4
   0FF1 C0 05              4927 	push	ar5
   0FF3 C0 06              4928 	push	ar6
   0FF5 12 0D 5C           4929 	lcall	_send_data
   0FF8 D0 06              4930 	pop	ar6
   0FFA D0 05              4931 	pop	ar5
   0FFC D0 04              4932 	pop	ar4
   0FFE D0 02              4933 	pop	ar2
                           4934 ;	main.c:824: send_data(tempaddr);
                           4935 ;	genCall
   1000 8A 82              4936 	mov	dpl,r2
   1002 C0 04              4937 	push	ar4
   1004 C0 05              4938 	push	ar5
   1006 C0 06              4939 	push	ar6
   1008 12 0D 5C           4940 	lcall	_send_data
   100B D0 06              4941 	pop	ar6
   100D D0 05              4942 	pop	ar5
   100F D0 04              4943 	pop	ar4
                           4944 ;	main.c:825: startbit();
                           4945 ;	genCall
   1011 C0 04              4946 	push	ar4
   1013 C0 05              4947 	push	ar5
   1015 C0 06              4948 	push	ar6
   1017 12 0C FB           4949 	lcall	_startbit
   101A D0 06              4950 	pop	ar6
   101C D0 05              4951 	pop	ar5
   101E D0 04              4952 	pop	ar4
                           4953 ;	main.c:826: temp=temp | I2C_READ_MASK_BITS;
                           4954 ;	genOr
   1020 43 06 01           4955 	orl	ar6,#0x01
                           4956 ;	main.c:827: send_data(temp);
                           4957 ;	genCall
   1023 8E 82              4958 	mov	dpl,r6
   1025 C0 04              4959 	push	ar4
   1027 C0 05              4960 	push	ar5
   1029 12 0D 5C           4961 	lcall	_send_data
   102C D0 05              4962 	pop	ar5
   102E D0 04              4963 	pop	ar4
                           4964 ;	main.c:828: putstr("\n\r");
                           4965 ;	genCall
                           4966 ;	Peephole 182.a	used 16 bit load of DPTR
   1030 90 25 9C           4967 	mov	dptr,#__str_3
   1033 75 F0 80           4968 	mov	b,#0x80
   1036 C0 04              4969 	push	ar4
   1038 C0 05              4970 	push	ar5
   103A 12 00 AA           4971 	lcall	_putstr
   103D D0 05              4972 	pop	ar5
   103F D0 04              4973 	pop	ar4
                           4974 ;	main.c:829: putstr("\n\r");
                           4975 ;	genCall
                           4976 ;	Peephole 182.a	used 16 bit load of DPTR
   1041 90 25 9C           4977 	mov	dptr,#__str_3
   1044 75 F0 80           4978 	mov	b,#0x80
   1047 C0 04              4979 	push	ar4
   1049 C0 05              4980 	push	ar5
   104B 12 00 AA           4981 	lcall	_putstr
   104E D0 05              4982 	pop	ar5
   1050 D0 04              4983 	pop	ar4
                           4984 ;	main.c:830: putstr("EEPROM data\n\r");
                           4985 ;	genCall
                           4986 ;	Peephole 182.a	used 16 bit load of DPTR
   1052 90 28 64           4987 	mov	dptr,#__str_30
   1055 75 F0 80           4988 	mov	b,#0x80
   1058 C0 04              4989 	push	ar4
   105A C0 05              4990 	push	ar5
   105C 12 00 AA           4991 	lcall	_putstr
   105F D0 05              4992 	pop	ar5
   1061 D0 04              4993 	pop	ar4
                           4994 ;	main.c:831: putstr("ADDR |  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r");
                           4995 ;	genCall
                           4996 ;	Peephole 182.a	used 16 bit load of DPTR
   1063 90 28 72           4997 	mov	dptr,#__str_31
   1066 75 F0 80           4998 	mov	b,#0x80
   1069 C0 04              4999 	push	ar4
   106B C0 05              5000 	push	ar5
   106D 12 00 AA           5001 	lcall	_putstr
   1070 D0 05              5002 	pop	ar5
   1072 D0 04              5003 	pop	ar4
                           5004 ;	main.c:832: putstr("\n\r");
                           5005 ;	genCall
                           5006 ;	Peephole 182.a	used 16 bit load of DPTR
   1074 90 25 9C           5007 	mov	dptr,#__str_3
   1077 75 F0 80           5008 	mov	b,#0x80
   107A C0 04              5009 	push	ar4
   107C C0 05              5010 	push	ar5
   107E 12 00 AA           5011 	lcall	_putstr
   1081 D0 05              5012 	pop	ar5
   1083 D0 04              5013 	pop	ar4
                           5014 ;	main.c:833: while(tempvalue<=tempvalue1)
   1085                    5015 00118$:
                           5016 ;	genAssign
   1085 90 00 45           5017 	mov	dptr,#_readall_function_tempvalue_1_1
   1088 E0                 5018 	movx	a,@dptr
   1089 FA                 5019 	mov	r2,a
   108A A3                 5020 	inc	dptr
   108B E0                 5021 	movx	a,@dptr
   108C FB                 5022 	mov	r3,a
                           5023 ;	genCmpGt
                           5024 ;	genCmp
   108D C3                 5025 	clr	c
   108E EC                 5026 	mov	a,r4
   108F 9A                 5027 	subb	a,r2
   1090 ED                 5028 	mov	a,r5
   1091 9B                 5029 	subb	a,r3
                           5030 ;	genIfxJump
   1092 50 03              5031 	jnc	00138$
   1094 02 11 6D           5032 	ljmp	00120$
   1097                    5033 00138$:
                           5034 ;	main.c:835: printf("0x%03X: ", tempvalue);
                           5035 ;	genIpush
   1097 C0 02              5036 	push	ar2
   1099 C0 03              5037 	push	ar3
   109B C0 04              5038 	push	ar4
   109D C0 05              5039 	push	ar5
   109F C0 02              5040 	push	ar2
   10A1 C0 03              5041 	push	ar3
                           5042 ;	genIpush
   10A3 74 BB              5043 	mov	a,#__str_32
   10A5 C0 E0              5044 	push	acc
   10A7 74 28              5045 	mov	a,#(__str_32 >> 8)
   10A9 C0 E0              5046 	push	acc
   10AB 74 80              5047 	mov	a,#0x80
   10AD C0 E0              5048 	push	acc
                           5049 ;	genCall
   10AF 12 1C 74           5050 	lcall	_printf
   10B2 E5 81              5051 	mov	a,sp
   10B4 24 FB              5052 	add	a,#0xfb
   10B6 F5 81              5053 	mov	sp,a
   10B8 D0 05              5054 	pop	ar5
   10BA D0 04              5055 	pop	ar4
   10BC D0 03              5056 	pop	ar3
   10BE D0 02              5057 	pop	ar2
                           5058 ;	main.c:836: for(i=0; i<=15; i++)
                           5059 ;	genAssign
                           5060 ;	genAssign
   10C0 7E 00              5061 	mov	r6,#0x00
   10C2                    5062 00114$:
                           5063 ;	genCmpGt
                           5064 ;	genCmp
                           5065 ;	genIfxJump
                           5066 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   10C2 EE                 5067 	mov	a,r6
   10C3 24 F0              5068 	add	a,#0xff - 0x0F
   10C5 50 03              5069 	jnc	00139$
   10C7 02 11 59           5070 	ljmp	00117$
   10CA                    5071 00139$:
                           5072 ;	main.c:838: clock_func();
                           5073 ;	genCall
   10CA C0 02              5074 	push	ar2
   10CC C0 03              5075 	push	ar3
   10CE C0 04              5076 	push	ar4
   10D0 C0 05              5077 	push	ar5
   10D2 C0 06              5078 	push	ar6
   10D4 12 0D 88           5079 	lcall	_clock_func
   10D7 D0 06              5080 	pop	ar6
   10D9 D0 05              5081 	pop	ar5
   10DB D0 04              5082 	pop	ar4
   10DD D0 03              5083 	pop	ar3
   10DF D0 02              5084 	pop	ar2
                           5085 ;	main.c:839: backup=ACC;
                           5086 ;	genAssign
                           5087 ;	main.c:840: printf("%02X  ", backup);
                           5088 ;	genAssign
   10E1 90 00 44           5089 	mov	dptr,#_readall_function_backup_1_1
   10E4 E5 E0              5090 	mov	a,_ACC
   10E6 F0                 5091 	movx	@dptr,a
                           5092 ;	Peephole 180.a	removed redundant mov to dptr
   10E7 E0                 5093 	movx	a,@dptr
   10E8 FF                 5094 	mov	r7,a
                           5095 ;	genCast
   10E9 78 00              5096 	mov	r0,#0x00
                           5097 ;	genIpush
   10EB C0 02              5098 	push	ar2
   10ED C0 03              5099 	push	ar3
   10EF C0 04              5100 	push	ar4
   10F1 C0 05              5101 	push	ar5
   10F3 C0 06              5102 	push	ar6
   10F5 C0 07              5103 	push	ar7
   10F7 C0 00              5104 	push	ar0
                           5105 ;	genIpush
   10F9 74 C4              5106 	mov	a,#__str_33
   10FB C0 E0              5107 	push	acc
   10FD 74 28              5108 	mov	a,#(__str_33 >> 8)
   10FF C0 E0              5109 	push	acc
   1101 74 80              5110 	mov	a,#0x80
   1103 C0 E0              5111 	push	acc
                           5112 ;	genCall
   1105 12 1C 74           5113 	lcall	_printf
   1108 E5 81              5114 	mov	a,sp
   110A 24 FB              5115 	add	a,#0xfb
   110C F5 81              5116 	mov	sp,a
   110E D0 06              5117 	pop	ar6
   1110 D0 05              5118 	pop	ar5
   1112 D0 04              5119 	pop	ar4
   1114 D0 03              5120 	pop	ar3
   1116 D0 02              5121 	pop	ar2
                           5122 ;	main.c:841: if(tempvalue!=tempvalue1)
                           5123 ;	genCmpEq
                           5124 ;	gencjneshort
   1118 EA                 5125 	mov	a,r2
   1119 B5 04 06           5126 	cjne	a,ar4,00140$
   111C EB                 5127 	mov	a,r3
   111D B5 05 02           5128 	cjne	a,ar5,00140$
                           5129 ;	Peephole 112.b	changed ljmp to sjmp
   1120 80 26              5130 	sjmp	00112$
   1122                    5131 00140$:
                           5132 ;	main.c:843: Master_ack();
                           5133 ;	genCall
   1122 C0 02              5134 	push	ar2
   1124 C0 03              5135 	push	ar3
   1126 C0 04              5136 	push	ar4
   1128 C0 05              5137 	push	ar5
   112A C0 06              5138 	push	ar6
   112C 12 0D 2D           5139 	lcall	_Master_ack
   112F D0 06              5140 	pop	ar6
   1131 D0 05              5141 	pop	ar5
   1133 D0 04              5142 	pop	ar4
   1135 D0 03              5143 	pop	ar3
   1137 D0 02              5144 	pop	ar2
                           5145 ;	main.c:844: tempvalue++;
                           5146 ;	genPlus
                           5147 ;     genPlusIncr
   1139 0A                 5148 	inc	r2
   113A BA 00 01           5149 	cjne	r2,#0x00,00141$
   113D 0B                 5150 	inc	r3
   113E                    5151 00141$:
                           5152 ;	genAssign
   113E 90 00 45           5153 	mov	dptr,#_readall_function_tempvalue_1_1
   1141 EA                 5154 	mov	a,r2
   1142 F0                 5155 	movx	@dptr,a
   1143 A3                 5156 	inc	dptr
   1144 EB                 5157 	mov	a,r3
   1145 F0                 5158 	movx	@dptr,a
                           5159 ;	Peephole 112.b	changed ljmp to sjmp
   1146 80 0D              5160 	sjmp	00116$
   1148                    5161 00112$:
                           5162 ;	main.c:848: tempvalue++;
                           5163 ;	genPlus
   1148 90 00 45           5164 	mov	dptr,#_readall_function_tempvalue_1_1
                           5165 ;     genPlusIncr
   114B 74 01              5166 	mov	a,#0x01
                           5167 ;	Peephole 236.a	used r2 instead of ar2
   114D 2A                 5168 	add	a,r2
   114E F0                 5169 	movx	@dptr,a
                           5170 ;	Peephole 181	changed mov to clr
   114F E4                 5171 	clr	a
                           5172 ;	Peephole 236.b	used r3 instead of ar3
   1150 3B                 5173 	addc	a,r3
   1151 A3                 5174 	inc	dptr
   1152 F0                 5175 	movx	@dptr,a
                           5176 ;	main.c:849: break;
                           5177 ;	Peephole 112.b	changed ljmp to sjmp
   1153 80 04              5178 	sjmp	00117$
   1155                    5179 00116$:
                           5180 ;	main.c:836: for(i=0; i<=15; i++)
                           5181 ;	genPlus
                           5182 ;     genPlusIncr
   1155 0E                 5183 	inc	r6
   1156 02 10 C2           5184 	ljmp	00114$
   1159                    5185 00117$:
                           5186 ;	main.c:853: putstr("\n\r");
                           5187 ;	genCall
                           5188 ;	Peephole 182.a	used 16 bit load of DPTR
   1159 90 25 9C           5189 	mov	dptr,#__str_3
   115C 75 F0 80           5190 	mov	b,#0x80
   115F C0 04              5191 	push	ar4
   1161 C0 05              5192 	push	ar5
   1163 12 00 AA           5193 	lcall	_putstr
   1166 D0 05              5194 	pop	ar5
   1168 D0 04              5195 	pop	ar4
   116A 02 10 85           5196 	ljmp	00118$
   116D                    5197 00120$:
                           5198 ;	main.c:855: stopbit();
                           5199 ;	genCall
                           5200 ;	Peephole 253.b	replaced lcall/ret with ljmp
   116D 02 0D 0A           5201 	ljmp	_stopbit
                           5202 ;
                           5203 ;------------------------------------------------------------
                           5204 ;Allocation info for local variables in function 'write_function'
                           5205 ;------------------------------------------------------------
                           5206 ;temp                      Allocated with name '_write_function_temp_1_1'
                           5207 ;temp1                     Allocated with name '_write_function_temp1_1_1'
                           5208 ;------------------------------------------------------------
                           5209 ;	main.c:860: void write_function()
                           5210 ;	-----------------------------------------
                           5211 ;	 function write_function
                           5212 ;	-----------------------------------------
   1170                    5213 _write_function:
                           5214 ;	main.c:863: putstr("Enter the address\n\r");
                           5215 ;	genCall
                           5216 ;	Peephole 182.a	used 16 bit load of DPTR
   1170 90 28 12           5217 	mov	dptr,#__str_27
   1173 75 F0 80           5218 	mov	b,#0x80
   1176 12 00 AA           5219 	lcall	_putstr
                           5220 ;	main.c:864: do
   1179                    5221 00103$:
                           5222 ;	main.c:866: temp = ReadVariable();
                           5223 ;	genCall
   1179 12 0B 32           5224 	lcall	_ReadVariable
   117C AA 82              5225 	mov	r2,dpl
   117E AB 83              5226 	mov	r3,dph
                           5227 ;	main.c:867: if(temp>I2C_ADDR_MASK_BITS)
                           5228 ;	genCmpGt
                           5229 ;	genCmp
   1180 C3                 5230 	clr	c
   1181 74 FF              5231 	mov	a,#0xFF
   1183 9A                 5232 	subb	a,r2
   1184 74 07              5233 	mov	a,#0x07
   1186 9B                 5234 	subb	a,r3
   1187 E4                 5235 	clr	a
   1188 33                 5236 	rlc	a
                           5237 ;	genIfx
   1189 FC                 5238 	mov	r4,a
                           5239 ;	Peephole 105	removed redundant mov
                           5240 ;	genIfxJump
                           5241 ;	Peephole 108.c	removed ljmp by inverse jump logic
   118A 60 15              5242 	jz	00104$
                           5243 ;	Peephole 300	removed redundant label 00119$
                           5244 ;	main.c:868: putstr("Enter the correct value\n\r");
                           5245 ;	genCall
                           5246 ;	Peephole 182.a	used 16 bit load of DPTR
   118C 90 28 26           5247 	mov	dptr,#__str_28
   118F 75 F0 80           5248 	mov	b,#0x80
   1192 C0 02              5249 	push	ar2
   1194 C0 03              5250 	push	ar3
   1196 C0 04              5251 	push	ar4
   1198 12 00 AA           5252 	lcall	_putstr
   119B D0 04              5253 	pop	ar4
   119D D0 03              5254 	pop	ar3
   119F D0 02              5255 	pop	ar2
   11A1                    5256 00104$:
                           5257 ;	main.c:869: }while(temp>I2C_ADDR_MASK_BITS);
                           5258 ;	genIfx
   11A1 EC                 5259 	mov	a,r4
                           5260 ;	genIfxJump
                           5261 ;	Peephole 108.b	removed ljmp by inverse jump logic
   11A2 70 D5              5262 	jnz	00103$
                           5263 ;	Peephole 300	removed redundant label 00120$
                           5264 ;	main.c:870: putstr("\n\rEnter the Value\n\r");
                           5265 ;	genCall
                           5266 ;	Peephole 182.a	used 16 bit load of DPTR
   11A4 90 28 CB           5267 	mov	dptr,#__str_34
   11A7 75 F0 80           5268 	mov	b,#0x80
   11AA C0 02              5269 	push	ar2
   11AC C0 03              5270 	push	ar3
   11AE 12 00 AA           5271 	lcall	_putstr
   11B1 D0 03              5272 	pop	ar3
   11B3 D0 02              5273 	pop	ar2
                           5274 ;	main.c:871: do
   11B5                    5275 00108$:
                           5276 ;	main.c:873: temp1=ReadVariable();
                           5277 ;	genCall
   11B5 C0 02              5278 	push	ar2
   11B7 C0 03              5279 	push	ar3
   11B9 12 0B 32           5280 	lcall	_ReadVariable
   11BC AC 82              5281 	mov	r4,dpl
   11BE AD 83              5282 	mov	r5,dph
   11C0 D0 03              5283 	pop	ar3
   11C2 D0 02              5284 	pop	ar2
                           5285 ;	main.c:874: if(temp1>I2C_ADDR_MASK_BITS)
                           5286 ;	genCmpGt
                           5287 ;	genCmp
   11C4 C3                 5288 	clr	c
   11C5 74 FF              5289 	mov	a,#0xFF
   11C7 9C                 5290 	subb	a,r4
   11C8 74 07              5291 	mov	a,#0x07
   11CA 9D                 5292 	subb	a,r5
   11CB E4                 5293 	clr	a
   11CC 33                 5294 	rlc	a
                           5295 ;	genIfx
   11CD FE                 5296 	mov	r6,a
                           5297 ;	Peephole 105	removed redundant mov
                           5298 ;	genIfxJump
                           5299 ;	Peephole 108.c	removed ljmp by inverse jump logic
   11CE 60 1D              5300 	jz	00109$
                           5301 ;	Peephole 300	removed redundant label 00121$
                           5302 ;	main.c:875: putstr("Enter the correct value\n\r");
                           5303 ;	genCall
                           5304 ;	Peephole 182.a	used 16 bit load of DPTR
   11D0 90 28 26           5305 	mov	dptr,#__str_28
   11D3 75 F0 80           5306 	mov	b,#0x80
   11D6 C0 02              5307 	push	ar2
   11D8 C0 03              5308 	push	ar3
   11DA C0 04              5309 	push	ar4
   11DC C0 05              5310 	push	ar5
   11DE C0 06              5311 	push	ar6
   11E0 12 00 AA           5312 	lcall	_putstr
   11E3 D0 06              5313 	pop	ar6
   11E5 D0 05              5314 	pop	ar5
   11E7 D0 04              5315 	pop	ar4
   11E9 D0 03              5316 	pop	ar3
   11EB D0 02              5317 	pop	ar2
   11ED                    5318 00109$:
                           5319 ;	main.c:876: }while(temp1>I2C_ADDR_MASK_BITS);
                           5320 ;	genIfx
   11ED EE                 5321 	mov	a,r6
                           5322 ;	genIfxJump
                           5323 ;	Peephole 108.b	removed ljmp by inverse jump logic
   11EE 70 C5              5324 	jnz	00108$
                           5325 ;	Peephole 300	removed redundant label 00122$
                           5326 ;	main.c:878: eebytew(temp, temp1);
                           5327 ;	genCast
   11F0 90 00 3E           5328 	mov	dptr,#_eebytew_PARM_2
   11F3 EC                 5329 	mov	a,r4
   11F4 F0                 5330 	movx	@dptr,a
                           5331 ;	genCall
   11F5 8A 82              5332 	mov	dpl,r2
   11F7 8B 83              5333 	mov	dph,r3
                           5334 ;	Peephole 253.b	replaced lcall/ret with ljmp
   11F9 02 0D A1           5335 	ljmp	_eebytew
                           5336 ;
                           5337 ;------------------------------------------------------------
                           5338 ;Allocation info for local variables in function 'help_menu'
                           5339 ;------------------------------------------------------------
                           5340 ;------------------------------------------------------------
                           5341 ;	main.c:882: void help_menu()
                           5342 ;	-----------------------------------------
                           5343 ;	 function help_menu
                           5344 ;	-----------------------------------------
   11FC                    5345 _help_menu:
                           5346 ;	main.c:884: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           5347 ;	genCall
                           5348 ;	Peephole 182.a	used 16 bit load of DPTR
   11FC 90 28 DF           5349 	mov	dptr,#__str_35
   11FF 75 F0 80           5350 	mov	b,#0x80
   1202 12 00 AA           5351 	lcall	_putstr
                           5352 ;	main.c:885: putstr("EEPROM Control\n\r");
                           5353 ;	genCall
                           5354 ;	Peephole 182.a	used 16 bit load of DPTR
   1205 90 29 3F           5355 	mov	dptr,#__str_36
   1208 75 F0 80           5356 	mov	b,#0x80
   120B 12 00 AA           5357 	lcall	_putstr
                           5358 ;	main.c:886: putstr("Press 'R' to read a byte from the user input address\n\r");
                           5359 ;	genCall
                           5360 ;	Peephole 182.a	used 16 bit load of DPTR
   120E 90 29 50           5361 	mov	dptr,#__str_37
   1211 75 F0 80           5362 	mov	b,#0x80
   1214 12 00 AA           5363 	lcall	_putstr
                           5364 ;	main.c:887: putstr("Press 'W' to write a byte data to user input address\n\r");
                           5365 ;	genCall
                           5366 ;	Peephole 182.a	used 16 bit load of DPTR
   1217 90 29 87           5367 	mov	dptr,#__str_38
   121A 75 F0 80           5368 	mov	b,#0x80
   121D 12 00 AA           5369 	lcall	_putstr
                           5370 ;	main.c:888: putstr("Press 'S' for sequential read between the adresses\n\r");
                           5371 ;	genCall
                           5372 ;	Peephole 182.a	used 16 bit load of DPTR
   1220 90 29 BE           5373 	mov	dptr,#__str_39
   1223 75 F0 80           5374 	mov	b,#0x80
   1226 12 00 AA           5375 	lcall	_putstr
                           5376 ;	main.c:889: putstr("Press 'T' for EEPROM software reset\n\r");
                           5377 ;	genCall
                           5378 ;	Peephole 182.a	used 16 bit load of DPTR
   1229 90 29 F3           5379 	mov	dptr,#__str_40
   122C 75 F0 80           5380 	mov	b,#0x80
   122F 12 00 AA           5381 	lcall	_putstr
                           5382 ;	main.c:890: putstr("LCD Control\n\r");
                           5383 ;	genCall
                           5384 ;	Peephole 182.a	used 16 bit load of DPTR
   1232 90 2A 19           5385 	mov	dptr,#__str_41
   1235 75 F0 80           5386 	mov	b,#0x80
   1238 12 00 AA           5387 	lcall	_putstr
                           5388 ;	main.c:891: putstr("Press 'C' for CGRAM HEX dump\n\r");
                           5389 ;	genCall
                           5390 ;	Peephole 182.a	used 16 bit load of DPTR
   123B 90 2A 27           5391 	mov	dptr,#__str_42
   123E 75 F0 80           5392 	mov	b,#0x80
   1241 12 00 AA           5393 	lcall	_putstr
                           5394 ;	main.c:892: putstr("Press 'D' for DDRAM HEX dump\n\r");
                           5395 ;	genCall
                           5396 ;	Peephole 182.a	used 16 bit load of DPTR
   1244 90 2A 46           5397 	mov	dptr,#__str_43
   1247 75 F0 80           5398 	mov	b,#0x80
   124A 12 00 AA           5399 	lcall	_putstr
                           5400 ;	main.c:893: putstr("Press 'E' for Clearing the LCD\n\r");
                           5401 ;	genCall
                           5402 ;	Peephole 182.a	used 16 bit load of DPTR
   124D 90 2A 65           5403 	mov	dptr,#__str_44
   1250 75 F0 80           5404 	mov	b,#0x80
   1253 12 00 AA           5405 	lcall	_putstr
                           5406 ;	main.c:894: putstr("Press 'P' to display the custom character\n\r");
                           5407 ;	genCall
                           5408 ;	Peephole 182.a	used 16 bit load of DPTR
   1256 90 2A 86           5409 	mov	dptr,#__str_45
   1259 75 F0 80           5410 	mov	b,#0x80
   125C 12 00 AA           5411 	lcall	_putstr
                           5412 ;	main.c:895: putstr("Press 'N' to add custom character\n\r");
                           5413 ;	genCall
                           5414 ;	Peephole 182.a	used 16 bit load of DPTR
   125F 90 2A B2           5415 	mov	dptr,#__str_46
   1262 75 F0 80           5416 	mov	b,#0x80
   1265 12 00 AA           5417 	lcall	_putstr
                           5418 ;	main.c:896: putstr("Press 'I' for printing letters on LCD\n\r");
                           5419 ;	genCall
                           5420 ;	Peephole 182.a	used 16 bit load of DPTR
   1268 90 2A D6           5421 	mov	dptr,#__str_47
   126B 75 F0 80           5422 	mov	b,#0x80
   126E 12 00 AA           5423 	lcall	_putstr
                           5424 ;	main.c:897: putstr("Press 'K' for displaying the logo\n\r");
                           5425 ;	genCall
                           5426 ;	Peephole 182.a	used 16 bit load of DPTR
   1271 90 2A FE           5427 	mov	dptr,#__str_48
   1274 75 F0 80           5428 	mov	b,#0x80
   1277 12 00 AA           5429 	lcall	_putstr
                           5430 ;	main.c:898: putstr("Press 'X' for moving the cursor to user input x,y position\n\r");
                           5431 ;	genCall
                           5432 ;	Peephole 182.a	used 16 bit load of DPTR
   127A 90 2B 22           5433 	mov	dptr,#__str_49
   127D 75 F0 80           5434 	mov	b,#0x80
   1280 12 00 AA           5435 	lcall	_putstr
                           5436 ;	main.c:899: putstr("Press 'G' for moving the cursor to user input address\n\r");
                           5437 ;	genCall
                           5438 ;	Peephole 182.a	used 16 bit load of DPTR
   1283 90 2B 5F           5439 	mov	dptr,#__str_50
   1286 75 F0 80           5440 	mov	b,#0x80
   1289 12 00 AA           5441 	lcall	_putstr
                           5442 ;	main.c:900: putstr("Clock Control\n\r");
                           5443 ;	genCall
                           5444 ;	Peephole 182.a	used 16 bit load of DPTR
   128C 90 2B 97           5445 	mov	dptr,#__str_51
   128F 75 F0 80           5446 	mov	b,#0x80
   1292 12 00 AA           5447 	lcall	_putstr
                           5448 ;	main.c:901: putstr("Press '0' for pausing the clock\n\r");
                           5449 ;	genCall
                           5450 ;	Peephole 182.a	used 16 bit load of DPTR
   1295 90 2B A7           5451 	mov	dptr,#__str_52
   1298 75 F0 80           5452 	mov	b,#0x80
   129B 12 00 AA           5453 	lcall	_putstr
                           5454 ;	main.c:902: putstr("Press '1' to resume the clock \n\r");
                           5455 ;	genCall
                           5456 ;	Peephole 182.a	used 16 bit load of DPTR
   129E 90 2B C9           5457 	mov	dptr,#__str_53
   12A1 75 F0 80           5458 	mov	b,#0x80
   12A4 12 00 AA           5459 	lcall	_putstr
                           5460 ;	main.c:903: putstr("Press '2' to reset the clock to zero\n\r");
                           5461 ;	genCall
                           5462 ;	Peephole 182.a	used 16 bit load of DPTR
   12A7 90 2B EA           5463 	mov	dptr,#__str_54
   12AA 75 F0 80           5464 	mov	b,#0x80
   12AD 12 00 AA           5465 	lcall	_putstr
                           5466 ;	main.c:904: putstr("IO Expander control\n\r");
                           5467 ;	genCall
                           5468 ;	Peephole 182.a	used 16 bit load of DPTR
   12B0 90 2C 11           5469 	mov	dptr,#__str_55
   12B3 75 F0 80           5470 	mov	b,#0x80
   12B6 12 00 AA           5471 	lcall	_putstr
                           5472 ;	main.c:905: putstr("Press 'B' for writing data to IO Expander\n\r");
                           5473 ;	genCall
                           5474 ;	Peephole 182.a	used 16 bit load of DPTR
   12B9 90 2C 27           5475 	mov	dptr,#__str_56
   12BC 75 F0 80           5476 	mov	b,#0x80
   12BF 12 00 AA           5477 	lcall	_putstr
                           5478 ;	main.c:906: putstr("Press 'F' to read data from IO Expander\n\r");
                           5479 ;	genCall
                           5480 ;	Peephole 182.a	used 16 bit load of DPTR
   12C2 90 2C 53           5481 	mov	dptr,#__str_57
   12C5 75 F0 80           5482 	mov	b,#0x80
   12C8 12 00 AA           5483 	lcall	_putstr
                           5484 ;	main.c:907: putstr("Press 'H' for configuring the individual GPIO pin\n\r");
                           5485 ;	genCall
                           5486 ;	Peephole 182.a	used 16 bit load of DPTR
   12CB 90 2C 7D           5487 	mov	dptr,#__str_58
   12CE 75 F0 80           5488 	mov	b,#0x80
   12D1 12 00 AA           5489 	lcall	_putstr
                           5490 ;	main.c:908: putstr("Press 'J' for configuring IO expander's one pin as input and one as output\n\r");
                           5491 ;	genCall
                           5492 ;	Peephole 182.a	used 16 bit load of DPTR
   12D4 90 2C B1           5493 	mov	dptr,#__str_59
   12D7 75 F0 80           5494 	mov	b,#0x80
   12DA 12 00 AA           5495 	lcall	_putstr
                           5496 ;	main.c:909: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           5497 ;	genCall
                           5498 ;	Peephole 182.a	used 16 bit load of DPTR
   12DD 90 28 DF           5499 	mov	dptr,#__str_35
   12E0 75 F0 80           5500 	mov	b,#0x80
                           5501 ;	Peephole 253.b	replaced lcall/ret with ljmp
   12E3 02 00 AA           5502 	ljmp	_putstr
                           5503 ;
                           5504 ;------------------------------------------------------------
                           5505 ;Allocation info for local variables in function 'read_function'
                           5506 ;------------------------------------------------------------
                           5507 ;temp1                     Allocated with name '_read_function_temp1_1_1'
                           5508 ;------------------------------------------------------------
                           5509 ;	main.c:912: void read_function()
                           5510 ;	-----------------------------------------
                           5511 ;	 function read_function
                           5512 ;	-----------------------------------------
   12E6                    5513 _read_function:
                           5514 ;	main.c:915: putstr("Enter the address in hex\n\r");
                           5515 ;	genCall
                           5516 ;	Peephole 182.a	used 16 bit load of DPTR
   12E6 90 27 A1           5517 	mov	dptr,#__str_23
   12E9 75 F0 80           5518 	mov	b,#0x80
   12EC 12 00 AA           5519 	lcall	_putstr
                           5520 ;	main.c:916: do
   12EF                    5521 00103$:
                           5522 ;	main.c:918: temp1=ReadVariable();
                           5523 ;	genCall
   12EF 12 0B 32           5524 	lcall	_ReadVariable
   12F2 AA 82              5525 	mov	r2,dpl
   12F4 AB 83              5526 	mov	r3,dph
                           5527 ;	main.c:919: if(temp1>I2C_ADDR_MASK_BITS)
                           5528 ;	genCmpGt
                           5529 ;	genCmp
   12F6 C3                 5530 	clr	c
   12F7 74 FF              5531 	mov	a,#0xFF
   12F9 9A                 5532 	subb	a,r2
   12FA 74 07              5533 	mov	a,#0x07
   12FC 9B                 5534 	subb	a,r3
   12FD E4                 5535 	clr	a
   12FE 33                 5536 	rlc	a
                           5537 ;	genIfx
   12FF FC                 5538 	mov	r4,a
                           5539 ;	Peephole 105	removed redundant mov
                           5540 ;	genIfxJump
                           5541 ;	Peephole 108.c	removed ljmp by inverse jump logic
   1300 60 15              5542 	jz	00104$
                           5543 ;	Peephole 300	removed redundant label 00111$
                           5544 ;	main.c:920: putstr("Enter the correct value\n\r");
                           5545 ;	genCall
                           5546 ;	Peephole 182.a	used 16 bit load of DPTR
   1302 90 28 26           5547 	mov	dptr,#__str_28
   1305 75 F0 80           5548 	mov	b,#0x80
   1308 C0 02              5549 	push	ar2
   130A C0 03              5550 	push	ar3
   130C C0 04              5551 	push	ar4
   130E 12 00 AA           5552 	lcall	_putstr
   1311 D0 04              5553 	pop	ar4
   1313 D0 03              5554 	pop	ar3
   1315 D0 02              5555 	pop	ar2
   1317                    5556 00104$:
                           5557 ;	main.c:921: }while(temp1>I2C_ADDR_MASK_BITS);
                           5558 ;	genIfx
   1317 EC                 5559 	mov	a,r4
                           5560 ;	genIfxJump
                           5561 ;	Peephole 108.b	removed ljmp by inverse jump logic
   1318 70 D5              5562 	jnz	00103$
                           5563 ;	Peephole 300	removed redundant label 00112$
                           5564 ;	main.c:922: eebyter(temp1);
                           5565 ;	genCall
   131A 8A 82              5566 	mov	dpl,r2
   131C 8B 83              5567 	mov	dph,r3
                           5568 ;	Peephole 253.b	replaced lcall/ret with ljmp
   131E 02 0E 6D           5569 	ljmp	_eebyter
                           5570 ;
                           5571 ;------------------------------------------------------------
                           5572 ;Allocation info for local variables in function 'custom_character'
                           5573 ;------------------------------------------------------------
                           5574 ;in_array                  Allocated with name '_custom_character_in_array_1_1'
                           5575 ;in_array5                 Allocated with name '_custom_character_in_array5_1_1'
                           5576 ;in_array1                 Allocated with name '_custom_character_in_array1_1_1'
                           5577 ;in_array2                 Allocated with name '_custom_character_in_array2_1_1'
                           5578 ;in_array3                 Allocated with name '_custom_character_in_array3_1_1'
                           5579 ;in_array4                 Allocated with name '_custom_character_in_array4_1_1'
                           5580 ;in_array6                 Allocated with name '_custom_character_in_array6_1_1'
                           5581 ;in_array7                 Allocated with name '_custom_character_in_array7_1_1'
                           5582 ;kk                        Allocated with name '_custom_character_kk_1_1'
                           5583 ;------------------------------------------------------------
                           5584 ;	main.c:925: void custom_character()
                           5585 ;	-----------------------------------------
                           5586 ;	 function custom_character
                           5587 ;	-----------------------------------------
   1321                    5588 _custom_character:
                           5589 ;	main.c:927: unsigned char in_array[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c};
                           5590 ;	genPointerSet
                           5591 ;     genFarPointerSet
   1321 90 00 47           5592 	mov	dptr,#_custom_character_in_array_1_1
   1324 74 0C              5593 	mov	a,#0x0C
   1326 F0                 5594 	movx	@dptr,a
                           5595 ;	genPointerSet
                           5596 ;     genFarPointerSet
   1327 90 00 48           5597 	mov	dptr,#(_custom_character_in_array_1_1 + 0x0001)
   132A 74 0C              5598 	mov	a,#0x0C
   132C F0                 5599 	movx	@dptr,a
                           5600 ;	genPointerSet
                           5601 ;     genFarPointerSet
   132D 90 00 49           5602 	mov	dptr,#(_custom_character_in_array_1_1 + 0x0002)
   1330 74 0C              5603 	mov	a,#0x0C
   1332 F0                 5604 	movx	@dptr,a
                           5605 ;	genPointerSet
                           5606 ;     genFarPointerSet
   1333 90 00 4A           5607 	mov	dptr,#(_custom_character_in_array_1_1 + 0x0003)
   1336 74 0C              5608 	mov	a,#0x0C
   1338 F0                 5609 	movx	@dptr,a
                           5610 ;	genPointerSet
                           5611 ;     genFarPointerSet
   1339 90 00 4B           5612 	mov	dptr,#(_custom_character_in_array_1_1 + 0x0004)
   133C 74 0C              5613 	mov	a,#0x0C
   133E F0                 5614 	movx	@dptr,a
                           5615 ;	genPointerSet
                           5616 ;     genFarPointerSet
   133F 90 00 4C           5617 	mov	dptr,#(_custom_character_in_array_1_1 + 0x0005)
   1342 74 0C              5618 	mov	a,#0x0C
   1344 F0                 5619 	movx	@dptr,a
                           5620 ;	genPointerSet
                           5621 ;     genFarPointerSet
   1345 90 00 4D           5622 	mov	dptr,#(_custom_character_in_array_1_1 + 0x0006)
   1348 74 0C              5623 	mov	a,#0x0C
   134A F0                 5624 	movx	@dptr,a
                           5625 ;	genPointerSet
                           5626 ;     genFarPointerSet
   134B 90 00 4E           5627 	mov	dptr,#(_custom_character_in_array_1_1 + 0x0007)
   134E 74 0C              5628 	mov	a,#0x0C
   1350 F0                 5629 	movx	@dptr,a
                           5630 ;	main.c:928: unsigned char in_array5[] = {0x0f, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0f};
                           5631 ;	genPointerSet
                           5632 ;     genFarPointerSet
   1351 90 00 4F           5633 	mov	dptr,#_custom_character_in_array5_1_1
   1354 74 0F              5634 	mov	a,#0x0F
   1356 F0                 5635 	movx	@dptr,a
                           5636 ;	genPointerSet
                           5637 ;     genFarPointerSet
   1357 90 00 50           5638 	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0001)
   135A 74 0C              5639 	mov	a,#0x0C
   135C F0                 5640 	movx	@dptr,a
                           5641 ;	genPointerSet
                           5642 ;     genFarPointerSet
   135D 90 00 51           5643 	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0002)
   1360 74 0C              5644 	mov	a,#0x0C
   1362 F0                 5645 	movx	@dptr,a
                           5646 ;	genPointerSet
                           5647 ;     genFarPointerSet
   1363 90 00 52           5648 	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0003)
   1366 74 0C              5649 	mov	a,#0x0C
   1368 F0                 5650 	movx	@dptr,a
                           5651 ;	genPointerSet
                           5652 ;     genFarPointerSet
   1369 90 00 53           5653 	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0004)
   136C 74 0C              5654 	mov	a,#0x0C
   136E F0                 5655 	movx	@dptr,a
                           5656 ;	genPointerSet
                           5657 ;     genFarPointerSet
   136F 90 00 54           5658 	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0005)
   1372 74 0C              5659 	mov	a,#0x0C
   1374 F0                 5660 	movx	@dptr,a
                           5661 ;	genPointerSet
                           5662 ;     genFarPointerSet
   1375 90 00 55           5663 	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0006)
   1378 74 0C              5664 	mov	a,#0x0C
   137A F0                 5665 	movx	@dptr,a
                           5666 ;	genPointerSet
                           5667 ;     genFarPointerSet
   137B 90 00 56           5668 	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0007)
   137E 74 0F              5669 	mov	a,#0x0F
   1380 F0                 5670 	movx	@dptr,a
                           5671 ;	main.c:929: unsigned char in_array1[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x1f, 0x1f, 0x1f};
                           5672 ;	genPointerSet
                           5673 ;     genFarPointerSet
   1381 90 00 57           5674 	mov	dptr,#_custom_character_in_array1_1_1
   1384 74 0C              5675 	mov	a,#0x0C
   1386 F0                 5676 	movx	@dptr,a
                           5677 ;	genPointerSet
                           5678 ;     genFarPointerSet
   1387 90 00 58           5679 	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0001)
   138A 74 0C              5680 	mov	a,#0x0C
   138C F0                 5681 	movx	@dptr,a
                           5682 ;	genPointerSet
                           5683 ;     genFarPointerSet
   138D 90 00 59           5684 	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0002)
   1390 74 0C              5685 	mov	a,#0x0C
   1392 F0                 5686 	movx	@dptr,a
                           5687 ;	genPointerSet
                           5688 ;     genFarPointerSet
   1393 90 00 5A           5689 	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0003)
   1396 74 0C              5690 	mov	a,#0x0C
   1398 F0                 5691 	movx	@dptr,a
                           5692 ;	genPointerSet
                           5693 ;     genFarPointerSet
   1399 90 00 5B           5694 	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0004)
   139C 74 0C              5695 	mov	a,#0x0C
   139E F0                 5696 	movx	@dptr,a
                           5697 ;	genPointerSet
                           5698 ;     genFarPointerSet
   139F 90 00 5C           5699 	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0005)
   13A2 74 1F              5700 	mov	a,#0x1F
   13A4 F0                 5701 	movx	@dptr,a
                           5702 ;	genPointerSet
                           5703 ;     genFarPointerSet
   13A5 90 00 5D           5704 	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0006)
   13A8 74 1F              5705 	mov	a,#0x1F
   13AA F0                 5706 	movx	@dptr,a
                           5707 ;	genPointerSet
                           5708 ;     genFarPointerSet
   13AB 90 00 5E           5709 	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0007)
   13AE 74 1F              5710 	mov	a,#0x1F
   13B0 F0                 5711 	movx	@dptr,a
                           5712 ;	main.c:930: unsigned char in_array2[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x03};
                           5713 ;	genPointerSet
                           5714 ;     genFarPointerSet
   13B1 90 00 5F           5715 	mov	dptr,#_custom_character_in_array2_1_1
                           5716 ;	Peephole 181	changed mov to clr
                           5717 ;	genPointerSet
                           5718 ;     genFarPointerSet
                           5719 ;	Peephole 181	changed mov to clr
                           5720 ;	Peephole 219.a	removed redundant clear
                           5721 ;	genPointerSet
                           5722 ;     genFarPointerSet
                           5723 ;	Peephole 181	changed mov to clr
                           5724 ;	genPointerSet
                           5725 ;     genFarPointerSet
                           5726 ;	Peephole 181	changed mov to clr
                           5727 ;	Peephole 219.a	removed redundant clear
   13B4 E4                 5728 	clr	a
   13B5 F0                 5729 	movx	@dptr,a
   13B6 90 00 60           5730 	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0001)
   13B9 F0                 5731 	movx	@dptr,a
   13BA 90 00 61           5732 	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0002)
                           5733 ;	Peephole 219.b	removed redundant clear
   13BD F0                 5734 	movx	@dptr,a
   13BE 90 00 62           5735 	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0003)
   13C1 F0                 5736 	movx	@dptr,a
                           5737 ;	genPointerSet
                           5738 ;     genFarPointerSet
   13C2 90 00 63           5739 	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0004)
                           5740 ;	Peephole 181	changed mov to clr
   13C5 E4                 5741 	clr	a
   13C6 F0                 5742 	movx	@dptr,a
                           5743 ;	genPointerSet
                           5744 ;     genFarPointerSet
   13C7 90 00 64           5745 	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0005)
   13CA 74 03              5746 	mov	a,#0x03
   13CC F0                 5747 	movx	@dptr,a
                           5748 ;	genPointerSet
                           5749 ;     genFarPointerSet
   13CD 90 00 65           5750 	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0006)
   13D0 74 03              5751 	mov	a,#0x03
   13D2 F0                 5752 	movx	@dptr,a
                           5753 ;	genPointerSet
                           5754 ;     genFarPointerSet
   13D3 90 00 66           5755 	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0007)
   13D6 74 03              5756 	mov	a,#0x03
   13D8 F0                 5757 	movx	@dptr,a
                           5758 ;	main.c:931: unsigned char in_array3[] = {0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f};
                           5759 ;	genPointerSet
                           5760 ;     genFarPointerSet
   13D9 90 00 67           5761 	mov	dptr,#_custom_character_in_array3_1_1
   13DC 74 1F              5762 	mov	a,#0x1F
   13DE F0                 5763 	movx	@dptr,a
                           5764 ;	genPointerSet
                           5765 ;     genFarPointerSet
   13DF 90 00 68           5766 	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0001)
                           5767 ;	Peephole 181	changed mov to clr
                           5768 ;	genPointerSet
                           5769 ;     genFarPointerSet
                           5770 ;	Peephole 181	changed mov to clr
                           5771 ;	Peephole 219.a	removed redundant clear
                           5772 ;	genPointerSet
                           5773 ;     genFarPointerSet
                           5774 ;	Peephole 181	changed mov to clr
                           5775 ;	genPointerSet
                           5776 ;     genFarPointerSet
                           5777 ;	Peephole 181	changed mov to clr
                           5778 ;	Peephole 219.a	removed redundant clear
   13E2 E4                 5779 	clr	a
   13E3 F0                 5780 	movx	@dptr,a
   13E4 90 00 69           5781 	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0002)
   13E7 F0                 5782 	movx	@dptr,a
   13E8 90 00 6A           5783 	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0003)
                           5784 ;	Peephole 219.b	removed redundant clear
   13EB F0                 5785 	movx	@dptr,a
   13EC 90 00 6B           5786 	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0004)
   13EF F0                 5787 	movx	@dptr,a
                           5788 ;	genPointerSet
                           5789 ;     genFarPointerSet
   13F0 90 00 6C           5790 	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0005)
                           5791 ;	Peephole 181	changed mov to clr
                           5792 ;	genPointerSet
                           5793 ;     genFarPointerSet
                           5794 ;	Peephole 181	changed mov to clr
                           5795 ;	Peephole 219.a	removed redundant clear
   13F3 E4                 5796 	clr	a
   13F4 F0                 5797 	movx	@dptr,a
   13F5 90 00 6D           5798 	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0006)
   13F8 F0                 5799 	movx	@dptr,a
                           5800 ;	genPointerSet
                           5801 ;     genFarPointerSet
   13F9 90 00 6E           5802 	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0007)
   13FC 74 1F              5803 	mov	a,#0x1F
   13FE F0                 5804 	movx	@dptr,a
                           5805 ;	main.c:932: unsigned char in_array4[] = {0x1f, 0x00, 0x04, 0x0A, 0x0A, 0x04, 0x00, 0x1f};
                           5806 ;	genPointerSet
                           5807 ;     genFarPointerSet
   13FF 90 00 6F           5808 	mov	dptr,#_custom_character_in_array4_1_1
   1402 74 1F              5809 	mov	a,#0x1F
   1404 F0                 5810 	movx	@dptr,a
                           5811 ;	genPointerSet
                           5812 ;     genFarPointerSet
   1405 90 00 70           5813 	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0001)
                           5814 ;	Peephole 181	changed mov to clr
   1408 E4                 5815 	clr	a
   1409 F0                 5816 	movx	@dptr,a
                           5817 ;	genPointerSet
                           5818 ;     genFarPointerSet
   140A 90 00 71           5819 	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0002)
   140D 74 04              5820 	mov	a,#0x04
   140F F0                 5821 	movx	@dptr,a
                           5822 ;	genPointerSet
                           5823 ;     genFarPointerSet
   1410 90 00 72           5824 	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0003)
   1413 74 0A              5825 	mov	a,#0x0A
   1415 F0                 5826 	movx	@dptr,a
                           5827 ;	genPointerSet
                           5828 ;     genFarPointerSet
   1416 90 00 73           5829 	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0004)
   1419 74 0A              5830 	mov	a,#0x0A
   141B F0                 5831 	movx	@dptr,a
                           5832 ;	genPointerSet
                           5833 ;     genFarPointerSet
   141C 90 00 74           5834 	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0005)
   141F 74 04              5835 	mov	a,#0x04
   1421 F0                 5836 	movx	@dptr,a
                           5837 ;	genPointerSet
                           5838 ;     genFarPointerSet
   1422 90 00 75           5839 	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0006)
                           5840 ;	Peephole 181	changed mov to clr
   1425 E4                 5841 	clr	a
   1426 F0                 5842 	movx	@dptr,a
                           5843 ;	genPointerSet
                           5844 ;     genFarPointerSet
   1427 90 00 76           5845 	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0007)
   142A 74 1F              5846 	mov	a,#0x1F
   142C F0                 5847 	movx	@dptr,a
                           5848 ;	main.c:933: unsigned char in_array6[] = {0x1f, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x1f};
                           5849 ;	genPointerSet
                           5850 ;     genFarPointerSet
   142D 90 00 77           5851 	mov	dptr,#_custom_character_in_array6_1_1
   1430 74 1F              5852 	mov	a,#0x1F
   1432 F0                 5853 	movx	@dptr,a
                           5854 ;	genPointerSet
                           5855 ;     genFarPointerSet
   1433 90 00 78           5856 	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0001)
   1436 74 01              5857 	mov	a,#0x01
   1438 F0                 5858 	movx	@dptr,a
                           5859 ;	genPointerSet
                           5860 ;     genFarPointerSet
   1439 90 00 79           5861 	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0002)
   143C 74 01              5862 	mov	a,#0x01
   143E F0                 5863 	movx	@dptr,a
                           5864 ;	genPointerSet
                           5865 ;     genFarPointerSet
   143F 90 00 7A           5866 	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0003)
   1442 74 01              5867 	mov	a,#0x01
   1444 F0                 5868 	movx	@dptr,a
                           5869 ;	genPointerSet
                           5870 ;     genFarPointerSet
   1445 90 00 7B           5871 	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0004)
   1448 74 01              5872 	mov	a,#0x01
   144A F0                 5873 	movx	@dptr,a
                           5874 ;	genPointerSet
                           5875 ;     genFarPointerSet
   144B 90 00 7C           5876 	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0005)
   144E 74 01              5877 	mov	a,#0x01
   1450 F0                 5878 	movx	@dptr,a
                           5879 ;	genPointerSet
                           5880 ;     genFarPointerSet
   1451 90 00 7D           5881 	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0006)
   1454 74 01              5882 	mov	a,#0x01
   1456 F0                 5883 	movx	@dptr,a
                           5884 ;	genPointerSet
                           5885 ;     genFarPointerSet
   1457 90 00 7E           5886 	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0007)
   145A 74 1F              5887 	mov	a,#0x1F
   145C F0                 5888 	movx	@dptr,a
                           5889 ;	main.c:934: unsigned char in_array7[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x018, 0x18};
                           5890 ;	genPointerSet
                           5891 ;     genFarPointerSet
   145D 90 00 7F           5892 	mov	dptr,#_custom_character_in_array7_1_1
                           5893 ;	Peephole 181	changed mov to clr
                           5894 ;	genPointerSet
                           5895 ;     genFarPointerSet
                           5896 ;	Peephole 181	changed mov to clr
                           5897 ;	Peephole 219.a	removed redundant clear
                           5898 ;	genPointerSet
                           5899 ;     genFarPointerSet
                           5900 ;	Peephole 181	changed mov to clr
                           5901 ;	genPointerSet
                           5902 ;     genFarPointerSet
                           5903 ;	Peephole 181	changed mov to clr
                           5904 ;	Peephole 219.a	removed redundant clear
   1460 E4                 5905 	clr	a
   1461 F0                 5906 	movx	@dptr,a
   1462 90 00 80           5907 	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0001)
   1465 F0                 5908 	movx	@dptr,a
   1466 90 00 81           5909 	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0002)
                           5910 ;	Peephole 219.b	removed redundant clear
   1469 F0                 5911 	movx	@dptr,a
   146A 90 00 82           5912 	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0003)
   146D F0                 5913 	movx	@dptr,a
                           5914 ;	genPointerSet
                           5915 ;     genFarPointerSet
   146E 90 00 83           5916 	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0004)
                           5917 ;	Peephole 181	changed mov to clr
   1471 E4                 5918 	clr	a
   1472 F0                 5919 	movx	@dptr,a
                           5920 ;	genPointerSet
                           5921 ;     genFarPointerSet
   1473 90 00 84           5922 	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0005)
   1476 74 18              5923 	mov	a,#0x18
   1478 F0                 5924 	movx	@dptr,a
                           5925 ;	genPointerSet
                           5926 ;     genFarPointerSet
   1479 90 00 85           5927 	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0006)
   147C 74 18              5928 	mov	a,#0x18
   147E F0                 5929 	movx	@dptr,a
                           5930 ;	genPointerSet
                           5931 ;     genFarPointerSet
   147F 90 00 86           5932 	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0007)
   1482 74 18              5933 	mov	a,#0x18
   1484 F0                 5934 	movx	@dptr,a
                           5935 ;	main.c:936: EA=0;
                           5936 ;	genAssign
   1485 C2 AF              5937 	clr	_EA
                           5938 ;	main.c:937: kk=*INST_READ;
                           5939 ;	genPointerGet
                           5940 ;	genFarPointerGet
                           5941 ;	Peephole 182.b	used 16 bit load of dptr
   1487 90 F8 00           5942 	mov	dptr,#0xF800
   148A E0                 5943 	movx	a,@dptr
   148B FA                 5944 	mov	r2,a
                           5945 ;	main.c:938: EA=1;
                           5946 ;	genAssign
   148C D2 AF              5947 	setb	_EA
                           5948 ;	main.c:939: kk &= CURSORADDR_MASK_BITS;
                           5949 ;	genAnd
   148E 53 02 7F           5950 	anl	ar2,#0x7F
                           5951 ;	main.c:940: lcdclear();
                           5952 ;	genCall
   1491 C0 02              5953 	push	ar2
   1493 12 02 67           5954 	lcall	_lcdclear
   1496 D0 02              5955 	pop	ar2
                           5956 ;	main.c:941: putstr("Look at LCD\n\r");
                           5957 ;	genCall
                           5958 ;	Peephole 182.a	used 16 bit load of DPTR
   1498 90 2C FE           5959 	mov	dptr,#__str_60
   149B 75 F0 80           5960 	mov	b,#0x80
   149E C0 02              5961 	push	ar2
   14A0 12 00 AA           5962 	lcall	_putstr
   14A3 D0 02              5963 	pop	ar2
                           5964 ;	main.c:942: lcdcreatechar(CGRAM_FIRSTCHAR_ADDR, in_array);
                           5965 ;	genCast
   14A5 90 00 28           5966 	mov	dptr,#_lcdcreatechar_PARM_2
   14A8 74 47              5967 	mov	a,#_custom_character_in_array_1_1
   14AA F0                 5968 	movx	@dptr,a
   14AB A3                 5969 	inc	dptr
   14AC 74 00              5970 	mov	a,#(_custom_character_in_array_1_1 >> 8)
   14AE F0                 5971 	movx	@dptr,a
   14AF A3                 5972 	inc	dptr
   14B0 74 00              5973 	mov	a,#0x0
   14B2 F0                 5974 	movx	@dptr,a
                           5975 ;	genCall
   14B3 75 82 00           5976 	mov	dpl,#0x00
   14B6 C0 02              5977 	push	ar2
   14B8 12 05 16           5978 	lcall	_lcdcreatechar
   14BB D0 02              5979 	pop	ar2
                           5980 ;	main.c:943: lcdcreatechar(CGRAM_SECONDCHAR_ADDR, in_array1);
                           5981 ;	genCast
   14BD 90 00 28           5982 	mov	dptr,#_lcdcreatechar_PARM_2
   14C0 74 57              5983 	mov	a,#_custom_character_in_array1_1_1
   14C2 F0                 5984 	movx	@dptr,a
   14C3 A3                 5985 	inc	dptr
   14C4 74 00              5986 	mov	a,#(_custom_character_in_array1_1_1 >> 8)
   14C6 F0                 5987 	movx	@dptr,a
   14C7 A3                 5988 	inc	dptr
   14C8 74 00              5989 	mov	a,#0x0
   14CA F0                 5990 	movx	@dptr,a
                           5991 ;	genCall
   14CB 75 82 08           5992 	mov	dpl,#0x08
   14CE C0 02              5993 	push	ar2
   14D0 12 05 16           5994 	lcall	_lcdcreatechar
   14D3 D0 02              5995 	pop	ar2
                           5996 ;	main.c:944: lcdcreatechar(CGRAM_THIRDCHAR_ADDR, in_array2);
                           5997 ;	genCast
   14D5 90 00 28           5998 	mov	dptr,#_lcdcreatechar_PARM_2
   14D8 74 5F              5999 	mov	a,#_custom_character_in_array2_1_1
   14DA F0                 6000 	movx	@dptr,a
   14DB A3                 6001 	inc	dptr
   14DC 74 00              6002 	mov	a,#(_custom_character_in_array2_1_1 >> 8)
   14DE F0                 6003 	movx	@dptr,a
   14DF A3                 6004 	inc	dptr
   14E0 74 00              6005 	mov	a,#0x0
   14E2 F0                 6006 	movx	@dptr,a
                           6007 ;	genCall
   14E3 75 82 10           6008 	mov	dpl,#0x10
   14E6 C0 02              6009 	push	ar2
   14E8 12 05 16           6010 	lcall	_lcdcreatechar
   14EB D0 02              6011 	pop	ar2
                           6012 ;	main.c:945: lcdcreatechar(CGRAM_FOURTHCHAR_ADDR, in_array3);
                           6013 ;	genCast
   14ED 90 00 28           6014 	mov	dptr,#_lcdcreatechar_PARM_2
   14F0 74 67              6015 	mov	a,#_custom_character_in_array3_1_1
   14F2 F0                 6016 	movx	@dptr,a
   14F3 A3                 6017 	inc	dptr
   14F4 74 00              6018 	mov	a,#(_custom_character_in_array3_1_1 >> 8)
   14F6 F0                 6019 	movx	@dptr,a
   14F7 A3                 6020 	inc	dptr
   14F8 74 00              6021 	mov	a,#0x0
   14FA F0                 6022 	movx	@dptr,a
                           6023 ;	genCall
   14FB 75 82 18           6024 	mov	dpl,#0x18
   14FE C0 02              6025 	push	ar2
   1500 12 05 16           6026 	lcall	_lcdcreatechar
   1503 D0 02              6027 	pop	ar2
                           6028 ;	main.c:946: lcdcreatechar(CGRAM_FIFTHCHAR_ADDR, in_array4);
                           6029 ;	genCast
   1505 90 00 28           6030 	mov	dptr,#_lcdcreatechar_PARM_2
   1508 74 6F              6031 	mov	a,#_custom_character_in_array4_1_1
   150A F0                 6032 	movx	@dptr,a
   150B A3                 6033 	inc	dptr
   150C 74 00              6034 	mov	a,#(_custom_character_in_array4_1_1 >> 8)
   150E F0                 6035 	movx	@dptr,a
   150F A3                 6036 	inc	dptr
   1510 74 00              6037 	mov	a,#0x0
   1512 F0                 6038 	movx	@dptr,a
                           6039 ;	genCall
   1513 75 82 20           6040 	mov	dpl,#0x20
   1516 C0 02              6041 	push	ar2
   1518 12 05 16           6042 	lcall	_lcdcreatechar
   151B D0 02              6043 	pop	ar2
                           6044 ;	main.c:947: lcdcreatechar(CGRAM_SIXTHCHAR_ADDR, in_array5);
                           6045 ;	genCast
   151D 90 00 28           6046 	mov	dptr,#_lcdcreatechar_PARM_2
   1520 74 4F              6047 	mov	a,#_custom_character_in_array5_1_1
   1522 F0                 6048 	movx	@dptr,a
   1523 A3                 6049 	inc	dptr
   1524 74 00              6050 	mov	a,#(_custom_character_in_array5_1_1 >> 8)
   1526 F0                 6051 	movx	@dptr,a
   1527 A3                 6052 	inc	dptr
   1528 74 00              6053 	mov	a,#0x0
   152A F0                 6054 	movx	@dptr,a
                           6055 ;	genCall
   152B 75 82 28           6056 	mov	dpl,#0x28
   152E C0 02              6057 	push	ar2
   1530 12 05 16           6058 	lcall	_lcdcreatechar
   1533 D0 02              6059 	pop	ar2
                           6060 ;	main.c:948: lcdcreatechar(CGRAM_SEVENTHCHAR_ADDR, in_array6);
                           6061 ;	genCast
   1535 90 00 28           6062 	mov	dptr,#_lcdcreatechar_PARM_2
   1538 74 77              6063 	mov	a,#_custom_character_in_array6_1_1
   153A F0                 6064 	movx	@dptr,a
   153B A3                 6065 	inc	dptr
   153C 74 00              6066 	mov	a,#(_custom_character_in_array6_1_1 >> 8)
   153E F0                 6067 	movx	@dptr,a
   153F A3                 6068 	inc	dptr
   1540 74 00              6069 	mov	a,#0x0
   1542 F0                 6070 	movx	@dptr,a
                           6071 ;	genCall
   1543 75 82 30           6072 	mov	dpl,#0x30
   1546 C0 02              6073 	push	ar2
   1548 12 05 16           6074 	lcall	_lcdcreatechar
   154B D0 02              6075 	pop	ar2
                           6076 ;	main.c:949: lcdcreatechar(CGRAM_EIGHTHCHAR_ADDR, in_array7);
                           6077 ;	genCast
   154D 90 00 28           6078 	mov	dptr,#_lcdcreatechar_PARM_2
   1550 74 7F              6079 	mov	a,#_custom_character_in_array7_1_1
   1552 F0                 6080 	movx	@dptr,a
   1553 A3                 6081 	inc	dptr
   1554 74 00              6082 	mov	a,#(_custom_character_in_array7_1_1 >> 8)
   1556 F0                 6083 	movx	@dptr,a
   1557 A3                 6084 	inc	dptr
   1558 74 00              6085 	mov	a,#0x0
   155A F0                 6086 	movx	@dptr,a
                           6087 ;	genCall
   155B 75 82 38           6088 	mov	dpl,#0x38
   155E C0 02              6089 	push	ar2
   1560 12 05 16           6090 	lcall	_lcdcreatechar
   1563 D0 02              6091 	pop	ar2
                           6092 ;	main.c:950: lcdgotoxy(3,2);
                           6093 ;	genAssign
   1565 90 00 1D           6094 	mov	dptr,#_lcdgotoxy_PARM_2
   1568 74 02              6095 	mov	a,#0x02
   156A F0                 6096 	movx	@dptr,a
                           6097 ;	genCall
   156B 75 82 03           6098 	mov	dpl,#0x03
   156E C0 02              6099 	push	ar2
   1570 12 02 10           6100 	lcall	_lcdgotoxy
   1573 D0 02              6101 	pop	ar2
                           6102 ;	main.c:951: lcdputch(0x01);
                           6103 ;	genCall
   1575 75 82 01           6104 	mov	dpl,#0x01
   1578 C0 02              6105 	push	ar2
   157A 12 02 7D           6106 	lcall	_lcdputch
   157D D0 02              6107 	pop	ar2
                           6108 ;	main.c:952: lcdgotoxy(2,2);
                           6109 ;	genAssign
   157F 90 00 1D           6110 	mov	dptr,#_lcdgotoxy_PARM_2
   1582 74 02              6111 	mov	a,#0x02
   1584 F0                 6112 	movx	@dptr,a
                           6113 ;	genCall
   1585 75 82 02           6114 	mov	dpl,#0x02
   1588 C0 02              6115 	push	ar2
   158A 12 02 10           6116 	lcall	_lcdgotoxy
   158D D0 02              6117 	pop	ar2
                           6118 ;	main.c:953: lcdputch(0x00);
                           6119 ;	genCall
   158F 75 82 00           6120 	mov	dpl,#0x00
   1592 C0 02              6121 	push	ar2
   1594 12 02 7D           6122 	lcall	_lcdputch
   1597 D0 02              6123 	pop	ar2
                           6124 ;	main.c:954: lcdgotoxy(1,2);
                           6125 ;	genAssign
   1599 90 00 1D           6126 	mov	dptr,#_lcdgotoxy_PARM_2
   159C 74 02              6127 	mov	a,#0x02
   159E F0                 6128 	movx	@dptr,a
                           6129 ;	genCall
   159F 75 82 01           6130 	mov	dpl,#0x01
   15A2 C0 02              6131 	push	ar2
   15A4 12 02 10           6132 	lcall	_lcdgotoxy
   15A7 D0 02              6133 	pop	ar2
                           6134 ;	main.c:955: lcdputch(0x00);
                           6135 ;	genCall
   15A9 75 82 00           6136 	mov	dpl,#0x00
   15AC C0 02              6137 	push	ar2
   15AE 12 02 7D           6138 	lcall	_lcdputch
   15B1 D0 02              6139 	pop	ar2
                           6140 ;	main.c:956: lcdgotoxy(0,2);
                           6141 ;	genAssign
   15B3 90 00 1D           6142 	mov	dptr,#_lcdgotoxy_PARM_2
   15B6 74 02              6143 	mov	a,#0x02
   15B8 F0                 6144 	movx	@dptr,a
                           6145 ;	genCall
   15B9 75 82 00           6146 	mov	dpl,#0x00
   15BC C0 02              6147 	push	ar2
   15BE 12 02 10           6148 	lcall	_lcdgotoxy
   15C1 D0 02              6149 	pop	ar2
                           6150 ;	main.c:957: lcdputch(0x05);
                           6151 ;	genCall
   15C3 75 82 05           6152 	mov	dpl,#0x05
   15C6 C0 02              6153 	push	ar2
   15C8 12 02 7D           6154 	lcall	_lcdputch
   15CB D0 02              6155 	pop	ar2
                           6156 ;	main.c:958: lcdgotoxy(3,1);
                           6157 ;	genAssign
   15CD 90 00 1D           6158 	mov	dptr,#_lcdgotoxy_PARM_2
   15D0 74 01              6159 	mov	a,#0x01
   15D2 F0                 6160 	movx	@dptr,a
                           6161 ;	genCall
   15D3 75 82 03           6162 	mov	dpl,#0x03
   15D6 C0 02              6163 	push	ar2
   15D8 12 02 10           6164 	lcall	_lcdgotoxy
   15DB D0 02              6165 	pop	ar2
                           6166 ;	main.c:959: lcdputch(0x02);
                           6167 ;	genCall
   15DD 75 82 02           6168 	mov	dpl,#0x02
   15E0 C0 02              6169 	push	ar2
   15E2 12 02 7D           6170 	lcall	_lcdputch
   15E5 D0 02              6171 	pop	ar2
                           6172 ;	main.c:960: lcdgotoxy(3,3);
                           6173 ;	genAssign
   15E7 90 00 1D           6174 	mov	dptr,#_lcdgotoxy_PARM_2
   15EA 74 03              6175 	mov	a,#0x03
   15EC F0                 6176 	movx	@dptr,a
                           6177 ;	genCall
   15ED 75 82 03           6178 	mov	dpl,#0x03
   15F0 C0 02              6179 	push	ar2
   15F2 12 02 10           6180 	lcall	_lcdgotoxy
   15F5 D0 02              6181 	pop	ar2
                           6182 ;	main.c:961: lcdputch(0x07);
                           6183 ;	genCall
   15F7 75 82 07           6184 	mov	dpl,#0x07
   15FA C0 02              6185 	push	ar2
   15FC 12 02 7D           6186 	lcall	_lcdputch
   15FF D0 02              6187 	pop	ar2
                           6188 ;	main.c:962: lcdgotoxy(0,3);
                           6189 ;	genAssign
   1601 90 00 1D           6190 	mov	dptr,#_lcdgotoxy_PARM_2
   1604 74 03              6191 	mov	a,#0x03
   1606 F0                 6192 	movx	@dptr,a
                           6193 ;	genCall
   1607 75 82 00           6194 	mov	dpl,#0x00
   160A C0 02              6195 	push	ar2
   160C 12 02 10           6196 	lcall	_lcdgotoxy
   160F D0 02              6197 	pop	ar2
                           6198 ;	main.c:963: lcdputch(0x03);
                           6199 ;	genCall
   1611 75 82 03           6200 	mov	dpl,#0x03
   1614 C0 02              6201 	push	ar2
   1616 12 02 7D           6202 	lcall	_lcdputch
   1619 D0 02              6203 	pop	ar2
                           6204 ;	main.c:964: lcdgotoxy(0,4);
                           6205 ;	genAssign
   161B 90 00 1D           6206 	mov	dptr,#_lcdgotoxy_PARM_2
   161E 74 04              6207 	mov	a,#0x04
   1620 F0                 6208 	movx	@dptr,a
                           6209 ;	genCall
   1621 75 82 00           6210 	mov	dpl,#0x00
   1624 C0 02              6211 	push	ar2
   1626 12 02 10           6212 	lcall	_lcdgotoxy
   1629 D0 02              6213 	pop	ar2
                           6214 ;	main.c:965: lcdputch(0x04);
                           6215 ;	genCall
   162B 75 82 04           6216 	mov	dpl,#0x04
   162E C0 02              6217 	push	ar2
   1630 12 02 7D           6218 	lcall	_lcdputch
   1633 D0 02              6219 	pop	ar2
                           6220 ;	main.c:966: lcdgotoxy(0,5);
                           6221 ;	genAssign
   1635 90 00 1D           6222 	mov	dptr,#_lcdgotoxy_PARM_2
   1638 74 05              6223 	mov	a,#0x05
   163A F0                 6224 	movx	@dptr,a
                           6225 ;	genCall
   163B 75 82 00           6226 	mov	dpl,#0x00
   163E C0 02              6227 	push	ar2
   1640 12 02 10           6228 	lcall	_lcdgotoxy
   1643 D0 02              6229 	pop	ar2
                           6230 ;	main.c:967: lcdputch(0x06);
                           6231 ;	genCall
   1645 75 82 06           6232 	mov	dpl,#0x06
   1648 C0 02              6233 	push	ar2
   164A 12 02 7D           6234 	lcall	_lcdputch
   164D D0 02              6235 	pop	ar2
                           6236 ;	main.c:968: *INST_WRITE=(kk|DDRAM_WRITE_MASK_BITS);
                           6237 ;	genAssign
                           6238 ;	Peephole 182.b	used 16 bit load of dptr
   164F 90 F0 00           6239 	mov	dptr,#0xF000
                           6240 ;	genOr
   1652 43 02 80           6241 	orl	ar2,#0x80
                           6242 ;	genPointerSet
                           6243 ;     genFarPointerSet
   1655 EA                 6244 	mov	a,r2
   1656 F0                 6245 	movx	@dptr,a
                           6246 ;	main.c:969: lcdbusywait();
                           6247 ;	genCall
                           6248 ;	Peephole 253.b	replaced lcall/ret with ljmp
   1657 02 01 00           6249 	ljmp	_lcdbusywait
                           6250 ;
                           6251 ;------------------------------------------------------------
                           6252 ;Allocation info for local variables in function 'Timer0Initialize'
                           6253 ;------------------------------------------------------------
                           6254 ;------------------------------------------------------------
                           6255 ;	main.c:973: void Timer0Initialize()
                           6256 ;	-----------------------------------------
                           6257 ;	 function Timer0Initialize
                           6258 ;	-----------------------------------------
   165A                    6259 _Timer0Initialize:
                           6260 ;	main.c:975: TMOD |= 0x09;
                           6261 ;	genOr
   165A 43 89 09           6262 	orl	_TMOD,#0x09
                           6263 ;	main.c:976: TH0 = 0x4C;
                           6264 ;	genAssign
   165D 75 8C 4C           6265 	mov	_TH0,#0x4C
                           6266 ;	main.c:977: TL0 = 0x20;
                           6267 ;	genAssign
   1660 75 8A 20           6268 	mov	_TL0,#0x20
                           6269 ;	main.c:978: IE = 0x82;
                           6270 ;	genAssign
   1663 75 A8 82           6271 	mov	_IE,#0x82
                           6272 ;	main.c:979: TR0=1;
                           6273 ;	genAssign
   1666 D2 8C              6274 	setb	_TR0
                           6275 ;	Peephole 300	removed redundant label 00101$
   1668 22                 6276 	ret
                           6277 ;------------------------------------------------------------
                           6278 ;Allocation info for local variables in function 'lcdtime'
                           6279 ;------------------------------------------------------------
                           6280 ;------------------------------------------------------------
                           6281 ;	main.c:982: void lcdtime()
                           6282 ;	-----------------------------------------
                           6283 ;	 function lcdtime
                           6284 ;	-----------------------------------------
   1669                    6285 _lcdtime:
                           6286 ;	main.c:984: lcdclockgotoaddr(0x59);
                           6287 ;	genCall
   1669 75 82 59           6288 	mov	dpl,#0x59
   166C 12 01 C8           6289 	lcall	_lcdclockgotoaddr
                           6290 ;	main.c:985: lcdputstr("00:00.0");
                           6291 ;	genCall
                           6292 ;	Peephole 182.a	used 16 bit load of DPTR
   166F 90 2D 0C           6293 	mov	dptr,#__str_61
   1672 75 F0 80           6294 	mov	b,#0x80
                           6295 ;	Peephole 253.b	replaced lcall/ret with ljmp
   1675 02 03 1F           6296 	ljmp	_lcdputstr
                           6297 ;
                           6298 ;------------------------------------------------------------
                           6299 ;Allocation info for local variables in function 'read_ioexpander'
                           6300 ;------------------------------------------------------------
                           6301 ;io_tem                    Allocated with name '_read_ioexpander_io_tem_1_1'
                           6302 ;------------------------------------------------------------
                           6303 ;	main.c:988: unsigned char read_ioexpander()
                           6304 ;	-----------------------------------------
                           6305 ;	 function read_ioexpander
                           6306 ;	-----------------------------------------
   1678                    6307 _read_ioexpander:
                           6308 ;	main.c:992: startbit();
                           6309 ;	genCall
   1678 12 0C FB           6310 	lcall	_startbit
                           6311 ;	main.c:993: send_data(io_tem);
                           6312 ;	genCall
   167B 75 82 71           6313 	mov	dpl,#0x71
   167E 12 0D 5C           6314 	lcall	_send_data
                           6315 ;	main.c:994: clock_func();
                           6316 ;	genCall
   1681 12 0D 88           6317 	lcall	_clock_func
                           6318 ;	main.c:995: stopbit();
                           6319 ;	genCall
   1684 12 0D 0A           6320 	lcall	_stopbit
                           6321 ;	main.c:996: io_tem = ACC;
                           6322 ;	genAssign
                           6323 ;	main.c:997: return io_tem;
                           6324 ;	genAssign
   1687 90 00 87           6325 	mov	dptr,#_read_ioexpander_io_tem_1_1
   168A E5 E0              6326 	mov	a,_ACC
   168C F0                 6327 	movx	@dptr,a
                           6328 ;	Peephole 180.a	removed redundant mov to dptr
   168D E0                 6329 	movx	a,@dptr
                           6330 ;	genRet
                           6331 ;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
   168E F5 82              6332 	mov	dpl,a
                           6333 ;	Peephole 300	removed redundant label 00101$
   1690 22                 6334 	ret
                           6335 ;------------------------------------------------------------
                           6336 ;Allocation info for local variables in function 'invert_ioexpander'
                           6337 ;------------------------------------------------------------
                           6338 ;value_io                  Allocated with name '_invert_ioexpander_value_io_1_1'
                           6339 ;------------------------------------------------------------
                           6340 ;	main.c:1000: unsigned char invert_ioexpander(unsigned char value_io)
                           6341 ;	-----------------------------------------
                           6342 ;	 function invert_ioexpander
                           6343 ;	-----------------------------------------
   1691                    6344 _invert_ioexpander:
                           6345 ;	genReceive
   1691 E5 82              6346 	mov	a,dpl
   1693 90 00 88           6347 	mov	dptr,#_invert_ioexpander_value_io_1_1
   1696 F0                 6348 	movx	@dptr,a
                           6349 ;	main.c:1002: value_io &= IOEXP_INIT_BITS;
                           6350 ;	genAssign
                           6351 ;	genAnd
   1697 90 00 88           6352 	mov	dptr,#_invert_ioexpander_value_io_1_1
   169A E0                 6353 	movx	a,@dptr
   169B FA                 6354 	mov	r2,a
                           6355 ;	Peephole 248.b	optimized and to xdata
   169C 54 01              6356 	anl	a,#0x01
   169E F0                 6357 	movx	@dptr,a
                           6358 ;	main.c:1003: value_io<<=1;
                           6359 ;	genAssign
   169F 90 00 88           6360 	mov	dptr,#_invert_ioexpander_value_io_1_1
   16A2 E0                 6361 	movx	a,@dptr
                           6362 ;	genLeftShift
                           6363 ;	genLeftShiftLiteral
                           6364 ;	genlshOne
                           6365 ;	Peephole 105	removed redundant mov
                           6366 ;	genAssign
                           6367 ;	Peephole 204	removed redundant mov
   16A3 25 E0              6368 	add	a,acc
   16A5 FA                 6369 	mov	r2,a
   16A6 90 00 88           6370 	mov	dptr,#_invert_ioexpander_value_io_1_1
                           6371 ;	Peephole 100	removed redundant mov
   16A9 F0                 6372 	movx	@dptr,a
                           6373 ;	main.c:1004: value_io ^= 0xFF;
                           6374 ;	genAssign
                           6375 ;	genXor
                           6376 ;	Peephole 236.n	used r2 instead of ar2
   16AA 90 00 88           6377 	mov	dptr,#_invert_ioexpander_value_io_1_1
   16AD E0                 6378 	movx	a,@dptr
   16AE FA                 6379 	mov	r2,a
                           6380 ;	Peephole 248.c	optimized xor to xdata
   16AF 64 FF              6381 	xrl	a,#0xFF
   16B1 F0                 6382 	movx	@dptr,a
                           6383 ;	main.c:1005: value_io &= 0x02;
                           6384 ;	genAssign
                           6385 ;	genAnd
                           6386 ;	Peephole 248.b	optimized and to xdata
                           6387 ;	main.c:1006: value_io |= 0x01;
                           6388 ;	genAssign
                           6389 ;	genOr
                           6390 ;	Peephole 248.a	optimized or to xdata
   16B2 90 00 88           6391 	mov	dptr,#_invert_ioexpander_value_io_1_1
   16B5 E0                 6392 	movx	a,@dptr
                           6393 ;	Peephole 248.g	optimized and/or to volatile xdata
   16B6 54 02              6394 	anl	a,#0x02
   16B8 F0                 6395 	movx	@dptr,a
   16B9 E0                 6396 	movx	a,@dptr
   16BA FA                 6397 	mov	r2,a
   16BB 44 01              6398 	orl	a,#0x01
   16BD F0                 6399 	movx	@dptr,a
                           6400 ;	main.c:1007: return value_io;
                           6401 ;	genAssign
   16BE 90 00 88           6402 	mov	dptr,#_invert_ioexpander_value_io_1_1
   16C1 E0                 6403 	movx	a,@dptr
                           6404 ;	genRet
                           6405 ;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
   16C2 F5 82              6406 	mov	dpl,a
                           6407 ;	Peephole 300	removed redundant label 00101$
   16C4 22                 6408 	ret
                           6409 ;------------------------------------------------------------
                           6410 ;Allocation info for local variables in function 'write_ioexpander'
                           6411 ;------------------------------------------------------------
                           6412 ;send_bits                 Allocated with name '_write_ioexpander_send_bits_1_1'
                           6413 ;io_temp                   Allocated with name '_write_ioexpander_io_temp_1_1'
                           6414 ;------------------------------------------------------------
                           6415 ;	main.c:1010: void write_ioexpander(unsigned char send_bits)
                           6416 ;	-----------------------------------------
                           6417 ;	 function write_ioexpander
                           6418 ;	-----------------------------------------
   16C5                    6419 _write_ioexpander:
                           6420 ;	genReceive
   16C5 E5 82              6421 	mov	a,dpl
   16C7 90 00 89           6422 	mov	dptr,#_write_ioexpander_send_bits_1_1
   16CA F0                 6423 	movx	@dptr,a
                           6424 ;	main.c:1014: startbit();
                           6425 ;	genCall
   16CB 12 0C FB           6426 	lcall	_startbit
                           6427 ;	main.c:1015: send_data(io_temp);
                           6428 ;	genCall
   16CE 75 82 70           6429 	mov	dpl,#0x70
   16D1 12 0D 5C           6430 	lcall	_send_data
                           6431 ;	main.c:1016: send_data(send_bits);
                           6432 ;	genAssign
   16D4 90 00 89           6433 	mov	dptr,#_write_ioexpander_send_bits_1_1
   16D7 E0                 6434 	movx	a,@dptr
                           6435 ;	genCall
   16D8 FA                 6436 	mov	r2,a
                           6437 ;	Peephole 244.c	loading dpl from a instead of r2
   16D9 F5 82              6438 	mov	dpl,a
   16DB 12 0D 5C           6439 	lcall	_send_data
                           6440 ;	main.c:1017: stopbit();
                           6441 ;	genCall
                           6442 ;	Peephole 253.b	replaced lcall/ret with ljmp
   16DE 02 0D 0A           6443 	ljmp	_stopbit
                           6444 ;
                           6445 ;------------------------------------------------------------
                           6446 ;Allocation info for local variables in function 'external_isr'
                           6447 ;------------------------------------------------------------
                           6448 ;io_data                   Allocated with name '_external_isr_io_data_1_1'
                           6449 ;io_inver                  Allocated with name '_external_isr_io_inver_1_1'
                           6450 ;------------------------------------------------------------
                           6451 ;	main.c:1020: void external_isr(void) __interrupt(2) __using(2)
                           6452 ;	-----------------------------------------
                           6453 ;	 function external_isr
                           6454 ;	-----------------------------------------
   16E1                    6455 _external_isr:
                    0012   6456 	ar2 = 0x12
                    0013   6457 	ar3 = 0x13
                    0014   6458 	ar4 = 0x14
                    0015   6459 	ar5 = 0x15
                    0016   6460 	ar6 = 0x16
                    0017   6461 	ar7 = 0x17
                    0010   6462 	ar0 = 0x10
                    0011   6463 	ar1 = 0x11
   16E1 C0 E0              6464 	push	acc
   16E3 C0 F0              6465 	push	b
   16E5 C0 82              6466 	push	dpl
   16E7 C0 83              6467 	push	dph
   16E9 C0 02              6468 	push	(0+2)
   16EB C0 03              6469 	push	(0+3)
   16ED C0 04              6470 	push	(0+4)
   16EF C0 05              6471 	push	(0+5)
   16F1 C0 06              6472 	push	(0+6)
   16F3 C0 07              6473 	push	(0+7)
   16F5 C0 00              6474 	push	(0+0)
   16F7 C0 01              6475 	push	(0+1)
   16F9 C0 D0              6476 	push	psw
   16FB 75 D0 10           6477 	mov	psw,#0x10
                           6478 ;	main.c:1023: EX1=0;
                           6479 ;	genAssign
   16FE C2 AA              6480 	clr	_EX1
                           6481 ;	main.c:1024: io_data=read_ioexpander();
                           6482 ;	genCall
   1700 75 D0 00           6483 	mov	psw,#0x00
   1703 12 16 78           6484 	lcall	_read_ioexpander
   1706 75 D0 10           6485 	mov	psw,#0x10
                           6486 ;	main.c:1025: io_inver=invert_ioexpander(io_data);
                           6487 ;	genCall
   1709 AA 82              6488 	mov  r2,dpl
                           6489 ;	Peephole 177.a	removed redundant mov
   170B 75 D0 00           6490 	mov	psw,#0x00
   170E 12 16 91           6491 	lcall	_invert_ioexpander
   1711 75 D0 10           6492 	mov	psw,#0x10
                           6493 ;	main.c:1026: write_ioexpander(io_inver);
                           6494 ;	genCall
   1714 AA 82              6495 	mov  r2,dpl
                           6496 ;	Peephole 177.a	removed redundant mov
   1716 75 D0 00           6497 	mov	psw,#0x00
   1719 12 16 C5           6498 	lcall	_write_ioexpander
   171C 75 D0 10           6499 	mov	psw,#0x10
                           6500 ;	main.c:1027: EX1=1;
                           6501 ;	genAssign
   171F D2 AA              6502 	setb	_EX1
                           6503 ;	Peephole 300	removed redundant label 00101$
   1721 D0 D0              6504 	pop	psw
   1723 D0 01              6505 	pop	(0+1)
   1725 D0 00              6506 	pop	(0+0)
   1727 D0 07              6507 	pop	(0+7)
   1729 D0 06              6508 	pop	(0+6)
   172B D0 05              6509 	pop	(0+5)
   172D D0 04              6510 	pop	(0+4)
   172F D0 03              6511 	pop	(0+3)
   1731 D0 02              6512 	pop	(0+2)
   1733 D0 83              6513 	pop	dph
   1735 D0 82              6514 	pop	dpl
   1737 D0 F0              6515 	pop	b
   1739 D0 E0              6516 	pop	acc
   173B 32                 6517 	reti
                           6518 ;------------------------------------------------------------
                           6519 ;Allocation info for local variables in function 'timer_isr'
                           6520 ;------------------------------------------------------------
                           6521 ;interr                    Allocated with name '_timer_isr_interr_1_1'
                           6522 ;bb                        Allocated with name '_timer_isr_bb_1_1'
                           6523 ;------------------------------------------------------------
                           6524 ;	main.c:1030: void timer_isr(void) __critical __interrupt(1)__using(1)
                           6525 ;	-----------------------------------------
                           6526 ;	 function timer_isr
                           6527 ;	-----------------------------------------
   173C                    6528 _timer_isr:
                    000A   6529 	ar2 = 0x0a
                    000B   6530 	ar3 = 0x0b
                    000C   6531 	ar4 = 0x0c
                    000D   6532 	ar5 = 0x0d
                    000E   6533 	ar6 = 0x0e
                    000F   6534 	ar7 = 0x0f
                    0008   6535 	ar0 = 0x08
                    0009   6536 	ar1 = 0x09
   173C C0 E0              6537 	push	acc
   173E C0 F0              6538 	push	b
   1740 C0 82              6539 	push	dpl
   1742 C0 83              6540 	push	dph
   1744 C0 02              6541 	push	(0+2)
   1746 C0 03              6542 	push	(0+3)
   1748 C0 04              6543 	push	(0+4)
   174A C0 05              6544 	push	(0+5)
   174C C0 06              6545 	push	(0+6)
   174E C0 07              6546 	push	(0+7)
   1750 C0 00              6547 	push	(0+0)
   1752 C0 01              6548 	push	(0+1)
   1754 C0 D0              6549 	push	psw
   1756 75 D0 08           6550 	mov	psw,#0x08
   1759 D3                 6551 	setb	c
   175A 10 AF 01           6552 	jbc	ea,00126$
   175D C3                 6553 	clr	c
   175E                    6554 00126$:
   175E C0 D0              6555 	push	psw
                           6556 ;	main.c:1034: TF0=0;
                           6557 ;	genAssign
   1760 C2 8D              6558 	clr	_TF0
                           6559 ;	main.c:1035: TH0 = 0x4C;
                           6560 ;	genAssign
   1762 75 8C 4C           6561 	mov	_TH0,#0x4C
                           6562 ;	main.c:1036: TL0 = 0x20;
                           6563 ;	genAssign
   1765 75 8A 20           6564 	mov	_TL0,#0x20
                           6565 ;	main.c:1037: TR0 = 1;
                           6566 ;	genAssign
   1768 D2 8C              6567 	setb	_TR0
                           6568 ;	main.c:1038: if(CLOCK_ENABLE_FLAG)
                           6569 ;	genAssign
   176A 90 00 BC           6570 	mov	dptr,#_CLOCK_ENABLE_FLAG
   176D E0                 6571 	movx	a,@dptr
                           6572 ;	genIfx
   176E FA                 6573 	mov	r2,a
                           6574 ;	Peephole 105	removed redundant mov
                           6575 ;	genIfxJump
   176F 70 03              6576 	jnz	00127$
   1771 02 19 5C           6577 	ljmp	00116$
   1774                    6578 00127$:
                           6579 ;	main.c:1040: if(count_flag==0x00)
                           6580 ;	genAssign
   1774 90 00 10           6581 	mov	dptr,#_count_flag
   1777 E0                 6582 	movx	a,@dptr
   1778 FA                 6583 	mov	r2,a
   1779 A3                 6584 	inc	dptr
   177A E0                 6585 	movx	a,@dptr
                           6586 ;	genIfx
   177B FB                 6587 	mov	r3,a
                           6588 ;	Peephole 135	removed redundant mov
   177C 4A                 6589 	orl	a,r2
                           6590 ;	genIfxJump
   177D 60 03              6591 	jz	00128$
   177F 02 19 47           6592 	ljmp	00113$
   1782                    6593 00128$:
                           6594 ;	main.c:1042: P1_6 = 0;
                           6595 ;	genAssign
   1782 C2 96              6596 	clr	_P1_6
                           6597 ;	main.c:1043: P1_6=1;
                           6598 ;	genAssign
   1784 D2 96              6599 	setb	_P1_6
                           6600 ;	main.c:1044: interr=*INST_READ;
                           6601 ;	genPointerGet
                           6602 ;	genFarPointerGet
                           6603 ;	Peephole 182.b	used 16 bit load of dptr
   1786 90 F8 00           6604 	mov	dptr,#0xF800
   1789 E0                 6605 	movx	a,@dptr
   178A FA                 6606 	mov	r2,a
                           6607 ;	main.c:1045: interr= interr&CURSORADDR_MASK_BITS;
                           6608 ;	genAnd
   178B 53 0A 7F           6609 	anl	ar2,#0x7F
                           6610 ;	main.c:1046: count_flag=0x02;
                           6611 ;	genAssign
   178E 90 00 10           6612 	mov	dptr,#_count_flag
   1791 74 02              6613 	mov	a,#0x02
   1793 F0                 6614 	movx	@dptr,a
   1794 E4                 6615 	clr	a
   1795 A3                 6616 	inc	dptr
   1796 F0                 6617 	movx	@dptr,a
                           6618 ;	main.c:1047: MS++;
                           6619 ;	genPlus
   1797 90 00 0B           6620 	mov	dptr,#_MS
   179A E0                 6621 	movx	a,@dptr
   179B 24 01              6622 	add	a,#0x01
   179D F0                 6623 	movx	@dptr,a
                           6624 ;	main.c:1048: lcdclockgotoaddr(0x5F);
                           6625 ;	genCall
   179E 75 82 5F           6626 	mov	dpl,#0x5F
   17A1 C0 0A              6627 	push	ar2
   17A3 75 D0 00           6628 	mov	psw,#0x00
   17A6 12 01 C8           6629 	lcall	_lcdclockgotoaddr
   17A9 75 D0 08           6630 	mov	psw,#0x08
   17AC D0 0A              6631 	pop	ar2
                           6632 ;	main.c:1049: if(MS>9)
                           6633 ;	genAssign
   17AE 90 00 0B           6634 	mov	dptr,#_MS
   17B1 E0                 6635 	movx	a,@dptr
                           6636 ;	genCmpGt
                           6637 ;	genCmp
                           6638 ;	genIfxJump
                           6639 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           6640 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   17B2 FB                 6641 	mov  r3,a
                           6642 ;	Peephole 177.a	removed redundant mov
   17B3 24 F6              6643 	add	a,#0xff - 0x09
   17B5 50 0C              6644 	jnc	00102$
                           6645 ;	Peephole 300	removed redundant label 00129$
                           6646 ;	main.c:1051: MS=0;
                           6647 ;	genAssign
   17B7 90 00 0B           6648 	mov	dptr,#_MS
                           6649 ;	Peephole 181	changed mov to clr
   17BA E4                 6650 	clr	a
   17BB F0                 6651 	movx	@dptr,a
                           6652 ;	main.c:1052: S++;
                           6653 ;	genPlus
   17BC 90 00 0C           6654 	mov	dptr,#_S
   17BF E0                 6655 	movx	a,@dptr
   17C0 24 01              6656 	add	a,#0x01
   17C2 F0                 6657 	movx	@dptr,a
   17C3                    6658 00102$:
                           6659 ;	main.c:1054: lcdputch(MS+CLOCK_CGRAM_BASE_ADDR);
                           6660 ;	genAssign
   17C3 90 00 0B           6661 	mov	dptr,#_MS
   17C6 E0                 6662 	movx	a,@dptr
   17C7 FB                 6663 	mov	r3,a
                           6664 ;	genPlus
                           6665 ;     genPlusIncr
   17C8 74 30              6666 	mov	a,#0x30
                           6667 ;	Peephole 236.a	used r3 instead of ar3
   17CA 2B                 6668 	add	a,r3
                           6669 ;	genCall
   17CB FB                 6670 	mov	r3,a
                           6671 ;	Peephole 244.c	loading dpl from a instead of r3
   17CC F5 82              6672 	mov	dpl,a
   17CE C0 0A              6673 	push	ar2
   17D0 75 D0 00           6674 	mov	psw,#0x00
   17D3 12 02 7D           6675 	lcall	_lcdputch
   17D6 75 D0 08           6676 	mov	psw,#0x08
   17D9 D0 0A              6677 	pop	ar2
                           6678 ;	main.c:1055: if(S>9)
                           6679 ;	genAssign
   17DB 90 00 0C           6680 	mov	dptr,#_S
   17DE E0                 6681 	movx	a,@dptr
                           6682 ;	genCmpGt
                           6683 ;	genCmp
                           6684 ;	genIfxJump
                           6685 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           6686 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   17DF FB                 6687 	mov  r3,a
                           6688 ;	Peephole 177.a	removed redundant mov
   17E0 24 F6              6689 	add	a,#0xff - 0x09
   17E2 50 0C              6690 	jnc	00104$
                           6691 ;	Peephole 300	removed redundant label 00130$
                           6692 ;	main.c:1057: S=0;
                           6693 ;	genAssign
   17E4 90 00 0C           6694 	mov	dptr,#_S
                           6695 ;	Peephole 181	changed mov to clr
   17E7 E4                 6696 	clr	a
   17E8 F0                 6697 	movx	@dptr,a
                           6698 ;	main.c:1058: SS++;
                           6699 ;	genPlus
   17E9 90 00 0D           6700 	mov	dptr,#_SS
   17EC E0                 6701 	movx	a,@dptr
   17ED 24 01              6702 	add	a,#0x01
   17EF F0                 6703 	movx	@dptr,a
   17F0                    6704 00104$:
                           6705 ;	main.c:1060: lcdclockgotoaddr(0x5D);
                           6706 ;	genCall
   17F0 75 82 5D           6707 	mov	dpl,#0x5D
   17F3 C0 0A              6708 	push	ar2
   17F5 75 D0 00           6709 	mov	psw,#0x00
   17F8 12 01 C8           6710 	lcall	_lcdclockgotoaddr
   17FB 75 D0 08           6711 	mov	psw,#0x08
   17FE D0 0A              6712 	pop	ar2
                           6713 ;	main.c:1061: lcdputch(S+CLOCK_CGRAM_BASE_ADDR);
                           6714 ;	genAssign
   1800 90 00 0C           6715 	mov	dptr,#_S
   1803 E0                 6716 	movx	a,@dptr
   1804 FB                 6717 	mov	r3,a
                           6718 ;	genPlus
                           6719 ;     genPlusIncr
   1805 74 30              6720 	mov	a,#0x30
                           6721 ;	Peephole 236.a	used r3 instead of ar3
   1807 2B                 6722 	add	a,r3
                           6723 ;	genCall
   1808 FB                 6724 	mov	r3,a
                           6725 ;	Peephole 244.c	loading dpl from a instead of r3
   1809 F5 82              6726 	mov	dpl,a
   180B C0 0A              6727 	push	ar2
   180D 75 D0 00           6728 	mov	psw,#0x00
   1810 12 02 7D           6729 	lcall	_lcdputch
   1813 75 D0 08           6730 	mov	psw,#0x08
   1816 D0 0A              6731 	pop	ar2
                           6732 ;	main.c:1062: if(SS>5)
                           6733 ;	genAssign
   1818 90 00 0D           6734 	mov	dptr,#_SS
   181B E0                 6735 	movx	a,@dptr
                           6736 ;	genCmpGt
                           6737 ;	genCmp
                           6738 ;	genIfxJump
                           6739 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           6740 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   181C FB                 6741 	mov  r3,a
                           6742 ;	Peephole 177.a	removed redundant mov
   181D 24 FA              6743 	add	a,#0xff - 0x05
   181F 50 0C              6744 	jnc	00106$
                           6745 ;	Peephole 300	removed redundant label 00131$
                           6746 ;	main.c:1064: SS=0;
                           6747 ;	genAssign
   1821 90 00 0D           6748 	mov	dptr,#_SS
                           6749 ;	Peephole 181	changed mov to clr
   1824 E4                 6750 	clr	a
   1825 F0                 6751 	movx	@dptr,a
                           6752 ;	main.c:1065: M++;
                           6753 ;	genPlus
   1826 90 00 0E           6754 	mov	dptr,#_M
   1829 E0                 6755 	movx	a,@dptr
   182A 24 01              6756 	add	a,#0x01
   182C F0                 6757 	movx	@dptr,a
   182D                    6758 00106$:
                           6759 ;	main.c:1067: lcdclockgotoaddr(0x5C);
                           6760 ;	genCall
   182D 75 82 5C           6761 	mov	dpl,#0x5C
   1830 C0 0A              6762 	push	ar2
   1832 75 D0 00           6763 	mov	psw,#0x00
   1835 12 01 C8           6764 	lcall	_lcdclockgotoaddr
   1838 75 D0 08           6765 	mov	psw,#0x08
   183B D0 0A              6766 	pop	ar2
                           6767 ;	main.c:1068: lcdputch(SS+CLOCK_CGRAM_BASE_ADDR);
                           6768 ;	genAssign
   183D 90 00 0D           6769 	mov	dptr,#_SS
   1840 E0                 6770 	movx	a,@dptr
   1841 FB                 6771 	mov	r3,a
                           6772 ;	genPlus
                           6773 ;     genPlusIncr
   1842 74 30              6774 	mov	a,#0x30
                           6775 ;	Peephole 236.a	used r3 instead of ar3
   1844 2B                 6776 	add	a,r3
                           6777 ;	genCall
   1845 FB                 6778 	mov	r3,a
                           6779 ;	Peephole 244.c	loading dpl from a instead of r3
   1846 F5 82              6780 	mov	dpl,a
   1848 C0 0A              6781 	push	ar2
   184A 75 D0 00           6782 	mov	psw,#0x00
   184D 12 02 7D           6783 	lcall	_lcdputch
   1850 75 D0 08           6784 	mov	psw,#0x08
   1853 D0 0A              6785 	pop	ar2
                           6786 ;	main.c:1069: if(M>9)
                           6787 ;	genAssign
   1855 90 00 0E           6788 	mov	dptr,#_M
   1858 E0                 6789 	movx	a,@dptr
                           6790 ;	genCmpGt
                           6791 ;	genCmp
                           6792 ;	genIfxJump
                           6793 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           6794 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   1859 FB                 6795 	mov  r3,a
                           6796 ;	Peephole 177.a	removed redundant mov
   185A 24 F6              6797 	add	a,#0xff - 0x09
   185C 50 0C              6798 	jnc	00108$
                           6799 ;	Peephole 300	removed redundant label 00132$
                           6800 ;	main.c:1071: M=0;
                           6801 ;	genAssign
   185E 90 00 0E           6802 	mov	dptr,#_M
                           6803 ;	Peephole 181	changed mov to clr
   1861 E4                 6804 	clr	a
   1862 F0                 6805 	movx	@dptr,a
                           6806 ;	main.c:1072: MM++;
                           6807 ;	genPlus
   1863 90 00 0F           6808 	mov	dptr,#_MM
   1866 E0                 6809 	movx	a,@dptr
   1867 24 01              6810 	add	a,#0x01
   1869 F0                 6811 	movx	@dptr,a
   186A                    6812 00108$:
                           6813 ;	main.c:1074: lcdclockgotoaddr(0x5A);
                           6814 ;	genCall
   186A 75 82 5A           6815 	mov	dpl,#0x5A
   186D C0 0A              6816 	push	ar2
   186F 75 D0 00           6817 	mov	psw,#0x00
   1872 12 01 C8           6818 	lcall	_lcdclockgotoaddr
   1875 75 D0 08           6819 	mov	psw,#0x08
   1878 D0 0A              6820 	pop	ar2
                           6821 ;	main.c:1075: lcdputch(M+CLOCK_CGRAM_BASE_ADDR);
                           6822 ;	genAssign
   187A 90 00 0E           6823 	mov	dptr,#_M
   187D E0                 6824 	movx	a,@dptr
   187E FB                 6825 	mov	r3,a
                           6826 ;	genPlus
                           6827 ;     genPlusIncr
   187F 74 30              6828 	mov	a,#0x30
                           6829 ;	Peephole 236.a	used r3 instead of ar3
   1881 2B                 6830 	add	a,r3
                           6831 ;	genCall
   1882 FB                 6832 	mov	r3,a
                           6833 ;	Peephole 244.c	loading dpl from a instead of r3
   1883 F5 82              6834 	mov	dpl,a
   1885 C0 0A              6835 	push	ar2
   1887 75 D0 00           6836 	mov	psw,#0x00
   188A 12 02 7D           6837 	lcall	_lcdputch
   188D 75 D0 08           6838 	mov	psw,#0x08
   1890 D0 0A              6839 	pop	ar2
                           6840 ;	main.c:1076: lcdclockgotoaddr(0x59);
                           6841 ;	genCall
   1892 75 82 59           6842 	mov	dpl,#0x59
   1895 C0 0A              6843 	push	ar2
   1897 75 D0 00           6844 	mov	psw,#0x00
   189A 12 01 C8           6845 	lcall	_lcdclockgotoaddr
   189D 75 D0 08           6846 	mov	psw,#0x08
   18A0 D0 0A              6847 	pop	ar2
                           6848 ;	main.c:1077: lcdputch(MM+CLOCK_CGRAM_BASE_ADDR);
                           6849 ;	genAssign
   18A2 90 00 0F           6850 	mov	dptr,#_MM
   18A5 E0                 6851 	movx	a,@dptr
   18A6 FB                 6852 	mov	r3,a
                           6853 ;	genPlus
                           6854 ;     genPlusIncr
   18A7 74 30              6855 	mov	a,#0x30
                           6856 ;	Peephole 236.a	used r3 instead of ar3
   18A9 2B                 6857 	add	a,r3
                           6858 ;	genCall
   18AA FB                 6859 	mov	r3,a
                           6860 ;	Peephole 244.c	loading dpl from a instead of r3
   18AB F5 82              6861 	mov	dpl,a
   18AD C0 0A              6862 	push	ar2
   18AF 75 D0 00           6863 	mov	psw,#0x00
   18B2 12 02 7D           6864 	lcall	_lcdputch
   18B5 75 D0 08           6865 	mov	psw,#0x08
   18B8 D0 0A              6866 	pop	ar2
                           6867 ;	main.c:1078: lcdclockgotoaddr(0x5E);
                           6868 ;	genCall
   18BA 75 82 5E           6869 	mov	dpl,#0x5E
   18BD C0 0A              6870 	push	ar2
   18BF 75 D0 00           6871 	mov	psw,#0x00
   18C2 12 01 C8           6872 	lcall	_lcdclockgotoaddr
   18C5 75 D0 08           6873 	mov	psw,#0x08
   18C8 D0 0A              6874 	pop	ar2
                           6875 ;	main.c:1079: lcdputch('.');
                           6876 ;	genCall
   18CA 75 82 2E           6877 	mov	dpl,#0x2E
   18CD C0 0A              6878 	push	ar2
   18CF 75 D0 00           6879 	mov	psw,#0x00
   18D2 12 02 7D           6880 	lcall	_lcdputch
   18D5 75 D0 08           6881 	mov	psw,#0x08
   18D8 D0 0A              6882 	pop	ar2
                           6883 ;	main.c:1080: lcdclockgotoaddr(0x5B);
                           6884 ;	genCall
   18DA 75 82 5B           6885 	mov	dpl,#0x5B
   18DD C0 0A              6886 	push	ar2
   18DF 75 D0 00           6887 	mov	psw,#0x00
   18E2 12 01 C8           6888 	lcall	_lcdclockgotoaddr
   18E5 75 D0 08           6889 	mov	psw,#0x08
   18E8 D0 0A              6890 	pop	ar2
                           6891 ;	main.c:1081: lcdputch(':');
                           6892 ;	genCall
   18EA 75 82 3A           6893 	mov	dpl,#0x3A
   18ED C0 0A              6894 	push	ar2
   18EF 75 D0 00           6895 	mov	psw,#0x00
   18F2 12 02 7D           6896 	lcall	_lcdputch
   18F5 75 D0 08           6897 	mov	psw,#0x08
   18F8 D0 0A              6898 	pop	ar2
                           6899 ;	main.c:1082: *INST_WRITE=(interr | DDRAM_WRITE_MASK_BITS);
                           6900 ;	genAssign
                           6901 ;	Peephole 182.b	used 16 bit load of dptr
   18FA 90 F0 00           6902 	mov	dptr,#0xF000
                           6903 ;	genOr
   18FD 43 0A 80           6904 	orl	ar2,#0x80
                           6905 ;	genPointerSet
                           6906 ;     genFarPointerSet
   1900 EA                 6907 	mov	a,r2
   1901 F0                 6908 	movx	@dptr,a
                           6909 ;	main.c:1083: bb=*INST_READ;
                           6910 ;	genPointerGet
                           6911 ;	genFarPointerGet
                           6912 ;	Peephole 182.b	used 16 bit load of dptr
   1902 90 F8 00           6913 	mov	dptr,#0xF800
   1905 E0                 6914 	movx	a,@dptr
                           6915 ;	genCast
   1906 FA                 6916 	mov	r2,a
   1907 90 00 8A           6917 	mov	dptr,#_timer_isr_bb_1_1
                           6918 ;	Peephole 100	removed redundant mov
   190A F0                 6919 	movx	@dptr,a
   190B A3                 6920 	inc	dptr
                           6921 ;	Peephole 181	changed mov to clr
   190C E4                 6922 	clr	a
   190D F0                 6923 	movx	@dptr,a
                           6924 ;	main.c:1084: MSDelay(1);
                           6925 ;	genCall
                           6926 ;	Peephole 182.b	used 16 bit load of dptr
   190E 90 00 01           6927 	mov	dptr,#0x0001
   1911 75 D0 00           6928 	mov	psw,#0x00
   1914 12 00 77           6929 	lcall	_MSDelay
   1917 75 D0 08           6930 	mov	psw,#0x08
                           6931 ;	main.c:1085: while((bb&BUSYFLAG_MASK_BITS)==BUSYFLAG_MASK_BITS)
   191A                    6932 00109$:
                           6933 ;	genAssign
   191A 90 00 8A           6934 	mov	dptr,#_timer_isr_bb_1_1
   191D E0                 6935 	movx	a,@dptr
   191E FA                 6936 	mov	r2,a
   191F A3                 6937 	inc	dptr
   1920 E0                 6938 	movx	a,@dptr
   1921 FB                 6939 	mov	r3,a
                           6940 ;	genAnd
   1922 53 0A 80           6941 	anl	ar2,#0x80
   1925 7B 00              6942 	mov	r3,#0x00
                           6943 ;	genCmpEq
                           6944 ;	gencjneshort
                           6945 ;	Peephole 112.b	changed ljmp to sjmp
                           6946 ;	Peephole 198.a	optimized misc jump sequence
   1927 BA 80 1D           6947 	cjne	r2,#0x80,00113$
   192A BB 00 1A           6948 	cjne	r3,#0x00,00113$
                           6949 ;	Peephole 200.b	removed redundant sjmp
                           6950 ;	Peephole 300	removed redundant label 00133$
                           6951 ;	Peephole 300	removed redundant label 00134$
                           6952 ;	main.c:1087: bb= *INST_READ;
                           6953 ;	genPointerGet
                           6954 ;	genFarPointerGet
                           6955 ;	Peephole 182.b	used 16 bit load of dptr
   192D 90 F8 00           6956 	mov	dptr,#0xF800
   1930 E0                 6957 	movx	a,@dptr
                           6958 ;	genCast
   1931 FA                 6959 	mov	r2,a
   1932 90 00 8A           6960 	mov	dptr,#_timer_isr_bb_1_1
                           6961 ;	Peephole 100	removed redundant mov
   1935 F0                 6962 	movx	@dptr,a
   1936 A3                 6963 	inc	dptr
                           6964 ;	Peephole 181	changed mov to clr
   1937 E4                 6965 	clr	a
   1938 F0                 6966 	movx	@dptr,a
                           6967 ;	main.c:1088: MSDelay(1);
                           6968 ;	genCall
                           6969 ;	Peephole 182.b	used 16 bit load of dptr
   1939 90 00 01           6970 	mov	dptr,#0x0001
   193C 75 D0 00           6971 	mov	psw,#0x00
   193F 12 00 77           6972 	lcall	_MSDelay
   1942 75 D0 08           6973 	mov	psw,#0x08
                           6974 ;	Peephole 112.b	changed ljmp to sjmp
   1945 80 D3              6975 	sjmp	00109$
   1947                    6976 00113$:
                           6977 ;	main.c:1091: count_flag--;
                           6978 ;	genAssign
   1947 90 00 10           6979 	mov	dptr,#_count_flag
   194A E0                 6980 	movx	a,@dptr
   194B FA                 6981 	mov	r2,a
   194C A3                 6982 	inc	dptr
   194D E0                 6983 	movx	a,@dptr
   194E FB                 6984 	mov	r3,a
                           6985 ;	genMinus
                           6986 ;	genMinusDec
   194F 1A                 6987 	dec	r2
   1950 BA FF 01           6988 	cjne	r2,#0xff,00135$
   1953 1B                 6989 	dec	r3
   1954                    6990 00135$:
                           6991 ;	genAssign
   1954 90 00 10           6992 	mov	dptr,#_count_flag
   1957 EA                 6993 	mov	a,r2
   1958 F0                 6994 	movx	@dptr,a
   1959 A3                 6995 	inc	dptr
   195A EB                 6996 	mov	a,r3
   195B F0                 6997 	movx	@dptr,a
   195C                    6998 00116$:
   195C D0 D0              6999 	pop	psw
   195E 92 AF              7000 	mov	ea,c
   1960 D0 D0              7001 	pop	psw
   1962 D0 01              7002 	pop	(0+1)
   1964 D0 00              7003 	pop	(0+0)
   1966 D0 07              7004 	pop	(0+7)
   1968 D0 06              7005 	pop	(0+6)
   196A D0 05              7006 	pop	(0+5)
   196C D0 04              7007 	pop	(0+4)
   196E D0 03              7008 	pop	(0+3)
   1970 D0 02              7009 	pop	(0+2)
   1972 D0 83              7010 	pop	dph
   1974 D0 82              7011 	pop	dpl
   1976 D0 F0              7012 	pop	b
   1978 D0 E0              7013 	pop	acc
   197A 32                 7014 	reti
                           7015 ;------------------------------------------------------------
                           7016 ;Allocation info for local variables in function 'clock_reset'
                           7017 ;------------------------------------------------------------
                           7018 ;------------------------------------------------------------
                           7019 ;	main.c:1095: void clock_reset()
                           7020 ;	-----------------------------------------
                           7021 ;	 function clock_reset
                           7022 ;	-----------------------------------------
   197B                    7023 _clock_reset:
                    0002   7024 	ar2 = 0x02
                    0003   7025 	ar3 = 0x03
                    0004   7026 	ar4 = 0x04
                    0005   7027 	ar5 = 0x05
                    0006   7028 	ar6 = 0x06
                    0007   7029 	ar7 = 0x07
                    0000   7030 	ar0 = 0x00
                    0001   7031 	ar1 = 0x01
                           7032 ;	main.c:1097: EA=0;
                           7033 ;	genAssign
   197B C2 AF              7034 	clr	_EA
                           7035 ;	main.c:1098: MS=0;
                           7036 ;	genAssign
   197D 90 00 0B           7037 	mov	dptr,#_MS
                           7038 ;	Peephole 181	changed mov to clr
                           7039 ;	main.c:1099: S=0; SS=0;
                           7040 ;	genAssign
                           7041 ;	Peephole 181	changed mov to clr
                           7042 ;	Peephole 219.a	removed redundant clear
                           7043 ;	genAssign
                           7044 ;	Peephole 181	changed mov to clr
                           7045 ;	main.c:1100: M=0; MM=0;
                           7046 ;	genAssign
                           7047 ;	Peephole 181	changed mov to clr
                           7048 ;	Peephole 219.a	removed redundant clear
   1980 E4                 7049 	clr	a
   1981 F0                 7050 	movx	@dptr,a
   1982 90 00 0C           7051 	mov	dptr,#_S
   1985 F0                 7052 	movx	@dptr,a
   1986 90 00 0D           7053 	mov	dptr,#_SS
                           7054 ;	Peephole 219.b	removed redundant clear
   1989 F0                 7055 	movx	@dptr,a
   198A 90 00 0E           7056 	mov	dptr,#_M
   198D F0                 7057 	movx	@dptr,a
                           7058 ;	genAssign
   198E 90 00 0F           7059 	mov	dptr,#_MM
                           7060 ;	Peephole 181	changed mov to clr
   1991 E4                 7061 	clr	a
   1992 F0                 7062 	movx	@dptr,a
                           7063 ;	main.c:1101: lcdtime();
                           7064 ;	genCall
   1993 12 16 69           7065 	lcall	_lcdtime
                           7066 ;	main.c:1102: EA=1;
                           7067 ;	genAssign
   1996 D2 AF              7068 	setb	_EA
                           7069 ;	Peephole 300	removed redundant label 00101$
   1998 22                 7070 	ret
                           7071 ;------------------------------------------------------------
                           7072 ;Allocation info for local variables in function 'eereset'
                           7073 ;------------------------------------------------------------
                           7074 ;------------------------------------------------------------
                           7075 ;	main.c:1108: void eereset()
                           7076 ;	-----------------------------------------
                           7077 ;	 function eereset
                           7078 ;	-----------------------------------------
   1999                    7079 _eereset:
                           7080 ;	main.c:1110: ACC=0xFF;
                           7081 ;	genAssign
   1999 75 E0 FF           7082 	mov	_ACC,#0xFF
                           7083 ;	main.c:1111: startbit();
                           7084 ;	genCall
   199C 12 0C FB           7085 	lcall	_startbit
                           7086 ;	main.c:1112: send_data_assembly();
                           7087 ;	genCall
   199F 12 0D 41           7088 	lcall	_send_data_assembly
                           7089 ;	main.c:1113: ninthbit();
                           7090 ;	genCall
   19A2 12 0D 77           7091 	lcall	_ninthbit
                           7092 ;	main.c:1114: startbit();
                           7093 ;	genCall
   19A5 12 0C FB           7094 	lcall	_startbit
                           7095 ;	main.c:1115: stopbit();
                           7096 ;	genCall
                           7097 ;	Peephole 253.b	replaced lcall/ret with ljmp
   19A8 02 0D 0A           7098 	ljmp	_stopbit
                           7099 ;
                           7100 ;------------------------------------------------------------
                           7101 ;Allocation info for local variables in function 'IOexpInit'
                           7102 ;------------------------------------------------------------
                           7103 ;------------------------------------------------------------
                           7104 ;	main.c:1118: void IOexpInit()
                           7105 ;	-----------------------------------------
                           7106 ;	 function IOexpInit
                           7107 ;	-----------------------------------------
   19AB                    7108 _IOexpInit:
                           7109 ;	main.c:1120: IT1=1;
                           7110 ;	genAssign
   19AB D2 8A              7111 	setb	_IT1
                           7112 ;	main.c:1121: IE |=0x84;
                           7113 ;	genOr
   19AD 43 A8 84           7114 	orl	_IE,#0x84
                           7115 ;	main.c:1122: write_ioexpander(0x01);
                           7116 ;	genCall
   19B0 75 82 01           7117 	mov	dpl,#0x01
                           7118 ;	Peephole 253.b	replaced lcall/ret with ljmp
   19B3 02 16 C5           7119 	ljmp	_write_ioexpander
                           7120 ;
                           7121 ;------------------------------------------------------------
                           7122 ;Allocation info for local variables in function 'ioexpander_writedata'
                           7123 ;------------------------------------------------------------
                           7124 ;ioexp_write               Allocated with name '_ioexpander_writedata_ioexp_write_1_1'
                           7125 ;------------------------------------------------------------
                           7126 ;	main.c:1125: void ioexpander_writedata()
                           7127 ;	-----------------------------------------
                           7128 ;	 function ioexpander_writedata
                           7129 ;	-----------------------------------------
   19B6                    7130 _ioexpander_writedata:
                           7131 ;	main.c:1128: putstr("Enter the value to write\n\r");
                           7132 ;	genCall
                           7133 ;	Peephole 182.a	used 16 bit load of DPTR
   19B6 90 2D 14           7134 	mov	dptr,#__str_62
   19B9 75 F0 80           7135 	mov	b,#0x80
   19BC 12 00 AA           7136 	lcall	_putstr
                           7137 ;	main.c:1129: do
   19BF                    7138 00103$:
                           7139 ;	main.c:1131: ioexp_write=ReadVariable();
                           7140 ;	genCall
   19BF 12 0B 32           7141 	lcall	_ReadVariable
   19C2 AA 82              7142 	mov	r2,dpl
   19C4 AB 83              7143 	mov	r3,dph
                           7144 ;	main.c:1132: if(ioexp_write>0xFF)
                           7145 ;	genCmpGt
                           7146 ;	genCmp
   19C6 C3                 7147 	clr	c
   19C7 74 FF              7148 	mov	a,#0xFF
   19C9 9A                 7149 	subb	a,r2
                           7150 ;	Peephole 181	changed mov to clr
   19CA E4                 7151 	clr	a
   19CB 9B                 7152 	subb	a,r3
   19CC E4                 7153 	clr	a
   19CD 33                 7154 	rlc	a
                           7155 ;	genIfx
   19CE FC                 7156 	mov	r4,a
                           7157 ;	Peephole 105	removed redundant mov
                           7158 ;	genIfxJump
                           7159 ;	Peephole 108.c	removed ljmp by inverse jump logic
   19CF 60 15              7160 	jz	00104$
                           7161 ;	Peephole 300	removed redundant label 00111$
                           7162 ;	main.c:1133: putstr("Enter proper value\n\r");
                           7163 ;	genCall
                           7164 ;	Peephole 182.a	used 16 bit load of DPTR
   19D1 90 2D 2F           7165 	mov	dptr,#__str_63
   19D4 75 F0 80           7166 	mov	b,#0x80
   19D7 C0 02              7167 	push	ar2
   19D9 C0 03              7168 	push	ar3
   19DB C0 04              7169 	push	ar4
   19DD 12 00 AA           7170 	lcall	_putstr
   19E0 D0 04              7171 	pop	ar4
   19E2 D0 03              7172 	pop	ar3
   19E4 D0 02              7173 	pop	ar2
   19E6                    7174 00104$:
                           7175 ;	main.c:1134: }while(ioexp_write>0xFF);
                           7176 ;	genIfx
   19E6 EC                 7177 	mov	a,r4
                           7178 ;	genIfxJump
                           7179 ;	Peephole 108.b	removed ljmp by inverse jump logic
                           7180 ;	main.c:1135: write_ioexpander(0x00);
                           7181 ;	genCall
   19E7 70 D6              7182 	jnz	00103$
                           7183 ;	Peephole 300	removed redundant label 00112$
                           7184 ;	Peephole 256.c	loading dpl with zero from a
   19E9 F5 82              7185 	mov	dpl,a
   19EB C0 02              7186 	push	ar2
   19ED C0 03              7187 	push	ar3
   19EF 12 16 C5           7188 	lcall	_write_ioexpander
   19F2 D0 03              7189 	pop	ar3
   19F4 D0 02              7190 	pop	ar2
                           7191 ;	main.c:1136: write_ioexpander(ioexp_write);
                           7192 ;	genCast
                           7193 ;	genCall
   19F6 8A 82              7194 	mov	dpl,r2
                           7195 ;	Peephole 253.b	replaced lcall/ret with ljmp
   19F8 02 16 C5           7196 	ljmp	_write_ioexpander
                           7197 ;
                           7198 ;------------------------------------------------------------
                           7199 ;Allocation info for local variables in function 'ioexpander_readdata'
                           7200 ;------------------------------------------------------------
                           7201 ;ioexp_read                Allocated with name '_ioexpander_readdata_ioexp_read_1_1'
                           7202 ;------------------------------------------------------------
                           7203 ;	main.c:1139: void ioexpander_readdata()
                           7204 ;	-----------------------------------------
                           7205 ;	 function ioexpander_readdata
                           7206 ;	-----------------------------------------
   19FB                    7207 _ioexpander_readdata:
                           7208 ;	main.c:1142: write_ioexpander(0xFF);
                           7209 ;	genCall
   19FB 75 82 FF           7210 	mov	dpl,#0xFF
   19FE 12 16 C5           7211 	lcall	_write_ioexpander
                           7212 ;	main.c:1143: ioexp_read = read_ioexpander();
                           7213 ;	genCall
   1A01 12 16 78           7214 	lcall	_read_ioexpander
   1A04 AA 82              7215 	mov	r2,dpl
                           7216 ;	main.c:1144: putstr("Read input value from the IO Expander is ");
                           7217 ;	genCall
                           7218 ;	Peephole 182.a	used 16 bit load of DPTR
   1A06 90 2D 44           7219 	mov	dptr,#__str_64
   1A09 75 F0 80           7220 	mov	b,#0x80
   1A0C C0 02              7221 	push	ar2
   1A0E 12 00 AA           7222 	lcall	_putstr
   1A11 D0 02              7223 	pop	ar2
                           7224 ;	main.c:1145: printf("0x%2x\n\r", ioexp_read);
                           7225 ;	genCast
   1A13 7B 00              7226 	mov	r3,#0x00
                           7227 ;	genIpush
   1A15 C0 02              7228 	push	ar2
   1A17 C0 03              7229 	push	ar3
                           7230 ;	genIpush
   1A19 74 6E              7231 	mov	a,#__str_65
   1A1B C0 E0              7232 	push	acc
   1A1D 74 2D              7233 	mov	a,#(__str_65 >> 8)
   1A1F C0 E0              7234 	push	acc
   1A21 74 80              7235 	mov	a,#0x80
   1A23 C0 E0              7236 	push	acc
                           7237 ;	genCall
   1A25 12 1C 74           7238 	lcall	_printf
   1A28 E5 81              7239 	mov	a,sp
   1A2A 24 FB              7240 	add	a,#0xfb
   1A2C F5 81              7241 	mov	sp,a
                           7242 ;	Peephole 300	removed redundant label 00101$
   1A2E 22                 7243 	ret
                           7244 ;------------------------------------------------------------
                           7245 ;Allocation info for local variables in function 'ioexpander_config'
                           7246 ;------------------------------------------------------------
                           7247 ;io_con                    Allocated with name '_ioexpander_config_io_con_1_1'
                           7248 ;------------------------------------------------------------
                           7249 ;	main.c:1148: void ioexpander_config()
                           7250 ;	-----------------------------------------
                           7251 ;	 function ioexpander_config
                           7252 ;	-----------------------------------------
   1A2F                    7253 _ioexpander_config:
                           7254 ;	main.c:1151: putstr("Press 1 for input and 0 for output. Enter eight values for eight port pins and hit enter\n\r");
                           7255 ;	genCall
                           7256 ;	Peephole 182.a	used 16 bit load of DPTR
   1A2F 90 2D 76           7257 	mov	dptr,#__str_66
   1A32 75 F0 80           7258 	mov	b,#0x80
   1A35 12 00 AA           7259 	lcall	_putstr
                           7260 ;	main.c:1152: do
   1A38                    7261 00103$:
                           7262 ;	main.c:1154: io_con=readinput();
                           7263 ;	genCall
   1A38 12 07 45           7264 	lcall	_readinput
   1A3B AA 82              7265 	mov	r2,dpl
                           7266 ;	genCast
   1A3D 7B 00              7267 	mov	r3,#0x00
                           7268 ;	main.c:1155: if(io_con>0xFF)
                           7269 ;	genCmpGt
                           7270 ;	genCmp
   1A3F C3                 7271 	clr	c
   1A40 74 FF              7272 	mov	a,#0xFF
   1A42 9A                 7273 	subb	a,r2
                           7274 ;	Peephole 181	changed mov to clr
   1A43 E4                 7275 	clr	a
   1A44 9B                 7276 	subb	a,r3
   1A45 E4                 7277 	clr	a
   1A46 33                 7278 	rlc	a
                           7279 ;	genIfx
   1A47 FC                 7280 	mov	r4,a
                           7281 ;	Peephole 105	removed redundant mov
                           7282 ;	genIfxJump
                           7283 ;	Peephole 108.c	removed ljmp by inverse jump logic
   1A48 60 15              7284 	jz	00104$
                           7285 ;	Peephole 300	removed redundant label 00111$
                           7286 ;	main.c:1156: putstr("Enter proper value\n\r");
                           7287 ;	genCall
                           7288 ;	Peephole 182.a	used 16 bit load of DPTR
   1A4A 90 2D 2F           7289 	mov	dptr,#__str_63
   1A4D 75 F0 80           7290 	mov	b,#0x80
   1A50 C0 02              7291 	push	ar2
   1A52 C0 03              7292 	push	ar3
   1A54 C0 04              7293 	push	ar4
   1A56 12 00 AA           7294 	lcall	_putstr
   1A59 D0 04              7295 	pop	ar4
   1A5B D0 03              7296 	pop	ar3
   1A5D D0 02              7297 	pop	ar2
   1A5F                    7298 00104$:
                           7299 ;	main.c:1157: }while(io_con>0xFF);
                           7300 ;	genIfx
   1A5F EC                 7301 	mov	a,r4
                           7302 ;	genIfxJump
                           7303 ;	Peephole 108.b	removed ljmp by inverse jump logic
   1A60 70 D6              7304 	jnz	00103$
                           7305 ;	Peephole 300	removed redundant label 00112$
                           7306 ;	main.c:1158: write_ioexpander(io_con);
                           7307 ;	genCast
                           7308 ;	genCall
   1A62 8A 82              7309 	mov	dpl,r2
                           7310 ;	Peephole 253.b	replaced lcall/ret with ljmp
   1A64 02 16 C5           7311 	ljmp	_write_ioexpander
                           7312 ;
                           7313 ;------------------------------------------------------------
                           7314 ;Allocation info for local variables in function 'main'
                           7315 ;------------------------------------------------------------
                           7316 ;in                        Allocated with name '_main_in_1_1'
                           7317 ;------------------------------------------------------------
                           7318 ;	main.c:1161: void main(void)
                           7319 ;	-----------------------------------------
                           7320 ;	 function main
                           7321 ;	-----------------------------------------
   1A67                    7322 _main:
                           7323 ;	main.c:1164: MS=0; S=0; SS=0; M=0; MM=0;
                           7324 ;	genAssign
   1A67 90 00 0B           7325 	mov	dptr,#_MS
                           7326 ;	Peephole 181	changed mov to clr
                           7327 ;	genAssign
                           7328 ;	Peephole 181	changed mov to clr
                           7329 ;	Peephole 219.a	removed redundant clear
                           7330 ;	genAssign
                           7331 ;	Peephole 181	changed mov to clr
                           7332 ;	genAssign
                           7333 ;	Peephole 181	changed mov to clr
                           7334 ;	Peephole 219.a	removed redundant clear
   1A6A E4                 7335 	clr	a
   1A6B F0                 7336 	movx	@dptr,a
   1A6C 90 00 0C           7337 	mov	dptr,#_S
   1A6F F0                 7338 	movx	@dptr,a
   1A70 90 00 0D           7339 	mov	dptr,#_SS
                           7340 ;	Peephole 219.b	removed redundant clear
   1A73 F0                 7341 	movx	@dptr,a
   1A74 90 00 0E           7342 	mov	dptr,#_M
   1A77 F0                 7343 	movx	@dptr,a
                           7344 ;	genAssign
   1A78 90 00 0F           7345 	mov	dptr,#_MM
                           7346 ;	Peephole 181	changed mov to clr
   1A7B E4                 7347 	clr	a
   1A7C F0                 7348 	movx	@dptr,a
                           7349 ;	main.c:1165: count_flag=0x02;
                           7350 ;	genAssign
   1A7D 90 00 10           7351 	mov	dptr,#_count_flag
   1A80 74 02              7352 	mov	a,#0x02
   1A82 F0                 7353 	movx	@dptr,a
   1A83 E4                 7354 	clr	a
   1A84 A3                 7355 	inc	dptr
   1A85 F0                 7356 	movx	@dptr,a
                           7357 ;	main.c:1166: Timer0Initialize();
                           7358 ;	genCall
   1A86 12 16 5A           7359 	lcall	_Timer0Initialize
                           7360 ;	main.c:1167: SerialInitialize();
                           7361 ;	genCall
   1A89 12 0C D0           7362 	lcall	_SerialInitialize
                           7363 ;	main.c:1168: lcdinit();
                           7364 ;	genCall
   1A8C 12 03 75           7365 	lcall	_lcdinit
                           7366 ;	main.c:1169: eereset();
                           7367 ;	genCall
   1A8F 12 19 99           7368 	lcall	_eereset
                           7369 ;	main.c:1170: lcdtime();
                           7370 ;	genCall
   1A92 12 16 69           7371 	lcall	_lcdtime
                           7372 ;	main.c:1171: IOexpInit();
                           7373 ;	genCall
   1A95 12 19 AB           7374 	lcall	_IOexpInit
                           7375 ;	main.c:1172: lcdgotoxy(1,0);
                           7376 ;	genAssign
   1A98 90 00 1D           7377 	mov	dptr,#_lcdgotoxy_PARM_2
                           7378 ;	Peephole 181	changed mov to clr
   1A9B E4                 7379 	clr	a
   1A9C F0                 7380 	movx	@dptr,a
                           7381 ;	genCall
   1A9D 75 82 01           7382 	mov	dpl,#0x01
   1AA0 12 02 10           7383 	lcall	_lcdgotoxy
                           7384 ;	main.c:1174: lcdgotoxy(2,4);
                           7385 ;	genAssign
   1AA3 90 00 1D           7386 	mov	dptr,#_lcdgotoxy_PARM_2
   1AA6 74 04              7387 	mov	a,#0x04
   1AA8 F0                 7388 	movx	@dptr,a
                           7389 ;	genCall
   1AA9 75 82 02           7390 	mov	dpl,#0x02
   1AAC 12 02 10           7391 	lcall	_lcdgotoxy
                           7392 ;	main.c:1176: EA=0;
                           7393 ;	genAssign
   1AAF C2 AF              7394 	clr	_EA
                           7395 ;	main.c:1177: *INST_WRITE=(DDRAM_WRITE_MASK_BITS);
                           7396 ;	genAssign
                           7397 ;	Peephole 182.b	used 16 bit load of dptr
   1AB1 90 F0 00           7398 	mov	dptr,#0xF000
                           7399 ;	genPointerSet
                           7400 ;     genFarPointerSet
   1AB4 74 80              7401 	mov	a,#0x80
   1AB6 F0                 7402 	movx	@dptr,a
                           7403 ;	main.c:1178: EA=1;
                           7404 ;	genAssign
   1AB7 D2 AF              7405 	setb	_EA
                           7406 ;	main.c:1179: lcdbusywait();
                           7407 ;	genCall
   1AB9 12 01 00           7408 	lcall	_lcdbusywait
                           7409 ;	main.c:1180: putstr("Welcome\n\r");
                           7410 ;	genCall
                           7411 ;	Peephole 182.a	used 16 bit load of DPTR
   1ABC 90 2D D1           7412 	mov	dptr,#__str_67
   1ABF 75 F0 80           7413 	mov	b,#0x80
   1AC2 12 00 AA           7414 	lcall	_putstr
                           7415 ;	main.c:1181: putstr("Press '>' for help menu\n\r");
                           7416 ;	genCall
                           7417 ;	Peephole 182.a	used 16 bit load of DPTR
   1AC5 90 2D DB           7418 	mov	dptr,#__str_68
   1AC8 75 F0 80           7419 	mov	b,#0x80
   1ACB 12 00 AA           7420 	lcall	_putstr
                           7421 ;	main.c:1182: putstr("Enter the character\n\r");
                           7422 ;	genCall
                           7423 ;	Peephole 182.a	used 16 bit load of DPTR
   1ACE 90 2D F5           7424 	mov	dptr,#__str_69
   1AD1 75 F0 80           7425 	mov	b,#0x80
   1AD4 12 00 AA           7426 	lcall	_putstr
                           7427 ;	main.c:1183: while(1)
   1AD7                    7428 00125$:
                           7429 ;	main.c:1185: putstr("\n\rEnter the character\n\r");
                           7430 ;	genCall
                           7431 ;	Peephole 182.a	used 16 bit load of DPTR
   1AD7 90 2E 0B           7432 	mov	dptr,#__str_70
   1ADA 75 F0 80           7433 	mov	b,#0x80
   1ADD 12 00 AA           7434 	lcall	_putstr
                           7435 ;	main.c:1186: in=getchar();
                           7436 ;	genCall
   1AE0 12 0C F1           7437 	lcall	_getchar
   1AE3 AA 82              7438 	mov	r2,dpl
                           7439 ;	main.c:1187: switch(in)
                           7440 ;	genAssign
   1AE5 8A 03              7441 	mov	ar3,r2
                           7442 ;	genCmpLt
                           7443 ;	genCmp
   1AE7 BB 30 00           7444 	cjne	r3,#0x30,00132$
   1AEA                    7445 00132$:
                           7446 ;	genIfxJump
   1AEA 50 03              7447 	jnc	00133$
   1AEC 02 1B FC           7448 	ljmp	00122$
   1AEF                    7449 00133$:
                           7450 ;	genCmpGt
                           7451 ;	genCmp
                           7452 ;	genIfxJump
                           7453 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   1AEF EB                 7454 	mov	a,r3
   1AF0 24 A7              7455 	add	a,#0xff - 0x58
   1AF2 50 03              7456 	jnc	00134$
   1AF4 02 1B FC           7457 	ljmp	00122$
   1AF7                    7458 00134$:
                           7459 ;	genMinus
   1AF7 EA                 7460 	mov	a,r2
   1AF8 24 D0              7461 	add	a,#0xd0
                           7462 ;	genJumpTab
   1AFA FA                 7463 	mov	r2,a
                           7464 ;	Peephole 105	removed redundant mov
   1AFB 24 09              7465 	add	a,#(00135$-3-.)
   1AFD 83                 7466 	movc	a,@a+pc
   1AFE C0 E0              7467 	push	acc
   1B00 EA                 7468 	mov	a,r2
   1B01 24 2C              7469 	add	a,#(00136$-3-.)
   1B03 83                 7470 	movc	a,@a+pc
   1B04 C0 E0              7471 	push	acc
   1B06 22                 7472 	ret
   1B07                    7473 00135$:
   1B07 59                 7474 	.db	00101$
   1B08 6A                 7475 	.db	00102$
   1B09 7C                 7476 	.db	00103$
   1B0A FC                 7477 	.db	00122$
   1B0B FC                 7478 	.db	00122$
   1B0C FC                 7479 	.db	00122$
   1B0D FC                 7480 	.db	00122$
   1B0E FC                 7481 	.db	00122$
   1B0F FC                 7482 	.db	00122$
   1B10 FC                 7483 	.db	00122$
   1B11 FC                 7484 	.db	00122$
   1B12 FC                 7485 	.db	00122$
   1B13 FC                 7486 	.db	00122$
   1B14 FC                 7487 	.db	00122$
   1B15 9D                 7488 	.db	00107$
   1B16 FC                 7489 	.db	00122$
   1B17 FC                 7490 	.db	00122$
   1B18 FC                 7491 	.db	00122$
   1B19 E4                 7492 	.db	00118$
   1B1A AF                 7493 	.db	00110$
   1B1B B5                 7494 	.db	00111$
   1B1C BB                 7495 	.db	00112$
   1B1D EA                 7496 	.db	00119$
   1B1E A9                 7497 	.db	00109$
   1B1F F0                 7498 	.db	00120$
   1B20 CD                 7499 	.db	00115$
   1B21 F6                 7500 	.db	00121$
   1B22 D3                 7501 	.db	00116$
   1B23 FC                 7502 	.db	00122$
   1B24 FC                 7503 	.db	00122$
   1B25 C1                 7504 	.db	00113$
   1B26 FC                 7505 	.db	00122$
   1B27 C7                 7506 	.db	00114$
   1B28 FC                 7507 	.db	00122$
   1B29 91                 7508 	.db	00105$
   1B2A 97                 7509 	.db	00106$
   1B2B DE                 7510 	.db	00117$
   1B2C FC                 7511 	.db	00122$
   1B2D FC                 7512 	.db	00122$
   1B2E 8B                 7513 	.db	00104$
   1B2F A3                 7514 	.db	00108$
   1B30                    7515 00136$:
   1B30 1B                 7516 	.db	00101$>>8
   1B31 1B                 7517 	.db	00102$>>8
   1B32 1B                 7518 	.db	00103$>>8
   1B33 1B                 7519 	.db	00122$>>8
   1B34 1B                 7520 	.db	00122$>>8
   1B35 1B                 7521 	.db	00122$>>8
   1B36 1B                 7522 	.db	00122$>>8
   1B37 1B                 7523 	.db	00122$>>8
   1B38 1B                 7524 	.db	00122$>>8
   1B39 1B                 7525 	.db	00122$>>8
   1B3A 1B                 7526 	.db	00122$>>8
   1B3B 1B                 7527 	.db	00122$>>8
   1B3C 1B                 7528 	.db	00122$>>8
   1B3D 1B                 7529 	.db	00122$>>8
   1B3E 1B                 7530 	.db	00107$>>8
   1B3F 1B                 7531 	.db	00122$>>8
   1B40 1B                 7532 	.db	00122$>>8
   1B41 1B                 7533 	.db	00122$>>8
   1B42 1B                 7534 	.db	00118$>>8
   1B43 1B                 7535 	.db	00110$>>8
   1B44 1B                 7536 	.db	00111$>>8
   1B45 1B                 7537 	.db	00112$>>8
   1B46 1B                 7538 	.db	00119$>>8
   1B47 1B                 7539 	.db	00109$>>8
   1B48 1B                 7540 	.db	00120$>>8
   1B49 1B                 7541 	.db	00115$>>8
   1B4A 1B                 7542 	.db	00121$>>8
   1B4B 1B                 7543 	.db	00116$>>8
   1B4C 1B                 7544 	.db	00122$>>8
   1B4D 1B                 7545 	.db	00122$>>8
   1B4E 1B                 7546 	.db	00113$>>8
   1B4F 1B                 7547 	.db	00122$>>8
   1B50 1B                 7548 	.db	00114$>>8
   1B51 1B                 7549 	.db	00122$>>8
   1B52 1B                 7550 	.db	00105$>>8
   1B53 1B                 7551 	.db	00106$>>8
   1B54 1B                 7552 	.db	00117$>>8
   1B55 1B                 7553 	.db	00122$>>8
   1B56 1B                 7554 	.db	00122$>>8
   1B57 1B                 7555 	.db	00104$>>8
   1B58 1B                 7556 	.db	00108$>>8
                           7557 ;	main.c:1189: case '0' : putstr("The clock is paused\n\r");
   1B59                    7558 00101$:
                           7559 ;	genCall
                           7560 ;	Peephole 182.a	used 16 bit load of DPTR
   1B59 90 2E 23           7561 	mov	dptr,#__str_71
   1B5C 75 F0 80           7562 	mov	b,#0x80
   1B5F 12 00 AA           7563 	lcall	_putstr
                           7564 ;	main.c:1190: CLOCK_ENABLE_FLAG=0;
                           7565 ;	genAssign
   1B62 90 00 BC           7566 	mov	dptr,#_CLOCK_ENABLE_FLAG
                           7567 ;	Peephole 181	changed mov to clr
   1B65 E4                 7568 	clr	a
   1B66 F0                 7569 	movx	@dptr,a
                           7570 ;	main.c:1191: break;
   1B67 02 1A D7           7571 	ljmp	00125$
                           7572 ;	main.c:1192: case '1' : putstr("The clock is resuming\n\r");
   1B6A                    7573 00102$:
                           7574 ;	genCall
                           7575 ;	Peephole 182.a	used 16 bit load of DPTR
   1B6A 90 2E 39           7576 	mov	dptr,#__str_72
   1B6D 75 F0 80           7577 	mov	b,#0x80
   1B70 12 00 AA           7578 	lcall	_putstr
                           7579 ;	main.c:1193: CLOCK_ENABLE_FLAG=1;
                           7580 ;	genAssign
   1B73 90 00 BC           7581 	mov	dptr,#_CLOCK_ENABLE_FLAG
   1B76 74 01              7582 	mov	a,#0x01
   1B78 F0                 7583 	movx	@dptr,a
                           7584 ;	main.c:1194: break;
   1B79 02 1A D7           7585 	ljmp	00125$
                           7586 ;	main.c:1195: case '2' : putstr("Clock is reset to zero\n\r");
   1B7C                    7587 00103$:
                           7588 ;	genCall
                           7589 ;	Peephole 182.a	used 16 bit load of DPTR
   1B7C 90 2E 51           7590 	mov	dptr,#__str_73
   1B7F 75 F0 80           7591 	mov	b,#0x80
   1B82 12 00 AA           7592 	lcall	_putstr
                           7593 ;	main.c:1196: clock_reset();
                           7594 ;	genCall
   1B85 12 19 7B           7595 	lcall	_clock_reset
                           7596 ;	main.c:1197: break;
   1B88 02 1A D7           7597 	ljmp	00125$
                           7598 ;	main.c:1198: case 'W' : write_function();
   1B8B                    7599 00104$:
                           7600 ;	genCall
   1B8B 12 11 70           7601 	lcall	_write_function
                           7602 ;	main.c:1199: break;
   1B8E 02 1A D7           7603 	ljmp	00125$
                           7604 ;	main.c:1200: case 'R' : read_function();
   1B91                    7605 00105$:
                           7606 ;	genCall
   1B91 12 12 E6           7607 	lcall	_read_function
                           7608 ;	main.c:1201: break;
   1B94 02 1A D7           7609 	ljmp	00125$
                           7610 ;	main.c:1202: case 'S' : readall_function();
   1B97                    7611 00106$:
                           7612 ;	genCall
   1B97 12 0F 36           7613 	lcall	_readall_function
                           7614 ;	main.c:1203: break;
   1B9A 02 1A D7           7615 	ljmp	00125$
                           7616 ;	main.c:1204: case '>' : help_menu();
   1B9D                    7617 00107$:
                           7618 ;	genCall
   1B9D 12 11 FC           7619 	lcall	_help_menu
                           7620 ;	main.c:1205: break;
   1BA0 02 1A D7           7621 	ljmp	00125$
                           7622 ;	main.c:1206: case 'X' : gotoxy();
   1BA3                    7623 00108$:
                           7624 ;	genCall
   1BA3 12 0B F7           7625 	lcall	_gotoxy
                           7626 ;	main.c:1207: break;
   1BA6 02 1A D7           7627 	ljmp	00125$
                           7628 ;	main.c:1208: case 'G' : gotoaddress();
   1BA9                    7629 00109$:
                           7630 ;	genCall
   1BA9 12 0C 63           7631 	lcall	_gotoaddress
                           7632 ;	main.c:1209: break;
   1BAC 02 1A D7           7633 	ljmp	00125$
                           7634 ;	main.c:1210: case 'C' : cgramdump();
   1BAF                    7635 00110$:
                           7636 ;	genCall
   1BAF 12 03 C9           7637 	lcall	_cgramdump
                           7638 ;	main.c:1211: break;
   1BB2 02 1A D7           7639 	ljmp	00125$
                           7640 ;	main.c:1212: case 'D' : ddramdump();
   1BB5                    7641 00111$:
                           7642 ;	genCall
   1BB5 12 05 CE           7643 	lcall	_ddramdump
                           7644 ;	main.c:1213: break;
   1BB8 02 1A D7           7645 	ljmp	00125$
                           7646 ;	main.c:1214: case 'E' : lcdclear();
   1BBB                    7647 00112$:
                           7648 ;	genCall
   1BBB 12 02 67           7649 	lcall	_lcdclear
                           7650 ;	main.c:1215: break;
   1BBE 02 1A D7           7651 	ljmp	00125$
                           7652 ;	main.c:1216: case 'N' : addcharacter();
   1BC1                    7653 00113$:
                           7654 ;	genCall
   1BC1 12 07 B3           7655 	lcall	_addcharacter
                           7656 ;	main.c:1217: break;
   1BC4 02 1A D7           7657 	ljmp	00125$
                           7658 ;	main.c:1218: case 'P' : display();
   1BC7                    7659 00114$:
                           7660 ;	genCall
   1BC7 12 09 28           7661 	lcall	_display
                           7662 ;	main.c:1219: break;
   1BCA 02 1A D7           7663 	ljmp	00125$
                           7664 ;	main.c:1220: case 'I' : interactive();
   1BCD                    7665 00115$:
                           7666 ;	genCall
   1BCD 12 0B B8           7667 	lcall	_interactive
                           7668 ;	main.c:1221: break;
   1BD0 02 1A D7           7669 	ljmp	00125$
                           7670 ;	main.c:1222: case 'K' :  u=0;
   1BD3                    7671 00116$:
                           7672 ;	genAssign
   1BD3 90 00 0A           7673 	mov	dptr,#_u
                           7674 ;	Peephole 181	changed mov to clr
   1BD6 E4                 7675 	clr	a
   1BD7 F0                 7676 	movx	@dptr,a
                           7677 ;	main.c:1223: custom_character();
                           7678 ;	genCall
   1BD8 12 13 21           7679 	lcall	_custom_character
                           7680 ;	main.c:1224: break;
   1BDB 02 1A D7           7681 	ljmp	00125$
                           7682 ;	main.c:1225: case 'T' : eereset();
   1BDE                    7683 00117$:
                           7684 ;	genCall
   1BDE 12 19 99           7685 	lcall	_eereset
                           7686 ;	main.c:1226: break;
   1BE1 02 1A D7           7687 	ljmp	00125$
                           7688 ;	main.c:1227: case 'B' : ioexpander_writedata();
   1BE4                    7689 00118$:
                           7690 ;	genCall
   1BE4 12 19 B6           7691 	lcall	_ioexpander_writedata
                           7692 ;	main.c:1228: break;
   1BE7 02 1A D7           7693 	ljmp	00125$
                           7694 ;	main.c:1229: case 'F' : ioexpander_readdata();
   1BEA                    7695 00119$:
                           7696 ;	genCall
   1BEA 12 19 FB           7697 	lcall	_ioexpander_readdata
                           7698 ;	main.c:1230: break;
   1BED 02 1A D7           7699 	ljmp	00125$
                           7700 ;	main.c:1231: case 'H' : ioexpander_config();
   1BF0                    7701 00120$:
                           7702 ;	genCall
   1BF0 12 1A 2F           7703 	lcall	_ioexpander_config
                           7704 ;	main.c:1232: break;
   1BF3 02 1A D7           7705 	ljmp	00125$
                           7706 ;	main.c:1233: case 'J' : IOexpInit();
   1BF6                    7707 00121$:
                           7708 ;	genCall
   1BF6 12 19 AB           7709 	lcall	_IOexpInit
                           7710 ;	main.c:1234: break;
   1BF9 02 1A D7           7711 	ljmp	00125$
                           7712 ;	main.c:1235: default: putstr("Enter the correct option\n\r");
   1BFC                    7713 00122$:
                           7714 ;	genCall
                           7715 ;	Peephole 182.a	used 16 bit load of DPTR
   1BFC 90 2E 6A           7716 	mov	dptr,#__str_74
   1BFF 75 F0 80           7717 	mov	b,#0x80
   1C02 12 00 AA           7718 	lcall	_putstr
                           7719 ;	main.c:1236: }
   1C05 02 1A D7           7720 	ljmp	00125$
                           7721 ;	Peephole 259.b	removed redundant label 00127$ and ret
                           7722 ;
                           7723 	.area CSEG    (CODE)
                           7724 	.area CONST   (CODE)
   254C                    7725 __str_0:
   254C 0A                 7726 	.db 0x0A
   254D 0D                 7727 	.db 0x0D
   254E 45 6E 74 65 72 20  7728 	.ascii "Enter Numbers"
        4E 75 6D 62 65 72
        73
   255B 0A                 7729 	.db 0x0A
   255C 0D                 7730 	.db 0x0D
   255D 00                 7731 	.db 0x00
   255E                    7732 __str_1:
   255E 0A                 7733 	.db 0x0A
   255F 0D                 7734 	.db 0x0D
   2560 43 61 6E 27 74 20  7735 	.ascii "Can't move to given position"
        6D 6F 76 65 20 74
        6F 20 67 69 76 65
        6E 20 70 6F 73 69
        74 69 6F 6E
   257C 0A                 7736 	.db 0x0A
   257D 0D                 7737 	.db 0x0D
   257E 00                 7738 	.db 0x00
   257F                    7739 __str_2:
   257F 43 6C 65 61 72 69  7740 	.ascii "Clearing the LCD completed"
        6E 67 20 74 68 65
        20 4C 43 44 20 63
        6F 6D 70 6C 65 74
        65 64
   2599 0A                 7741 	.db 0x0A
   259A 0D                 7742 	.db 0x0D
   259B 00                 7743 	.db 0x00
   259C                    7744 __str_3:
   259C 0A                 7745 	.db 0x0A
   259D 0D                 7746 	.db 0x0D
   259E 00                 7747 	.db 0x00
   259F                    7748 __str_4:
   259F 43 47 52 41 4D 20  7749 	.ascii "CGRAM values"
        76 61 6C 75 65 73
   25AB 0A                 7750 	.db 0x0A
   25AC 0D                 7751 	.db 0x0D
   25AD 00                 7752 	.db 0x00
   25AE                    7753 __str_5:
   25AE 0A                 7754 	.db 0x0A
   25AF 0D                 7755 	.db 0x0D
   25B0 43 43 4F 44 45 20  7756 	.ascii "CCODE |  Values"
        7C 20 20 56 61 6C
        75 65 73
   25BF 0A                 7757 	.db 0x0A
   25C0 0D                 7758 	.db 0x0D
   25C1 00                 7759 	.db 0x00
   25C2                    7760 __str_6:
   25C2 30 78 25 30 34 78  7761 	.ascii "0x%04x:  "
        3A 20 20
   25CB 00                 7762 	.db 0x00
   25CC                    7763 __str_7:
   25CC 25 30 32 78 20 20  7764 	.ascii "%02x  "
   25D2 00                 7765 	.db 0x00
   25D3                    7766 __str_8:
   25D3 44 44 52 41 4D 20  7767 	.ascii "DDRAM Values"
        56 61 6C 75 65 73
   25DF 0A                 7768 	.db 0x0A
   25E0 0D                 7769 	.db 0x0D
   25E1 00                 7770 	.db 0x00
   25E2                    7771 __str_9:
   25E2 0A                 7772 	.db 0x0A
   25E3 0D                 7773 	.db 0x0D
   25E4 41 44 44 52 7C 20  7774 	.ascii "ADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C "
        20 2B 30 20 20 2B
        31 20 20 2B 32 20
        20 2B 33 20 20 2B
        34 20 20 2B 35 20
        20 2B 36 20 20 2B
        37 20 20 2B 38 20
        20 2B 39 20 20 2B
        41 20 20 2B 42 20
        20 2B 43 20
   261E 20 2B 44 20 20 2B  7775 	.ascii " +D  +E  +F"
        45 20 20 2B 46
   2629 0A                 7776 	.db 0x0A
   262A 0D                 7777 	.db 0x0D
   262B 0A                 7778 	.db 0x0A
   262C 0D                 7779 	.db 0x0D
   262D 00                 7780 	.db 0x00
   262E                    7781 __str_10:
   262E 30 78 25 30 32 78  7782 	.ascii "0x%02x:  "
        3A 20 20
   2637 00                 7783 	.db 0x00
   2638                    7784 __str_11:
   2638 0A                 7785 	.db 0x0A
   2639 0D                 7786 	.db 0x0D
   263A 45 6E 74 65 72 20  7787 	.ascii "Enter Valid Numbers"
        56 61 6C 69 64 20
        4E 75 6D 62 65 72
        73
   264D 0A                 7788 	.db 0x0A
   264E 0D                 7789 	.db 0x0D
   264F 00                 7790 	.db 0x00
   2650                    7791 __str_12:
   2650 0A                 7792 	.db 0x0A
   2651 0D                 7793 	.db 0x0D
   2652 45 6E 74 65 72 20  7794 	.ascii "Enter eight pixel values"
        65 69 67 68 74 20
        70 69 78 65 6C 20
        76 61 6C 75 65 73
   266A 0A                 7795 	.db 0x0A
   266B 0D                 7796 	.db 0x0D
   266C 00                 7797 	.db 0x00
   266D                    7798 __str_13:
   266D 45 6E 74 65 72 20  7799 	.ascii "Enter five binary values for every pixel"
        66 69 76 65 20 62
        69 6E 61 72 79 20
        76 61 6C 75 65 73
        20 66 6F 72 20 65
        76 65 72 79 20 70
        69 78 65 6C
   2695 0A                 7800 	.db 0x0A
   2696 0D                 7801 	.db 0x0D
   2697 00                 7802 	.db 0x00
   2698                    7803 __str_14:
   2698 49 6E 76 61 6C 69  7804 	.ascii "Invalid Number. Enter Again"
        64 20 4E 75 6D 62
        65 72 2E 20 45 6E
        74 65 72 20 41 67
        61 69 6E
   26B3 0A                 7805 	.db 0x0A
   26B4 0D                 7806 	.db 0x0D
   26B5 00                 7807 	.db 0x00
   26B6                    7808 __str_15:
   26B6 45 69 67 68 74 20  7809 	.ascii "Eight custom character created. No more custom character ple"
        63 75 73 74 6F 6D
        20 63 68 61 72 61
        63 74 65 72 20 63
        72 65 61 74 65 64
        2E 20 4E 6F 20 6D
        6F 72 65 20 63 75
        73 74 6F 6D 20 63
        68 61 72 61 63 74
        65 72 20 70 6C 65
   26F2 61 73 65           7810 	.ascii "ase"
   26F5 0A                 7811 	.db 0x0A
   26F6 0D                 7812 	.db 0x0D
   26F7 00                 7813 	.db 0x00
   26F8                    7814 __str_16:
   26F8 4C 6F 6F 6B 20 61  7815 	.ascii "Look at the LCD"
        74 20 74 68 65 20
        4C 43 44
   2707 0A                 7816 	.db 0x0A
   2708 0D                 7817 	.db 0x0D
   2709 00                 7818 	.db 0x00
   270A                    7819 __str_18:
   270A 45 6E 74 65 72 20  7820 	.ascii "Enter the character to display. Hit enter to stop"
        74 68 65 20 63 68
        61 72 61 63 74 65
        72 20 74 6F 20 64
        69 73 70 6C 61 79
        2E 20 48 69 74 20
        65 6E 74 65 72 20
        74 6F 20 73 74 6F
        70
   273B 0A                 7821 	.db 0x0A
   273C 0D                 7822 	.db 0x0D
   273D 00                 7823 	.db 0x00
   273E                    7824 __str_19:
   273E 0A                 7825 	.db 0x0A
   273F 0D                 7826 	.db 0x0D
   2740 45 6E 74 65 72 20  7827 	.ascii "Enter row number"
        72 6F 77 20 6E 75
        6D 62 65 72
   2750 0A                 7828 	.db 0x0A
   2751 0D                 7829 	.db 0x0D
   2752 00                 7830 	.db 0x00
   2753                    7831 __str_20:
   2753 0A                 7832 	.db 0x0A
   2754 0D                 7833 	.db 0x0D
   2755 45 6E 74 65 72 20  7834 	.ascii "Enter correct row"
        63 6F 72 72 65 63
        74 20 72 6F 77
   2766 0A                 7835 	.db 0x0A
   2767 0D                 7836 	.db 0x0D
   2768 00                 7837 	.db 0x00
   2769                    7838 __str_21:
   2769 0A                 7839 	.db 0x0A
   276A 0D                 7840 	.db 0x0D
   276B 45 6E 74 65 72 20  7841 	.ascii "Enter column number"
        63 6F 6C 75 6D 6E
        20 6E 75 6D 62 65
        72
   277E 0A                 7842 	.db 0x0A
   277F 0D                 7843 	.db 0x0D
   2780 00                 7844 	.db 0x00
   2781                    7845 __str_22:
   2781 0A                 7846 	.db 0x0A
   2782 0D                 7847 	.db 0x0D
   2783 45 6E 74 65 72 20  7848 	.ascii "Enter correct column number"
        63 6F 72 72 65 63
        74 20 63 6F 6C 75
        6D 6E 20 6E 75 6D
        62 65 72
   279E 0A                 7849 	.db 0x0A
   279F 0D                 7850 	.db 0x0D
   27A0 00                 7851 	.db 0x00
   27A1                    7852 __str_23:
   27A1 45 6E 74 65 72 20  7853 	.ascii "Enter the address in hex"
        74 68 65 20 61 64
        64 72 65 73 73 20
        69 6E 20 68 65 78
   27B9 0A                 7854 	.db 0x0A
   27BA 0D                 7855 	.db 0x0D
   27BB 00                 7856 	.db 0x00
   27BC                    7857 __str_24:
   27BC 0A                 7858 	.db 0x0A
   27BD 0D                 7859 	.db 0x0D
   27BE 45 6E 74 65 72 20  7860 	.ascii "Enter correct address values in hex"
        63 6F 72 72 65 63
        74 20 61 64 64 72
        65 73 73 20 76 61
        6C 75 65 73 20 69
        6E 20 68 65 78
   27E1 0A                 7861 	.db 0x0A
   27E2 0D                 7862 	.db 0x0D
   27E3 00                 7863 	.db 0x00
   27E4                    7864 __str_25:
   27E4 0A                 7865 	.db 0x0A
   27E5 0D                 7866 	.db 0x0D
   27E6 57 72 69 74 69 6E  7867 	.ascii "Writing 0x%03X: 0x%02X"
        67 20 30 78 25 30
        33 58 3A 20 30 78
        25 30 32 58
   27FC 0A                 7868 	.db 0x0A
   27FD 0D                 7869 	.db 0x0D
   27FE 00                 7870 	.db 0x00
   27FF                    7871 __str_26:
   27FF 0A                 7872 	.db 0x0A
   2800 0D                 7873 	.db 0x0D
   2801 30 78 25 30 33 58  7874 	.ascii "0x%03X: 0x%02X"
        3A 20 30 78 25 30
        32 58
   280F 0A                 7875 	.db 0x0A
   2810 0D                 7876 	.db 0x0D
   2811 00                 7877 	.db 0x00
   2812                    7878 __str_27:
   2812 45 6E 74 65 72 20  7879 	.ascii "Enter the address"
        74 68 65 20 61 64
        64 72 65 73 73
   2823 0A                 7880 	.db 0x0A
   2824 0D                 7881 	.db 0x0D
   2825 00                 7882 	.db 0x00
   2826                    7883 __str_28:
   2826 45 6E 74 65 72 20  7884 	.ascii "Enter the correct value"
        74 68 65 20 63 6F
        72 72 65 63 74 20
        76 61 6C 75 65
   283D 0A                 7885 	.db 0x0A
   283E 0D                 7886 	.db 0x0D
   283F 00                 7887 	.db 0x00
   2840                    7888 __str_29:
   2840 0A                 7889 	.db 0x0A
   2841 0D                 7890 	.db 0x0D
   2842 45 6E 74 65 72 20  7891 	.ascii "Enter the second higher address"
        74 68 65 20 73 65
        63 6F 6E 64 20 68
        69 67 68 65 72 20
        61 64 64 72 65 73
        73
   2861 0A                 7892 	.db 0x0A
   2862 0D                 7893 	.db 0x0D
   2863 00                 7894 	.db 0x00
   2864                    7895 __str_30:
   2864 45 45 50 52 4F 4D  7896 	.ascii "EEPROM data"
        20 64 61 74 61
   286F 0A                 7897 	.db 0x0A
   2870 0D                 7898 	.db 0x0D
   2871 00                 7899 	.db 0x00
   2872                    7900 __str_31:
   2872 41 44 44 52 20 7C  7901 	.ascii "ADDR |  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  "
        20 20 2B 30 20 20
        2B 31 20 20 2B 32
        20 20 2B 33 20 20
        2B 34 20 20 2B 35
        20 20 2B 36 20 20
        2B 37 20 20 2B 38
        20 20 2B 39 20 20
        2B 41 20 20 2B 42
        20 20 2B 43 20 20
   28AE 2B 44 20 20 2B 45  7902 	.ascii "+D  +E  +F"
        20 20 2B 46
   28B8 0A                 7903 	.db 0x0A
   28B9 0D                 7904 	.db 0x0D
   28BA 00                 7905 	.db 0x00
   28BB                    7906 __str_32:
   28BB 30 78 25 30 33 58  7907 	.ascii "0x%03X: "
        3A 20
   28C3 00                 7908 	.db 0x00
   28C4                    7909 __str_33:
   28C4 25 30 32 58 20 20  7910 	.ascii "%02X  "
   28CA 00                 7911 	.db 0x00
   28CB                    7912 __str_34:
   28CB 0A                 7913 	.db 0x0A
   28CC 0D                 7914 	.db 0x0D
   28CD 45 6E 74 65 72 20  7915 	.ascii "Enter the Value"
        74 68 65 20 56 61
        6C 75 65
   28DC 0A                 7916 	.db 0x0A
   28DD 0D                 7917 	.db 0x0D
   28DE 00                 7918 	.db 0x00
   28DF                    7919 __str_35:
   28DF 0A                 7920 	.db 0x0A
   28E0 0D                 7921 	.db 0x0D
   28E1 7E 7E 7E 7E 7E 7E  7922 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E
   291B 7E 7E 7E 7E 7E 7E  7923 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E
   293C 0A                 7924 	.db 0x0A
   293D 0D                 7925 	.db 0x0D
   293E 00                 7926 	.db 0x00
   293F                    7927 __str_36:
   293F 45 45 50 52 4F 4D  7928 	.ascii "EEPROM Control"
        20 43 6F 6E 74 72
        6F 6C
   294D 0A                 7929 	.db 0x0A
   294E 0D                 7930 	.db 0x0D
   294F 00                 7931 	.db 0x00
   2950                    7932 __str_37:
   2950 50 72 65 73 73 20  7933 	.ascii "Press 'R' to read a byte from the user input address"
        27 52 27 20 74 6F
        20 72 65 61 64 20
        61 20 62 79 74 65
        20 66 72 6F 6D 20
        74 68 65 20 75 73
        65 72 20 69 6E 70
        75 74 20 61 64 64
        72 65 73 73
   2984 0A                 7934 	.db 0x0A
   2985 0D                 7935 	.db 0x0D
   2986 00                 7936 	.db 0x00
   2987                    7937 __str_38:
   2987 50 72 65 73 73 20  7938 	.ascii "Press 'W' to write a byte data to user input address"
        27 57 27 20 74 6F
        20 77 72 69 74 65
        20 61 20 62 79 74
        65 20 64 61 74 61
        20 74 6F 20 75 73
        65 72 20 69 6E 70
        75 74 20 61 64 64
        72 65 73 73
   29BB 0A                 7939 	.db 0x0A
   29BC 0D                 7940 	.db 0x0D
   29BD 00                 7941 	.db 0x00
   29BE                    7942 __str_39:
   29BE 50 72 65 73 73 20  7943 	.ascii "Press 'S' for sequential read between the adresses"
        27 53 27 20 66 6F
        72 20 73 65 71 75
        65 6E 74 69 61 6C
        20 72 65 61 64 20
        62 65 74 77 65 65
        6E 20 74 68 65 20
        61 64 72 65 73 73
        65 73
   29F0 0A                 7944 	.db 0x0A
   29F1 0D                 7945 	.db 0x0D
   29F2 00                 7946 	.db 0x00
   29F3                    7947 __str_40:
   29F3 50 72 65 73 73 20  7948 	.ascii "Press 'T' for EEPROM software reset"
        27 54 27 20 66 6F
        72 20 45 45 50 52
        4F 4D 20 73 6F 66
        74 77 61 72 65 20
        72 65 73 65 74
   2A16 0A                 7949 	.db 0x0A
   2A17 0D                 7950 	.db 0x0D
   2A18 00                 7951 	.db 0x00
   2A19                    7952 __str_41:
   2A19 4C 43 44 20 43 6F  7953 	.ascii "LCD Control"
        6E 74 72 6F 6C
   2A24 0A                 7954 	.db 0x0A
   2A25 0D                 7955 	.db 0x0D
   2A26 00                 7956 	.db 0x00
   2A27                    7957 __str_42:
   2A27 50 72 65 73 73 20  7958 	.ascii "Press 'C' for CGRAM HEX dump"
        27 43 27 20 66 6F
        72 20 43 47 52 41
        4D 20 48 45 58 20
        64 75 6D 70
   2A43 0A                 7959 	.db 0x0A
   2A44 0D                 7960 	.db 0x0D
   2A45 00                 7961 	.db 0x00
   2A46                    7962 __str_43:
   2A46 50 72 65 73 73 20  7963 	.ascii "Press 'D' for DDRAM HEX dump"
        27 44 27 20 66 6F
        72 20 44 44 52 41
        4D 20 48 45 58 20
        64 75 6D 70
   2A62 0A                 7964 	.db 0x0A
   2A63 0D                 7965 	.db 0x0D
   2A64 00                 7966 	.db 0x00
   2A65                    7967 __str_44:
   2A65 50 72 65 73 73 20  7968 	.ascii "Press 'E' for Clearing the LCD"
        27 45 27 20 66 6F
        72 20 43 6C 65 61
        72 69 6E 67 20 74
        68 65 20 4C 43 44
   2A83 0A                 7969 	.db 0x0A
   2A84 0D                 7970 	.db 0x0D
   2A85 00                 7971 	.db 0x00
   2A86                    7972 __str_45:
   2A86 50 72 65 73 73 20  7973 	.ascii "Press 'P' to display the custom character"
        27 50 27 20 74 6F
        20 64 69 73 70 6C
        61 79 20 74 68 65
        20 63 75 73 74 6F
        6D 20 63 68 61 72
        61 63 74 65 72
   2AAF 0A                 7974 	.db 0x0A
   2AB0 0D                 7975 	.db 0x0D
   2AB1 00                 7976 	.db 0x00
   2AB2                    7977 __str_46:
   2AB2 50 72 65 73 73 20  7978 	.ascii "Press 'N' to add custom character"
        27 4E 27 20 74 6F
        20 61 64 64 20 63
        75 73 74 6F 6D 20
        63 68 61 72 61 63
        74 65 72
   2AD3 0A                 7979 	.db 0x0A
   2AD4 0D                 7980 	.db 0x0D
   2AD5 00                 7981 	.db 0x00
   2AD6                    7982 __str_47:
   2AD6 50 72 65 73 73 20  7983 	.ascii "Press 'I' for printing letters on LCD"
        27 49 27 20 66 6F
        72 20 70 72 69 6E
        74 69 6E 67 20 6C
        65 74 74 65 72 73
        20 6F 6E 20 4C 43
        44
   2AFB 0A                 7984 	.db 0x0A
   2AFC 0D                 7985 	.db 0x0D
   2AFD 00                 7986 	.db 0x00
   2AFE                    7987 __str_48:
   2AFE 50 72 65 73 73 20  7988 	.ascii "Press 'K' for displaying the logo"
        27 4B 27 20 66 6F
        72 20 64 69 73 70
        6C 61 79 69 6E 67
        20 74 68 65 20 6C
        6F 67 6F
   2B1F 0A                 7989 	.db 0x0A
   2B20 0D                 7990 	.db 0x0D
   2B21 00                 7991 	.db 0x00
   2B22                    7992 __str_49:
   2B22 50 72 65 73 73 20  7993 	.ascii "Press 'X' for moving the cursor to user input x,y position"
        27 58 27 20 66 6F
        72 20 6D 6F 76 69
        6E 67 20 74 68 65
        20 63 75 72 73 6F
        72 20 74 6F 20 75
        73 65 72 20 69 6E
        70 75 74 20 78 2C
        79 20 70 6F 73 69
        74 69 6F 6E
   2B5C 0A                 7994 	.db 0x0A
   2B5D 0D                 7995 	.db 0x0D
   2B5E 00                 7996 	.db 0x00
   2B5F                    7997 __str_50:
   2B5F 50 72 65 73 73 20  7998 	.ascii "Press 'G' for moving the cursor to user input address"
        27 47 27 20 66 6F
        72 20 6D 6F 76 69
        6E 67 20 74 68 65
        20 63 75 72 73 6F
        72 20 74 6F 20 75
        73 65 72 20 69 6E
        70 75 74 20 61 64
        64 72 65 73 73
   2B94 0A                 7999 	.db 0x0A
   2B95 0D                 8000 	.db 0x0D
   2B96 00                 8001 	.db 0x00
   2B97                    8002 __str_51:
   2B97 43 6C 6F 63 6B 20  8003 	.ascii "Clock Control"
        43 6F 6E 74 72 6F
        6C
   2BA4 0A                 8004 	.db 0x0A
   2BA5 0D                 8005 	.db 0x0D
   2BA6 00                 8006 	.db 0x00
   2BA7                    8007 __str_52:
   2BA7 50 72 65 73 73 20  8008 	.ascii "Press '0' for pausing the clock"
        27 30 27 20 66 6F
        72 20 70 61 75 73
        69 6E 67 20 74 68
        65 20 63 6C 6F 63
        6B
   2BC6 0A                 8009 	.db 0x0A
   2BC7 0D                 8010 	.db 0x0D
   2BC8 00                 8011 	.db 0x00
   2BC9                    8012 __str_53:
   2BC9 50 72 65 73 73 20  8013 	.ascii "Press '1' to resume the clock "
        27 31 27 20 74 6F
        20 72 65 73 75 6D
        65 20 74 68 65 20
        63 6C 6F 63 6B 20
   2BE7 0A                 8014 	.db 0x0A
   2BE8 0D                 8015 	.db 0x0D
   2BE9 00                 8016 	.db 0x00
   2BEA                    8017 __str_54:
   2BEA 50 72 65 73 73 20  8018 	.ascii "Press '2' to reset the clock to zero"
        27 32 27 20 74 6F
        20 72 65 73 65 74
        20 74 68 65 20 63
        6C 6F 63 6B 20 74
        6F 20 7A 65 72 6F
   2C0E 0A                 8019 	.db 0x0A
   2C0F 0D                 8020 	.db 0x0D
   2C10 00                 8021 	.db 0x00
   2C11                    8022 __str_55:
   2C11 49 4F 20 45 78 70  8023 	.ascii "IO Expander control"
        61 6E 64 65 72 20
        63 6F 6E 74 72 6F
        6C
   2C24 0A                 8024 	.db 0x0A
   2C25 0D                 8025 	.db 0x0D
   2C26 00                 8026 	.db 0x00
   2C27                    8027 __str_56:
   2C27 50 72 65 73 73 20  8028 	.ascii "Press 'B' for writing data to IO Expander"
        27 42 27 20 66 6F
        72 20 77 72 69 74
        69 6E 67 20 64 61
        74 61 20 74 6F 20
        49 4F 20 45 78 70
        61 6E 64 65 72
   2C50 0A                 8029 	.db 0x0A
   2C51 0D                 8030 	.db 0x0D
   2C52 00                 8031 	.db 0x00
   2C53                    8032 __str_57:
   2C53 50 72 65 73 73 20  8033 	.ascii "Press 'F' to read data from IO Expander"
        27 46 27 20 74 6F
        20 72 65 61 64 20
        64 61 74 61 20 66
        72 6F 6D 20 49 4F
        20 45 78 70 61 6E
        64 65 72
   2C7A 0A                 8034 	.db 0x0A
   2C7B 0D                 8035 	.db 0x0D
   2C7C 00                 8036 	.db 0x00
   2C7D                    8037 __str_58:
   2C7D 50 72 65 73 73 20  8038 	.ascii "Press 'H' for configuring the individual GPIO pin"
        27 48 27 20 66 6F
        72 20 63 6F 6E 66
        69 67 75 72 69 6E
        67 20 74 68 65 20
        69 6E 64 69 76 69
        64 75 61 6C 20 47
        50 49 4F 20 70 69
        6E
   2CAE 0A                 8039 	.db 0x0A
   2CAF 0D                 8040 	.db 0x0D
   2CB0 00                 8041 	.db 0x00
   2CB1                    8042 __str_59:
   2CB1 50 72 65 73 73 20  8043 	.ascii "Press 'J' for configuring IO expander's one pin as input and"
        27 4A 27 20 66 6F
        72 20 63 6F 6E 66
        69 67 75 72 69 6E
        67 20 49 4F 20 65
        78 70 61 6E 64 65
        72 27 73 20 6F 6E
        65 20 70 69 6E 20
        61 73 20 69 6E 70
        75 74 20 61 6E 64
   2CED 20 6F 6E 65 20 61  8044 	.ascii " one as output"
        73 20 6F 75 74 70
        75 74
   2CFB 0A                 8045 	.db 0x0A
   2CFC 0D                 8046 	.db 0x0D
   2CFD 00                 8047 	.db 0x00
   2CFE                    8048 __str_60:
   2CFE 4C 6F 6F 6B 20 61  8049 	.ascii "Look at LCD"
        74 20 4C 43 44
   2D09 0A                 8050 	.db 0x0A
   2D0A 0D                 8051 	.db 0x0D
   2D0B 00                 8052 	.db 0x00
   2D0C                    8053 __str_61:
   2D0C 30 30 3A 30 30 2E  8054 	.ascii "00:00.0"
        30
   2D13 00                 8055 	.db 0x00
   2D14                    8056 __str_62:
   2D14 45 6E 74 65 72 20  8057 	.ascii "Enter the value to write"
        74 68 65 20 76 61
        6C 75 65 20 74 6F
        20 77 72 69 74 65
   2D2C 0A                 8058 	.db 0x0A
   2D2D 0D                 8059 	.db 0x0D
   2D2E 00                 8060 	.db 0x00
   2D2F                    8061 __str_63:
   2D2F 45 6E 74 65 72 20  8062 	.ascii "Enter proper value"
        70 72 6F 70 65 72
        20 76 61 6C 75 65
   2D41 0A                 8063 	.db 0x0A
   2D42 0D                 8064 	.db 0x0D
   2D43 00                 8065 	.db 0x00
   2D44                    8066 __str_64:
   2D44 52 65 61 64 20 69  8067 	.ascii "Read input value from the IO Expander is "
        6E 70 75 74 20 76
        61 6C 75 65 20 66
        72 6F 6D 20 74 68
        65 20 49 4F 20 45
        78 70 61 6E 64 65
        72 20 69 73 20
   2D6D 00                 8068 	.db 0x00
   2D6E                    8069 __str_65:
   2D6E 30 78 25 32 78     8070 	.ascii "0x%2x"
   2D73 0A                 8071 	.db 0x0A
   2D74 0D                 8072 	.db 0x0D
   2D75 00                 8073 	.db 0x00
   2D76                    8074 __str_66:
   2D76 50 72 65 73 73 20  8075 	.ascii "Press 1 for input and 0 for output. Enter eight values for e"
        31 20 66 6F 72 20
        69 6E 70 75 74 20
        61 6E 64 20 30 20
        66 6F 72 20 6F 75
        74 70 75 74 2E 20
        45 6E 74 65 72 20
        65 69 67 68 74 20
        76 61 6C 75 65 73
        20 66 6F 72 20 65
   2DB2 69 67 68 74 20 70  8076 	.ascii "ight port pins and hit enter"
        6F 72 74 20 70 69
        6E 73 20 61 6E 64
        20 68 69 74 20 65
        6E 74 65 72
   2DCE 0A                 8077 	.db 0x0A
   2DCF 0D                 8078 	.db 0x0D
   2DD0 00                 8079 	.db 0x00
   2DD1                    8080 __str_67:
   2DD1 57 65 6C 63 6F 6D  8081 	.ascii "Welcome"
        65
   2DD8 0A                 8082 	.db 0x0A
   2DD9 0D                 8083 	.db 0x0D
   2DDA 00                 8084 	.db 0x00
   2DDB                    8085 __str_68:
   2DDB 50 72 65 73 73 20  8086 	.ascii "Press '>' for help menu"
        27 3E 27 20 66 6F
        72 20 68 65 6C 70
        20 6D 65 6E 75
   2DF2 0A                 8087 	.db 0x0A
   2DF3 0D                 8088 	.db 0x0D
   2DF4 00                 8089 	.db 0x00
   2DF5                    8090 __str_69:
   2DF5 45 6E 74 65 72 20  8091 	.ascii "Enter the character"
        74 68 65 20 63 68
        61 72 61 63 74 65
        72
   2E08 0A                 8092 	.db 0x0A
   2E09 0D                 8093 	.db 0x0D
   2E0A 00                 8094 	.db 0x00
   2E0B                    8095 __str_70:
   2E0B 0A                 8096 	.db 0x0A
   2E0C 0D                 8097 	.db 0x0D
   2E0D 45 6E 74 65 72 20  8098 	.ascii "Enter the character"
        74 68 65 20 63 68
        61 72 61 63 74 65
        72
   2E20 0A                 8099 	.db 0x0A
   2E21 0D                 8100 	.db 0x0D
   2E22 00                 8101 	.db 0x00
   2E23                    8102 __str_71:
   2E23 54 68 65 20 63 6C  8103 	.ascii "The clock is paused"
        6F 63 6B 20 69 73
        20 70 61 75 73 65
        64
   2E36 0A                 8104 	.db 0x0A
   2E37 0D                 8105 	.db 0x0D
   2E38 00                 8106 	.db 0x00
   2E39                    8107 __str_72:
   2E39 54 68 65 20 63 6C  8108 	.ascii "The clock is resuming"
        6F 63 6B 20 69 73
        20 72 65 73 75 6D
        69 6E 67
   2E4E 0A                 8109 	.db 0x0A
   2E4F 0D                 8110 	.db 0x0D
   2E50 00                 8111 	.db 0x00
   2E51                    8112 __str_73:
   2E51 43 6C 6F 63 6B 20  8113 	.ascii "Clock is reset to zero"
        69 73 20 72 65 73
        65 74 20 74 6F 20
        7A 65 72 6F
   2E67 0A                 8114 	.db 0x0A
   2E68 0D                 8115 	.db 0x0D
   2E69 00                 8116 	.db 0x00
   2E6A                    8117 __str_74:
   2E6A 45 6E 74 65 72 20  8118 	.ascii "Enter the correct option"
        74 68 65 20 63 6F
        72 72 65 63 74 20
        6F 70 74 69 6F 6E
   2E82 0A                 8119 	.db 0x0A
   2E83 0D                 8120 	.db 0x0D
   2E84 00                 8121 	.db 0x00
                           8122 	.area XINIT   (CODE)
   2E90                    8123 __xinit__CLOCK_ENABLE_FLAG:
   2E90 01                 8124 	.db #0x01
   2E91                    8125 __xinit__confirm:
   2E91 01 00              8126 	.byte #0x01,#0x00
