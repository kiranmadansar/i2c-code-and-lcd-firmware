;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.6.0 #4309 (Jul 28 2006)
; This file generated Fri Nov 10 15:54:57 2017
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _custom_character
	.globl _read_function
	.globl _help_menu
	.globl _write_function
	.globl _readall_function
	.globl _eebyter
	.globl _eebytew
	.globl _clock_func
	.globl _send_data
	.globl _send_data_assembly
	.globl _Master_ack
	.globl _ackbit
	.globl _stopbit
	.globl _startbit
	.globl _SerialInitialize
	.globl _delay
	.globl __sdcc_external_startup
	.globl _gotoaddress
	.globl _gotoxy
	.globl _interactive
	.globl _ReadVariable
	.globl _hex2int
	.globl _display
	.globl _addcharacter
	.globl _readinput
	.globl _ddramdump
	.globl _lcdcreatechar
	.globl _cgramdump
	.globl _lcdinit
	.globl _lcdputstr
	.globl _lcdputch
	.globl _lcdclear
	.globl _lcdgotoxy
	.globl _lcdgotoaddr
	.globl _input_value
	.globl _lcdbusywait
	.globl _MSDelay
	.globl _P5_7
	.globl _P5_6
	.globl _P5_5
	.globl _P5_4
	.globl _P5_3
	.globl _P5_2
	.globl _P5_1
	.globl _P5_0
	.globl _P4_7
	.globl _P4_6
	.globl _P4_5
	.globl _P4_4
	.globl _P4_3
	.globl _P4_2
	.globl _P4_1
	.globl _P4_0
	.globl _PX0L
	.globl _PT0L
	.globl _PX1L
	.globl _PT1L
	.globl _PLS
	.globl _PT2L
	.globl _PPCL
	.globl _EC
	.globl _CCF0
	.globl _CCF1
	.globl _CCF2
	.globl _CCF3
	.globl _CCF4
	.globl _CR
	.globl _CF
	.globl _TF2
	.globl _EXF2
	.globl _RCLK
	.globl _TCLK
	.globl _EXEN2
	.globl _TR2
	.globl _C_T2
	.globl _CP_RL2
	.globl _T2CON_7
	.globl _T2CON_6
	.globl _T2CON_5
	.globl _T2CON_4
	.globl _T2CON_3
	.globl _T2CON_2
	.globl _T2CON_1
	.globl _T2CON_0
	.globl _PT2
	.globl _ET2
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _PS
	.globl _PT1
	.globl _PX1
	.globl _PT0
	.globl _PX0
	.globl _RD
	.globl _WR
	.globl _T1
	.globl _T0
	.globl _INT1
	.globl _INT0
	.globl _TXD
	.globl _RXD
	.globl _P3_7
	.globl _P3_6
	.globl _P3_5
	.globl _P3_4
	.globl _P3_3
	.globl _P3_2
	.globl _P3_1
	.globl _P3_0
	.globl _EA
	.globl _ES
	.globl _ET1
	.globl _EX1
	.globl _ET0
	.globl _EX0
	.globl _P2_7
	.globl _P2_6
	.globl _P2_5
	.globl _P2_4
	.globl _P2_3
	.globl _P2_2
	.globl _P2_1
	.globl _P2_0
	.globl _SM0
	.globl _SM1
	.globl _SM2
	.globl _REN
	.globl _TB8
	.globl _RB8
	.globl _TI
	.globl _RI
	.globl _P1_7
	.globl _P1_6
	.globl _P1_5
	.globl _P1_4
	.globl _P1_3
	.globl _P1_2
	.globl _P1_1
	.globl _P1_0
	.globl _TF1
	.globl _TR1
	.globl _TF0
	.globl _TR0
	.globl _IE1
	.globl _IT1
	.globl _IE0
	.globl _IT0
	.globl _P0_7
	.globl _P0_6
	.globl _P0_5
	.globl _P0_4
	.globl _P0_3
	.globl _P0_2
	.globl _P0_1
	.globl _P0_0
	.globl _EECON
	.globl _KBF
	.globl _KBE
	.globl _KBLS
	.globl _BRL
	.globl _BDRCON
	.globl _T2MOD
	.globl _SPDAT
	.globl _SPSTA
	.globl _SPCON
	.globl _SADEN
	.globl _SADDR
	.globl _WDTPRG
	.globl _WDTRST
	.globl _P5
	.globl _P4
	.globl _IPH1
	.globl _IPL1
	.globl _IPH0
	.globl _IPL0
	.globl _IEN1
	.globl _IEN0
	.globl _CMOD
	.globl _CL
	.globl _CH
	.globl _CCON
	.globl _CCAPM4
	.globl _CCAPM3
	.globl _CCAPM2
	.globl _CCAPM1
	.globl _CCAPM0
	.globl _CCAP4L
	.globl _CCAP3L
	.globl _CCAP2L
	.globl _CCAP1L
	.globl _CCAP0L
	.globl _CCAP4H
	.globl _CCAP3H
	.globl _CCAP2H
	.globl _CCAP1H
	.globl _CCAP0H
	.globl _CKCKON1
	.globl _CKCKON0
	.globl _CKRL
	.globl _AUXR1
	.globl _AUXR
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T2CON
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _IP
	.globl _P3
	.globl _IE
	.globl _P2
	.globl _SBUF
	.globl _SCON
	.globl _P1
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _P0
	.globl _confirm
	.globl _eebytew_PARM_2
	.globl _hex2int_PARM_2
	.globl _lcdcreatechar_PARM_2
	.globl _lcdgotoxy_PARM_2
	.globl _u
	.globl _array
	.globl _rec
	.globl _t
	.globl _putstr
	.globl _putchar
	.globl _getchar
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (DATA)
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
_T2CON	=	0x00c8
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_TL2	=	0x00cc
_TH2	=	0x00cd
_AUXR	=	0x008e
_AUXR1	=	0x00a2
_CKRL	=	0x0097
_CKCKON0	=	0x008f
_CKCKON1	=	0x008f
_CCAP0H	=	0x00fa
_CCAP1H	=	0x00fb
_CCAP2H	=	0x00fc
_CCAP3H	=	0x00fd
_CCAP4H	=	0x00fe
_CCAP0L	=	0x00ea
_CCAP1L	=	0x00eb
_CCAP2L	=	0x00ec
_CCAP3L	=	0x00ed
_CCAP4L	=	0x00ee
_CCAPM0	=	0x00da
_CCAPM1	=	0x00db
_CCAPM2	=	0x00dc
_CCAPM3	=	0x00dd
_CCAPM4	=	0x00de
_CCON	=	0x00d8
_CH	=	0x00f9
_CL	=	0x00e9
_CMOD	=	0x00d9
_IEN0	=	0x00a8
_IEN1	=	0x00b1
_IPL0	=	0x00b8
_IPH0	=	0x00b7
_IPL1	=	0x00b2
_IPH1	=	0x00b3
_P4	=	0x00c0
_P5	=	0x00d8
_WDTRST	=	0x00a6
_WDTPRG	=	0x00a7
_SADDR	=	0x00a9
_SADEN	=	0x00b9
_SPCON	=	0x00c3
_SPSTA	=	0x00c4
_SPDAT	=	0x00c5
_T2MOD	=	0x00c9
_BDRCON	=	0x009b
_BRL	=	0x009a
_KBLS	=	0x009c
_KBE	=	0x009d
_KBF	=	0x009e
_EECON	=	0x00d2
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (DATA)
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
_ET2	=	0x00ad
_PT2	=	0x00bd
_T2CON_0	=	0x00c8
_T2CON_1	=	0x00c9
_T2CON_2	=	0x00ca
_T2CON_3	=	0x00cb
_T2CON_4	=	0x00cc
_T2CON_5	=	0x00cd
_T2CON_6	=	0x00ce
_T2CON_7	=	0x00cf
_CP_RL2	=	0x00c8
_C_T2	=	0x00c9
_TR2	=	0x00ca
_EXEN2	=	0x00cb
_TCLK	=	0x00cc
_RCLK	=	0x00cd
_EXF2	=	0x00ce
_TF2	=	0x00cf
_CF	=	0x00df
_CR	=	0x00de
_CCF4	=	0x00dc
_CCF3	=	0x00db
_CCF2	=	0x00da
_CCF1	=	0x00d9
_CCF0	=	0x00d8
_EC	=	0x00ae
_PPCL	=	0x00be
_PT2L	=	0x00bd
_PLS	=	0x00bc
_PT1L	=	0x00bb
_PX1L	=	0x00ba
_PT0L	=	0x00b9
_PX0L	=	0x00b8
_P4_0	=	0x00c0
_P4_1	=	0x00c1
_P4_2	=	0x00c2
_P4_3	=	0x00c3
_P4_4	=	0x00c4
_P4_5	=	0x00c5
_P4_6	=	0x00c6
_P4_7	=	0x00c7
_P5_0	=	0x00d8
_P5_1	=	0x00d9
_P5_2	=	0x00da
_P5_3	=	0x00db
_P5_4	=	0x00dc
_P5_5	=	0x00dd
_P5_6	=	0x00de
_P5_7	=	0x00df
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_lcdcreatechar_sloc0_1_0:
	.ds 2
_hex2int_sloc0_1_0:
	.ds 1
_hex2int_sloc1_1_0:
	.ds 2
_hex2int_sloc2_1_0:
	.ds 4
_hex2int_sloc3_1_0:
	.ds 4
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_t::
	.ds 2
_rec::
	.ds 1
_array::
	.ds 8
_u::
	.ds 1
_MSDelay_value_1_1:
	.ds 2
_lcdbusywait_i_1_1:
	.ds 2
_input_value_l_1_1:
	.ds 1
_input_value_m_1_1:
	.ds 1
_lcdgotoaddr_addr_1_1:
	.ds 1
_lcdgotoxy_PARM_2:
	.ds 1
_lcdgotoxy_row_1_1:
	.ds 1
_lcdputch_cc_1_1:
	.ds 1
_lcdputch_i_1_1:
	.ds 2
_lcdputch_temp_1_1:
	.ds 2
_putstr_ptr_1_1:
	.ds 3
_lcdputstr_ptr_1_1:
	.ds 3
_cgramdump_temp_val_1_1:
	.ds 1
_lcdcreatechar_PARM_2:
	.ds 3
_lcdcreatechar_ccode_1_1:
	.ds 1
_ddramdump_temp_val_1_1:
	.ds 1
_readinput_value_1_1:
	.ds 1
_addcharacter_temp_1_1:
	.ds 1
_hex2int_PARM_2:
	.ds 2
_hex2int_a_1_1:
	.ds 3
_hex2int_val_1_1:
	.ds 4
_ReadVariable_ptr_1_1:
	.ds 1
_gotoaddress_temp_1_1:
	.ds 1
_putchar_x_1_1:
	.ds 1
_send_data_value_1_1:
	.ds 1
_eebytew_PARM_2:
	.ds 1
_eebytew_addr_1_1:
	.ds 2
_eebyter_addr_1_1:
	.ds 2
_eebyter_backup_1_1:
	.ds 1
_readall_function_backup_1_1:
	.ds 1
_readall_function_tempvalue_1_1:
	.ds 2
_custom_character_in_array_1_1:
	.ds 8
_custom_character_in_array5_1_1:
	.ds 8
_custom_character_in_array1_1_1:
	.ds 8
_custom_character_in_array2_1_1:
	.ds 8
_custom_character_in_array3_1_1:
	.ds 8
_custom_character_in_array4_1_1:
	.ds 8
_custom_character_in_array6_1_1:
	.ds 8
_custom_character_in_array7_1_1:
	.ds 8
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
_confirm::
	.ds 2
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'MSDelay'
;------------------------------------------------------------
;value                     Allocated with name '_MSDelay_value_1_1'
;x                         Allocated with name '_MSDelay_x_1_1'
;y                         Allocated with name '_MSDelay_y_1_1'
;------------------------------------------------------------
;	main.c:52: void MSDelay(unsigned int value)
;	-----------------------------------------
;	 function MSDelay
;	-----------------------------------------
_MSDelay:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_MSDelay_value_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:55: for(y=0;y<value;y++)
;	genAssign
	mov	dptr,#_MSDelay_value_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	r4,#0x00
	mov	r5,#0x00
00104$:
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,r2
	mov	a,r5
	subb	a,r3
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00108$
;	Peephole 300	removed redundant label 00116$
;	main.c:57: for(x=0;x<126;x++)
;	genAssign
	mov	r6,#0x7E
	mov	r7,#0x00
00103$:
;	genMinus
;	genMinusDec
	dec	r6
	cjne	r6,#0xff,00117$
	dec	r7
00117$:
;	genIfx
	mov	a,r6
	orl	a,r7
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00118$
;	main.c:55: for(y=0;y<value;y++)
;	genPlus
;     genPlusIncr
;	tail increment optimized (range 7)
	inc	r4
	cjne	r4,#0x00,00104$
	inc	r5
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00104$
00108$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdbusywait'
;------------------------------------------------------------
;i                         Allocated with name '_lcdbusywait_i_1_1'
;------------------------------------------------------------
;	main.c:67: void lcdbusywait()
;	-----------------------------------------
;	 function lcdbusywait
;	-----------------------------------------
_lcdbusywait:
;	main.c:70: i=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_lcdbusywait_i_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:71: MSDelay(1);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
	lcall	_MSDelay
;	main.c:72: while((i&0x80)==0x80)
00101$:
;	genAssign
	mov	dptr,#_lcdbusywait_i_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAnd
	anl	ar2,#0x80
	mov	r3,#0x00
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x80,00104$
	cjne	r3,#0x00,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:74: i = *INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_lcdbusywait_i_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:75: MSDelay(1);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
	lcall	_MSDelay
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'input_value'
;------------------------------------------------------------
;l                         Allocated with name '_input_value_l_1_1'
;m                         Allocated with name '_input_value_m_1_1'
;------------------------------------------------------------
;	main.c:80: unsigned char input_value()
;	-----------------------------------------
;	 function input_value
;	-----------------------------------------
_input_value:
;	main.c:82: unsigned volatile char l=0, m=0;
;	genAssign
	mov	dptr,#_input_value_l_1_1
;	Peephole 181	changed mov to clr
;	genAssign
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#_input_value_m_1_1
	movx	@dptr,a
;	main.c:83: do
00110$:
;	main.c:85: l=getchar();
;	genCall
	lcall	_getchar
	mov	a,dpl
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	@dptr,a
;	main.c:86: if(l>='0' && l<='9')
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x30,00121$
00121$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00122$
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x39
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00123$
;	main.c:88: m=((m*10)+(l-'0'));
;	genAssign
	mov	dptr,#_input_value_m_1_1
	movx	a,@dptr
;	genMult
;	genMultOneByte
	mov	r2,a
;	Peephole 105	removed redundant mov
	mov	b,#0x0A
	mul	ab
	mov	r2,a
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
;	genMinus
	mov	r3,a
;	Peephole 105	removed redundant mov
	add	a,#0xd0
;	genPlus
	mov	dptr,#_input_value_m_1_1
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	main.c:89: putchar(l);                 /* CONVERTING IT TO DECIMAL VALUE*/
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
	lcall	_putchar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00107$:
;	main.c:92: else if(l==127)
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x7F,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00124$
;	Peephole 300	removed redundant label 00125$
;	main.c:94: m=m/10;
;	genAssign
	mov	dptr,#_input_value_m_1_1
	movx	a,@dptr
	mov	r2,a
;	genDiv
	mov	dptr,#_input_value_m_1_1
;     genDivOneByte
	mov	b,#0x0A
	mov	a,r2
	div	ab
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00104$:
;	main.c:96: else if(l!=13)
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x0D,00126$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00126$:
;	main.c:97: {putstr("\n\rEnter Numbers\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_0
	mov	b,#0x80
	lcall	_putstr
00111$:
;	main.c:98: }while(l!=13);
;	genAssign
	mov	dptr,#_input_value_l_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x0D,00127$
	sjmp	00128$
00127$:
	ljmp	00110$
00128$:
;	main.c:99: return m;                                   /* UNTIL ENTER IS PRESSES*/
;	genAssign
	mov	dptr,#_input_value_m_1_1
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdgotoaddr'
;------------------------------------------------------------
;addr                      Allocated with name '_lcdgotoaddr_addr_1_1'
;temp                      Allocated with name '_lcdgotoaddr_temp_2_2'
;------------------------------------------------------------
;	main.c:103: void lcdgotoaddr(unsigned char addr)
;	-----------------------------------------
;	 function lcdgotoaddr
;	-----------------------------------------
_lcdgotoaddr:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdgotoaddr_addr_1_1
	movx	@dptr,a
;	main.c:105: if(addr>=0x00 && addr<=0x67)
;	genAssign
	mov	dptr,#_lcdgotoaddr_addr_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x67
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00104$
;	Peephole 300	removed redundant label 00107$
;	main.c:109: temp |= addr;
;	genOr
	orl	ar2,#0x80
;	main.c:110: *INST_WRITE = temp;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:111: lcdbusywait();
;	genCall
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	_lcdbusywait
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdgotoxy'
;------------------------------------------------------------
;column                    Allocated with name '_lcdgotoxy_PARM_2'
;row                       Allocated with name '_lcdgotoxy_row_1_1'
;------------------------------------------------------------
;	main.c:116: void lcdgotoxy(unsigned char row, unsigned char column)
;	-----------------------------------------
;	 function lcdgotoxy
;	-----------------------------------------
_lcdgotoxy:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdgotoxy_row_1_1
	movx	@dptr,a
;	main.c:118: switch(row)
;	genAssign
	mov	dptr,#_lcdgotoxy_row_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x03
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00106$
;	Peephole 300	removed redundant label 00109$
;	genJumpTab
	mov	a,r2
;	Peephole 254	optimized left shift
	add	a,r2
	add	a,r2
	mov	dptr,#00110$
	jmp	@a+dptr
00110$:
	ljmp	00101$
	ljmp	00102$
	ljmp	00103$
	ljmp	00104$
;	main.c:120: case 0: lcdgotoaddr((BASE_ADDRROW0+column));
00101$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:121: break;
;	main.c:122: case 1: lcdgotoaddr((BASE_ADDRROW1+column));
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoaddr
00102$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genPlus
;     genPlusIncr
	mov	a,#0x40
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:123: break;
;	main.c:124: case 2: lcdgotoaddr((BASE_ADDRROW2+column));
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoaddr
00103$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genPlus
;     genPlusIncr
	mov	a,#0x10
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:125: break;
;	main.c:126: case 3: lcdgotoaddr((BASE_ADDRROW3+column));
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoaddr
00104$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genPlus
;     genPlusIncr
	mov	a,#0x50
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:128: }
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	_lcdgotoaddr
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdclear'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:132: void lcdclear()
;	-----------------------------------------
;	 function lcdclear
;	-----------------------------------------
_lcdclear:
;	main.c:134: *INST_WRITE = 0x01;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x01
	movx	@dptr,a
;	main.c:135: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:136: putstr("Clearing the LCD completed\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdputch'
;------------------------------------------------------------
;cc                        Allocated with name '_lcdputch_cc_1_1'
;i                         Allocated with name '_lcdputch_i_1_1'
;temp                      Allocated with name '_lcdputch_temp_1_1'
;------------------------------------------------------------
;	main.c:140: void lcdputch(unsigned char cc)
;	-----------------------------------------
;	 function lcdputch
;	-----------------------------------------
_lcdputch:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdputch_cc_1_1
	movx	@dptr,a
;	main.c:143: *DATA_WRITE = cc;
;	genAssign
	mov	r2,#0x00
	mov	r3,#0xF4
;	genAssign
	mov	dptr,#_lcdputch_cc_1_1
	movx	a,@dptr
;	genPointerSet
;     genFarPointerSet
	mov	r4,a
	mov	dpl,r2
	mov	dph,r3
;	Peephole 136	removed redundant move
	movx	@dptr,a
;	main.c:144: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:145: i=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_lcdputch_i_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:146: temp = i&0x7F;              /* THIS LOOP HANDLES THE ROW ADDRESS MISMATCH OF THE LCD*/
;	genAssign
	mov	dptr,#_lcdputch_i_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAnd
	mov	dptr,#_lcdputch_temp_1_1
	mov	a,#0x7F
	anl	a,r2
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:147: if(temp==0x10)
;	genAssign
	mov	dptr,#_lcdputch_temp_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x10,00110$
	cjne	r3,#0x00,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00118$
;	Peephole 300	removed redundant label 00119$
;	main.c:149: lcdgotoxy(1,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x01
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
00110$:
;	main.c:151: else if(temp==0x50)
;	genAssign
	mov	dptr,#_lcdputch_temp_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x50,00107$
	cjne	r3,#0x00,00107$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00120$
;	Peephole 300	removed redundant label 00121$
;	main.c:153: lcdgotoxy(2,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x02
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
00107$:
;	main.c:155: else if(temp==0x20)
;	genAssign
	mov	dptr,#_lcdputch_temp_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x20,00104$
	cjne	r3,#0x00,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00122$
;	Peephole 300	removed redundant label 00123$
;	main.c:157: lcdgotoxy(3,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
00104$:
;	main.c:159: else if(temp==0x60)
;	genAssign
	mov	dptr,#_lcdputch_temp_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x60,00112$
	cjne	r3,#0x00,00112$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00124$
;	Peephole 300	removed redundant label 00125$
;	main.c:161: lcdgotoxy(0,0);//lcdgotoaddr(temp);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	_lcdgotoxy
00112$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putstr'
;------------------------------------------------------------
;ptr                       Allocated with name '_putstr_ptr_1_1'
;------------------------------------------------------------
;	main.c:170: void putstr(char *ptr)
;	-----------------------------------------
;	 function putstr
;	-----------------------------------------
_putstr:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_putstr_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:172: while(*ptr)
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
	mov	r5,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:174: putchar(*ptr);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_putchar
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:175: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00110$
	inc	r3
00110$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00108$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdputstr'
;------------------------------------------------------------
;ptr                       Allocated with name '_lcdputstr_ptr_1_1'
;------------------------------------------------------------
;	main.c:181: void lcdputstr(unsigned char *ptr)
;	-----------------------------------------
;	 function lcdputstr
;	-----------------------------------------
_lcdputstr:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_lcdputstr_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:183: while(*ptr)
;	genAssign
	mov	dptr,#_lcdputstr_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
	mov	r5,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:185: lcdputch(*ptr);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_lcdputch
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:186: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00110$
	inc	r3
00110$:
;	genAssign
	mov	dptr,#_lcdputstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00108$:
;	genAssign
	mov	dptr,#_lcdputstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdinit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:195: void lcdinit()
;	-----------------------------------------
;	 function lcdinit
;	-----------------------------------------
_lcdinit:
;	main.c:197: MSDelay(20);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0014
	lcall	_MSDelay
;	main.c:198: *INST_WRITE = 0x30;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x30
	movx	@dptr,a
;	main.c:199: MSDelay(10);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x000A
	lcall	_MSDelay
;	main.c:200: *INST_WRITE = 0x30;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x30
	movx	@dptr,a
;	main.c:201: MSDelay(10);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x000A
	lcall	_MSDelay
;	main.c:202: *INST_WRITE = 0x30;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x30
	movx	@dptr,a
;	main.c:203: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:204: *INST_WRITE = 0x38;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x38
	movx	@dptr,a
;	main.c:205: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:206: *INST_WRITE = 0x08;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x08
	movx	@dptr,a
;	main.c:207: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:208: *INST_WRITE = 0x0C;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x0C
	movx	@dptr,a
;	main.c:209: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:210: *INST_WRITE = 0x06;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x06
	movx	@dptr,a
;	main.c:211: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:212: *INST_WRITE = 0x01;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x01
	movx	@dptr,a
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'cgramdump'
;------------------------------------------------------------
;count_row                 Allocated with name '_cgramdump_count_row_1_1'
;count                     Allocated with name '_cgramdump_count_1_1'
;temp_val                  Allocated with name '_cgramdump_temp_val_1_1'
;k                         Allocated with name '_cgramdump_k_1_1'
;------------------------------------------------------------
;	main.c:216: void cgramdump()
;	-----------------------------------------
;	 function cgramdump
;	-----------------------------------------
_cgramdump:
;	main.c:220: unsigned char temp_val=0x40, k;
;	genAssign
	mov	dptr,#_cgramdump_temp_val_1_1
	mov	a,#0x40
	movx	@dptr,a
;	main.c:221: k=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:222: *INST_WRITE = temp_val;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x40
	movx	@dptr,a
;	main.c:223: lcdbusywait();
;	genCall
	push	ar2
	lcall	_lcdbusywait
	pop	ar2
;	main.c:224: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:225: putstr("CGRAM values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:226: putstr("\n\rCCODE |  Values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_4
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:227: for (count_row=0; count_row<=7; count_row++)
;	genAssign
	mov	r3,#0x00
	mov	r4,#0x00
00104$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x07
	subb	a,r3
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r4
;	genIfxJump
	jnc	00116$
	ljmp	00107$
00116$:
;	main.c:229: printf("0x%04x:  ", (temp_val&0x3F));
;	genAssign
	mov	dptr,#_cgramdump_temp_val_1_1
	movx	a,@dptr
	mov	r5,a
;	genAnd
	mov	a,#0x3F
	anl	a,r5
	mov	r6,a
;	genCast
	mov	r7,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_5
	push	acc
	mov	a,#(__str_5 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:230: for(count=0; count<=7; count++)
;	genAssign
;	genAssign
	mov	r6,#0x08
	mov	r7,#0x00
00103$:
;	main.c:232: temp_val++;
;	genPlus
;     genPlusIncr
	inc	r5
;	main.c:233: printf("%02x  ",*DATA_READ);
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xFC00
	movx	a,@dptr
	mov	r0,a
;	genCast
	mov	r1,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
;	genIpush
	mov	a,#__str_6
	push	acc
	mov	a,#(__str_6 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:234: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genMinus
;	genMinusDec
	dec	r6
	cjne	r6,#0xff,00117$
	dec	r7
00117$:
;	main.c:230: for(count=0; count<=7; count++)
;	genIfx
	mov	a,r6
	orl	a,r7
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00118$
;	main.c:236: putstr("\n\r");
;	genAssign
	mov	dptr,#_cgramdump_temp_val_1_1
	mov	a,r5
	movx	@dptr,a
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:227: for (count_row=0; count_row<=7; count_row++)
;	genPlus
;     genPlusIncr
	inc	r3
	cjne	r3,#0x00,00119$
	inc	r4
00119$:
	ljmp	00104$
00107$:
;	main.c:238: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:239: *INST_WRITE=(k|0x80);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:240: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdcreatechar'
;------------------------------------------------------------
;sloc0                     Allocated with name '_lcdcreatechar_sloc0_1_0'
;row_vals                  Allocated with name '_lcdcreatechar_PARM_2'
;ccode                     Allocated with name '_lcdcreatechar_ccode_1_1'
;n                         Allocated with name '_lcdcreatechar_n_1_1'
;k                         Allocated with name '_lcdcreatechar_k_1_1'
;------------------------------------------------------------
;	main.c:245: void lcdcreatechar(unsigned char ccode, unsigned char row_vals[])
;	-----------------------------------------
;	 function lcdcreatechar
;	-----------------------------------------
_lcdcreatechar:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdcreatechar_ccode_1_1
	movx	@dptr,a
;	main.c:248: k=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:249: lcdbusywait();
;	genCall
	push	ar2
	lcall	_lcdbusywait
	pop	ar2
;	main.c:250: ccode |= 0x40;
;	genAssign
;	genOr
	mov	dptr,#_lcdcreatechar_ccode_1_1
	movx	a,@dptr
	mov	r3,a
;	Peephole 248.a	optimized or to xdata
	orl	a,#0x40
	movx	@dptr,a
;	main.c:251: for (n=0; n<=7; n++)
;	genAssign
	mov	dptr,#_lcdcreatechar_PARM_2
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genAssign
	mov	dptr,#_lcdcreatechar_ccode_1_1
	movx	a,@dptr
	mov	r6,a
;	genAssign
	mov	r7,#0x00
00101$:
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r7
	add	a,#0xff - 0x07
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00109$
;	Peephole 300	removed redundant label 00110$
;	main.c:253: *INST_WRITE = ccode;
;	genIpush
	push	ar2
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r6
	movx	@dptr,a
;	main.c:254: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:255: *DATA_WRITE = (row_vals[n]&0x1F);
;	genAssign
	mov	_lcdcreatechar_sloc0_1_0,#0x00
	mov	(_lcdcreatechar_sloc0_1_0 + 1),#0xF4
;	genPlus
;	Peephole 236.g	used r7 instead of ar7
	mov	a,r7
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	mov	r2,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	mov	r0,a
	mov	ar1,r5
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r0
	mov	b,r1
	lcall	__gptrget
	mov	r2,a
;	genAnd
	anl	ar2,#0x1F
;	genPointerSet
;     genFarPointerSet
	mov	dpl,_lcdcreatechar_sloc0_1_0
	mov	dph,(_lcdcreatechar_sloc0_1_0 + 1)
	mov	a,r2
	movx	@dptr,a
;	main.c:256: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:257: ccode++;
;	genPlus
;     genPlusIncr
	inc	r6
;	genAssign
	mov	dptr,#_lcdcreatechar_ccode_1_1
	mov	a,r6
	movx	@dptr,a
;	main.c:251: for (n=0; n<=7; n++)
;	genPlus
;     genPlusIncr
	inc	r7
;	genIpop
	pop	ar2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00109$:
;	genAssign
	mov	dptr,#_lcdcreatechar_ccode_1_1
	mov	a,r6
	movx	@dptr,a
;	main.c:259: *INST_WRITE=(k|0x80);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:260: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'ddramdump'
;------------------------------------------------------------
;count_row                 Allocated with name '_ddramdump_count_row_1_1'
;count                     Allocated with name '_ddramdump_count_1_1'
;temp_val                  Allocated with name '_ddramdump_temp_val_1_1'
;k                         Allocated with name '_ddramdump_k_1_1'
;------------------------------------------------------------
;	main.c:264: void ddramdump()
;	-----------------------------------------
;	 function ddramdump
;	-----------------------------------------
_ddramdump:
;	main.c:268: unsigned char temp_val=0, k;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:269: k=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:270: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:271: putstr("DDRAM Values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_7
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:272: putstr("\n\rADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_8
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:273: for (count_row=0; count_row<=3; count_row++)
;	genAssign
	mov	r3,#0x00
	mov	r4,#0x00
00112$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x03
	subb	a,r3
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r4
;	genIfxJump
	jnc	00128$
	ljmp	00115$
00128$:
;	main.c:275: if(count_row==0)
;	genIfx
	mov	a,r3
	orl	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00102$
;	Peephole 300	removed redundant label 00129$
;	main.c:276: temp_val=0x80;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0x80
	movx	@dptr,a
00102$:
;	main.c:277: if(count_row==1)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r3,#0x01,00104$
	cjne	r4,#0x00,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00130$
;	Peephole 300	removed redundant label 00131$
;	main.c:278: temp_val=0xC0;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0xC0
	movx	@dptr,a
00104$:
;	main.c:279: if(count_row==2)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r3,#0x02,00106$
	cjne	r4,#0x00,00106$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00132$
;	Peephole 300	removed redundant label 00133$
;	main.c:280: temp_val=0x90;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0x90
	movx	@dptr,a
00106$:
;	main.c:281: if(count_row==3)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r3,#0x03,00108$
	cjne	r4,#0x00,00108$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00134$
;	Peephole 300	removed redundant label 00135$
;	main.c:282: temp_val=0xD0;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0xD0
	movx	@dptr,a
00108$:
;	main.c:283: printf("0x%02x:  ", (temp_val&0x7F));
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	movx	a,@dptr
	mov	r5,a
;	genAnd
	mov	a,#0x7F
	anl	a,r5
	mov	r6,a
;	genCast
	mov	r7,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_9
	push	acc
	mov	a,#(__str_9 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:284: for(count=0; count<=15; count++)
;	genAssign
;	genAssign
	mov	r6,#0x10
	mov	r7,#0x00
00111$:
;	main.c:286: *INST_WRITE = temp_val;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r5
	movx	@dptr,a
;	main.c:287: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:288: temp_val++;
;	genPlus
;     genPlusIncr
	inc	r5
;	main.c:289: printf("%02x  ",*DATA_READ);
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xFC00
	movx	a,@dptr
	mov	r0,a
;	genCast
	mov	r1,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
;	genIpush
	mov	a,#__str_6
	push	acc
	mov	a,#(__str_6 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:290: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genMinus
;	genMinusDec
	dec	r6
	cjne	r6,#0xff,00136$
	dec	r7
00136$:
;	main.c:284: for(count=0; count<=15; count++)
;	genIfx
	mov	a,r6
	orl	a,r7
;	genIfxJump
	jz	00137$
	ljmp	00111$
00137$:
;	main.c:292: putstr("\n\r");
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,r5
	movx	@dptr,a
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:273: for (count_row=0; count_row<=3; count_row++)
;	genPlus
;     genPlusIncr
	inc	r3
	cjne	r3,#0x00,00138$
	inc	r4
00138$:
	ljmp	00112$
00115$:
;	main.c:294: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:295: *INST_WRITE=(k|0x80);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	Peephole 300	removed redundant label 00116$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'readinput'
;------------------------------------------------------------
;value                     Allocated with name '_readinput_value_1_1'
;i                         Allocated with name '_readinput_i_1_1'
;j                         Allocated with name '_readinput_j_1_1'
;------------------------------------------------------------
;	main.c:299: unsigned char readinput()
;	-----------------------------------------
;	 function readinput
;	-----------------------------------------
_readinput:
;	main.c:301: unsigned char value=0, i, j=0;
;	genAssign
	mov	dptr,#_readinput_value_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:302: do
;	genAssign
	mov	r2,#0x00
00110$:
;	main.c:304: i=getchar();
;	genCall
	push	ar2
	lcall	_getchar
	mov	r3,dpl
	pop	ar2
;	main.c:305: if(i>='0'&&i<='1')          /* works only if its a number */
;	genAssign
	mov	ar4,r3
;	genCmpLt
;	genCmp
	cjne	r4,#0x30,00122$
00122$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00123$
;	genAssign
	mov	ar4,r3
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r4
	add	a,#0xff - 0x31
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00124$
;	main.c:307: value=((value*2)+(i-'0')); /* Convert character to integers */
;	genAssign
	mov	dptr,#_readinput_value_1_1
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
;	Peephole 105	removed redundant mov
;	Peephole 204	removed redundant mov
	add	a,acc
	mov	r4,a
;	genMinus
	mov	a,r3
	add	a,#0xd0
;	genPlus
	mov	dptr,#_readinput_value_1_1
;	Peephole 236.a	used r4 instead of ar4
	add	a,r4
	movx	@dptr,a
;	main.c:308: putchar(i);
;	genCall
	mov	dpl,r3
	push	ar2
	push	ar3
	lcall	_putchar
	pop	ar3
	pop	ar2
;	main.c:309: j++;
;	genPlus
;     genPlusIncr
	inc	r2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00107$:
;	main.c:311: else if(i==127)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r3,#0x7F,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00125$
;	Peephole 300	removed redundant label 00126$
;	main.c:313: value=value/2;
;	genAssign
	mov	dptr,#_readinput_value_1_1
	movx	a,@dptr
;	genRightShift
;	genRightShiftLiteral
;	genrshOne
	mov	r4,a
;	Peephole 105	removed redundant mov
	clr	c
	rrc	a
;	genAssign
	mov	r4,a
	mov	dptr,#_readinput_value_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00104$:
;	main.c:315: else if(i!=13)
;	genCmpEq
;	gencjneshort
	cjne	r3,#0x0D,00127$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00127$:
;	main.c:316: {putstr("\n\rEnter Valid Numbers\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_10
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
00111$:
;	main.c:317: }while(i!=13);
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r3,#0x0D,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00128$
;	Peephole 300	removed redundant label 00129$
;	main.c:319: return value;
;	genAssign
	mov	dptr,#_readinput_value_1_1
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'addcharacter'
;------------------------------------------------------------
;temp                      Allocated with name '_addcharacter_temp_1_1'
;y                         Allocated with name '_addcharacter_y_1_1'
;------------------------------------------------------------
;	main.c:323: void addcharacter()
;	-----------------------------------------
;	 function addcharacter
;	-----------------------------------------
_addcharacter:
;	main.c:326: if(u<8)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x08,00148$
00148$:
;	genIfxJump
	jc	00149$
	ljmp	00128$
00149$:
;	main.c:328: putstr("\n\rEnter eight pixel values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	lcall	_putstr
;	main.c:329: putstr("Enter five binary values for every pixel\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_12
	mov	b,#0x80
	lcall	_putstr
;	main.c:332: for(temp=0; temp<=7; temp++)
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
00130$:
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x07
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00133$
;	Peephole 300	removed redundant label 00150$
;	main.c:334: y=readinput();
;	genCall
	push	ar2
	lcall	_readinput
	mov	r3,dpl
	pop	ar2
;	main.c:335: if(y>0x1F)
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x1F
	jnc	00102$
;	Peephole 300	removed redundant label 00151$
;	main.c:337: putstr("Invalid Number. Enter Again\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_13
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:338: temp--;
;	genMinus
;	genMinusDec
	mov	a,r2
	dec	a
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00132$
00102$:
;	main.c:342: array[temp]=y;
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_array
	mov	dpl,a
;	Peephole 181	changed mov to clr
	clr	a
	addc	a,#(_array >> 8)
	mov	dph,a
;	genPointerSet
;     genFarPointerSet
	mov	a,r3
	movx	@dptr,a
;	main.c:343: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	lcall	_putstr
00132$:
;	main.c:332: for(temp=0; temp<=7; temp++)
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
	movx	a,@dptr
	mov	r2,a
;	genPlus
	mov	dptr,#_addcharacter_temp_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00130$
00133$:
;	main.c:346: if(u==0)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
;	genIfx
	mov	r2,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00125$
;	Peephole 300	removed redundant label 00152$
;	main.c:347: lcdcreatechar(0x00, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	lcall	_lcdcreatechar
	ljmp	00126$
00125$:
;	main.c:348: else if(u==1)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x01,00122$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00153$
;	Peephole 300	removed redundant label 00154$
;	main.c:349: lcdcreatechar(0x08, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x08
	lcall	_lcdcreatechar
	ljmp	00126$
00122$:
;	main.c:350: else if(u==2)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x02,00119$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00155$
;	Peephole 300	removed redundant label 00156$
;	main.c:351: lcdcreatechar(0x10, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x10
	lcall	_lcdcreatechar
	ljmp	00126$
00119$:
;	main.c:352: else if(u==3)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x03,00116$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00157$
;	Peephole 300	removed redundant label 00158$
;	main.c:353: lcdcreatechar(0x18, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x18
	lcall	_lcdcreatechar
	ljmp	00126$
00116$:
;	main.c:354: else if(u==4)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x04,00113$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00159$
;	Peephole 300	removed redundant label 00160$
;	main.c:355: lcdcreatechar(0x20, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x20
	lcall	_lcdcreatechar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00126$
00113$:
;	main.c:356: else if(u==5)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x05,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00161$
;	Peephole 300	removed redundant label 00162$
;	main.c:357: lcdcreatechar(0x28, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x28
	lcall	_lcdcreatechar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00126$
00110$:
;	main.c:358: else if(u==6)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x06,00107$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00163$
;	Peephole 300	removed redundant label 00164$
;	main.c:359: lcdcreatechar(0x30, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x30
	lcall	_lcdcreatechar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00126$
00107$:
;	main.c:360: else if(u==7)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x07,00126$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00165$
;	Peephole 300	removed redundant label 00166$
;	main.c:361: lcdcreatechar(0x38, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x38
	lcall	_lcdcreatechar
00126$:
;	main.c:362: u++;
;	genPlus
	mov	dptr,#_u
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
	ret
00128$:
;	main.c:366: putstr("Eight custom character created. No more custom character please\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_14
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'display'
;------------------------------------------------------------
;hey                       Allocated with name '_display_hey_1_1'
;------------------------------------------------------------
;	main.c:371: void display()
;	-----------------------------------------
;	 function display
;	-----------------------------------------
_display:
;	main.c:374: putstr("Look at the LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_15
	mov	b,#0x80
	lcall	_putstr
;	main.c:375: while(hey<0x8)
;	genAssign
	mov	r2,#0x00
00101$:
;	genCmpLt
;	genCmp
	cjne	r2,#0x08,00109$
00109$:
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00104$
;	Peephole 300	removed redundant label 00110$
;	main.c:377: lcdputch(hey);
;	genCall
	mov	dpl,r2
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:378: hey++;
;	genPlus
;     genPlusIncr
	inc	r2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'hex2int'
;------------------------------------------------------------
;sloc0                     Allocated with name '_hex2int_sloc0_1_0'
;sloc1                     Allocated with name '_hex2int_sloc1_1_0'
;sloc2                     Allocated with name '_hex2int_sloc2_1_0'
;sloc3                     Allocated with name '_hex2int_sloc3_1_0'
;len                       Allocated with name '_hex2int_PARM_2'
;a                         Allocated with name '_hex2int_a_1_1'
;i                         Allocated with name '_hex2int_i_1_1'
;val                       Allocated with name '_hex2int_val_1_1'
;------------------------------------------------------------
;	main.c:386: unsigned long hex2int(char *a, unsigned int len)
;	-----------------------------------------
;	 function hex2int
;	-----------------------------------------
_hex2int:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_hex2int_a_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:389: unsigned long val = 0;
;	genAssign
	mov	dptr,#_hex2int_val_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:391: for(i=0;i<len;i++)
;	genAssign
	mov	dptr,#_hex2int_a_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genAssign
	mov	dptr,#_hex2int_PARM_2
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genAssign
	mov	r7,#0x00
	mov	r0,#0x00
00104$:
;	genIpush
	push	ar2
	push	ar3
	push	ar4
;	genAssign
	mov	ar1,r7
	mov	ar2,r0
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r1
	subb	a,r5
	mov	a,r2
	subb	a,r6
;	genIpop
;	genIfx
;	genIfxJump
;	Peephole 129.d	optimized condition
	pop	ar4
	pop	ar3
	pop	ar2
	jc	00114$
	ljmp	00107$
00114$:
;	main.c:392: if(a[i] <= 57)
;	genIpush
	push	ar5
	push	ar6
;	genPlus
;	Peephole 236.g	used r7 instead of ar7
	mov	a,r7
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	mov	r1,a
;	Peephole 236.g	used r0 instead of ar0
	mov	a,r0
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	mov	r5,a
	mov	ar6,r4
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r1
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
	mov	_hex2int_sloc0_1_0,a
;	genCmpGt
;	genCmp
	clr	c
;	Peephole 159	avoided xrl during execution
	mov	a,#(0x39 ^ 0x80)
	mov	b,_hex2int_sloc0_1_0
	xrl	b,#0x80
	subb	a,b
	clr	a
	rlc	a
;	genIpop
	pop	ar6
	pop	ar5
;	genIfx
;	genIfxJump
	jz	00115$
	ljmp	00102$
00115$:
;	main.c:393: val += (a[i]-48)*(1<<(4*(len-1-i)));
;	genIpush
	push	ar2
	push	ar3
	push	ar4
;	genCast
	mov	r1,_hex2int_sloc0_1_0
	mov	a,_hex2int_sloc0_1_0
	rlc	a
	subb	a,acc
	mov	r2,a
;	genMinus
	mov	a,r1
	add	a,#0xd0
	mov	r1,a
	mov	a,r2
	addc	a,#0xff
	mov	r2,a
;	genMinus
;	genMinusDec
	mov	a,r5
	add	a,#0xff
	mov	r3,a
	mov	a,r6
	addc	a,#0xff
	mov	r4,a
;	genMinus
	mov	a,r3
	clr	c
;	Peephole 236.l	used r7 instead of ar7
	subb	a,r7
	mov	r3,a
	mov	a,r4
;	Peephole 236.l	used r0 instead of ar0
	subb	a,r0
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r4,a
;	Peephole 105	removed redundant mov
	xch	a,r3
	add	a,acc
	xch	a,r3
	rlc	a
	xch	a,r3
	add	a,acc
	xch	a,r3
	rlc	a
	mov	r4,a
;	genLeftShift
	mov	b,r3
	inc	b
	mov	r3,#0x01
	mov	r4,#0x00
	sjmp	00117$
00116$:
	mov	a,r3
;	Peephole 254	optimized left shift
	add	a,r3
	mov	r3,a
	mov	a,r4
	rlc	a
	mov	r4,a
00117$:
	djnz	b,00116$
;	genAssign
	mov	dptr,#__mulint_PARM_2
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	genCall
	mov	dpl,r1
	mov	dph,r2
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	lcall	__mulint
	mov	_hex2int_sloc1_1_0,dpl
	mov	(_hex2int_sloc1_1_0 + 1),dph
	pop	ar0
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	dptr,#_hex2int_val_1_1
	movx	a,@dptr
	mov	_hex2int_sloc2_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc2_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc2_1_0 + 2),a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc2_1_0 + 3),a
;	genCast
	mov	r2,_hex2int_sloc1_1_0
	mov	r3,(_hex2int_sloc1_1_0 + 1)
	mov	r4,#0x00
	mov	r1,#0x00
;	genPlus
	mov	dptr,#_hex2int_val_1_1
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,_hex2int_sloc2_1_0
	movx	@dptr,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,(_hex2int_sloc2_1_0 + 1)
	inc	dptr
	movx	@dptr,a
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	addc	a,(_hex2int_sloc2_1_0 + 2)
	inc	dptr
	movx	@dptr,a
;	Peephole 236.g	used r1 instead of ar1
	mov	a,r1
	addc	a,(_hex2int_sloc2_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	genIpop
	pop	ar4
	pop	ar3
	pop	ar2
	ljmp	00106$
00102$:
;	main.c:395: val += (a[i]-55)*(1<<(4*(len-1-i)));
;	genIpush
	push	ar2
	push	ar3
	push	ar4
;	genCast
	mov	r1,_hex2int_sloc0_1_0
	mov	a,_hex2int_sloc0_1_0
	rlc	a
	subb	a,acc
	mov	r2,a
;	genMinus
	mov	a,r1
	add	a,#0xc9
	mov	r1,a
	mov	a,r2
	addc	a,#0xff
	mov	r2,a
;	genMinus
;	genMinusDec
	mov	a,r5
	add	a,#0xff
	mov	r3,a
	mov	a,r6
	addc	a,#0xff
	mov	r4,a
;	genMinus
	mov	a,r3
	clr	c
;	Peephole 236.l	used r7 instead of ar7
	subb	a,r7
	mov	r3,a
	mov	a,r4
;	Peephole 236.l	used r0 instead of ar0
	subb	a,r0
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r4,a
;	Peephole 105	removed redundant mov
	xch	a,r3
	add	a,acc
	xch	a,r3
	rlc	a
	xch	a,r3
	add	a,acc
	xch	a,r3
	rlc	a
	mov	r4,a
;	genLeftShift
	mov	b,r3
	inc	b
	mov	r3,#0x01
	mov	r4,#0x00
	sjmp	00119$
00118$:
	mov	a,r3
;	Peephole 254	optimized left shift
	add	a,r3
	mov	r3,a
	mov	a,r4
	rlc	a
	mov	r4,a
00119$:
	djnz	b,00118$
;	genAssign
	mov	dptr,#__mulint_PARM_2
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	genCall
	mov	dpl,r1
	mov	dph,r2
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	lcall	__mulint
	mov	_hex2int_sloc2_1_0,dpl
	mov	(_hex2int_sloc2_1_0 + 1),dph
	pop	ar0
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genAssign
	mov	dptr,#_hex2int_val_1_1
	movx	a,@dptr
	mov	_hex2int_sloc3_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc3_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc3_1_0 + 2),a
	inc	dptr
	movx	a,@dptr
	mov	(_hex2int_sloc3_1_0 + 3),a
;	genCast
	mov	r2,_hex2int_sloc2_1_0
	mov	r3,(_hex2int_sloc2_1_0 + 1)
	mov	r4,#0x00
	mov	r1,#0x00
;	genPlus
	mov	dptr,#_hex2int_val_1_1
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,_hex2int_sloc3_1_0
	movx	@dptr,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,(_hex2int_sloc3_1_0 + 1)
	inc	dptr
	movx	@dptr,a
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	addc	a,(_hex2int_sloc3_1_0 + 2)
	inc	dptr
	movx	@dptr,a
;	Peephole 236.g	used r1 instead of ar1
	mov	a,r1
	addc	a,(_hex2int_sloc3_1_0 + 3)
	inc	dptr
	movx	@dptr,a
;	main.c:397: return val;
;	genIpop
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:395: val += (a[i]-55)*(1<<(4*(len-1-i)));
00106$:
;	main.c:391: for(i=0;i<len;i++)
;	genPlus
;     genPlusIncr
	inc	r7
	cjne	r7,#0x00,00120$
	inc	r0
00120$:
	ljmp	00104$
00107$:
;	main.c:397: return val;
;	genAssign
	mov	dptr,#_hex2int_val_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
;	genRet
	mov	r5,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 191	removed redundant mov
;	Peephole 300	removed redundant label 00108$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ReadVariable'
;------------------------------------------------------------
;l                         Allocated with name '_ReadVariable_l_1_1'
;m                         Allocated with name '_ReadVariable_m_1_1'
;n                         Allocated with name '_ReadVariable_n_1_1'
;k                         Allocated with name '_ReadVariable_k_1_1'
;ptr                       Allocated with name '_ReadVariable_ptr_1_1'
;------------------------------------------------------------
;	main.c:400: uint16_t ReadVariable()
;	-----------------------------------------
;	 function ReadVariable
;	-----------------------------------------
_ReadVariable:
;	main.c:403: unsigned char ptr[]="";
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_ReadVariable_ptr_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:404: do
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
00103$:
;	main.c:406: l=getchar();
;	genCall
	push	ar2
	push	ar3
	lcall	_getchar
	mov	r4,dpl
	pop	ar3
	pop	ar2
;	genCast
	mov	a,r4
	rlc	a
	subb	a,acc
	mov	r5,a
;	main.c:407: if(l!=13)
;	genCmpEq
;	gencjne
;	gencjneshort
;	Peephole 241.c	optimized compare
	clr	a
	cjne	r4,#0x0D,00112$
	cjne	r5,#0x00,00112$
	inc	a
00112$:
;	Peephole 300	removed redundant label 00113$
;	genIfx
	mov	r6,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00104$
;	Peephole 300	removed redundant label 00114$
;	main.c:409: ptr[k]=l;
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_ReadVariable_ptr_1_1
	mov	dpl,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,#(_ReadVariable_ptr_1_1 >> 8)
	mov	dph,a
;	genCast
	mov	ar7,r4
;	genPointerSet
;     genFarPointerSet
	mov	a,r7
	movx	@dptr,a
;	main.c:410: k++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00115$
	inc	r3
00115$:
;	main.c:411: putchar(l);
;	genCast
;	genCall
	mov	dpl,r4
	push	ar2
	push	ar3
	push	ar6
	lcall	_putchar
	pop	ar6
	pop	ar3
	pop	ar2
00104$:
;	main.c:413: }while(l!=13);
;	genIfx
	mov	a,r6
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00103$
;	Peephole 300	removed redundant label 00116$
;	main.c:414: n=hex2int(ptr,k);
;	genAssign
	mov	dptr,#_hex2int_PARM_2
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#_ReadVariable_ptr_1_1
	mov	b,#0x00
;	genCast
;	main.c:415: return n;
;	genRet
;	Peephole 150.h	removed misc moves via dph, dpl, b, a before return
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_hex2int
;
;------------------------------------------------------------
;Allocation info for local variables in function 'interactive'
;------------------------------------------------------------
;co                        Allocated with name '_interactive_co_1_1'
;i                         Allocated with name '_interactive_i_1_1'
;------------------------------------------------------------
;	main.c:419: void interactive()
;	-----------------------------------------
;	 function interactive
;	-----------------------------------------
_interactive:
;	main.c:422: i=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:423: i=i&0x80;
;	genAnd
	anl	ar2,#0x80
;	main.c:424: *INST_WRITE=i;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:425: putstr("Enter the character to display. Hit enter to stop\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_17
	mov	b,#0x80
	lcall	_putstr
;	main.c:426: do
00103$:
;	main.c:427: {   co=getchar();
;	genCall
	lcall	_getchar
;	main.c:428: putchar(co);
;	genCall
	mov  r2,dpl
;	Peephole 177.a	removed redundant mov
	push	ar2
	lcall	_putchar
	pop	ar2
;	main.c:429: if(co!=13)
;	genCmpEq
;	gencjne
;	gencjneshort
;	Peephole 241.d	optimized compare
	clr	a
	cjne	r2,#0x0D,00110$
	inc	a
00110$:
;	Peephole 300	removed redundant label 00111$
;	genIfx
	mov	r3,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00104$
;	Peephole 300	removed redundant label 00112$
;	main.c:430: lcdputch(co);
;	genCall
	mov	dpl,r2
	push	ar3
	lcall	_lcdputch
	pop	ar3
00104$:
;	main.c:431: }while(co!=13);
;	genIfx
	mov	a,r3
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00103$
;	Peephole 300	removed redundant label 00113$
;	Peephole 300	removed redundant label 00106$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'gotoxy'
;------------------------------------------------------------
;temp                      Allocated with name '_gotoxy_temp_1_1'
;temp1                     Allocated with name '_gotoxy_temp1_1_1'
;------------------------------------------------------------
;	main.c:438: void gotoxy()
;	-----------------------------------------
;	 function gotoxy
;	-----------------------------------------
_gotoxy:
;	main.c:441: putstr("\n\rEnter row number\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_18
	mov	b,#0x80
	lcall	_putstr
;	main.c:442: do
00103$:
;	main.c:444: temp=input_value();
;	genCall
	lcall	_input_value
	mov	r2,dpl
;	main.c:445: if(temp>3)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x03
	subb	a,r2
	clr	a
	rlc	a
;	genIfx
	mov	r3,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00119$
;	main.c:446: putstr("\n\rEnter correct row\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_19
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
00104$:
;	main.c:447: }while(temp>3);
;	genIfx
	mov	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00120$
;	main.c:448: putstr("\n\rEnter column number\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_20
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:449: do
00108$:
;	main.c:451: temp1=input_value();
;	genCall
	push	ar2
	lcall	_input_value
	mov	r3,dpl
	pop	ar2
;	main.c:452: if(temp1>15)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x0F
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00109$
;	Peephole 300	removed redundant label 00121$
;	main.c:453: putstr("\n\rEnter correct column number\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_21
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00109$:
;	main.c:454: }while(temp1>15);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00108$
;	Peephole 300	removed redundant label 00122$
;	main.c:455: lcdgotoxy(temp, temp1);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,r3
	movx	@dptr,a
;	genCall
	mov	dpl,r2
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
;
;------------------------------------------------------------
;Allocation info for local variables in function 'gotoaddress'
;------------------------------------------------------------
;temp                      Allocated with name '_gotoaddress_temp_1_1'
;------------------------------------------------------------
;	main.c:458: void gotoaddress()
;	-----------------------------------------
;	 function gotoaddress
;	-----------------------------------------
_gotoaddress:
;	main.c:460: unsigned char temp=0x66;
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	mov	a,#0x66
	movx	@dptr,a
;	main.c:461: putstr("Enter the address in hex\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_22
	mov	b,#0x80
	lcall	_putstr
;	main.c:462: while((temp>0x1F && temp<0x40)||(temp>=0x5F))
00107$:
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x1F
	jnc	00106$
;	Peephole 300	removed redundant label 00115$
;	genCmpLt
;	genCmp
	cjne	r2,#0x40,00116$
00116$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00108$
;	Peephole 300	removed redundant label 00117$
00106$:
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x5F,00118$
00118$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00109$
;	Peephole 300	removed redundant label 00119$
00108$:
;	main.c:464: temp=ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r3,dpl
	mov	r4,dph
;	genCast
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	mov	a,r3
	movx	@dptr,a
;	main.c:465: if((temp>0x1F && temp<0x40)||(temp>=0x5F))
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x1F
	jnc	00104$
;	Peephole 300	removed redundant label 00120$
;	genCmpLt
;	genCmp
	cjne	r3,#0x40,00121$
00121$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00101$
;	Peephole 300	removed redundant label 00122$
00104$:
;	genCmpLt
;	genCmp
	cjne	r3,#0x5F,00123$
00123$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00124$
00101$:
;	main.c:466: putstr("\n\rEnter correct address values in hex\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_23
	mov	b,#0x80
	lcall	_putstr
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00107$
00109$:
;	main.c:468: lcdgotoaddr(temp);
;	genCall
	mov	dpl,r2
	lcall	_lcdgotoaddr
;	main.c:469: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function '_sdcc_external_startup'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:472: _sdcc_external_startup()
;	-----------------------------------------
;	 function _sdcc_external_startup
;	-----------------------------------------
__sdcc_external_startup:
;	main.c:474: AUXR |= 0x0C;
;	genOr
	orl	_AUXR,#0x0C
;	main.c:475: return 0;
;	genRet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0000
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'delay'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:480: void delay()
;	-----------------------------------------
;	 function delay
;	-----------------------------------------
_delay:
;	main.c:489: _endasm ;
;	genInline
	    DELAY:MOV R5, #5
    DEL1:
	MOV R4, #10
    DEL:
	NOP
	        DJNZ r4, DEL
	         DJNZ R5, DEL1
	         ret
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'SerialInitialize'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:493: void SerialInitialize()
;	-----------------------------------------
;	 function SerialInitialize
;	-----------------------------------------
_SerialInitialize:
;	main.c:495: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
;	genAssign
	mov	_TMOD,#0x20
;	main.c:496: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
;	genAssign
	mov	_TH1,#0xFD
;	main.c:497: SCON=0x50;
;	genAssign
	mov	_SCON,#0x50
;	main.c:498: TR1=1;  // to start timer
;	genAssign
	setb	_TR1
;	main.c:499: IE=0x90; // to make serial interrupt interrupt enable
;	genAssign
	mov	_IE,#0x90
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
;x                         Allocated with name '_putchar_x_1_1'
;------------------------------------------------------------
;	main.c:502: void putchar(char x)
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
;	genReceive
	mov	a,dpl
	mov	dptr,#_putchar_x_1_1
	movx	@dptr,a
;	main.c:504: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
;	genAssign
	mov	dptr,#_putchar_x_1_1
	movx	a,@dptr
	mov	_SBUF,a
;	main.c:505: while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
;	main.c:506: TI=0; // make TI bit to zero for next transmission
;	genAssign
;	Peephole 250.a	using atomic test and clear
	jbc	_TI,00108$
	sjmp	00101$
00108$:
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getchar'
;------------------------------------------------------------
;rec                       Allocated with name '_getchar_rec_1_1'
;------------------------------------------------------------
;	main.c:510: char getchar()
;	-----------------------------------------
;	 function getchar
;	-----------------------------------------
_getchar:
;	main.c:514: while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
	jnb	_RI,00101$
;	Peephole 300	removed redundant label 00108$
;	main.c:515: rec=SBUF;   // data is received in SBUF register present in controller during receiving
;	genAssign
	mov	r2,_SBUF
;	main.c:516: RI=0;  // make RI bit to 0 so that next data is received
;	genAssign
	clr	_RI
;	main.c:517: return rec;  // return rec where function is called
;	genRet
	mov	dpl,r2
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'startbit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:521: void startbit()
;	-----------------------------------------
;	 function startbit
;	-----------------------------------------
_startbit:
;	main.c:531: _endasm ;
;	genInline
	        SETB P1.3
	        SETB P1.4
	        ACALL DELAY
	        CLR P1.4
	        ACALL DELAY
	        CLR P1.3
	        ACALL DELAY
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'stopbit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:535: void stopbit()
;	-----------------------------------------
;	 function stopbit
;	-----------------------------------------
_stopbit:
;	main.c:545: _endasm;
;	genInline
	        ACALL DELAY
	        CLR P1.4
	        ACALL DELAY
	        SETB P1.3
	        ACALL DELAY
	        SETB P1.4
	        ACALL DELAY
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ackbit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:549: void ackbit()
;	-----------------------------------------
;	 function ackbit
;	-----------------------------------------
_ackbit:
;	main.c:562: _endasm;
;	genInline
	        CLR C
	        ACALL DELAY
	        SETB P1.4
	        ACALL DELAY
	        SETB P1.3
	        ACALL DELAY
	        MOV C, P1.4
	        ACALL DELAY
	        CLR P1.3
	        ACALL DELAY
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Master_ack'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:567: void Master_ack()
;	-----------------------------------------
;	 function Master_ack
;	-----------------------------------------
_Master_ack:
;	main.c:580: _endasm;
;	genInline
	        ACALL DELAY
	        ACALL DELAY
	        CLR P1.4
	        ACALL DELAY
	        SETB P1.3
	        ACALL DELAY
	        ACALL DELAY
	        CLR P1.3
	        nop
	        setb P1.4
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'send_data_assembly'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:584: void send_data_assembly()
;	-----------------------------------------
;	 function send_data_assembly
;	-----------------------------------------
_send_data_assembly:
;	main.c:602: _endasm;
;	genInline
	        ACALL DELAY
	        MOV R1, #8
	        HERE:CLR C
	            RLC A
	            JNC NEXT
	            SETB P1.4
	            AJMP NEXT1
        NEXT:
	CLR P1.4
        NEXT1:
	            ACALL DELAY
	            SETB P1.3
	            ACALL DELAY
	            CLR P1.3
	            ACALL DELAY
	            DJNZ R1, HERE
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'send_data'
;------------------------------------------------------------
;value                     Allocated with name '_send_data_value_1_1'
;status_send               Allocated with name '_send_data_status_send_1_1'
;------------------------------------------------------------
;	main.c:605: int send_data(unsigned char value)
;	-----------------------------------------
;	 function send_data
;	-----------------------------------------
_send_data:
;	genReceive
	mov	a,dpl
	mov	dptr,#_send_data_value_1_1
	movx	@dptr,a
;	main.c:608: ACC = value;
;	genAssign
	mov	dptr,#_send_data_value_1_1
	movx	a,@dptr
	mov	_ACC,a
;	main.c:609: CY=1;
;	genAssign
	setb	_CY
;	main.c:610: send_data_assembly();
;	genCall
	lcall	_send_data_assembly
;	main.c:611: ackbit();
;	genCall
	lcall	_ackbit
;	main.c:613: ACC = 0x00;
;	genAssign
	mov	_ACC,#0x00
;	main.c:614: return status_send;
;	genRet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'clock_func'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:618: void clock_func()
;	-----------------------------------------
;	 function clock_func
;	-----------------------------------------
_clock_func:
;	main.c:634: _endasm;
;	genInline
	            MOV A, #0
	            MOV R7, #8
        LOOP2:
	NOP
	                ACALL DELAY
	                clr P1.3
	                ACALL DELAY
	                setb P1.3
	                MOV C, P1.4
	                RLC A
	                DJNZ R7, LOOP2
	                clr P1.3
	                ACALL DELAY
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'eebytew'
;------------------------------------------------------------
;databyte                  Allocated with name '_eebytew_PARM_2'
;addr                      Allocated with name '_eebytew_addr_1_1'
;address                   Allocated with name '_eebytew_address_1_1'
;result                    Allocated with name '_eebytew_result_1_1'
;result1                   Allocated with name '_eebytew_result1_1_1'
;result2                   Allocated with name '_eebytew_result2_1_1'
;temp                      Allocated with name '_eebytew_temp_1_1'
;tempaddr                  Allocated with name '_eebytew_tempaddr_1_1'
;------------------------------------------------------------
;	main.c:638: int eebytew(uint16_t addr, unsigned char databyte)  // write byte, returns status
;	-----------------------------------------
;	 function eebytew
;	-----------------------------------------
_eebytew:
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_eebytew_addr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:643: printf("\n\rWriting 0x%03X: 0x%02X\n\r",addr, databyte);
;	genAssign
	mov	dptr,#_eebytew_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genCast
	mov	ar3,r2
	mov	r4,#0x00
;	genAssign
	mov	dptr,#_eebytew_addr_1_1
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genIpush
	push	ar2
	push	ar5
	push	ar6
	push	ar3
	push	ar4
;	genIpush
	push	ar5
	push	ar6
;	genIpush
	mov	a,#__str_24
	push	acc
	mov	a,#(__str_24 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xf9
	mov	sp,a
	pop	ar6
	pop	ar5
	pop	ar2
;	main.c:646: address &=0x700;
;	genAnd
	mov	r3,#0x00
	mov	a,#0x07
	anl	a,r6
;	main.c:647: address >>=7;
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	r4,a
;	Peephole 105	removed redundant mov
	mov	c,acc.7
	xch	a,r3
	rlc	a
	xch	a,r3
	rlc	a
	xch	a,r3
	anl	a,#0x01
	mov	r4,a
;	main.c:648: temp |= address;
;	genOr
	orl	ar3,#0xA0
;	genCast
;	main.c:649: temp &= 0xFE;
;	genAnd
	anl	ar3,#0xFE
;	main.c:650: tempaddr=addr;
;	genCast
;	main.c:651: startbit();
;	genCall
	push	ar2
	push	ar3
	push	ar5
	lcall	_startbit
	pop	ar5
	pop	ar3
	pop	ar2
;	main.c:652: result = send_data(temp);
;	genCall
	mov	dpl,r3
	push	ar2
	push	ar5
	lcall	_send_data
	mov	r3,dpl
	mov	r4,dph
	pop	ar5
	pop	ar2
;	main.c:653: result1 = send_data(tempaddr);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_send_data
	mov	r5,dpl
	mov	r6,dph
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:654: result2 = send_data(databyte);
;	genCall
	mov	dpl,r2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_send_data
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
;	main.c:655: stopbit();
;	genCall
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_stopbit
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
;	main.c:656: confirm=1;
;	genAssign
	mov	dptr,#_confirm
	mov	a,#0x01
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	main.c:657: return (result&result1);
;	genAnd
	mov	a,r5
	anl	ar3,a
	mov	a,r6
	anl	ar4,a
;	genRet
	mov	dpl,r3
	mov	dph,r4
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'eebyter'
;------------------------------------------------------------
;addr                      Allocated with name '_eebyter_addr_1_1'
;address                   Allocated with name '_eebyter_address_1_1'
;temp                      Allocated with name '_eebyter_temp_1_1'
;temp1                     Allocated with name '_eebyter_temp1_1_1'
;tempaddr                  Allocated with name '_eebyter_tempaddr_1_1'
;backup                    Allocated with name '_eebyter_backup_1_1'
;------------------------------------------------------------
;	main.c:661: int eebyter(uint16_t addr)
;	-----------------------------------------
;	 function eebyter
;	-----------------------------------------
_eebyter:
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_eebyter_addr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:666: address=addr;
;	genAssign
	mov	dptr,#_eebyter_addr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	main.c:667: address &=0x700;
;	genAnd
	mov	r4,#0x00
	mov	a,#0x07
	anl	a,r3
;	main.c:668: address >>=7;
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	r5,a
;	Peephole 105	removed redundant mov
	mov	c,acc.7
	xch	a,r4
	rlc	a
	xch	a,r4
	rlc	a
	xch	a,r4
	anl	a,#0x01
	mov	r5,a
;	main.c:669: temp |= address;
;	genOr
	orl	ar4,#0xA0
;	genCast
;	main.c:670: temp1=(temp&0xFE);
;	genAnd
	mov	a,#0xFE
	anl	a,r4
	mov	r5,a
;	main.c:671: tempaddr=addr;
;	genCast
	mov	ar6,r2
;	main.c:672: startbit();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_startbit
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:673: send_data(temp1);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	push	ar6
	lcall	_send_data
	pop	ar6
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:674: send_data(tempaddr);
;	genCall
	mov	dpl,r6
	push	ar2
	push	ar3
	push	ar4
	lcall	_send_data
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:675: startbit();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	lcall	_startbit
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:676: temp=temp | 0x01;
;	genOr
	orl	ar4,#0x01
;	main.c:677: send_data(temp);
;	genCall
	mov	dpl,r4
	push	ar2
	push	ar3
	lcall	_send_data
	pop	ar3
	pop	ar2
;	main.c:678: clock_func();
;	genCall
	push	ar2
	push	ar3
	lcall	_clock_func
	pop	ar3
	pop	ar2
;	main.c:679: backup=ACC;
;	genAssign
;	main.c:680: printf("\n\r0x%03X: 0x%02X\n\r", addr, backup);
;	genAssign
	mov	dptr,#_eebyter_backup_1_1
	mov	a,_ACC
	movx	@dptr,a
;	Peephole 180.a	removed redundant mov to dptr
	movx	a,@dptr
	mov	r4,a
;	genCast
	mov	r5,#0x00
;	genIpush
	push	ar4
	push	ar5
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_25
	push	acc
	mov	a,#(__str_25 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xf9
	mov	sp,a
;	main.c:681: stopbit();
;	genCall
	lcall	_stopbit
;	main.c:682: return 0;
;	genRet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0000
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'readall_function'
;------------------------------------------------------------
;address                   Allocated with name '_readall_function_address_1_1'
;temp                      Allocated with name '_readall_function_temp_1_1'
;temp1                     Allocated with name '_readall_function_temp1_1_1'
;tempaddr                  Allocated with name '_readall_function_tempaddr_1_1'
;backup                    Allocated with name '_readall_function_backup_1_1'
;i                         Allocated with name '_readall_function_i_1_1'
;tempvalue                 Allocated with name '_readall_function_tempvalue_1_1'
;tempvalue1                Allocated with name '_readall_function_tempvalue1_1_1'
;------------------------------------------------------------
;	main.c:686: void readall_function()
;	-----------------------------------------
;	 function readall_function
;	-----------------------------------------
_readall_function:
;	main.c:691: putstr("Enter the address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_26
	mov	b,#0x80
	lcall	_putstr
;	main.c:692: do
00103$:
;	main.c:694: tempvalue = ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r2,dpl
	mov	r3,dph
;	genAssign
	mov	dptr,#_readall_function_tempvalue_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:695: if(tempvalue>0x7FF)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r2
	mov	a,#0x07
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00134$
;	main.c:696: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00104$:
;	main.c:697: }while(tempvalue>0x7FF);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00135$
;	main.c:698: putstr("\n\rEnter the second higher address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_28
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
;	main.c:699: do
00108$:
;	main.c:701: tempvalue1 = ReadVariable();
;	genCall
	push	ar2
	push	ar3
	lcall	_ReadVariable
	mov	r4,dpl
	mov	r5,dph
	pop	ar3
	pop	ar2
;	main.c:702: if(tempvalue1>0x7FF)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r4
	mov	a,#0x07
	subb	a,r5
	clr	a
	rlc	a
;	genIfx
	mov	r6,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00109$
;	Peephole 300	removed redundant label 00136$
;	main.c:703: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_putstr
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
00109$:
;	main.c:704: }while(tempvalue1>0x7FF);
;	genIfx
	mov	a,r6
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
;	main.c:708: address &=0x700;
;	genAnd
	jnz	00108$
;	Peephole 300	removed redundant label 00137$
;	Peephole 256.c	loading r6 with zero from a
	mov	r6,a
	mov	a,#0x07
	anl	a,r3
;	main.c:709: address >>=7;
;	genRightShift
;	genRightShiftLiteral
;	genrshTwo
	mov	r7,a
;	Peephole 105	removed redundant mov
	mov	c,acc.7
	xch	a,r6
	rlc	a
	xch	a,r6
	rlc	a
	xch	a,r6
	anl	a,#0x01
	mov	r7,a
;	main.c:710: temp |= address;
;	genOr
	orl	ar6,#0xA0
;	genCast
;	main.c:711: temp1=(temp&0xFE);
;	genAnd
	mov	a,#0xFE
	anl	a,r6
	mov	r7,a
;	main.c:712: tempaddr=tempvalue;
;	genCast
;	main.c:713: startbit();
;	genCall
	push	ar2
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_startbit
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar2
;	main.c:714: send_data(temp1);
;	genCall
	mov	dpl,r7
	push	ar2
	push	ar4
	push	ar5
	push	ar6
	lcall	_send_data
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar2
;	main.c:715: send_data(tempaddr);
;	genCall
	mov	dpl,r2
	push	ar4
	push	ar5
	push	ar6
	lcall	_send_data
	pop	ar6
	pop	ar5
	pop	ar4
;	main.c:716: startbit();
;	genCall
	push	ar4
	push	ar5
	push	ar6
	lcall	_startbit
	pop	ar6
	pop	ar5
	pop	ar4
;	main.c:717: temp=temp | 0x01;
;	genOr
	orl	ar6,#0x01
;	main.c:718: send_data(temp);
;	genCall
	mov	dpl,r6
	push	ar4
	push	ar5
	lcall	_send_data
	pop	ar5
	pop	ar4
;	main.c:719: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:720: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:721: putstr("EEPROM data\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_29
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:722: putstr("ADDR |  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_30
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:723: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
;	main.c:724: while(tempvalue<=tempvalue1)
00118$:
;	genAssign
	mov	dptr,#_readall_function_tempvalue_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpGt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,r2
	mov	a,r5
	subb	a,r3
;	genIfxJump
	jnc	00138$
	ljmp	00120$
00138$:
;	main.c:726: printf("0x%03X: ", tempvalue);
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_31
	push	acc
	mov	a,#(__str_31 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:727: for(i=0; i<=15; i++)
;	genAssign
;	genAssign
	mov	r6,#0x00
00114$:
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r6
	add	a,#0xff - 0x0F
	jnc	00139$
	ljmp	00117$
00139$:
;	main.c:729: clock_func();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_clock_func
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:730: backup=ACC;
;	genAssign
;	main.c:731: printf("%02X  ", backup);
;	genAssign
	mov	dptr,#_readall_function_backup_1_1
	mov	a,_ACC
	movx	@dptr,a
;	Peephole 180.a	removed redundant mov to dptr
	movx	a,@dptr
	mov	r7,a
;	genCast
	mov	r0,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
;	genIpush
	mov	a,#__str_32
	push	acc
	mov	a,#(__str_32 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:732: if(tempvalue!=tempvalue1)
;	genCmpEq
;	gencjneshort
	mov	a,r2
	cjne	a,ar4,00140$
	mov	a,r3
	cjne	a,ar5,00140$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00112$
00140$:
;	main.c:734: Master_ack();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	lcall	_Master_ack
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:735: tempvalue++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00141$
	inc	r3
00141$:
;	genAssign
	mov	dptr,#_readall_function_tempvalue_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00116$
00112$:
;	main.c:739: tempvalue++;
;	genPlus
	mov	dptr,#_readall_function_tempvalue_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	main.c:740: break;
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00116$:
;	main.c:727: for(i=0; i<=15; i++)
;	genPlus
;     genPlusIncr
	inc	r6
	ljmp	00114$
00117$:
;	main.c:744: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
	ljmp	00118$
00120$:
;	main.c:746: stopbit();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_stopbit
;
;------------------------------------------------------------
;Allocation info for local variables in function 'write_function'
;------------------------------------------------------------
;temp                      Allocated with name '_write_function_temp_1_1'
;temp1                     Allocated with name '_write_function_temp1_1_1'
;------------------------------------------------------------
;	main.c:751: void write_function()
;	-----------------------------------------
;	 function write_function
;	-----------------------------------------
_write_function:
;	main.c:754: putstr("Enter the address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_26
	mov	b,#0x80
	lcall	_putstr
;	main.c:755: do
00103$:
;	main.c:757: temp = ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r2,dpl
	mov	r3,dph
;	main.c:758: if(temp>0x7FF)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r2
	mov	a,#0x07
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00119$
;	main.c:759: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00104$:
;	main.c:760: }while(temp>0x7FF);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00120$
;	main.c:761: putstr("\n\rEnter the Value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_33
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
;	main.c:762: do
00108$:
;	main.c:764: temp1=ReadVariable();
;	genCall
	push	ar2
	push	ar3
	lcall	_ReadVariable
	mov	r4,dpl
	mov	r5,dph
	pop	ar3
	pop	ar2
;	main.c:765: if(temp1>0x7FF)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r4
	mov	a,#0x07
	subb	a,r5
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00109$
;	Peephole 300	removed redundant label 00121$
;	main.c:766: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	lcall	_putstr
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
00109$:
;	main.c:767: }while(temp1>0xFF);
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r4
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r5
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00108$
;	Peephole 300	removed redundant label 00122$
;	main.c:769: eebytew(temp, temp1);
;	genCast
	mov	dptr,#_eebytew_PARM_2
	mov	a,r4
	movx	@dptr,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_eebytew
;
;------------------------------------------------------------
;Allocation info for local variables in function 'help_menu'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:773: void help_menu()
;	-----------------------------------------
;	 function help_menu
;	-----------------------------------------
_help_menu:
;	main.c:775: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_34
	mov	b,#0x80
	lcall	_putstr
;	main.c:776: putstr("EEPROM Control\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_35
	mov	b,#0x80
	lcall	_putstr
;	main.c:777: putstr("Press 'R' to read a byte from the user input address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_36
	mov	b,#0x80
	lcall	_putstr
;	main.c:778: putstr("Press 'W' to write a byte data to user input address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_37
	mov	b,#0x80
	lcall	_putstr
;	main.c:779: putstr("Press 'S' for sequential read between the adresses\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_38
	mov	b,#0x80
	lcall	_putstr
;	main.c:780: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	lcall	_putstr
;	main.c:781: putstr("LCD Control\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_39
	mov	b,#0x80
	lcall	_putstr
;	main.c:782: putstr("Press 'C' for CGRAM HEX dump\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_40
	mov	b,#0x80
	lcall	_putstr
;	main.c:783: putstr("Press 'D' for DDRAM HEX dump\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_41
	mov	b,#0x80
	lcall	_putstr
;	main.c:784: putstr("Press 'E' for Clearing the LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_42
	mov	b,#0x80
	lcall	_putstr
;	main.c:785: putstr("Press 'P' to display the custom character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_43
	mov	b,#0x80
	lcall	_putstr
;	main.c:786: putstr("Press 'N' to add custom character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_44
	mov	b,#0x80
	lcall	_putstr
;	main.c:787: putstr("Press 'I' for printing letters on LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_45
	mov	b,#0x80
	lcall	_putstr
;	main.c:788: putstr("Press 'K' for displaying the logo\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_46
	mov	b,#0x80
	lcall	_putstr
;	main.c:789: putstr("Press 'X' for moving the cursor to user input x,y position\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_47
	mov	b,#0x80
	lcall	_putstr
;	main.c:790: putstr("Press 'G' for moving the cursor to user input address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_48
	mov	b,#0x80
	lcall	_putstr
;	main.c:791: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_34
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'read_function'
;------------------------------------------------------------
;temp1                     Allocated with name '_read_function_temp1_1_1'
;------------------------------------------------------------
;	main.c:794: void read_function()
;	-----------------------------------------
;	 function read_function
;	-----------------------------------------
_read_function:
;	main.c:797: putstr("Enter the address in hex\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_22
	mov	b,#0x80
	lcall	_putstr
;	main.c:798: do
00103$:
;	main.c:800: temp1=ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r2,dpl
	mov	r3,dph
;	main.c:801: if(temp1>0x7FF)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0xFF
	subb	a,r2
	mov	a,#0x07
	subb	a,r3
	clr	a
	rlc	a
;	genIfx
	mov	r4,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00104$
;	Peephole 300	removed redundant label 00111$
;	main.c:802: putstr("Enter the correct value\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
00104$:
;	main.c:803: }while(temp1>0x7FF);
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00112$
;	main.c:804: eebyter(temp1);
;	genCall
	mov	dpl,r2
	mov	dph,r3
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_eebyter
;
;------------------------------------------------------------
;Allocation info for local variables in function 'custom_character'
;------------------------------------------------------------
;in_array                  Allocated with name '_custom_character_in_array_1_1'
;in_array5                 Allocated with name '_custom_character_in_array5_1_1'
;in_array1                 Allocated with name '_custom_character_in_array1_1_1'
;in_array2                 Allocated with name '_custom_character_in_array2_1_1'
;in_array3                 Allocated with name '_custom_character_in_array3_1_1'
;in_array4                 Allocated with name '_custom_character_in_array4_1_1'
;in_array6                 Allocated with name '_custom_character_in_array6_1_1'
;in_array7                 Allocated with name '_custom_character_in_array7_1_1'
;k                         Allocated with name '_custom_character_k_1_1'
;------------------------------------------------------------
;	main.c:808: void custom_character()
;	-----------------------------------------
;	 function custom_character
;	-----------------------------------------
_custom_character:
;	main.c:811: unsigned char in_array[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array_1_1
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0001)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0002)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0003)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0004)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0005)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0006)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array_1_1 + 0x0007)
	mov	a,#0x0C
	movx	@dptr,a
;	main.c:812: unsigned char in_array5[] = {0x0f, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array5_1_1
	mov	a,#0x0F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0001)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0002)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0003)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0004)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0005)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0006)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array5_1_1 + 0x0007)
	mov	a,#0x0F
	movx	@dptr,a
;	main.c:813: unsigned char in_array1[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x1f, 0x1f, 0x1f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array1_1_1
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0001)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0002)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0003)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0004)
	mov	a,#0x0C
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0005)
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0006)
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array1_1_1 + 0x0007)
	mov	a,#0x1F
	movx	@dptr,a
;	main.c:814: unsigned char in_array2[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x03};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array2_1_1
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0001)
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0002)
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0003)
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0004)
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0005)
	mov	a,#0x03
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0006)
	mov	a,#0x03
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array2_1_1 + 0x0007)
	mov	a,#0x03
	movx	@dptr,a
;	main.c:815: unsigned char in_array3[] = {0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array3_1_1
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0001)
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0002)
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0003)
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0004)
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0005)
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0006)
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array3_1_1 + 0x0007)
	mov	a,#0x1F
	movx	@dptr,a
;	main.c:816: unsigned char in_array4[] = {0x1f, 0x00, 0x04, 0x0A, 0x0A, 0x04, 0x00, 0x1f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array4_1_1
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0001)
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0002)
	mov	a,#0x04
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0003)
	mov	a,#0x0A
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0004)
	mov	a,#0x0A
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0005)
	mov	a,#0x04
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0006)
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array4_1_1 + 0x0007)
	mov	a,#0x1F
	movx	@dptr,a
;	main.c:817: unsigned char in_array6[] = {0x1f, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x1f};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array6_1_1
	mov	a,#0x1F
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0001)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0002)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0003)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0004)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0005)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0006)
	mov	a,#0x01
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array6_1_1 + 0x0007)
	mov	a,#0x1F
	movx	@dptr,a
;	main.c:818: unsigned char in_array7[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x018, 0x18};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_custom_character_in_array7_1_1
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0001)
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0002)
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0003)
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0004)
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0005)
	mov	a,#0x18
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0006)
	mov	a,#0x18
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_custom_character_in_array7_1_1 + 0x0007)
	mov	a,#0x18
	movx	@dptr,a
;	main.c:820: k=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:821: lcdclear();
;	genCall
	push	ar2
	lcall	_lcdclear
	pop	ar2
;	main.c:822: putstr("Look at LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_49
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:823: lcdcreatechar(0x00, in_array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:824: lcdcreatechar(0x08, in_array1);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array1_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array1_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x08
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:825: lcdcreatechar(0x10, in_array2);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array2_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array2_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x10
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:826: lcdcreatechar(0x18, in_array3);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array3_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array3_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x18
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:827: lcdcreatechar(0x20, in_array4);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array4_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array4_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x20
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:828: lcdcreatechar(0x28, in_array5);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array5_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array5_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x28
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:829: lcdcreatechar(0x30, in_array6);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array6_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array6_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x30
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:830: lcdcreatechar(0x38, in_array7);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_custom_character_in_array7_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,#(_custom_character_in_array7_1_1 >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x38
	push	ar2
	lcall	_lcdcreatechar
	pop	ar2
;	main.c:831: lcdgotoxy(3,2);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x02
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:832: lcdputch(0x01);
;	genCall
	mov	dpl,#0x01
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:833: lcdgotoxy(2,2);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x02
	movx	@dptr,a
;	genCall
	mov	dpl,#0x02
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:834: lcdputch(0x00);
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:835: lcdgotoxy(1,2);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x02
	movx	@dptr,a
;	genCall
	mov	dpl,#0x01
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:836: lcdputch(0x00);
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:837: lcdgotoxy(0,2);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x02
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:838: lcdputch(0x05);
;	genCall
	mov	dpl,#0x05
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:839: lcdgotoxy(3,1);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x01
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:840: lcdputch(0x02);
;	genCall
	mov	dpl,#0x02
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:841: lcdgotoxy(3,3);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x03
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:842: lcdputch(0x07);
;	genCall
	mov	dpl,#0x07
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:843: lcdgotoxy(0,3);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x03
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:844: lcdputch(0x03);
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:845: lcdgotoxy(0,4);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x04
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:846: lcdputch(0x04);
;	genCall
	mov	dpl,#0x04
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:847: lcdgotoxy(0,5);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x05
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:848: lcdputch(0x06);
;	genCall
	mov	dpl,#0x06
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:849: lcdgotoxy(3,9);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x09
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:850: lcdputch(0x01);
;	genCall
	mov	dpl,#0x01
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:851: lcdgotoxy(2,9);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x09
	movx	@dptr,a
;	genCall
	mov	dpl,#0x02
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:852: lcdputch(0x00);
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:853: lcdgotoxy(1,9);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x09
	movx	@dptr,a
;	genCall
	mov	dpl,#0x01
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:854: lcdputch(0x00);
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:855: lcdgotoxy(0,9);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x09
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:856: lcdputch(0x05);
;	genCall
	mov	dpl,#0x05
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:857: lcdgotoxy(3,8);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x08
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:858: lcdputch(0x02);
;	genCall
	mov	dpl,#0x02
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:859: lcdgotoxy(3,10);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x0A
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:860: lcdputch(0x07);
;	genCall
	mov	dpl,#0x07
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:861: lcdgotoxy(0,10);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x0A
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:862: lcdputch(0x03);
;	genCall
	mov	dpl,#0x03
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:863: lcdgotoxy(0,11);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x0B
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:864: lcdputch(0x04);
;	genCall
	mov	dpl,#0x04
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:865: lcdgotoxy(0,12);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x0C
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	push	ar2
	lcall	_lcdgotoxy
	pop	ar2
;	main.c:866: lcdputch(0x06);
;	genCall
	mov	dpl,#0x06
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:867: *INST_WRITE=(k|0x80);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:868: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;in                        Allocated with name '_main_in_1_1'
;------------------------------------------------------------
;	main.c:871: void main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	main.c:874: SerialInitialize();
;	genCall
	lcall	_SerialInitialize
;	main.c:875: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	lcall	_putstr
;	main.c:876: lcdinit();
;	genCall
	lcall	_lcdinit
;	main.c:877: *INST_WRITE=(0x80);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x80
	movx	@dptr,a
;	main.c:878: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:879: lcdgotoxy(1,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x01
	lcall	_lcdgotoxy
;	main.c:880: lcdputstr("YOU KNOW NOTHING");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_50
	mov	b,#0x80
	lcall	_lcdputstr
;	main.c:881: lcdgotoxy(2,4);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x04
	movx	@dptr,a
;	genCall
	mov	dpl,#0x02
	lcall	_lcdgotoxy
;	main.c:882: lcdputstr("JON SNOW");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_51
	mov	b,#0x80
	lcall	_lcdputstr
;	main.c:883: MSDelay(100);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0064
	lcall	_MSDelay
;	main.c:884: putstr("Welcome\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_52
	mov	b,#0x80
	lcall	_putstr
;	main.c:885: putstr("Press '>' for help menu\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_53
	mov	b,#0x80
	lcall	_putstr
;	main.c:886: putstr("Enter the character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_54
	mov	b,#0x80
	lcall	_putstr
;	main.c:887: while(1)
00117$:
;	main.c:889: putstr("\n\rEnter the character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_55
	mov	b,#0x80
	lcall	_putstr
;	main.c:890: in=getchar();
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	main.c:891: switch(in)
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x3E,00135$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00104$
00135$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x43,00136$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00107$
00136$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x44,00137$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00108$
00137$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x45,00138$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00109$
00138$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x47,00139$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00106$
00139$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x49,00140$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00112$
00140$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x4B,00141$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00141$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x4E,00142$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00110$
00142$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x50,00143$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00143$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x52,00144$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00102$
00144$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x53,00145$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00103$
00145$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x57,00146$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00146$:
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	main.c:893: case 'W' : write_function();
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x58,00114$
	sjmp	00105$
;	Peephole 300	removed redundant label 00147$
00101$:
;	genCall
	lcall	_write_function
;	main.c:894: break;
;	main.c:895: case 'R' : read_function();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00102$:
;	genCall
	lcall	_read_function
;	main.c:896: break;
;	main.c:897: case 'S' : readall_function();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00103$:
;	genCall
	lcall	_readall_function
;	main.c:898: break;
;	main.c:899: case '>' : help_menu();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00104$:
;	genCall
	lcall	_help_menu
;	main.c:900: break;
;	main.c:901: case 'X' : gotoxy();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00105$:
;	genCall
	lcall	_gotoxy
;	main.c:902: break;
;	main.c:903: case 'G' : gotoaddress();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00106$:
;	genCall
	lcall	_gotoaddress
;	main.c:904: break;
;	main.c:905: case 'C' : cgramdump();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00107$:
;	genCall
	lcall	_cgramdump
;	main.c:906: break;
;	main.c:907: case 'D' : ddramdump();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00108$:
;	genCall
	lcall	_ddramdump
;	main.c:908: break;
;	main.c:909: case 'E' : lcdclear();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00117$
00109$:
;	genCall
	lcall	_lcdclear
;	main.c:910: break;
	ljmp	00117$
;	main.c:911: case 'N' : addcharacter();
00110$:
;	genCall
	lcall	_addcharacter
;	main.c:912: break;
	ljmp	00117$
;	main.c:913: case 'P' : display();
00111$:
;	genCall
	lcall	_display
;	main.c:914: break;
	ljmp	00117$
;	main.c:915: case 'I' : interactive();
00112$:
;	genCall
	lcall	_interactive
;	main.c:916: break;
	ljmp	00117$
;	main.c:917: case 'K' : custom_character();
00113$:
;	genCall
	lcall	_custom_character
;	main.c:918: break;
	ljmp	00117$
;	main.c:919: default: putstr("Enter the correct option\n\r");
00114$:
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_56
	mov	b,#0x80
	lcall	_putstr
;	main.c:920: }
	ljmp	00117$
;	Peephole 259.b	removed redundant label 00119$ and ret
;
	.area CSEG    (CODE)
	.area CONST   (CODE)
__str_0:
	.db 0x0A
	.db 0x0D
	.ascii "Enter Numbers"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_1:
	.ascii "Clearing the LCD completed"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_2:
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_3:
	.ascii "CGRAM values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_4:
	.db 0x0A
	.db 0x0D
	.ascii "CCODE |  Values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_5:
	.ascii "0x%04x:  "
	.db 0x00
__str_6:
	.ascii "%02x  "
	.db 0x00
__str_7:
	.ascii "DDRAM Values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_8:
	.db 0x0A
	.db 0x0D
	.ascii "ADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C "
	.ascii " +D  +E  +F"
	.db 0x0A
	.db 0x0D
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_9:
	.ascii "0x%02x:  "
	.db 0x00
__str_10:
	.db 0x0A
	.db 0x0D
	.ascii "Enter Valid Numbers"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_11:
	.db 0x0A
	.db 0x0D
	.ascii "Enter eight pixel values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_12:
	.ascii "Enter five binary values for every pixel"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_13:
	.ascii "Invalid Number. Enter Again"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_14:
	.ascii "Eight custom character created. No more custom character ple"
	.ascii "ase"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_15:
	.ascii "Look at the LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_17:
	.ascii "Enter the character to display. Hit enter to stop"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_18:
	.db 0x0A
	.db 0x0D
	.ascii "Enter row number"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_19:
	.db 0x0A
	.db 0x0D
	.ascii "Enter correct row"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_20:
	.db 0x0A
	.db 0x0D
	.ascii "Enter column number"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_21:
	.db 0x0A
	.db 0x0D
	.ascii "Enter correct column number"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_22:
	.ascii "Enter the address in hex"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_23:
	.db 0x0A
	.db 0x0D
	.ascii "Enter correct address values in hex"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_24:
	.db 0x0A
	.db 0x0D
	.ascii "Writing 0x%03X: 0x%02X"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_25:
	.db 0x0A
	.db 0x0D
	.ascii "0x%03X: 0x%02X"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_26:
	.ascii "Enter the address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_27:
	.ascii "Enter the correct value"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_28:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the second higher address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_29:
	.ascii "EEPROM data"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_30:
	.ascii "ADDR |  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  "
	.ascii "+D  +E  +F"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_31:
	.ascii "0x%03X: "
	.db 0x00
__str_32:
	.ascii "%02X  "
	.db 0x00
__str_33:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the Value"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_34:
	.db 0x0A
	.db 0x0D
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_35:
	.ascii "EEPROM Control"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_36:
	.ascii "Press 'R' to read a byte from the user input address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_37:
	.ascii "Press 'W' to write a byte data to user input address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_38:
	.ascii "Press 'S' for sequential read between the adresses"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_39:
	.ascii "LCD Control"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_40:
	.ascii "Press 'C' for CGRAM HEX dump"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_41:
	.ascii "Press 'D' for DDRAM HEX dump"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_42:
	.ascii "Press 'E' for Clearing the LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_43:
	.ascii "Press 'P' to display the custom character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_44:
	.ascii "Press 'N' to add custom character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_45:
	.ascii "Press 'I' for printing letters on LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_46:
	.ascii "Press 'K' for displaying the logo"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_47:
	.ascii "Press 'X' for moving the cursor to user input x,y position"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_48:
	.ascii "Press 'G' for moving the cursor to user input address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_49:
	.ascii "Look at LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_50:
	.ascii "YOU KNOW NOTHING"
	.db 0x00
__str_51:
	.ascii "JON SNOW"
	.db 0x00
__str_52:
	.ascii "Welcome"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_53:
	.ascii "Press '>' for help menu"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_54:
	.ascii "Enter the character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_55:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_56:
	.ascii "Enter the correct option"
	.db 0x0A
	.db 0x0D
	.db 0x00
	.area XINIT   (CODE)
__xinit__confirm:
	.byte #0x01,#0x00
