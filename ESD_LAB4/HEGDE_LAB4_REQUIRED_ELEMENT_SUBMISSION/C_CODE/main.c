/*

UNIVERSITY OF COLORADO BOULDER

C PROGRAM WHICH DEALS WITH I2C COMMUNICATION WITH EEPROM 24LC16B
AND WITH LCD MODULE.
PROVIDES A FRIENDLY COMMAND LINE INTERFACE FOR THE USER

WRITTEN BY KIRAN NARAYANA HEGDE (kihe6592@colorado.edu)

SUBMITTED AS PART OF THE LAB4 REQUIRED ELEMENT SUBMISSION ECEN5613 - EMBEDDED SYSTEM DESIGN

SPECIAL THANKS TO SDCC LIBRARY
 */

/* INCLUDE ALL THE HEADER FILE NEEDED*/
#include <mcs51/8051.h>
#include <at89c51ed2.h>
#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>

/*DEFINING PINS FOR I2C COMMUNICATION*/
#define SDA P1.4
#define SCL P1.3
#define SDA_C P1_4
#define SCL_C P1_3

/* DEFINING THE MACRO FOR MEMORY MAPPED PERIPHERAL COMMUNICATION */
#define INST_WRITE ((xdata unsigned char *)0xF000)
#define DATA_WRITE ((xdata unsigned char *)0xF400)
#define DATA_READ ((xdata unsigned char *)0xFC00)
#define INST_READ ((xdata unsigned char *)0xF800)

/* DEFINING THE BASE ADDRESS FOR LCD ROWS */
#define BASE_ADDRROW0 0x00
#define BASE_ADDRROW1 0x40
#define BASE_ADDRROW2 0x10
#define BASE_ADDRROW3 0x50


/*DEFINING GLOBAL VARIABLES */
void putstr(char *);
unsigned volatile int t;
unsigned char rec;
unsigned char array[8];
volatile unsigned char u;

unsigned int confirm=1;

/* DELAY FUNCTION DEFINED FOR LCD COMMUNICATION */
void MSDelay(unsigned int value)
  {
    unsigned int x, y;
    for(y=0;y<value;y++)
    {
        for(x=0;x<126;x++)
        {
            ;
        }
    }

  }

  /* Description: Polls the LCD busy flag. Function does not return
  until the LCD controller is ready to accept another command */
void lcdbusywait()
{
    unsigned volatile int i;
    i=*INST_READ;
    MSDelay(1);
    while((i&0x80)==0x80)
    {
        i = *INST_READ;
        MSDelay(1);
    }
}

/* FUNCTION TO GET THE USER INPUT FROM SERIAL TERMINAL */
unsigned char input_value()
{
    unsigned volatile char l=0, m=0;
    do
    {
        l=getchar();
        if(l>='0' && l<='9')
        {
            m=((m*10)+(l-'0'));
            putchar(l);                 /* CONVERTING IT TO DECIMAL VALUE*/
            //k++;
        }
        else if(l==127)
        {
            m=m/10;
        }
        else if(l!=13)
            {putstr("\n\rEnter Numbers\n\r");}
    }while(l!=13);
    return m;                                   /* UNTIL ENTER IS PRESSES*/
}

/*GOTO ADDRESS FOR THE EEPROM. USER HAS TO INPUT THE ADDRESS*/
void lcdgotoaddr(unsigned char addr)
{
    if(addr>=0x00 && addr<=0x67)
    {
        unsigned char temp;
        temp = 0x80;
        temp |= addr;
        *INST_WRITE = temp;
        lcdbusywait();
    }
}

/* GO TO THE X ROW, Y COLUMN CURSOR POSITION */
void lcdgotoxy(unsigned char row, unsigned char column)
{
        switch(row)
        {
            case 0: lcdgotoaddr((BASE_ADDRROW0+column));
                    break;
            case 1: lcdgotoaddr((BASE_ADDRROW1+column));
                    break;
            case 2: lcdgotoaddr((BASE_ADDRROW2+column));
                    break;
            case 3: lcdgotoaddr((BASE_ADDRROW3+column));
                    break;
        }
}

/* CLEARS THE LCD AND MOVES THE CURSOR TO HOME POSITION*/
void lcdclear()
{
    *INST_WRITE = 0x01;
    lcdbusywait();
    putstr("Clearing the LCD completed\n\r");
}

/* DISPLAY A CHARACTER IN THE LCD */
void lcdputch(unsigned char cc)
{
    unsigned volatile int i, temp;
    *DATA_WRITE = cc;
    lcdbusywait();
    i=*INST_READ;
        temp = i&0x7F;              /* THIS LOOP HANDLES THE ROW ADDRESS MISMATCH OF THE LCD*/
        if(temp==0x10)
        {
            lcdgotoxy(1,0);
        }
        else if(temp==0x50)
        {
            lcdgotoxy(2,0);
        }
        else if(temp==0x20)
        {
            lcdgotoxy(3,0);
        }
        else if(temp==0x60)
        {
            lcdgotoxy(0,0);//lcdgotoaddr(temp);
        }
        else
        {
            ;
        }
}

/* DISPLAY STIRING ON THE SERIAL TERMINAL */
void putstr(char *ptr)
{
        while(*ptr)
        {
            putchar(*ptr);
            ptr++;
        }

}

/* dISPLAY STRING ON THE LCD*/
void lcdputstr(unsigned char *ptr)
{
    while(*ptr)
    {
        lcdputch(*ptr);
        ptr++;
    }
}

/*-----------------------------------------------------------------------------------------------*/
/* Name: lcdinit()
 Description: Initializes the LCD (see Figure25 on page 212 )
  of the HD44780U data sheet)
*/
void lcdinit()
{
    MSDelay(20);
   *INST_WRITE = 0x30;
   MSDelay(10);
   *INST_WRITE = 0x30;
   MSDelay(10);
   *INST_WRITE = 0x30;
   lcdbusywait();
   *INST_WRITE = 0x38;
   lcdbusywait();
   *INST_WRITE = 0x08;
   lcdbusywait();
   *INST_WRITE = 0x0C;
   lcdbusywait();
   *INST_WRITE = 0x06;
   lcdbusywait();
   *INST_WRITE = 0x01;
}

/* DISPLAY THE HEX VALUES OF CGRAM DATA*/
void cgramdump()
{
    unsigned int count_row=0;
    unsigned int count=0;
    unsigned char temp_val=0x40, k;
    k=*INST_READ;
    *INST_WRITE = temp_val;
    lcdbusywait();
    putstr("\n\r");
    putstr("CGRAM values\n\r");
    putstr("\n\rCCODE |  Values\n\r");
        for (count_row=0; count_row<=7; count_row++)
        {
            printf("0x%04x:  ", (temp_val&0x3F));
            for(count=0; count<=7; count++)
            {
                temp_val++;
                printf("%02x  ",*DATA_READ);
                lcdbusywait();
            }
            putstr("\n\r");
        }
        putstr("\n\r");
        *INST_WRITE=(k|0x80);
        lcdbusywait();

}

/* CREATE A NEW CUSTOM CHARACTER */
void lcdcreatechar(unsigned char ccode, unsigned char row_vals[])
{
    unsigned char n, k;
    k=*INST_READ;
    lcdbusywait();
    ccode |= 0x40;
    for (n=0; n<=7; n++)
    {
        *INST_WRITE = ccode;
        lcdbusywait();
        *DATA_WRITE = (row_vals[n]&0x1F);
        lcdbusywait();
        ccode++;
    }
    *INST_WRITE=(k|0x80);
    lcdbusywait();
}

/* DISPLAY THE HEX VALUES OF DDRAM DATA*/
void ddramdump()
{
    unsigned int count_row=0;
    unsigned int count=0;
    unsigned char temp_val=0, k;
    k=*INST_READ;
    putstr("\n\r");
    putstr("DDRAM Values\n\r");
    putstr("\n\rADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r\n\r");
        for (count_row=0; count_row<=3; count_row++)
        {
            if(count_row==0)
                temp_val=0x80;
            if(count_row==1)
                temp_val=0xC0;
            if(count_row==2)
                temp_val=0x90;
            if(count_row==3)
                temp_val=0xD0;
            printf("0x%02x:  ", (temp_val&0x7F));
            for(count=0; count<=15; count++)
            {
                *INST_WRITE = temp_val;
                lcdbusywait();
                temp_val++;
                printf("%02x  ",*DATA_READ);
                lcdbusywait();
            }
            putstr("\n\r");
        }
        putstr("\n\r");
        *INST_WRITE=(k|0x80);
}

/* READS THE BINARY INPUT FOR THE CUSTOM CHARACTER GENERATION FUNCTION*/
unsigned char readinput()
{
    unsigned char value=0, i, j=0;
    do
    {
        i=getchar();
        if(i>='0'&&i<='1')          /* works only if its a number */
        {
            value=((value*2)+(i-'0')); /* Convert character to integers */
            putchar(i);
            j++;
        }
        else if(i==127)
        {
            value=value/2;
        }
        else if(i!=13)
            {putstr("\n\rEnter Valid Numbers\n\r");}
    }while(i!=13);
    j=0;
    return value;
}

/* cREATES A CUSTOM CHARACTER */
void addcharacter()
{
    unsigned char temp, y;
    if(u<8)
    {
        putstr("\n\rEnter eight pixel values\n\r");
        putstr("Enter five binary values for every pixel\n\r");
    //if(add_val<=64)
    //{
        for(temp=0; temp<=7; temp++)
        {
            y=readinput();
            if(y>0x1F)
            {
                putstr("Invalid Number. Enter Again\n\r");
                temp--;
            }
            else
            {
              array[temp]=y;
            putstr("\n\r");
            }
        }
        if(u==0)
            lcdcreatechar(0x00, array);
        else if(u==1)
            lcdcreatechar(0x08, array);
        else if(u==2)
            lcdcreatechar(0x10, array);
        else if(u==3)
            lcdcreatechar(0x18, array);
        else if(u==4)
            lcdcreatechar(0x20, array);
        else if(u==5)
            lcdcreatechar(0x28, array);
        else if(u==6)
            lcdcreatechar(0x30, array);
        else if(u==7)
            lcdcreatechar(0x38, array);
        u++;
    }
    else
    {
        putstr("Eight custom character created. No more custom character please\n\r");
    }

}
 /* DISPLAY THE CUSTOM CHARACTER */
void display()
{
    unsigned char hey=0;
    putstr("Look at the LCD\n\r");
    while(hey<0x8)
    {
        lcdputch(hey);
        hey++;
    }
}

/* CONVERT HEX VALUES FROM THE HEX STRING
STACKOVERFLOW: https://stackoverflow.com/questions/10324/convert-a-hexadecimal-string-to-an-integer-efficiently-in-c
WRITTEN BY MARKUS SAFAR
*/
unsigned long hex2int(char *a, unsigned int len)
{
    int i;
    unsigned long val = 0;

    for(i=0;i<len;i++)
       if(a[i] <= 57)
        val += (a[i]-48)*(1<<(4*(len-1-i)));
       else
        val += (a[i]-55)*(1<<(4*(len-1-i)));

    return val;
}

uint16_t ReadVariable()
{
    uint16_t l=0, m=0, n, k=0;
    unsigned char ptr[]="";
    do
    {
        l=getchar();
        if(l!=13)
        {
            ptr[k]=l;
            k++;
            putchar(l);
         }
    }while(l!=13);
    n=hex2int(ptr,k);
    return n;
}

/* DYNAMICALLY PRITING THE DATA ON LCD */
void interactive()
{
    unsigned char co, i;
    i=*INST_READ;
    i=i&0x80;
    *INST_WRITE=i;
    putstr("Enter the character to display. Hit enter to stop\n\r");
    do
    {   co=getchar();
        putchar(co);
        if(co!=13)
            lcdputch(co);
    }while(co!=13);
    //*pointer='\0';
    //co=*INST_READ;
    //*INST_WRITE= (co|0x80);

}

void gotoxy()
{
    unsigned char temp=10, temp1=20;
    putstr("\n\rEnter row number\n\r");
    do
    {
        temp=input_value();
        if(temp>3)
            putstr("\n\rEnter correct row\n\r");
    }while(temp>3);
    putstr("\n\rEnter column number\n\r");
    do
    {
        temp1=input_value();
        if(temp1>15)
            putstr("\n\rEnter correct column number\n\r");
    }while(temp1>15);
    lcdgotoxy(temp, temp1);
}

void gotoaddress()
{
    unsigned char temp=0x66;
    putstr("Enter the address in hex\n\r");
    while((temp>0x1F && temp<0x40)||(temp>=0x5F))
    {
        temp=ReadVariable();
        if((temp>0x1F && temp<0x40)||(temp>=0x5F))
            putstr("\n\rEnter correct address values in hex\n\r");
    }
    lcdgotoaddr(temp);
    putstr("\n\r");
}

_sdcc_external_startup()
{
    AUXR |= 0x0C;
    return 0;
}



void delay()
{
_asm
    DELAY:MOV R5, #5
    DEL1: MOV R4, #10
    DEL: NOP
        DJNZ r4, DEL
         DJNZ R5, DEL1
         ret
_endasm ;
}


void SerialInitialize()
{
    TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
    TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
    SCON=0x50;
    TR1=1;  // to start timer
    IE=0x90; // to make serial interrupt interrupt enable
}

void putchar(char x)
{
    SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
    while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
    TI=0; // make TI bit to zero for next transmission
}


char getchar()
{
    char rec;
    /* RI bit is present in SCON register*/
    while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
    rec=SBUF;   // data is received in SBUF register present in controller during receiving
    RI=0;  // make RI bit to 0 so that next data is received
    return rec;  // return rec where function is called
}

/* START BIT CONDITION FOR THE I2C*/
void startbit()
{
    _asm
        SETB SCL
        SETB SDA
        ACALL DELAY
        CLR SDA
        ACALL DELAY
        CLR SCL
        ACALL DELAY
    _endasm ;
}

/* STOP BIT CONDITION FOR THE I2C*/
void stopbit()
{
    _asm
        ACALL DELAY
        CLR SDA
        ACALL DELAY
        SETB SCL
        ACALL DELAY
        SETB SDA
        ACALL DELAY
    _endasm;
}

/*ACKNOWLEDGEMENT CONDITION FOR RECEIVING */
void ackbit()
{
    _asm
        CLR C
        ACALL DELAY
        SETB SDA
        ACALL DELAY
        SETB SCL
        ACALL DELAY
        MOV C, SDA
        ACALL DELAY
        CLR SCL
        ACALL DELAY
    _endasm;
    //return SDA_C
}

/* SENDING ACKNOWLEDGEMENT TO SLAVE*/
void Master_ack()
{
    _asm
        ACALL DELAY
        ACALL DELAY
        CLR SDA
        ACALL DELAY
        SETB SCL
        ACALL DELAY
        ACALL DELAY
        CLR SCL
        nop
        setb SDA
    _endasm;
}

/* SENDING DATA TO ASSEMBLY */
void send_data_assembly()
{
    _asm
        ACALL DELAY
        MOV R1, #8
        HERE:CLR C
            RLC A
            JNC NEXT
            SETB SDA
            AJMP NEXT1
        NEXT: CLR SDA
        NEXT1:
            ACALL DELAY
            SETB SCL
            ACALL DELAY
            CLR SCL
            ACALL DELAY
            DJNZ R1, HERE
    _endasm;
}

int send_data(unsigned char value)
{
    int status_send=1;
    ACC = value;
    CY=1;
    send_data_assembly();
    ackbit();
    //printf("CY = %x\n\r", CY);
    ACC = 0x00;
    return status_send;
}

/* TOGGLES THE CLOCK FOR DATAREAD*/
void clock_func()
{
    _asm
            MOV A, #0
            MOV R7, #8
        LOOP2: NOP
                ACALL DELAY
                clr SCL
                ACALL DELAY
                setb SCL
                MOV C, SDA
                RLC A
                DJNZ R7, LOOP2
                clr SCL
                ACALL DELAY
                //SETB SCL
    _endasm;
}

/* WRITING A BYTE OF DATA */
int eebytew(uint16_t addr, unsigned char databyte)  // write byte, returns status
{
    uint16_t address;
    int result, result1, result2;
    unsigned char temp, tempaddr;
    printf("\n\rWriting 0x%03X: 0x%02X\n\r",addr, databyte);
    temp = 0xA0;
    address=addr;
    address &=0x700;
    address >>=7;
    temp |= address;
    temp &= 0xFE;
    tempaddr=addr;
    startbit();
    result = send_data(temp);
    result1 = send_data(tempaddr);
    result2 = send_data(databyte);
    stopbit();
    confirm=1;
    return (result&result1);
}

/* READING A BYTE OF DATA*/
int eebyter(uint16_t addr)
{
    uint16_t address;
    unsigned char temp, temp1, tempaddr, backup;
    temp = 0xA0;
    address=addr;
    address &=0x700;
    address >>=7;
    temp |= address;
    temp1=(temp&0xFE);
    tempaddr=addr;
    startbit();
    send_data(temp1);
    send_data(tempaddr);
    startbit();
    temp=temp | 0x01;
    send_data(temp);
    clock_func();
    backup=ACC;
    printf("\n\r0x%03X: 0x%02X\n\r", addr, backup);
    stopbit();
    return 0;
}

/* SEQUENTIAL READ FUNCTION */
void readall_function()
{
    uint16_t address;
    unsigned char temp, temp1, tempaddr, backup, i;
    uint16_t tempvalue, tempvalue1;
    putstr("Enter the address\n\r");
    do
    {
        tempvalue = ReadVariable();
        if(tempvalue>0x7FF)
            putstr("Enter the correct value\n\r");
    }while(tempvalue>0x7FF);
    putstr("\n\rEnter the second higher address\n\r");
    do
    {
        tempvalue1 = ReadVariable();
        if(tempvalue1>0x7FF)
            putstr("Enter the correct value\n\r");
    }while(tempvalue1>0x7FF);

    temp = 0xA0;
    address=tempvalue;
    address &=0x700;
    address >>=7;
    temp |= address;
    temp1=(temp&0xFE);
    tempaddr=tempvalue;
    startbit();
    send_data(temp1);
    send_data(tempaddr);
    startbit();
    temp=temp | 0x01;
    send_data(temp);
    putstr("\n\r");
    putstr("\n\r");
    putstr("EEPROM data\n\r");
    putstr("ADDR |  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r");
    putstr("\n\r");
    while(tempvalue<=tempvalue1)
    {
        printf("0x%03X: ", tempvalue);
        for(i=0; i<=15; i++)
        {
            clock_func();
            backup=ACC;
            printf("%02X  ", backup);
            if(tempvalue!=tempvalue1)
            {
                Master_ack();
                tempvalue++;
            }
            else
            {
                tempvalue++;
                break;
            }
        }
        i=0;
        putstr("\n\r");
    }
    stopbit();

}

/* WRITE THE DATA TO EEPROM*/
void write_function()
{
    uint16_t temp=0x8000, temp1=0x100;
    putstr("Enter the address\n\r");
    do
    {
        temp = ReadVariable();
        if(temp>0x7FF)
            putstr("Enter the correct value\n\r");
    }while(temp>0x7FF);
    putstr("\n\rEnter the Value\n\r");
    do
    {
        temp1=ReadVariable();
        if(temp1>0x7FF)
            putstr("Enter the correct value\n\r");
    }while(temp1>0xFF);

    eebytew(temp, temp1);
}

/* HELP MENU*/
void help_menu()
{
    putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    putstr("EEPROM Control\n\r");
    putstr("Press 'R' to read a byte from the user input address\n\r");
    putstr("Press 'W' to write a byte data to user input address\n\r");
    putstr("Press 'S' for sequential read between the adresses\n\r");
    putstr("\n\r");
    putstr("LCD Control\n\r");
    putstr("Press 'C' for CGRAM HEX dump\n\r");
    putstr("Press 'D' for DDRAM HEX dump\n\r");
    putstr("Press 'E' for Clearing the LCD\n\r");
    putstr("Press 'P' to display the custom character\n\r");
    putstr("Press 'N' to add custom character\n\r");
    putstr("Press 'I' for printing letters on LCD\n\r");
    putstr("Press 'K' for displaying the logo\n\r");
    putstr("Press 'X' for moving the cursor to user input x,y position\n\r");
    putstr("Press 'G' for moving the cursor to user input address\n\r");
    putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
}

void read_function()
{
    uint16_t temp1;
    putstr("Enter the address in hex\n\r");
    do
    {
        temp1=ReadVariable();
        if(temp1>0x7FF)
            putstr("Enter the correct value\n\r");
    }while(temp1>0x7FF);
    eebyter(temp1);
}

/*CREATING A LOGO */
void custom_character()
{

    unsigned char in_array[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c};
    unsigned char in_array5[] = {0x0f, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x0f};
    unsigned char in_array1[] = {0x0c, 0x0c, 0x0c, 0x0c, 0x0c, 0x1f, 0x1f, 0x1f};
    unsigned char in_array2[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x03};
    unsigned char in_array3[] = {0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f};
    unsigned char in_array4[] = {0x1f, 0x00, 0x04, 0x0A, 0x0A, 0x04, 0x00, 0x1f};
    unsigned char in_array6[] = {0x1f, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x1f};
    unsigned char in_array7[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x018, 0x18};
    unsigned char k;
    k=*INST_READ;
    lcdclear();
    putstr("Look at LCD\n\r");
    lcdcreatechar(0x00, in_array);
    lcdcreatechar(0x08, in_array1);
    lcdcreatechar(0x10, in_array2);
    lcdcreatechar(0x18, in_array3);
    lcdcreatechar(0x20, in_array4);
    lcdcreatechar(0x28, in_array5);
    lcdcreatechar(0x30, in_array6);
    lcdcreatechar(0x38, in_array7);
    lcdgotoxy(3,2);
    lcdputch(0x01);
    lcdgotoxy(2,2);
    lcdputch(0x00);
    lcdgotoxy(1,2);
    lcdputch(0x00);
    lcdgotoxy(0,2);
    lcdputch(0x05);
    lcdgotoxy(3,1);
    lcdputch(0x02);
    lcdgotoxy(3,3);
    lcdputch(0x07);
    lcdgotoxy(0,3);
    lcdputch(0x03);
    lcdgotoxy(0,4);
    lcdputch(0x04);
    lcdgotoxy(0,5);
    lcdputch(0x06);
    lcdgotoxy(3,9);
    lcdputch(0x01);
    lcdgotoxy(2,9);
    lcdputch(0x00);
    lcdgotoxy(1,9);
    lcdputch(0x00);
    lcdgotoxy(0,9);
    lcdputch(0x05);
    lcdgotoxy(3,8);
    lcdputch(0x02);
    lcdgotoxy(3,10);
    lcdputch(0x07);
    lcdgotoxy(0,10);
    lcdputch(0x03);
    lcdgotoxy(0,11);
    lcdputch(0x04);
    lcdgotoxy(0,12);
    lcdputch(0x06);
    *INST_WRITE=(k|0x80);
    lcdbusywait();
}

void main(void)
{
    unsigned char in;
    SerialInitialize();
    putstr("\n\r");
    lcdinit();
    *INST_WRITE=(0x80);
    lcdbusywait();
    lcdgotoxy(1,0);
    lcdputstr("YOU KNOW NOTHING");
    lcdgotoxy(2,4);
    lcdputstr("JON SNOW");
    MSDelay(100);
    putstr("Welcome\n\r");
    putstr("Press '>' for help menu\n\r");
    putstr("Enter the character\n\r");
    while(1)
    {
        putstr("\n\rEnter the character\n\r");
        in=getchar();
        switch(in)
        {
            case 'W' : write_function();
                        break;
            case 'R' : read_function();
                        break;
            case 'S' : readall_function();
                        break;
            case '>' : help_menu();
                        break;
            case 'X' : gotoxy();
                        break;
            case 'G' : gotoaddress();
                        break;
            case 'C' : cgramdump();
                        break;
            case 'D' : ddramdump();
                        break;
            case 'E' : lcdclear();
                        break;
            case 'N' : addcharacter();
                        break;
            case 'P' : display();
                        break;
            case 'I' : interactive();
                        break;
            case 'K' : custom_character();
                        break;
            default: putstr("Enter the correct option\n\r");
        }
    }
}
