/*
 */

#include <mcs51/8051.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <at89c51ed2.h>

#define INST_WRITE ((xdata unsigned char *)0xF000)
#define DATA_WRITE ((xdata unsigned char *)0xF400)
#define DATA_READ ((xdata unsigned char *)0xFC00)
#define INST_READ ((xdata unsigned char *)0xF800)

#define BASE_ADDRROW0 0x00
#define BASE_ADDRROW1 0x40
#define BASE_ADDRROW2 0x10
#define BASE_ADDRROW3 0x50

void putstr(char *);
unsigned volatile int t;
unsigned char rec;
unsigned char array[8];
volatile unsigned char u;


void MSDelay(unsigned int value)
  {
    unsigned int x, y;
    for(y=0;y<value;y++)
    {
        for(x=0;x<126;x++)
        {
            ;
        }
    }

  }

  void lcdbusywait()
{
    unsigned volatile int i;
    i=*INST_READ;
    MSDelay(1);
    while((i&0x80)==0x80)
    {
        i = *INST_READ;
        MSDelay(1);
    }
}

void lcdgotoaddr(unsigned char addr)
{
    if(addr>=0x00 && addr<=0x67)
    {
        unsigned char temp;
        temp = 0x80;
        temp |= addr;
        *INST_WRITE = temp;
        lcdbusywait();
    }
}


void lcdgotoxy(unsigned char row, unsigned char column)
{
        switch(row)
        {
            case 0: lcdgotoaddr((BASE_ADDRROW0+column));
                    break;
            case 1: lcdgotoaddr((BASE_ADDRROW1+column));
                    break;
            case 2: lcdgotoaddr((BASE_ADDRROW2+column));
                    break;
            case 3: lcdgotoaddr((BASE_ADDRROW3+column));
                    break;
        }
}

void lcdclear()
{
    *INST_WRITE = 0x01;
    lcdbusywait();
    putstr("Clearing the LCD completed\n\r");
}

void lcdputch(unsigned char cc)
{
    unsigned volatile int i, temp;
    *DATA_WRITE = cc;
    lcdbusywait();
    i=*INST_READ;
        temp = i&0x7F;
        if(temp==0x10)
        {
            lcdgotoxy(1,0);
        }
        else if(temp==0x50)
        {
            lcdgotoxy(2,0);
        }
        else if(temp==0x20)
        {
            lcdgotoxy(3,0);
        }
        else if(temp==0x60)
        {
            lcdgotoxy(0,0);//lcdgotoaddr(temp);
        }
        else
        {
            ;
        }
}

void putstr(char *ptr)
{
        while(*ptr)
        {
            putchar(*ptr);
            ptr++;
        }

}


void lcdputstr(unsigned char *ptr)
{
    while(*ptr)
    {
        lcdputch(*ptr);
        ptr++;
    }
}

/*-----------------------------------------------------------------------------------------------*/
/* Name: lcdinit()
 Description: Initializes the LCD (see Figure25 on page 212 )
  of the HD44780U data sheet)
*/
void lcdinit()
{
    MSDelay(20);
   *INST_WRITE = 0x30;
   MSDelay(10);
   *INST_WRITE = 0x30;
   MSDelay(10);
   *INST_WRITE = 0x30;
   lcdbusywait();
   *INST_WRITE = 0x38;
   lcdbusywait();
   *INST_WRITE = 0x08;
   lcdbusywait();
   *INST_WRITE = 0x0C;
   lcdbusywait();
   *INST_WRITE = 0x06;
   lcdbusywait();
   *INST_WRITE = 0x01;
}

_sdcc_external_startup()
{
    AUXR |= 0x0C;
    return 0;
}

void SerialInitialize()
{
    TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
    TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
    SCON=0x50;
    TR1=1;  // to start timer
    IE=0x90; // to make serial interrupt interrupt enable
}

void putchar(char x)
{
    SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
    while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
    TI=0; // make TI bit to zero for next transmission
}


char getchar()
{
    /* RI bit is present in SCON register*/
    while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
    rec=SBUF;   // data is received in SBUF register present in controller during receiving
    RI=0;  // make RI bit to 0 so that next data is received
    return rec;  // return rec where function is called
}

void help_menu()
{
    putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    putstr("Welcome to Help Menu\n\r");
    putstr("Press 'C' for CGRAM HEX dump\n\r");
    putstr("Press 'D' for DDRAM HEX dump\n\r");
    putstr("Press 'E' for Clearing the LCD\n\r");
    putstr("Press 'S' to display the custom character\n\r");
    putstr("Press 'N' to add custom character\n\r");
    putstr("Press 'I' for interactive mode\n\r");
    putstr("Press 'X' for moving the cursor to user input x,y position\n\r");
    putstr("Press 'G' for moving the cursor to user input address\n\r");
    putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
}

void cgramdump()
{
    unsigned int count_row=0;
    unsigned int count=0;
    unsigned char temp_val=0x40, k;
    k=*INST_READ;
    *INST_WRITE = temp_val;
    lcdbusywait();
    putstr("\n\r");
    putstr("CGRAM values\n\r");
    putstr("\n\rCCODE |  Values\n\r");
        for (count_row=0; count_row<=7; count_row++)
        {
            printf("0x%04x:  ", (temp_val&0x3F));
            for(count=0; count<=7; count++)
            {
                temp_val++;
                printf("%02x  ",*DATA_READ);
                lcdbusywait();
            }
            putstr("\n\r");
        }
        putstr("\n\r");
        *INST_WRITE=(k|0x80);
        lcdbusywait();

}

void lcdcreatechar(unsigned char ccode, unsigned char row_vals[])
{
    unsigned char n, k;
    k=*INST_READ;
    lcdbusywait();
    ccode |= 0x40;
    for (n=0; n<=7; n++)
    {
        *INST_WRITE = ccode;
        lcdbusywait();
        *DATA_WRITE = (row_vals[n]&0x1F);
        lcdbusywait();
        ccode++;
    }
    *INST_WRITE=(k|0x80);
    lcdbusywait();
}

void ddramdump()
{
    unsigned int count_row=0;
    unsigned int count=0;
    unsigned char temp_val, k;
    k=*INST_READ;
    putstr("\n\r");
    putstr("DDRAM Values\n\r");
    putstr("\n\rADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r\n\r");
        for (count_row=0; count_row<=3; count_row++)
        {
            if(count_row==0)
                temp_val=0x80;
            if(count_row==1)
                temp_val=0xC0;
            if(count_row==2)
                temp_val=0x90;
            if(count_row==3)
                temp_val=0xD0;
            printf("0x%02x:  ", (temp_val&0x7F));
            for(count=0; count<=15; count++)
            {
                *INST_WRITE = temp_val;
                lcdbusywait();
                temp_val++;
                printf("%02x  ",*DATA_READ);
                lcdbusywait();
            }
            putstr("\n\r");
        }
        putstr("\n\r");
        *INST_WRITE=(k|0x80);
}


unsigned char ReadVariable()
{
    unsigned char l=0, m=0;
    do
    {
        l=getchar();
        if(l>='0' && l<='9')
        {
            m=((m*10)+(l-'0'));
            putchar(l);
            //k++;
        }
        else if(l==127)
        {
            m=m/10;
        }
        else if(l!=13)
            {putstr("\n\rEnter Numbers\n\r");}
    }while(l!=13);
    return m;
}


unsigned char readinput()
{
    unsigned char value=0, i, j=0;
    do
    {
        i=getchar();
        if(i>='0'&&i<='1')          /* works only if its a number */
        {
            value=((value*2)+(i-'0')); /* Convert character to integers */
            putchar(i);
            j++;
        }
        else if(i==127)
        {
            value=value/2;
        }
        else if(i!=13)
            {putstr("\n\rEnter Valid Numbers\n\r");}
    }while(i!=13);
    j=0;
    return value;
}

void addcharacter()
{
    unsigned char temp, y;
    if(u<8)
    {
        putstr("\n\rEnter eight pixel values\n\r");
        putstr("Enter five binary values for every pixel\n\r");
    //if(add_val<=64)
    //{
        for(temp=0; temp<=7; temp++)
        {
            y=readinput();
            if(y>0x1F)
            {
                putstr("Invalid Number. Enter Again\n\r");
                temp--;
            }
            else
            {
              array[temp]=y;
            putstr("\n\r");
            }
        }
        if(u==0)
            lcdcreatechar(0x00, array);
        else if(u==1)
            lcdcreatechar(0x08, array);
        else if(u==2)
            lcdcreatechar(0x10, array);
        else if(u==3)
            lcdcreatechar(0x18, array);
        else if(u==4)
            lcdcreatechar(0x20, array);
        else if(u==5)
            lcdcreatechar(0x28, array);
        else if(u==6)
            lcdcreatechar(0x30, array);
        else if(u==7)
            lcdcreatechar(0x38, array);
        u++;
    }
    else
    {
        putstr("Eight custom character created. No more custom character please\n\r");
    }

}

void display()
{
    unsigned char hey=0;
    putstr("Look at the LCD\n\r");
    while(hey<0x8)
    {
        lcdputch(hey);
        hey++;
    }
}

void interactive()
{
    unsigned char co, i;
    i=*INST_READ;
    i=i&0x80;
    *INST_WRITE=i;
    putstr("Enter the character to display. Hit enter to stop\n\r");
    do
    {   co=getchar();
        putchar(co);
        if(co!=13)
            lcdputch(co);
    }while(co!=13);
    //*pointer='\0';
    //co=*INST_READ;
    //*INST_WRITE= (co|0x80);

}

void gotoxy()
{
    unsigned char temp=10, temp1=20;
    putstr("\n\rEnter row number\n\r");
    while(temp>3)
    {
        temp=ReadVariable();
        if(temp>3)
            putstr("\n\rEnter correct row\n\r");
    }
    putstr("\n\rEnter column number\n\r");
    while(temp1>15)
    {
        temp1=ReadVariable();
        if(temp1>15)
            putstr("\n\rEnter correct column number\n\r");
    }
    lcdgotoxy(temp, temp1);
}

void gotoaddress()
{
    unsigned char temp=0x66;
    putstr("Enter the address in decimal\n\r");
    while((temp>0x1F && temp<0x40)||(temp>=0x5F))
    {
        temp=ReadVariable();
        if((temp>0x1F && temp<0x40)||(temp>=0x5F))
            putstr("\n\rEnter correct address values\n\r");
    }
    lcdgotoaddr(temp);
    putstr("\n\r");
}

void main(void)
{
    unsigned char input;
    unsigned char array1[]={0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    u=0;
    SerialInitialize();
    putstr("\n\r");
    lcdinit();
    *INST_WRITE=(0x80);
    lcdbusywait();
    /*lcdcreatechar(0x00, array1);
    lcdcreatechar(0x08, array1);
    lcdcreatechar(0x10, array1);
    lcdcreatechar(0x18, array1);
    lcdcreatechar(0x20, array1);
    lcdcreatechar(0x28, array1);
    lcdcreatechar(0x30, array1);
    lcdcreatechar(0x38, array1);*/
    lcdgotoxy(1,0);
    lcdputstr("YOU KNOW NOTHING");
    lcdgotoxy(2,4);
    lcdputstr("JON SNOW");
    MSDelay(100);
    putstr("Welcome\n\n\r");
    putstr("Enter > for help menu\n\r");

    while (1)
    {
        putstr("\n\rEnter the character\n\r");
        input = getchar();
        switch(input)
        {
            case 'X' : gotoxy();
                        break;
            case 'G' : gotoaddress();
                        break;
            case '>' : help_menu();
                        break;
            case 'C' : cgramdump();
                        break;
            case 'D' : ddramdump();
                        break;
            case 'E' : lcdclear();
                        break;
            case 'N' : addcharacter();
                        break;
            case 'S' : display();
                        break;
            case 'I' : interactive();
                        break;
            default : putstr("Enter the correct character\n\r");
        }
    }

    //lcd_putstr("Hegde");
   // __nop_();
}

