;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.6.0 #4309 (Jul 28 2006)
; This file generated Thu Nov 09 16:46:08 2017
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _gotoaddress
	.globl _gotoxy
	.globl _interactive
	.globl _display
	.globl _addcharacter
	.globl _readinput
	.globl _ReadVariable
	.globl _ddramdump
	.globl _lcdcreatechar
	.globl _cgramdump
	.globl _help_menu
	.globl _SerialInitialize
	.globl __sdcc_external_startup
	.globl _lcdinit
	.globl _lcdputstr
	.globl _lcdputch
	.globl _lcdclear
	.globl _lcdgotoxy
	.globl _lcdgotoaddr
	.globl _lcdbusywait
	.globl _MSDelay
	.globl _P5_7
	.globl _P5_6
	.globl _P5_5
	.globl _P5_4
	.globl _P5_3
	.globl _P5_2
	.globl _P5_1
	.globl _P5_0
	.globl _P4_7
	.globl _P4_6
	.globl _P4_5
	.globl _P4_4
	.globl _P4_3
	.globl _P4_2
	.globl _P4_1
	.globl _P4_0
	.globl _PX0L
	.globl _PT0L
	.globl _PX1L
	.globl _PT1L
	.globl _PLS
	.globl _PT2L
	.globl _PPCL
	.globl _EC
	.globl _CCF0
	.globl _CCF1
	.globl _CCF2
	.globl _CCF3
	.globl _CCF4
	.globl _CR
	.globl _CF
	.globl _TF2
	.globl _EXF2
	.globl _RCLK
	.globl _TCLK
	.globl _EXEN2
	.globl _TR2
	.globl _C_T2
	.globl _CP_RL2
	.globl _T2CON_7
	.globl _T2CON_6
	.globl _T2CON_5
	.globl _T2CON_4
	.globl _T2CON_3
	.globl _T2CON_2
	.globl _T2CON_1
	.globl _T2CON_0
	.globl _PT2
	.globl _ET2
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _PS
	.globl _PT1
	.globl _PX1
	.globl _PT0
	.globl _PX0
	.globl _RD
	.globl _WR
	.globl _T1
	.globl _T0
	.globl _INT1
	.globl _INT0
	.globl _TXD
	.globl _RXD
	.globl _P3_7
	.globl _P3_6
	.globl _P3_5
	.globl _P3_4
	.globl _P3_3
	.globl _P3_2
	.globl _P3_1
	.globl _P3_0
	.globl _EA
	.globl _ES
	.globl _ET1
	.globl _EX1
	.globl _ET0
	.globl _EX0
	.globl _P2_7
	.globl _P2_6
	.globl _P2_5
	.globl _P2_4
	.globl _P2_3
	.globl _P2_2
	.globl _P2_1
	.globl _P2_0
	.globl _SM0
	.globl _SM1
	.globl _SM2
	.globl _REN
	.globl _TB8
	.globl _RB8
	.globl _TI
	.globl _RI
	.globl _P1_7
	.globl _P1_6
	.globl _P1_5
	.globl _P1_4
	.globl _P1_3
	.globl _P1_2
	.globl _P1_1
	.globl _P1_0
	.globl _TF1
	.globl _TR1
	.globl _TF0
	.globl _TR0
	.globl _IE1
	.globl _IT1
	.globl _IE0
	.globl _IT0
	.globl _P0_7
	.globl _P0_6
	.globl _P0_5
	.globl _P0_4
	.globl _P0_3
	.globl _P0_2
	.globl _P0_1
	.globl _P0_0
	.globl _EECON
	.globl _KBF
	.globl _KBE
	.globl _KBLS
	.globl _BRL
	.globl _BDRCON
	.globl _T2MOD
	.globl _SPDAT
	.globl _SPSTA
	.globl _SPCON
	.globl _SADEN
	.globl _SADDR
	.globl _WDTPRG
	.globl _WDTRST
	.globl _P5
	.globl _P4
	.globl _IPH1
	.globl _IPL1
	.globl _IPH0
	.globl _IPL0
	.globl _IEN1
	.globl _IEN0
	.globl _CMOD
	.globl _CL
	.globl _CH
	.globl _CCON
	.globl _CCAPM4
	.globl _CCAPM3
	.globl _CCAPM2
	.globl _CCAPM1
	.globl _CCAPM0
	.globl _CCAP4L
	.globl _CCAP3L
	.globl _CCAP2L
	.globl _CCAP1L
	.globl _CCAP0L
	.globl _CCAP4H
	.globl _CCAP3H
	.globl _CCAP2H
	.globl _CCAP1H
	.globl _CCAP0H
	.globl _CKCKON1
	.globl _CKCKON0
	.globl _CKRL
	.globl _AUXR1
	.globl _AUXR
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T2CON
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _IP
	.globl _P3
	.globl _IE
	.globl _P2
	.globl _SBUF
	.globl _SCON
	.globl _P1
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _P0
	.globl _lcdcreatechar_PARM_2
	.globl _lcdgotoxy_PARM_2
	.globl _u
	.globl _array
	.globl _rec
	.globl _t
	.globl _putstr
	.globl _putchar
	.globl _getchar
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (DATA)
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
_T2CON	=	0x00c8
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_TL2	=	0x00cc
_TH2	=	0x00cd
_AUXR	=	0x008e
_AUXR1	=	0x00a2
_CKRL	=	0x0097
_CKCKON0	=	0x008f
_CKCKON1	=	0x008f
_CCAP0H	=	0x00fa
_CCAP1H	=	0x00fb
_CCAP2H	=	0x00fc
_CCAP3H	=	0x00fd
_CCAP4H	=	0x00fe
_CCAP0L	=	0x00ea
_CCAP1L	=	0x00eb
_CCAP2L	=	0x00ec
_CCAP3L	=	0x00ed
_CCAP4L	=	0x00ee
_CCAPM0	=	0x00da
_CCAPM1	=	0x00db
_CCAPM2	=	0x00dc
_CCAPM3	=	0x00dd
_CCAPM4	=	0x00de
_CCON	=	0x00d8
_CH	=	0x00f9
_CL	=	0x00e9
_CMOD	=	0x00d9
_IEN0	=	0x00a8
_IEN1	=	0x00b1
_IPL0	=	0x00b8
_IPH0	=	0x00b7
_IPL1	=	0x00b2
_IPH1	=	0x00b3
_P4	=	0x00c0
_P5	=	0x00d8
_WDTRST	=	0x00a6
_WDTPRG	=	0x00a7
_SADDR	=	0x00a9
_SADEN	=	0x00b9
_SPCON	=	0x00c3
_SPSTA	=	0x00c4
_SPDAT	=	0x00c5
_T2MOD	=	0x00c9
_BDRCON	=	0x009b
_BRL	=	0x009a
_KBLS	=	0x009c
_KBE	=	0x009d
_KBF	=	0x009e
_EECON	=	0x00d2
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (DATA)
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
_ET2	=	0x00ad
_PT2	=	0x00bd
_T2CON_0	=	0x00c8
_T2CON_1	=	0x00c9
_T2CON_2	=	0x00ca
_T2CON_3	=	0x00cb
_T2CON_4	=	0x00cc
_T2CON_5	=	0x00cd
_T2CON_6	=	0x00ce
_T2CON_7	=	0x00cf
_CP_RL2	=	0x00c8
_C_T2	=	0x00c9
_TR2	=	0x00ca
_EXEN2	=	0x00cb
_TCLK	=	0x00cc
_RCLK	=	0x00cd
_EXF2	=	0x00ce
_TF2	=	0x00cf
_CF	=	0x00df
_CR	=	0x00de
_CCF4	=	0x00dc
_CCF3	=	0x00db
_CCF2	=	0x00da
_CCF1	=	0x00d9
_CCF0	=	0x00d8
_EC	=	0x00ae
_PPCL	=	0x00be
_PT2L	=	0x00bd
_PLS	=	0x00bc
_PT1L	=	0x00bb
_PX1L	=	0x00ba
_PT0L	=	0x00b9
_PX0L	=	0x00b8
_P4_0	=	0x00c0
_P4_1	=	0x00c1
_P4_2	=	0x00c2
_P4_3	=	0x00c3
_P4_4	=	0x00c4
_P4_5	=	0x00c5
_P4_6	=	0x00c6
_P4_7	=	0x00c7
_P5_0	=	0x00d8
_P5_1	=	0x00d9
_P5_2	=	0x00da
_P5_3	=	0x00db
_P5_4	=	0x00dc
_P5_5	=	0x00dd
_P5_6	=	0x00de
_P5_7	=	0x00df
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_lcdcreatechar_sloc0_1_0:
	.ds 2
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_t::
	.ds 2
_rec::
	.ds 1
_array::
	.ds 8
_u::
	.ds 1
_MSDelay_value_1_1:
	.ds 2
_lcdbusywait_i_1_1:
	.ds 2
_lcdgotoaddr_addr_1_1:
	.ds 1
_lcdgotoxy_PARM_2:
	.ds 1
_lcdgotoxy_row_1_1:
	.ds 1
_lcdputch_cc_1_1:
	.ds 1
_lcdputch_i_1_1:
	.ds 2
_lcdputch_temp_1_1:
	.ds 2
_putstr_ptr_1_1:
	.ds 3
_lcdputstr_ptr_1_1:
	.ds 3
_putchar_x_1_1:
	.ds 1
_cgramdump_temp_val_1_1:
	.ds 1
_lcdcreatechar_PARM_2:
	.ds 3
_lcdcreatechar_ccode_1_1:
	.ds 1
_ddramdump_temp_val_1_1:
	.ds 1
_ReadVariable_m_1_1:
	.ds 1
_readinput_value_1_1:
	.ds 1
_addcharacter_temp_1_1:
	.ds 1
_gotoxy_temp_1_1:
	.ds 1
_gotoxy_temp1_1_1:
	.ds 1
_gotoaddress_temp_1_1:
	.ds 1
_main_array1_1_1:
	.ds 8
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'MSDelay'
;------------------------------------------------------------
;value                     Allocated with name '_MSDelay_value_1_1'
;x                         Allocated with name '_MSDelay_x_1_1'
;y                         Allocated with name '_MSDelay_y_1_1'
;------------------------------------------------------------
;	main.c:27: void MSDelay(unsigned int value)
;	-----------------------------------------
;	 function MSDelay
;	-----------------------------------------
_MSDelay:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_MSDelay_value_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:30: for(y=0;y<value;y++)
;	genAssign
	mov	dptr,#_MSDelay_value_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	r4,#0x00
	mov	r5,#0x00
00104$:
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,r2
	mov	a,r5
	subb	a,r3
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00108$
;	Peephole 300	removed redundant label 00116$
;	main.c:32: for(x=0;x<126;x++)
;	genAssign
	mov	r6,#0x7E
	mov	r7,#0x00
00103$:
;	genMinus
;	genMinusDec
	dec	r6
	cjne	r6,#0xff,00117$
	dec	r7
00117$:
;	genIfx
	mov	a,r6
	orl	a,r7
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00118$
;	main.c:30: for(y=0;y<value;y++)
;	genPlus
;     genPlusIncr
;	tail increment optimized (range 7)
	inc	r4
	cjne	r4,#0x00,00104$
	inc	r5
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00104$
00108$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdbusywait'
;------------------------------------------------------------
;i                         Allocated with name '_lcdbusywait_i_1_1'
;------------------------------------------------------------
;	main.c:40: void lcdbusywait()
;	-----------------------------------------
;	 function lcdbusywait
;	-----------------------------------------
_lcdbusywait:
;	main.c:44: i=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_lcdbusywait_i_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:46: MSDelay(1);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
	lcall	_MSDelay
;	main.c:47: while((i&0x80)==0x80)
00101$:
;	genAssign
	mov	dptr,#_lcdbusywait_i_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAnd
	anl	ar2,#0x80
	mov	r3,#0x00
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x80,00104$
	cjne	r3,#0x00,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:49: i = *INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_lcdbusywait_i_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:50: MSDelay(1);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
	lcall	_MSDelay
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdgotoaddr'
;------------------------------------------------------------
;addr                      Allocated with name '_lcdgotoaddr_addr_1_1'
;temp                      Allocated with name '_lcdgotoaddr_temp_2_2'
;------------------------------------------------------------
;	main.c:56: void lcdgotoaddr(unsigned char addr)
;	-----------------------------------------
;	 function lcdgotoaddr
;	-----------------------------------------
_lcdgotoaddr:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdgotoaddr_addr_1_1
	movx	@dptr,a
;	main.c:58: if(addr>=0x00 && addr<=0x67)
;	genAssign
	mov	dptr,#_lcdgotoaddr_addr_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x67
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00104$
;	Peephole 300	removed redundant label 00107$
;	main.c:62: temp |= addr;
;	genOr
	orl	ar2,#0x80
;	main.c:63: *INST_WRITE = temp;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:64: lcdbusywait();
;	genCall
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	_lcdbusywait
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdgotoxy'
;------------------------------------------------------------
;column                    Allocated with name '_lcdgotoxy_PARM_2'
;row                       Allocated with name '_lcdgotoxy_row_1_1'
;------------------------------------------------------------
;	main.c:69: void lcdgotoxy(unsigned char row, unsigned char column)
;	-----------------------------------------
;	 function lcdgotoxy
;	-----------------------------------------
_lcdgotoxy:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdgotoxy_row_1_1
	movx	@dptr,a
;	main.c:71: switch(row)
;	genAssign
	mov	dptr,#_lcdgotoxy_row_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x03
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00106$
;	Peephole 300	removed redundant label 00109$
;	genJumpTab
	mov	a,r2
;	Peephole 254	optimized left shift
	add	a,r2
	add	a,r2
	mov	dptr,#00110$
	jmp	@a+dptr
00110$:
	ljmp	00101$
	ljmp	00102$
	ljmp	00103$
	ljmp	00104$
;	main.c:73: case 0: lcdgotoaddr((BASE_ADDRROW0+column));
00101$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:74: break;
;	main.c:75: case 1: lcdgotoaddr((BASE_ADDRROW1+column));
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoaddr
00102$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genPlus
;     genPlusIncr
	mov	a,#0x40
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:76: break;
;	main.c:77: case 2: lcdgotoaddr((BASE_ADDRROW2+column));
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoaddr
00103$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genPlus
;     genPlusIncr
	mov	a,#0x10
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:78: break;
;	main.c:79: case 3: lcdgotoaddr((BASE_ADDRROW3+column));
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoaddr
00104$:
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	movx	a,@dptr
	mov	r2,a
;	genPlus
;     genPlusIncr
	mov	a,#0x50
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
;	genCall
	mov	r2,a
;	Peephole 244.c	loading dpl from a instead of r2
	mov	dpl,a
;	main.c:81: }
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	_lcdgotoaddr
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdclear'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:84: void lcdclear()
;	-----------------------------------------
;	 function lcdclear
;	-----------------------------------------
_lcdclear:
;	main.c:86: *INST_WRITE = 0x01;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x01
	movx	@dptr,a
;	main.c:87: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:88: putstr("Clearing the LCD completed\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_0
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdputch'
;------------------------------------------------------------
;cc                        Allocated with name '_lcdputch_cc_1_1'
;i                         Allocated with name '_lcdputch_i_1_1'
;temp                      Allocated with name '_lcdputch_temp_1_1'
;------------------------------------------------------------
;	main.c:91: void lcdputch(unsigned char cc)
;	-----------------------------------------
;	 function lcdputch
;	-----------------------------------------
_lcdputch:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdputch_cc_1_1
	movx	@dptr,a
;	main.c:94: *DATA_WRITE = cc;
;	genAssign
	mov	r2,#0x00
	mov	r3,#0xF4
;	genAssign
	mov	dptr,#_lcdputch_cc_1_1
	movx	a,@dptr
;	genPointerSet
;     genFarPointerSet
	mov	r4,a
	mov	dpl,r2
	mov	dph,r3
;	Peephole 136	removed redundant move
	movx	@dptr,a
;	main.c:95: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:96: i=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
;	genCast
	mov	r2,a
	mov	dptr,#_lcdputch_i_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:98: temp = i&0x7F;
;	genAssign
	mov	dptr,#_lcdputch_i_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAnd
	mov	dptr,#_lcdputch_temp_1_1
	mov	a,#0x7F
	anl	a,r2
	movx	@dptr,a
	inc	dptr
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:99: if(temp==0x10)
;	genAssign
	mov	dptr,#_lcdputch_temp_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x10,00110$
	cjne	r3,#0x00,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00118$
;	Peephole 300	removed redundant label 00119$
;	main.c:101: lcdgotoxy(1,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x01
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
00110$:
;	main.c:103: else if(temp==0x50)
;	genAssign
	mov	dptr,#_lcdputch_temp_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x50,00107$
	cjne	r3,#0x00,00107$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00120$
;	Peephole 300	removed redundant label 00121$
;	main.c:105: lcdgotoxy(2,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x02
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
00107$:
;	main.c:107: else if(temp==0x20)
;	genAssign
	mov	dptr,#_lcdputch_temp_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x20,00104$
	cjne	r3,#0x00,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00122$
;	Peephole 300	removed redundant label 00123$
;	main.c:109: lcdgotoxy(3,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x03
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
00104$:
;	main.c:111: else if(temp==0x60)
;	genAssign
	mov	dptr,#_lcdputch_temp_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x60,00112$
	cjne	r3,#0x00,00112$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00124$
;	Peephole 300	removed redundant label 00125$
;	main.c:113: lcdgotoxy(0,0);//lcdgotoaddr(temp);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	_lcdgotoxy
00112$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putstr'
;------------------------------------------------------------
;ptr                       Allocated with name '_putstr_ptr_1_1'
;------------------------------------------------------------
;	main.c:123: void putstr(char *ptr)
;	-----------------------------------------
;	 function putstr
;	-----------------------------------------
_putstr:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_putstr_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:125: while(*ptr)
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
	mov	r5,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:127: putchar(*ptr);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_putchar
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:128: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00110$
	inc	r3
00110$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00108$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdputstr'
;------------------------------------------------------------
;ptr                       Allocated with name '_lcdputstr_ptr_1_1'
;------------------------------------------------------------
;	main.c:134: void lcdputstr(unsigned char *ptr)
;	-----------------------------------------
;	 function lcdputstr
;	-----------------------------------------
_lcdputstr:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_lcdputstr_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:136: while(*ptr)
;	genAssign
	mov	dptr,#_lcdputstr_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
	mov	r5,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:138: lcdputch(*ptr);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_lcdputch
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:139: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00110$
	inc	r3
00110$:
;	genAssign
	mov	dptr,#_lcdputstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00108$:
;	genAssign
	mov	dptr,#_lcdputstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdinit'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:148: void lcdinit()
;	-----------------------------------------
;	 function lcdinit
;	-----------------------------------------
_lcdinit:
;	main.c:150: MSDelay(20);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0014
	lcall	_MSDelay
;	main.c:151: *INST_WRITE = 0x30;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x30
	movx	@dptr,a
;	main.c:152: MSDelay(10);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x000A
	lcall	_MSDelay
;	main.c:153: *INST_WRITE = 0x30;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x30
	movx	@dptr,a
;	main.c:154: MSDelay(10);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x000A
	lcall	_MSDelay
;	main.c:155: *INST_WRITE = 0x30;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x30
	movx	@dptr,a
;	main.c:156: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:157: *INST_WRITE = 0x38;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x38
	movx	@dptr,a
;	main.c:158: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:159: *INST_WRITE = 0x08;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x08
	movx	@dptr,a
;	main.c:160: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:161: *INST_WRITE = 0x0C;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x0C
	movx	@dptr,a
;	main.c:162: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:163: *INST_WRITE = 0x06;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x06
	movx	@dptr,a
;	main.c:164: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:165: *INST_WRITE = 0x01;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x01
	movx	@dptr,a
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function '_sdcc_external_startup'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:168: _sdcc_external_startup()
;	-----------------------------------------
;	 function _sdcc_external_startup
;	-----------------------------------------
__sdcc_external_startup:
;	main.c:170: AUXR |= 0x0C;
;	genOr
	orl	_AUXR,#0x0C
;	main.c:171: return 0;
;	genRet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0000
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'SerialInitialize'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:174: void SerialInitialize()
;	-----------------------------------------
;	 function SerialInitialize
;	-----------------------------------------
_SerialInitialize:
;	main.c:176: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
;	genAssign
	mov	_TMOD,#0x20
;	main.c:177: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
;	genAssign
	mov	_TH1,#0xFD
;	main.c:178: SCON=0x50;
;	genAssign
	mov	_SCON,#0x50
;	main.c:179: TR1=1;  // to start timer
;	genAssign
	setb	_TR1
;	main.c:180: IE=0x90; // to make serial interrupt interrupt enable
;	genAssign
	mov	_IE,#0x90
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
;x                         Allocated with name '_putchar_x_1_1'
;------------------------------------------------------------
;	main.c:183: void putchar(char x)
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
;	genReceive
	mov	a,dpl
	mov	dptr,#_putchar_x_1_1
	movx	@dptr,a
;	main.c:185: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
;	genAssign
	mov	dptr,#_putchar_x_1_1
	movx	a,@dptr
	mov	_SBUF,a
;	main.c:186: while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
;	main.c:187: TI=0; // make TI bit to zero for next transmission
;	genAssign
;	Peephole 250.a	using atomic test and clear
	jbc	_TI,00108$
	sjmp	00101$
00108$:
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getchar'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:191: char getchar()
;	-----------------------------------------
;	 function getchar
;	-----------------------------------------
_getchar:
;	main.c:194: while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
	jnb	_RI,00101$
;	Peephole 300	removed redundant label 00108$
;	main.c:195: rec=SBUF;   // data is received in SBUF register present in controller during receiving
;	genAssign
	mov	r2,_SBUF
;	genAssign
	mov	dptr,#_rec
	mov	a,r2
	movx	@dptr,a
;	main.c:196: RI=0;  // make RI bit to 0 so that next data is received
;	genAssign
	clr	_RI
;	main.c:197: return rec;  // return rec where function is called
;	genRet
	mov	dpl,r2
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'help_menu'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:200: void help_menu()
;	-----------------------------------------
;	 function help_menu
;	-----------------------------------------
_help_menu:
;	main.c:202: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	lcall	_putstr
;	main.c:203: putstr("Welcome to Help Menu\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	lcall	_putstr
;	main.c:204: putstr("Press 'C' for CGRAM HEX dump\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:205: putstr("Press 'D' for DDRAM HEX dump\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_4
	mov	b,#0x80
	lcall	_putstr
;	main.c:206: putstr("Press 'E' for Clearing the LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_5
	mov	b,#0x80
	lcall	_putstr
;	main.c:207: putstr("Press 'S' to display the custom character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_6
	mov	b,#0x80
	lcall	_putstr
;	main.c:208: putstr("Press 'A' to add custom character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_7
	mov	b,#0x80
	lcall	_putstr
;	main.c:209: putstr("Press 'I' for interactive mode\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_8
	mov	b,#0x80
	lcall	_putstr
;	main.c:210: putstr("Press 'X' for moving the cursor to user input x,y position\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_9
	mov	b,#0x80
	lcall	_putstr
;	main.c:211: putstr("Press 'G' for moving the cursor to user input address\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_10
	mov	b,#0x80
	lcall	_putstr
;	main.c:212: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'cgramdump'
;------------------------------------------------------------
;count_row                 Allocated with name '_cgramdump_count_row_1_1'
;count                     Allocated with name '_cgramdump_count_1_1'
;temp_val                  Allocated with name '_cgramdump_temp_val_1_1'
;k                         Allocated with name '_cgramdump_k_1_1'
;------------------------------------------------------------
;	main.c:215: void cgramdump()
;	-----------------------------------------
;	 function cgramdump
;	-----------------------------------------
_cgramdump:
;	main.c:219: unsigned char temp_val=0x40, k;
;	genAssign
	mov	dptr,#_cgramdump_temp_val_1_1
	mov	a,#0x40
	movx	@dptr,a
;	main.c:220: k=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:221: *INST_WRITE = temp_val;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x40
	movx	@dptr,a
;	main.c:222: lcdbusywait();
;	genCall
	push	ar2
	lcall	_lcdbusywait
	pop	ar2
;	main.c:223: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:224: putstr("CGRAM values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_12
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:225: putstr("\n\rCCODE |  Values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_13
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:226: for (count_row=0; count_row<=7; count_row++)
;	genAssign
	mov	r3,#0x00
	mov	r4,#0x00
00104$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x07
	subb	a,r3
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r4
;	genIfxJump
	jnc	00116$
	ljmp	00107$
00116$:
;	main.c:228: printf("0x%04x:  ", (temp_val&0x3F));
;	genAssign
	mov	dptr,#_cgramdump_temp_val_1_1
	movx	a,@dptr
	mov	r5,a
;	genAnd
	mov	a,#0x3F
	anl	a,r5
	mov	r6,a
;	genCast
	mov	r7,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_14
	push	acc
	mov	a,#(__str_14 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:229: for(count=0; count<=7; count++)
;	genAssign
;	genAssign
	mov	r6,#0x08
	mov	r7,#0x00
00103$:
;	main.c:231: temp_val++;
;	genPlus
;     genPlusIncr
	inc	r5
;	main.c:232: printf("%02x  ",*DATA_READ);
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xFC00
	movx	a,@dptr
	mov	r0,a
;	genCast
	mov	r1,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
;	genIpush
	mov	a,#__str_15
	push	acc
	mov	a,#(__str_15 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:233: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genMinus
;	genMinusDec
	dec	r6
	cjne	r6,#0xff,00117$
	dec	r7
00117$:
;	main.c:229: for(count=0; count<=7; count++)
;	genIfx
	mov	a,r6
	orl	a,r7
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00103$
;	Peephole 300	removed redundant label 00118$
;	main.c:235: putstr("\n\r");
;	genAssign
	mov	dptr,#_cgramdump_temp_val_1_1
	mov	a,r5
	movx	@dptr,a
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:226: for (count_row=0; count_row<=7; count_row++)
;	genPlus
;     genPlusIncr
	inc	r3
	cjne	r3,#0x00,00119$
	inc	r4
00119$:
	ljmp	00104$
00107$:
;	main.c:237: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:238: *INST_WRITE=(k|0x80);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:239: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'lcdcreatechar'
;------------------------------------------------------------
;sloc0                     Allocated with name '_lcdcreatechar_sloc0_1_0'
;row_vals                  Allocated with name '_lcdcreatechar_PARM_2'
;ccode                     Allocated with name '_lcdcreatechar_ccode_1_1'
;n                         Allocated with name '_lcdcreatechar_n_1_1'
;k                         Allocated with name '_lcdcreatechar_k_1_1'
;------------------------------------------------------------
;	main.c:243: void lcdcreatechar(unsigned char ccode, unsigned char row_vals[])
;	-----------------------------------------
;	 function lcdcreatechar
;	-----------------------------------------
_lcdcreatechar:
;	genReceive
	mov	a,dpl
	mov	dptr,#_lcdcreatechar_ccode_1_1
	movx	@dptr,a
;	main.c:246: k=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:247: lcdbusywait();
;	genCall
	push	ar2
	lcall	_lcdbusywait
	pop	ar2
;	main.c:248: ccode |= 0x40;
;	genAssign
;	genOr
	mov	dptr,#_lcdcreatechar_ccode_1_1
	movx	a,@dptr
	mov	r3,a
;	Peephole 248.a	optimized or to xdata
	orl	a,#0x40
	movx	@dptr,a
;	main.c:249: for (n=0; n<=7; n++)
;	genAssign
	mov	dptr,#_lcdcreatechar_PARM_2
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genAssign
	mov	dptr,#_lcdcreatechar_ccode_1_1
	movx	a,@dptr
	mov	r6,a
;	genAssign
	mov	r7,#0x00
00101$:
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r7
	add	a,#0xff - 0x07
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00109$
;	Peephole 300	removed redundant label 00110$
;	main.c:251: *INST_WRITE = ccode;
;	genIpush
	push	ar2
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r6
	movx	@dptr,a
;	main.c:252: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:253: *DATA_WRITE = (row_vals[n]&0x1F);
;	genAssign
	mov	_lcdcreatechar_sloc0_1_0,#0x00
	mov	(_lcdcreatechar_sloc0_1_0 + 1),#0xF4
;	genPlus
;	Peephole 236.g	used r7 instead of ar7
	mov	a,r7
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	mov	r2,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	mov	r0,a
	mov	ar1,r5
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r0
	mov	b,r1
	lcall	__gptrget
	mov	r2,a
;	genAnd
	anl	ar2,#0x1F
;	genPointerSet
;     genFarPointerSet
	mov	dpl,_lcdcreatechar_sloc0_1_0
	mov	dph,(_lcdcreatechar_sloc0_1_0 + 1)
	mov	a,r2
	movx	@dptr,a
;	main.c:254: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:255: ccode++;
;	genPlus
;     genPlusIncr
	inc	r6
;	genAssign
	mov	dptr,#_lcdcreatechar_ccode_1_1
	mov	a,r6
	movx	@dptr,a
;	main.c:249: for (n=0; n<=7; n++)
;	genPlus
;     genPlusIncr
	inc	r7
;	genIpop
	pop	ar2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00109$:
;	genAssign
	mov	dptr,#_lcdcreatechar_ccode_1_1
	mov	a,r6
	movx	@dptr,a
;	main.c:257: *INST_WRITE=(k|0x80);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:258: lcdbusywait();
;	genCall
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdbusywait
;
;------------------------------------------------------------
;Allocation info for local variables in function 'ddramdump'
;------------------------------------------------------------
;count_row                 Allocated with name '_ddramdump_count_row_1_1'
;count                     Allocated with name '_ddramdump_count_1_1'
;temp_val                  Allocated with name '_ddramdump_temp_val_1_1'
;k                         Allocated with name '_ddramdump_k_1_1'
;------------------------------------------------------------
;	main.c:261: void ddramdump()
;	-----------------------------------------
;	 function ddramdump
;	-----------------------------------------
_ddramdump:
;	main.c:266: k=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:267: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:268: putstr("DDRAM Values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_16
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:269: putstr("\n\rADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_17
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:270: for (count_row=0; count_row<=3; count_row++)
;	genAssign
	mov	r3,#0x00
	mov	r4,#0x00
00112$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x03
	subb	a,r3
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r4
;	genIfxJump
	jnc	00128$
	ljmp	00115$
00128$:
;	main.c:272: if(count_row==0)
;	genIfx
	mov	a,r3
	orl	a,r4
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00102$
;	Peephole 300	removed redundant label 00129$
;	main.c:273: temp_val=0x80;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0x80
	movx	@dptr,a
00102$:
;	main.c:274: if(count_row==1)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r3,#0x01,00104$
	cjne	r4,#0x00,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00130$
;	Peephole 300	removed redundant label 00131$
;	main.c:275: temp_val=0xC0;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0xC0
	movx	@dptr,a
00104$:
;	main.c:276: if(count_row==2)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r3,#0x02,00106$
	cjne	r4,#0x00,00106$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00132$
;	Peephole 300	removed redundant label 00133$
;	main.c:277: temp_val=0x90;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0x90
	movx	@dptr,a
00106$:
;	main.c:278: if(count_row==3)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r3,#0x03,00108$
	cjne	r4,#0x00,00108$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00134$
;	Peephole 300	removed redundant label 00135$
;	main.c:279: temp_val=0xD0;
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,#0xD0
	movx	@dptr,a
00108$:
;	main.c:280: printf("0x%02x:  ", (temp_val&0x7F));
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	movx	a,@dptr
	mov	r5,a
;	genAnd
	mov	a,#0x7F
	anl	a,r5
	mov	r6,a
;	genCast
	mov	r7,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_18
	push	acc
	mov	a,#(__str_18 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:281: for(count=0; count<=15; count++)
;	genAssign
;	genAssign
	mov	r6,#0x10
	mov	r7,#0x00
00111$:
;	main.c:283: *INST_WRITE = temp_val;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r5
	movx	@dptr,a
;	main.c:284: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:285: temp_val++;
;	genPlus
;     genPlusIncr
	inc	r5
;	main.c:286: printf("%02x  ",*DATA_READ);
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xFC00
	movx	a,@dptr
	mov	r0,a
;	genCast
	mov	r1,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	push	ar0
	push	ar1
;	genIpush
	mov	a,#__str_15
	push	acc
	mov	a,#(__str_15 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:287: lcdbusywait();
;	genCall
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	lcall	_lcdbusywait
	pop	ar7
	pop	ar6
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	genMinus
;	genMinusDec
	dec	r6
	cjne	r6,#0xff,00136$
	dec	r7
00136$:
;	main.c:281: for(count=0; count<=15; count++)
;	genIfx
	mov	a,r6
	orl	a,r7
;	genIfxJump
	jz	00137$
	ljmp	00111$
00137$:
;	main.c:289: putstr("\n\r");
;	genAssign
	mov	dptr,#_ddramdump_temp_val_1_1
	mov	a,r5
	movx	@dptr,a
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar4
	lcall	_putstr
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:270: for (count_row=0; count_row<=3; count_row++)
;	genPlus
;     genPlusIncr
	inc	r3
	cjne	r3,#0x00,00138$
	inc	r4
00138$:
	ljmp	00112$
00115$:
;	main.c:291: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:292: *INST_WRITE=(k|0x80);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genOr
	orl	ar2,#0x80
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	Peephole 300	removed redundant label 00116$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ReadVariable'
;------------------------------------------------------------
;l                         Allocated with name '_ReadVariable_l_1_1'
;m                         Allocated with name '_ReadVariable_m_1_1'
;------------------------------------------------------------
;	main.c:296: unsigned char ReadVariable()
;	-----------------------------------------
;	 function ReadVariable
;	-----------------------------------------
_ReadVariable:
;	main.c:298: unsigned char l=0, m=0;
;	genAssign
	mov	dptr,#_ReadVariable_m_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:299: do
00110$:
;	main.c:301: l=getchar();
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	main.c:302: if(l>='0' && l<='8')
;	genAssign
	mov	ar3,r2
;	genCmpLt
;	genCmp
	cjne	r3,#0x30,00121$
00121$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00122$
;	genAssign
	mov	ar3,r2
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x38
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00123$
;	main.c:304: m=((m*10)+(l-'0'));
;	genAssign
	mov	dptr,#_ReadVariable_m_1_1
	movx	a,@dptr
;	genMult
;	genMultOneByte
	mov	r3,a
;	Peephole 105	removed redundant mov
	mov	b,#0x0A
	mul	ab
	mov	r3,a
;	genMinus
	mov	a,r2
	add	a,#0xd0
;	genPlus
	mov	dptr,#_ReadVariable_m_1_1
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	main.c:305: putchar(l);
;	genCall
	mov	dpl,r2
	push	ar2
	lcall	_putchar
	pop	ar2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00107$:
;	main.c:308: else if(l==127)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x7F,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00124$
;	Peephole 300	removed redundant label 00125$
;	main.c:310: m=m/10;
;	genAssign
	mov	dptr,#_ReadVariable_m_1_1
	movx	a,@dptr
	mov	r3,a
;	genDiv
	mov	dptr,#_ReadVariable_m_1_1
;     genDivOneByte
	mov	b,#0x0A
	mov	a,r3
	div	ab
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00104$:
;	main.c:312: else if(l!=13)
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x0D,00126$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00126$:
;	main.c:313: {putstr("\n\rEnter Numbers\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_19
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
00111$:
;	main.c:314: }while(l!=13);
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x0D,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00127$
;	Peephole 300	removed redundant label 00128$
;	main.c:315: return m;
;	genAssign
	mov	dptr,#_ReadVariable_m_1_1
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'readinput'
;------------------------------------------------------------
;value                     Allocated with name '_readinput_value_1_1'
;i                         Allocated with name '_readinput_i_1_1'
;j                         Allocated with name '_readinput_j_1_1'
;------------------------------------------------------------
;	main.c:319: unsigned char readinput()
;	-----------------------------------------
;	 function readinput
;	-----------------------------------------
_readinput:
;	main.c:321: unsigned char value=0, i, j=0;
;	genAssign
	mov	dptr,#_readinput_value_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:322: do
;	genAssign
	mov	r2,#0x00
00110$:
;	main.c:324: i=getchar();
;	genCall
	push	ar2
	lcall	_getchar
	mov	r3,dpl
	pop	ar2
;	main.c:325: if(i>='0'&&i<='1')          /* works only if its a number */
;	genAssign
	mov	ar4,r3
;	genCmpLt
;	genCmp
	cjne	r4,#0x30,00122$
00122$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00123$
;	genAssign
	mov	ar4,r3
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r4
	add	a,#0xff - 0x31
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00124$
;	main.c:327: value=((value*2)+(i-'0')); /* Convert character to integers */
;	genAssign
	mov	dptr,#_readinput_value_1_1
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshOne
;	Peephole 105	removed redundant mov
;	Peephole 204	removed redundant mov
	add	a,acc
	mov	r4,a
;	genMinus
	mov	a,r3
	add	a,#0xd0
;	genPlus
	mov	dptr,#_readinput_value_1_1
;	Peephole 236.a	used r4 instead of ar4
	add	a,r4
	movx	@dptr,a
;	main.c:328: putchar(i);
;	genCall
	mov	dpl,r3
	push	ar2
	push	ar3
	lcall	_putchar
	pop	ar3
	pop	ar2
;	main.c:329: j++;
;	genPlus
;     genPlusIncr
	inc	r2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00107$:
;	main.c:331: else if(i==127)
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r3,#0x7F,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00125$
;	Peephole 300	removed redundant label 00126$
;	main.c:333: value=value/2;
;	genAssign
	mov	dptr,#_readinput_value_1_1
	movx	a,@dptr
;	genRightShift
;	genRightShiftLiteral
;	genrshOne
	mov	r4,a
;	Peephole 105	removed redundant mov
	clr	c
	rrc	a
;	genAssign
	mov	r4,a
	mov	dptr,#_readinput_value_1_1
;	Peephole 100	removed redundant mov
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00104$:
;	main.c:335: else if(i!=13)
;	genCmpEq
;	gencjneshort
	cjne	r3,#0x0D,00127$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00127$:
;	main.c:336: {putstr("\n\rEnter Valid Numbers\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_20
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
00111$:
;	main.c:337: }while(i!=13);
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r3,#0x0D,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00128$
;	Peephole 300	removed redundant label 00129$
;	main.c:339: return value;
;	genAssign
	mov	dptr,#_readinput_value_1_1
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'addcharacter'
;------------------------------------------------------------
;temp                      Allocated with name '_addcharacter_temp_1_1'
;y                         Allocated with name '_addcharacter_y_1_1'
;------------------------------------------------------------
;	main.c:342: void addcharacter()
;	-----------------------------------------
;	 function addcharacter
;	-----------------------------------------
_addcharacter:
;	main.c:345: if(u<8)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x08,00148$
00148$:
;	genIfxJump
	jc	00149$
	ljmp	00128$
00149$:
;	main.c:347: putstr("\n\rEnter eight pixel values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_21
	mov	b,#0x80
	lcall	_putstr
;	main.c:348: putstr("Enter five binary values for every pixel\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_22
	mov	b,#0x80
	lcall	_putstr
;	main.c:351: for(temp=0; temp<=7; temp++)
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
00130$:
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x07
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00133$
;	Peephole 300	removed redundant label 00150$
;	main.c:353: y=readinput();
;	genCall
	push	ar2
	lcall	_readinput
	mov	r3,dpl
	pop	ar2
;	main.c:354: if(y>0x1F)
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x1F
	jnc	00102$
;	Peephole 300	removed redundant label 00151$
;	main.c:356: putstr("Invalid Number. Enter Again\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_23
	mov	b,#0x80
	push	ar2
	lcall	_putstr
	pop	ar2
;	main.c:357: temp--;
;	genMinus
;	genMinusDec
	mov	a,r2
	dec	a
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00132$
00102$:
;	main.c:361: array[temp]=y;
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_array
	mov	dpl,a
;	Peephole 181	changed mov to clr
	clr	a
	addc	a,#(_array >> 8)
	mov	dph,a
;	genPointerSet
;     genFarPointerSet
	mov	a,r3
	movx	@dptr,a
;	main.c:362: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	lcall	_putstr
00132$:
;	main.c:351: for(temp=0; temp<=7; temp++)
;	genAssign
	mov	dptr,#_addcharacter_temp_1_1
	movx	a,@dptr
	mov	r2,a
;	genPlus
	mov	dptr,#_addcharacter_temp_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00130$
00133$:
;	main.c:365: if(u==0)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
;	genIfx
	mov	r2,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00125$
;	Peephole 300	removed redundant label 00152$
;	main.c:366: lcdcreatechar(0x00, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x00
	lcall	_lcdcreatechar
	ljmp	00126$
00125$:
;	main.c:367: else if(u==1)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x01,00122$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00153$
;	Peephole 300	removed redundant label 00154$
;	main.c:368: lcdcreatechar(0x08, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x08
	lcall	_lcdcreatechar
	ljmp	00126$
00122$:
;	main.c:369: else if(u==2)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x02,00119$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00155$
;	Peephole 300	removed redundant label 00156$
;	main.c:370: lcdcreatechar(0x10, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x10
	lcall	_lcdcreatechar
	ljmp	00126$
00119$:
;	main.c:371: else if(u==3)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x03,00116$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00157$
;	Peephole 300	removed redundant label 00158$
;	main.c:372: lcdcreatechar(0x18, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x18
	lcall	_lcdcreatechar
	ljmp	00126$
00116$:
;	main.c:373: else if(u==4)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x04,00113$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00159$
;	Peephole 300	removed redundant label 00160$
;	main.c:374: lcdcreatechar(0x20, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x20
	lcall	_lcdcreatechar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00126$
00113$:
;	main.c:375: else if(u==5)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x05,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00161$
;	Peephole 300	removed redundant label 00162$
;	main.c:376: lcdcreatechar(0x28, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x28
	lcall	_lcdcreatechar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00126$
00110$:
;	main.c:377: else if(u==6)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x06,00107$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00163$
;	Peephole 300	removed redundant label 00164$
;	main.c:378: lcdcreatechar(0x30, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x30
	lcall	_lcdcreatechar
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00126$
00107$:
;	main.c:379: else if(u==7)
;	genAssign
	mov	dptr,#_u
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x07,00126$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00165$
;	Peephole 300	removed redundant label 00166$
;	main.c:380: lcdcreatechar(0x38, array);
;	genCast
	mov	dptr,#_lcdcreatechar_PARM_2
	mov	a,#_array
	movx	@dptr,a
	inc	dptr
	mov	a,#(_array >> 8)
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	genCall
	mov	dpl,#0x38
	lcall	_lcdcreatechar
00126$:
;	main.c:381: u++;
;	genPlus
	mov	dptr,#_u
	movx	a,@dptr
	add	a,#0x01
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
	ret
00128$:
;	main.c:385: putstr("Eight custom character created. No more custom character please\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_24
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'display'
;------------------------------------------------------------
;hey                       Allocated with name '_display_hey_1_1'
;------------------------------------------------------------
;	main.c:390: void display()
;	-----------------------------------------
;	 function display
;	-----------------------------------------
_display:
;	main.c:393: putstr("Look at the LCD\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_25
	mov	b,#0x80
	lcall	_putstr
;	main.c:394: while(hey<0x8)
;	genAssign
	mov	r2,#0x00
00101$:
;	genCmpLt
;	genCmp
	cjne	r2,#0x08,00109$
00109$:
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00104$
;	Peephole 300	removed redundant label 00110$
;	main.c:396: lcdputch(hey);
;	genCall
	mov	dpl,r2
	push	ar2
	lcall	_lcdputch
	pop	ar2
;	main.c:397: hey++;
;	genPlus
;     genPlusIncr
	inc	r2
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00104$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'interactive'
;------------------------------------------------------------
;co                        Allocated with name '_interactive_co_1_1'
;i                         Allocated with name '_interactive_i_1_1'
;------------------------------------------------------------
;	main.c:401: void interactive()
;	-----------------------------------------
;	 function interactive
;	-----------------------------------------
_interactive:
;	main.c:404: i=*INST_READ;
;	genPointerGet
;	genFarPointerGet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF800
	movx	a,@dptr
	mov	r2,a
;	main.c:405: i=i&0x80;
;	genAnd
	anl	ar2,#0x80
;	main.c:406: *INST_WRITE=i;
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,r2
	movx	@dptr,a
;	main.c:407: putstr("Enter the character to display. Hit enter to stop\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_26
	mov	b,#0x80
	lcall	_putstr
;	main.c:408: do
00103$:
;	main.c:409: {   co=getchar();
;	genCall
	lcall	_getchar
;	main.c:410: putchar(co);
;	genCall
	mov  r2,dpl
;	Peephole 177.a	removed redundant mov
	push	ar2
	lcall	_putchar
	pop	ar2
;	main.c:411: if(co!=13)
;	genCmpEq
;	gencjne
;	gencjneshort
;	Peephole 241.d	optimized compare
	clr	a
	cjne	r2,#0x0D,00110$
	inc	a
00110$:
;	Peephole 300	removed redundant label 00111$
;	genIfx
	mov	r3,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00104$
;	Peephole 300	removed redundant label 00112$
;	main.c:412: lcdputch(co);
;	genCall
	mov	dpl,r2
	push	ar3
	lcall	_lcdputch
	pop	ar3
00104$:
;	main.c:413: }while(co!=13);
;	genIfx
	mov	a,r3
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00103$
;	Peephole 300	removed redundant label 00113$
;	Peephole 300	removed redundant label 00106$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'gotoxy'
;------------------------------------------------------------
;temp                      Allocated with name '_gotoxy_temp_1_1'
;temp1                     Allocated with name '_gotoxy_temp1_1_1'
;------------------------------------------------------------
;	main.c:420: void gotoxy()
;	-----------------------------------------
;	 function gotoxy
;	-----------------------------------------
_gotoxy:
;	main.c:422: unsigned char temp=10, temp1=20;
;	genAssign
	mov	dptr,#_gotoxy_temp_1_1
	mov	a,#0x0A
	movx	@dptr,a
;	genAssign
	mov	dptr,#_gotoxy_temp1_1_1
	mov	a,#0x14
	movx	@dptr,a
;	main.c:423: putstr("\n\rEnter row number\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
	lcall	_putstr
;	main.c:424: while(temp>3)
00103$:
;	genAssign
	mov	dptr,#_gotoxy_temp_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x03
	jnc	00105$
;	Peephole 300	removed redundant label 00119$
;	main.c:426: temp=ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r2,dpl
;	genAssign
	mov	dptr,#_gotoxy_temp_1_1
	mov	a,r2
	movx	@dptr,a
;	main.c:427: if(temp>3)
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r2
	add	a,#0xff - 0x03
	jnc	00103$
;	Peephole 300	removed redundant label 00120$
;	main.c:428: putstr("\n\rEnter correct row\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_28
	mov	b,#0x80
	lcall	_putstr
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00103$
00105$:
;	main.c:430: putstr("\n\rEnter column number\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_29
	mov	b,#0x80
	lcall	_putstr
;	main.c:431: while(temp1>15)
00108$:
;	genAssign
	mov	dptr,#_gotoxy_temp1_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x0F
	jnc	00110$
;	Peephole 300	removed redundant label 00121$
;	main.c:433: temp1=ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r3,dpl
;	genAssign
	mov	dptr,#_gotoxy_temp1_1_1
	mov	a,r3
	movx	@dptr,a
;	main.c:434: if(temp1>15)
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x0F
	jnc	00108$
;	Peephole 300	removed redundant label 00122$
;	main.c:435: putstr("\n\rEnter correct column number\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_30
	mov	b,#0x80
	lcall	_putstr
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00108$
00110$:
;	main.c:437: lcdgotoxy(temp, temp1);
;	genAssign
	mov	dptr,#_gotoxy_temp_1_1
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,r2
	movx	@dptr,a
;	genCall
	mov	dpl,r3
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_lcdgotoxy
;
;------------------------------------------------------------
;Allocation info for local variables in function 'gotoaddress'
;------------------------------------------------------------
;temp                      Allocated with name '_gotoaddress_temp_1_1'
;------------------------------------------------------------
;	main.c:440: void gotoaddress()
;	-----------------------------------------
;	 function gotoaddress
;	-----------------------------------------
_gotoaddress:
;	main.c:442: unsigned char temp=0x66;
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	mov	a,#0x66
	movx	@dptr,a
;	main.c:443: putstr("Enter the address in decimal\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_31
	mov	b,#0x80
	lcall	_putstr
;	main.c:444: while((temp>0x1F && temp<0x40)||(temp>=0x5F))
00107$:
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	movx	a,@dptr
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov  r2,a
;	Peephole 177.a	removed redundant mov
	add	a,#0xff - 0x1F
	jnc	00106$
;	Peephole 300	removed redundant label 00115$
;	genCmpLt
;	genCmp
	cjne	r2,#0x40,00116$
00116$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00108$
;	Peephole 300	removed redundant label 00117$
00106$:
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x5F,00118$
00118$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00109$
;	Peephole 300	removed redundant label 00119$
00108$:
;	main.c:446: temp=ReadVariable();
;	genCall
	lcall	_ReadVariable
	mov	r3,dpl
;	genAssign
	mov	dptr,#_gotoaddress_temp_1_1
	mov	a,r3
	movx	@dptr,a
;	main.c:447: if((temp>0x1F && temp<0x40)||(temp>=0x5F))
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x1F
	jnc	00104$
;	Peephole 300	removed redundant label 00120$
;	genCmpLt
;	genCmp
	cjne	r3,#0x40,00121$
00121$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00101$
;	Peephole 300	removed redundant label 00122$
00104$:
;	genCmpLt
;	genCmp
	cjne	r3,#0x5F,00123$
00123$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00124$
00101$:
;	main.c:448: putstr("\n\rEnter correct address values\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_32
	mov	b,#0x80
	lcall	_putstr
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00107$
00109$:
;	main.c:450: lcdgotoaddr(temp);
;	genCall
	mov	dpl,r2
	lcall	_lcdgotoaddr
;	main.c:451: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;input                     Allocated with name '_main_input_1_1'
;array1                    Allocated with name '_main_array1_1_1'
;------------------------------------------------------------
;	main.c:454: void main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	main.c:457: unsigned char array1[]={0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_main_array1_1_1
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_main_array1_1_1 + 0x0001)
	movx	@dptr,a
	mov	dptr,#(_main_array1_1_1 + 0x0002)
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#(_main_array1_1_1 + 0x0003)
	movx	@dptr,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_main_array1_1_1 + 0x0004)
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
	clr	a
	movx	@dptr,a
	mov	dptr,#(_main_array1_1_1 + 0x0005)
	movx	@dptr,a
	mov	dptr,#(_main_array1_1_1 + 0x0006)
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
	mov	dptr,#(_main_array1_1_1 + 0x0007)
	movx	@dptr,a
;	main.c:458: u=0;
;	genAssign
	mov	dptr,#_u
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:459: SerialInitialize();
;	genCall
	lcall	_SerialInitialize
;	main.c:460: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	lcall	_putstr
;	main.c:461: lcdinit();
;	genCall
	lcall	_lcdinit
;	main.c:462: *INST_WRITE=(0x80);
;	genAssign
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0xF000
;	genPointerSet
;     genFarPointerSet
	mov	a,#0x80
	movx	@dptr,a
;	main.c:463: lcdbusywait();
;	genCall
	lcall	_lcdbusywait
;	main.c:472: lcdgotoxy(1,0);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	genCall
	mov	dpl,#0x01
	lcall	_lcdgotoxy
;	main.c:473: lcdputstr("YOU KNOW NOTHING");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_33
	mov	b,#0x80
	lcall	_lcdputstr
;	main.c:474: lcdgotoxy(2,4);
;	genAssign
	mov	dptr,#_lcdgotoxy_PARM_2
	mov	a,#0x04
	movx	@dptr,a
;	genCall
	mov	dpl,#0x02
	lcall	_lcdgotoxy
;	main.c:475: lcdputstr("JON SNOW");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_34
	mov	b,#0x80
	lcall	_lcdputstr
;	main.c:476: MSDelay(100);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0064
	lcall	_MSDelay
;	main.c:477: putstr("Welcome\n\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_35
	mov	b,#0x80
	lcall	_putstr
;	main.c:478: putstr("Enter > for help menu\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_36
	mov	b,#0x80
	lcall	_putstr
;	main.c:480: while (1)
00113$:
;	main.c:482: putstr("\n\rEnter the character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_37
	mov	b,#0x80
	lcall	_putstr
;	main.c:483: input = getchar();
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	main.c:484: switch(input)
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x3E,00127$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00103$
00127$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x41,00128$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00107$
00128$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x43,00129$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00104$
00129$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x44,00130$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00105$
00130$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x45,00131$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00106$
00131$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x47,00132$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00102$
00132$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x49,00133$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00109$
00133$:
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x53,00134$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00108$
00134$:
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x58,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00135$
;	Peephole 300	removed redundant label 00136$
;	main.c:486: case 'X' : gotoxy();
;	genCall
	lcall	_gotoxy
;	main.c:487: break;
;	main.c:488: case 'G' : gotoaddress();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00102$:
;	genCall
	lcall	_gotoaddress
;	main.c:489: break;
;	main.c:490: case '>' : help_menu();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00103$:
;	genCall
	lcall	_help_menu
;	main.c:491: break;
;	main.c:492: case 'C' : cgramdump();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00104$:
;	genCall
	lcall	_cgramdump
;	main.c:493: break;
;	main.c:494: case 'D' : ddramdump();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00105$:
;	genCall
	lcall	_ddramdump
;	main.c:495: break;
;	main.c:496: case 'E' : lcdclear();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00106$:
;	genCall
	lcall	_lcdclear
;	main.c:497: break;
;	main.c:498: case 'A' : addcharacter();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00107$:
;	genCall
	lcall	_addcharacter
;	main.c:499: break;
;	main.c:500: case 'S' : display();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00108$:
;	genCall
	lcall	_display
;	main.c:501: break;
;	main.c:502: case 'I' : interactive();
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00109$:
;	genCall
	lcall	_interactive
;	main.c:503: break;
;	main.c:504: default : putstr("Enter the correct character\n\r");
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
00110$:
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_38
	mov	b,#0x80
	lcall	_putstr
;	main.c:505: }
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00113$
;	Peephole 259.a	removed redundant label 00115$ and ret
;
	.area CSEG    (CODE)
	.area CONST   (CODE)
__str_0:
	.ascii "Clearing the LCD completed"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_1:
	.db 0x0A
	.db 0x0D
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_2:
	.ascii "Welcome to Help Menu"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_3:
	.ascii "Press 'C' for CGRAM HEX dump"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_4:
	.ascii "Press 'D' for DDRAM HEX dump"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_5:
	.ascii "Press 'E' for Clearing the LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_6:
	.ascii "Press 'S' to display the custom character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_7:
	.ascii "Press 'A' to add custom character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_8:
	.ascii "Press 'I' for interactive mode"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_9:
	.ascii "Press 'X' for moving the cursor to user input x,y position"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_10:
	.ascii "Press 'G' for moving the cursor to user input address"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_11:
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_12:
	.ascii "CGRAM values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_13:
	.db 0x0A
	.db 0x0D
	.ascii "CCODE |  Values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_14:
	.ascii "0x%04x:  "
	.db 0x00
__str_15:
	.ascii "%02x  "
	.db 0x00
__str_16:
	.ascii "DDRAM Values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_17:
	.db 0x0A
	.db 0x0D
	.ascii "ADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C "
	.ascii " +D  +E  +F"
	.db 0x0A
	.db 0x0D
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_18:
	.ascii "0x%02x:  "
	.db 0x00
__str_19:
	.db 0x0A
	.db 0x0D
	.ascii "Enter Numbers"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_20:
	.db 0x0A
	.db 0x0D
	.ascii "Enter Valid Numbers"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_21:
	.db 0x0A
	.db 0x0D
	.ascii "Enter eight pixel values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_22:
	.ascii "Enter five binary values for every pixel"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_23:
	.ascii "Invalid Number. Enter Again"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_24:
	.ascii "Eight custom character created. No more custom character ple"
	.ascii "ase"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_25:
	.ascii "Look at the LCD"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_26:
	.ascii "Enter the character to display. Hit enter to stop"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_27:
	.db 0x0A
	.db 0x0D
	.ascii "Enter row number"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_28:
	.db 0x0A
	.db 0x0D
	.ascii "Enter correct row"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_29:
	.db 0x0A
	.db 0x0D
	.ascii "Enter column number"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_30:
	.db 0x0A
	.db 0x0D
	.ascii "Enter correct column number"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_31:
	.ascii "Enter the address in decimal"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_32:
	.db 0x0A
	.db 0x0D
	.ascii "Enter correct address values"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_33:
	.ascii "YOU KNOW NOTHING"
	.db 0x00
__str_34:
	.ascii "JON SNOW"
	.db 0x00
__str_35:
	.ascii "Welcome"
	.db 0x0A
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_36:
	.ascii "Enter > for help menu"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_37:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_38:
	.ascii "Enter the correct character"
	.db 0x0A
	.db 0x0D
	.db 0x00
	.area XINIT   (CODE)
