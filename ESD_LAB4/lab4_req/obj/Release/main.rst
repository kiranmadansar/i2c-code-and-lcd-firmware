                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : FreeWare ANSI-C Compiler
                              3 ; Version 2.6.0 #4309 (Jul 28 2006)
                              4 ; This file generated Thu Nov 09 16:46:08 2017
                              5 ;--------------------------------------------------------
                              6 	.module main
                              7 	.optsdcc -mmcs51 --model-large
                              8 	
                              9 ;--------------------------------------------------------
                             10 ; Public variables in this module
                             11 ;--------------------------------------------------------
                             12 	.globl _main
                             13 	.globl _gotoaddress
                             14 	.globl _gotoxy
                             15 	.globl _interactive
                             16 	.globl _display
                             17 	.globl _addcharacter
                             18 	.globl _readinput
                             19 	.globl _ReadVariable
                             20 	.globl _ddramdump
                             21 	.globl _lcdcreatechar
                             22 	.globl _cgramdump
                             23 	.globl _help_menu
                             24 	.globl _SerialInitialize
                             25 	.globl __sdcc_external_startup
                             26 	.globl _lcdinit
                             27 	.globl _lcdputstr
                             28 	.globl _lcdputch
                             29 	.globl _lcdclear
                             30 	.globl _lcdgotoxy
                             31 	.globl _lcdgotoaddr
                             32 	.globl _lcdbusywait
                             33 	.globl _MSDelay
                             34 	.globl _P5_7
                             35 	.globl _P5_6
                             36 	.globl _P5_5
                             37 	.globl _P5_4
                             38 	.globl _P5_3
                             39 	.globl _P5_2
                             40 	.globl _P5_1
                             41 	.globl _P5_0
                             42 	.globl _P4_7
                             43 	.globl _P4_6
                             44 	.globl _P4_5
                             45 	.globl _P4_4
                             46 	.globl _P4_3
                             47 	.globl _P4_2
                             48 	.globl _P4_1
                             49 	.globl _P4_0
                             50 	.globl _PX0L
                             51 	.globl _PT0L
                             52 	.globl _PX1L
                             53 	.globl _PT1L
                             54 	.globl _PLS
                             55 	.globl _PT2L
                             56 	.globl _PPCL
                             57 	.globl _EC
                             58 	.globl _CCF0
                             59 	.globl _CCF1
                             60 	.globl _CCF2
                             61 	.globl _CCF3
                             62 	.globl _CCF4
                             63 	.globl _CR
                             64 	.globl _CF
                             65 	.globl _TF2
                             66 	.globl _EXF2
                             67 	.globl _RCLK
                             68 	.globl _TCLK
                             69 	.globl _EXEN2
                             70 	.globl _TR2
                             71 	.globl _C_T2
                             72 	.globl _CP_RL2
                             73 	.globl _T2CON_7
                             74 	.globl _T2CON_6
                             75 	.globl _T2CON_5
                             76 	.globl _T2CON_4
                             77 	.globl _T2CON_3
                             78 	.globl _T2CON_2
                             79 	.globl _T2CON_1
                             80 	.globl _T2CON_0
                             81 	.globl _PT2
                             82 	.globl _ET2
                             83 	.globl _CY
                             84 	.globl _AC
                             85 	.globl _F0
                             86 	.globl _RS1
                             87 	.globl _RS0
                             88 	.globl _OV
                             89 	.globl _F1
                             90 	.globl _P
                             91 	.globl _PS
                             92 	.globl _PT1
                             93 	.globl _PX1
                             94 	.globl _PT0
                             95 	.globl _PX0
                             96 	.globl _RD
                             97 	.globl _WR
                             98 	.globl _T1
                             99 	.globl _T0
                            100 	.globl _INT1
                            101 	.globl _INT0
                            102 	.globl _TXD
                            103 	.globl _RXD
                            104 	.globl _P3_7
                            105 	.globl _P3_6
                            106 	.globl _P3_5
                            107 	.globl _P3_4
                            108 	.globl _P3_3
                            109 	.globl _P3_2
                            110 	.globl _P3_1
                            111 	.globl _P3_0
                            112 	.globl _EA
                            113 	.globl _ES
                            114 	.globl _ET1
                            115 	.globl _EX1
                            116 	.globl _ET0
                            117 	.globl _EX0
                            118 	.globl _P2_7
                            119 	.globl _P2_6
                            120 	.globl _P2_5
                            121 	.globl _P2_4
                            122 	.globl _P2_3
                            123 	.globl _P2_2
                            124 	.globl _P2_1
                            125 	.globl _P2_0
                            126 	.globl _SM0
                            127 	.globl _SM1
                            128 	.globl _SM2
                            129 	.globl _REN
                            130 	.globl _TB8
                            131 	.globl _RB8
                            132 	.globl _TI
                            133 	.globl _RI
                            134 	.globl _P1_7
                            135 	.globl _P1_6
                            136 	.globl _P1_5
                            137 	.globl _P1_4
                            138 	.globl _P1_3
                            139 	.globl _P1_2
                            140 	.globl _P1_1
                            141 	.globl _P1_0
                            142 	.globl _TF1
                            143 	.globl _TR1
                            144 	.globl _TF0
                            145 	.globl _TR0
                            146 	.globl _IE1
                            147 	.globl _IT1
                            148 	.globl _IE0
                            149 	.globl _IT0
                            150 	.globl _P0_7
                            151 	.globl _P0_6
                            152 	.globl _P0_5
                            153 	.globl _P0_4
                            154 	.globl _P0_3
                            155 	.globl _P0_2
                            156 	.globl _P0_1
                            157 	.globl _P0_0
                            158 	.globl _EECON
                            159 	.globl _KBF
                            160 	.globl _KBE
                            161 	.globl _KBLS
                            162 	.globl _BRL
                            163 	.globl _BDRCON
                            164 	.globl _T2MOD
                            165 	.globl _SPDAT
                            166 	.globl _SPSTA
                            167 	.globl _SPCON
                            168 	.globl _SADEN
                            169 	.globl _SADDR
                            170 	.globl _WDTPRG
                            171 	.globl _WDTRST
                            172 	.globl _P5
                            173 	.globl _P4
                            174 	.globl _IPH1
                            175 	.globl _IPL1
                            176 	.globl _IPH0
                            177 	.globl _IPL0
                            178 	.globl _IEN1
                            179 	.globl _IEN0
                            180 	.globl _CMOD
                            181 	.globl _CL
                            182 	.globl _CH
                            183 	.globl _CCON
                            184 	.globl _CCAPM4
                            185 	.globl _CCAPM3
                            186 	.globl _CCAPM2
                            187 	.globl _CCAPM1
                            188 	.globl _CCAPM0
                            189 	.globl _CCAP4L
                            190 	.globl _CCAP3L
                            191 	.globl _CCAP2L
                            192 	.globl _CCAP1L
                            193 	.globl _CCAP0L
                            194 	.globl _CCAP4H
                            195 	.globl _CCAP3H
                            196 	.globl _CCAP2H
                            197 	.globl _CCAP1H
                            198 	.globl _CCAP0H
                            199 	.globl _CKCKON1
                            200 	.globl _CKCKON0
                            201 	.globl _CKRL
                            202 	.globl _AUXR1
                            203 	.globl _AUXR
                            204 	.globl _TH2
                            205 	.globl _TL2
                            206 	.globl _RCAP2H
                            207 	.globl _RCAP2L
                            208 	.globl _T2CON
                            209 	.globl _B
                            210 	.globl _ACC
                            211 	.globl _PSW
                            212 	.globl _IP
                            213 	.globl _P3
                            214 	.globl _IE
                            215 	.globl _P2
                            216 	.globl _SBUF
                            217 	.globl _SCON
                            218 	.globl _P1
                            219 	.globl _TH1
                            220 	.globl _TH0
                            221 	.globl _TL1
                            222 	.globl _TL0
                            223 	.globl _TMOD
                            224 	.globl _TCON
                            225 	.globl _PCON
                            226 	.globl _DPH
                            227 	.globl _DPL
                            228 	.globl _SP
                            229 	.globl _P0
                            230 	.globl _lcdcreatechar_PARM_2
                            231 	.globl _lcdgotoxy_PARM_2
                            232 	.globl _u
                            233 	.globl _array
                            234 	.globl _rec
                            235 	.globl _t
                            236 	.globl _putstr
                            237 	.globl _putchar
                            238 	.globl _getchar
                            239 ;--------------------------------------------------------
                            240 ; special function registers
                            241 ;--------------------------------------------------------
                            242 	.area RSEG    (DATA)
                    0080    243 _P0	=	0x0080
                    0081    244 _SP	=	0x0081
                    0082    245 _DPL	=	0x0082
                    0083    246 _DPH	=	0x0083
                    0087    247 _PCON	=	0x0087
                    0088    248 _TCON	=	0x0088
                    0089    249 _TMOD	=	0x0089
                    008A    250 _TL0	=	0x008a
                    008B    251 _TL1	=	0x008b
                    008C    252 _TH0	=	0x008c
                    008D    253 _TH1	=	0x008d
                    0090    254 _P1	=	0x0090
                    0098    255 _SCON	=	0x0098
                    0099    256 _SBUF	=	0x0099
                    00A0    257 _P2	=	0x00a0
                    00A8    258 _IE	=	0x00a8
                    00B0    259 _P3	=	0x00b0
                    00B8    260 _IP	=	0x00b8
                    00D0    261 _PSW	=	0x00d0
                    00E0    262 _ACC	=	0x00e0
                    00F0    263 _B	=	0x00f0
                    00C8    264 _T2CON	=	0x00c8
                    00CA    265 _RCAP2L	=	0x00ca
                    00CB    266 _RCAP2H	=	0x00cb
                    00CC    267 _TL2	=	0x00cc
                    00CD    268 _TH2	=	0x00cd
                    008E    269 _AUXR	=	0x008e
                    00A2    270 _AUXR1	=	0x00a2
                    0097    271 _CKRL	=	0x0097
                    008F    272 _CKCKON0	=	0x008f
                    008F    273 _CKCKON1	=	0x008f
                    00FA    274 _CCAP0H	=	0x00fa
                    00FB    275 _CCAP1H	=	0x00fb
                    00FC    276 _CCAP2H	=	0x00fc
                    00FD    277 _CCAP3H	=	0x00fd
                    00FE    278 _CCAP4H	=	0x00fe
                    00EA    279 _CCAP0L	=	0x00ea
                    00EB    280 _CCAP1L	=	0x00eb
                    00EC    281 _CCAP2L	=	0x00ec
                    00ED    282 _CCAP3L	=	0x00ed
                    00EE    283 _CCAP4L	=	0x00ee
                    00DA    284 _CCAPM0	=	0x00da
                    00DB    285 _CCAPM1	=	0x00db
                    00DC    286 _CCAPM2	=	0x00dc
                    00DD    287 _CCAPM3	=	0x00dd
                    00DE    288 _CCAPM4	=	0x00de
                    00D8    289 _CCON	=	0x00d8
                    00F9    290 _CH	=	0x00f9
                    00E9    291 _CL	=	0x00e9
                    00D9    292 _CMOD	=	0x00d9
                    00A8    293 _IEN0	=	0x00a8
                    00B1    294 _IEN1	=	0x00b1
                    00B8    295 _IPL0	=	0x00b8
                    00B7    296 _IPH0	=	0x00b7
                    00B2    297 _IPL1	=	0x00b2
                    00B3    298 _IPH1	=	0x00b3
                    00C0    299 _P4	=	0x00c0
                    00D8    300 _P5	=	0x00d8
                    00A6    301 _WDTRST	=	0x00a6
                    00A7    302 _WDTPRG	=	0x00a7
                    00A9    303 _SADDR	=	0x00a9
                    00B9    304 _SADEN	=	0x00b9
                    00C3    305 _SPCON	=	0x00c3
                    00C4    306 _SPSTA	=	0x00c4
                    00C5    307 _SPDAT	=	0x00c5
                    00C9    308 _T2MOD	=	0x00c9
                    009B    309 _BDRCON	=	0x009b
                    009A    310 _BRL	=	0x009a
                    009C    311 _KBLS	=	0x009c
                    009D    312 _KBE	=	0x009d
                    009E    313 _KBF	=	0x009e
                    00D2    314 _EECON	=	0x00d2
                            315 ;--------------------------------------------------------
                            316 ; special function bits
                            317 ;--------------------------------------------------------
                            318 	.area RSEG    (DATA)
                    0080    319 _P0_0	=	0x0080
                    0081    320 _P0_1	=	0x0081
                    0082    321 _P0_2	=	0x0082
                    0083    322 _P0_3	=	0x0083
                    0084    323 _P0_4	=	0x0084
                    0085    324 _P0_5	=	0x0085
                    0086    325 _P0_6	=	0x0086
                    0087    326 _P0_7	=	0x0087
                    0088    327 _IT0	=	0x0088
                    0089    328 _IE0	=	0x0089
                    008A    329 _IT1	=	0x008a
                    008B    330 _IE1	=	0x008b
                    008C    331 _TR0	=	0x008c
                    008D    332 _TF0	=	0x008d
                    008E    333 _TR1	=	0x008e
                    008F    334 _TF1	=	0x008f
                    0090    335 _P1_0	=	0x0090
                    0091    336 _P1_1	=	0x0091
                    0092    337 _P1_2	=	0x0092
                    0093    338 _P1_3	=	0x0093
                    0094    339 _P1_4	=	0x0094
                    0095    340 _P1_5	=	0x0095
                    0096    341 _P1_6	=	0x0096
                    0097    342 _P1_7	=	0x0097
                    0098    343 _RI	=	0x0098
                    0099    344 _TI	=	0x0099
                    009A    345 _RB8	=	0x009a
                    009B    346 _TB8	=	0x009b
                    009C    347 _REN	=	0x009c
                    009D    348 _SM2	=	0x009d
                    009E    349 _SM1	=	0x009e
                    009F    350 _SM0	=	0x009f
                    00A0    351 _P2_0	=	0x00a0
                    00A1    352 _P2_1	=	0x00a1
                    00A2    353 _P2_2	=	0x00a2
                    00A3    354 _P2_3	=	0x00a3
                    00A4    355 _P2_4	=	0x00a4
                    00A5    356 _P2_5	=	0x00a5
                    00A6    357 _P2_6	=	0x00a6
                    00A7    358 _P2_7	=	0x00a7
                    00A8    359 _EX0	=	0x00a8
                    00A9    360 _ET0	=	0x00a9
                    00AA    361 _EX1	=	0x00aa
                    00AB    362 _ET1	=	0x00ab
                    00AC    363 _ES	=	0x00ac
                    00AF    364 _EA	=	0x00af
                    00B0    365 _P3_0	=	0x00b0
                    00B1    366 _P3_1	=	0x00b1
                    00B2    367 _P3_2	=	0x00b2
                    00B3    368 _P3_3	=	0x00b3
                    00B4    369 _P3_4	=	0x00b4
                    00B5    370 _P3_5	=	0x00b5
                    00B6    371 _P3_6	=	0x00b6
                    00B7    372 _P3_7	=	0x00b7
                    00B0    373 _RXD	=	0x00b0
                    00B1    374 _TXD	=	0x00b1
                    00B2    375 _INT0	=	0x00b2
                    00B3    376 _INT1	=	0x00b3
                    00B4    377 _T0	=	0x00b4
                    00B5    378 _T1	=	0x00b5
                    00B6    379 _WR	=	0x00b6
                    00B7    380 _RD	=	0x00b7
                    00B8    381 _PX0	=	0x00b8
                    00B9    382 _PT0	=	0x00b9
                    00BA    383 _PX1	=	0x00ba
                    00BB    384 _PT1	=	0x00bb
                    00BC    385 _PS	=	0x00bc
                    00D0    386 _P	=	0x00d0
                    00D1    387 _F1	=	0x00d1
                    00D2    388 _OV	=	0x00d2
                    00D3    389 _RS0	=	0x00d3
                    00D4    390 _RS1	=	0x00d4
                    00D5    391 _F0	=	0x00d5
                    00D6    392 _AC	=	0x00d6
                    00D7    393 _CY	=	0x00d7
                    00AD    394 _ET2	=	0x00ad
                    00BD    395 _PT2	=	0x00bd
                    00C8    396 _T2CON_0	=	0x00c8
                    00C9    397 _T2CON_1	=	0x00c9
                    00CA    398 _T2CON_2	=	0x00ca
                    00CB    399 _T2CON_3	=	0x00cb
                    00CC    400 _T2CON_4	=	0x00cc
                    00CD    401 _T2CON_5	=	0x00cd
                    00CE    402 _T2CON_6	=	0x00ce
                    00CF    403 _T2CON_7	=	0x00cf
                    00C8    404 _CP_RL2	=	0x00c8
                    00C9    405 _C_T2	=	0x00c9
                    00CA    406 _TR2	=	0x00ca
                    00CB    407 _EXEN2	=	0x00cb
                    00CC    408 _TCLK	=	0x00cc
                    00CD    409 _RCLK	=	0x00cd
                    00CE    410 _EXF2	=	0x00ce
                    00CF    411 _TF2	=	0x00cf
                    00DF    412 _CF	=	0x00df
                    00DE    413 _CR	=	0x00de
                    00DC    414 _CCF4	=	0x00dc
                    00DB    415 _CCF3	=	0x00db
                    00DA    416 _CCF2	=	0x00da
                    00D9    417 _CCF1	=	0x00d9
                    00D8    418 _CCF0	=	0x00d8
                    00AE    419 _EC	=	0x00ae
                    00BE    420 _PPCL	=	0x00be
                    00BD    421 _PT2L	=	0x00bd
                    00BC    422 _PLS	=	0x00bc
                    00BB    423 _PT1L	=	0x00bb
                    00BA    424 _PX1L	=	0x00ba
                    00B9    425 _PT0L	=	0x00b9
                    00B8    426 _PX0L	=	0x00b8
                    00C0    427 _P4_0	=	0x00c0
                    00C1    428 _P4_1	=	0x00c1
                    00C2    429 _P4_2	=	0x00c2
                    00C3    430 _P4_3	=	0x00c3
                    00C4    431 _P4_4	=	0x00c4
                    00C5    432 _P4_5	=	0x00c5
                    00C6    433 _P4_6	=	0x00c6
                    00C7    434 _P4_7	=	0x00c7
                    00D8    435 _P5_0	=	0x00d8
                    00D9    436 _P5_1	=	0x00d9
                    00DA    437 _P5_2	=	0x00da
                    00DB    438 _P5_3	=	0x00db
                    00DC    439 _P5_4	=	0x00dc
                    00DD    440 _P5_5	=	0x00dd
                    00DE    441 _P5_6	=	0x00de
                    00DF    442 _P5_7	=	0x00df
                            443 ;--------------------------------------------------------
                            444 ; overlayable register banks
                            445 ;--------------------------------------------------------
                            446 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     447 	.ds 8
                            448 ;--------------------------------------------------------
                            449 ; internal ram data
                            450 ;--------------------------------------------------------
                            451 	.area DSEG    (DATA)
   0008                     452 _lcdcreatechar_sloc0_1_0:
   0008                     453 	.ds 2
                            454 ;--------------------------------------------------------
                            455 ; overlayable items in internal ram 
                            456 ;--------------------------------------------------------
                            457 	.area OSEG    (OVR,DATA)
                            458 ;--------------------------------------------------------
                            459 ; Stack segment in internal ram 
                            460 ;--------------------------------------------------------
                            461 	.area	SSEG	(DATA)
   0022                     462 __start__stack:
   0022                     463 	.ds	1
                            464 
                            465 ;--------------------------------------------------------
                            466 ; indirectly addressable internal ram data
                            467 ;--------------------------------------------------------
                            468 	.area ISEG    (DATA)
                            469 ;--------------------------------------------------------
                            470 ; bit data
                            471 ;--------------------------------------------------------
                            472 	.area BSEG    (BIT)
                            473 ;--------------------------------------------------------
                            474 ; paged external ram data
                            475 ;--------------------------------------------------------
                            476 	.area PSEG    (PAG,XDATA)
                            477 ;--------------------------------------------------------
                            478 ; external ram data
                            479 ;--------------------------------------------------------
                            480 	.area XSEG    (XDATA)
   0000                     481 _t::
   0000                     482 	.ds 2
   0002                     483 _rec::
   0002                     484 	.ds 1
   0003                     485 _array::
   0003                     486 	.ds 8
   000B                     487 _u::
   000B                     488 	.ds 1
   000C                     489 _MSDelay_value_1_1:
   000C                     490 	.ds 2
   000E                     491 _lcdbusywait_i_1_1:
   000E                     492 	.ds 2
   0010                     493 _lcdgotoaddr_addr_1_1:
   0010                     494 	.ds 1
   0011                     495 _lcdgotoxy_PARM_2:
   0011                     496 	.ds 1
   0012                     497 _lcdgotoxy_row_1_1:
   0012                     498 	.ds 1
   0013                     499 _lcdputch_cc_1_1:
   0013                     500 	.ds 1
   0014                     501 _lcdputch_i_1_1:
   0014                     502 	.ds 2
   0016                     503 _lcdputch_temp_1_1:
   0016                     504 	.ds 2
   0018                     505 _putstr_ptr_1_1:
   0018                     506 	.ds 3
   001B                     507 _lcdputstr_ptr_1_1:
   001B                     508 	.ds 3
   001E                     509 _putchar_x_1_1:
   001E                     510 	.ds 1
   001F                     511 _cgramdump_temp_val_1_1:
   001F                     512 	.ds 1
   0020                     513 _lcdcreatechar_PARM_2:
   0020                     514 	.ds 3
   0023                     515 _lcdcreatechar_ccode_1_1:
   0023                     516 	.ds 1
   0024                     517 _ddramdump_temp_val_1_1:
   0024                     518 	.ds 1
   0025                     519 _ReadVariable_m_1_1:
   0025                     520 	.ds 1
   0026                     521 _readinput_value_1_1:
   0026                     522 	.ds 1
   0027                     523 _addcharacter_temp_1_1:
   0027                     524 	.ds 1
   0028                     525 _gotoxy_temp_1_1:
   0028                     526 	.ds 1
   0029                     527 _gotoxy_temp1_1_1:
   0029                     528 	.ds 1
   002A                     529 _gotoaddress_temp_1_1:
   002A                     530 	.ds 1
   002B                     531 _main_array1_1_1:
   002B                     532 	.ds 8
                            533 ;--------------------------------------------------------
                            534 ; external initialized ram data
                            535 ;--------------------------------------------------------
                            536 	.area XISEG   (XDATA)
                            537 	.area HOME    (CODE)
                            538 	.area GSINIT0 (CODE)
                            539 	.area GSINIT1 (CODE)
                            540 	.area GSINIT2 (CODE)
                            541 	.area GSINIT3 (CODE)
                            542 	.area GSINIT4 (CODE)
                            543 	.area GSINIT5 (CODE)
                            544 	.area GSINIT  (CODE)
                            545 	.area GSFINAL (CODE)
                            546 	.area CSEG    (CODE)
                            547 ;--------------------------------------------------------
                            548 ; interrupt vector 
                            549 ;--------------------------------------------------------
                            550 	.area HOME    (CODE)
   0000                     551 __interrupt_vect:
   0000 02 00 03            552 	ljmp	__sdcc_gsinit_startup
                            553 ;--------------------------------------------------------
                            554 ; global & static initialisations
                            555 ;--------------------------------------------------------
                            556 	.area HOME    (CODE)
                            557 	.area GSINIT  (CODE)
                            558 	.area GSFINAL (CODE)
                            559 	.area GSINIT  (CODE)
                            560 	.globl __sdcc_gsinit_startup
                            561 	.globl __sdcc_program_startup
                            562 	.globl __start__stack
                            563 	.globl __mcs51_genXINIT
                            564 	.globl __mcs51_genXRAMCLEAR
                            565 	.globl __mcs51_genRAMCLEAR
                            566 	.area GSFINAL (CODE)
   005C 02 00 5F            567 	ljmp	__sdcc_program_startup
                            568 ;--------------------------------------------------------
                            569 ; Home
                            570 ;--------------------------------------------------------
                            571 	.area HOME    (CODE)
                            572 	.area CSEG    (CODE)
   005F                     573 __sdcc_program_startup:
   005F 12 0A 10            574 	lcall	_main
                            575 ;	return from main will lock up
   0062 80 FE               576 	sjmp .
                            577 ;--------------------------------------------------------
                            578 ; code
                            579 ;--------------------------------------------------------
                            580 	.area CSEG    (CODE)
                            581 ;------------------------------------------------------------
                            582 ;Allocation info for local variables in function 'MSDelay'
                            583 ;------------------------------------------------------------
                            584 ;value                     Allocated with name '_MSDelay_value_1_1'
                            585 ;x                         Allocated with name '_MSDelay_x_1_1'
                            586 ;y                         Allocated with name '_MSDelay_y_1_1'
                            587 ;------------------------------------------------------------
                            588 ;	main.c:27: void MSDelay(unsigned int value)
                            589 ;	-----------------------------------------
                            590 ;	 function MSDelay
                            591 ;	-----------------------------------------
   0064                     592 _MSDelay:
                    0002    593 	ar2 = 0x02
                    0003    594 	ar3 = 0x03
                    0004    595 	ar4 = 0x04
                    0005    596 	ar5 = 0x05
                    0006    597 	ar6 = 0x06
                    0007    598 	ar7 = 0x07
                    0000    599 	ar0 = 0x00
                    0001    600 	ar1 = 0x01
                            601 ;	genReceive
   0064 AA 83               602 	mov	r2,dph
   0066 E5 82               603 	mov	a,dpl
   0068 90 00 0C            604 	mov	dptr,#_MSDelay_value_1_1
   006B F0                  605 	movx	@dptr,a
   006C A3                  606 	inc	dptr
   006D EA                  607 	mov	a,r2
   006E F0                  608 	movx	@dptr,a
                            609 ;	main.c:30: for(y=0;y<value;y++)
                            610 ;	genAssign
   006F 90 00 0C            611 	mov	dptr,#_MSDelay_value_1_1
   0072 E0                  612 	movx	a,@dptr
   0073 FA                  613 	mov	r2,a
   0074 A3                  614 	inc	dptr
   0075 E0                  615 	movx	a,@dptr
   0076 FB                  616 	mov	r3,a
                            617 ;	genAssign
   0077 7C 00               618 	mov	r4,#0x00
   0079 7D 00               619 	mov	r5,#0x00
   007B                     620 00104$:
                            621 ;	genCmpLt
                            622 ;	genCmp
   007B C3                  623 	clr	c
   007C EC                  624 	mov	a,r4
   007D 9A                  625 	subb	a,r2
   007E ED                  626 	mov	a,r5
   007F 9B                  627 	subb	a,r3
                            628 ;	genIfxJump
                            629 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0080 50 14               630 	jnc	00108$
                            631 ;	Peephole 300	removed redundant label 00116$
                            632 ;	main.c:32: for(x=0;x<126;x++)
                            633 ;	genAssign
   0082 7E 7E               634 	mov	r6,#0x7E
   0084 7F 00               635 	mov	r7,#0x00
   0086                     636 00103$:
                            637 ;	genMinus
                            638 ;	genMinusDec
   0086 1E                  639 	dec	r6
   0087 BE FF 01            640 	cjne	r6,#0xff,00117$
   008A 1F                  641 	dec	r7
   008B                     642 00117$:
                            643 ;	genIfx
   008B EE                  644 	mov	a,r6
   008C 4F                  645 	orl	a,r7
                            646 ;	genIfxJump
                            647 ;	Peephole 108.b	removed ljmp by inverse jump logic
   008D 70 F7               648 	jnz	00103$
                            649 ;	Peephole 300	removed redundant label 00118$
                            650 ;	main.c:30: for(y=0;y<value;y++)
                            651 ;	genPlus
                            652 ;     genPlusIncr
                            653 ;	tail increment optimized (range 7)
   008F 0C                  654 	inc	r4
   0090 BC 00 E8            655 	cjne	r4,#0x00,00104$
   0093 0D                  656 	inc	r5
                            657 ;	Peephole 112.b	changed ljmp to sjmp
   0094 80 E5               658 	sjmp	00104$
   0096                     659 00108$:
   0096 22                  660 	ret
                            661 ;------------------------------------------------------------
                            662 ;Allocation info for local variables in function 'lcdbusywait'
                            663 ;------------------------------------------------------------
                            664 ;i                         Allocated with name '_lcdbusywait_i_1_1'
                            665 ;------------------------------------------------------------
                            666 ;	main.c:40: void lcdbusywait()
                            667 ;	-----------------------------------------
                            668 ;	 function lcdbusywait
                            669 ;	-----------------------------------------
   0097                     670 _lcdbusywait:
                            671 ;	main.c:44: i=*INST_READ;
                            672 ;	genPointerGet
                            673 ;	genFarPointerGet
                            674 ;	Peephole 182.b	used 16 bit load of dptr
   0097 90 F8 00            675 	mov	dptr,#0xF800
   009A E0                  676 	movx	a,@dptr
                            677 ;	genCast
   009B FA                  678 	mov	r2,a
   009C 90 00 0E            679 	mov	dptr,#_lcdbusywait_i_1_1
                            680 ;	Peephole 100	removed redundant mov
   009F F0                  681 	movx	@dptr,a
   00A0 A3                  682 	inc	dptr
                            683 ;	Peephole 181	changed mov to clr
   00A1 E4                  684 	clr	a
   00A2 F0                  685 	movx	@dptr,a
                            686 ;	main.c:46: MSDelay(1);
                            687 ;	genCall
                            688 ;	Peephole 182.b	used 16 bit load of dptr
   00A3 90 00 01            689 	mov	dptr,#0x0001
   00A6 12 00 64            690 	lcall	_MSDelay
                            691 ;	main.c:47: while((i&0x80)==0x80)
   00A9                     692 00101$:
                            693 ;	genAssign
   00A9 90 00 0E            694 	mov	dptr,#_lcdbusywait_i_1_1
   00AC E0                  695 	movx	a,@dptr
   00AD FA                  696 	mov	r2,a
   00AE A3                  697 	inc	dptr
   00AF E0                  698 	movx	a,@dptr
   00B0 FB                  699 	mov	r3,a
                            700 ;	genAnd
   00B1 53 02 80            701 	anl	ar2,#0x80
   00B4 7B 00               702 	mov	r3,#0x00
                            703 ;	genCmpEq
                            704 ;	gencjneshort
                            705 ;	Peephole 112.b	changed ljmp to sjmp
                            706 ;	Peephole 198.a	optimized misc jump sequence
   00B6 BA 80 17            707 	cjne	r2,#0x80,00104$
   00B9 BB 00 14            708 	cjne	r3,#0x00,00104$
                            709 ;	Peephole 200.b	removed redundant sjmp
                            710 ;	Peephole 300	removed redundant label 00108$
                            711 ;	Peephole 300	removed redundant label 00109$
                            712 ;	main.c:49: i = *INST_READ;
                            713 ;	genPointerGet
                            714 ;	genFarPointerGet
                            715 ;	Peephole 182.b	used 16 bit load of dptr
   00BC 90 F8 00            716 	mov	dptr,#0xF800
   00BF E0                  717 	movx	a,@dptr
                            718 ;	genCast
   00C0 FA                  719 	mov	r2,a
   00C1 90 00 0E            720 	mov	dptr,#_lcdbusywait_i_1_1
                            721 ;	Peephole 100	removed redundant mov
   00C4 F0                  722 	movx	@dptr,a
   00C5 A3                  723 	inc	dptr
                            724 ;	Peephole 181	changed mov to clr
   00C6 E4                  725 	clr	a
   00C7 F0                  726 	movx	@dptr,a
                            727 ;	main.c:50: MSDelay(1);
                            728 ;	genCall
                            729 ;	Peephole 182.b	used 16 bit load of dptr
   00C8 90 00 01            730 	mov	dptr,#0x0001
   00CB 12 00 64            731 	lcall	_MSDelay
                            732 ;	Peephole 112.b	changed ljmp to sjmp
   00CE 80 D9               733 	sjmp	00101$
   00D0                     734 00104$:
   00D0 22                  735 	ret
                            736 ;------------------------------------------------------------
                            737 ;Allocation info for local variables in function 'lcdgotoaddr'
                            738 ;------------------------------------------------------------
                            739 ;addr                      Allocated with name '_lcdgotoaddr_addr_1_1'
                            740 ;temp                      Allocated with name '_lcdgotoaddr_temp_2_2'
                            741 ;------------------------------------------------------------
                            742 ;	main.c:56: void lcdgotoaddr(unsigned char addr)
                            743 ;	-----------------------------------------
                            744 ;	 function lcdgotoaddr
                            745 ;	-----------------------------------------
   00D1                     746 _lcdgotoaddr:
                            747 ;	genReceive
   00D1 E5 82               748 	mov	a,dpl
   00D3 90 00 10            749 	mov	dptr,#_lcdgotoaddr_addr_1_1
   00D6 F0                  750 	movx	@dptr,a
                            751 ;	main.c:58: if(addr>=0x00 && addr<=0x67)
                            752 ;	genAssign
   00D7 90 00 10            753 	mov	dptr,#_lcdgotoaddr_addr_1_1
   00DA E0                  754 	movx	a,@dptr
                            755 ;	genCmpGt
                            756 ;	genCmp
                            757 ;	genIfxJump
                            758 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   00DB FA                  759 	mov  r2,a
                            760 ;	Peephole 177.a	removed redundant mov
   00DC 24 98               761 	add	a,#0xff - 0x67
                            762 ;	Peephole 112.b	changed ljmp to sjmp
                            763 ;	Peephole 160.a	removed sjmp by inverse jump logic
   00DE 40 0B               764 	jc	00104$
                            765 ;	Peephole 300	removed redundant label 00107$
                            766 ;	main.c:62: temp |= addr;
                            767 ;	genOr
   00E0 43 02 80            768 	orl	ar2,#0x80
                            769 ;	main.c:63: *INST_WRITE = temp;
                            770 ;	genAssign
                            771 ;	Peephole 182.b	used 16 bit load of dptr
   00E3 90 F0 00            772 	mov	dptr,#0xF000
                            773 ;	genPointerSet
                            774 ;     genFarPointerSet
   00E6 EA                  775 	mov	a,r2
   00E7 F0                  776 	movx	@dptr,a
                            777 ;	main.c:64: lcdbusywait();
                            778 ;	genCall
                            779 ;	Peephole 253.c	replaced lcall with ljmp
   00E8 02 00 97            780 	ljmp	_lcdbusywait
   00EB                     781 00104$:
   00EB 22                  782 	ret
                            783 ;------------------------------------------------------------
                            784 ;Allocation info for local variables in function 'lcdgotoxy'
                            785 ;------------------------------------------------------------
                            786 ;column                    Allocated with name '_lcdgotoxy_PARM_2'
                            787 ;row                       Allocated with name '_lcdgotoxy_row_1_1'
                            788 ;------------------------------------------------------------
                            789 ;	main.c:69: void lcdgotoxy(unsigned char row, unsigned char column)
                            790 ;	-----------------------------------------
                            791 ;	 function lcdgotoxy
                            792 ;	-----------------------------------------
   00EC                     793 _lcdgotoxy:
                            794 ;	genReceive
   00EC E5 82               795 	mov	a,dpl
   00EE 90 00 12            796 	mov	dptr,#_lcdgotoxy_row_1_1
   00F1 F0                  797 	movx	@dptr,a
                            798 ;	main.c:71: switch(row)
                            799 ;	genAssign
   00F2 90 00 12            800 	mov	dptr,#_lcdgotoxy_row_1_1
   00F5 E0                  801 	movx	a,@dptr
                            802 ;	genCmpGt
                            803 ;	genCmp
                            804 ;	genIfxJump
                            805 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   00F6 FA                  806 	mov  r2,a
                            807 ;	Peephole 177.a	removed redundant mov
   00F7 24 FC               808 	add	a,#0xff - 0x03
                            809 ;	Peephole 112.b	changed ljmp to sjmp
                            810 ;	Peephole 160.a	removed sjmp by inverse jump logic
   00F9 40 47               811 	jc	00106$
                            812 ;	Peephole 300	removed redundant label 00109$
                            813 ;	genJumpTab
   00FB EA                  814 	mov	a,r2
                            815 ;	Peephole 254	optimized left shift
   00FC 2A                  816 	add	a,r2
   00FD 2A                  817 	add	a,r2
   00FE 90 01 02            818 	mov	dptr,#00110$
   0101 73                  819 	jmp	@a+dptr
   0102                     820 00110$:
   0102 02 01 0E            821 	ljmp	00101$
   0105 02 01 18            822 	ljmp	00102$
   0108 02 01 26            823 	ljmp	00103$
   010B 02 01 34            824 	ljmp	00104$
                            825 ;	main.c:73: case 0: lcdgotoaddr((BASE_ADDRROW0+column));
   010E                     826 00101$:
                            827 ;	genAssign
   010E 90 00 11            828 	mov	dptr,#_lcdgotoxy_PARM_2
   0111 E0                  829 	movx	a,@dptr
                            830 ;	genCall
   0112 FA                  831 	mov	r2,a
                            832 ;	Peephole 244.c	loading dpl from a instead of r2
   0113 F5 82               833 	mov	dpl,a
                            834 ;	main.c:74: break;
                            835 ;	main.c:75: case 1: lcdgotoaddr((BASE_ADDRROW1+column));
                            836 ;	Peephole 112.b	changed ljmp to sjmp
                            837 ;	Peephole 251.b	replaced sjmp to ret with ret
                            838 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0115 02 00 D1            839 	ljmp	_lcdgotoaddr
   0118                     840 00102$:
                            841 ;	genAssign
   0118 90 00 11            842 	mov	dptr,#_lcdgotoxy_PARM_2
   011B E0                  843 	movx	a,@dptr
   011C FA                  844 	mov	r2,a
                            845 ;	genPlus
                            846 ;     genPlusIncr
   011D 74 40               847 	mov	a,#0x40
                            848 ;	Peephole 236.a	used r2 instead of ar2
   011F 2A                  849 	add	a,r2
                            850 ;	genCall
   0120 FA                  851 	mov	r2,a
                            852 ;	Peephole 244.c	loading dpl from a instead of r2
   0121 F5 82               853 	mov	dpl,a
                            854 ;	main.c:76: break;
                            855 ;	main.c:77: case 2: lcdgotoaddr((BASE_ADDRROW2+column));
                            856 ;	Peephole 112.b	changed ljmp to sjmp
                            857 ;	Peephole 251.b	replaced sjmp to ret with ret
                            858 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0123 02 00 D1            859 	ljmp	_lcdgotoaddr
   0126                     860 00103$:
                            861 ;	genAssign
   0126 90 00 11            862 	mov	dptr,#_lcdgotoxy_PARM_2
   0129 E0                  863 	movx	a,@dptr
   012A FA                  864 	mov	r2,a
                            865 ;	genPlus
                            866 ;     genPlusIncr
   012B 74 10               867 	mov	a,#0x10
                            868 ;	Peephole 236.a	used r2 instead of ar2
   012D 2A                  869 	add	a,r2
                            870 ;	genCall
   012E FA                  871 	mov	r2,a
                            872 ;	Peephole 244.c	loading dpl from a instead of r2
   012F F5 82               873 	mov	dpl,a
                            874 ;	main.c:78: break;
                            875 ;	main.c:79: case 3: lcdgotoaddr((BASE_ADDRROW3+column));
                            876 ;	Peephole 112.b	changed ljmp to sjmp
                            877 ;	Peephole 251.b	replaced sjmp to ret with ret
                            878 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0131 02 00 D1            879 	ljmp	_lcdgotoaddr
   0134                     880 00104$:
                            881 ;	genAssign
   0134 90 00 11            882 	mov	dptr,#_lcdgotoxy_PARM_2
   0137 E0                  883 	movx	a,@dptr
   0138 FA                  884 	mov	r2,a
                            885 ;	genPlus
                            886 ;     genPlusIncr
   0139 74 50               887 	mov	a,#0x50
                            888 ;	Peephole 236.a	used r2 instead of ar2
   013B 2A                  889 	add	a,r2
                            890 ;	genCall
   013C FA                  891 	mov	r2,a
                            892 ;	Peephole 244.c	loading dpl from a instead of r2
   013D F5 82               893 	mov	dpl,a
                            894 ;	main.c:81: }
                            895 ;	Peephole 253.c	replaced lcall with ljmp
   013F 02 00 D1            896 	ljmp	_lcdgotoaddr
   0142                     897 00106$:
   0142 22                  898 	ret
                            899 ;------------------------------------------------------------
                            900 ;Allocation info for local variables in function 'lcdclear'
                            901 ;------------------------------------------------------------
                            902 ;------------------------------------------------------------
                            903 ;	main.c:84: void lcdclear()
                            904 ;	-----------------------------------------
                            905 ;	 function lcdclear
                            906 ;	-----------------------------------------
   0143                     907 _lcdclear:
                            908 ;	main.c:86: *INST_WRITE = 0x01;
                            909 ;	genAssign
                            910 ;	Peephole 182.b	used 16 bit load of dptr
   0143 90 F0 00            911 	mov	dptr,#0xF000
                            912 ;	genPointerSet
                            913 ;     genFarPointerSet
   0146 74 01               914 	mov	a,#0x01
   0148 F0                  915 	movx	@dptr,a
                            916 ;	main.c:87: lcdbusywait();
                            917 ;	genCall
   0149 12 00 97            918 	lcall	_lcdbusywait
                            919 ;	main.c:88: putstr("Clearing the LCD completed\n\r");
                            920 ;	genCall
                            921 ;	Peephole 182.a	used 16 bit load of DPTR
   014C 90 14 25            922 	mov	dptr,#__str_0
   014F 75 F0 80            923 	mov	b,#0x80
                            924 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0152 02 01 EF            925 	ljmp	_putstr
                            926 ;
                            927 ;------------------------------------------------------------
                            928 ;Allocation info for local variables in function 'lcdputch'
                            929 ;------------------------------------------------------------
                            930 ;cc                        Allocated with name '_lcdputch_cc_1_1'
                            931 ;i                         Allocated with name '_lcdputch_i_1_1'
                            932 ;temp                      Allocated with name '_lcdputch_temp_1_1'
                            933 ;------------------------------------------------------------
                            934 ;	main.c:91: void lcdputch(unsigned char cc)
                            935 ;	-----------------------------------------
                            936 ;	 function lcdputch
                            937 ;	-----------------------------------------
   0155                     938 _lcdputch:
                            939 ;	genReceive
   0155 E5 82               940 	mov	a,dpl
   0157 90 00 13            941 	mov	dptr,#_lcdputch_cc_1_1
   015A F0                  942 	movx	@dptr,a
                            943 ;	main.c:94: *DATA_WRITE = cc;
                            944 ;	genAssign
   015B 7A 00               945 	mov	r2,#0x00
   015D 7B F4               946 	mov	r3,#0xF4
                            947 ;	genAssign
   015F 90 00 13            948 	mov	dptr,#_lcdputch_cc_1_1
   0162 E0                  949 	movx	a,@dptr
                            950 ;	genPointerSet
                            951 ;     genFarPointerSet
   0163 FC                  952 	mov	r4,a
   0164 8A 82               953 	mov	dpl,r2
   0166 8B 83               954 	mov	dph,r3
                            955 ;	Peephole 136	removed redundant move
   0168 F0                  956 	movx	@dptr,a
                            957 ;	main.c:95: lcdbusywait();
                            958 ;	genCall
   0169 12 00 97            959 	lcall	_lcdbusywait
                            960 ;	main.c:96: i=*INST_READ;
                            961 ;	genPointerGet
                            962 ;	genFarPointerGet
                            963 ;	Peephole 182.b	used 16 bit load of dptr
   016C 90 F8 00            964 	mov	dptr,#0xF800
   016F E0                  965 	movx	a,@dptr
                            966 ;	genCast
   0170 FA                  967 	mov	r2,a
   0171 90 00 14            968 	mov	dptr,#_lcdputch_i_1_1
                            969 ;	Peephole 100	removed redundant mov
   0174 F0                  970 	movx	@dptr,a
   0175 A3                  971 	inc	dptr
                            972 ;	Peephole 181	changed mov to clr
   0176 E4                  973 	clr	a
   0177 F0                  974 	movx	@dptr,a
                            975 ;	main.c:98: temp = i&0x7F;
                            976 ;	genAssign
   0178 90 00 14            977 	mov	dptr,#_lcdputch_i_1_1
   017B E0                  978 	movx	a,@dptr
   017C FA                  979 	mov	r2,a
   017D A3                  980 	inc	dptr
   017E E0                  981 	movx	a,@dptr
   017F FB                  982 	mov	r3,a
                            983 ;	genAnd
   0180 90 00 16            984 	mov	dptr,#_lcdputch_temp_1_1
   0183 74 7F               985 	mov	a,#0x7F
   0185 5A                  986 	anl	a,r2
   0186 F0                  987 	movx	@dptr,a
   0187 A3                  988 	inc	dptr
                            989 ;	Peephole 181	changed mov to clr
   0188 E4                  990 	clr	a
   0189 F0                  991 	movx	@dptr,a
                            992 ;	main.c:99: if(temp==0x10)
                            993 ;	genAssign
   018A 90 00 16            994 	mov	dptr,#_lcdputch_temp_1_1
   018D E0                  995 	movx	a,@dptr
   018E FA                  996 	mov	r2,a
   018F A3                  997 	inc	dptr
   0190 E0                  998 	movx	a,@dptr
   0191 FB                  999 	mov	r3,a
                           1000 ;	genCmpEq
                           1001 ;	gencjneshort
                           1002 ;	Peephole 112.b	changed ljmp to sjmp
                           1003 ;	Peephole 198.a	optimized misc jump sequence
   0192 BA 10 0E           1004 	cjne	r2,#0x10,00110$
   0195 BB 00 0B           1005 	cjne	r3,#0x00,00110$
                           1006 ;	Peephole 200.b	removed redundant sjmp
                           1007 ;	Peephole 300	removed redundant label 00118$
                           1008 ;	Peephole 300	removed redundant label 00119$
                           1009 ;	main.c:101: lcdgotoxy(1,0);
                           1010 ;	genAssign
   0198 90 00 11           1011 	mov	dptr,#_lcdgotoxy_PARM_2
                           1012 ;	Peephole 181	changed mov to clr
   019B E4                 1013 	clr	a
   019C F0                 1014 	movx	@dptr,a
                           1015 ;	genCall
   019D 75 82 01           1016 	mov	dpl,#0x01
                           1017 ;	Peephole 112.b	changed ljmp to sjmp
                           1018 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1019 ;	Peephole 253.a	replaced lcall/ret with ljmp
   01A0 02 00 EC           1020 	ljmp	_lcdgotoxy
   01A3                    1021 00110$:
                           1022 ;	main.c:103: else if(temp==0x50)
                           1023 ;	genAssign
   01A3 90 00 16           1024 	mov	dptr,#_lcdputch_temp_1_1
   01A6 E0                 1025 	movx	a,@dptr
   01A7 FA                 1026 	mov	r2,a
   01A8 A3                 1027 	inc	dptr
   01A9 E0                 1028 	movx	a,@dptr
   01AA FB                 1029 	mov	r3,a
                           1030 ;	genCmpEq
                           1031 ;	gencjneshort
                           1032 ;	Peephole 112.b	changed ljmp to sjmp
                           1033 ;	Peephole 198.a	optimized misc jump sequence
   01AB BA 50 0E           1034 	cjne	r2,#0x50,00107$
   01AE BB 00 0B           1035 	cjne	r3,#0x00,00107$
                           1036 ;	Peephole 200.b	removed redundant sjmp
                           1037 ;	Peephole 300	removed redundant label 00120$
                           1038 ;	Peephole 300	removed redundant label 00121$
                           1039 ;	main.c:105: lcdgotoxy(2,0);
                           1040 ;	genAssign
   01B1 90 00 11           1041 	mov	dptr,#_lcdgotoxy_PARM_2
                           1042 ;	Peephole 181	changed mov to clr
   01B4 E4                 1043 	clr	a
   01B5 F0                 1044 	movx	@dptr,a
                           1045 ;	genCall
   01B6 75 82 02           1046 	mov	dpl,#0x02
                           1047 ;	Peephole 112.b	changed ljmp to sjmp
                           1048 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1049 ;	Peephole 253.a	replaced lcall/ret with ljmp
   01B9 02 00 EC           1050 	ljmp	_lcdgotoxy
   01BC                    1051 00107$:
                           1052 ;	main.c:107: else if(temp==0x20)
                           1053 ;	genAssign
   01BC 90 00 16           1054 	mov	dptr,#_lcdputch_temp_1_1
   01BF E0                 1055 	movx	a,@dptr
   01C0 FA                 1056 	mov	r2,a
   01C1 A3                 1057 	inc	dptr
   01C2 E0                 1058 	movx	a,@dptr
   01C3 FB                 1059 	mov	r3,a
                           1060 ;	genCmpEq
                           1061 ;	gencjneshort
                           1062 ;	Peephole 112.b	changed ljmp to sjmp
                           1063 ;	Peephole 198.a	optimized misc jump sequence
   01C4 BA 20 0E           1064 	cjne	r2,#0x20,00104$
   01C7 BB 00 0B           1065 	cjne	r3,#0x00,00104$
                           1066 ;	Peephole 200.b	removed redundant sjmp
                           1067 ;	Peephole 300	removed redundant label 00122$
                           1068 ;	Peephole 300	removed redundant label 00123$
                           1069 ;	main.c:109: lcdgotoxy(3,0);
                           1070 ;	genAssign
   01CA 90 00 11           1071 	mov	dptr,#_lcdgotoxy_PARM_2
                           1072 ;	Peephole 181	changed mov to clr
   01CD E4                 1073 	clr	a
   01CE F0                 1074 	movx	@dptr,a
                           1075 ;	genCall
   01CF 75 82 03           1076 	mov	dpl,#0x03
                           1077 ;	Peephole 112.b	changed ljmp to sjmp
                           1078 ;	Peephole 251.b	replaced sjmp to ret with ret
                           1079 ;	Peephole 253.a	replaced lcall/ret with ljmp
   01D2 02 00 EC           1080 	ljmp	_lcdgotoxy
   01D5                    1081 00104$:
                           1082 ;	main.c:111: else if(temp==0x60)
                           1083 ;	genAssign
   01D5 90 00 16           1084 	mov	dptr,#_lcdputch_temp_1_1
   01D8 E0                 1085 	movx	a,@dptr
   01D9 FA                 1086 	mov	r2,a
   01DA A3                 1087 	inc	dptr
   01DB E0                 1088 	movx	a,@dptr
   01DC FB                 1089 	mov	r3,a
                           1090 ;	genCmpEq
                           1091 ;	gencjneshort
                           1092 ;	Peephole 112.b	changed ljmp to sjmp
                           1093 ;	Peephole 198.a	optimized misc jump sequence
   01DD BA 60 0E           1094 	cjne	r2,#0x60,00112$
   01E0 BB 00 0B           1095 	cjne	r3,#0x00,00112$
                           1096 ;	Peephole 200.b	removed redundant sjmp
                           1097 ;	Peephole 300	removed redundant label 00124$
                           1098 ;	Peephole 300	removed redundant label 00125$
                           1099 ;	main.c:113: lcdgotoxy(0,0);//lcdgotoaddr(temp);
                           1100 ;	genAssign
   01E3 90 00 11           1101 	mov	dptr,#_lcdgotoxy_PARM_2
                           1102 ;	Peephole 181	changed mov to clr
   01E6 E4                 1103 	clr	a
   01E7 F0                 1104 	movx	@dptr,a
                           1105 ;	genCall
   01E8 75 82 00           1106 	mov	dpl,#0x00
                           1107 ;	Peephole 253.c	replaced lcall with ljmp
   01EB 02 00 EC           1108 	ljmp	_lcdgotoxy
   01EE                    1109 00112$:
   01EE 22                 1110 	ret
                           1111 ;------------------------------------------------------------
                           1112 ;Allocation info for local variables in function 'putstr'
                           1113 ;------------------------------------------------------------
                           1114 ;ptr                       Allocated with name '_putstr_ptr_1_1'
                           1115 ;------------------------------------------------------------
                           1116 ;	main.c:123: void putstr(char *ptr)
                           1117 ;	-----------------------------------------
                           1118 ;	 function putstr
                           1119 ;	-----------------------------------------
   01EF                    1120 _putstr:
                           1121 ;	genReceive
   01EF AA F0              1122 	mov	r2,b
   01F1 AB 83              1123 	mov	r3,dph
   01F3 E5 82              1124 	mov	a,dpl
   01F5 90 00 18           1125 	mov	dptr,#_putstr_ptr_1_1
   01F8 F0                 1126 	movx	@dptr,a
   01F9 A3                 1127 	inc	dptr
   01FA EB                 1128 	mov	a,r3
   01FB F0                 1129 	movx	@dptr,a
   01FC A3                 1130 	inc	dptr
   01FD EA                 1131 	mov	a,r2
   01FE F0                 1132 	movx	@dptr,a
                           1133 ;	main.c:125: while(*ptr)
                           1134 ;	genAssign
   01FF 90 00 18           1135 	mov	dptr,#_putstr_ptr_1_1
   0202 E0                 1136 	movx	a,@dptr
   0203 FA                 1137 	mov	r2,a
   0204 A3                 1138 	inc	dptr
   0205 E0                 1139 	movx	a,@dptr
   0206 FB                 1140 	mov	r3,a
   0207 A3                 1141 	inc	dptr
   0208 E0                 1142 	movx	a,@dptr
   0209 FC                 1143 	mov	r4,a
   020A                    1144 00101$:
                           1145 ;	genPointerGet
                           1146 ;	genGenPointerGet
   020A 8A 82              1147 	mov	dpl,r2
   020C 8B 83              1148 	mov	dph,r3
   020E 8C F0              1149 	mov	b,r4
   0210 12 13 EC           1150 	lcall	__gptrget
                           1151 ;	genIfx
   0213 FD                 1152 	mov	r5,a
                           1153 ;	Peephole 105	removed redundant mov
                           1154 ;	genIfxJump
                           1155 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0214 60 23              1156 	jz	00108$
                           1157 ;	Peephole 300	removed redundant label 00109$
                           1158 ;	main.c:127: putchar(*ptr);
                           1159 ;	genCall
   0216 8D 82              1160 	mov	dpl,r5
   0218 C0 02              1161 	push	ar2
   021A C0 03              1162 	push	ar3
   021C C0 04              1163 	push	ar4
   021E 12 03 03           1164 	lcall	_putchar
   0221 D0 04              1165 	pop	ar4
   0223 D0 03              1166 	pop	ar3
   0225 D0 02              1167 	pop	ar2
                           1168 ;	main.c:128: ptr++;
                           1169 ;	genPlus
                           1170 ;     genPlusIncr
   0227 0A                 1171 	inc	r2
   0228 BA 00 01           1172 	cjne	r2,#0x00,00110$
   022B 0B                 1173 	inc	r3
   022C                    1174 00110$:
                           1175 ;	genAssign
   022C 90 00 18           1176 	mov	dptr,#_putstr_ptr_1_1
   022F EA                 1177 	mov	a,r2
   0230 F0                 1178 	movx	@dptr,a
   0231 A3                 1179 	inc	dptr
   0232 EB                 1180 	mov	a,r3
   0233 F0                 1181 	movx	@dptr,a
   0234 A3                 1182 	inc	dptr
   0235 EC                 1183 	mov	a,r4
   0236 F0                 1184 	movx	@dptr,a
                           1185 ;	Peephole 112.b	changed ljmp to sjmp
   0237 80 D1              1186 	sjmp	00101$
   0239                    1187 00108$:
                           1188 ;	genAssign
   0239 90 00 18           1189 	mov	dptr,#_putstr_ptr_1_1
   023C EA                 1190 	mov	a,r2
   023D F0                 1191 	movx	@dptr,a
   023E A3                 1192 	inc	dptr
   023F EB                 1193 	mov	a,r3
   0240 F0                 1194 	movx	@dptr,a
   0241 A3                 1195 	inc	dptr
   0242 EC                 1196 	mov	a,r4
   0243 F0                 1197 	movx	@dptr,a
                           1198 ;	Peephole 300	removed redundant label 00104$
   0244 22                 1199 	ret
                           1200 ;------------------------------------------------------------
                           1201 ;Allocation info for local variables in function 'lcdputstr'
                           1202 ;------------------------------------------------------------
                           1203 ;ptr                       Allocated with name '_lcdputstr_ptr_1_1'
                           1204 ;------------------------------------------------------------
                           1205 ;	main.c:134: void lcdputstr(unsigned char *ptr)
                           1206 ;	-----------------------------------------
                           1207 ;	 function lcdputstr
                           1208 ;	-----------------------------------------
   0245                    1209 _lcdputstr:
                           1210 ;	genReceive
   0245 AA F0              1211 	mov	r2,b
   0247 AB 83              1212 	mov	r3,dph
   0249 E5 82              1213 	mov	a,dpl
   024B 90 00 1B           1214 	mov	dptr,#_lcdputstr_ptr_1_1
   024E F0                 1215 	movx	@dptr,a
   024F A3                 1216 	inc	dptr
   0250 EB                 1217 	mov	a,r3
   0251 F0                 1218 	movx	@dptr,a
   0252 A3                 1219 	inc	dptr
   0253 EA                 1220 	mov	a,r2
   0254 F0                 1221 	movx	@dptr,a
                           1222 ;	main.c:136: while(*ptr)
                           1223 ;	genAssign
   0255 90 00 1B           1224 	mov	dptr,#_lcdputstr_ptr_1_1
   0258 E0                 1225 	movx	a,@dptr
   0259 FA                 1226 	mov	r2,a
   025A A3                 1227 	inc	dptr
   025B E0                 1228 	movx	a,@dptr
   025C FB                 1229 	mov	r3,a
   025D A3                 1230 	inc	dptr
   025E E0                 1231 	movx	a,@dptr
   025F FC                 1232 	mov	r4,a
   0260                    1233 00101$:
                           1234 ;	genPointerGet
                           1235 ;	genGenPointerGet
   0260 8A 82              1236 	mov	dpl,r2
   0262 8B 83              1237 	mov	dph,r3
   0264 8C F0              1238 	mov	b,r4
   0266 12 13 EC           1239 	lcall	__gptrget
                           1240 ;	genIfx
   0269 FD                 1241 	mov	r5,a
                           1242 ;	Peephole 105	removed redundant mov
                           1243 ;	genIfxJump
                           1244 ;	Peephole 108.c	removed ljmp by inverse jump logic
   026A 60 23              1245 	jz	00108$
                           1246 ;	Peephole 300	removed redundant label 00109$
                           1247 ;	main.c:138: lcdputch(*ptr);
                           1248 ;	genCall
   026C 8D 82              1249 	mov	dpl,r5
   026E C0 02              1250 	push	ar2
   0270 C0 03              1251 	push	ar3
   0272 C0 04              1252 	push	ar4
   0274 12 01 55           1253 	lcall	_lcdputch
   0277 D0 04              1254 	pop	ar4
   0279 D0 03              1255 	pop	ar3
   027B D0 02              1256 	pop	ar2
                           1257 ;	main.c:139: ptr++;
                           1258 ;	genPlus
                           1259 ;     genPlusIncr
   027D 0A                 1260 	inc	r2
   027E BA 00 01           1261 	cjne	r2,#0x00,00110$
   0281 0B                 1262 	inc	r3
   0282                    1263 00110$:
                           1264 ;	genAssign
   0282 90 00 1B           1265 	mov	dptr,#_lcdputstr_ptr_1_1
   0285 EA                 1266 	mov	a,r2
   0286 F0                 1267 	movx	@dptr,a
   0287 A3                 1268 	inc	dptr
   0288 EB                 1269 	mov	a,r3
   0289 F0                 1270 	movx	@dptr,a
   028A A3                 1271 	inc	dptr
   028B EC                 1272 	mov	a,r4
   028C F0                 1273 	movx	@dptr,a
                           1274 ;	Peephole 112.b	changed ljmp to sjmp
   028D 80 D1              1275 	sjmp	00101$
   028F                    1276 00108$:
                           1277 ;	genAssign
   028F 90 00 1B           1278 	mov	dptr,#_lcdputstr_ptr_1_1
   0292 EA                 1279 	mov	a,r2
   0293 F0                 1280 	movx	@dptr,a
   0294 A3                 1281 	inc	dptr
   0295 EB                 1282 	mov	a,r3
   0296 F0                 1283 	movx	@dptr,a
   0297 A3                 1284 	inc	dptr
   0298 EC                 1285 	mov	a,r4
   0299 F0                 1286 	movx	@dptr,a
                           1287 ;	Peephole 300	removed redundant label 00104$
   029A 22                 1288 	ret
                           1289 ;------------------------------------------------------------
                           1290 ;Allocation info for local variables in function 'lcdinit'
                           1291 ;------------------------------------------------------------
                           1292 ;------------------------------------------------------------
                           1293 ;	main.c:148: void lcdinit()
                           1294 ;	-----------------------------------------
                           1295 ;	 function lcdinit
                           1296 ;	-----------------------------------------
   029B                    1297 _lcdinit:
                           1298 ;	main.c:150: MSDelay(20);
                           1299 ;	genCall
                           1300 ;	Peephole 182.b	used 16 bit load of dptr
   029B 90 00 14           1301 	mov	dptr,#0x0014
   029E 12 00 64           1302 	lcall	_MSDelay
                           1303 ;	main.c:151: *INST_WRITE = 0x30;
                           1304 ;	genAssign
                           1305 ;	Peephole 182.b	used 16 bit load of dptr
   02A1 90 F0 00           1306 	mov	dptr,#0xF000
                           1307 ;	genPointerSet
                           1308 ;     genFarPointerSet
   02A4 74 30              1309 	mov	a,#0x30
   02A6 F0                 1310 	movx	@dptr,a
                           1311 ;	main.c:152: MSDelay(10);
                           1312 ;	genCall
                           1313 ;	Peephole 182.b	used 16 bit load of dptr
   02A7 90 00 0A           1314 	mov	dptr,#0x000A
   02AA 12 00 64           1315 	lcall	_MSDelay
                           1316 ;	main.c:153: *INST_WRITE = 0x30;
                           1317 ;	genAssign
                           1318 ;	Peephole 182.b	used 16 bit load of dptr
   02AD 90 F0 00           1319 	mov	dptr,#0xF000
                           1320 ;	genPointerSet
                           1321 ;     genFarPointerSet
   02B0 74 30              1322 	mov	a,#0x30
   02B2 F0                 1323 	movx	@dptr,a
                           1324 ;	main.c:154: MSDelay(10);
                           1325 ;	genCall
                           1326 ;	Peephole 182.b	used 16 bit load of dptr
   02B3 90 00 0A           1327 	mov	dptr,#0x000A
   02B6 12 00 64           1328 	lcall	_MSDelay
                           1329 ;	main.c:155: *INST_WRITE = 0x30;
                           1330 ;	genAssign
                           1331 ;	Peephole 182.b	used 16 bit load of dptr
   02B9 90 F0 00           1332 	mov	dptr,#0xF000
                           1333 ;	genPointerSet
                           1334 ;     genFarPointerSet
   02BC 74 30              1335 	mov	a,#0x30
   02BE F0                 1336 	movx	@dptr,a
                           1337 ;	main.c:156: lcdbusywait();
                           1338 ;	genCall
   02BF 12 00 97           1339 	lcall	_lcdbusywait
                           1340 ;	main.c:157: *INST_WRITE = 0x38;
                           1341 ;	genAssign
                           1342 ;	Peephole 182.b	used 16 bit load of dptr
   02C2 90 F0 00           1343 	mov	dptr,#0xF000
                           1344 ;	genPointerSet
                           1345 ;     genFarPointerSet
   02C5 74 38              1346 	mov	a,#0x38
   02C7 F0                 1347 	movx	@dptr,a
                           1348 ;	main.c:158: lcdbusywait();
                           1349 ;	genCall
   02C8 12 00 97           1350 	lcall	_lcdbusywait
                           1351 ;	main.c:159: *INST_WRITE = 0x08;
                           1352 ;	genAssign
                           1353 ;	Peephole 182.b	used 16 bit load of dptr
   02CB 90 F0 00           1354 	mov	dptr,#0xF000
                           1355 ;	genPointerSet
                           1356 ;     genFarPointerSet
   02CE 74 08              1357 	mov	a,#0x08
   02D0 F0                 1358 	movx	@dptr,a
                           1359 ;	main.c:160: lcdbusywait();
                           1360 ;	genCall
   02D1 12 00 97           1361 	lcall	_lcdbusywait
                           1362 ;	main.c:161: *INST_WRITE = 0x0C;
                           1363 ;	genAssign
                           1364 ;	Peephole 182.b	used 16 bit load of dptr
   02D4 90 F0 00           1365 	mov	dptr,#0xF000
                           1366 ;	genPointerSet
                           1367 ;     genFarPointerSet
   02D7 74 0C              1368 	mov	a,#0x0C
   02D9 F0                 1369 	movx	@dptr,a
                           1370 ;	main.c:162: lcdbusywait();
                           1371 ;	genCall
   02DA 12 00 97           1372 	lcall	_lcdbusywait
                           1373 ;	main.c:163: *INST_WRITE = 0x06;
                           1374 ;	genAssign
                           1375 ;	Peephole 182.b	used 16 bit load of dptr
   02DD 90 F0 00           1376 	mov	dptr,#0xF000
                           1377 ;	genPointerSet
                           1378 ;     genFarPointerSet
   02E0 74 06              1379 	mov	a,#0x06
   02E2 F0                 1380 	movx	@dptr,a
                           1381 ;	main.c:164: lcdbusywait();
                           1382 ;	genCall
   02E3 12 00 97           1383 	lcall	_lcdbusywait
                           1384 ;	main.c:165: *INST_WRITE = 0x01;
                           1385 ;	genAssign
                           1386 ;	Peephole 182.b	used 16 bit load of dptr
   02E6 90 F0 00           1387 	mov	dptr,#0xF000
                           1388 ;	genPointerSet
                           1389 ;     genFarPointerSet
   02E9 74 01              1390 	mov	a,#0x01
   02EB F0                 1391 	movx	@dptr,a
                           1392 ;	Peephole 300	removed redundant label 00101$
   02EC 22                 1393 	ret
                           1394 ;------------------------------------------------------------
                           1395 ;Allocation info for local variables in function '_sdcc_external_startup'
                           1396 ;------------------------------------------------------------
                           1397 ;------------------------------------------------------------
                           1398 ;	main.c:168: _sdcc_external_startup()
                           1399 ;	-----------------------------------------
                           1400 ;	 function _sdcc_external_startup
                           1401 ;	-----------------------------------------
   02ED                    1402 __sdcc_external_startup:
                           1403 ;	main.c:170: AUXR |= 0x0C;
                           1404 ;	genOr
   02ED 43 8E 0C           1405 	orl	_AUXR,#0x0C
                           1406 ;	main.c:171: return 0;
                           1407 ;	genRet
                           1408 ;	Peephole 182.b	used 16 bit load of dptr
   02F0 90 00 00           1409 	mov	dptr,#0x0000
                           1410 ;	Peephole 300	removed redundant label 00101$
   02F3 22                 1411 	ret
                           1412 ;------------------------------------------------------------
                           1413 ;Allocation info for local variables in function 'SerialInitialize'
                           1414 ;------------------------------------------------------------
                           1415 ;------------------------------------------------------------
                           1416 ;	main.c:174: void SerialInitialize()
                           1417 ;	-----------------------------------------
                           1418 ;	 function SerialInitialize
                           1419 ;	-----------------------------------------
   02F4                    1420 _SerialInitialize:
                           1421 ;	main.c:176: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
                           1422 ;	genAssign
   02F4 75 89 20           1423 	mov	_TMOD,#0x20
                           1424 ;	main.c:177: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
                           1425 ;	genAssign
   02F7 75 8D FD           1426 	mov	_TH1,#0xFD
                           1427 ;	main.c:178: SCON=0x50;
                           1428 ;	genAssign
   02FA 75 98 50           1429 	mov	_SCON,#0x50
                           1430 ;	main.c:179: TR1=1;  // to start timer
                           1431 ;	genAssign
   02FD D2 8E              1432 	setb	_TR1
                           1433 ;	main.c:180: IE=0x90; // to make serial interrupt interrupt enable
                           1434 ;	genAssign
   02FF 75 A8 90           1435 	mov	_IE,#0x90
                           1436 ;	Peephole 300	removed redundant label 00101$
   0302 22                 1437 	ret
                           1438 ;------------------------------------------------------------
                           1439 ;Allocation info for local variables in function 'putchar'
                           1440 ;------------------------------------------------------------
                           1441 ;x                         Allocated with name '_putchar_x_1_1'
                           1442 ;------------------------------------------------------------
                           1443 ;	main.c:183: void putchar(char x)
                           1444 ;	-----------------------------------------
                           1445 ;	 function putchar
                           1446 ;	-----------------------------------------
   0303                    1447 _putchar:
                           1448 ;	genReceive
   0303 E5 82              1449 	mov	a,dpl
   0305 90 00 1E           1450 	mov	dptr,#_putchar_x_1_1
   0308 F0                 1451 	movx	@dptr,a
                           1452 ;	main.c:185: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
                           1453 ;	genAssign
   0309 90 00 1E           1454 	mov	dptr,#_putchar_x_1_1
   030C E0                 1455 	movx	a,@dptr
   030D F5 99              1456 	mov	_SBUF,a
                           1457 ;	main.c:186: while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
   030F                    1458 00101$:
                           1459 ;	genIfx
                           1460 ;	genIfxJump
                           1461 ;	Peephole 108.d	removed ljmp by inverse jump logic
                           1462 ;	main.c:187: TI=0; // make TI bit to zero for next transmission
                           1463 ;	genAssign
                           1464 ;	Peephole 250.a	using atomic test and clear
   030F 10 99 02           1465 	jbc	_TI,00108$
   0312 80 FB              1466 	sjmp	00101$
   0314                    1467 00108$:
                           1468 ;	Peephole 300	removed redundant label 00104$
   0314 22                 1469 	ret
                           1470 ;------------------------------------------------------------
                           1471 ;Allocation info for local variables in function 'getchar'
                           1472 ;------------------------------------------------------------
                           1473 ;------------------------------------------------------------
                           1474 ;	main.c:191: char getchar()
                           1475 ;	-----------------------------------------
                           1476 ;	 function getchar
                           1477 ;	-----------------------------------------
   0315                    1478 _getchar:
                           1479 ;	main.c:194: while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
   0315                    1480 00101$:
                           1481 ;	genIfx
                           1482 ;	genIfxJump
                           1483 ;	Peephole 108.d	removed ljmp by inverse jump logic
   0315 30 98 FD           1484 	jnb	_RI,00101$
                           1485 ;	Peephole 300	removed redundant label 00108$
                           1486 ;	main.c:195: rec=SBUF;   // data is received in SBUF register present in controller during receiving
                           1487 ;	genAssign
   0318 AA 99              1488 	mov	r2,_SBUF
                           1489 ;	genAssign
   031A 90 00 02           1490 	mov	dptr,#_rec
   031D EA                 1491 	mov	a,r2
   031E F0                 1492 	movx	@dptr,a
                           1493 ;	main.c:196: RI=0;  // make RI bit to 0 so that next data is received
                           1494 ;	genAssign
   031F C2 98              1495 	clr	_RI
                           1496 ;	main.c:197: return rec;  // return rec where function is called
                           1497 ;	genRet
   0321 8A 82              1498 	mov	dpl,r2
                           1499 ;	Peephole 300	removed redundant label 00104$
   0323 22                 1500 	ret
                           1501 ;------------------------------------------------------------
                           1502 ;Allocation info for local variables in function 'help_menu'
                           1503 ;------------------------------------------------------------
                           1504 ;------------------------------------------------------------
                           1505 ;	main.c:200: void help_menu()
                           1506 ;	-----------------------------------------
                           1507 ;	 function help_menu
                           1508 ;	-----------------------------------------
   0324                    1509 _help_menu:
                           1510 ;	main.c:202: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           1511 ;	genCall
                           1512 ;	Peephole 182.a	used 16 bit load of DPTR
   0324 90 14 42           1513 	mov	dptr,#__str_1
   0327 75 F0 80           1514 	mov	b,#0x80
   032A 12 01 EF           1515 	lcall	_putstr
                           1516 ;	main.c:203: putstr("Welcome to Help Menu\n\r");
                           1517 ;	genCall
                           1518 ;	Peephole 182.a	used 16 bit load of DPTR
   032D 90 14 9B           1519 	mov	dptr,#__str_2
   0330 75 F0 80           1520 	mov	b,#0x80
   0333 12 01 EF           1521 	lcall	_putstr
                           1522 ;	main.c:204: putstr("Press 'C' for CGRAM HEX dump\n\r");
                           1523 ;	genCall
                           1524 ;	Peephole 182.a	used 16 bit load of DPTR
   0336 90 14 B2           1525 	mov	dptr,#__str_3
   0339 75 F0 80           1526 	mov	b,#0x80
   033C 12 01 EF           1527 	lcall	_putstr
                           1528 ;	main.c:205: putstr("Press 'D' for DDRAM HEX dump\n\r");
                           1529 ;	genCall
                           1530 ;	Peephole 182.a	used 16 bit load of DPTR
   033F 90 14 D1           1531 	mov	dptr,#__str_4
   0342 75 F0 80           1532 	mov	b,#0x80
   0345 12 01 EF           1533 	lcall	_putstr
                           1534 ;	main.c:206: putstr("Press 'E' for Clearing the LCD\n\r");
                           1535 ;	genCall
                           1536 ;	Peephole 182.a	used 16 bit load of DPTR
   0348 90 14 F0           1537 	mov	dptr,#__str_5
   034B 75 F0 80           1538 	mov	b,#0x80
   034E 12 01 EF           1539 	lcall	_putstr
                           1540 ;	main.c:207: putstr("Press 'S' to display the custom character\n\r");
                           1541 ;	genCall
                           1542 ;	Peephole 182.a	used 16 bit load of DPTR
   0351 90 15 11           1543 	mov	dptr,#__str_6
   0354 75 F0 80           1544 	mov	b,#0x80
   0357 12 01 EF           1545 	lcall	_putstr
                           1546 ;	main.c:208: putstr("Press 'A' to add custom character\n\r");
                           1547 ;	genCall
                           1548 ;	Peephole 182.a	used 16 bit load of DPTR
   035A 90 15 3D           1549 	mov	dptr,#__str_7
   035D 75 F0 80           1550 	mov	b,#0x80
   0360 12 01 EF           1551 	lcall	_putstr
                           1552 ;	main.c:209: putstr("Press 'I' for interactive mode\n\r");
                           1553 ;	genCall
                           1554 ;	Peephole 182.a	used 16 bit load of DPTR
   0363 90 15 61           1555 	mov	dptr,#__str_8
   0366 75 F0 80           1556 	mov	b,#0x80
   0369 12 01 EF           1557 	lcall	_putstr
                           1558 ;	main.c:210: putstr("Press 'X' for moving the cursor to user input x,y position\n\r");
                           1559 ;	genCall
                           1560 ;	Peephole 182.a	used 16 bit load of DPTR
   036C 90 15 82           1561 	mov	dptr,#__str_9
   036F 75 F0 80           1562 	mov	b,#0x80
   0372 12 01 EF           1563 	lcall	_putstr
                           1564 ;	main.c:211: putstr("Press 'G' for moving the cursor to user input address\n\r");
                           1565 ;	genCall
                           1566 ;	Peephole 182.a	used 16 bit load of DPTR
   0375 90 15 BF           1567 	mov	dptr,#__str_10
   0378 75 F0 80           1568 	mov	b,#0x80
   037B 12 01 EF           1569 	lcall	_putstr
                           1570 ;	main.c:212: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           1571 ;	genCall
                           1572 ;	Peephole 182.a	used 16 bit load of DPTR
   037E 90 14 42           1573 	mov	dptr,#__str_1
   0381 75 F0 80           1574 	mov	b,#0x80
                           1575 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0384 02 01 EF           1576 	ljmp	_putstr
                           1577 ;
                           1578 ;------------------------------------------------------------
                           1579 ;Allocation info for local variables in function 'cgramdump'
                           1580 ;------------------------------------------------------------
                           1581 ;count_row                 Allocated with name '_cgramdump_count_row_1_1'
                           1582 ;count                     Allocated with name '_cgramdump_count_1_1'
                           1583 ;temp_val                  Allocated with name '_cgramdump_temp_val_1_1'
                           1584 ;k                         Allocated with name '_cgramdump_k_1_1'
                           1585 ;------------------------------------------------------------
                           1586 ;	main.c:215: void cgramdump()
                           1587 ;	-----------------------------------------
                           1588 ;	 function cgramdump
                           1589 ;	-----------------------------------------
   0387                    1590 _cgramdump:
                           1591 ;	main.c:219: unsigned char temp_val=0x40, k;
                           1592 ;	genAssign
   0387 90 00 1F           1593 	mov	dptr,#_cgramdump_temp_val_1_1
   038A 74 40              1594 	mov	a,#0x40
   038C F0                 1595 	movx	@dptr,a
                           1596 ;	main.c:220: k=*INST_READ;
                           1597 ;	genPointerGet
                           1598 ;	genFarPointerGet
                           1599 ;	Peephole 182.b	used 16 bit load of dptr
   038D 90 F8 00           1600 	mov	dptr,#0xF800
   0390 E0                 1601 	movx	a,@dptr
   0391 FA                 1602 	mov	r2,a
                           1603 ;	main.c:221: *INST_WRITE = temp_val;
                           1604 ;	genAssign
                           1605 ;	Peephole 182.b	used 16 bit load of dptr
   0392 90 F0 00           1606 	mov	dptr,#0xF000
                           1607 ;	genPointerSet
                           1608 ;     genFarPointerSet
   0395 74 40              1609 	mov	a,#0x40
   0397 F0                 1610 	movx	@dptr,a
                           1611 ;	main.c:222: lcdbusywait();
                           1612 ;	genCall
   0398 C0 02              1613 	push	ar2
   039A 12 00 97           1614 	lcall	_lcdbusywait
   039D D0 02              1615 	pop	ar2
                           1616 ;	main.c:223: putstr("\n\r");
                           1617 ;	genCall
                           1618 ;	Peephole 182.a	used 16 bit load of DPTR
   039F 90 15 F7           1619 	mov	dptr,#__str_11
   03A2 75 F0 80           1620 	mov	b,#0x80
   03A5 C0 02              1621 	push	ar2
   03A7 12 01 EF           1622 	lcall	_putstr
   03AA D0 02              1623 	pop	ar2
                           1624 ;	main.c:224: putstr("CGRAM values\n\r");
                           1625 ;	genCall
                           1626 ;	Peephole 182.a	used 16 bit load of DPTR
   03AC 90 15 FA           1627 	mov	dptr,#__str_12
   03AF 75 F0 80           1628 	mov	b,#0x80
   03B2 C0 02              1629 	push	ar2
   03B4 12 01 EF           1630 	lcall	_putstr
   03B7 D0 02              1631 	pop	ar2
                           1632 ;	main.c:225: putstr("\n\rCCODE |  Values\n\r");
                           1633 ;	genCall
                           1634 ;	Peephole 182.a	used 16 bit load of DPTR
   03B9 90 16 09           1635 	mov	dptr,#__str_13
   03BC 75 F0 80           1636 	mov	b,#0x80
   03BF C0 02              1637 	push	ar2
   03C1 12 01 EF           1638 	lcall	_putstr
   03C4 D0 02              1639 	pop	ar2
                           1640 ;	main.c:226: for (count_row=0; count_row<=7; count_row++)
                           1641 ;	genAssign
   03C6 7B 00              1642 	mov	r3,#0x00
   03C8 7C 00              1643 	mov	r4,#0x00
   03CA                    1644 00104$:
                           1645 ;	genCmpGt
                           1646 ;	genCmp
   03CA C3                 1647 	clr	c
   03CB 74 07              1648 	mov	a,#0x07
   03CD 9B                 1649 	subb	a,r3
                           1650 ;	Peephole 181	changed mov to clr
   03CE E4                 1651 	clr	a
   03CF 9C                 1652 	subb	a,r4
                           1653 ;	genIfxJump
   03D0 50 03              1654 	jnc	00116$
   03D2 02 04 8C           1655 	ljmp	00107$
   03D5                    1656 00116$:
                           1657 ;	main.c:228: printf("0x%04x:  ", (temp_val&0x3F));
                           1658 ;	genAssign
   03D5 90 00 1F           1659 	mov	dptr,#_cgramdump_temp_val_1_1
   03D8 E0                 1660 	movx	a,@dptr
   03D9 FD                 1661 	mov	r5,a
                           1662 ;	genAnd
   03DA 74 3F              1663 	mov	a,#0x3F
   03DC 5D                 1664 	anl	a,r5
   03DD FE                 1665 	mov	r6,a
                           1666 ;	genCast
   03DE 7F 00              1667 	mov	r7,#0x00
                           1668 ;	genIpush
   03E0 C0 02              1669 	push	ar2
   03E2 C0 03              1670 	push	ar3
   03E4 C0 04              1671 	push	ar4
   03E6 C0 05              1672 	push	ar5
   03E8 C0 06              1673 	push	ar6
   03EA C0 07              1674 	push	ar7
                           1675 ;	genIpush
   03EC 74 1D              1676 	mov	a,#__str_14
   03EE C0 E0              1677 	push	acc
   03F0 74 16              1678 	mov	a,#(__str_14 >> 8)
   03F2 C0 E0              1679 	push	acc
   03F4 74 80              1680 	mov	a,#0x80
   03F6 C0 E0              1681 	push	acc
                           1682 ;	genCall
   03F8 12 0B 4D           1683 	lcall	_printf
   03FB E5 81              1684 	mov	a,sp
   03FD 24 FB              1685 	add	a,#0xfb
   03FF F5 81              1686 	mov	sp,a
   0401 D0 05              1687 	pop	ar5
   0403 D0 04              1688 	pop	ar4
   0405 D0 03              1689 	pop	ar3
   0407 D0 02              1690 	pop	ar2
                           1691 ;	main.c:229: for(count=0; count<=7; count++)
                           1692 ;	genAssign
                           1693 ;	genAssign
   0409 7E 08              1694 	mov	r6,#0x08
   040B 7F 00              1695 	mov	r7,#0x00
   040D                    1696 00103$:
                           1697 ;	main.c:231: temp_val++;
                           1698 ;	genPlus
                           1699 ;     genPlusIncr
   040D 0D                 1700 	inc	r5
                           1701 ;	main.c:232: printf("%02x  ",*DATA_READ);
                           1702 ;	genPointerGet
                           1703 ;	genFarPointerGet
                           1704 ;	Peephole 182.b	used 16 bit load of dptr
   040E 90 FC 00           1705 	mov	dptr,#0xFC00
   0411 E0                 1706 	movx	a,@dptr
   0412 F8                 1707 	mov	r0,a
                           1708 ;	genCast
   0413 79 00              1709 	mov	r1,#0x00
                           1710 ;	genIpush
   0415 C0 02              1711 	push	ar2
   0417 C0 03              1712 	push	ar3
   0419 C0 04              1713 	push	ar4
   041B C0 05              1714 	push	ar5
   041D C0 06              1715 	push	ar6
   041F C0 07              1716 	push	ar7
   0421 C0 00              1717 	push	ar0
   0423 C0 01              1718 	push	ar1
                           1719 ;	genIpush
   0425 74 27              1720 	mov	a,#__str_15
   0427 C0 E0              1721 	push	acc
   0429 74 16              1722 	mov	a,#(__str_15 >> 8)
   042B C0 E0              1723 	push	acc
   042D 74 80              1724 	mov	a,#0x80
   042F C0 E0              1725 	push	acc
                           1726 ;	genCall
   0431 12 0B 4D           1727 	lcall	_printf
   0434 E5 81              1728 	mov	a,sp
   0436 24 FB              1729 	add	a,#0xfb
   0438 F5 81              1730 	mov	sp,a
   043A D0 07              1731 	pop	ar7
   043C D0 06              1732 	pop	ar6
   043E D0 05              1733 	pop	ar5
   0440 D0 04              1734 	pop	ar4
   0442 D0 03              1735 	pop	ar3
   0444 D0 02              1736 	pop	ar2
                           1737 ;	main.c:233: lcdbusywait();
                           1738 ;	genCall
   0446 C0 02              1739 	push	ar2
   0448 C0 03              1740 	push	ar3
   044A C0 04              1741 	push	ar4
   044C C0 05              1742 	push	ar5
   044E C0 06              1743 	push	ar6
   0450 C0 07              1744 	push	ar7
   0452 12 00 97           1745 	lcall	_lcdbusywait
   0455 D0 07              1746 	pop	ar7
   0457 D0 06              1747 	pop	ar6
   0459 D0 05              1748 	pop	ar5
   045B D0 04              1749 	pop	ar4
   045D D0 03              1750 	pop	ar3
   045F D0 02              1751 	pop	ar2
                           1752 ;	genMinus
                           1753 ;	genMinusDec
   0461 1E                 1754 	dec	r6
   0462 BE FF 01           1755 	cjne	r6,#0xff,00117$
   0465 1F                 1756 	dec	r7
   0466                    1757 00117$:
                           1758 ;	main.c:229: for(count=0; count<=7; count++)
                           1759 ;	genIfx
   0466 EE                 1760 	mov	a,r6
   0467 4F                 1761 	orl	a,r7
                           1762 ;	genIfxJump
                           1763 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0468 70 A3              1764 	jnz	00103$
                           1765 ;	Peephole 300	removed redundant label 00118$
                           1766 ;	main.c:235: putstr("\n\r");
                           1767 ;	genAssign
   046A 90 00 1F           1768 	mov	dptr,#_cgramdump_temp_val_1_1
   046D ED                 1769 	mov	a,r5
   046E F0                 1770 	movx	@dptr,a
                           1771 ;	genCall
                           1772 ;	Peephole 182.a	used 16 bit load of DPTR
   046F 90 15 F7           1773 	mov	dptr,#__str_11
   0472 75 F0 80           1774 	mov	b,#0x80
   0475 C0 02              1775 	push	ar2
   0477 C0 03              1776 	push	ar3
   0479 C0 04              1777 	push	ar4
   047B 12 01 EF           1778 	lcall	_putstr
   047E D0 04              1779 	pop	ar4
   0480 D0 03              1780 	pop	ar3
   0482 D0 02              1781 	pop	ar2
                           1782 ;	main.c:226: for (count_row=0; count_row<=7; count_row++)
                           1783 ;	genPlus
                           1784 ;     genPlusIncr
   0484 0B                 1785 	inc	r3
   0485 BB 00 01           1786 	cjne	r3,#0x00,00119$
   0488 0C                 1787 	inc	r4
   0489                    1788 00119$:
   0489 02 03 CA           1789 	ljmp	00104$
   048C                    1790 00107$:
                           1791 ;	main.c:237: putstr("\n\r");
                           1792 ;	genCall
                           1793 ;	Peephole 182.a	used 16 bit load of DPTR
   048C 90 15 F7           1794 	mov	dptr,#__str_11
   048F 75 F0 80           1795 	mov	b,#0x80
   0492 C0 02              1796 	push	ar2
   0494 12 01 EF           1797 	lcall	_putstr
   0497 D0 02              1798 	pop	ar2
                           1799 ;	main.c:238: *INST_WRITE=(k|0x80);
                           1800 ;	genAssign
                           1801 ;	Peephole 182.b	used 16 bit load of dptr
   0499 90 F0 00           1802 	mov	dptr,#0xF000
                           1803 ;	genOr
   049C 43 02 80           1804 	orl	ar2,#0x80
                           1805 ;	genPointerSet
                           1806 ;     genFarPointerSet
   049F EA                 1807 	mov	a,r2
   04A0 F0                 1808 	movx	@dptr,a
                           1809 ;	main.c:239: lcdbusywait();
                           1810 ;	genCall
                           1811 ;	Peephole 253.b	replaced lcall/ret with ljmp
   04A1 02 00 97           1812 	ljmp	_lcdbusywait
                           1813 ;
                           1814 ;------------------------------------------------------------
                           1815 ;Allocation info for local variables in function 'lcdcreatechar'
                           1816 ;------------------------------------------------------------
                           1817 ;sloc0                     Allocated with name '_lcdcreatechar_sloc0_1_0'
                           1818 ;row_vals                  Allocated with name '_lcdcreatechar_PARM_2'
                           1819 ;ccode                     Allocated with name '_lcdcreatechar_ccode_1_1'
                           1820 ;n                         Allocated with name '_lcdcreatechar_n_1_1'
                           1821 ;k                         Allocated with name '_lcdcreatechar_k_1_1'
                           1822 ;------------------------------------------------------------
                           1823 ;	main.c:243: void lcdcreatechar(unsigned char ccode, unsigned char row_vals[])
                           1824 ;	-----------------------------------------
                           1825 ;	 function lcdcreatechar
                           1826 ;	-----------------------------------------
   04A4                    1827 _lcdcreatechar:
                           1828 ;	genReceive
   04A4 E5 82              1829 	mov	a,dpl
   04A6 90 00 23           1830 	mov	dptr,#_lcdcreatechar_ccode_1_1
   04A9 F0                 1831 	movx	@dptr,a
                           1832 ;	main.c:246: k=*INST_READ;
                           1833 ;	genPointerGet
                           1834 ;	genFarPointerGet
                           1835 ;	Peephole 182.b	used 16 bit load of dptr
   04AA 90 F8 00           1836 	mov	dptr,#0xF800
   04AD E0                 1837 	movx	a,@dptr
   04AE FA                 1838 	mov	r2,a
                           1839 ;	main.c:247: lcdbusywait();
                           1840 ;	genCall
   04AF C0 02              1841 	push	ar2
   04B1 12 00 97           1842 	lcall	_lcdbusywait
   04B4 D0 02              1843 	pop	ar2
                           1844 ;	main.c:248: ccode |= 0x40;
                           1845 ;	genAssign
                           1846 ;	genOr
   04B6 90 00 23           1847 	mov	dptr,#_lcdcreatechar_ccode_1_1
   04B9 E0                 1848 	movx	a,@dptr
   04BA FB                 1849 	mov	r3,a
                           1850 ;	Peephole 248.a	optimized or to xdata
   04BB 44 40              1851 	orl	a,#0x40
   04BD F0                 1852 	movx	@dptr,a
                           1853 ;	main.c:249: for (n=0; n<=7; n++)
                           1854 ;	genAssign
   04BE 90 00 20           1855 	mov	dptr,#_lcdcreatechar_PARM_2
   04C1 E0                 1856 	movx	a,@dptr
   04C2 FB                 1857 	mov	r3,a
   04C3 A3                 1858 	inc	dptr
   04C4 E0                 1859 	movx	a,@dptr
   04C5 FC                 1860 	mov	r4,a
   04C6 A3                 1861 	inc	dptr
   04C7 E0                 1862 	movx	a,@dptr
   04C8 FD                 1863 	mov	r5,a
                           1864 ;	genAssign
   04C9 90 00 23           1865 	mov	dptr,#_lcdcreatechar_ccode_1_1
   04CC E0                 1866 	movx	a,@dptr
   04CD FE                 1867 	mov	r6,a
                           1868 ;	genAssign
   04CE 7F 00              1869 	mov	r7,#0x00
   04D0                    1870 00101$:
                           1871 ;	genCmpGt
                           1872 ;	genCmp
                           1873 ;	genIfxJump
                           1874 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   04D0 EF                 1875 	mov	a,r7
   04D1 24 F8              1876 	add	a,#0xff - 0x07
                           1877 ;	Peephole 112.b	changed ljmp to sjmp
                           1878 ;	Peephole 160.a	removed sjmp by inverse jump logic
   04D3 40 6B              1879 	jc	00109$
                           1880 ;	Peephole 300	removed redundant label 00110$
                           1881 ;	main.c:251: *INST_WRITE = ccode;
                           1882 ;	genIpush
   04D5 C0 02              1883 	push	ar2
                           1884 ;	genAssign
                           1885 ;	Peephole 182.b	used 16 bit load of dptr
   04D7 90 F0 00           1886 	mov	dptr,#0xF000
                           1887 ;	genPointerSet
                           1888 ;     genFarPointerSet
   04DA EE                 1889 	mov	a,r6
   04DB F0                 1890 	movx	@dptr,a
                           1891 ;	main.c:252: lcdbusywait();
                           1892 ;	genCall
   04DC C0 02              1893 	push	ar2
   04DE C0 03              1894 	push	ar3
   04E0 C0 04              1895 	push	ar4
   04E2 C0 05              1896 	push	ar5
   04E4 C0 06              1897 	push	ar6
   04E6 C0 07              1898 	push	ar7
   04E8 12 00 97           1899 	lcall	_lcdbusywait
   04EB D0 07              1900 	pop	ar7
   04ED D0 06              1901 	pop	ar6
   04EF D0 05              1902 	pop	ar5
   04F1 D0 04              1903 	pop	ar4
   04F3 D0 03              1904 	pop	ar3
   04F5 D0 02              1905 	pop	ar2
                           1906 ;	main.c:253: *DATA_WRITE = (row_vals[n]&0x1F);
                           1907 ;	genAssign
   04F7 75 08 00           1908 	mov	_lcdcreatechar_sloc0_1_0,#0x00
   04FA 75 09 F4           1909 	mov	(_lcdcreatechar_sloc0_1_0 + 1),#0xF4
                           1910 ;	genPlus
                           1911 ;	Peephole 236.g	used r7 instead of ar7
   04FD EF                 1912 	mov	a,r7
                           1913 ;	Peephole 236.a	used r3 instead of ar3
   04FE 2B                 1914 	add	a,r3
   04FF FA                 1915 	mov	r2,a
                           1916 ;	Peephole 181	changed mov to clr
   0500 E4                 1917 	clr	a
                           1918 ;	Peephole 236.b	used r4 instead of ar4
   0501 3C                 1919 	addc	a,r4
   0502 F8                 1920 	mov	r0,a
   0503 8D 01              1921 	mov	ar1,r5
                           1922 ;	genPointerGet
                           1923 ;	genGenPointerGet
   0505 8A 82              1924 	mov	dpl,r2
   0507 88 83              1925 	mov	dph,r0
   0509 89 F0              1926 	mov	b,r1
   050B 12 13 EC           1927 	lcall	__gptrget
   050E FA                 1928 	mov	r2,a
                           1929 ;	genAnd
   050F 53 02 1F           1930 	anl	ar2,#0x1F
                           1931 ;	genPointerSet
                           1932 ;     genFarPointerSet
   0512 85 08 82           1933 	mov	dpl,_lcdcreatechar_sloc0_1_0
   0515 85 09 83           1934 	mov	dph,(_lcdcreatechar_sloc0_1_0 + 1)
   0518 EA                 1935 	mov	a,r2
   0519 F0                 1936 	movx	@dptr,a
                           1937 ;	main.c:254: lcdbusywait();
                           1938 ;	genCall
   051A C0 02              1939 	push	ar2
   051C C0 03              1940 	push	ar3
   051E C0 04              1941 	push	ar4
   0520 C0 05              1942 	push	ar5
   0522 C0 06              1943 	push	ar6
   0524 C0 07              1944 	push	ar7
   0526 12 00 97           1945 	lcall	_lcdbusywait
   0529 D0 07              1946 	pop	ar7
   052B D0 06              1947 	pop	ar6
   052D D0 05              1948 	pop	ar5
   052F D0 04              1949 	pop	ar4
   0531 D0 03              1950 	pop	ar3
   0533 D0 02              1951 	pop	ar2
                           1952 ;	main.c:255: ccode++;
                           1953 ;	genPlus
                           1954 ;     genPlusIncr
   0535 0E                 1955 	inc	r6
                           1956 ;	genAssign
   0536 90 00 23           1957 	mov	dptr,#_lcdcreatechar_ccode_1_1
   0539 EE                 1958 	mov	a,r6
   053A F0                 1959 	movx	@dptr,a
                           1960 ;	main.c:249: for (n=0; n<=7; n++)
                           1961 ;	genPlus
                           1962 ;     genPlusIncr
   053B 0F                 1963 	inc	r7
                           1964 ;	genIpop
   053C D0 02              1965 	pop	ar2
                           1966 ;	Peephole 112.b	changed ljmp to sjmp
   053E 80 90              1967 	sjmp	00101$
   0540                    1968 00109$:
                           1969 ;	genAssign
   0540 90 00 23           1970 	mov	dptr,#_lcdcreatechar_ccode_1_1
   0543 EE                 1971 	mov	a,r6
   0544 F0                 1972 	movx	@dptr,a
                           1973 ;	main.c:257: *INST_WRITE=(k|0x80);
                           1974 ;	genAssign
                           1975 ;	Peephole 182.b	used 16 bit load of dptr
   0545 90 F0 00           1976 	mov	dptr,#0xF000
                           1977 ;	genOr
   0548 43 02 80           1978 	orl	ar2,#0x80
                           1979 ;	genPointerSet
                           1980 ;     genFarPointerSet
   054B EA                 1981 	mov	a,r2
   054C F0                 1982 	movx	@dptr,a
                           1983 ;	main.c:258: lcdbusywait();
                           1984 ;	genCall
                           1985 ;	Peephole 253.b	replaced lcall/ret with ljmp
   054D 02 00 97           1986 	ljmp	_lcdbusywait
                           1987 ;
                           1988 ;------------------------------------------------------------
                           1989 ;Allocation info for local variables in function 'ddramdump'
                           1990 ;------------------------------------------------------------
                           1991 ;count_row                 Allocated with name '_ddramdump_count_row_1_1'
                           1992 ;count                     Allocated with name '_ddramdump_count_1_1'
                           1993 ;temp_val                  Allocated with name '_ddramdump_temp_val_1_1'
                           1994 ;k                         Allocated with name '_ddramdump_k_1_1'
                           1995 ;------------------------------------------------------------
                           1996 ;	main.c:261: void ddramdump()
                           1997 ;	-----------------------------------------
                           1998 ;	 function ddramdump
                           1999 ;	-----------------------------------------
   0550                    2000 _ddramdump:
                           2001 ;	main.c:266: k=*INST_READ;
                           2002 ;	genPointerGet
                           2003 ;	genFarPointerGet
                           2004 ;	Peephole 182.b	used 16 bit load of dptr
   0550 90 F8 00           2005 	mov	dptr,#0xF800
   0553 E0                 2006 	movx	a,@dptr
   0554 FA                 2007 	mov	r2,a
                           2008 ;	main.c:267: putstr("\n\r");
                           2009 ;	genCall
                           2010 ;	Peephole 182.a	used 16 bit load of DPTR
   0555 90 15 F7           2011 	mov	dptr,#__str_11
   0558 75 F0 80           2012 	mov	b,#0x80
   055B C0 02              2013 	push	ar2
   055D 12 01 EF           2014 	lcall	_putstr
   0560 D0 02              2015 	pop	ar2
                           2016 ;	main.c:268: putstr("DDRAM Values\n\r");
                           2017 ;	genCall
                           2018 ;	Peephole 182.a	used 16 bit load of DPTR
   0562 90 16 2E           2019 	mov	dptr,#__str_16
   0565 75 F0 80           2020 	mov	b,#0x80
   0568 C0 02              2021 	push	ar2
   056A 12 01 EF           2022 	lcall	_putstr
   056D D0 02              2023 	pop	ar2
                           2024 ;	main.c:269: putstr("\n\rADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C  +D  +E  +F\n\r\n\r");
                           2025 ;	genCall
                           2026 ;	Peephole 182.a	used 16 bit load of DPTR
   056F 90 16 3D           2027 	mov	dptr,#__str_17
   0572 75 F0 80           2028 	mov	b,#0x80
   0575 C0 02              2029 	push	ar2
   0577 12 01 EF           2030 	lcall	_putstr
   057A D0 02              2031 	pop	ar2
                           2032 ;	main.c:270: for (count_row=0; count_row<=3; count_row++)
                           2033 ;	genAssign
   057C 7B 00              2034 	mov	r3,#0x00
   057E 7C 00              2035 	mov	r4,#0x00
   0580                    2036 00112$:
                           2037 ;	genCmpGt
                           2038 ;	genCmp
   0580 C3                 2039 	clr	c
   0581 74 03              2040 	mov	a,#0x03
   0583 9B                 2041 	subb	a,r3
                           2042 ;	Peephole 181	changed mov to clr
   0584 E4                 2043 	clr	a
   0585 9C                 2044 	subb	a,r4
                           2045 ;	genIfxJump
   0586 50 03              2046 	jnc	00128$
   0588 02 06 93           2047 	ljmp	00115$
   058B                    2048 00128$:
                           2049 ;	main.c:272: if(count_row==0)
                           2050 ;	genIfx
   058B EB                 2051 	mov	a,r3
   058C 4C                 2052 	orl	a,r4
                           2053 ;	genIfxJump
                           2054 ;	Peephole 108.b	removed ljmp by inverse jump logic
   058D 70 06              2055 	jnz	00102$
                           2056 ;	Peephole 300	removed redundant label 00129$
                           2057 ;	main.c:273: temp_val=0x80;
                           2058 ;	genAssign
   058F 90 00 24           2059 	mov	dptr,#_ddramdump_temp_val_1_1
   0592 74 80              2060 	mov	a,#0x80
   0594 F0                 2061 	movx	@dptr,a
   0595                    2062 00102$:
                           2063 ;	main.c:274: if(count_row==1)
                           2064 ;	genCmpEq
                           2065 ;	gencjneshort
                           2066 ;	Peephole 112.b	changed ljmp to sjmp
                           2067 ;	Peephole 198.a	optimized misc jump sequence
   0595 BB 01 09           2068 	cjne	r3,#0x01,00104$
   0598 BC 00 06           2069 	cjne	r4,#0x00,00104$
                           2070 ;	Peephole 200.b	removed redundant sjmp
                           2071 ;	Peephole 300	removed redundant label 00130$
                           2072 ;	Peephole 300	removed redundant label 00131$
                           2073 ;	main.c:275: temp_val=0xC0;
                           2074 ;	genAssign
   059B 90 00 24           2075 	mov	dptr,#_ddramdump_temp_val_1_1
   059E 74 C0              2076 	mov	a,#0xC0
   05A0 F0                 2077 	movx	@dptr,a
   05A1                    2078 00104$:
                           2079 ;	main.c:276: if(count_row==2)
                           2080 ;	genCmpEq
                           2081 ;	gencjneshort
                           2082 ;	Peephole 112.b	changed ljmp to sjmp
                           2083 ;	Peephole 198.a	optimized misc jump sequence
   05A1 BB 02 09           2084 	cjne	r3,#0x02,00106$
   05A4 BC 00 06           2085 	cjne	r4,#0x00,00106$
                           2086 ;	Peephole 200.b	removed redundant sjmp
                           2087 ;	Peephole 300	removed redundant label 00132$
                           2088 ;	Peephole 300	removed redundant label 00133$
                           2089 ;	main.c:277: temp_val=0x90;
                           2090 ;	genAssign
   05A7 90 00 24           2091 	mov	dptr,#_ddramdump_temp_val_1_1
   05AA 74 90              2092 	mov	a,#0x90
   05AC F0                 2093 	movx	@dptr,a
   05AD                    2094 00106$:
                           2095 ;	main.c:278: if(count_row==3)
                           2096 ;	genCmpEq
                           2097 ;	gencjneshort
                           2098 ;	Peephole 112.b	changed ljmp to sjmp
                           2099 ;	Peephole 198.a	optimized misc jump sequence
   05AD BB 03 09           2100 	cjne	r3,#0x03,00108$
   05B0 BC 00 06           2101 	cjne	r4,#0x00,00108$
                           2102 ;	Peephole 200.b	removed redundant sjmp
                           2103 ;	Peephole 300	removed redundant label 00134$
                           2104 ;	Peephole 300	removed redundant label 00135$
                           2105 ;	main.c:279: temp_val=0xD0;
                           2106 ;	genAssign
   05B3 90 00 24           2107 	mov	dptr,#_ddramdump_temp_val_1_1
   05B6 74 D0              2108 	mov	a,#0xD0
   05B8 F0                 2109 	movx	@dptr,a
   05B9                    2110 00108$:
                           2111 ;	main.c:280: printf("0x%02x:  ", (temp_val&0x7F));
                           2112 ;	genAssign
   05B9 90 00 24           2113 	mov	dptr,#_ddramdump_temp_val_1_1
   05BC E0                 2114 	movx	a,@dptr
   05BD FD                 2115 	mov	r5,a
                           2116 ;	genAnd
   05BE 74 7F              2117 	mov	a,#0x7F
   05C0 5D                 2118 	anl	a,r5
   05C1 FE                 2119 	mov	r6,a
                           2120 ;	genCast
   05C2 7F 00              2121 	mov	r7,#0x00
                           2122 ;	genIpush
   05C4 C0 02              2123 	push	ar2
   05C6 C0 03              2124 	push	ar3
   05C8 C0 04              2125 	push	ar4
   05CA C0 05              2126 	push	ar5
   05CC C0 06              2127 	push	ar6
   05CE C0 07              2128 	push	ar7
                           2129 ;	genIpush
   05D0 74 89              2130 	mov	a,#__str_18
   05D2 C0 E0              2131 	push	acc
   05D4 74 16              2132 	mov	a,#(__str_18 >> 8)
   05D6 C0 E0              2133 	push	acc
   05D8 74 80              2134 	mov	a,#0x80
   05DA C0 E0              2135 	push	acc
                           2136 ;	genCall
   05DC 12 0B 4D           2137 	lcall	_printf
   05DF E5 81              2138 	mov	a,sp
   05E1 24 FB              2139 	add	a,#0xfb
   05E3 F5 81              2140 	mov	sp,a
   05E5 D0 05              2141 	pop	ar5
   05E7 D0 04              2142 	pop	ar4
   05E9 D0 03              2143 	pop	ar3
   05EB D0 02              2144 	pop	ar2
                           2145 ;	main.c:281: for(count=0; count<=15; count++)
                           2146 ;	genAssign
                           2147 ;	genAssign
   05ED 7E 10              2148 	mov	r6,#0x10
   05EF 7F 00              2149 	mov	r7,#0x00
   05F1                    2150 00111$:
                           2151 ;	main.c:283: *INST_WRITE = temp_val;
                           2152 ;	genAssign
                           2153 ;	Peephole 182.b	used 16 bit load of dptr
   05F1 90 F0 00           2154 	mov	dptr,#0xF000
                           2155 ;	genPointerSet
                           2156 ;     genFarPointerSet
   05F4 ED                 2157 	mov	a,r5
   05F5 F0                 2158 	movx	@dptr,a
                           2159 ;	main.c:284: lcdbusywait();
                           2160 ;	genCall
   05F6 C0 02              2161 	push	ar2
   05F8 C0 03              2162 	push	ar3
   05FA C0 04              2163 	push	ar4
   05FC C0 05              2164 	push	ar5
   05FE C0 06              2165 	push	ar6
   0600 C0 07              2166 	push	ar7
   0602 12 00 97           2167 	lcall	_lcdbusywait
   0605 D0 07              2168 	pop	ar7
   0607 D0 06              2169 	pop	ar6
   0609 D0 05              2170 	pop	ar5
   060B D0 04              2171 	pop	ar4
   060D D0 03              2172 	pop	ar3
   060F D0 02              2173 	pop	ar2
                           2174 ;	main.c:285: temp_val++;
                           2175 ;	genPlus
                           2176 ;     genPlusIncr
   0611 0D                 2177 	inc	r5
                           2178 ;	main.c:286: printf("%02x  ",*DATA_READ);
                           2179 ;	genPointerGet
                           2180 ;	genFarPointerGet
                           2181 ;	Peephole 182.b	used 16 bit load of dptr
   0612 90 FC 00           2182 	mov	dptr,#0xFC00
   0615 E0                 2183 	movx	a,@dptr
   0616 F8                 2184 	mov	r0,a
                           2185 ;	genCast
   0617 79 00              2186 	mov	r1,#0x00
                           2187 ;	genIpush
   0619 C0 02              2188 	push	ar2
   061B C0 03              2189 	push	ar3
   061D C0 04              2190 	push	ar4
   061F C0 05              2191 	push	ar5
   0621 C0 06              2192 	push	ar6
   0623 C0 07              2193 	push	ar7
   0625 C0 00              2194 	push	ar0
   0627 C0 01              2195 	push	ar1
                           2196 ;	genIpush
   0629 74 27              2197 	mov	a,#__str_15
   062B C0 E0              2198 	push	acc
   062D 74 16              2199 	mov	a,#(__str_15 >> 8)
   062F C0 E0              2200 	push	acc
   0631 74 80              2201 	mov	a,#0x80
   0633 C0 E0              2202 	push	acc
                           2203 ;	genCall
   0635 12 0B 4D           2204 	lcall	_printf
   0638 E5 81              2205 	mov	a,sp
   063A 24 FB              2206 	add	a,#0xfb
   063C F5 81              2207 	mov	sp,a
   063E D0 07              2208 	pop	ar7
   0640 D0 06              2209 	pop	ar6
   0642 D0 05              2210 	pop	ar5
   0644 D0 04              2211 	pop	ar4
   0646 D0 03              2212 	pop	ar3
   0648 D0 02              2213 	pop	ar2
                           2214 ;	main.c:287: lcdbusywait();
                           2215 ;	genCall
   064A C0 02              2216 	push	ar2
   064C C0 03              2217 	push	ar3
   064E C0 04              2218 	push	ar4
   0650 C0 05              2219 	push	ar5
   0652 C0 06              2220 	push	ar6
   0654 C0 07              2221 	push	ar7
   0656 12 00 97           2222 	lcall	_lcdbusywait
   0659 D0 07              2223 	pop	ar7
   065B D0 06              2224 	pop	ar6
   065D D0 05              2225 	pop	ar5
   065F D0 04              2226 	pop	ar4
   0661 D0 03              2227 	pop	ar3
   0663 D0 02              2228 	pop	ar2
                           2229 ;	genMinus
                           2230 ;	genMinusDec
   0665 1E                 2231 	dec	r6
   0666 BE FF 01           2232 	cjne	r6,#0xff,00136$
   0669 1F                 2233 	dec	r7
   066A                    2234 00136$:
                           2235 ;	main.c:281: for(count=0; count<=15; count++)
                           2236 ;	genIfx
   066A EE                 2237 	mov	a,r6
   066B 4F                 2238 	orl	a,r7
                           2239 ;	genIfxJump
   066C 60 03              2240 	jz	00137$
   066E 02 05 F1           2241 	ljmp	00111$
   0671                    2242 00137$:
                           2243 ;	main.c:289: putstr("\n\r");
                           2244 ;	genAssign
   0671 90 00 24           2245 	mov	dptr,#_ddramdump_temp_val_1_1
   0674 ED                 2246 	mov	a,r5
   0675 F0                 2247 	movx	@dptr,a
                           2248 ;	genCall
                           2249 ;	Peephole 182.a	used 16 bit load of DPTR
   0676 90 15 F7           2250 	mov	dptr,#__str_11
   0679 75 F0 80           2251 	mov	b,#0x80
   067C C0 02              2252 	push	ar2
   067E C0 03              2253 	push	ar3
   0680 C0 04              2254 	push	ar4
   0682 12 01 EF           2255 	lcall	_putstr
   0685 D0 04              2256 	pop	ar4
   0687 D0 03              2257 	pop	ar3
   0689 D0 02              2258 	pop	ar2
                           2259 ;	main.c:270: for (count_row=0; count_row<=3; count_row++)
                           2260 ;	genPlus
                           2261 ;     genPlusIncr
   068B 0B                 2262 	inc	r3
   068C BB 00 01           2263 	cjne	r3,#0x00,00138$
   068F 0C                 2264 	inc	r4
   0690                    2265 00138$:
   0690 02 05 80           2266 	ljmp	00112$
   0693                    2267 00115$:
                           2268 ;	main.c:291: putstr("\n\r");
                           2269 ;	genCall
                           2270 ;	Peephole 182.a	used 16 bit load of DPTR
   0693 90 15 F7           2271 	mov	dptr,#__str_11
   0696 75 F0 80           2272 	mov	b,#0x80
   0699 C0 02              2273 	push	ar2
   069B 12 01 EF           2274 	lcall	_putstr
   069E D0 02              2275 	pop	ar2
                           2276 ;	main.c:292: *INST_WRITE=(k|0x80);
                           2277 ;	genAssign
                           2278 ;	Peephole 182.b	used 16 bit load of dptr
   06A0 90 F0 00           2279 	mov	dptr,#0xF000
                           2280 ;	genOr
   06A3 43 02 80           2281 	orl	ar2,#0x80
                           2282 ;	genPointerSet
                           2283 ;     genFarPointerSet
   06A6 EA                 2284 	mov	a,r2
   06A7 F0                 2285 	movx	@dptr,a
                           2286 ;	Peephole 300	removed redundant label 00116$
   06A8 22                 2287 	ret
                           2288 ;------------------------------------------------------------
                           2289 ;Allocation info for local variables in function 'ReadVariable'
                           2290 ;------------------------------------------------------------
                           2291 ;l                         Allocated with name '_ReadVariable_l_1_1'
                           2292 ;m                         Allocated with name '_ReadVariable_m_1_1'
                           2293 ;------------------------------------------------------------
                           2294 ;	main.c:296: unsigned char ReadVariable()
                           2295 ;	-----------------------------------------
                           2296 ;	 function ReadVariable
                           2297 ;	-----------------------------------------
   06A9                    2298 _ReadVariable:
                           2299 ;	main.c:298: unsigned char l=0, m=0;
                           2300 ;	genAssign
   06A9 90 00 25           2301 	mov	dptr,#_ReadVariable_m_1_1
                           2302 ;	Peephole 181	changed mov to clr
   06AC E4                 2303 	clr	a
   06AD F0                 2304 	movx	@dptr,a
                           2305 ;	main.c:299: do
   06AE                    2306 00110$:
                           2307 ;	main.c:301: l=getchar();
                           2308 ;	genCall
   06AE 12 03 15           2309 	lcall	_getchar
   06B1 AA 82              2310 	mov	r2,dpl
                           2311 ;	main.c:302: if(l>='0' && l<='8')
                           2312 ;	genAssign
   06B3 8A 03              2313 	mov	ar3,r2
                           2314 ;	genCmpLt
                           2315 ;	genCmp
   06B5 BB 30 00           2316 	cjne	r3,#0x30,00121$
   06B8                    2317 00121$:
                           2318 ;	genIfxJump
                           2319 ;	Peephole 112.b	changed ljmp to sjmp
                           2320 ;	Peephole 160.a	removed sjmp by inverse jump logic
   06B8 40 24              2321 	jc	00107$
                           2322 ;	Peephole 300	removed redundant label 00122$
                           2323 ;	genAssign
   06BA 8A 03              2324 	mov	ar3,r2
                           2325 ;	genCmpGt
                           2326 ;	genCmp
                           2327 ;	genIfxJump
                           2328 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   06BC EB                 2329 	mov	a,r3
   06BD 24 C7              2330 	add	a,#0xff - 0x38
                           2331 ;	Peephole 112.b	changed ljmp to sjmp
                           2332 ;	Peephole 160.a	removed sjmp by inverse jump logic
   06BF 40 1D              2333 	jc	00107$
                           2334 ;	Peephole 300	removed redundant label 00123$
                           2335 ;	main.c:304: m=((m*10)+(l-'0'));
                           2336 ;	genAssign
   06C1 90 00 25           2337 	mov	dptr,#_ReadVariable_m_1_1
   06C4 E0                 2338 	movx	a,@dptr
                           2339 ;	genMult
                           2340 ;	genMultOneByte
   06C5 FB                 2341 	mov	r3,a
                           2342 ;	Peephole 105	removed redundant mov
   06C6 75 F0 0A           2343 	mov	b,#0x0A
   06C9 A4                 2344 	mul	ab
   06CA FB                 2345 	mov	r3,a
                           2346 ;	genMinus
   06CB EA                 2347 	mov	a,r2
   06CC 24 D0              2348 	add	a,#0xd0
                           2349 ;	genPlus
   06CE 90 00 25           2350 	mov	dptr,#_ReadVariable_m_1_1
                           2351 ;	Peephole 236.a	used r3 instead of ar3
   06D1 2B                 2352 	add	a,r3
   06D2 F0                 2353 	movx	@dptr,a
                           2354 ;	main.c:305: putchar(l);
                           2355 ;	genCall
   06D3 8A 82              2356 	mov	dpl,r2
   06D5 C0 02              2357 	push	ar2
   06D7 12 03 03           2358 	lcall	_putchar
   06DA D0 02              2359 	pop	ar2
                           2360 ;	Peephole 112.b	changed ljmp to sjmp
   06DC 80 25              2361 	sjmp	00111$
   06DE                    2362 00107$:
                           2363 ;	main.c:308: else if(l==127)
                           2364 ;	genCmpEq
                           2365 ;	gencjneshort
                           2366 ;	Peephole 112.b	changed ljmp to sjmp
                           2367 ;	Peephole 198.b	optimized misc jump sequence
   06DE BA 7F 10           2368 	cjne	r2,#0x7F,00104$
                           2369 ;	Peephole 200.b	removed redundant sjmp
                           2370 ;	Peephole 300	removed redundant label 00124$
                           2371 ;	Peephole 300	removed redundant label 00125$
                           2372 ;	main.c:310: m=m/10;
                           2373 ;	genAssign
   06E1 90 00 25           2374 	mov	dptr,#_ReadVariable_m_1_1
   06E4 E0                 2375 	movx	a,@dptr
   06E5 FB                 2376 	mov	r3,a
                           2377 ;	genDiv
   06E6 90 00 25           2378 	mov	dptr,#_ReadVariable_m_1_1
                           2379 ;     genDivOneByte
   06E9 75 F0 0A           2380 	mov	b,#0x0A
   06EC EB                 2381 	mov	a,r3
   06ED 84                 2382 	div	ab
   06EE F0                 2383 	movx	@dptr,a
                           2384 ;	Peephole 112.b	changed ljmp to sjmp
   06EF 80 12              2385 	sjmp	00111$
   06F1                    2386 00104$:
                           2387 ;	main.c:312: else if(l!=13)
                           2388 ;	genCmpEq
                           2389 ;	gencjneshort
   06F1 BA 0D 02           2390 	cjne	r2,#0x0D,00126$
                           2391 ;	Peephole 112.b	changed ljmp to sjmp
   06F4 80 0D              2392 	sjmp	00111$
   06F6                    2393 00126$:
                           2394 ;	main.c:313: {putstr("\n\rEnter Numbers\n\r");}
                           2395 ;	genCall
                           2396 ;	Peephole 182.a	used 16 bit load of DPTR
   06F6 90 16 93           2397 	mov	dptr,#__str_19
   06F9 75 F0 80           2398 	mov	b,#0x80
   06FC C0 02              2399 	push	ar2
   06FE 12 01 EF           2400 	lcall	_putstr
   0701 D0 02              2401 	pop	ar2
   0703                    2402 00111$:
                           2403 ;	main.c:314: }while(l!=13);
                           2404 ;	genCmpEq
                           2405 ;	gencjneshort
                           2406 ;	Peephole 112.b	changed ljmp to sjmp
                           2407 ;	Peephole 198.b	optimized misc jump sequence
   0703 BA 0D A8           2408 	cjne	r2,#0x0D,00110$
                           2409 ;	Peephole 200.b	removed redundant sjmp
                           2410 ;	Peephole 300	removed redundant label 00127$
                           2411 ;	Peephole 300	removed redundant label 00128$
                           2412 ;	main.c:315: return m;
                           2413 ;	genAssign
   0706 90 00 25           2414 	mov	dptr,#_ReadVariable_m_1_1
   0709 E0                 2415 	movx	a,@dptr
                           2416 ;	genRet
                           2417 ;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
   070A F5 82              2418 	mov	dpl,a
                           2419 ;	Peephole 300	removed redundant label 00113$
   070C 22                 2420 	ret
                           2421 ;------------------------------------------------------------
                           2422 ;Allocation info for local variables in function 'readinput'
                           2423 ;------------------------------------------------------------
                           2424 ;value                     Allocated with name '_readinput_value_1_1'
                           2425 ;i                         Allocated with name '_readinput_i_1_1'
                           2426 ;j                         Allocated with name '_readinput_j_1_1'
                           2427 ;------------------------------------------------------------
                           2428 ;	main.c:319: unsigned char readinput()
                           2429 ;	-----------------------------------------
                           2430 ;	 function readinput
                           2431 ;	-----------------------------------------
   070D                    2432 _readinput:
                           2433 ;	main.c:321: unsigned char value=0, i, j=0;
                           2434 ;	genAssign
   070D 90 00 26           2435 	mov	dptr,#_readinput_value_1_1
                           2436 ;	Peephole 181	changed mov to clr
   0710 E4                 2437 	clr	a
   0711 F0                 2438 	movx	@dptr,a
                           2439 ;	main.c:322: do
                           2440 ;	genAssign
   0712 7A 00              2441 	mov	r2,#0x00
   0714                    2442 00110$:
                           2443 ;	main.c:324: i=getchar();
                           2444 ;	genCall
   0714 C0 02              2445 	push	ar2
   0716 12 03 15           2446 	lcall	_getchar
   0719 AB 82              2447 	mov	r3,dpl
   071B D0 02              2448 	pop	ar2
                           2449 ;	main.c:325: if(i>='0'&&i<='1')          /* works only if its a number */
                           2450 ;	genAssign
   071D 8B 04              2451 	mov	ar4,r3
                           2452 ;	genCmpLt
                           2453 ;	genCmp
   071F BC 30 00           2454 	cjne	r4,#0x30,00122$
   0722                    2455 00122$:
                           2456 ;	genIfxJump
                           2457 ;	Peephole 112.b	changed ljmp to sjmp
                           2458 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0722 40 26              2459 	jc	00107$
                           2460 ;	Peephole 300	removed redundant label 00123$
                           2461 ;	genAssign
   0724 8B 04              2462 	mov	ar4,r3
                           2463 ;	genCmpGt
                           2464 ;	genCmp
                           2465 ;	genIfxJump
                           2466 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   0726 EC                 2467 	mov	a,r4
   0727 24 CE              2468 	add	a,#0xff - 0x31
                           2469 ;	Peephole 112.b	changed ljmp to sjmp
                           2470 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0729 40 1F              2471 	jc	00107$
                           2472 ;	Peephole 300	removed redundant label 00124$
                           2473 ;	main.c:327: value=((value*2)+(i-'0')); /* Convert character to integers */
                           2474 ;	genAssign
   072B 90 00 26           2475 	mov	dptr,#_readinput_value_1_1
   072E E0                 2476 	movx	a,@dptr
                           2477 ;	genLeftShift
                           2478 ;	genLeftShiftLiteral
                           2479 ;	genlshOne
                           2480 ;	Peephole 105	removed redundant mov
                           2481 ;	Peephole 204	removed redundant mov
   072F 25 E0              2482 	add	a,acc
   0731 FC                 2483 	mov	r4,a
                           2484 ;	genMinus
   0732 EB                 2485 	mov	a,r3
   0733 24 D0              2486 	add	a,#0xd0
                           2487 ;	genPlus
   0735 90 00 26           2488 	mov	dptr,#_readinput_value_1_1
                           2489 ;	Peephole 236.a	used r4 instead of ar4
   0738 2C                 2490 	add	a,r4
   0739 F0                 2491 	movx	@dptr,a
                           2492 ;	main.c:328: putchar(i);
                           2493 ;	genCall
   073A 8B 82              2494 	mov	dpl,r3
   073C C0 02              2495 	push	ar2
   073E C0 03              2496 	push	ar3
   0740 12 03 03           2497 	lcall	_putchar
   0743 D0 03              2498 	pop	ar3
   0745 D0 02              2499 	pop	ar2
                           2500 ;	main.c:329: j++;
                           2501 ;	genPlus
                           2502 ;     genPlusIncr
   0747 0A                 2503 	inc	r2
                           2504 ;	Peephole 112.b	changed ljmp to sjmp
   0748 80 27              2505 	sjmp	00111$
   074A                    2506 00107$:
                           2507 ;	main.c:331: else if(i==127)
                           2508 ;	genCmpEq
                           2509 ;	gencjneshort
                           2510 ;	Peephole 112.b	changed ljmp to sjmp
                           2511 ;	Peephole 198.b	optimized misc jump sequence
   074A BB 7F 0E           2512 	cjne	r3,#0x7F,00104$
                           2513 ;	Peephole 200.b	removed redundant sjmp
                           2514 ;	Peephole 300	removed redundant label 00125$
                           2515 ;	Peephole 300	removed redundant label 00126$
                           2516 ;	main.c:333: value=value/2;
                           2517 ;	genAssign
   074D 90 00 26           2518 	mov	dptr,#_readinput_value_1_1
   0750 E0                 2519 	movx	a,@dptr
                           2520 ;	genRightShift
                           2521 ;	genRightShiftLiteral
                           2522 ;	genrshOne
   0751 FC                 2523 	mov	r4,a
                           2524 ;	Peephole 105	removed redundant mov
   0752 C3                 2525 	clr	c
   0753 13                 2526 	rrc	a
                           2527 ;	genAssign
   0754 FC                 2528 	mov	r4,a
   0755 90 00 26           2529 	mov	dptr,#_readinput_value_1_1
                           2530 ;	Peephole 100	removed redundant mov
   0758 F0                 2531 	movx	@dptr,a
                           2532 ;	Peephole 112.b	changed ljmp to sjmp
   0759 80 16              2533 	sjmp	00111$
   075B                    2534 00104$:
                           2535 ;	main.c:335: else if(i!=13)
                           2536 ;	genCmpEq
                           2537 ;	gencjneshort
   075B BB 0D 02           2538 	cjne	r3,#0x0D,00127$
                           2539 ;	Peephole 112.b	changed ljmp to sjmp
   075E 80 11              2540 	sjmp	00111$
   0760                    2541 00127$:
                           2542 ;	main.c:336: {putstr("\n\rEnter Valid Numbers\n\r");}
                           2543 ;	genCall
                           2544 ;	Peephole 182.a	used 16 bit load of DPTR
   0760 90 16 A5           2545 	mov	dptr,#__str_20
   0763 75 F0 80           2546 	mov	b,#0x80
   0766 C0 02              2547 	push	ar2
   0768 C0 03              2548 	push	ar3
   076A 12 01 EF           2549 	lcall	_putstr
   076D D0 03              2550 	pop	ar3
   076F D0 02              2551 	pop	ar2
   0771                    2552 00111$:
                           2553 ;	main.c:337: }while(i!=13);
                           2554 ;	genCmpEq
                           2555 ;	gencjneshort
                           2556 ;	Peephole 112.b	changed ljmp to sjmp
                           2557 ;	Peephole 198.b	optimized misc jump sequence
   0771 BB 0D A0           2558 	cjne	r3,#0x0D,00110$
                           2559 ;	Peephole 200.b	removed redundant sjmp
                           2560 ;	Peephole 300	removed redundant label 00128$
                           2561 ;	Peephole 300	removed redundant label 00129$
                           2562 ;	main.c:339: return value;
                           2563 ;	genAssign
   0774 90 00 26           2564 	mov	dptr,#_readinput_value_1_1
   0777 E0                 2565 	movx	a,@dptr
                           2566 ;	genRet
                           2567 ;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
   0778 F5 82              2568 	mov	dpl,a
                           2569 ;	Peephole 300	removed redundant label 00113$
   077A 22                 2570 	ret
                           2571 ;------------------------------------------------------------
                           2572 ;Allocation info for local variables in function 'addcharacter'
                           2573 ;------------------------------------------------------------
                           2574 ;temp                      Allocated with name '_addcharacter_temp_1_1'
                           2575 ;y                         Allocated with name '_addcharacter_y_1_1'
                           2576 ;------------------------------------------------------------
                           2577 ;	main.c:342: void addcharacter()
                           2578 ;	-----------------------------------------
                           2579 ;	 function addcharacter
                           2580 ;	-----------------------------------------
   077B                    2581 _addcharacter:
                           2582 ;	main.c:345: if(u<8)
                           2583 ;	genAssign
   077B 90 00 0B           2584 	mov	dptr,#_u
   077E E0                 2585 	movx	a,@dptr
   077F FA                 2586 	mov	r2,a
                           2587 ;	genCmpLt
                           2588 ;	genCmp
   0780 BA 08 00           2589 	cjne	r2,#0x08,00148$
   0783                    2590 00148$:
                           2591 ;	genIfxJump
   0783 40 03              2592 	jc	00149$
   0785 02 08 E7           2593 	ljmp	00128$
   0788                    2594 00149$:
                           2595 ;	main.c:347: putstr("\n\rEnter eight pixel values\n\r");
                           2596 ;	genCall
                           2597 ;	Peephole 182.a	used 16 bit load of DPTR
   0788 90 16 BD           2598 	mov	dptr,#__str_21
   078B 75 F0 80           2599 	mov	b,#0x80
   078E 12 01 EF           2600 	lcall	_putstr
                           2601 ;	main.c:348: putstr("Enter five binary values for every pixel\n\r");
                           2602 ;	genCall
                           2603 ;	Peephole 182.a	used 16 bit load of DPTR
   0791 90 16 DA           2604 	mov	dptr,#__str_22
   0794 75 F0 80           2605 	mov	b,#0x80
   0797 12 01 EF           2606 	lcall	_putstr
                           2607 ;	main.c:351: for(temp=0; temp<=7; temp++)
                           2608 ;	genAssign
   079A 90 00 27           2609 	mov	dptr,#_addcharacter_temp_1_1
                           2610 ;	Peephole 181	changed mov to clr
   079D E4                 2611 	clr	a
   079E F0                 2612 	movx	@dptr,a
   079F                    2613 00130$:
                           2614 ;	genAssign
   079F 90 00 27           2615 	mov	dptr,#_addcharacter_temp_1_1
   07A2 E0                 2616 	movx	a,@dptr
                           2617 ;	genCmpGt
                           2618 ;	genCmp
                           2619 ;	genIfxJump
                           2620 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   07A3 FA                 2621 	mov  r2,a
                           2622 ;	Peephole 177.a	removed redundant mov
   07A4 24 F8              2623 	add	a,#0xff - 0x07
                           2624 ;	Peephole 112.b	changed ljmp to sjmp
                           2625 ;	Peephole 160.a	removed sjmp by inverse jump logic
   07A6 40 46              2626 	jc	00133$
                           2627 ;	Peephole 300	removed redundant label 00150$
                           2628 ;	main.c:353: y=readinput();
                           2629 ;	genCall
   07A8 C0 02              2630 	push	ar2
   07AA 12 07 0D           2631 	lcall	_readinput
   07AD AB 82              2632 	mov	r3,dpl
   07AF D0 02              2633 	pop	ar2
                           2634 ;	main.c:354: if(y>0x1F)
                           2635 ;	genCmpGt
                           2636 ;	genCmp
                           2637 ;	genIfxJump
                           2638 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           2639 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   07B1 EB                 2640 	mov	a,r3
   07B2 24 E0              2641 	add	a,#0xff - 0x1F
   07B4 50 15              2642 	jnc	00102$
                           2643 ;	Peephole 300	removed redundant label 00151$
                           2644 ;	main.c:356: putstr("Invalid Number. Enter Again\n\r");
                           2645 ;	genCall
                           2646 ;	Peephole 182.a	used 16 bit load of DPTR
   07B6 90 17 05           2647 	mov	dptr,#__str_23
   07B9 75 F0 80           2648 	mov	b,#0x80
   07BC C0 02              2649 	push	ar2
   07BE 12 01 EF           2650 	lcall	_putstr
   07C1 D0 02              2651 	pop	ar2
                           2652 ;	main.c:357: temp--;
                           2653 ;	genMinus
                           2654 ;	genMinusDec
   07C3 EA                 2655 	mov	a,r2
   07C4 14                 2656 	dec	a
                           2657 ;	genAssign
   07C5 90 00 27           2658 	mov	dptr,#_addcharacter_temp_1_1
   07C8 F0                 2659 	movx	@dptr,a
                           2660 ;	Peephole 112.b	changed ljmp to sjmp
   07C9 80 15              2661 	sjmp	00132$
   07CB                    2662 00102$:
                           2663 ;	main.c:361: array[temp]=y;
                           2664 ;	genPlus
                           2665 ;	Peephole 236.g	used r2 instead of ar2
   07CB EA                 2666 	mov	a,r2
   07CC 24 03              2667 	add	a,#_array
   07CE F5 82              2668 	mov	dpl,a
                           2669 ;	Peephole 181	changed mov to clr
   07D0 E4                 2670 	clr	a
   07D1 34 00              2671 	addc	a,#(_array >> 8)
   07D3 F5 83              2672 	mov	dph,a
                           2673 ;	genPointerSet
                           2674 ;     genFarPointerSet
   07D5 EB                 2675 	mov	a,r3
   07D6 F0                 2676 	movx	@dptr,a
                           2677 ;	main.c:362: putstr("\n\r");
                           2678 ;	genCall
                           2679 ;	Peephole 182.a	used 16 bit load of DPTR
   07D7 90 15 F7           2680 	mov	dptr,#__str_11
   07DA 75 F0 80           2681 	mov	b,#0x80
   07DD 12 01 EF           2682 	lcall	_putstr
   07E0                    2683 00132$:
                           2684 ;	main.c:351: for(temp=0; temp<=7; temp++)
                           2685 ;	genAssign
   07E0 90 00 27           2686 	mov	dptr,#_addcharacter_temp_1_1
   07E3 E0                 2687 	movx	a,@dptr
   07E4 FA                 2688 	mov	r2,a
                           2689 ;	genPlus
   07E5 90 00 27           2690 	mov	dptr,#_addcharacter_temp_1_1
                           2691 ;     genPlusIncr
   07E8 74 01              2692 	mov	a,#0x01
                           2693 ;	Peephole 236.a	used r2 instead of ar2
   07EA 2A                 2694 	add	a,r2
   07EB F0                 2695 	movx	@dptr,a
                           2696 ;	Peephole 112.b	changed ljmp to sjmp
   07EC 80 B1              2697 	sjmp	00130$
   07EE                    2698 00133$:
                           2699 ;	main.c:365: if(u==0)
                           2700 ;	genAssign
   07EE 90 00 0B           2701 	mov	dptr,#_u
   07F1 E0                 2702 	movx	a,@dptr
                           2703 ;	genIfx
   07F2 FA                 2704 	mov	r2,a
                           2705 ;	Peephole 105	removed redundant mov
                           2706 ;	genIfxJump
                           2707 ;	Peephole 108.b	removed ljmp by inverse jump logic
   07F3 70 17              2708 	jnz	00125$
                           2709 ;	Peephole 300	removed redundant label 00152$
                           2710 ;	main.c:366: lcdcreatechar(0x00, array);
                           2711 ;	genCast
   07F5 90 00 20           2712 	mov	dptr,#_lcdcreatechar_PARM_2
   07F8 74 03              2713 	mov	a,#_array
   07FA F0                 2714 	movx	@dptr,a
   07FB A3                 2715 	inc	dptr
   07FC 74 00              2716 	mov	a,#(_array >> 8)
   07FE F0                 2717 	movx	@dptr,a
   07FF A3                 2718 	inc	dptr
   0800 74 00              2719 	mov	a,#0x0
   0802 F0                 2720 	movx	@dptr,a
                           2721 ;	genCall
   0803 75 82 00           2722 	mov	dpl,#0x00
   0806 12 04 A4           2723 	lcall	_lcdcreatechar
   0809 02 08 DF           2724 	ljmp	00126$
   080C                    2725 00125$:
                           2726 ;	main.c:367: else if(u==1)
                           2727 ;	genAssign
   080C 90 00 0B           2728 	mov	dptr,#_u
   080F E0                 2729 	movx	a,@dptr
   0810 FA                 2730 	mov	r2,a
                           2731 ;	genCmpEq
                           2732 ;	gencjneshort
                           2733 ;	Peephole 112.b	changed ljmp to sjmp
                           2734 ;	Peephole 198.b	optimized misc jump sequence
   0811 BA 01 17           2735 	cjne	r2,#0x01,00122$
                           2736 ;	Peephole 200.b	removed redundant sjmp
                           2737 ;	Peephole 300	removed redundant label 00153$
                           2738 ;	Peephole 300	removed redundant label 00154$
                           2739 ;	main.c:368: lcdcreatechar(0x08, array);
                           2740 ;	genCast
   0814 90 00 20           2741 	mov	dptr,#_lcdcreatechar_PARM_2
   0817 74 03              2742 	mov	a,#_array
   0819 F0                 2743 	movx	@dptr,a
   081A A3                 2744 	inc	dptr
   081B 74 00              2745 	mov	a,#(_array >> 8)
   081D F0                 2746 	movx	@dptr,a
   081E A3                 2747 	inc	dptr
   081F 74 00              2748 	mov	a,#0x0
   0821 F0                 2749 	movx	@dptr,a
                           2750 ;	genCall
   0822 75 82 08           2751 	mov	dpl,#0x08
   0825 12 04 A4           2752 	lcall	_lcdcreatechar
   0828 02 08 DF           2753 	ljmp	00126$
   082B                    2754 00122$:
                           2755 ;	main.c:369: else if(u==2)
                           2756 ;	genAssign
   082B 90 00 0B           2757 	mov	dptr,#_u
   082E E0                 2758 	movx	a,@dptr
   082F FA                 2759 	mov	r2,a
                           2760 ;	genCmpEq
                           2761 ;	gencjneshort
                           2762 ;	Peephole 112.b	changed ljmp to sjmp
                           2763 ;	Peephole 198.b	optimized misc jump sequence
   0830 BA 02 17           2764 	cjne	r2,#0x02,00119$
                           2765 ;	Peephole 200.b	removed redundant sjmp
                           2766 ;	Peephole 300	removed redundant label 00155$
                           2767 ;	Peephole 300	removed redundant label 00156$
                           2768 ;	main.c:370: lcdcreatechar(0x10, array);
                           2769 ;	genCast
   0833 90 00 20           2770 	mov	dptr,#_lcdcreatechar_PARM_2
   0836 74 03              2771 	mov	a,#_array
   0838 F0                 2772 	movx	@dptr,a
   0839 A3                 2773 	inc	dptr
   083A 74 00              2774 	mov	a,#(_array >> 8)
   083C F0                 2775 	movx	@dptr,a
   083D A3                 2776 	inc	dptr
   083E 74 00              2777 	mov	a,#0x0
   0840 F0                 2778 	movx	@dptr,a
                           2779 ;	genCall
   0841 75 82 10           2780 	mov	dpl,#0x10
   0844 12 04 A4           2781 	lcall	_lcdcreatechar
   0847 02 08 DF           2782 	ljmp	00126$
   084A                    2783 00119$:
                           2784 ;	main.c:371: else if(u==3)
                           2785 ;	genAssign
   084A 90 00 0B           2786 	mov	dptr,#_u
   084D E0                 2787 	movx	a,@dptr
   084E FA                 2788 	mov	r2,a
                           2789 ;	genCmpEq
                           2790 ;	gencjneshort
                           2791 ;	Peephole 112.b	changed ljmp to sjmp
                           2792 ;	Peephole 198.b	optimized misc jump sequence
   084F BA 03 17           2793 	cjne	r2,#0x03,00116$
                           2794 ;	Peephole 200.b	removed redundant sjmp
                           2795 ;	Peephole 300	removed redundant label 00157$
                           2796 ;	Peephole 300	removed redundant label 00158$
                           2797 ;	main.c:372: lcdcreatechar(0x18, array);
                           2798 ;	genCast
   0852 90 00 20           2799 	mov	dptr,#_lcdcreatechar_PARM_2
   0855 74 03              2800 	mov	a,#_array
   0857 F0                 2801 	movx	@dptr,a
   0858 A3                 2802 	inc	dptr
   0859 74 00              2803 	mov	a,#(_array >> 8)
   085B F0                 2804 	movx	@dptr,a
   085C A3                 2805 	inc	dptr
   085D 74 00              2806 	mov	a,#0x0
   085F F0                 2807 	movx	@dptr,a
                           2808 ;	genCall
   0860 75 82 18           2809 	mov	dpl,#0x18
   0863 12 04 A4           2810 	lcall	_lcdcreatechar
   0866 02 08 DF           2811 	ljmp	00126$
   0869                    2812 00116$:
                           2813 ;	main.c:373: else if(u==4)
                           2814 ;	genAssign
   0869 90 00 0B           2815 	mov	dptr,#_u
   086C E0                 2816 	movx	a,@dptr
   086D FA                 2817 	mov	r2,a
                           2818 ;	genCmpEq
                           2819 ;	gencjneshort
                           2820 ;	Peephole 112.b	changed ljmp to sjmp
                           2821 ;	Peephole 198.b	optimized misc jump sequence
   086E BA 04 16           2822 	cjne	r2,#0x04,00113$
                           2823 ;	Peephole 200.b	removed redundant sjmp
                           2824 ;	Peephole 300	removed redundant label 00159$
                           2825 ;	Peephole 300	removed redundant label 00160$
                           2826 ;	main.c:374: lcdcreatechar(0x20, array);
                           2827 ;	genCast
   0871 90 00 20           2828 	mov	dptr,#_lcdcreatechar_PARM_2
   0874 74 03              2829 	mov	a,#_array
   0876 F0                 2830 	movx	@dptr,a
   0877 A3                 2831 	inc	dptr
   0878 74 00              2832 	mov	a,#(_array >> 8)
   087A F0                 2833 	movx	@dptr,a
   087B A3                 2834 	inc	dptr
   087C 74 00              2835 	mov	a,#0x0
   087E F0                 2836 	movx	@dptr,a
                           2837 ;	genCall
   087F 75 82 20           2838 	mov	dpl,#0x20
   0882 12 04 A4           2839 	lcall	_lcdcreatechar
                           2840 ;	Peephole 112.b	changed ljmp to sjmp
   0885 80 58              2841 	sjmp	00126$
   0887                    2842 00113$:
                           2843 ;	main.c:375: else if(u==5)
                           2844 ;	genAssign
   0887 90 00 0B           2845 	mov	dptr,#_u
   088A E0                 2846 	movx	a,@dptr
   088B FA                 2847 	mov	r2,a
                           2848 ;	genCmpEq
                           2849 ;	gencjneshort
                           2850 ;	Peephole 112.b	changed ljmp to sjmp
                           2851 ;	Peephole 198.b	optimized misc jump sequence
   088C BA 05 16           2852 	cjne	r2,#0x05,00110$
                           2853 ;	Peephole 200.b	removed redundant sjmp
                           2854 ;	Peephole 300	removed redundant label 00161$
                           2855 ;	Peephole 300	removed redundant label 00162$
                           2856 ;	main.c:376: lcdcreatechar(0x28, array);
                           2857 ;	genCast
   088F 90 00 20           2858 	mov	dptr,#_lcdcreatechar_PARM_2
   0892 74 03              2859 	mov	a,#_array
   0894 F0                 2860 	movx	@dptr,a
   0895 A3                 2861 	inc	dptr
   0896 74 00              2862 	mov	a,#(_array >> 8)
   0898 F0                 2863 	movx	@dptr,a
   0899 A3                 2864 	inc	dptr
   089A 74 00              2865 	mov	a,#0x0
   089C F0                 2866 	movx	@dptr,a
                           2867 ;	genCall
   089D 75 82 28           2868 	mov	dpl,#0x28
   08A0 12 04 A4           2869 	lcall	_lcdcreatechar
                           2870 ;	Peephole 112.b	changed ljmp to sjmp
   08A3 80 3A              2871 	sjmp	00126$
   08A5                    2872 00110$:
                           2873 ;	main.c:377: else if(u==6)
                           2874 ;	genAssign
   08A5 90 00 0B           2875 	mov	dptr,#_u
   08A8 E0                 2876 	movx	a,@dptr
   08A9 FA                 2877 	mov	r2,a
                           2878 ;	genCmpEq
                           2879 ;	gencjneshort
                           2880 ;	Peephole 112.b	changed ljmp to sjmp
                           2881 ;	Peephole 198.b	optimized misc jump sequence
   08AA BA 06 16           2882 	cjne	r2,#0x06,00107$
                           2883 ;	Peephole 200.b	removed redundant sjmp
                           2884 ;	Peephole 300	removed redundant label 00163$
                           2885 ;	Peephole 300	removed redundant label 00164$
                           2886 ;	main.c:378: lcdcreatechar(0x30, array);
                           2887 ;	genCast
   08AD 90 00 20           2888 	mov	dptr,#_lcdcreatechar_PARM_2
   08B0 74 03              2889 	mov	a,#_array
   08B2 F0                 2890 	movx	@dptr,a
   08B3 A3                 2891 	inc	dptr
   08B4 74 00              2892 	mov	a,#(_array >> 8)
   08B6 F0                 2893 	movx	@dptr,a
   08B7 A3                 2894 	inc	dptr
   08B8 74 00              2895 	mov	a,#0x0
   08BA F0                 2896 	movx	@dptr,a
                           2897 ;	genCall
   08BB 75 82 30           2898 	mov	dpl,#0x30
   08BE 12 04 A4           2899 	lcall	_lcdcreatechar
                           2900 ;	Peephole 112.b	changed ljmp to sjmp
   08C1 80 1C              2901 	sjmp	00126$
   08C3                    2902 00107$:
                           2903 ;	main.c:379: else if(u==7)
                           2904 ;	genAssign
   08C3 90 00 0B           2905 	mov	dptr,#_u
   08C6 E0                 2906 	movx	a,@dptr
   08C7 FA                 2907 	mov	r2,a
                           2908 ;	genCmpEq
                           2909 ;	gencjneshort
                           2910 ;	Peephole 112.b	changed ljmp to sjmp
                           2911 ;	Peephole 198.b	optimized misc jump sequence
   08C8 BA 07 14           2912 	cjne	r2,#0x07,00126$
                           2913 ;	Peephole 200.b	removed redundant sjmp
                           2914 ;	Peephole 300	removed redundant label 00165$
                           2915 ;	Peephole 300	removed redundant label 00166$
                           2916 ;	main.c:380: lcdcreatechar(0x38, array);
                           2917 ;	genCast
   08CB 90 00 20           2918 	mov	dptr,#_lcdcreatechar_PARM_2
   08CE 74 03              2919 	mov	a,#_array
   08D0 F0                 2920 	movx	@dptr,a
   08D1 A3                 2921 	inc	dptr
   08D2 74 00              2922 	mov	a,#(_array >> 8)
   08D4 F0                 2923 	movx	@dptr,a
   08D5 A3                 2924 	inc	dptr
   08D6 74 00              2925 	mov	a,#0x0
   08D8 F0                 2926 	movx	@dptr,a
                           2927 ;	genCall
   08D9 75 82 38           2928 	mov	dpl,#0x38
   08DC 12 04 A4           2929 	lcall	_lcdcreatechar
   08DF                    2930 00126$:
                           2931 ;	main.c:381: u++;
                           2932 ;	genPlus
   08DF 90 00 0B           2933 	mov	dptr,#_u
   08E2 E0                 2934 	movx	a,@dptr
   08E3 24 01              2935 	add	a,#0x01
   08E5 F0                 2936 	movx	@dptr,a
                           2937 ;	Peephole 112.b	changed ljmp to sjmp
                           2938 ;	Peephole 251.b	replaced sjmp to ret with ret
   08E6 22                 2939 	ret
   08E7                    2940 00128$:
                           2941 ;	main.c:385: putstr("Eight custom character created. No more custom character please\n\r");
                           2942 ;	genCall
                           2943 ;	Peephole 182.a	used 16 bit load of DPTR
   08E7 90 17 23           2944 	mov	dptr,#__str_24
   08EA 75 F0 80           2945 	mov	b,#0x80
                           2946 ;	Peephole 253.b	replaced lcall/ret with ljmp
   08ED 02 01 EF           2947 	ljmp	_putstr
                           2948 ;
                           2949 ;------------------------------------------------------------
                           2950 ;Allocation info for local variables in function 'display'
                           2951 ;------------------------------------------------------------
                           2952 ;hey                       Allocated with name '_display_hey_1_1'
                           2953 ;------------------------------------------------------------
                           2954 ;	main.c:390: void display()
                           2955 ;	-----------------------------------------
                           2956 ;	 function display
                           2957 ;	-----------------------------------------
   08F0                    2958 _display:
                           2959 ;	main.c:393: putstr("Look at the LCD\n\r");
                           2960 ;	genCall
                           2961 ;	Peephole 182.a	used 16 bit load of DPTR
   08F0 90 17 65           2962 	mov	dptr,#__str_25
   08F3 75 F0 80           2963 	mov	b,#0x80
   08F6 12 01 EF           2964 	lcall	_putstr
                           2965 ;	main.c:394: while(hey<0x8)
                           2966 ;	genAssign
   08F9 7A 00              2967 	mov	r2,#0x00
   08FB                    2968 00101$:
                           2969 ;	genCmpLt
                           2970 ;	genCmp
   08FB BA 08 00           2971 	cjne	r2,#0x08,00109$
   08FE                    2972 00109$:
                           2973 ;	genIfxJump
                           2974 ;	Peephole 108.a	removed ljmp by inverse jump logic
   08FE 50 0C              2975 	jnc	00104$
                           2976 ;	Peephole 300	removed redundant label 00110$
                           2977 ;	main.c:396: lcdputch(hey);
                           2978 ;	genCall
   0900 8A 82              2979 	mov	dpl,r2
   0902 C0 02              2980 	push	ar2
   0904 12 01 55           2981 	lcall	_lcdputch
   0907 D0 02              2982 	pop	ar2
                           2983 ;	main.c:397: hey++;
                           2984 ;	genPlus
                           2985 ;     genPlusIncr
   0909 0A                 2986 	inc	r2
                           2987 ;	Peephole 112.b	changed ljmp to sjmp
   090A 80 EF              2988 	sjmp	00101$
   090C                    2989 00104$:
   090C 22                 2990 	ret
                           2991 ;------------------------------------------------------------
                           2992 ;Allocation info for local variables in function 'interactive'
                           2993 ;------------------------------------------------------------
                           2994 ;co                        Allocated with name '_interactive_co_1_1'
                           2995 ;i                         Allocated with name '_interactive_i_1_1'
                           2996 ;------------------------------------------------------------
                           2997 ;	main.c:401: void interactive()
                           2998 ;	-----------------------------------------
                           2999 ;	 function interactive
                           3000 ;	-----------------------------------------
   090D                    3001 _interactive:
                           3002 ;	main.c:404: i=*INST_READ;
                           3003 ;	genPointerGet
                           3004 ;	genFarPointerGet
                           3005 ;	Peephole 182.b	used 16 bit load of dptr
   090D 90 F8 00           3006 	mov	dptr,#0xF800
   0910 E0                 3007 	movx	a,@dptr
   0911 FA                 3008 	mov	r2,a
                           3009 ;	main.c:405: i=i&0x80;
                           3010 ;	genAnd
   0912 53 02 80           3011 	anl	ar2,#0x80
                           3012 ;	main.c:406: *INST_WRITE=i;
                           3013 ;	genAssign
                           3014 ;	Peephole 182.b	used 16 bit load of dptr
   0915 90 F0 00           3015 	mov	dptr,#0xF000
                           3016 ;	genPointerSet
                           3017 ;     genFarPointerSet
   0918 EA                 3018 	mov	a,r2
   0919 F0                 3019 	movx	@dptr,a
                           3020 ;	main.c:407: putstr("Enter the character to display. Hit enter to stop\n\r");
                           3021 ;	genCall
                           3022 ;	Peephole 182.a	used 16 bit load of DPTR
   091A 90 17 77           3023 	mov	dptr,#__str_26
   091D 75 F0 80           3024 	mov	b,#0x80
   0920 12 01 EF           3025 	lcall	_putstr
                           3026 ;	main.c:408: do
   0923                    3027 00103$:
                           3028 ;	main.c:409: {   co=getchar();
                           3029 ;	genCall
   0923 12 03 15           3030 	lcall	_getchar
                           3031 ;	main.c:410: putchar(co);
                           3032 ;	genCall
   0926 AA 82              3033 	mov  r2,dpl
                           3034 ;	Peephole 177.a	removed redundant mov
   0928 C0 02              3035 	push	ar2
   092A 12 03 03           3036 	lcall	_putchar
   092D D0 02              3037 	pop	ar2
                           3038 ;	main.c:411: if(co!=13)
                           3039 ;	genCmpEq
                           3040 ;	gencjne
                           3041 ;	gencjneshort
                           3042 ;	Peephole 241.d	optimized compare
   092F E4                 3043 	clr	a
   0930 BA 0D 01           3044 	cjne	r2,#0x0D,00110$
   0933 04                 3045 	inc	a
   0934                    3046 00110$:
                           3047 ;	Peephole 300	removed redundant label 00111$
                           3048 ;	genIfx
   0934 FB                 3049 	mov	r3,a
                           3050 ;	Peephole 105	removed redundant mov
                           3051 ;	genIfxJump
                           3052 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0935 70 09              3053 	jnz	00104$
                           3054 ;	Peephole 300	removed redundant label 00112$
                           3055 ;	main.c:412: lcdputch(co);
                           3056 ;	genCall
   0937 8A 82              3057 	mov	dpl,r2
   0939 C0 03              3058 	push	ar3
   093B 12 01 55           3059 	lcall	_lcdputch
   093E D0 03              3060 	pop	ar3
   0940                    3061 00104$:
                           3062 ;	main.c:413: }while(co!=13);
                           3063 ;	genIfx
   0940 EB                 3064 	mov	a,r3
                           3065 ;	genIfxJump
                           3066 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0941 60 E0              3067 	jz	00103$
                           3068 ;	Peephole 300	removed redundant label 00113$
                           3069 ;	Peephole 300	removed redundant label 00106$
   0943 22                 3070 	ret
                           3071 ;------------------------------------------------------------
                           3072 ;Allocation info for local variables in function 'gotoxy'
                           3073 ;------------------------------------------------------------
                           3074 ;temp                      Allocated with name '_gotoxy_temp_1_1'
                           3075 ;temp1                     Allocated with name '_gotoxy_temp1_1_1'
                           3076 ;------------------------------------------------------------
                           3077 ;	main.c:420: void gotoxy()
                           3078 ;	-----------------------------------------
                           3079 ;	 function gotoxy
                           3080 ;	-----------------------------------------
   0944                    3081 _gotoxy:
                           3082 ;	main.c:422: unsigned char temp=10, temp1=20;
                           3083 ;	genAssign
   0944 90 00 28           3084 	mov	dptr,#_gotoxy_temp_1_1
   0947 74 0A              3085 	mov	a,#0x0A
   0949 F0                 3086 	movx	@dptr,a
                           3087 ;	genAssign
   094A 90 00 29           3088 	mov	dptr,#_gotoxy_temp1_1_1
   094D 74 14              3089 	mov	a,#0x14
   094F F0                 3090 	movx	@dptr,a
                           3091 ;	main.c:423: putstr("\n\rEnter row number\n\r");
                           3092 ;	genCall
                           3093 ;	Peephole 182.a	used 16 bit load of DPTR
   0950 90 17 AB           3094 	mov	dptr,#__str_27
   0953 75 F0 80           3095 	mov	b,#0x80
   0956 12 01 EF           3096 	lcall	_putstr
                           3097 ;	main.c:424: while(temp>3)
   0959                    3098 00103$:
                           3099 ;	genAssign
   0959 90 00 28           3100 	mov	dptr,#_gotoxy_temp_1_1
   095C E0                 3101 	movx	a,@dptr
                           3102 ;	genCmpGt
                           3103 ;	genCmp
                           3104 ;	genIfxJump
                           3105 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           3106 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   095D FA                 3107 	mov  r2,a
                           3108 ;	Peephole 177.a	removed redundant mov
   095E 24 FC              3109 	add	a,#0xff - 0x03
   0960 50 1A              3110 	jnc	00105$
                           3111 ;	Peephole 300	removed redundant label 00119$
                           3112 ;	main.c:426: temp=ReadVariable();
                           3113 ;	genCall
   0962 12 06 A9           3114 	lcall	_ReadVariable
   0965 AA 82              3115 	mov	r2,dpl
                           3116 ;	genAssign
   0967 90 00 28           3117 	mov	dptr,#_gotoxy_temp_1_1
   096A EA                 3118 	mov	a,r2
   096B F0                 3119 	movx	@dptr,a
                           3120 ;	main.c:427: if(temp>3)
                           3121 ;	genCmpGt
                           3122 ;	genCmp
                           3123 ;	genIfxJump
                           3124 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           3125 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   096C EA                 3126 	mov	a,r2
   096D 24 FC              3127 	add	a,#0xff - 0x03
   096F 50 E8              3128 	jnc	00103$
                           3129 ;	Peephole 300	removed redundant label 00120$
                           3130 ;	main.c:428: putstr("\n\rEnter correct row\n\r");
                           3131 ;	genCall
                           3132 ;	Peephole 182.a	used 16 bit load of DPTR
   0971 90 17 C0           3133 	mov	dptr,#__str_28
   0974 75 F0 80           3134 	mov	b,#0x80
   0977 12 01 EF           3135 	lcall	_putstr
                           3136 ;	Peephole 112.b	changed ljmp to sjmp
   097A 80 DD              3137 	sjmp	00103$
   097C                    3138 00105$:
                           3139 ;	main.c:430: putstr("\n\rEnter column number\n\r");
                           3140 ;	genCall
                           3141 ;	Peephole 182.a	used 16 bit load of DPTR
   097C 90 17 D6           3142 	mov	dptr,#__str_29
   097F 75 F0 80           3143 	mov	b,#0x80
   0982 12 01 EF           3144 	lcall	_putstr
                           3145 ;	main.c:431: while(temp1>15)
   0985                    3146 00108$:
                           3147 ;	genAssign
   0985 90 00 29           3148 	mov	dptr,#_gotoxy_temp1_1_1
   0988 E0                 3149 	movx	a,@dptr
                           3150 ;	genCmpGt
                           3151 ;	genCmp
                           3152 ;	genIfxJump
                           3153 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           3154 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   0989 FA                 3155 	mov  r2,a
                           3156 ;	Peephole 177.a	removed redundant mov
   098A 24 F0              3157 	add	a,#0xff - 0x0F
   098C 50 1A              3158 	jnc	00110$
                           3159 ;	Peephole 300	removed redundant label 00121$
                           3160 ;	main.c:433: temp1=ReadVariable();
                           3161 ;	genCall
   098E 12 06 A9           3162 	lcall	_ReadVariable
   0991 AB 82              3163 	mov	r3,dpl
                           3164 ;	genAssign
   0993 90 00 29           3165 	mov	dptr,#_gotoxy_temp1_1_1
   0996 EB                 3166 	mov	a,r3
   0997 F0                 3167 	movx	@dptr,a
                           3168 ;	main.c:434: if(temp1>15)
                           3169 ;	genCmpGt
                           3170 ;	genCmp
                           3171 ;	genIfxJump
                           3172 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           3173 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   0998 EB                 3174 	mov	a,r3
   0999 24 F0              3175 	add	a,#0xff - 0x0F
   099B 50 E8              3176 	jnc	00108$
                           3177 ;	Peephole 300	removed redundant label 00122$
                           3178 ;	main.c:435: putstr("\n\rEnter correct column number\n\r");
                           3179 ;	genCall
                           3180 ;	Peephole 182.a	used 16 bit load of DPTR
   099D 90 17 EE           3181 	mov	dptr,#__str_30
   09A0 75 F0 80           3182 	mov	b,#0x80
   09A3 12 01 EF           3183 	lcall	_putstr
                           3184 ;	Peephole 112.b	changed ljmp to sjmp
   09A6 80 DD              3185 	sjmp	00108$
   09A8                    3186 00110$:
                           3187 ;	main.c:437: lcdgotoxy(temp, temp1);
                           3188 ;	genAssign
   09A8 90 00 28           3189 	mov	dptr,#_gotoxy_temp_1_1
   09AB E0                 3190 	movx	a,@dptr
   09AC FB                 3191 	mov	r3,a
                           3192 ;	genAssign
   09AD 90 00 11           3193 	mov	dptr,#_lcdgotoxy_PARM_2
   09B0 EA                 3194 	mov	a,r2
   09B1 F0                 3195 	movx	@dptr,a
                           3196 ;	genCall
   09B2 8B 82              3197 	mov	dpl,r3
                           3198 ;	Peephole 253.b	replaced lcall/ret with ljmp
   09B4 02 00 EC           3199 	ljmp	_lcdgotoxy
                           3200 ;
                           3201 ;------------------------------------------------------------
                           3202 ;Allocation info for local variables in function 'gotoaddress'
                           3203 ;------------------------------------------------------------
                           3204 ;temp                      Allocated with name '_gotoaddress_temp_1_1'
                           3205 ;------------------------------------------------------------
                           3206 ;	main.c:440: void gotoaddress()
                           3207 ;	-----------------------------------------
                           3208 ;	 function gotoaddress
                           3209 ;	-----------------------------------------
   09B7                    3210 _gotoaddress:
                           3211 ;	main.c:442: unsigned char temp=0x66;
                           3212 ;	genAssign
   09B7 90 00 2A           3213 	mov	dptr,#_gotoaddress_temp_1_1
   09BA 74 66              3214 	mov	a,#0x66
   09BC F0                 3215 	movx	@dptr,a
                           3216 ;	main.c:443: putstr("Enter the address in decimal\n\r");
                           3217 ;	genCall
                           3218 ;	Peephole 182.a	used 16 bit load of DPTR
   09BD 90 18 0E           3219 	mov	dptr,#__str_31
   09C0 75 F0 80           3220 	mov	b,#0x80
   09C3 12 01 EF           3221 	lcall	_putstr
                           3222 ;	main.c:444: while((temp>0x1F && temp<0x40)||(temp>=0x5F))
   09C6                    3223 00107$:
                           3224 ;	genAssign
   09C6 90 00 2A           3225 	mov	dptr,#_gotoaddress_temp_1_1
   09C9 E0                 3226 	movx	a,@dptr
                           3227 ;	genCmpGt
                           3228 ;	genCmp
                           3229 ;	genIfxJump
                           3230 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           3231 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   09CA FA                 3232 	mov  r2,a
                           3233 ;	Peephole 177.a	removed redundant mov
   09CB 24 E0              3234 	add	a,#0xff - 0x1F
   09CD 50 05              3235 	jnc	00106$
                           3236 ;	Peephole 300	removed redundant label 00115$
                           3237 ;	genCmpLt
                           3238 ;	genCmp
   09CF BA 40 00           3239 	cjne	r2,#0x40,00116$
   09D2                    3240 00116$:
                           3241 ;	genIfxJump
                           3242 ;	Peephole 112.b	changed ljmp to sjmp
                           3243 ;	Peephole 160.a	removed sjmp by inverse jump logic
   09D2 40 0A              3244 	jc	00108$
                           3245 ;	Peephole 300	removed redundant label 00117$
   09D4                    3246 00106$:
                           3247 ;	genAssign
   09D4 90 00 2A           3248 	mov	dptr,#_gotoaddress_temp_1_1
   09D7 E0                 3249 	movx	a,@dptr
   09D8 FA                 3250 	mov	r2,a
                           3251 ;	genCmpLt
                           3252 ;	genCmp
   09D9 BA 5F 00           3253 	cjne	r2,#0x5F,00118$
   09DC                    3254 00118$:
                           3255 ;	genIfxJump
                           3256 ;	Peephole 112.b	changed ljmp to sjmp
                           3257 ;	Peephole 160.a	removed sjmp by inverse jump logic
   09DC 40 24              3258 	jc	00109$
                           3259 ;	Peephole 300	removed redundant label 00119$
   09DE                    3260 00108$:
                           3261 ;	main.c:446: temp=ReadVariable();
                           3262 ;	genCall
   09DE 12 06 A9           3263 	lcall	_ReadVariable
   09E1 AB 82              3264 	mov	r3,dpl
                           3265 ;	genAssign
   09E3 90 00 2A           3266 	mov	dptr,#_gotoaddress_temp_1_1
   09E6 EB                 3267 	mov	a,r3
   09E7 F0                 3268 	movx	@dptr,a
                           3269 ;	main.c:447: if((temp>0x1F && temp<0x40)||(temp>=0x5F))
                           3270 ;	genCmpGt
                           3271 ;	genCmp
                           3272 ;	genIfxJump
                           3273 ;	Peephole 108.a	removed ljmp by inverse jump logic
                           3274 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   09E8 EB                 3275 	mov	a,r3
   09E9 24 E0              3276 	add	a,#0xff - 0x1F
   09EB 50 05              3277 	jnc	00104$
                           3278 ;	Peephole 300	removed redundant label 00120$
                           3279 ;	genCmpLt
                           3280 ;	genCmp
   09ED BB 40 00           3281 	cjne	r3,#0x40,00121$
   09F0                    3282 00121$:
                           3283 ;	genIfxJump
                           3284 ;	Peephole 112.b	changed ljmp to sjmp
                           3285 ;	Peephole 160.a	removed sjmp by inverse jump logic
   09F0 40 05              3286 	jc	00101$
                           3287 ;	Peephole 300	removed redundant label 00122$
   09F2                    3288 00104$:
                           3289 ;	genCmpLt
                           3290 ;	genCmp
   09F2 BB 5F 00           3291 	cjne	r3,#0x5F,00123$
   09F5                    3292 00123$:
                           3293 ;	genIfxJump
                           3294 ;	Peephole 112.b	changed ljmp to sjmp
                           3295 ;	Peephole 160.a	removed sjmp by inverse jump logic
   09F5 40 CF              3296 	jc	00107$
                           3297 ;	Peephole 300	removed redundant label 00124$
   09F7                    3298 00101$:
                           3299 ;	main.c:448: putstr("\n\rEnter correct address values\n\r");
                           3300 ;	genCall
                           3301 ;	Peephole 182.a	used 16 bit load of DPTR
   09F7 90 18 2D           3302 	mov	dptr,#__str_32
   09FA 75 F0 80           3303 	mov	b,#0x80
   09FD 12 01 EF           3304 	lcall	_putstr
                           3305 ;	Peephole 112.b	changed ljmp to sjmp
   0A00 80 C4              3306 	sjmp	00107$
   0A02                    3307 00109$:
                           3308 ;	main.c:450: lcdgotoaddr(temp);
                           3309 ;	genCall
   0A02 8A 82              3310 	mov	dpl,r2
   0A04 12 00 D1           3311 	lcall	_lcdgotoaddr
                           3312 ;	main.c:451: putstr("\n\r");
                           3313 ;	genCall
                           3314 ;	Peephole 182.a	used 16 bit load of DPTR
   0A07 90 15 F7           3315 	mov	dptr,#__str_11
   0A0A 75 F0 80           3316 	mov	b,#0x80
                           3317 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0A0D 02 01 EF           3318 	ljmp	_putstr
                           3319 ;
                           3320 ;------------------------------------------------------------
                           3321 ;Allocation info for local variables in function 'main'
                           3322 ;------------------------------------------------------------
                           3323 ;input                     Allocated with name '_main_input_1_1'
                           3324 ;array1                    Allocated with name '_main_array1_1_1'
                           3325 ;------------------------------------------------------------
                           3326 ;	main.c:454: void main(void)
                           3327 ;	-----------------------------------------
                           3328 ;	 function main
                           3329 ;	-----------------------------------------
   0A10                    3330 _main:
                           3331 ;	main.c:457: unsigned char array1[]={0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
                           3332 ;	genPointerSet
                           3333 ;     genFarPointerSet
   0A10 90 00 2B           3334 	mov	dptr,#_main_array1_1_1
                           3335 ;	Peephole 181	changed mov to clr
                           3336 ;	genPointerSet
                           3337 ;     genFarPointerSet
                           3338 ;	Peephole 181	changed mov to clr
                           3339 ;	Peephole 219.a	removed redundant clear
                           3340 ;	genPointerSet
                           3341 ;     genFarPointerSet
                           3342 ;	Peephole 181	changed mov to clr
                           3343 ;	genPointerSet
                           3344 ;     genFarPointerSet
                           3345 ;	Peephole 181	changed mov to clr
                           3346 ;	Peephole 219.a	removed redundant clear
   0A13 E4                 3347 	clr	a
   0A14 F0                 3348 	movx	@dptr,a
   0A15 90 00 2C           3349 	mov	dptr,#(_main_array1_1_1 + 0x0001)
   0A18 F0                 3350 	movx	@dptr,a
   0A19 90 00 2D           3351 	mov	dptr,#(_main_array1_1_1 + 0x0002)
                           3352 ;	Peephole 219.b	removed redundant clear
   0A1C F0                 3353 	movx	@dptr,a
   0A1D 90 00 2E           3354 	mov	dptr,#(_main_array1_1_1 + 0x0003)
   0A20 F0                 3355 	movx	@dptr,a
                           3356 ;	genPointerSet
                           3357 ;     genFarPointerSet
   0A21 90 00 2F           3358 	mov	dptr,#(_main_array1_1_1 + 0x0004)
                           3359 ;	Peephole 181	changed mov to clr
                           3360 ;	genPointerSet
                           3361 ;     genFarPointerSet
                           3362 ;	Peephole 181	changed mov to clr
                           3363 ;	Peephole 219.a	removed redundant clear
                           3364 ;	genPointerSet
                           3365 ;     genFarPointerSet
                           3366 ;	Peephole 181	changed mov to clr
                           3367 ;	genPointerSet
                           3368 ;     genFarPointerSet
                           3369 ;	Peephole 181	changed mov to clr
                           3370 ;	Peephole 219.a	removed redundant clear
   0A24 E4                 3371 	clr	a
   0A25 F0                 3372 	movx	@dptr,a
   0A26 90 00 30           3373 	mov	dptr,#(_main_array1_1_1 + 0x0005)
   0A29 F0                 3374 	movx	@dptr,a
   0A2A 90 00 31           3375 	mov	dptr,#(_main_array1_1_1 + 0x0006)
                           3376 ;	Peephole 219.b	removed redundant clear
   0A2D F0                 3377 	movx	@dptr,a
   0A2E 90 00 32           3378 	mov	dptr,#(_main_array1_1_1 + 0x0007)
   0A31 F0                 3379 	movx	@dptr,a
                           3380 ;	main.c:458: u=0;
                           3381 ;	genAssign
   0A32 90 00 0B           3382 	mov	dptr,#_u
                           3383 ;	Peephole 181	changed mov to clr
   0A35 E4                 3384 	clr	a
   0A36 F0                 3385 	movx	@dptr,a
                           3386 ;	main.c:459: SerialInitialize();
                           3387 ;	genCall
   0A37 12 02 F4           3388 	lcall	_SerialInitialize
                           3389 ;	main.c:460: putstr("\n\r");
                           3390 ;	genCall
                           3391 ;	Peephole 182.a	used 16 bit load of DPTR
   0A3A 90 15 F7           3392 	mov	dptr,#__str_11
   0A3D 75 F0 80           3393 	mov	b,#0x80
   0A40 12 01 EF           3394 	lcall	_putstr
                           3395 ;	main.c:461: lcdinit();
                           3396 ;	genCall
   0A43 12 02 9B           3397 	lcall	_lcdinit
                           3398 ;	main.c:462: *INST_WRITE=(0x80);
                           3399 ;	genAssign
                           3400 ;	Peephole 182.b	used 16 bit load of dptr
   0A46 90 F0 00           3401 	mov	dptr,#0xF000
                           3402 ;	genPointerSet
                           3403 ;     genFarPointerSet
   0A49 74 80              3404 	mov	a,#0x80
   0A4B F0                 3405 	movx	@dptr,a
                           3406 ;	main.c:463: lcdbusywait();
                           3407 ;	genCall
   0A4C 12 00 97           3408 	lcall	_lcdbusywait
                           3409 ;	main.c:472: lcdgotoxy(1,0);
                           3410 ;	genAssign
   0A4F 90 00 11           3411 	mov	dptr,#_lcdgotoxy_PARM_2
                           3412 ;	Peephole 181	changed mov to clr
   0A52 E4                 3413 	clr	a
   0A53 F0                 3414 	movx	@dptr,a
                           3415 ;	genCall
   0A54 75 82 01           3416 	mov	dpl,#0x01
   0A57 12 00 EC           3417 	lcall	_lcdgotoxy
                           3418 ;	main.c:473: lcdputstr("YOU KNOW NOTHING");
                           3419 ;	genCall
                           3420 ;	Peephole 182.a	used 16 bit load of DPTR
   0A5A 90 18 4E           3421 	mov	dptr,#__str_33
   0A5D 75 F0 80           3422 	mov	b,#0x80
   0A60 12 02 45           3423 	lcall	_lcdputstr
                           3424 ;	main.c:474: lcdgotoxy(2,4);
                           3425 ;	genAssign
   0A63 90 00 11           3426 	mov	dptr,#_lcdgotoxy_PARM_2
   0A66 74 04              3427 	mov	a,#0x04
   0A68 F0                 3428 	movx	@dptr,a
                           3429 ;	genCall
   0A69 75 82 02           3430 	mov	dpl,#0x02
   0A6C 12 00 EC           3431 	lcall	_lcdgotoxy
                           3432 ;	main.c:475: lcdputstr("JON SNOW");
                           3433 ;	genCall
                           3434 ;	Peephole 182.a	used 16 bit load of DPTR
   0A6F 90 18 5F           3435 	mov	dptr,#__str_34
   0A72 75 F0 80           3436 	mov	b,#0x80
   0A75 12 02 45           3437 	lcall	_lcdputstr
                           3438 ;	main.c:476: MSDelay(100);
                           3439 ;	genCall
                           3440 ;	Peephole 182.b	used 16 bit load of dptr
   0A78 90 00 64           3441 	mov	dptr,#0x0064
   0A7B 12 00 64           3442 	lcall	_MSDelay
                           3443 ;	main.c:477: putstr("Welcome\n\n\r");
                           3444 ;	genCall
                           3445 ;	Peephole 182.a	used 16 bit load of DPTR
   0A7E 90 18 68           3446 	mov	dptr,#__str_35
   0A81 75 F0 80           3447 	mov	b,#0x80
   0A84 12 01 EF           3448 	lcall	_putstr
                           3449 ;	main.c:478: putstr("Enter > for help menu\n\r");
                           3450 ;	genCall
                           3451 ;	Peephole 182.a	used 16 bit load of DPTR
   0A87 90 18 73           3452 	mov	dptr,#__str_36
   0A8A 75 F0 80           3453 	mov	b,#0x80
   0A8D 12 01 EF           3454 	lcall	_putstr
                           3455 ;	main.c:480: while (1)
   0A90                    3456 00113$:
                           3457 ;	main.c:482: putstr("\n\rEnter the character\n\r");
                           3458 ;	genCall
                           3459 ;	Peephole 182.a	used 16 bit load of DPTR
   0A90 90 18 8B           3460 	mov	dptr,#__str_37
   0A93 75 F0 80           3461 	mov	b,#0x80
   0A96 12 01 EF           3462 	lcall	_putstr
                           3463 ;	main.c:483: input = getchar();
                           3464 ;	genCall
   0A99 12 03 15           3465 	lcall	_getchar
   0A9C AA 82              3466 	mov	r2,dpl
                           3467 ;	main.c:484: switch(input)
                           3468 ;	genCmpEq
                           3469 ;	gencjneshort
   0A9E BA 3E 02           3470 	cjne	r2,#0x3E,00127$
                           3471 ;	Peephole 112.b	changed ljmp to sjmp
   0AA1 80 30              3472 	sjmp	00103$
   0AA3                    3473 00127$:
                           3474 ;	genCmpEq
                           3475 ;	gencjneshort
   0AA3 BA 41 02           3476 	cjne	r2,#0x41,00128$
                           3477 ;	Peephole 112.b	changed ljmp to sjmp
   0AA6 80 3F              3478 	sjmp	00107$
   0AA8                    3479 00128$:
                           3480 ;	genCmpEq
                           3481 ;	gencjneshort
   0AA8 BA 43 02           3482 	cjne	r2,#0x43,00129$
                           3483 ;	Peephole 112.b	changed ljmp to sjmp
   0AAB 80 2B              3484 	sjmp	00104$
   0AAD                    3485 00129$:
                           3486 ;	genCmpEq
                           3487 ;	gencjneshort
   0AAD BA 44 02           3488 	cjne	r2,#0x44,00130$
                           3489 ;	Peephole 112.b	changed ljmp to sjmp
   0AB0 80 2B              3490 	sjmp	00105$
   0AB2                    3491 00130$:
                           3492 ;	genCmpEq
                           3493 ;	gencjneshort
   0AB2 BA 45 02           3494 	cjne	r2,#0x45,00131$
                           3495 ;	Peephole 112.b	changed ljmp to sjmp
   0AB5 80 2B              3496 	sjmp	00106$
   0AB7                    3497 00131$:
                           3498 ;	genCmpEq
                           3499 ;	gencjneshort
   0AB7 BA 47 02           3500 	cjne	r2,#0x47,00132$
                           3501 ;	Peephole 112.b	changed ljmp to sjmp
   0ABA 80 12              3502 	sjmp	00102$
   0ABC                    3503 00132$:
                           3504 ;	genCmpEq
                           3505 ;	gencjneshort
   0ABC BA 49 02           3506 	cjne	r2,#0x49,00133$
                           3507 ;	Peephole 112.b	changed ljmp to sjmp
   0ABF 80 30              3508 	sjmp	00109$
   0AC1                    3509 00133$:
                           3510 ;	genCmpEq
                           3511 ;	gencjneshort
   0AC1 BA 53 02           3512 	cjne	r2,#0x53,00134$
                           3513 ;	Peephole 112.b	changed ljmp to sjmp
   0AC4 80 26              3514 	sjmp	00108$
   0AC6                    3515 00134$:
                           3516 ;	genCmpEq
                           3517 ;	gencjneshort
                           3518 ;	Peephole 112.b	changed ljmp to sjmp
                           3519 ;	Peephole 198.b	optimized misc jump sequence
   0AC6 BA 58 2D           3520 	cjne	r2,#0x58,00110$
                           3521 ;	Peephole 200.b	removed redundant sjmp
                           3522 ;	Peephole 300	removed redundant label 00135$
                           3523 ;	Peephole 300	removed redundant label 00136$
                           3524 ;	main.c:486: case 'X' : gotoxy();
                           3525 ;	genCall
   0AC9 12 09 44           3526 	lcall	_gotoxy
                           3527 ;	main.c:487: break;
                           3528 ;	main.c:488: case 'G' : gotoaddress();
                           3529 ;	Peephole 112.b	changed ljmp to sjmp
   0ACC 80 C2              3530 	sjmp	00113$
   0ACE                    3531 00102$:
                           3532 ;	genCall
   0ACE 12 09 B7           3533 	lcall	_gotoaddress
                           3534 ;	main.c:489: break;
                           3535 ;	main.c:490: case '>' : help_menu();
                           3536 ;	Peephole 112.b	changed ljmp to sjmp
   0AD1 80 BD              3537 	sjmp	00113$
   0AD3                    3538 00103$:
                           3539 ;	genCall
   0AD3 12 03 24           3540 	lcall	_help_menu
                           3541 ;	main.c:491: break;
                           3542 ;	main.c:492: case 'C' : cgramdump();
                           3543 ;	Peephole 112.b	changed ljmp to sjmp
   0AD6 80 B8              3544 	sjmp	00113$
   0AD8                    3545 00104$:
                           3546 ;	genCall
   0AD8 12 03 87           3547 	lcall	_cgramdump
                           3548 ;	main.c:493: break;
                           3549 ;	main.c:494: case 'D' : ddramdump();
                           3550 ;	Peephole 112.b	changed ljmp to sjmp
   0ADB 80 B3              3551 	sjmp	00113$
   0ADD                    3552 00105$:
                           3553 ;	genCall
   0ADD 12 05 50           3554 	lcall	_ddramdump
                           3555 ;	main.c:495: break;
                           3556 ;	main.c:496: case 'E' : lcdclear();
                           3557 ;	Peephole 112.b	changed ljmp to sjmp
   0AE0 80 AE              3558 	sjmp	00113$
   0AE2                    3559 00106$:
                           3560 ;	genCall
   0AE2 12 01 43           3561 	lcall	_lcdclear
                           3562 ;	main.c:497: break;
                           3563 ;	main.c:498: case 'A' : addcharacter();
                           3564 ;	Peephole 112.b	changed ljmp to sjmp
   0AE5 80 A9              3565 	sjmp	00113$
   0AE7                    3566 00107$:
                           3567 ;	genCall
   0AE7 12 07 7B           3568 	lcall	_addcharacter
                           3569 ;	main.c:499: break;
                           3570 ;	main.c:500: case 'S' : display();
                           3571 ;	Peephole 112.b	changed ljmp to sjmp
   0AEA 80 A4              3572 	sjmp	00113$
   0AEC                    3573 00108$:
                           3574 ;	genCall
   0AEC 12 08 F0           3575 	lcall	_display
                           3576 ;	main.c:501: break;
                           3577 ;	main.c:502: case 'I' : interactive();
                           3578 ;	Peephole 112.b	changed ljmp to sjmp
   0AEF 80 9F              3579 	sjmp	00113$
   0AF1                    3580 00109$:
                           3581 ;	genCall
   0AF1 12 09 0D           3582 	lcall	_interactive
                           3583 ;	main.c:503: break;
                           3584 ;	main.c:504: default : putstr("Enter the correct character\n\r");
                           3585 ;	Peephole 112.b	changed ljmp to sjmp
   0AF4 80 9A              3586 	sjmp	00113$
   0AF6                    3587 00110$:
                           3588 ;	genCall
                           3589 ;	Peephole 182.a	used 16 bit load of DPTR
   0AF6 90 18 A3           3590 	mov	dptr,#__str_38
   0AF9 75 F0 80           3591 	mov	b,#0x80
   0AFC 12 01 EF           3592 	lcall	_putstr
                           3593 ;	main.c:505: }
                           3594 ;	Peephole 112.b	changed ljmp to sjmp
   0AFF 80 8F              3595 	sjmp	00113$
                           3596 ;	Peephole 259.a	removed redundant label 00115$ and ret
                           3597 ;
                           3598 	.area CSEG    (CODE)
                           3599 	.area CONST   (CODE)
   1425                    3600 __str_0:
   1425 43 6C 65 61 72 69  3601 	.ascii "Clearing the LCD completed"
        6E 67 20 74 68 65
        20 4C 43 44 20 63
        6F 6D 70 6C 65 74
        65 64
   143F 0A                 3602 	.db 0x0A
   1440 0D                 3603 	.db 0x0D
   1441 00                 3604 	.db 0x00
   1442                    3605 __str_1:
   1442 0A                 3606 	.db 0x0A
   1443 0D                 3607 	.db 0x0D
   1444 7E 7E 7E 7E 7E 7E  3608 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E
   147E 7E 7E 7E 7E 7E 7E  3609 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E
   1498 0A                 3610 	.db 0x0A
   1499 0D                 3611 	.db 0x0D
   149A 00                 3612 	.db 0x00
   149B                    3613 __str_2:
   149B 57 65 6C 63 6F 6D  3614 	.ascii "Welcome to Help Menu"
        65 20 74 6F 20 48
        65 6C 70 20 4D 65
        6E 75
   14AF 0A                 3615 	.db 0x0A
   14B0 0D                 3616 	.db 0x0D
   14B1 00                 3617 	.db 0x00
   14B2                    3618 __str_3:
   14B2 50 72 65 73 73 20  3619 	.ascii "Press 'C' for CGRAM HEX dump"
        27 43 27 20 66 6F
        72 20 43 47 52 41
        4D 20 48 45 58 20
        64 75 6D 70
   14CE 0A                 3620 	.db 0x0A
   14CF 0D                 3621 	.db 0x0D
   14D0 00                 3622 	.db 0x00
   14D1                    3623 __str_4:
   14D1 50 72 65 73 73 20  3624 	.ascii "Press 'D' for DDRAM HEX dump"
        27 44 27 20 66 6F
        72 20 44 44 52 41
        4D 20 48 45 58 20
        64 75 6D 70
   14ED 0A                 3625 	.db 0x0A
   14EE 0D                 3626 	.db 0x0D
   14EF 00                 3627 	.db 0x00
   14F0                    3628 __str_5:
   14F0 50 72 65 73 73 20  3629 	.ascii "Press 'E' for Clearing the LCD"
        27 45 27 20 66 6F
        72 20 43 6C 65 61
        72 69 6E 67 20 74
        68 65 20 4C 43 44
   150E 0A                 3630 	.db 0x0A
   150F 0D                 3631 	.db 0x0D
   1510 00                 3632 	.db 0x00
   1511                    3633 __str_6:
   1511 50 72 65 73 73 20  3634 	.ascii "Press 'S' to display the custom character"
        27 53 27 20 74 6F
        20 64 69 73 70 6C
        61 79 20 74 68 65
        20 63 75 73 74 6F
        6D 20 63 68 61 72
        61 63 74 65 72
   153A 0A                 3635 	.db 0x0A
   153B 0D                 3636 	.db 0x0D
   153C 00                 3637 	.db 0x00
   153D                    3638 __str_7:
   153D 50 72 65 73 73 20  3639 	.ascii "Press 'A' to add custom character"
        27 41 27 20 74 6F
        20 61 64 64 20 63
        75 73 74 6F 6D 20
        63 68 61 72 61 63
        74 65 72
   155E 0A                 3640 	.db 0x0A
   155F 0D                 3641 	.db 0x0D
   1560 00                 3642 	.db 0x00
   1561                    3643 __str_8:
   1561 50 72 65 73 73 20  3644 	.ascii "Press 'I' for interactive mode"
        27 49 27 20 66 6F
        72 20 69 6E 74 65
        72 61 63 74 69 76
        65 20 6D 6F 64 65
   157F 0A                 3645 	.db 0x0A
   1580 0D                 3646 	.db 0x0D
   1581 00                 3647 	.db 0x00
   1582                    3648 __str_9:
   1582 50 72 65 73 73 20  3649 	.ascii "Press 'X' for moving the cursor to user input x,y position"
        27 58 27 20 66 6F
        72 20 6D 6F 76 69
        6E 67 20 74 68 65
        20 63 75 72 73 6F
        72 20 74 6F 20 75
        73 65 72 20 69 6E
        70 75 74 20 78 2C
        79 20 70 6F 73 69
        74 69 6F 6E
   15BC 0A                 3650 	.db 0x0A
   15BD 0D                 3651 	.db 0x0D
   15BE 00                 3652 	.db 0x00
   15BF                    3653 __str_10:
   15BF 50 72 65 73 73 20  3654 	.ascii "Press 'G' for moving the cursor to user input address"
        27 47 27 20 66 6F
        72 20 6D 6F 76 69
        6E 67 20 74 68 65
        20 63 75 72 73 6F
        72 20 74 6F 20 75
        73 65 72 20 69 6E
        70 75 74 20 61 64
        64 72 65 73 73
   15F4 0A                 3655 	.db 0x0A
   15F5 0D                 3656 	.db 0x0D
   15F6 00                 3657 	.db 0x00
   15F7                    3658 __str_11:
   15F7 0A                 3659 	.db 0x0A
   15F8 0D                 3660 	.db 0x0D
   15F9 00                 3661 	.db 0x00
   15FA                    3662 __str_12:
   15FA 43 47 52 41 4D 20  3663 	.ascii "CGRAM values"
        76 61 6C 75 65 73
   1606 0A                 3664 	.db 0x0A
   1607 0D                 3665 	.db 0x0D
   1608 00                 3666 	.db 0x00
   1609                    3667 __str_13:
   1609 0A                 3668 	.db 0x0A
   160A 0D                 3669 	.db 0x0D
   160B 43 43 4F 44 45 20  3670 	.ascii "CCODE |  Values"
        7C 20 20 56 61 6C
        75 65 73
   161A 0A                 3671 	.db 0x0A
   161B 0D                 3672 	.db 0x0D
   161C 00                 3673 	.db 0x00
   161D                    3674 __str_14:
   161D 30 78 25 30 34 78  3675 	.ascii "0x%04x:  "
        3A 20 20
   1626 00                 3676 	.db 0x00
   1627                    3677 __str_15:
   1627 25 30 32 78 20 20  3678 	.ascii "%02x  "
   162D 00                 3679 	.db 0x00
   162E                    3680 __str_16:
   162E 44 44 52 41 4D 20  3681 	.ascii "DDRAM Values"
        56 61 6C 75 65 73
   163A 0A                 3682 	.db 0x0A
   163B 0D                 3683 	.db 0x0D
   163C 00                 3684 	.db 0x00
   163D                    3685 __str_17:
   163D 0A                 3686 	.db 0x0A
   163E 0D                 3687 	.db 0x0D
   163F 41 44 44 52 7C 20  3688 	.ascii "ADDR|  +0  +1  +2  +3  +4  +5  +6  +7  +8  +9  +A  +B  +C "
        20 2B 30 20 20 2B
        31 20 20 2B 32 20
        20 2B 33 20 20 2B
        34 20 20 2B 35 20
        20 2B 36 20 20 2B
        37 20 20 2B 38 20
        20 2B 39 20 20 2B
        41 20 20 2B 42 20
        20 2B 43 20
   1679 20 2B 44 20 20 2B  3689 	.ascii " +D  +E  +F"
        45 20 20 2B 46
   1684 0A                 3690 	.db 0x0A
   1685 0D                 3691 	.db 0x0D
   1686 0A                 3692 	.db 0x0A
   1687 0D                 3693 	.db 0x0D
   1688 00                 3694 	.db 0x00
   1689                    3695 __str_18:
   1689 30 78 25 30 32 78  3696 	.ascii "0x%02x:  "
        3A 20 20
   1692 00                 3697 	.db 0x00
   1693                    3698 __str_19:
   1693 0A                 3699 	.db 0x0A
   1694 0D                 3700 	.db 0x0D
   1695 45 6E 74 65 72 20  3701 	.ascii "Enter Numbers"
        4E 75 6D 62 65 72
        73
   16A2 0A                 3702 	.db 0x0A
   16A3 0D                 3703 	.db 0x0D
   16A4 00                 3704 	.db 0x00
   16A5                    3705 __str_20:
   16A5 0A                 3706 	.db 0x0A
   16A6 0D                 3707 	.db 0x0D
   16A7 45 6E 74 65 72 20  3708 	.ascii "Enter Valid Numbers"
        56 61 6C 69 64 20
        4E 75 6D 62 65 72
        73
   16BA 0A                 3709 	.db 0x0A
   16BB 0D                 3710 	.db 0x0D
   16BC 00                 3711 	.db 0x00
   16BD                    3712 __str_21:
   16BD 0A                 3713 	.db 0x0A
   16BE 0D                 3714 	.db 0x0D
   16BF 45 6E 74 65 72 20  3715 	.ascii "Enter eight pixel values"
        65 69 67 68 74 20
        70 69 78 65 6C 20
        76 61 6C 75 65 73
   16D7 0A                 3716 	.db 0x0A
   16D8 0D                 3717 	.db 0x0D
   16D9 00                 3718 	.db 0x00
   16DA                    3719 __str_22:
   16DA 45 6E 74 65 72 20  3720 	.ascii "Enter five binary values for every pixel"
        66 69 76 65 20 62
        69 6E 61 72 79 20
        76 61 6C 75 65 73
        20 66 6F 72 20 65
        76 65 72 79 20 70
        69 78 65 6C
   1702 0A                 3721 	.db 0x0A
   1703 0D                 3722 	.db 0x0D
   1704 00                 3723 	.db 0x00
   1705                    3724 __str_23:
   1705 49 6E 76 61 6C 69  3725 	.ascii "Invalid Number. Enter Again"
        64 20 4E 75 6D 62
        65 72 2E 20 45 6E
        74 65 72 20 41 67
        61 69 6E
   1720 0A                 3726 	.db 0x0A
   1721 0D                 3727 	.db 0x0D
   1722 00                 3728 	.db 0x00
   1723                    3729 __str_24:
   1723 45 69 67 68 74 20  3730 	.ascii "Eight custom character created. No more custom character ple"
        63 75 73 74 6F 6D
        20 63 68 61 72 61
        63 74 65 72 20 63
        72 65 61 74 65 64
        2E 20 4E 6F 20 6D
        6F 72 65 20 63 75
        73 74 6F 6D 20 63
        68 61 72 61 63 74
        65 72 20 70 6C 65
   175F 61 73 65           3731 	.ascii "ase"
   1762 0A                 3732 	.db 0x0A
   1763 0D                 3733 	.db 0x0D
   1764 00                 3734 	.db 0x00
   1765                    3735 __str_25:
   1765 4C 6F 6F 6B 20 61  3736 	.ascii "Look at the LCD"
        74 20 74 68 65 20
        4C 43 44
   1774 0A                 3737 	.db 0x0A
   1775 0D                 3738 	.db 0x0D
   1776 00                 3739 	.db 0x00
   1777                    3740 __str_26:
   1777 45 6E 74 65 72 20  3741 	.ascii "Enter the character to display. Hit enter to stop"
        74 68 65 20 63 68
        61 72 61 63 74 65
        72 20 74 6F 20 64
        69 73 70 6C 61 79
        2E 20 48 69 74 20
        65 6E 74 65 72 20
        74 6F 20 73 74 6F
        70
   17A8 0A                 3742 	.db 0x0A
   17A9 0D                 3743 	.db 0x0D
   17AA 00                 3744 	.db 0x00
   17AB                    3745 __str_27:
   17AB 0A                 3746 	.db 0x0A
   17AC 0D                 3747 	.db 0x0D
   17AD 45 6E 74 65 72 20  3748 	.ascii "Enter row number"
        72 6F 77 20 6E 75
        6D 62 65 72
   17BD 0A                 3749 	.db 0x0A
   17BE 0D                 3750 	.db 0x0D
   17BF 00                 3751 	.db 0x00
   17C0                    3752 __str_28:
   17C0 0A                 3753 	.db 0x0A
   17C1 0D                 3754 	.db 0x0D
   17C2 45 6E 74 65 72 20  3755 	.ascii "Enter correct row"
        63 6F 72 72 65 63
        74 20 72 6F 77
   17D3 0A                 3756 	.db 0x0A
   17D4 0D                 3757 	.db 0x0D
   17D5 00                 3758 	.db 0x00
   17D6                    3759 __str_29:
   17D6 0A                 3760 	.db 0x0A
   17D7 0D                 3761 	.db 0x0D
   17D8 45 6E 74 65 72 20  3762 	.ascii "Enter column number"
        63 6F 6C 75 6D 6E
        20 6E 75 6D 62 65
        72
   17EB 0A                 3763 	.db 0x0A
   17EC 0D                 3764 	.db 0x0D
   17ED 00                 3765 	.db 0x00
   17EE                    3766 __str_30:
   17EE 0A                 3767 	.db 0x0A
   17EF 0D                 3768 	.db 0x0D
   17F0 45 6E 74 65 72 20  3769 	.ascii "Enter correct column number"
        63 6F 72 72 65 63
        74 20 63 6F 6C 75
        6D 6E 20 6E 75 6D
        62 65 72
   180B 0A                 3770 	.db 0x0A
   180C 0D                 3771 	.db 0x0D
   180D 00                 3772 	.db 0x00
   180E                    3773 __str_31:
   180E 45 6E 74 65 72 20  3774 	.ascii "Enter the address in decimal"
        74 68 65 20 61 64
        64 72 65 73 73 20
        69 6E 20 64 65 63
        69 6D 61 6C
   182A 0A                 3775 	.db 0x0A
   182B 0D                 3776 	.db 0x0D
   182C 00                 3777 	.db 0x00
   182D                    3778 __str_32:
   182D 0A                 3779 	.db 0x0A
   182E 0D                 3780 	.db 0x0D
   182F 45 6E 74 65 72 20  3781 	.ascii "Enter correct address values"
        63 6F 72 72 65 63
        74 20 61 64 64 72
        65 73 73 20 76 61
        6C 75 65 73
   184B 0A                 3782 	.db 0x0A
   184C 0D                 3783 	.db 0x0D
   184D 00                 3784 	.db 0x00
   184E                    3785 __str_33:
   184E 59 4F 55 20 4B 4E  3786 	.ascii "YOU KNOW NOTHING"
        4F 57 20 4E 4F 54
        48 49 4E 47
   185E 00                 3787 	.db 0x00
   185F                    3788 __str_34:
   185F 4A 4F 4E 20 53 4E  3789 	.ascii "JON SNOW"
        4F 57
   1867 00                 3790 	.db 0x00
   1868                    3791 __str_35:
   1868 57 65 6C 63 6F 6D  3792 	.ascii "Welcome"
        65
   186F 0A                 3793 	.db 0x0A
   1870 0A                 3794 	.db 0x0A
   1871 0D                 3795 	.db 0x0D
   1872 00                 3796 	.db 0x00
   1873                    3797 __str_36:
   1873 45 6E 74 65 72 20  3798 	.ascii "Enter > for help menu"
        3E 20 66 6F 72 20
        68 65 6C 70 20 6D
        65 6E 75
   1888 0A                 3799 	.db 0x0A
   1889 0D                 3800 	.db 0x0D
   188A 00                 3801 	.db 0x00
   188B                    3802 __str_37:
   188B 0A                 3803 	.db 0x0A
   188C 0D                 3804 	.db 0x0D
   188D 45 6E 74 65 72 20  3805 	.ascii "Enter the character"
        74 68 65 20 63 68
        61 72 61 63 74 65
        72
   18A0 0A                 3806 	.db 0x0A
   18A1 0D                 3807 	.db 0x0D
   18A2 00                 3808 	.db 0x00
   18A3                    3809 __str_38:
   18A3 45 6E 74 65 72 20  3810 	.ascii "Enter the correct character"
        74 68 65 20 63 6F
        72 72 65 63 74 20
        63 68 61 72 61 63
        74 65 72
   18BE 0A                 3811 	.db 0x0A
   18BF 0D                 3812 	.db 0x0D
   18C0 00                 3813 	.db 0x00
                           3814 	.area XINIT   (CODE)
