;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.6.0 #4309 (Jul 28 2006)
; This file generated Sun Oct 22 10:03:12 2017
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl __sdcc_external_startup
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _PS
	.globl _PT1
	.globl _PX1
	.globl _PT0
	.globl _PX0
	.globl _RD
	.globl _WR
	.globl _T1
	.globl _T0
	.globl _INT1
	.globl _INT0
	.globl _TXD
	.globl _RXD
	.globl _P3_7
	.globl _P3_6
	.globl _P3_5
	.globl _P3_4
	.globl _P3_3
	.globl _P3_2
	.globl _P3_1
	.globl _P3_0
	.globl _EA
	.globl _ES
	.globl _ET1
	.globl _EX1
	.globl _ET0
	.globl _EX0
	.globl _P2_7
	.globl _P2_6
	.globl _P2_5
	.globl _P2_4
	.globl _P2_3
	.globl _P2_2
	.globl _P2_1
	.globl _P2_0
	.globl _SM0
	.globl _SM1
	.globl _SM2
	.globl _REN
	.globl _TB8
	.globl _RB8
	.globl _TI
	.globl _RI
	.globl _P1_7
	.globl _P1_6
	.globl _P1_5
	.globl _P1_4
	.globl _P1_3
	.globl _P1_2
	.globl _P1_1
	.globl _P1_0
	.globl _TF1
	.globl _TR1
	.globl _TF0
	.globl _TR0
	.globl _IE1
	.globl _IT1
	.globl _IE0
	.globl _IT0
	.globl _P0_7
	.globl _P0_6
	.globl _P0_5
	.globl _P0_4
	.globl _P0_3
	.globl _P0_2
	.globl _P0_1
	.globl _P0_0
	.globl _TXD0
	.globl _RXD0
	.globl _BREG_F7
	.globl _BREG_F6
	.globl _BREG_F5
	.globl _BREG_F4
	.globl _BREG_F3
	.globl _BREG_F2
	.globl _BREG_F1
	.globl _BREG_F0
	.globl _P5_7
	.globl _P5_6
	.globl _P5_5
	.globl _P5_4
	.globl _P5_3
	.globl _P5_2
	.globl _P5_1
	.globl _P5_0
	.globl _P4_7
	.globl _P4_6
	.globl _P4_5
	.globl _P4_4
	.globl _P4_3
	.globl _P4_2
	.globl _P4_1
	.globl _P4_0
	.globl _PX0L
	.globl _PT0L
	.globl _PX1L
	.globl _PT1L
	.globl _PLS
	.globl _PT2L
	.globl _PPCL
	.globl _EC
	.globl _CCF0
	.globl _CCF1
	.globl _CCF2
	.globl _CCF3
	.globl _CCF4
	.globl _CR
	.globl _CF
	.globl _TF2
	.globl _EXF2
	.globl _RCLK
	.globl _TCLK
	.globl _EXEN2
	.globl _TR2
	.globl _C_T2
	.globl _CP_RL2
	.globl _T2CON_7
	.globl _T2CON_6
	.globl _T2CON_5
	.globl _T2CON_4
	.globl _T2CON_3
	.globl _T2CON_2
	.globl _T2CON_1
	.globl _T2CON_0
	.globl _PT2
	.globl _ET2
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _IP
	.globl _P3
	.globl _IE
	.globl _P2
	.globl _SBUF
	.globl _SCON
	.globl _P1
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _P0
	.globl _SBUF0
	.globl _DP0L
	.globl _DP0H
	.globl _EECON
	.globl _KBF
	.globl _KBE
	.globl _KBLS
	.globl _BRL
	.globl _BDRCON
	.globl _T2MOD
	.globl _SPDAT
	.globl _SPSTA
	.globl _SPCON
	.globl _SADEN
	.globl _SADDR
	.globl _WDTPRG
	.globl _WDTRST
	.globl _P5
	.globl _P4
	.globl _IPH1
	.globl _IPL1
	.globl _IPH0
	.globl _IPL0
	.globl _IEN1
	.globl _IEN0
	.globl _CMOD
	.globl _CL
	.globl _CH
	.globl _CCON
	.globl _CCAPM4
	.globl _CCAPM3
	.globl _CCAPM2
	.globl _CCAPM1
	.globl _CCAPM0
	.globl _CCAP4L
	.globl _CCAP3L
	.globl _CCAP2L
	.globl _CCAP1L
	.globl _CCAP0L
	.globl _CCAP4H
	.globl _CCAP3H
	.globl _CCAP2H
	.globl _CCAP1H
	.globl _CCAP0H
	.globl _CKCKON1
	.globl _CKCKON0
	.globl _CKRL
	.globl _AUXR1
	.globl _AUXR
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T2CON
	.globl _i
	.globl _res
	.globl _value
	.globl _ptr
	.globl _g
	.globl _j
	.globl _buff_val
	.globl _temp
	.globl _rec
	.globl _bufflen
	.globl _bptr1
	.globl _bptr0
	.globl _buff2ptr
	.globl _buff1ptr
	.globl _buffer
	.globl _heap
	.globl _BufferInitialize
	.globl _ReadValue
	.globl _newbuffer
	.globl _help_menu
	.globl _deletebuffer
	.globl _fillbuffer0
	.globl _fillbuffer1
	.globl _freeEverything
	.globl _displaydelete
	.globl _display
	.globl _putstrhex
	.globl _putstrbuff
	.globl _putstr
	.globl _SerialInitialize
	.globl _getchar
	.globl _putchar
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (DATA)
_T2CON	=	0x00c8
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_TL2	=	0x00cc
_TH2	=	0x00cd
_AUXR	=	0x008e
_AUXR1	=	0x00a2
_CKRL	=	0x0097
_CKCKON0	=	0x008f
_CKCKON1	=	0x008f
_CCAP0H	=	0x00fa
_CCAP1H	=	0x00fb
_CCAP2H	=	0x00fc
_CCAP3H	=	0x00fd
_CCAP4H	=	0x00fe
_CCAP0L	=	0x00ea
_CCAP1L	=	0x00eb
_CCAP2L	=	0x00ec
_CCAP3L	=	0x00ed
_CCAP4L	=	0x00ee
_CCAPM0	=	0x00da
_CCAPM1	=	0x00db
_CCAPM2	=	0x00dc
_CCAPM3	=	0x00dd
_CCAPM4	=	0x00de
_CCON	=	0x00d8
_CH	=	0x00f9
_CL	=	0x00e9
_CMOD	=	0x00d9
_IEN0	=	0x00a8
_IEN1	=	0x00b1
_IPL0	=	0x00b8
_IPH0	=	0x00b7
_IPL1	=	0x00b2
_IPH1	=	0x00b3
_P4	=	0x00c0
_P5	=	0x00d8
_WDTRST	=	0x00a6
_WDTPRG	=	0x00a7
_SADDR	=	0x00a9
_SADEN	=	0x00b9
_SPCON	=	0x00c3
_SPSTA	=	0x00c4
_SPDAT	=	0x00c5
_T2MOD	=	0x00c9
_BDRCON	=	0x009b
_BRL	=	0x009a
_KBLS	=	0x009c
_KBE	=	0x009d
_KBF	=	0x009e
_EECON	=	0x00d2
_DP0H	=	0x0083
_DP0L	=	0x0082
_SBUF0	=	0x0099
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (DATA)
_ET2	=	0x00ad
_PT2	=	0x00bd
_T2CON_0	=	0x00c8
_T2CON_1	=	0x00c9
_T2CON_2	=	0x00ca
_T2CON_3	=	0x00cb
_T2CON_4	=	0x00cc
_T2CON_5	=	0x00cd
_T2CON_6	=	0x00ce
_T2CON_7	=	0x00cf
_CP_RL2	=	0x00c8
_C_T2	=	0x00c9
_TR2	=	0x00ca
_EXEN2	=	0x00cb
_TCLK	=	0x00cc
_RCLK	=	0x00cd
_EXF2	=	0x00ce
_TF2	=	0x00cf
_CF	=	0x00df
_CR	=	0x00de
_CCF4	=	0x00dc
_CCF3	=	0x00db
_CCF2	=	0x00da
_CCF1	=	0x00d9
_CCF0	=	0x00d8
_EC	=	0x00ae
_PPCL	=	0x00be
_PT2L	=	0x00bd
_PLS	=	0x00bc
_PT1L	=	0x00bb
_PX1L	=	0x00ba
_PT0L	=	0x00b9
_PX0L	=	0x00b8
_P4_0	=	0x00c0
_P4_1	=	0x00c1
_P4_2	=	0x00c2
_P4_3	=	0x00c3
_P4_4	=	0x00c4
_P4_5	=	0x00c5
_P4_6	=	0x00c6
_P4_7	=	0x00c7
_P5_0	=	0x00d8
_P5_1	=	0x00d9
_P5_2	=	0x00da
_P5_3	=	0x00db
_P5_4	=	0x00dc
_P5_5	=	0x00dd
_P5_6	=	0x00de
_P5_7	=	0x00df
_BREG_F0	=	0x00f0
_BREG_F1	=	0x00f1
_BREG_F2	=	0x00f2
_BREG_F3	=	0x00f3
_BREG_F4	=	0x00f4
_BREG_F5	=	0x00f5
_BREG_F6	=	0x00f6
_BREG_F7	=	0x00f7
_RXD0	=	0x00b0
_TXD0	=	0x00b1
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
_putstrhex_sloc1_1_0:
	.ds 2
_putstrhex_sloc3_1_0:
	.ds 2
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
_fillbuffer0_sloc1_1_0::
	.ds 2
_fillbuffer0_sloc2_1_0::
	.ds 3
	.area	OSEG    (OVR,DATA)
_fillbuffer1_sloc1_1_0::
	.ds 2
_fillbuffer1_sloc2_1_0::
	.ds 3
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_heap::
	.ds 3000
_buffer_count:
	.ds 2
_stor_count:
	.ds 2
_char_count:
	.ds 2
_buffer_number:
	.ds 2
_lastchar_count:
	.ds 2
_buffer::
	.ds 512
_buff1ptr::
	.ds 3
_buff2ptr::
	.ds 3
_bptr0::
	.ds 3
_bptr1::
	.ds 3
_bufflen::
	.ds 512
_rec::
	.ds 1
_temp::
	.ds 1
_buff_val::
	.ds 1
_j::
	.ds 1
_g::
	.ds 2
_ptr::
	.ds 3
_value::
	.ds 2
_res::
	.ds 2
_fillbuffer0_buff_val_1_1:
	.ds 1
_fillbuffer1_buff_val_1_1:
	.ds 1
_displaydelete_charnum0_1_1:
	.ds 2
_displaydelete_charnum1_1_1:
	.ds 2
_putstrhex_g_1_1:
	.ds 2
_putstrhex_length_1_1:
	.ds 2
_putstrbuff_ptr_1_1:
	.ds 3
_putstrbuff_count_1_1:
	.ds 2
_putstr_ptr_1_1:
	.ds 3
_putchar_x_1_1:
	.ds 2
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
_i::
	.ds 1
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function '_sdcc_external_startup'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:43: _sdcc_external_startup()
;	-----------------------------------------
;	 function _sdcc_external_startup
;	-----------------------------------------
__sdcc_external_startup:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	main.c:45: AUXR |= 0x0C;
;	genOr
	orl	_AUXR,#0x0C
;	main.c:46: return 0;
;	genRet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0000
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:74: void main()
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	main.c:77: SerialInitialize();  /*  call the serial initialize function */
;	genCall
	lcall	_SerialInitialize
;	main.c:79: init_dynamic_memory((MEMHEADER xdata *)heap, HEAP_SIZE); /* Heap allocation */
;	genAssign
	mov	dptr,#_init_dynamic_memory_PARM_2
	mov	a,#0xB8
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0B
	movx	@dptr,a
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#_heap
	lcall	_init_dynamic_memory
;	main.c:80: here: lastchar_count=0; /*Number of characters since last '?' */
00101$:
;	genAssign
	mov	dptr,#_lastchar_count
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:81: char_count=0; /*Total number of characters received */
;	genAssign
	mov	dptr,#_char_count
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:82: stor_count=0; /* Total number of storage characters received */
;	genAssign
	mov	dptr,#_stor_count
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:83: buffer_count=2; /*buffer count to allocate new buffers */
;	genAssign
	mov	dptr,#_buffer_count
	mov	a,#0x02
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	main.c:84: buffer_number=0; /*Total number of buffers active */
;	genAssign
	mov	dptr,#_buffer_number
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:86: BufferInitialize(); /*Buffer initilisation */
;	genCall
	lcall	_BufferInitialize
;	main.c:88: putstr("\n\rEnter the character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_0
	mov	b,#0x80
	lcall	_putstr
;	main.c:89: putstr("Press '>' for help menu\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	lcall	_putstr
;	main.c:90: while(1)
00128$:
;	main.c:92: temp = getchar(); /* getting character from user input */
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	genAssign
	mov	dptr,#_temp
	mov	a,r2
	movx	@dptr,a
;	main.c:93: putchar(temp);  /* put character back on the screen */
;	genAssign
;	genCast
	mov	r3,#0x00
;	genCall
	mov	dpl,r2
	mov	dph,r3
	lcall	_putchar
;	main.c:94: putstr(" ");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	lcall	_putstr
;	main.c:95: if (temp>='A' && temp<='Z') /* Check if storage character*/
;	genAssign
	mov	dptr,#_temp
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x41,00144$
00144$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00124$
;	Peephole 300	removed redundant label 00145$
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r2
	add	a,#0xff - 0x5A
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00124$
;	Peephole 300	removed redundant label 00146$
;	main.c:97: stor_count++;
;	genAssign
	mov	dptr,#_stor_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_stor_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:98: char_count++;  /* for character count */
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:99: fillbuffer0(temp); /* Send data to the buffer*/
;	genCall
	mov	dpl,r2
	lcall	_fillbuffer0
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00128$
00124$:
;	main.c:101: else if (temp>='0' && temp<='9')
;	genAssign
	mov	dptr,#_temp
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x30,00147$
00147$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00120$
;	Peephole 300	removed redundant label 00148$
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r2
	add	a,#0xff - 0x39
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00120$
;	Peephole 300	removed redundant label 00149$
;	main.c:103: stor_count++;
;	genAssign
	mov	dptr,#_stor_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_stor_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:104: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:105: fillbuffer1(temp);
;	genCall
	mov	dpl,r2
	lcall	_fillbuffer1
	ljmp	00128$
00120$:
;	main.c:107: else if (temp=='+')
;	genAssign
	mov	dptr,#_temp
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x2B,00117$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00150$
;	Peephole 300	removed redundant label 00151$
;	main.c:109: newbuffer();  /* allocate new buffer */
;	genCall
	lcall	_newbuffer
;	main.c:110: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
	ljmp	00128$
00117$:
;	main.c:112: else if (temp=='-')
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x2D,00114$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00152$
;	Peephole 300	removed redundant label 00153$
;	main.c:114: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:115: deletebuffer();   /* delete buffer*/
;	genCall
	lcall	_deletebuffer
	ljmp	00128$
00114$:
;	main.c:117: else if (temp=='?')
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x3F,00111$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00154$
;	Peephole 300	removed redundant label 00155$
;	main.c:119: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:120: displaydelete(); /*display buffer content, detailed report and empty buffer */
;	genCall
	lcall	_displaydelete
	ljmp	00128$
00111$:
;	main.c:122: else if(temp=='=')
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x3D,00108$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00156$
;	Peephole 300	removed redundant label 00157$
;	main.c:124: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:125: display(); /* just display the buffer content in a nice table structure */
;	genCall
	lcall	_display
	ljmp	00128$
00108$:
;	main.c:127: else if(temp=='>')
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x3E,00105$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00158$
;	Peephole 300	removed redundant label 00159$
;	main.c:129: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:130: help_menu();  /* de-allocate every buffer*/
;	genCall
	lcall	_help_menu
	ljmp	00128$
00105$:
;	main.c:132: else if(temp=='@')
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x40,00160$
	sjmp	00161$
00160$:
	ljmp	00128$
00161$:
;	main.c:134: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	main.c:135: freeEverything();  /* de-allocate every buffer*/
;	genCall
	lcall	_freeEverything
;	main.c:136: break;
;	main.c:141: goto here;
	ljmp	00101$
;	Peephole 259.b	removed redundant label 00130$ and ret
;
;------------------------------------------------------------
;Allocation info for local variables in function 'BufferInitialize'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:144: void BufferInitialize()
;	-----------------------------------------
;	 function BufferInitialize
;	-----------------------------------------
_BufferInitialize:
;	main.c:146: buffer[0]=0;
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_buffer
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
	inc	dptr
;	Peephole 101	removed redundant mov
	movx	@dptr,a
;	main.c:147: buffer[1]=0;
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_buffer + 0x0002)
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
	inc	dptr
;	Peephole 101	removed redundant mov
	movx	@dptr,a
;	main.c:148: bptr0=NULL;
;	genAssign
	mov	dptr,#_bptr0
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:149: bptr1=NULL;
;	genAssign
	mov	dptr,#_bptr1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:150: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:151: putstr("Input buffer size between 32 and 2800 bytes, divisible by 16\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_4
	mov	b,#0x80
	lcall	_putstr
;	main.c:152: do
00113$:
;	main.c:154: res=ReadValue();
;	genCall
	lcall	_ReadValue
	mov	r2,dpl
	mov	r3,dph
;	genAssign
	mov	dptr,#_res
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:155: if(res>31 && res<2801)
;	genAssign
	mov	ar4,r2
	mov	ar5,r3
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x1F
	subb	a,r4
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r5
;	genIfxJump
	jc	00126$
	ljmp	00109$
00126$:
;	genAssign
	mov	ar4,r2
	mov	ar5,r3
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,#0xF1
	mov	a,r5
	subb	a,#0x0A
;	genIfxJump
	jc	00127$
	ljmp	00109$
00127$:
;	main.c:157: if(res%16==0)
;	genAssign
	mov	ar4,r2
	mov	ar5,r3
;	genAnd
	mov	a,r4
	anl	a,#0x0F
;	Peephole 160.c	removed sjmp by inverse jump logic
	jz	00129$
;	Peephole 300	removed redundant label 00128$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00106$
00129$:
;	main.c:159: if ((buffer[0] = malloc(res)) == 0)  //allocate buffer0
;	genCall
	mov	dpl,r2
	mov	dph,r3
	lcall	_malloc
	mov	r2,dpl
	mov	r3,dph
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_buffer
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00102$
;	Peephole 300	removed redundant label 00130$
;	main.c:160: {putstr("malloc buffer0 failed\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_5
	mov	b,#0x80
	lcall	_putstr
00102$:
;	main.c:161: if ((buffer[1] = malloc(res)) == 0)         //allocate buffer1
;	genAssign
	mov	dptr,#_res
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	lcall	_malloc
	mov	r2,dpl
	mov	r3,dph
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_buffer + 0x0002)
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00114$
;	Peephole 300	removed redundant label 00131$
;	main.c:163: putstr("malloc buffer1 failed\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_6
	mov	b,#0x80
	lcall	_putstr
;	main.c:164: putstr("Enter smaller size for buffer\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_7
	mov	b,#0x80
	lcall	_putstr
;	main.c:165: free (buffer[0]);  // if buffer1 malloc fails, free buffer 0
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_buffer
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCast
	mov	r4,#0x0
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_free
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00114$
00106$:
;	main.c:169: putstr("\n\rEnter the correct buffer size value divisible by 16\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_8
	mov	b,#0x80
	lcall	_putstr
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00114$
00109$:
;	main.c:172: putstr("\n\rEnter the correct buffer size between 32 and 2800\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_9
	mov	b,#0x80
	lcall	_putstr
00114$:
;	main.c:173: } while((buffer[0] == 0)||(buffer[1] ==0));
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_buffer
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r3,a
;	Peephole 135	removed redundant mov
	orl	a,r2
;	genIfxJump
	jnz	00132$
	ljmp	00113$
00132$:
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_buffer + 0x0002)
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r3,a
;	Peephole 135	removed redundant mov
	orl	a,r2
;	genIfxJump
	jnz	00133$
	ljmp	00113$
00133$:
;	main.c:174: putstr("\n\rBuffer allocation successful\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_10
	mov	b,#0x80
	lcall	_putstr
;	main.c:175: buffer_number=buffer_number+2;
;	genAssign
	mov	dptr,#_buffer_number
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genPlus
	mov	dptr,#_buffer_number
;     genPlusIncr
	mov	a,#0x02
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	main.c:176: bufflen[0]=res;
;	genAssign
	mov	dptr,#_res
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_bufflen
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:177: bufflen[1]=res;         /* buffer lengths */
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_bufflen + 0x0002)
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:178: buff1ptr=buffer[0];     /* assign pointers to buffer to do certain actions */
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_buffer
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCast
	mov	dptr,#_buff1ptr
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	main.c:179: buff2ptr=buffer[1];
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_buffer + 0x0002)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genCast
	mov	dptr,#_buff2ptr
	mov	a,r4
	movx	@dptr,a
	inc	dptr
	mov	a,r5
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	main.c:180: bptr0=buffer[0];
;	genAssign
;	genCast
	mov	dptr,#_bptr0
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	main.c:181: bptr1=buffer[1];
;	genAssign
;	genCast
	mov	dptr,#_bptr1
	mov	a,r4
	movx	@dptr,a
	inc	dptr
	mov	a,r5
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	Peephole 300	removed redundant label 00116$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ReadValue'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:185: int ReadValue()
;	-----------------------------------------
;	 function ReadValue
;	-----------------------------------------
_ReadValue:
;	main.c:187: value=0;
;	genAssign
	mov	dptr,#_value
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:188: do
00110$:
;	main.c:190: i=getchar();
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	genAssign
	mov	dptr,#_i
	mov	a,r2
	movx	@dptr,a
;	main.c:191: if(i>='0'&&i<='9')          /* works only if its a number */
;	genAssign
	mov	ar3,r2
;	genCmpLt
;	genCmp
	cjne	r3,#0x30,00121$
00121$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00122$
;	genAssign
	mov	ar3,r2
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r3
	add	a,#0xff - 0x39
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00123$
;	main.c:193: value=((value*10)+(i-'0')); /* Convert character to integers */
;	genAssign
	mov	dptr,#_value
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genAssign
	mov	dptr,#__mulint_PARM_2
	mov	a,#0x0A
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	genCall
	mov	dpl,r3
	mov	dph,r4
	push	ar2
	lcall	__mulint
	mov	r3,dpl
	mov	r4,dph
	pop	ar2
;	genAssign
;	genCast
	mov	r5,#0x00
;	genMinus
	mov	a,r2
	add	a,#0xd0
	mov	r6,a
	mov	a,r5
	addc	a,#0xff
	mov	r7,a
;	genPlus
	mov	dptr,#_value
;	Peephole 236.g	used r6 instead of ar6
	mov	a,r6
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 236.g	used r7 instead of ar7
	mov	a,r7
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:194: putchar(i);
;	genCall
	mov	dpl,r2
	mov	dph,r5
	lcall	_putchar
;	main.c:195: j++;
;	genAssign
	mov	dptr,#_j
	movx	a,@dptr
	mov	r2,a
;	genPlus
	mov	dptr,#_j
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00107$:
;	main.c:197: else if(i==127)
;	genAssign
	mov	dptr,#_i
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x7F,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00124$
;	Peephole 300	removed redundant label 00125$
;	main.c:199: value=value/10;
;	genAssign
	mov	dptr,#_value
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genAssign
	mov	dptr,#__divuint_PARM_2
	mov	a,#0x0A
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	genCall
	mov	dpl,r3
	mov	dph,r4
	lcall	__divuint
	mov	a,dpl
	mov	b,dph
;	genAssign
	mov	dptr,#_value
	movx	@dptr,a
	inc	dptr
	mov	a,b
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00104$:
;	main.c:201: else if(i!=13)
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x0D,00126$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00126$:
;	main.c:202: {putstr("\n\rEnter Numbers\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	lcall	_putstr
00111$:
;	main.c:203: }while(i!=13);
;	genAssign
	mov	dptr,#_i
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x0D,00127$
	sjmp	00128$
00127$:
	ljmp	00110$
00128$:
;	main.c:204: j=0;
;	genAssign
	mov	dptr,#_j
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:205: return value;
;	genAssign
	mov	dptr,#_value
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genRet
;	Peephole 234.b	loading dph directly from a(ccumulator), r3 not set
	mov	dpl,r2
	mov	dph,a
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'newbuffer'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:208: void newbuffer()
;	-----------------------------------------
;	 function newbuffer
;	-----------------------------------------
_newbuffer:
;	main.c:210: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:211: putstr("\n\rEnter the buffer size between 20 and 400\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_12
	mov	b,#0x80
	lcall	_putstr
;	main.c:213: res = ReadValue();
;	genCall
	lcall	_ReadValue
	mov	r2,dpl
	mov	r3,dph
;	genAssign
	mov	dptr,#_res
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:214: if(res>=20 && res<=400)     /* if size is in between the mentioned size*/
;	genAssign
	mov	ar4,r2
	mov	ar5,r3
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,#0x14
	mov	a,r5
	subb	a,#0x00
;	genIfxJump
	jnc	00113$
	ljmp	00105$
00113$:
;	genAssign
	mov	ar4,r2
	mov	ar5,r3
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x90
	subb	a,r4
	mov	a,#0x01
	subb	a,r5
;	genIfxJump
	jnc	00114$
	ljmp	00105$
00114$:
;	main.c:216: if((buffer[buffer_count]=malloc(res))==0)       /* allocate buffer */
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r5,a
;	Peephole 105	removed redundant mov
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	r4,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	r5,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar4
	push	ar5
	lcall	_malloc
	mov	r2,dpl
	mov	r3,dph
	pop	ar5
	pop	ar4
;	genPointerSet
;     genFarPointerSet
	mov	dpl,r4
	mov	dph,r5
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00102$
;	Peephole 300	removed redundant label 00115$
;	main.c:217: putstr("\n\rBuffer allocation failed\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_13
	mov	b,#0x80
	lcall	_putstr
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00106$
00102$:
;	main.c:219: {putstr("\n\rBuffer Allocated\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_14
	mov	b,#0x80
	lcall	_putstr
;	main.c:220: bufflen[buffer_count]=res;                  /* take down buffer length */
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r3,a
	mov	ar4,r2
;	Peephole 177.d	removed redundant move
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_bufflen
	mov	r6,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_bufflen >> 8)
	mov	r7,a
;	genAssign
	mov	dptr,#_res
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
;	genPointerSet
;     genFarPointerSet
	mov	dpl,r6
	mov	dph,r7
	mov	a,r0
	movx	@dptr,a
	inc	dptr
	mov	a,r1
	movx	@dptr,a
;	main.c:221: printf_tiny("Buffer address and buffer number 0x%x, %d\n\r", (unsigned int) buffer[buffer_count], buffer_count);
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genCast
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	push	ar4
	push	ar5
;	genIpush
	mov	a,#__str_15
	push	acc
	mov	a,#(__str_15 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfa
	mov	sp,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00106$
00105$:
;	main.c:225: putstr("\n\rNot a valid buffer size\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_16
	mov	b,#0x80
	lcall	_putstr
00106$:
;	main.c:226: buffer_count++;         /* increment buffer number and buffer count */
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genPlus
	mov	dptr,#_buffer_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	main.c:227: buffer_number++;
;	genAssign
	mov	dptr,#_buffer_number
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genPlus
	mov	dptr,#_buffer_number
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	Peephole 300	removed redundant label 00108$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'help_menu'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:230: void help_menu()
;	-----------------------------------------
;	 function help_menu
;	-----------------------------------------
_help_menu:
;	main.c:232: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HELP MENU~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_17
	push	acc
	mov	a,#(__str_17 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:233: printf_tiny("\n\r '+' Allocate new buffer\n\r");
;	genIpush
	mov	a,#__str_18
	push	acc
	mov	a,#(__str_18 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:234: printf_tiny(" '-' Delete a buffer\n\r");
;	genIpush
	mov	a,#__str_19
	push	acc
	mov	a,#(__str_19 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:235: printf_tiny(" '@' Start over from allocating heap\n\r");
;	genIpush
	mov	a,#__str_20
	push	acc
	mov	a,#(__str_20 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:236: printf_tiny(" '?' Display all buffer details and buffer0, buffer1 contents and erase it \n\r");
;	genIpush
	mov	a,#__str_21
	push	acc
	mov	a,#(__str_21 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:237: printf_tiny(" '=' Display the contents of the buffer\n\r");
;	genIpush
	mov	a,#__str_22
	push	acc
	mov	a,#(__str_22 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:238: printf_tiny(" Enter Capital alphabets to fill buffer0\n\r");
;	genIpush
	mov	a,#__str_23
	push	acc
	mov	a,#(__str_23 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:239: printf_tiny(" Enter Numerical values to fill buffer1\n\r");
;	genIpush
	mov	a,#__str_24
	push	acc
	mov	a,#(__str_24 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:240: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HELP MENU~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_17
	push	acc
	mov	a,#(__str_17 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:241: printf_tiny("\n\r\n\r Enter the option or Character\n\r");
;	genIpush
	mov	a,#__str_25
	push	acc
	mov	a,#(__str_25 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'deletebuffer'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:246: void deletebuffer()
;	-----------------------------------------
;	 function deletebuffer
;	-----------------------------------------
_deletebuffer:
;	main.c:248: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:249: putstr("\n\rEnter the buffer number which has to be deleted\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_26
	mov	b,#0x80
	lcall	_putstr
;	main.c:250: res=ReadValue();
;	genCall
	lcall	_ReadValue
	mov	r2,dpl
	mov	r3,dph
;	genAssign
	mov	dptr,#_res
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:251: if(res>buffer_count || res==1 || res==0)    /* check if buffer number to be deleted is proper*/
;	genAssign
	mov	ar4,r2
	mov	ar5,r3
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	genCmpGt
;	genCmp
	clr	c
	mov	a,r6
	subb	a,r4
	mov	a,r7
	subb	a,r5
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00104$
;	Peephole 300	removed redundant label 00114$
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x01,00115$
	cjne	r3,#0x00,00115$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00104$
00115$:
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00105$
;	Peephole 300	removed redundant label 00116$
00104$:
;	main.c:252: putstr("Enter valid buffer number next time\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_putstr
00105$:
;	main.c:253: else if (!buffer[res])                      /* check if buffer exists */
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	a,r3
	xch	a,r2
	add	a,acc
	xch	a,r2
	rlc	a
	mov	r3,a
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r3,a
;	Peephole 135	removed redundant mov
	orl	a,r2
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00102$
;	Peephole 300	removed redundant label 00117$
;	main.c:254: putstr("Enter valid buffer number next time\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_putstr
00102$:
;	main.c:257: free (buffer[res]);             /* free buffer */
;	genAssign
;	genCast
	mov	r4,#0x0
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_free
;	main.c:258: buffer[res]=0;
;	genAssign
	mov	dptr,#_res
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r3,a
;	Peephole 105	removed redundant mov
	xch	a,r2
	add	a,acc
	xch	a,r2
	rlc	a
	mov	r3,a
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
	inc	dptr
;	Peephole 101	removed redundant mov
	movx	@dptr,a
;	main.c:259: buffer_number--;                /* decrement the number of buffers that are active */
;	genAssign
	mov	dptr,#_buffer_number
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genMinus
;	genMinusDec
	dec	r2
	cjne	r2,#0xff,00118$
	dec	r3
00118$:
;	genAssign
	mov	dptr,#_buffer_number
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:260: putstr("Buffer deleted\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_28
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'fillbuffer0'
;------------------------------------------------------------
;buff_val                  Allocated with name '_fillbuffer0_buff_val_1_1'
;sloc0                     Allocated with name '_fillbuffer0_sloc0_1_0'
;sloc1                     Allocated with name '_fillbuffer0_sloc1_1_0'
;sloc2                     Allocated with name '_fillbuffer0_sloc2_1_0'
;------------------------------------------------------------
;	main.c:266: void fillbuffer0(unsigned char buff_val)
;	-----------------------------------------
;	 function fillbuffer0
;	-----------------------------------------
_fillbuffer0:
;	genReceive
	mov	a,dpl
	mov	dptr,#_fillbuffer0_buff_val_1_1
	movx	@dptr,a
;	main.c:268: if((unsigned int)bptr0<((unsigned int)buff1ptr+(int)bufflen[0]-1)) /*check if buffer is full */
;	genAssign
	mov	dptr,#_bptr0
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genCast
	mov	ar7,r2
	mov	ar0,r3
;	genAssign
	mov	dptr,#_buff1ptr
	movx	a,@dptr
	mov	_fillbuffer0_sloc2_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_fillbuffer0_sloc2_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_fillbuffer0_sloc2_1_0 + 2),a
;	genCast
	mov	r5,_fillbuffer0_sloc2_1_0
	mov	r6,(_fillbuffer0_sloc2_1_0 + 1)
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_bufflen
	movx	a,@dptr
	mov	_fillbuffer0_sloc1_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_fillbuffer0_sloc1_1_0 + 1),a
;	genPlus
	mov	a,_fillbuffer0_sloc1_1_0
;	Peephole 236.a	used r5 instead of ar5
	add	a,r5
	mov	r5,a
	mov	a,(_fillbuffer0_sloc1_1_0 + 1)
;	Peephole 236.b	used r6 instead of ar6
	addc	a,r6
	mov	r6,a
;	genMinus
;	genMinusDec
	dec	r5
	cjne	r5,#0xff,00110$
	dec	r6
00110$:
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r7
	subb	a,r5
	mov	a,r0
	subb	a,r6
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00104$
;	Peephole 300	removed redundant label 00111$
;	main.c:270: *bptr0 = buff_val;
;	genAssign
	mov	dptr,#_fillbuffer0_buff_val_1_1
	movx	a,@dptr
;	genPointerSet
;	genGenPointerSet
	mov	r5,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 191	removed redundant mov
	lcall	__gptrput
;	main.c:271: bptr0++;
;	genPlus
	mov	dptr,#_bptr0
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
	ret
00104$:
;	main.c:273: else if((unsigned int)bptr0==((unsigned int)buff1ptr+(int)bufflen[0]-1))
;	genAssign
	mov	ar5,r2
	mov	ar6,r3
;	genCast
;	genAssign
;	peephole 177.f	removed redundant move
	mov	r7,_fillbuffer0_sloc2_1_0
	mov	r0,(_fillbuffer0_sloc2_1_0 + 1)
	mov	r1,(_fillbuffer0_sloc2_1_0 + 2)
;	genCast
;	genPlus
	mov	a,_fillbuffer0_sloc1_1_0
;	Peephole 236.a	used r7 instead of ar7
	add	a,r7
	mov	r7,a
	mov	a,(_fillbuffer0_sloc1_1_0 + 1)
;	Peephole 236.b	used r0 instead of ar0
	addc	a,r0
	mov	r0,a
;	genMinus
;	genMinusDec
	dec	r7
	cjne	r7,#0xff,00112$
	dec	r0
00112$:
;	genCmpEq
;	gencjneshort
	mov	a,r5
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 197.b	optimized misc jump sequence
	cjne	a,ar7,00106$
	mov	a,r6
	cjne	a,ar0,00106$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00113$
;	Peephole 300	removed redundant label 00114$
;	main.c:275: *bptr0=buff_val;
;	genAssign
	mov	dptr,#_fillbuffer0_buff_val_1_1
	movx	a,@dptr
;	genPointerSet
;	genGenPointerSet
	mov	r5,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 191	removed redundant mov
	lcall	__gptrput
;	main.c:276: bptr0++;
;	genPlus
	mov	dptr,#_bptr0
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	main.c:277: *bptr0='\0';
;	genAssign
	mov	dptr,#_bptr0
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPointerSet
;	genGenPointerSet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	__gptrput
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'fillbuffer1'
;------------------------------------------------------------
;buff_val                  Allocated with name '_fillbuffer1_buff_val_1_1'
;sloc0                     Allocated with name '_fillbuffer1_sloc0_1_0'
;sloc1                     Allocated with name '_fillbuffer1_sloc1_1_0'
;sloc2                     Allocated with name '_fillbuffer1_sloc2_1_0'
;------------------------------------------------------------
;	main.c:281: void fillbuffer1(unsigned char buff_val)
;	-----------------------------------------
;	 function fillbuffer1
;	-----------------------------------------
_fillbuffer1:
;	genReceive
	mov	a,dpl
	mov	dptr,#_fillbuffer1_buff_val_1_1
	movx	@dptr,a
;	main.c:283: if((unsigned int)bptr1<((unsigned int)buff2ptr+(int)bufflen[1]-1))      /*checking if buffer is full */
;	genAssign
	mov	dptr,#_bptr1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genCast
	mov	ar7,r2
	mov	ar0,r3
;	genAssign
	mov	dptr,#_buff2ptr
	movx	a,@dptr
	mov	_fillbuffer1_sloc2_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_fillbuffer1_sloc2_1_0 + 1),a
	inc	dptr
	movx	a,@dptr
	mov	(_fillbuffer1_sloc2_1_0 + 2),a
;	genCast
	mov	r5,_fillbuffer1_sloc2_1_0
	mov	r6,(_fillbuffer1_sloc2_1_0 + 1)
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_bufflen + 0x0002)
	movx	a,@dptr
	mov	_fillbuffer1_sloc1_1_0,a
	inc	dptr
	movx	a,@dptr
	mov	(_fillbuffer1_sloc1_1_0 + 1),a
;	genPlus
	mov	a,_fillbuffer1_sloc1_1_0
;	Peephole 236.a	used r5 instead of ar5
	add	a,r5
	mov	r5,a
	mov	a,(_fillbuffer1_sloc1_1_0 + 1)
;	Peephole 236.b	used r6 instead of ar6
	addc	a,r6
	mov	r6,a
;	genMinus
;	genMinusDec
	dec	r5
	cjne	r5,#0xff,00110$
	dec	r6
00110$:
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r7
	subb	a,r5
	mov	a,r0
	subb	a,r6
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00104$
;	Peephole 300	removed redundant label 00111$
;	main.c:285: *bptr1 = buff_val;
;	genAssign
	mov	dptr,#_fillbuffer1_buff_val_1_1
	movx	a,@dptr
;	genPointerSet
;	genGenPointerSet
	mov	r5,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 191	removed redundant mov
	lcall	__gptrput
;	main.c:286: bptr1++;
;	genPlus
	mov	dptr,#_bptr1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
	ret
00104$:
;	main.c:288: else if((unsigned int)bptr1==((unsigned int)buff2ptr+(int)bufflen[1]-1))
;	genAssign
	mov	ar5,r2
	mov	ar6,r3
;	genCast
;	genAssign
;	peephole 177.f	removed redundant move
	mov	r7,_fillbuffer1_sloc2_1_0
	mov	r0,(_fillbuffer1_sloc2_1_0 + 1)
	mov	r1,(_fillbuffer1_sloc2_1_0 + 2)
;	genCast
;	genPlus
	mov	a,_fillbuffer1_sloc1_1_0
;	Peephole 236.a	used r7 instead of ar7
	add	a,r7
	mov	r7,a
	mov	a,(_fillbuffer1_sloc1_1_0 + 1)
;	Peephole 236.b	used r0 instead of ar0
	addc	a,r0
	mov	r0,a
;	genMinus
;	genMinusDec
	dec	r7
	cjne	r7,#0xff,00112$
	dec	r0
00112$:
;	genCmpEq
;	gencjneshort
	mov	a,r5
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 197.b	optimized misc jump sequence
	cjne	a,ar7,00106$
	mov	a,r6
	cjne	a,ar0,00106$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00113$
;	Peephole 300	removed redundant label 00114$
;	main.c:290: *bptr1=buff_val;
;	genAssign
	mov	dptr,#_fillbuffer1_buff_val_1_1
	movx	a,@dptr
;	genPointerSet
;	genGenPointerSet
	mov	r5,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 191	removed redundant mov
	lcall	__gptrput
;	main.c:291: bptr1++;
;	genPlus
	mov	dptr,#_bptr1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	main.c:292: *bptr1='\0';
;	genAssign
	mov	dptr,#_bptr1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPointerSet
;	genGenPointerSet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 253.c	replaced lcall with ljmp
	ljmp	__gptrput
00106$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'freeEverything'
;------------------------------------------------------------
;counter                   Allocated with name '_freeEverything_counter_1_1'
;------------------------------------------------------------
;	main.c:298: void freeEverything()
;	-----------------------------------------
;	 function freeEverything
;	-----------------------------------------
_freeEverything:
;	main.c:301: for (counter=0; counter<buffer_count; counter++)
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
00103$:
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r2
	subb	a,r4
	mov	a,r3
	subb	a,r5
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00107$
;	Peephole 300	removed redundant label 00113$
;	main.c:303: if(buffer[counter])
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	ar4,r2
	mov	a,r3
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r5,a
;	Peephole 135	removed redundant mov
	orl	a,r4
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00105$
;	Peephole 300	removed redundant label 00114$
;	main.c:305: free (buffer[counter]);                 /*Delete Every buffer */
;	genAssign
;	genCast
	mov	r6,#0x0
;	genCall
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	push	ar2
	push	ar3
	lcall	_free
	pop	ar3
	pop	ar2
;	main.c:306: printf_tiny("Deleted %d buffer\n\r",counter);
;	genIpush
	push	ar2
	push	ar3
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_29
	push	acc
	mov	a,#(__str_29 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
00105$:
;	main.c:301: for (counter=0; counter<buffer_count; counter++)
;	genPlus
;     genPlusIncr
	inc	r2
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 243	avoided branch to sjmp
	cjne	r2,#0x00,00103$
	inc	r3
;	Peephole 300	removed redundant label 00115$
	sjmp	00103$
00107$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'displaydelete'
;------------------------------------------------------------
;len                       Allocated with name '_displaydelete_len_1_1'
;charnum0                  Allocated with name '_displaydelete_charnum0_1_1'
;charnum1                  Allocated with name '_displaydelete_charnum1_1_1'
;------------------------------------------------------------
;	main.c:312: void displaydelete()
;	-----------------------------------------
;	 function displaydelete
;	-----------------------------------------
_displaydelete:
;	main.c:314: unsigned int len=0, charnum0=0, charnum1=0 ;
;	genAssign
	mov	dptr,#_displaydelete_charnum0_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	genAssign
	mov	dptr,#_displaydelete_charnum1_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:315: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:316: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYING DETAILS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_30
	push	acc
	mov	a,#(__str_30 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:317: printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_31
	push	acc
	mov	a,#(__str_31 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:318: printf_tiny("\n\rTotal Number of Buffer = %d\n\r", buffer_number);
;	genIpush
	mov	dptr,#_buffer_number
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
;	genIpush
	mov	a,#__str_32
	push	acc
	mov	a,#(__str_32 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:319: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:320: printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_31
	push	acc
	mov	a,#(__str_31 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:321: printf_tiny("\n\rTotal number of characters received = %d\n\r", char_count);
;	genIpush
	mov	dptr,#_char_count
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
;	genIpush
	mov	a,#__str_33
	push	acc
	mov	a,#(__str_33 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:322: printf_tiny("Total number of storage characters received = %d\n\r", stor_count);
;	genIpush
	mov	dptr,#_stor_count
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
;	genIpush
	mov	a,#__str_34
	push	acc
	mov	a,#(__str_34 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:323: printf_tiny("Total number of characters received since last '?' = %d\n\r", (char_count-lastchar_count));
;	genAssign
	mov	dptr,#_lastchar_count
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genMinus
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	mov	r2,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	mov	r3,a
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_35
	push	acc
	mov	a,#(__str_35 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:324: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_36
	push	acc
	mov	a,#(__str_36 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:325: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:326: lastchar_count=char_count;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_lastchar_count
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:327: while((unsigned char)*buff1ptr!='\0')           /* calculate number of characters in buffer0*/
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
00101$:
;	genAssign
	mov	dptr,#_buff1ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
	mov	r7,a
;	Peephole 115.b	jump optimization
	jz	00122$
;	Peephole 300	removed redundant label 00125$
;	main.c:329: charnum0++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00126$
	inc	r3
00126$:
;	genAssign
	mov	dptr,#_displaydelete_charnum0_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:330: (unsigned char)buff1ptr++;
;	genPlus
	mov	dptr,#_buff1ptr
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r4 instead of ar4
	add	a,r4
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r5 instead of ar5
	addc	a,r5
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00122$:
;	genAssign
	mov	dptr,#_displaydelete_charnum0_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:332: printf_tiny("\n\rNumber of characters in buffer0 = %d\n\r", charnum0);
;	genIpush
	push	ar2
	push	ar3
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_37
	push	acc
	mov	a,#(__str_37 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
;	main.c:333: printf_tiny("Number of free spaces in buffer0 = %d\n\r", (bufflen[0]-charnum0));
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_bufflen
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genMinus
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	mov	r4,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	mov	r5,a
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
;	genIpush
	mov	a,#__str_38
	push	acc
	mov	a,#(__str_38 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
;	main.c:334: buff1ptr=buff1ptr-charnum0;
;	genAssign
	mov	dptr,#_buff1ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genMinus
	mov	dptr,#_buff1ptr
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	movx	@dptr,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	main.c:335: while((unsigned char)*buff2ptr!='\0')           /* calculate number of characters in buffer1*/
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
00104$:
;	genAssign
	mov	dptr,#_buff2ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
	mov	r7,a
;	Peephole 115.b	jump optimization
	jz	00123$
;	Peephole 300	removed redundant label 00127$
;	main.c:337: charnum1++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00128$
	inc	r3
00128$:
;	genAssign
	mov	dptr,#_displaydelete_charnum1_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:338: (unsigned char)buff2ptr++;
;	genPlus
	mov	dptr,#_buff2ptr
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r4 instead of ar4
	add	a,r4
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r5 instead of ar5
	addc	a,r5
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00104$
00123$:
;	genAssign
	mov	dptr,#_displaydelete_charnum1_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:340: printf_tiny("Number of characters in buffer1 = %d\n\r", charnum1);
;	genIpush
	push	ar2
	push	ar3
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_39
	push	acc
	mov	a,#(__str_39 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
;	main.c:341: printf_tiny("Number of free spaces in buffer0 = %d\n\r", (bufflen[1]-charnum1));
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_bufflen + 0x0002)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genMinus
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	mov	r4,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	mov	r5,a
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
;	genIpush
	mov	a,#__str_38
	push	acc
	mov	a,#(__str_38 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
;	main.c:342: buff2ptr=buff2ptr-charnum1;
;	genAssign
	mov	dptr,#_buff2ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genMinus
	mov	dptr,#_buff2ptr
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	movx	@dptr,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	main.c:343: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:344: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_36
	push	acc
	mov	a,#(__str_36 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:345: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:346: for(len=0; len<buffer_count; len++)
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
00109$:
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r2
	subb	a,r4
	mov	a,r3
	subb	a,r5
;	genIfxJump
	jc	00129$
	ljmp	00112$
00129$:
;	main.c:348: if(buffer[len])
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	ar4,r2
	mov	a,r3
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r7,a
;	Peephole 135	removed redundant mov
	orl	a,r6
;	genIfxJump
	jnz	00130$
	ljmp	00111$
00130$:
;	main.c:350: printf_tiny("Buffer %d Information\n\r", len);
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_40
	push	acc
	mov	a,#(__str_40 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:351: printf_tiny("Starting address = 0x%x\n\r",(unsigned int)buffer[len]);
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	genCast
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_41
	push	acc
	mov	a,#(__str_41 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:352: printf_tiny("Ending address = 0x%x\n\r",((unsigned int)buffer[len]+(unsigned int)bufflen[len]));
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	genCast
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_bufflen
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_bufflen >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
;	genPlus
;	Peephole 236.g	used r0 instead of ar0
	mov	a,r0
;	Peephole 236.a	used r6 instead of ar6
	add	a,r6
	mov	r6,a
;	Peephole 236.g	used r1 instead of ar1
	mov	a,r1
;	Peephole 236.b	used r7 instead of ar7
	addc	a,r7
	mov	r7,a
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_42
	push	acc
	mov	a,#(__str_42 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:353: printf_tiny("Allocated size = %d\n\r", bufflen[len]);
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_bufflen
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_bufflen >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
;	genIpush
	mov	a,#__str_43
	push	acc
	mov	a,#(__str_43 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
;	main.c:354: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
00111$:
;	main.c:346: for(len=0; len<buffer_count; len++)
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00131$
	inc	r3
00131$:
	ljmp	00109$
00112$:
;	main.c:357: printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_31
	push	acc
	mov	a,#(__str_31 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:358: putstr("\n\rContents of Buffer0\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_44
	mov	b,#0x80
	lcall	_putstr
;	main.c:359: putstrbuff(buff1ptr);               /* Displays content */
;	genAssign
	mov	dptr,#_buff1ptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_putstrbuff
;	main.c:360: bptr0=bptr0-charnum0;               /*Move back the pointer to original after erasing buffer*/
;	genAssign
	mov	dptr,#_displaydelete_charnum0_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_bptr0
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genMinus
	mov	dptr,#_bptr0
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	movx	@dptr,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	main.c:361: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:362: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_36
	push	acc
	mov	a,#(__str_36 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:363: putstr("\n\rContents of Buffer1\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_45
	mov	b,#0x80
	lcall	_putstr
;	main.c:364: putstrbuff(buff2ptr);               /*Displays content*/
;	genAssign
	mov	dptr,#_buff2ptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_putstrbuff
;	main.c:365: bptr1=bptr1-charnum1;               /*Move back the pointer to original after erasing buffer*/
;	genAssign
	mov	dptr,#_displaydelete_charnum1_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_bptr1
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genMinus
	mov	dptr,#_bptr1
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	movx	@dptr,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	main.c:366: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:367: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_36
	push	acc
	mov	a,#(__str_36 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:368: printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYED ALL DETAILS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genIpush
	mov	a,#__str_46
	push	acc
	mov	a,#(__str_46 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'display'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:373: void display()
;	-----------------------------------------
;	 function display
;	-----------------------------------------
_display:
;	main.c:375: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:376: putstr("\n\rContents of Buffer0\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_44
	mov	b,#0x80
	lcall	_putstr
;	main.c:377: putstrhex(0);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0000
	lcall	_putstrhex
;	main.c:378: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:379: putstr("Contents of Buffer1\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_47
	mov	b,#0x80
	lcall	_putstr
;	main.c:380: putstrhex(1);
;	genCall
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0001
	lcall	_putstrhex
;	main.c:381: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'putstrhex'
;------------------------------------------------------------
;sloc0                     Allocated with name '_putstrhex_sloc0_1_0'
;sloc1                     Allocated with name '_putstrhex_sloc1_1_0'
;sloc2                     Allocated with name '_putstrhex_sloc2_1_0'
;sloc3                     Allocated with name '_putstrhex_sloc3_1_0'
;g                         Allocated with name '_putstrhex_g_1_1'
;count                     Allocated with name '_putstrhex_count_1_1'
;length                    Allocated with name '_putstrhex_length_1_1'
;------------------------------------------------------------
;	main.c:384: void putstrhex(unsigned int g)
;	-----------------------------------------
;	 function putstrhex
;	-----------------------------------------
_putstrhex:
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_putstrhex_g_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:387: unsigned int length=0;
;	genAssign
	mov	dptr,#_putstrhex_length_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:388: while(*buffer[g])
;	genAssign
	mov	dptr,#_putstrhex_g_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r3,a
;	Peephole 105	removed redundant mov
	xch	a,r2
	add	a,acc
	xch	a,r2
	rlc	a
	mov	r3,a
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_buffer
	mov	_putstrhex_sloc3_1_0,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,#(_buffer >> 8)
	mov	(_putstrhex_sloc3_1_0 + 1),a
;	genAssign
	mov	r6,_putstrhex_sloc3_1_0
	mov	r7,(_putstrhex_sloc3_1_0 + 1)
00108$:
;	genPointerGet
;	genFarPointerGet
	mov	dpl,r6
	mov	dph,r7
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
;	genPointerGet
;	genFarPointerGet
	mov	dpl,r0
	mov	dph,r1
	movx	a,@dptr
;	genIfxJump
	jnz	00119$
	ljmp	00110$
00119$:
;	main.c:390: printf("0x%04x:  ",((unsigned int)buffer[g]));          /*Displaying address. hex value of 16 characters in a line*/
;	genIpush
	push	ar6
	push	ar7
;	genAssign
	mov	ar6,r0
	mov	ar7,r1
;	genCast
;	genIpush
	push	ar2
	push	ar3
	push	ar6
	push	ar7
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_48
	push	acc
	mov	a,#(__str_48 >> 8)
	push	acc
	mov	a,#0x80
	push	acc
;	genCall
	lcall	_printf
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
	pop	ar7
	pop	ar6
	pop	ar3
	pop	ar2
;	main.c:408: putstr("\n\r");
;	genIpop
	pop	ar7
	pop	ar6
;	main.c:391: for(count=1; ((count%17)!=0); count++)
;	genAssign
	mov	dptr,#_putstrhex_length_1_1
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
;	genAssign
	mov	_putstrhex_sloc1_1_0,#0x01
	clr	a
	mov	(_putstrhex_sloc1_1_0 + 1),a
00104$:
;	genIpush
	push	ar6
	push	ar7
;	genAssign
	mov	dptr,#__moduint_PARM_2
	mov	a,#0x11
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	genCall
	mov	dpl,_putstrhex_sloc1_1_0
	mov	dph,(_putstrhex_sloc1_1_0 + 1)
	push	ar2
	push	ar3
	push	ar0
	push	ar1
	lcall	__moduint
	mov	r6,dpl
	mov	r7,dph
	pop	ar1
	pop	ar0
	pop	ar3
	pop	ar2
;	genCmpEq
;	gencjne
;	gencjneshort
;	Peephole 241.c	optimized compare
	clr	a
	cjne	r6,#0x00,00120$
	cjne	r7,#0x00,00120$
	inc	a
00120$:
;	Peephole 300	removed redundant label 00121$
;	genIpop
	pop	ar7
	pop	ar6
;	genIfx
;	genIfxJump
	jz	00122$
	ljmp	00118$
00122$:
;	main.c:393: if(*buffer[g])
;	genIpush
	push	ar6
	push	ar7
;	genPointerGet
;	genFarPointerGet
	mov	dpl,_putstrhex_sloc3_1_0
	mov	dph,(_putstrhex_sloc3_1_0 + 1)
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	genPointerGet
;	genFarPointerGet
	mov	dpl,r6
	mov	dph,r7
	movx	a,@dptr
	mov	r4,a
;	genIpop
	pop	ar7
	pop	ar6
;	genIfx
	mov	a,r4
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00118$
;	Peephole 300	removed redundant label 00123$
;	main.c:395: printf_tiny("%x ", *buffer[g]);
;	genIpush
	push	ar6
	push	ar7
;	genCast
	mov	ar6,r4
	mov	r7,#0x00
;	genIpush
	push	ar2
	push	ar3
	push	ar6
	push	ar7
	push	ar0
	push	ar1
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_49
	push	acc
	mov	a,#(__str_49 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar1
	pop	ar0
	pop	ar7
	pop	ar6
	pop	ar3
	pop	ar2
;	main.c:396: buffer[g]++;
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_buffer
	mov	r6,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,#(_buffer >> 8)
	mov	r7,a
;	genPointerGet
;	genFarPointerGet
	mov	dpl,r6
	mov	dph,r7
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genPlus
;     genPlusIncr
	inc	r4
	cjne	r4,#0x00,00124$
	inc	r5
00124$:
;	genPointerSet
;     genFarPointerSet
	mov	dpl,r6
	mov	dph,r7
	mov	a,r4
	movx	@dptr,a
	inc	dptr
	mov	a,r5
	movx	@dptr,a
;	main.c:397: length++;
;	genPlus
;     genPlusIncr
	inc	r0
	cjne	r0,#0x00,00125$
	inc	r1
00125$:
;	main.c:391: for(count=1; ((count%17)!=0); count++)
;	genPlus
;     genPlusIncr
	inc	_putstrhex_sloc1_1_0
	clr	a
	cjne	a,_putstrhex_sloc1_1_0,00126$
	inc	(_putstrhex_sloc1_1_0 + 1)
00126$:
;	genIpop
	pop	ar7
	pop	ar6
	ljmp	00104$
00118$:
;	genAssign
	mov	dptr,#_putstrhex_length_1_1
	mov	a,r0
	movx	@dptr,a
	inc	dptr
	mov	a,r1
	movx	@dptr,a
;	main.c:404: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	push	ar2
	push	ar3
	push	ar6
	push	ar7
	lcall	_putstr
	pop	ar7
	pop	ar6
	pop	ar3
	pop	ar2
	ljmp	00108$
00110$:
;	main.c:407: buffer[g]=buffer[g]-length;
;	genAssign
	mov	dptr,#_putstrhex_length_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genMinus
	mov	a,r0
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	mov	r0,a
	mov	a,r1
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	mov	r1,a
;	genPointerSet
;     genFarPointerSet
	mov	dpl,r6
	mov	dph,r7
	mov	a,r0
	movx	@dptr,a
	inc	dptr
	mov	a,r1
	movx	@dptr,a
;	main.c:408: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'putstrbuff'
;------------------------------------------------------------
;ptr                       Allocated with name '_putstrbuff_ptr_1_1'
;count                     Allocated with name '_putstrbuff_count_1_1'
;------------------------------------------------------------
;	main.c:413: void putstrbuff(char *ptr)
;	-----------------------------------------
;	 function putstrbuff
;	-----------------------------------------
_putstrbuff:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_putstrbuff_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:415: unsigned int count=0;
;	genAssign
	mov	dptr,#_putstrbuff_count_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:416: while(*ptr)
;	genAssign
	mov	dptr,#_putstrbuff_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00103$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfxJump
	jnz	00112$
	ljmp	00111$
00112$:
;	main.c:418: putchar(*ptr);
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genCast
	mov	r5,a
;	Peephole 105	removed redundant mov
	rlc	a
	subb	a,acc
	mov	r6,a
;	genCall
	mov	dpl,r5
	mov	dph,r6
	push	ar2
	push	ar3
	push	ar4
	lcall	_putchar
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:419: count++;
;	genAssign
	mov	dptr,#_putstrbuff_count_1_1
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genPlus
	mov	dptr,#_putstrbuff_count_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r5 instead of ar5
	add	a,r5
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r6 instead of ar6
	addc	a,r6
	inc	dptr
	movx	@dptr,a
;	main.c:420: *ptr='\0';
;	genPointerSet
;	genGenPointerSet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 181	changed mov to clr
	clr	a
	lcall	__gptrput
;	main.c:421: if (count>63)               /* 64 characters in  a line*/
;	genAssign
	mov	dptr,#_putstrbuff_count_1_1
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x3F
	subb	a,r5
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r6
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00102$
;	Peephole 300	removed redundant label 00113$
;	main.c:422: {count=0;
;	genAssign
	mov	dptr,#_putstrbuff_count_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:423: printf_tiny("\n\r");}
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	mov	a,#__str_3
	push	acc
	mov	a,#(__str_3 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
	pop	ar4
	pop	ar3
	pop	ar2
00102$:
;	main.c:424: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00114$
	inc	r3
00114$:
;	genAssign
	mov	dptr,#_putstrbuff_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
	ljmp	00103$
00111$:
;	genAssign
	mov	dptr,#_putstrbuff_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00106$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putstr'
;------------------------------------------------------------
;ptr                       Allocated with name '_putstr_ptr_1_1'
;------------------------------------------------------------
;	main.c:429: void putstr(char *ptr)
;	-----------------------------------------
;	 function putstr
;	-----------------------------------------
_putstr:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_putstr_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:431: while(*ptr)
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
	mov	r5,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:433: putchar(*ptr);
;	genCast
	mov	a,r5
	rlc	a
	subb	a,acc
	mov	r6,a
;	genCall
	mov	dpl,r5
	mov	dph,r6
	push	ar2
	push	ar3
	push	ar4
	lcall	_putchar
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:434: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00110$
	inc	r3
00110$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00108$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'SerialInitialize'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:439: void SerialInitialize()
;	-----------------------------------------
;	 function SerialInitialize
;	-----------------------------------------
_SerialInitialize:
;	main.c:441: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
;	genAssign
	mov	_TMOD,#0x20
;	main.c:442: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
;	genAssign
	mov	_TH1,#0xFD
;	main.c:443: SCON=0x50;
;	genAssign
	mov	_SCON,#0x50
;	main.c:444: TR1=1;  // to start timer
;	genAssign
	setb	_TR1
;	main.c:445: IE=0x90; // to make serial interrupt interrupt enable
;	genAssign
	mov	_IE,#0x90
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getchar'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:449: char getchar()
;	-----------------------------------------
;	 function getchar
;	-----------------------------------------
_getchar:
;	main.c:452: while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
	jnb	_RI,00101$
;	Peephole 300	removed redundant label 00108$
;	main.c:453: rec=SBUF;   // data is received in SBUF register present in controller during receiving
;	genAssign
	mov	r2,_SBUF
;	genAssign
	mov	dptr,#_rec
	mov	a,r2
	movx	@dptr,a
;	main.c:454: RI=0;  // make RI bit to 0 so that next data is received
;	genAssign
	clr	_RI
;	main.c:455: return rec;  // return rec where function is called
;	genRet
	mov	dpl,r2
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
;x                         Allocated with name '_putchar_x_1_1'
;------------------------------------------------------------
;	main.c:458: void putchar(int x)
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_putchar_x_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:460: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
;	genAssign
	mov	dptr,#_putchar_x_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCast
	mov	_SBUF,r2
;	main.c:461: while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
;	main.c:462: TI=0; // make TI bit to zero for next transmission
;	genAssign
;	Peephole 250.a	using atomic test and clear
	jbc	_TI,00108$
	sjmp	00101$
00108$:
;	Peephole 300	removed redundant label 00104$
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
__str_0:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_1:
	.ascii "Press '>' for help menu"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_2:
	.ascii " "
	.db 0x00
__str_3:
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_4:
	.ascii "Input buffer size between 32 and 2800 bytes, divisible by 16"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_5:
	.ascii "malloc buffer0 failed"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_6:
	.ascii "malloc buffer1 failed"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_7:
	.ascii "Enter smaller size for buffer"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_8:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the correct buffer size value divisible by 16"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_9:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the correct buffer size between 32 and 2800"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_10:
	.db 0x0A
	.db 0x0D
	.ascii "Buffer allocation successful"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_11:
	.db 0x0A
	.db 0x0D
	.ascii "Enter Numbers"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_12:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the buffer size between 20 and 400"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_13:
	.db 0x0A
	.db 0x0D
	.ascii "Buffer allocation failed"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_14:
	.db 0x0A
	.db 0x0D
	.ascii "Buffer Allocated"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_15:
	.ascii "Buffer address and buffer number 0x%x, %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_16:
	.db 0x0A
	.db 0x0D
	.ascii "Not a valid buffer size"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_17:
	.db 0x0A
	.db 0x0D
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HELP MENU~~~~~~~~~~~~~~~~"
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_18:
	.db 0x0A
	.db 0x0D
	.ascii " '+' Allocate new buffer"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_19:
	.ascii " '-' Delete a buffer"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_20:
	.ascii " '@' Start over from allocating heap"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_21:
	.ascii " '?' Display all buffer details and buffer0, buffer1 content"
	.ascii "s and erase it "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_22:
	.ascii " '=' Display the contents of the buffer"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_23:
	.ascii " Enter Capital alphabets to fill buffer0"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_24:
	.ascii " Enter Numerical values to fill buffer1"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_25:
	.db 0x0A
	.db 0x0D
	.db 0x0A
	.db 0x0D
	.ascii " Enter the option or Character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_26:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the buffer number which has to be deleted"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_27:
	.ascii "Enter valid buffer number next time"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_28:
	.ascii "Buffer deleted"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_29:
	.ascii "Deleted %d buffer"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_30:
	.db 0x0A
	.db 0x0D
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYING DETAILS~~~~~~~~~"
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_31:
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_32:
	.db 0x0A
	.db 0x0D
	.ascii "Total Number of Buffer = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_33:
	.db 0x0A
	.db 0x0D
	.ascii "Total number of characters received = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_34:
	.ascii "Total number of storage characters received = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_35:
	.ascii "Total number of characters received since last '?' = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_36:
	.db 0x0A
	.db 0x0D
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_37:
	.db 0x0A
	.db 0x0D
	.ascii "Number of characters in buffer0 = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_38:
	.ascii "Number of free spaces in buffer0 = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_39:
	.ascii "Number of characters in buffer1 = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_40:
	.ascii "Buffer %d Information"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_41:
	.ascii "Starting address = 0x%x"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_42:
	.ascii "Ending address = 0x%x"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_43:
	.ascii "Allocated size = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_44:
	.db 0x0A
	.db 0x0D
	.ascii "Contents of Buffer0"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_45:
	.db 0x0A
	.db 0x0D
	.ascii "Contents of Buffer1"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_46:
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYED ALL DETAILS~~~~~~~"
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_47:
	.ascii "Contents of Buffer1"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_48:
	.ascii "0x%04x:  "
	.db 0x00
__str_49:
	.ascii "%x "
	.db 0x00
	.area XINIT   (CODE)
__xinit__i:
	.db #0x00
