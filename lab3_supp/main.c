/*

  UNIVERSITY OF COLORADO BOULDER

  WRITTEN BY KIRAN NARAYANA HEGDE(kihe6592@colorado.edu)

  SUBMITTED AS PART OF LAB3 SUPPLEMENTAL ELEMENT FOR ECEN_5613.

  IMPLEMENTED A USER INTERFACE TO CONTROL THE PCA MODULES AND DIFFERENT MODES OF 8051 MICROCONTROLLER

  BITBUCKET LINK: https://bitbucket.org/kiranmadansar/ecen_5613 "ITS A PRIVATE REPOSITORY SO YOU NEED PERMISSION TO ACCESS IT".

 */

 /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
 /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


#include <mcs51/8051.h>
#include <at89c51ed2.h>
#include <stdint.h>

uint8_t value=0;
uint8_t i, j;
uint8_t ReadValue();
void SerialInitialize();
void putstr(unsigned char *);
void putchar(uint8_t);
unsigned char getchar();
unsigned char rec;


void main(void)
{
    unsigned int temp;
    value=0; i=0; j=0;
    //PCA_configure();
    SerialInitialize();
    putstr("\n\r");
    putstr("Hello \n\r");
    putstr("Press '>' for help menu\n\r");
    putstr("Enter the character \n\r");
    // Insert code
    //PCA_configure();
    while(1)
    {
        temp = getchar();
        putchar(temp);
        putstr("\n\r");
        switch(temp)
        {
            case 'E' :
                        if((CCAPM0 & 0x42)!=0x42)
                        {
                            putstr("Configuring PCA module 0 in PWM mode. Check P1.3 \n\r");
                            PWM_configure();
                        }
                        else
                        {
                            putstr("PWM already configured on P1.3 \n\r");
                        }
                        break;
            case 'D' :
                        if((CCAPM0 & 0x02)==0x02)
                        {
                            putstr("Disabling PWM mode on P1.3 \n\r");
                            PWM_disable();
                        }
                        else
                        {
                            putstr("PWM mode on P1.3 is already disabled\n\r");
                        }
                        break;
            case 'H' :
                        if((CCAPM1 & 0x4C)!= 0x4C)
                        {
                            putstr("Configuring PCA module 1 in High Speed mode. Check P1.4 \n\r");
                            HighSpeed_configure();
                        }
                        else
                        {
                            putstr("High Speed mode is already configured. Check P1.4 \n\r");
                        }
                        break;
            case 'S' :
                        if((CCAPM1 & 0x0C)==0x0C)
                        {
                            putstr("Disabling the High Speed mode on P1.4 \n\r");
                            HighSpeed_disable();
                        }
                        else
                        {
                            putstr("High Speed mode on P1.4 is already disabled \n\r");
                        }
                        break;
            case '>' :
                        help_menu();
                        break;
            case 'I' :
                        if((PCON &0x01)!=0x01)
                        {
                            putstr("Entering into Idle Mode. Press any key or generate external interrupt0 to come out of it \n\r");
                            Idle_mode();
                            putstr("Came out of Idle mode\n\r");
                        }
                        break;
            case 'P' :
                        if((PCON & 0x02)!= 0x02)
                        {
                            putstr("Entering Power-down mode. Generate external interrupt0 to come out of it \n\r");
                            Power_down();
                            putstr("Came out of power down mode\n\r");
                        }
                        break;
            case 'C' :
                        putstr("Changing the F(clkperiph) to minimum frequency\n\r");
                        Clock_frequency_min();
                        break;
            case 'N' :
                        Clock_frequency_max();
                        break;
            case 'W' :
                        putstr("Watchdog timer is set. Controller will be reset\n\r");
                        watch_dog();
                        break;
            case 'F' :
                        putstr("Going to minimum frequency mode\n\r");
                        Clock_frequency();
                        putstr("Coming back to maximum frequency mode\n\r");
                        break;
            default :
                        putstr("\n\rEnter valid input\n\r");
                        putstr("Press '>' for help menu\n\r");
        }
    }
}

void serial0(void) interrupt 4
{
    int da;
    da = SBUF;

}

void ISR_ex0(void) interrupt 0
{
    ;
}

void Clock_frequency_min()
{
    CKRL = 0x00;
}

void Clock_frequency_max()
{
    CKRL = 0xFF;
}

void watch_dog()
{
    CR=0;
    CH=0;
    CL=0;
    CCAP4L = 0xFF;
    CCAP4H = 0xFF;
    CCAPM4 = 0x4C;
    CMOD = CMOD | 0x40;
    CR=1;
}

void PWM_configure()
{
    CR=0;
    CMOD |= 0x02;
    CH = 0x00;
    CL = 0x00;
    CCAP0L = 0x4D;
    CCAP0H = 0X4D;
    CCAPM0 |= 0x42;
    CR=1;

}

void Idle_mode()
{
    IE |= 0x91;
    PCON |= 0x01;
    IE = 0x00;

}

void Power_down()
{
    IE |= 0x81;
    PCON |= 0x02;
    IE = 0x00;
}

void HighSpeed_configure()
{
    CR=0;
    CMOD |= 0x02;
    CH = 0x00;
    CL = 0x00;
    CCAP1H = 0x00;
    CCAP1L = 0x55;
    CCAPM1 = 0x4C;
    CR=1;
}

void PWM_disable()
{
    CR=0;
    CCAPM0 &= 0xFD;
    CR=1;
}

void HighSpeed_disable()
{
    CR=0;
    CCAPM1 &= 0xF3;
    CR=1;
}

void help_menu()
{
    putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    putstr("Welcome to the Help Menu\n\r");
    putstr("Press 'E' for enabling the PWM output on P1.3\n\r");
    putstr("Press 'D' for disabling the PWM output on P1.3\n\r");
    putstr("Press 'H' for enabling the High Speed output on P1.4\n\r");
    putstr("Press 'S' for disabling the High Speed output on P1.4\n\r");
    putstr("Press 'I' to enter into IDLE mode\n\r");
    putstr("Press 'P' to enter into Power down mode\n\r");
    putstr("Press 'C' to Set F(clkperiph) at the minimum frequency supported by the CKRL register\n\r");
    putstr("Press 'N' to Set F(clkperiph) at the maximum frequency supported by the CKRL register\n\r");
    putstr("Press 'F' to go into the minimum frequency for short amount of period and then come back to normal\n\r");
    putstr("Press 'W' to enable the watchdog timer\n\r");
    putstr("\n\r");
    putstr("Note: \n\r");
    putstr("1. To come out of IDLE mode, press any key on the keyboard or generate external interrupt0\n\r");
    putstr("2. To come out of Power down mode, generate external interrupt0\n\r");
    putstr("3. When you change F(clkperiph) to minimum, the baud rate changes. Hence set-\n\r");
    putstr("   -the baud rate as '18' in your serial terminal. And, when you come back to-\n\r");
    putstr("   -normal(maximum) frequency, change the baud rate to normal\n\r");
    putstr("4. Once you enable the watchdog timer, the controller gets reset within seconds\n\r");
    putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");

}

uint8_t ReadValue()
{
    do
    {
        i=getchar();
        if(i>='0'&&i<='9')          /* works only if its a number */
        {
            value=((value*10)+(i-'0')); /* Convert character to integers */
            putchar(i);
            j++;
        }
        else if(i==127)
        {
            value=value/10;
        }
        else if(i!=13)
            {putstr("\n\rEnter Numbers\n\r");}
    }while(i!=13);
    j=0;
    return value;
}

void putstr(unsigned char *ptr)
{
        while(*ptr)
        {
            putchar(*ptr);
            ptr++;
        }

}

void Clock_frequency()
{
    uint8_t i;
    CKRL=0x00;
    for(i=0; i<250; i++);
    CKRL = 0xFF;
}
void SerialInitialize()
{
    TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
    TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
    SCON=0x50;
    TR1=1;  // to start timer
    IE=0x90; // to make serial interrupt interrupt enable
}


unsigned char getchar()
{
    unsigned char rec;
    /* RI bit is present in SCON register*/
    while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
    rec=SBUF;   // data is received in SBUF register present in controller during receiving
    RI=0;  // make RI bit to 0 so that next data is received
    return rec;  // return rec where function is called
}

void putchar(uint8_t x)
{
    SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
    while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
    TI=0; // make TI bit to zero for next transmission
}
