;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.6.0 #4309 (Jul 28 2006)
; This file generated Sat Oct 28 15:18:29 2017
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _Clock_frequency
	.globl _help_menu
	.globl _HighSpeed_disable
	.globl _PWM_disable
	.globl _HighSpeed_configure
	.globl _Power_down
	.globl _Idle_mode
	.globl _PWM_configure
	.globl _watch_dog
	.globl _Clock_frequency_max
	.globl _Clock_frequency_min
	.globl _ISR_ex0
	.globl _serial0
	.globl _main
	.globl _P5_7
	.globl _P5_6
	.globl _P5_5
	.globl _P5_4
	.globl _P5_3
	.globl _P5_2
	.globl _P5_1
	.globl _P5_0
	.globl _P4_7
	.globl _P4_6
	.globl _P4_5
	.globl _P4_4
	.globl _P4_3
	.globl _P4_2
	.globl _P4_1
	.globl _P4_0
	.globl _PX0L
	.globl _PT0L
	.globl _PX1L
	.globl _PT1L
	.globl _PLS
	.globl _PT2L
	.globl _PPCL
	.globl _EC
	.globl _CCF0
	.globl _CCF1
	.globl _CCF2
	.globl _CCF3
	.globl _CCF4
	.globl _CR
	.globl _CF
	.globl _TF2
	.globl _EXF2
	.globl _RCLK
	.globl _TCLK
	.globl _EXEN2
	.globl _TR2
	.globl _C_T2
	.globl _CP_RL2
	.globl _T2CON_7
	.globl _T2CON_6
	.globl _T2CON_5
	.globl _T2CON_4
	.globl _T2CON_3
	.globl _T2CON_2
	.globl _T2CON_1
	.globl _T2CON_0
	.globl _PT2
	.globl _ET2
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _PS
	.globl _PT1
	.globl _PX1
	.globl _PT0
	.globl _PX0
	.globl _RD
	.globl _WR
	.globl _T1
	.globl _T0
	.globl _INT1
	.globl _INT0
	.globl _TXD
	.globl _RXD
	.globl _P3_7
	.globl _P3_6
	.globl _P3_5
	.globl _P3_4
	.globl _P3_3
	.globl _P3_2
	.globl _P3_1
	.globl _P3_0
	.globl _EA
	.globl _ES
	.globl _ET1
	.globl _EX1
	.globl _ET0
	.globl _EX0
	.globl _P2_7
	.globl _P2_6
	.globl _P2_5
	.globl _P2_4
	.globl _P2_3
	.globl _P2_2
	.globl _P2_1
	.globl _P2_0
	.globl _SM0
	.globl _SM1
	.globl _SM2
	.globl _REN
	.globl _TB8
	.globl _RB8
	.globl _TI
	.globl _RI
	.globl _P1_7
	.globl _P1_6
	.globl _P1_5
	.globl _P1_4
	.globl _P1_3
	.globl _P1_2
	.globl _P1_1
	.globl _P1_0
	.globl _TF1
	.globl _TR1
	.globl _TF0
	.globl _TR0
	.globl _IE1
	.globl _IT1
	.globl _IE0
	.globl _IT0
	.globl _P0_7
	.globl _P0_6
	.globl _P0_5
	.globl _P0_4
	.globl _P0_3
	.globl _P0_2
	.globl _P0_1
	.globl _P0_0
	.globl _EECON
	.globl _KBF
	.globl _KBE
	.globl _KBLS
	.globl _BRL
	.globl _BDRCON
	.globl _T2MOD
	.globl _SPDAT
	.globl _SPSTA
	.globl _SPCON
	.globl _SADEN
	.globl _SADDR
	.globl _WDTPRG
	.globl _WDTRST
	.globl _P5
	.globl _P4
	.globl _IPH1
	.globl _IPL1
	.globl _IPH0
	.globl _IPL0
	.globl _IEN1
	.globl _IEN0
	.globl _CMOD
	.globl _CL
	.globl _CH
	.globl _CCON
	.globl _CCAPM4
	.globl _CCAPM3
	.globl _CCAPM2
	.globl _CCAPM1
	.globl _CCAPM0
	.globl _CCAP4L
	.globl _CCAP3L
	.globl _CCAP2L
	.globl _CCAP1L
	.globl _CCAP0L
	.globl _CCAP4H
	.globl _CCAP3H
	.globl _CCAP2H
	.globl _CCAP1H
	.globl _CCAP0H
	.globl _CKCKON1
	.globl _CKCKON0
	.globl _CKRL
	.globl _AUXR1
	.globl _AUXR
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T2CON
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _IP
	.globl _P3
	.globl _IE
	.globl _P2
	.globl _SBUF
	.globl _SCON
	.globl _P1
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _P0
	.globl _value
	.globl _rec
	.globl _j
	.globl _i
	.globl _ReadValue
	.globl _putstr
	.globl _SerialInitialize
	.globl _getchar
	.globl _putchar
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (DATA)
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
_T2CON	=	0x00c8
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_TL2	=	0x00cc
_TH2	=	0x00cd
_AUXR	=	0x008e
_AUXR1	=	0x00a2
_CKRL	=	0x0097
_CKCKON0	=	0x008f
_CKCKON1	=	0x008f
_CCAP0H	=	0x00fa
_CCAP1H	=	0x00fb
_CCAP2H	=	0x00fc
_CCAP3H	=	0x00fd
_CCAP4H	=	0x00fe
_CCAP0L	=	0x00ea
_CCAP1L	=	0x00eb
_CCAP2L	=	0x00ec
_CCAP3L	=	0x00ed
_CCAP4L	=	0x00ee
_CCAPM0	=	0x00da
_CCAPM1	=	0x00db
_CCAPM2	=	0x00dc
_CCAPM3	=	0x00dd
_CCAPM4	=	0x00de
_CCON	=	0x00d8
_CH	=	0x00f9
_CL	=	0x00e9
_CMOD	=	0x00d9
_IEN0	=	0x00a8
_IEN1	=	0x00b1
_IPL0	=	0x00b8
_IPH0	=	0x00b7
_IPL1	=	0x00b2
_IPH1	=	0x00b3
_P4	=	0x00c0
_P5	=	0x00d8
_WDTRST	=	0x00a6
_WDTPRG	=	0x00a7
_SADDR	=	0x00a9
_SADEN	=	0x00b9
_SPCON	=	0x00c3
_SPSTA	=	0x00c4
_SPDAT	=	0x00c5
_T2MOD	=	0x00c9
_BDRCON	=	0x009b
_BRL	=	0x009a
_KBLS	=	0x009c
_KBE	=	0x009d
_KBF	=	0x009e
_EECON	=	0x00d2
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (DATA)
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
_ET2	=	0x00ad
_PT2	=	0x00bd
_T2CON_0	=	0x00c8
_T2CON_1	=	0x00c9
_T2CON_2	=	0x00ca
_T2CON_3	=	0x00cb
_T2CON_4	=	0x00cc
_T2CON_5	=	0x00cd
_T2CON_6	=	0x00ce
_T2CON_7	=	0x00cf
_CP_RL2	=	0x00c8
_C_T2	=	0x00c9
_TR2	=	0x00ca
_EXEN2	=	0x00cb
_TCLK	=	0x00cc
_RCLK	=	0x00cd
_EXF2	=	0x00ce
_TF2	=	0x00cf
_CF	=	0x00df
_CR	=	0x00de
_CCF4	=	0x00dc
_CCF3	=	0x00db
_CCF2	=	0x00da
_CCF1	=	0x00d9
_CCF0	=	0x00d8
_EC	=	0x00ae
_PPCL	=	0x00be
_PT2L	=	0x00bd
_PLS	=	0x00bc
_PT1L	=	0x00bb
_PX1L	=	0x00ba
_PT0L	=	0x00b9
_PX0L	=	0x00b8
_P4_0	=	0x00c0
_P4_1	=	0x00c1
_P4_2	=	0x00c2
_P4_3	=	0x00c3
_P4_4	=	0x00c4
_P4_5	=	0x00c5
_P4_6	=	0x00c6
_P4_7	=	0x00c7
_P5_0	=	0x00d8
_P5_1	=	0x00d9
_P5_2	=	0x00da
_P5_3	=	0x00db
_P5_4	=	0x00dc
_P5_5	=	0x00dd
_P5_6	=	0x00de
_P5_7	=	0x00df
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_i::
	.ds 1
_j::
	.ds 1
_rec::
	.ds 1
_putstr_ptr_1_1:
	.ds 3
_getchar_rec_1_1:
	.ds 1
_putchar_x_1_1:
	.ds 1
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
_value::
	.ds 1
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	ljmp	_ISR_ex0
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_serial0
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;temp                      Allocated with name '_main_temp_1_1'
;------------------------------------------------------------
;	main.c:33: void main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	main.c:36: value=0; i=0; j=0;
;	genAssign
	mov	dptr,#_value
;	Peephole 181	changed mov to clr
;	genAssign
;	Peephole 181	changed mov to clr
;	Peephole 219.a	removed redundant clear
;	genAssign
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
	mov	dptr,#_i
	movx	@dptr,a
	mov	dptr,#_j
;	Peephole 219.b	removed redundant clear
	movx	@dptr,a
;	main.c:38: SerialInitialize();
;	genCall
	lcall	_SerialInitialize
;	main.c:39: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_0
	mov	b,#0x80
	lcall	_putstr
;	main.c:40: putstr("Hello \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	lcall	_putstr
;	main.c:41: putstr("Press '>' for help menu\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	lcall	_putstr
;	main.c:42: putstr("Enter the character \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
;	main.c:45: while(1)
00131$:
;	main.c:47: temp = getchar();
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	genCast
	mov	r3,#0x00
;	main.c:48: putchar(temp);
;	genCast
	mov	ar4,r2
;	genCall
	mov	dpl,r4
	push	ar2
	push	ar3
	lcall	_putchar
	pop	ar3
	pop	ar2
;	main.c:49: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_0
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
;	main.c:50: switch(temp)
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r2
	subb	a,#0x3E
	mov	a,r3
	subb	a,#0x00
;	genIfxJump
	jnc	00144$
	ljmp	00128$
00144$:
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x57
	subb	a,r2
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r3
;	genIfxJump
	jnc	00145$
	ljmp	00128$
00145$:
;	genMinus
	mov	a,r2
	add	a,#0xc2
;	genJumpTab
	mov	r2,a
;	Peephole 105	removed redundant mov
	add	a,#(00146$-3-.)
	movc	a,@a+pc
	push	acc
	mov	a,r2
	add	a,#(00147$-3-.)
	movc	a,@a+pc
	push	acc
	ret
00146$:
	.db	00117$
	.db	00128$
	.db	00128$
	.db	00128$
	.db	00128$
	.db	00124$
	.db	00105$
	.db	00101$
	.db	00127$
	.db	00128$
	.db	00109$
	.db	00118$
	.db	00128$
	.db	00128$
	.db	00128$
	.db	00128$
	.db	00125$
	.db	00128$
	.db	00121$
	.db	00128$
	.db	00128$
	.db	00113$
	.db	00128$
	.db	00128$
	.db	00128$
	.db	00126$
00147$:
	.db	00117$>>8
	.db	00128$>>8
	.db	00128$>>8
	.db	00128$>>8
	.db	00128$>>8
	.db	00124$>>8
	.db	00105$>>8
	.db	00101$>>8
	.db	00127$>>8
	.db	00128$>>8
	.db	00109$>>8
	.db	00118$>>8
	.db	00128$>>8
	.db	00128$>>8
	.db	00128$>>8
	.db	00128$>>8
	.db	00125$>>8
	.db	00128$>>8
	.db	00121$>>8
	.db	00128$>>8
	.db	00128$>>8
	.db	00113$>>8
	.db	00128$>>8
	.db	00128$>>8
	.db	00128$>>8
	.db	00126$>>8
;	main.c:52: case 'E' :
00101$:
;	main.c:53: if((CCAPM0 & 0x42)!=0x42)
;	genAnd
	mov	a,#0x42
	anl	a,_CCAPM0
	mov	r2,a
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x42,00148$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00103$
00148$:
;	main.c:55: putstr("Configuring PCA module 0 in PWM mode. Check P1.3 \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_4
	mov	b,#0x80
	lcall	_putstr
;	main.c:56: PWM_configure();
;	genCall
	lcall	_PWM_configure
	ljmp	00131$
00103$:
;	main.c:60: putstr("PWM already configured on P1.3 \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_5
	mov	b,#0x80
	lcall	_putstr
;	main.c:62: break;
	ljmp	00131$
;	main.c:63: case 'D' :
00105$:
;	main.c:64: if((CCAPM0 & 0x02)==0x02)
;	genAnd
	mov	a,#0x02
	anl	a,_CCAPM0
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x02,00107$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00149$
;	Peephole 300	removed redundant label 00150$
;	main.c:66: putstr("Disabling PWM mode on P1.3 \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_6
	mov	b,#0x80
	lcall	_putstr
;	main.c:67: PWM_disable();
;	genCall
	lcall	_PWM_disable
	ljmp	00131$
00107$:
;	main.c:71: putstr("PWM mode on P1.3 is already disabled\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_7
	mov	b,#0x80
	lcall	_putstr
;	main.c:73: break;
	ljmp	00131$
;	main.c:74: case 'H' :
00109$:
;	main.c:75: if((CCAPM1 & 0x4C)!= 0x4C)
;	genAnd
	mov	a,#0x4C
	anl	a,_CCAPM1
	mov	r2,a
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x4C,00151$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00151$:
;	main.c:77: putstr("Configuring PCA module 1 in High Speed mode. Check P1.4 \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_8
	mov	b,#0x80
	lcall	_putstr
;	main.c:78: HighSpeed_configure();
;	genCall
	lcall	_HighSpeed_configure
	ljmp	00131$
00111$:
;	main.c:82: putstr("High Speed mode is already configured. Check P1.4 \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_9
	mov	b,#0x80
	lcall	_putstr
;	main.c:84: break;
	ljmp	00131$
;	main.c:85: case 'S' :
00113$:
;	main.c:86: if((CCAPM1 & 0x0C)==0x0C)
;	genAnd
	mov	a,#0x0C
	anl	a,_CCAPM1
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x0C,00115$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00152$
;	Peephole 300	removed redundant label 00153$
;	main.c:88: putstr("Disabling the High Speed mode on P1.4 \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_10
	mov	b,#0x80
	lcall	_putstr
;	main.c:89: HighSpeed_disable();
;	genCall
	lcall	_HighSpeed_disable
	ljmp	00131$
00115$:
;	main.c:93: putstr("High Speed mode on P1.4 is already disabled \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_11
	mov	b,#0x80
	lcall	_putstr
;	main.c:95: break;
	ljmp	00131$
;	main.c:96: case '>' :
00117$:
;	main.c:97: help_menu();
;	genCall
	lcall	_help_menu
;	main.c:98: break;
	ljmp	00131$
;	main.c:99: case 'I' :
00118$:
;	main.c:100: if((PCON &0x01)!=0x01)
;	genAnd
	mov	a,#0x01
	anl	a,_PCON
	mov	r2,a
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x01,00154$
	ljmp	00131$
00154$:
;	main.c:102: putstr("Entering into Idle Mode. Press any key or generate external interrupt0 to come out of it \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_12
	mov	b,#0x80
	lcall	_putstr
;	main.c:103: Idle_mode();
;	genCall
	lcall	_Idle_mode
;	main.c:104: putstr("Came out of Idle mode\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_13
	mov	b,#0x80
	lcall	_putstr
;	main.c:106: break;
	ljmp	00131$
;	main.c:107: case 'P' :
00121$:
;	main.c:108: if((PCON & 0x02)!= 0x02)
;	genAnd
	mov	a,#0x02
	anl	a,_PCON
	mov	r2,a
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x02,00155$
	ljmp	00131$
00155$:
;	main.c:110: putstr("Entering Power-down mode. Generate external interrupt0 to come out of it \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_14
	mov	b,#0x80
	lcall	_putstr
;	main.c:111: Power_down();
;	genCall
	lcall	_Power_down
;	main.c:112: putstr("Came out of power down mode\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_15
	mov	b,#0x80
	lcall	_putstr
;	main.c:114: break;
	ljmp	00131$
;	main.c:115: case 'C' :
00124$:
;	main.c:116: putstr("Changing the F(clkperiph) to minimum frequency\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_16
	mov	b,#0x80
	lcall	_putstr
;	main.c:117: Clock_frequency_min();
;	genCall
	lcall	_Clock_frequency_min
;	main.c:118: break;
	ljmp	00131$
;	main.c:119: case 'N' :
00125$:
;	main.c:120: Clock_frequency_max();
;	genCall
	lcall	_Clock_frequency_max
;	main.c:121: break;
	ljmp	00131$
;	main.c:122: case 'W' :
00126$:
;	main.c:123: putstr("Watchdog timer is set. Controller will be reset\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_17
	mov	b,#0x80
	lcall	_putstr
;	main.c:124: watch_dog();
;	genCall
	lcall	_watch_dog
;	main.c:125: break;
	ljmp	00131$
;	main.c:126: case 'F' :
00127$:
;	main.c:127: putstr("Going to minimum frequency mode\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_18
	mov	b,#0x80
	lcall	_putstr
;	main.c:128: Clock_frequency();
;	genCall
	lcall	_Clock_frequency
;	main.c:129: putstr("Coming back to maximum frequency mode\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_19
	mov	b,#0x80
	lcall	_putstr
;	main.c:130: break;
	ljmp	00131$
;	main.c:131: default :
00128$:
;	main.c:132: putstr("\n\rEnter valid input\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_20
	mov	b,#0x80
	lcall	_putstr
;	main.c:133: putstr("Press '>' for help menu\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	lcall	_putstr
;	main.c:134: }
	ljmp	00131$
;	Peephole 259.b	removed redundant label 00133$ and ret
;
;------------------------------------------------------------
;Allocation info for local variables in function 'serial0'
;------------------------------------------------------------
;da                        Allocated with name '_serial0_da_1_1'
;------------------------------------------------------------
;	main.c:138: void serial0(void) interrupt 4
;	-----------------------------------------
;	 function serial0
;	-----------------------------------------
_serial0:
	push	acc
;	main.c:141: da = SBUF;
;	genDummyRead
	mov	a,_SBUF
;	Peephole 300	removed redundant label 00101$
	pop	acc
	reti
;	eliminated unneeded push/pop psw
;	eliminated unneeded push/pop dpl
;	eliminated unneeded push/pop dph
;	eliminated unneeded push/pop b
;------------------------------------------------------------
;Allocation info for local variables in function 'ISR_ex0'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:145: void ISR_ex0(void) interrupt 0
;	-----------------------------------------
;	 function ISR_ex0
;	-----------------------------------------
_ISR_ex0:
;	main.c:148: }
;	Peephole 300	removed redundant label 00101$
	reti
;	eliminated unneeded push/pop psw
;	eliminated unneeded push/pop dpl
;	eliminated unneeded push/pop dph
;	eliminated unneeded push/pop b
;	eliminated unneeded push/pop acc
;------------------------------------------------------------
;Allocation info for local variables in function 'Clock_frequency_min'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:150: void Clock_frequency_min()
;	-----------------------------------------
;	 function Clock_frequency_min
;	-----------------------------------------
_Clock_frequency_min:
;	main.c:152: CKRL = 0x00;
;	genAssign
	mov	_CKRL,#0x00
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Clock_frequency_max'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:155: void Clock_frequency_max()
;	-----------------------------------------
;	 function Clock_frequency_max
;	-----------------------------------------
_Clock_frequency_max:
;	main.c:157: CKRL = 0xFF;
;	genAssign
	mov	_CKRL,#0xFF
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'watch_dog'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:160: void watch_dog()
;	-----------------------------------------
;	 function watch_dog
;	-----------------------------------------
_watch_dog:
;	main.c:162: CR=0;
;	genAssign
	clr	_CR
;	main.c:163: CH=0;
;	genAssign
	mov	_CH,#0x00
;	main.c:164: CL=0;
;	genAssign
	mov	_CL,#0x00
;	main.c:165: CCAP4L = 0xFF;
;	genAssign
	mov	_CCAP4L,#0xFF
;	main.c:166: CCAP4H = 0xFF;
;	genAssign
	mov	_CCAP4H,#0xFF
;	main.c:167: CCAPM4 = 0x4C;
;	genAssign
	mov	_CCAPM4,#0x4C
;	main.c:168: CMOD = CMOD | 0x40;
;	genOr
	orl	_CMOD,#0x40
;	main.c:169: CR=1;
;	genAssign
	setb	_CR
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'PWM_configure'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:172: void PWM_configure()
;	-----------------------------------------
;	 function PWM_configure
;	-----------------------------------------
_PWM_configure:
;	main.c:174: CR=0;
;	genAssign
	clr	_CR
;	main.c:175: CMOD |= 0x02;
;	genOr
	orl	_CMOD,#0x02
;	main.c:176: CH = 0x00;
;	genAssign
	mov	_CH,#0x00
;	main.c:177: CL = 0x00;
;	genAssign
	mov	_CL,#0x00
;	main.c:178: CCAP0L = 0x4D;
;	genAssign
	mov	_CCAP0L,#0x4D
;	main.c:179: CCAP0H = 0X4D;
;	genAssign
	mov	_CCAP0H,#0x4D
;	main.c:180: CCAPM0 |= 0x42;
;	genOr
	orl	_CCAPM0,#0x42
;	main.c:181: CR=1;
;	genAssign
	setb	_CR
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Idle_mode'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:185: void Idle_mode()
;	-----------------------------------------
;	 function Idle_mode
;	-----------------------------------------
_Idle_mode:
;	main.c:187: IE |= 0x91;
;	genOr
	orl	_IE,#0x91
;	main.c:188: PCON |= 0x01;
;	genOr
	orl	_PCON,#0x01
;	main.c:189: IE = 0x00;
;	genAssign
	mov	_IE,#0x00
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Power_down'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:193: void Power_down()
;	-----------------------------------------
;	 function Power_down
;	-----------------------------------------
_Power_down:
;	main.c:195: IE |= 0x81;
;	genOr
	orl	_IE,#0x81
;	main.c:196: PCON |= 0x02;
;	genOr
	orl	_PCON,#0x02
;	main.c:197: IE = 0x00;
;	genAssign
	mov	_IE,#0x00
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'HighSpeed_configure'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:200: void HighSpeed_configure()
;	-----------------------------------------
;	 function HighSpeed_configure
;	-----------------------------------------
_HighSpeed_configure:
;	main.c:202: CR=0;
;	genAssign
	clr	_CR
;	main.c:203: CMOD |= 0x02;
;	genOr
	orl	_CMOD,#0x02
;	main.c:204: CH = 0x00;
;	genAssign
	mov	_CH,#0x00
;	main.c:205: CL = 0x00;
;	genAssign
	mov	_CL,#0x00
;	main.c:206: CCAP1H = 0x00;
;	genAssign
	mov	_CCAP1H,#0x00
;	main.c:207: CCAP1L = 0x55;
;	genAssign
	mov	_CCAP1L,#0x55
;	main.c:208: CCAPM1 = 0x4C;
;	genAssign
	mov	_CCAPM1,#0x4C
;	main.c:209: CR=1;
;	genAssign
	setb	_CR
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'PWM_disable'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:212: void PWM_disable()
;	-----------------------------------------
;	 function PWM_disable
;	-----------------------------------------
_PWM_disable:
;	main.c:214: CR=0;
;	genAssign
	clr	_CR
;	main.c:215: CCAPM0 &= 0xFD;
;	genAnd
	anl	_CCAPM0,#0xFD
;	main.c:216: CR=1;
;	genAssign
	setb	_CR
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'HighSpeed_disable'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:219: void HighSpeed_disable()
;	-----------------------------------------
;	 function HighSpeed_disable
;	-----------------------------------------
_HighSpeed_disable:
;	main.c:221: CR=0;
;	genAssign
	clr	_CR
;	main.c:222: CCAPM1 &= 0xF3;
;	genAnd
	anl	_CCAPM1,#0xF3
;	main.c:223: CR=1;
;	genAssign
	setb	_CR
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'help_menu'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:226: void help_menu()
;	-----------------------------------------
;	 function help_menu
;	-----------------------------------------
_help_menu:
;	main.c:228: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_21
	mov	b,#0x80
	lcall	_putstr
;	main.c:229: putstr("Welcome to the Help Menu\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_22
	mov	b,#0x80
	lcall	_putstr
;	main.c:230: putstr("Press 'E' for enabling the PWM output on P1.3\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_23
	mov	b,#0x80
	lcall	_putstr
;	main.c:231: putstr("Press 'D' for disabling the PWM output on P1.3\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_24
	mov	b,#0x80
	lcall	_putstr
;	main.c:232: putstr("Press 'H' for enabling the High Speed output on P1.4\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_25
	mov	b,#0x80
	lcall	_putstr
;	main.c:233: putstr("Press 'S' for disabling the High Speed output on P1.4\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_26
	mov	b,#0x80
	lcall	_putstr
;	main.c:234: putstr("Press 'I' to enter into IDLE mode\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_27
	mov	b,#0x80
	lcall	_putstr
;	main.c:235: putstr("Press 'P' to enter into Power down mode\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_28
	mov	b,#0x80
	lcall	_putstr
;	main.c:236: putstr("Press 'C' to Set F(clkperiph) at the minimum frequency supported by the CKRL register\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_29
	mov	b,#0x80
	lcall	_putstr
;	main.c:237: putstr("Press 'N' to Set F(clkperiph) at the maximum frequency supported by the CKRL register\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_30
	mov	b,#0x80
	lcall	_putstr
;	main.c:238: putstr("Press 'F' to go into the minimum frequency for short amount of period and then come back to normal\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_31
	mov	b,#0x80
	lcall	_putstr
;	main.c:239: putstr("Press 'W' to enable the watchdog timer\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_32
	mov	b,#0x80
	lcall	_putstr
;	main.c:240: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_0
	mov	b,#0x80
	lcall	_putstr
;	main.c:241: putstr("Note: \n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_33
	mov	b,#0x80
	lcall	_putstr
;	main.c:242: putstr("1. To come out of IDLE mode, press any key on the keyboard or generate external interrupt0\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_34
	mov	b,#0x80
	lcall	_putstr
;	main.c:243: putstr("2. To come out of Power down mode, generate external interrupt0\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_35
	mov	b,#0x80
	lcall	_putstr
;	main.c:244: putstr("3. When you change F(clkperiph) to minimum, the baud rate changes. Hence set-\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_36
	mov	b,#0x80
	lcall	_putstr
;	main.c:245: putstr("   -the baud rate as '18' in your serial terminal. And, when you come back to-\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_37
	mov	b,#0x80
	lcall	_putstr
;	main.c:246: putstr("   -normal(maximum) frequency, change the baud rate to normal\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_38
	mov	b,#0x80
	lcall	_putstr
;	main.c:247: putstr("4. Once you enable the watchdog timer, the controller gets reset within seconds\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_39
	mov	b,#0x80
	lcall	_putstr
;	main.c:248: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_21
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'ReadValue'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:252: uint8_t ReadValue()
;	-----------------------------------------
;	 function ReadValue
;	-----------------------------------------
_ReadValue:
;	main.c:254: do
00110$:
;	main.c:256: i=getchar();
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	genAssign
	mov	dptr,#_i
	mov	a,r2
	movx	@dptr,a
;	main.c:257: if(i>='0'&&i<='9')          /* works only if its a number */
;	genCmpLt
;	genCmp
	cjne	r2,#0x30,00121$
00121$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00122$
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r2
	add	a,#0xff - 0x39
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00107$
;	Peephole 300	removed redundant label 00123$
;	main.c:259: value=((value*10)+(i-'0')); /* Convert character to integers */
;	genAssign
	mov	dptr,#_value
	movx	a,@dptr
;	genMult
;	genMultOneByte
	mov	r3,a
;	Peephole 105	removed redundant mov
	mov	b,#0x0A
	mul	ab
	mov	r3,a
;	genMinus
	mov	a,r2
	add	a,#0xd0
;	genPlus
	mov	dptr,#_value
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	main.c:260: putchar(i);
;	genCall
	mov	dpl,r2
	lcall	_putchar
;	main.c:261: j++;
;	genAssign
	mov	dptr,#_j
	movx	a,@dptr
	mov	r2,a
;	genPlus
	mov	dptr,#_j
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00107$:
;	main.c:263: else if(i==127)
;	genAssign
	mov	dptr,#_i
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x7F,00104$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00124$
;	Peephole 300	removed redundant label 00125$
;	main.c:265: value=value/10;
;	genAssign
	mov	dptr,#_value
	movx	a,@dptr
	mov	r3,a
;	genDiv
	mov	dptr,#_value
;     genDivOneByte
	mov	b,#0x0A
	mov	a,r3
	div	ab
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00104$:
;	main.c:267: else if(i!=13)
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x0D,00126$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00111$
00126$:
;	main.c:268: {putstr("\n\rEnter Numbers\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_40
	mov	b,#0x80
	lcall	_putstr
00111$:
;	main.c:269: }while(i!=13);
;	genAssign
	mov	dptr,#_i
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x0D,00110$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00127$
;	Peephole 300	removed redundant label 00128$
;	main.c:270: j=0;
;	genAssign
	mov	dptr,#_j
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:271: return value;
;	genAssign
	mov	dptr,#_value
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putstr'
;------------------------------------------------------------
;ptr                       Allocated with name '_putstr_ptr_1_1'
;------------------------------------------------------------
;	main.c:274: void putstr(unsigned char *ptr)
;	-----------------------------------------
;	 function putstr
;	-----------------------------------------
_putstr:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_putstr_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:276: while(*ptr)
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
	mov	r5,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:278: putchar(*ptr);
;	genCall
	mov	dpl,r5
	push	ar2
	push	ar3
	push	ar4
	lcall	_putchar
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:279: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00110$
	inc	r3
00110$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00108$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Clock_frequency'
;------------------------------------------------------------
;i                         Allocated with name '_Clock_frequency_i_1_1'
;------------------------------------------------------------
;	main.c:284: void Clock_frequency()
;	-----------------------------------------
;	 function Clock_frequency
;	-----------------------------------------
_Clock_frequency:
;	main.c:287: CKRL=0x00;
;	genAssign
	mov	_CKRL,#0x00
;	main.c:288: for(i=0; i<250; i++);
;	genAssign
	mov	r2,#0xFA
00103$:
;	genDjnz
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 205	optimized misc jump sequence
	djnz	r2,00103$
;	Peephole 300	removed redundant label 00109$
;	Peephole 300	removed redundant label 00110$
;	main.c:289: CKRL = 0xFF;
;	genAssign
	mov	_CKRL,#0xFF
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'SerialInitialize'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:291: void SerialInitialize()
;	-----------------------------------------
;	 function SerialInitialize
;	-----------------------------------------
_SerialInitialize:
;	main.c:293: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
;	genAssign
	mov	_TMOD,#0x20
;	main.c:294: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
;	genAssign
	mov	_TH1,#0xFD
;	main.c:295: SCON=0x50;
;	genAssign
	mov	_SCON,#0x50
;	main.c:296: TR1=1;  // to start timer
;	genAssign
	setb	_TR1
;	main.c:297: IE=0x90; // to make serial interrupt interrupt enable
;	genAssign
	mov	_IE,#0x90
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getchar'
;------------------------------------------------------------
;rec                       Allocated with name '_getchar_rec_1_1'
;------------------------------------------------------------
;	main.c:301: unsigned char getchar()
;	-----------------------------------------
;	 function getchar
;	-----------------------------------------
_getchar:
;	main.c:305: while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
	jnb	_RI,00101$
;	Peephole 300	removed redundant label 00108$
;	main.c:306: rec=SBUF;   // data is received in SBUF register present in controller during receiving
;	genAssign
	mov	dptr,#_getchar_rec_1_1
	mov	a,_SBUF
	movx	@dptr,a
;	main.c:307: RI=0;  // make RI bit to 0 so that next data is received
;	genAssign
	clr	_RI
;	main.c:308: return rec;  // return rec where function is called
;	genAssign
	mov	dptr,#_getchar_rec_1_1
	movx	a,@dptr
;	genRet
;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
	mov	dpl,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
;x                         Allocated with name '_putchar_x_1_1'
;------------------------------------------------------------
;	main.c:311: void putchar(uint8_t x)
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
;	genReceive
	mov	a,dpl
	mov	dptr,#_putchar_x_1_1
	movx	@dptr,a
;	main.c:313: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
;	genAssign
	mov	dptr,#_putchar_x_1_1
	movx	a,@dptr
	mov	_SBUF,a
;	main.c:314: while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
;	main.c:315: TI=0; // make TI bit to zero for next transmission
;	genAssign
;	Peephole 250.a	using atomic test and clear
	jbc	_TI,00108$
	sjmp	00101$
00108$:
;	Peephole 300	removed redundant label 00104$
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
__str_0:
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_1:
	.ascii "Hello "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_2:
	.ascii "Press '>' for help menu"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_3:
	.ascii "Enter the character "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_4:
	.ascii "Configuring PCA module 0 in PWM mode. Check P1.3 "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_5:
	.ascii "PWM already configured on P1.3 "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_6:
	.ascii "Disabling PWM mode on P1.3 "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_7:
	.ascii "PWM mode on P1.3 is already disabled"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_8:
	.ascii "Configuring PCA module 1 in High Speed mode. Check P1.4 "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_9:
	.ascii "High Speed mode is already configured. Check P1.4 "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_10:
	.ascii "Disabling the High Speed mode on P1.4 "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_11:
	.ascii "High Speed mode on P1.4 is already disabled "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_12:
	.ascii "Entering into Idle Mode. Press any key or generate external "
	.ascii "interrupt0 to come out of it "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_13:
	.ascii "Came out of Idle mode"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_14:
	.ascii "Entering Power-down mode. Generate external interrupt0 to co"
	.ascii "me out of it "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_15:
	.ascii "Came out of power down mode"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_16:
	.ascii "Changing the F(clkperiph) to minimum frequency"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_17:
	.ascii "Watchdog timer is set. Controller will be reset"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_18:
	.ascii "Going to minimum frequency mode"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_19:
	.ascii "Coming back to maximum frequency mode"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_20:
	.db 0x0A
	.db 0x0D
	.ascii "Enter valid input"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_21:
	.db 0x0A
	.db 0x0D
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_22:
	.ascii "Welcome to the Help Menu"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_23:
	.ascii "Press 'E' for enabling the PWM output on P1.3"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_24:
	.ascii "Press 'D' for disabling the PWM output on P1.3"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_25:
	.ascii "Press 'H' for enabling the High Speed output on P1.4"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_26:
	.ascii "Press 'S' for disabling the High Speed output on P1.4"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_27:
	.ascii "Press 'I' to enter into IDLE mode"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_28:
	.ascii "Press 'P' to enter into Power down mode"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_29:
	.ascii "Press 'C' to Set F(clkperiph) at the minimum frequency suppo"
	.ascii "rted by the CKRL register"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_30:
	.ascii "Press 'N' to Set F(clkperiph) at the maximum frequency suppo"
	.ascii "rted by the CKRL register"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_31:
	.ascii "Press 'F' to go into the minimum frequency for short amount "
	.ascii "of period and then come back to normal"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_32:
	.ascii "Press 'W' to enable the watchdog timer"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_33:
	.ascii "Note: "
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_34:
	.ascii "1. To come out of IDLE mode, press any key on the keyboard o"
	.ascii "r generate external interrupt0"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_35:
	.ascii "2. To come out of Power down mode, generate external interru"
	.ascii "pt0"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_36:
	.ascii "3. When you change F(clkperiph) to minimum, the baud rate ch"
	.ascii "anges. Hence set-"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_37:
	.ascii "   -the baud rate as '18' in your serial terminal. And, when"
	.ascii " you come back to-"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_38:
	.ascii "   -normal(maximum) frequency, change the baud rate to norma"
	.ascii "l"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_39:
	.ascii "4. Once you enable the watchdog timer, the controller gets r"
	.ascii "eset within seconds"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_40:
	.db 0x0A
	.db 0x0D
	.ascii "Enter Numbers"
	.db 0x0A
	.db 0x0D
	.db 0x00
	.area XINIT   (CODE)
__xinit__value:
	.db #0x00
