                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : FreeWare ANSI-C Compiler
                              3 ; Version 2.6.0 #4309 (Jul 28 2006)
                              4 ; This file generated Sat Oct 28 15:18:29 2017
                              5 ;--------------------------------------------------------
                              6 	.module main
                              7 	.optsdcc -mmcs51 --model-large
                              8 	
                              9 ;--------------------------------------------------------
                             10 ; Public variables in this module
                             11 ;--------------------------------------------------------
                             12 	.globl _Clock_frequency
                             13 	.globl _help_menu
                             14 	.globl _HighSpeed_disable
                             15 	.globl _PWM_disable
                             16 	.globl _HighSpeed_configure
                             17 	.globl _Power_down
                             18 	.globl _Idle_mode
                             19 	.globl _PWM_configure
                             20 	.globl _watch_dog
                             21 	.globl _Clock_frequency_max
                             22 	.globl _Clock_frequency_min
                             23 	.globl _ISR_ex0
                             24 	.globl _serial0
                             25 	.globl _main
                             26 	.globl _P5_7
                             27 	.globl _P5_6
                             28 	.globl _P5_5
                             29 	.globl _P5_4
                             30 	.globl _P5_3
                             31 	.globl _P5_2
                             32 	.globl _P5_1
                             33 	.globl _P5_0
                             34 	.globl _P4_7
                             35 	.globl _P4_6
                             36 	.globl _P4_5
                             37 	.globl _P4_4
                             38 	.globl _P4_3
                             39 	.globl _P4_2
                             40 	.globl _P4_1
                             41 	.globl _P4_0
                             42 	.globl _PX0L
                             43 	.globl _PT0L
                             44 	.globl _PX1L
                             45 	.globl _PT1L
                             46 	.globl _PLS
                             47 	.globl _PT2L
                             48 	.globl _PPCL
                             49 	.globl _EC
                             50 	.globl _CCF0
                             51 	.globl _CCF1
                             52 	.globl _CCF2
                             53 	.globl _CCF3
                             54 	.globl _CCF4
                             55 	.globl _CR
                             56 	.globl _CF
                             57 	.globl _TF2
                             58 	.globl _EXF2
                             59 	.globl _RCLK
                             60 	.globl _TCLK
                             61 	.globl _EXEN2
                             62 	.globl _TR2
                             63 	.globl _C_T2
                             64 	.globl _CP_RL2
                             65 	.globl _T2CON_7
                             66 	.globl _T2CON_6
                             67 	.globl _T2CON_5
                             68 	.globl _T2CON_4
                             69 	.globl _T2CON_3
                             70 	.globl _T2CON_2
                             71 	.globl _T2CON_1
                             72 	.globl _T2CON_0
                             73 	.globl _PT2
                             74 	.globl _ET2
                             75 	.globl _CY
                             76 	.globl _AC
                             77 	.globl _F0
                             78 	.globl _RS1
                             79 	.globl _RS0
                             80 	.globl _OV
                             81 	.globl _F1
                             82 	.globl _P
                             83 	.globl _PS
                             84 	.globl _PT1
                             85 	.globl _PX1
                             86 	.globl _PT0
                             87 	.globl _PX0
                             88 	.globl _RD
                             89 	.globl _WR
                             90 	.globl _T1
                             91 	.globl _T0
                             92 	.globl _INT1
                             93 	.globl _INT0
                             94 	.globl _TXD
                             95 	.globl _RXD
                             96 	.globl _P3_7
                             97 	.globl _P3_6
                             98 	.globl _P3_5
                             99 	.globl _P3_4
                            100 	.globl _P3_3
                            101 	.globl _P3_2
                            102 	.globl _P3_1
                            103 	.globl _P3_0
                            104 	.globl _EA
                            105 	.globl _ES
                            106 	.globl _ET1
                            107 	.globl _EX1
                            108 	.globl _ET0
                            109 	.globl _EX0
                            110 	.globl _P2_7
                            111 	.globl _P2_6
                            112 	.globl _P2_5
                            113 	.globl _P2_4
                            114 	.globl _P2_3
                            115 	.globl _P2_2
                            116 	.globl _P2_1
                            117 	.globl _P2_0
                            118 	.globl _SM0
                            119 	.globl _SM1
                            120 	.globl _SM2
                            121 	.globl _REN
                            122 	.globl _TB8
                            123 	.globl _RB8
                            124 	.globl _TI
                            125 	.globl _RI
                            126 	.globl _P1_7
                            127 	.globl _P1_6
                            128 	.globl _P1_5
                            129 	.globl _P1_4
                            130 	.globl _P1_3
                            131 	.globl _P1_2
                            132 	.globl _P1_1
                            133 	.globl _P1_0
                            134 	.globl _TF1
                            135 	.globl _TR1
                            136 	.globl _TF0
                            137 	.globl _TR0
                            138 	.globl _IE1
                            139 	.globl _IT1
                            140 	.globl _IE0
                            141 	.globl _IT0
                            142 	.globl _P0_7
                            143 	.globl _P0_6
                            144 	.globl _P0_5
                            145 	.globl _P0_4
                            146 	.globl _P0_3
                            147 	.globl _P0_2
                            148 	.globl _P0_1
                            149 	.globl _P0_0
                            150 	.globl _EECON
                            151 	.globl _KBF
                            152 	.globl _KBE
                            153 	.globl _KBLS
                            154 	.globl _BRL
                            155 	.globl _BDRCON
                            156 	.globl _T2MOD
                            157 	.globl _SPDAT
                            158 	.globl _SPSTA
                            159 	.globl _SPCON
                            160 	.globl _SADEN
                            161 	.globl _SADDR
                            162 	.globl _WDTPRG
                            163 	.globl _WDTRST
                            164 	.globl _P5
                            165 	.globl _P4
                            166 	.globl _IPH1
                            167 	.globl _IPL1
                            168 	.globl _IPH0
                            169 	.globl _IPL0
                            170 	.globl _IEN1
                            171 	.globl _IEN0
                            172 	.globl _CMOD
                            173 	.globl _CL
                            174 	.globl _CH
                            175 	.globl _CCON
                            176 	.globl _CCAPM4
                            177 	.globl _CCAPM3
                            178 	.globl _CCAPM2
                            179 	.globl _CCAPM1
                            180 	.globl _CCAPM0
                            181 	.globl _CCAP4L
                            182 	.globl _CCAP3L
                            183 	.globl _CCAP2L
                            184 	.globl _CCAP1L
                            185 	.globl _CCAP0L
                            186 	.globl _CCAP4H
                            187 	.globl _CCAP3H
                            188 	.globl _CCAP2H
                            189 	.globl _CCAP1H
                            190 	.globl _CCAP0H
                            191 	.globl _CKCKON1
                            192 	.globl _CKCKON0
                            193 	.globl _CKRL
                            194 	.globl _AUXR1
                            195 	.globl _AUXR
                            196 	.globl _TH2
                            197 	.globl _TL2
                            198 	.globl _RCAP2H
                            199 	.globl _RCAP2L
                            200 	.globl _T2CON
                            201 	.globl _B
                            202 	.globl _ACC
                            203 	.globl _PSW
                            204 	.globl _IP
                            205 	.globl _P3
                            206 	.globl _IE
                            207 	.globl _P2
                            208 	.globl _SBUF
                            209 	.globl _SCON
                            210 	.globl _P1
                            211 	.globl _TH1
                            212 	.globl _TH0
                            213 	.globl _TL1
                            214 	.globl _TL0
                            215 	.globl _TMOD
                            216 	.globl _TCON
                            217 	.globl _PCON
                            218 	.globl _DPH
                            219 	.globl _DPL
                            220 	.globl _SP
                            221 	.globl _P0
                            222 	.globl _value
                            223 	.globl _rec
                            224 	.globl _j
                            225 	.globl _i
                            226 	.globl _ReadValue
                            227 	.globl _putstr
                            228 	.globl _SerialInitialize
                            229 	.globl _getchar
                            230 	.globl _putchar
                            231 ;--------------------------------------------------------
                            232 ; special function registers
                            233 ;--------------------------------------------------------
                            234 	.area RSEG    (DATA)
                    0080    235 _P0	=	0x0080
                    0081    236 _SP	=	0x0081
                    0082    237 _DPL	=	0x0082
                    0083    238 _DPH	=	0x0083
                    0087    239 _PCON	=	0x0087
                    0088    240 _TCON	=	0x0088
                    0089    241 _TMOD	=	0x0089
                    008A    242 _TL0	=	0x008a
                    008B    243 _TL1	=	0x008b
                    008C    244 _TH0	=	0x008c
                    008D    245 _TH1	=	0x008d
                    0090    246 _P1	=	0x0090
                    0098    247 _SCON	=	0x0098
                    0099    248 _SBUF	=	0x0099
                    00A0    249 _P2	=	0x00a0
                    00A8    250 _IE	=	0x00a8
                    00B0    251 _P3	=	0x00b0
                    00B8    252 _IP	=	0x00b8
                    00D0    253 _PSW	=	0x00d0
                    00E0    254 _ACC	=	0x00e0
                    00F0    255 _B	=	0x00f0
                    00C8    256 _T2CON	=	0x00c8
                    00CA    257 _RCAP2L	=	0x00ca
                    00CB    258 _RCAP2H	=	0x00cb
                    00CC    259 _TL2	=	0x00cc
                    00CD    260 _TH2	=	0x00cd
                    008E    261 _AUXR	=	0x008e
                    00A2    262 _AUXR1	=	0x00a2
                    0097    263 _CKRL	=	0x0097
                    008F    264 _CKCKON0	=	0x008f
                    008F    265 _CKCKON1	=	0x008f
                    00FA    266 _CCAP0H	=	0x00fa
                    00FB    267 _CCAP1H	=	0x00fb
                    00FC    268 _CCAP2H	=	0x00fc
                    00FD    269 _CCAP3H	=	0x00fd
                    00FE    270 _CCAP4H	=	0x00fe
                    00EA    271 _CCAP0L	=	0x00ea
                    00EB    272 _CCAP1L	=	0x00eb
                    00EC    273 _CCAP2L	=	0x00ec
                    00ED    274 _CCAP3L	=	0x00ed
                    00EE    275 _CCAP4L	=	0x00ee
                    00DA    276 _CCAPM0	=	0x00da
                    00DB    277 _CCAPM1	=	0x00db
                    00DC    278 _CCAPM2	=	0x00dc
                    00DD    279 _CCAPM3	=	0x00dd
                    00DE    280 _CCAPM4	=	0x00de
                    00D8    281 _CCON	=	0x00d8
                    00F9    282 _CH	=	0x00f9
                    00E9    283 _CL	=	0x00e9
                    00D9    284 _CMOD	=	0x00d9
                    00A8    285 _IEN0	=	0x00a8
                    00B1    286 _IEN1	=	0x00b1
                    00B8    287 _IPL0	=	0x00b8
                    00B7    288 _IPH0	=	0x00b7
                    00B2    289 _IPL1	=	0x00b2
                    00B3    290 _IPH1	=	0x00b3
                    00C0    291 _P4	=	0x00c0
                    00D8    292 _P5	=	0x00d8
                    00A6    293 _WDTRST	=	0x00a6
                    00A7    294 _WDTPRG	=	0x00a7
                    00A9    295 _SADDR	=	0x00a9
                    00B9    296 _SADEN	=	0x00b9
                    00C3    297 _SPCON	=	0x00c3
                    00C4    298 _SPSTA	=	0x00c4
                    00C5    299 _SPDAT	=	0x00c5
                    00C9    300 _T2MOD	=	0x00c9
                    009B    301 _BDRCON	=	0x009b
                    009A    302 _BRL	=	0x009a
                    009C    303 _KBLS	=	0x009c
                    009D    304 _KBE	=	0x009d
                    009E    305 _KBF	=	0x009e
                    00D2    306 _EECON	=	0x00d2
                            307 ;--------------------------------------------------------
                            308 ; special function bits
                            309 ;--------------------------------------------------------
                            310 	.area RSEG    (DATA)
                    0080    311 _P0_0	=	0x0080
                    0081    312 _P0_1	=	0x0081
                    0082    313 _P0_2	=	0x0082
                    0083    314 _P0_3	=	0x0083
                    0084    315 _P0_4	=	0x0084
                    0085    316 _P0_5	=	0x0085
                    0086    317 _P0_6	=	0x0086
                    0087    318 _P0_7	=	0x0087
                    0088    319 _IT0	=	0x0088
                    0089    320 _IE0	=	0x0089
                    008A    321 _IT1	=	0x008a
                    008B    322 _IE1	=	0x008b
                    008C    323 _TR0	=	0x008c
                    008D    324 _TF0	=	0x008d
                    008E    325 _TR1	=	0x008e
                    008F    326 _TF1	=	0x008f
                    0090    327 _P1_0	=	0x0090
                    0091    328 _P1_1	=	0x0091
                    0092    329 _P1_2	=	0x0092
                    0093    330 _P1_3	=	0x0093
                    0094    331 _P1_4	=	0x0094
                    0095    332 _P1_5	=	0x0095
                    0096    333 _P1_6	=	0x0096
                    0097    334 _P1_7	=	0x0097
                    0098    335 _RI	=	0x0098
                    0099    336 _TI	=	0x0099
                    009A    337 _RB8	=	0x009a
                    009B    338 _TB8	=	0x009b
                    009C    339 _REN	=	0x009c
                    009D    340 _SM2	=	0x009d
                    009E    341 _SM1	=	0x009e
                    009F    342 _SM0	=	0x009f
                    00A0    343 _P2_0	=	0x00a0
                    00A1    344 _P2_1	=	0x00a1
                    00A2    345 _P2_2	=	0x00a2
                    00A3    346 _P2_3	=	0x00a3
                    00A4    347 _P2_4	=	0x00a4
                    00A5    348 _P2_5	=	0x00a5
                    00A6    349 _P2_6	=	0x00a6
                    00A7    350 _P2_7	=	0x00a7
                    00A8    351 _EX0	=	0x00a8
                    00A9    352 _ET0	=	0x00a9
                    00AA    353 _EX1	=	0x00aa
                    00AB    354 _ET1	=	0x00ab
                    00AC    355 _ES	=	0x00ac
                    00AF    356 _EA	=	0x00af
                    00B0    357 _P3_0	=	0x00b0
                    00B1    358 _P3_1	=	0x00b1
                    00B2    359 _P3_2	=	0x00b2
                    00B3    360 _P3_3	=	0x00b3
                    00B4    361 _P3_4	=	0x00b4
                    00B5    362 _P3_5	=	0x00b5
                    00B6    363 _P3_6	=	0x00b6
                    00B7    364 _P3_7	=	0x00b7
                    00B0    365 _RXD	=	0x00b0
                    00B1    366 _TXD	=	0x00b1
                    00B2    367 _INT0	=	0x00b2
                    00B3    368 _INT1	=	0x00b3
                    00B4    369 _T0	=	0x00b4
                    00B5    370 _T1	=	0x00b5
                    00B6    371 _WR	=	0x00b6
                    00B7    372 _RD	=	0x00b7
                    00B8    373 _PX0	=	0x00b8
                    00B9    374 _PT0	=	0x00b9
                    00BA    375 _PX1	=	0x00ba
                    00BB    376 _PT1	=	0x00bb
                    00BC    377 _PS	=	0x00bc
                    00D0    378 _P	=	0x00d0
                    00D1    379 _F1	=	0x00d1
                    00D2    380 _OV	=	0x00d2
                    00D3    381 _RS0	=	0x00d3
                    00D4    382 _RS1	=	0x00d4
                    00D5    383 _F0	=	0x00d5
                    00D6    384 _AC	=	0x00d6
                    00D7    385 _CY	=	0x00d7
                    00AD    386 _ET2	=	0x00ad
                    00BD    387 _PT2	=	0x00bd
                    00C8    388 _T2CON_0	=	0x00c8
                    00C9    389 _T2CON_1	=	0x00c9
                    00CA    390 _T2CON_2	=	0x00ca
                    00CB    391 _T2CON_3	=	0x00cb
                    00CC    392 _T2CON_4	=	0x00cc
                    00CD    393 _T2CON_5	=	0x00cd
                    00CE    394 _T2CON_6	=	0x00ce
                    00CF    395 _T2CON_7	=	0x00cf
                    00C8    396 _CP_RL2	=	0x00c8
                    00C9    397 _C_T2	=	0x00c9
                    00CA    398 _TR2	=	0x00ca
                    00CB    399 _EXEN2	=	0x00cb
                    00CC    400 _TCLK	=	0x00cc
                    00CD    401 _RCLK	=	0x00cd
                    00CE    402 _EXF2	=	0x00ce
                    00CF    403 _TF2	=	0x00cf
                    00DF    404 _CF	=	0x00df
                    00DE    405 _CR	=	0x00de
                    00DC    406 _CCF4	=	0x00dc
                    00DB    407 _CCF3	=	0x00db
                    00DA    408 _CCF2	=	0x00da
                    00D9    409 _CCF1	=	0x00d9
                    00D8    410 _CCF0	=	0x00d8
                    00AE    411 _EC	=	0x00ae
                    00BE    412 _PPCL	=	0x00be
                    00BD    413 _PT2L	=	0x00bd
                    00BC    414 _PLS	=	0x00bc
                    00BB    415 _PT1L	=	0x00bb
                    00BA    416 _PX1L	=	0x00ba
                    00B9    417 _PT0L	=	0x00b9
                    00B8    418 _PX0L	=	0x00b8
                    00C0    419 _P4_0	=	0x00c0
                    00C1    420 _P4_1	=	0x00c1
                    00C2    421 _P4_2	=	0x00c2
                    00C3    422 _P4_3	=	0x00c3
                    00C4    423 _P4_4	=	0x00c4
                    00C5    424 _P4_5	=	0x00c5
                    00C6    425 _P4_6	=	0x00c6
                    00C7    426 _P4_7	=	0x00c7
                    00D8    427 _P5_0	=	0x00d8
                    00D9    428 _P5_1	=	0x00d9
                    00DA    429 _P5_2	=	0x00da
                    00DB    430 _P5_3	=	0x00db
                    00DC    431 _P5_4	=	0x00dc
                    00DD    432 _P5_5	=	0x00dd
                    00DE    433 _P5_6	=	0x00de
                    00DF    434 _P5_7	=	0x00df
                            435 ;--------------------------------------------------------
                            436 ; overlayable register banks
                            437 ;--------------------------------------------------------
                            438 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     439 	.ds 8
                            440 ;--------------------------------------------------------
                            441 ; internal ram data
                            442 ;--------------------------------------------------------
                            443 	.area DSEG    (DATA)
                            444 ;--------------------------------------------------------
                            445 ; overlayable items in internal ram 
                            446 ;--------------------------------------------------------
                            447 	.area OSEG    (OVR,DATA)
                            448 ;--------------------------------------------------------
                            449 ; Stack segment in internal ram 
                            450 ;--------------------------------------------------------
                            451 	.area	SSEG	(DATA)
   0008                     452 __start__stack:
   0008                     453 	.ds	1
                            454 
                            455 ;--------------------------------------------------------
                            456 ; indirectly addressable internal ram data
                            457 ;--------------------------------------------------------
                            458 	.area ISEG    (DATA)
                            459 ;--------------------------------------------------------
                            460 ; bit data
                            461 ;--------------------------------------------------------
                            462 	.area BSEG    (BIT)
                            463 ;--------------------------------------------------------
                            464 ; paged external ram data
                            465 ;--------------------------------------------------------
                            466 	.area PSEG    (PAG,XDATA)
                            467 ;--------------------------------------------------------
                            468 ; external ram data
                            469 ;--------------------------------------------------------
                            470 	.area XSEG    (XDATA)
   0000                     471 _i::
   0000                     472 	.ds 1
   0001                     473 _j::
   0001                     474 	.ds 1
   0002                     475 _rec::
   0002                     476 	.ds 1
   0003                     477 _putstr_ptr_1_1:
   0003                     478 	.ds 3
   0006                     479 _getchar_rec_1_1:
   0006                     480 	.ds 1
   0007                     481 _putchar_x_1_1:
   0007                     482 	.ds 1
                            483 ;--------------------------------------------------------
                            484 ; external initialized ram data
                            485 ;--------------------------------------------------------
                            486 	.area XISEG   (XDATA)
   0008                     487 _value::
   0008                     488 	.ds 1
                            489 	.area HOME    (CODE)
                            490 	.area GSINIT0 (CODE)
                            491 	.area GSINIT1 (CODE)
                            492 	.area GSINIT2 (CODE)
                            493 	.area GSINIT3 (CODE)
                            494 	.area GSINIT4 (CODE)
                            495 	.area GSINIT5 (CODE)
                            496 	.area GSINIT  (CODE)
                            497 	.area GSFINAL (CODE)
                            498 	.area CSEG    (CODE)
                            499 ;--------------------------------------------------------
                            500 ; interrupt vector 
                            501 ;--------------------------------------------------------
                            502 	.area HOME    (CODE)
   0000                     503 __interrupt_vect:
   0000 02 00 26            504 	ljmp	__sdcc_gsinit_startup
   0003 02 02 71            505 	ljmp	_ISR_ex0
   0006                     506 	.ds	5
   000B 32                  507 	reti
   000C                     508 	.ds	7
   0013 32                  509 	reti
   0014                     510 	.ds	7
   001B 32                  511 	reti
   001C                     512 	.ds	7
   0023 02 02 6A            513 	ljmp	_serial0
                            514 ;--------------------------------------------------------
                            515 ; global & static initialisations
                            516 ;--------------------------------------------------------
                            517 	.area HOME    (CODE)
                            518 	.area GSINIT  (CODE)
                            519 	.area GSFINAL (CODE)
                            520 	.area GSINIT  (CODE)
                            521 	.globl __sdcc_gsinit_startup
                            522 	.globl __sdcc_program_startup
                            523 	.globl __start__stack
                            524 	.globl __mcs51_genXINIT
                            525 	.globl __mcs51_genXRAMCLEAR
                            526 	.globl __mcs51_genRAMCLEAR
                            527 	.area GSFINAL (CODE)
   007F 02 00 82            528 	ljmp	__sdcc_program_startup
                            529 ;--------------------------------------------------------
                            530 ; Home
                            531 ;--------------------------------------------------------
                            532 	.area HOME    (CODE)
                            533 	.area CSEG    (CODE)
   0082                     534 __sdcc_program_startup:
   0082 12 00 87            535 	lcall	_main
                            536 ;	return from main will lock up
   0085 80 FE               537 	sjmp .
                            538 ;--------------------------------------------------------
                            539 ; code
                            540 ;--------------------------------------------------------
                            541 	.area CSEG    (CODE)
                            542 ;------------------------------------------------------------
                            543 ;Allocation info for local variables in function 'main'
                            544 ;------------------------------------------------------------
                            545 ;temp                      Allocated with name '_main_temp_1_1'
                            546 ;------------------------------------------------------------
                            547 ;	main.c:33: void main(void)
                            548 ;	-----------------------------------------
                            549 ;	 function main
                            550 ;	-----------------------------------------
   0087                     551 _main:
                    0002    552 	ar2 = 0x02
                    0003    553 	ar3 = 0x03
                    0004    554 	ar4 = 0x04
                    0005    555 	ar5 = 0x05
                    0006    556 	ar6 = 0x06
                    0007    557 	ar7 = 0x07
                    0000    558 	ar0 = 0x00
                    0001    559 	ar1 = 0x01
                            560 ;	main.c:36: value=0; i=0; j=0;
                            561 ;	genAssign
   0087 90 00 08            562 	mov	dptr,#_value
                            563 ;	Peephole 181	changed mov to clr
                            564 ;	genAssign
                            565 ;	Peephole 181	changed mov to clr
                            566 ;	Peephole 219.a	removed redundant clear
                            567 ;	genAssign
                            568 ;	Peephole 181	changed mov to clr
   008A E4                  569 	clr	a
   008B F0                  570 	movx	@dptr,a
   008C 90 00 00            571 	mov	dptr,#_i
   008F F0                  572 	movx	@dptr,a
   0090 90 00 01            573 	mov	dptr,#_j
                            574 ;	Peephole 219.b	removed redundant clear
   0093 F0                  575 	movx	@dptr,a
                            576 ;	main.c:38: SerialInitialize();
                            577 ;	genCall
   0094 12 04 74            578 	lcall	_SerialInitialize
                            579 ;	main.c:39: putstr("\n\r");
                            580 ;	genCall
                            581 ;	Peephole 182.a	used 16 bit load of DPTR
   0097 90 04 C7            582 	mov	dptr,#__str_0
   009A 75 F0 80            583 	mov	b,#0x80
   009D 12 04 13            584 	lcall	_putstr
                            585 ;	main.c:40: putstr("Hello \n\r");
                            586 ;	genCall
                            587 ;	Peephole 182.a	used 16 bit load of DPTR
   00A0 90 04 CA            588 	mov	dptr,#__str_1
   00A3 75 F0 80            589 	mov	b,#0x80
   00A6 12 04 13            590 	lcall	_putstr
                            591 ;	main.c:41: putstr("Press '>' for help menu\n\r");
                            592 ;	genCall
                            593 ;	Peephole 182.a	used 16 bit load of DPTR
   00A9 90 04 D3            594 	mov	dptr,#__str_2
   00AC 75 F0 80            595 	mov	b,#0x80
   00AF 12 04 13            596 	lcall	_putstr
                            597 ;	main.c:42: putstr("Enter the character \n\r");
                            598 ;	genCall
                            599 ;	Peephole 182.a	used 16 bit load of DPTR
   00B2 90 04 ED            600 	mov	dptr,#__str_3
   00B5 75 F0 80            601 	mov	b,#0x80
   00B8 12 04 13            602 	lcall	_putstr
                            603 ;	main.c:45: while(1)
   00BB                     604 00131$:
                            605 ;	main.c:47: temp = getchar();
                            606 ;	genCall
   00BB 12 04 83            607 	lcall	_getchar
   00BE AA 82               608 	mov	r2,dpl
                            609 ;	genCast
   00C0 7B 00               610 	mov	r3,#0x00
                            611 ;	main.c:48: putchar(temp);
                            612 ;	genCast
   00C2 8A 04               613 	mov	ar4,r2
                            614 ;	genCall
   00C4 8C 82               615 	mov	dpl,r4
   00C6 C0 02               616 	push	ar2
   00C8 C0 03               617 	push	ar3
   00CA 12 04 95            618 	lcall	_putchar
   00CD D0 03               619 	pop	ar3
   00CF D0 02               620 	pop	ar2
                            621 ;	main.c:49: putstr("\n\r");
                            622 ;	genCall
                            623 ;	Peephole 182.a	used 16 bit load of DPTR
   00D1 90 04 C7            624 	mov	dptr,#__str_0
   00D4 75 F0 80            625 	mov	b,#0x80
   00D7 C0 02               626 	push	ar2
   00D9 C0 03               627 	push	ar3
   00DB 12 04 13            628 	lcall	_putstr
   00DE D0 03               629 	pop	ar3
   00E0 D0 02               630 	pop	ar2
                            631 ;	main.c:50: switch(temp)
                            632 ;	genCmpLt
                            633 ;	genCmp
   00E2 C3                  634 	clr	c
   00E3 EA                  635 	mov	a,r2
   00E4 94 3E               636 	subb	a,#0x3E
   00E6 EB                  637 	mov	a,r3
   00E7 94 00               638 	subb	a,#0x00
                            639 ;	genIfxJump
   00E9 50 03               640 	jnc	00144$
   00EB 02 02 55            641 	ljmp	00128$
   00EE                     642 00144$:
                            643 ;	genCmpGt
                            644 ;	genCmp
   00EE C3                  645 	clr	c
   00EF 74 57               646 	mov	a,#0x57
   00F1 9A                  647 	subb	a,r2
                            648 ;	Peephole 181	changed mov to clr
   00F2 E4                  649 	clr	a
   00F3 9B                  650 	subb	a,r3
                            651 ;	genIfxJump
   00F4 50 03               652 	jnc	00145$
   00F6 02 02 55            653 	ljmp	00128$
   00F9                     654 00145$:
                            655 ;	genMinus
   00F9 EA                  656 	mov	a,r2
   00FA 24 C2               657 	add	a,#0xc2
                            658 ;	genJumpTab
   00FC FA                  659 	mov	r2,a
                            660 ;	Peephole 105	removed redundant mov
   00FD 24 09               661 	add	a,#(00146$-3-.)
   00FF 83                  662 	movc	a,@a+pc
   0100 C0 E0               663 	push	acc
   0102 EA                  664 	mov	a,r2
   0103 24 1D               665 	add	a,#(00147$-3-.)
   0105 83                  666 	movc	a,@a+pc
   0106 C0 E0               667 	push	acc
   0108 22                  668 	ret
   0109                     669 00146$:
   0109 CD                  670 	.db	00117$
   010A 55                  671 	.db	00128$
   010B 55                  672 	.db	00128$
   010C 55                  673 	.db	00128$
   010D 55                  674 	.db	00128$
   010E 19                  675 	.db	00124$
   010F 62                  676 	.db	00105$
   0110 3D                  677 	.db	00101$
   0111 3D                  678 	.db	00127$
   0112 55                  679 	.db	00128$
   0113 85                  680 	.db	00109$
   0114 D3                  681 	.db	00118$
   0115 55                  682 	.db	00128$
   0116 55                  683 	.db	00128$
   0117 55                  684 	.db	00128$
   0118 55                  685 	.db	00128$
   0119 28                  686 	.db	00125$
   011A 55                  687 	.db	00128$
   011B F6                  688 	.db	00121$
   011C 55                  689 	.db	00128$
   011D 55                  690 	.db	00128$
   011E AA                  691 	.db	00113$
   011F 55                  692 	.db	00128$
   0120 55                  693 	.db	00128$
   0121 55                  694 	.db	00128$
   0122 2E                  695 	.db	00126$
   0123                     696 00147$:
   0123 01                  697 	.db	00117$>>8
   0124 02                  698 	.db	00128$>>8
   0125 02                  699 	.db	00128$>>8
   0126 02                  700 	.db	00128$>>8
   0127 02                  701 	.db	00128$>>8
   0128 02                  702 	.db	00124$>>8
   0129 01                  703 	.db	00105$>>8
   012A 01                  704 	.db	00101$>>8
   012B 02                  705 	.db	00127$>>8
   012C 02                  706 	.db	00128$>>8
   012D 01                  707 	.db	00109$>>8
   012E 01                  708 	.db	00118$>>8
   012F 02                  709 	.db	00128$>>8
   0130 02                  710 	.db	00128$>>8
   0131 02                  711 	.db	00128$>>8
   0132 02                  712 	.db	00128$>>8
   0133 02                  713 	.db	00125$>>8
   0134 02                  714 	.db	00128$>>8
   0135 01                  715 	.db	00121$>>8
   0136 02                  716 	.db	00128$>>8
   0137 02                  717 	.db	00128$>>8
   0138 01                  718 	.db	00113$>>8
   0139 02                  719 	.db	00128$>>8
   013A 02                  720 	.db	00128$>>8
   013B 02                  721 	.db	00128$>>8
   013C 02                  722 	.db	00126$>>8
                            723 ;	main.c:52: case 'E' :
   013D                     724 00101$:
                            725 ;	main.c:53: if((CCAPM0 & 0x42)!=0x42)
                            726 ;	genAnd
   013D 74 42               727 	mov	a,#0x42
   013F 55 DA               728 	anl	a,_CCAPM0
   0141 FA                  729 	mov	r2,a
                            730 ;	genCmpEq
                            731 ;	gencjneshort
   0142 BA 42 02            732 	cjne	r2,#0x42,00148$
                            733 ;	Peephole 112.b	changed ljmp to sjmp
   0145 80 0F               734 	sjmp	00103$
   0147                     735 00148$:
                            736 ;	main.c:55: putstr("Configuring PCA module 0 in PWM mode. Check P1.3 \n\r");
                            737 ;	genCall
                            738 ;	Peephole 182.a	used 16 bit load of DPTR
   0147 90 05 04            739 	mov	dptr,#__str_4
   014A 75 F0 80            740 	mov	b,#0x80
   014D 12 04 13            741 	lcall	_putstr
                            742 ;	main.c:56: PWM_configure();
                            743 ;	genCall
   0150 12 02 91            744 	lcall	_PWM_configure
   0153 02 00 BB            745 	ljmp	00131$
   0156                     746 00103$:
                            747 ;	main.c:60: putstr("PWM already configured on P1.3 \n\r");
                            748 ;	genCall
                            749 ;	Peephole 182.a	used 16 bit load of DPTR
   0156 90 05 38            750 	mov	dptr,#__str_5
   0159 75 F0 80            751 	mov	b,#0x80
   015C 12 04 13            752 	lcall	_putstr
                            753 ;	main.c:62: break;
   015F 02 00 BB            754 	ljmp	00131$
                            755 ;	main.c:63: case 'D' :
   0162                     756 00105$:
                            757 ;	main.c:64: if((CCAPM0 & 0x02)==0x02)
                            758 ;	genAnd
   0162 74 02               759 	mov	a,#0x02
   0164 55 DA               760 	anl	a,_CCAPM0
   0166 FA                  761 	mov	r2,a
                            762 ;	genCmpEq
                            763 ;	gencjneshort
                            764 ;	Peephole 112.b	changed ljmp to sjmp
                            765 ;	Peephole 198.b	optimized misc jump sequence
   0167 BA 02 0F            766 	cjne	r2,#0x02,00107$
                            767 ;	Peephole 200.b	removed redundant sjmp
                            768 ;	Peephole 300	removed redundant label 00149$
                            769 ;	Peephole 300	removed redundant label 00150$
                            770 ;	main.c:66: putstr("Disabling PWM mode on P1.3 \n\r");
                            771 ;	genCall
                            772 ;	Peephole 182.a	used 16 bit load of DPTR
   016A 90 05 5A            773 	mov	dptr,#__str_6
   016D 75 F0 80            774 	mov	b,#0x80
   0170 12 04 13            775 	lcall	_putstr
                            776 ;	main.c:67: PWM_disable();
                            777 ;	genCall
   0173 12 02 D3            778 	lcall	_PWM_disable
   0176 02 00 BB            779 	ljmp	00131$
   0179                     780 00107$:
                            781 ;	main.c:71: putstr("PWM mode on P1.3 is already disabled\n\r");
                            782 ;	genCall
                            783 ;	Peephole 182.a	used 16 bit load of DPTR
   0179 90 05 78            784 	mov	dptr,#__str_7
   017C 75 F0 80            785 	mov	b,#0x80
   017F 12 04 13            786 	lcall	_putstr
                            787 ;	main.c:73: break;
   0182 02 00 BB            788 	ljmp	00131$
                            789 ;	main.c:74: case 'H' :
   0185                     790 00109$:
                            791 ;	main.c:75: if((CCAPM1 & 0x4C)!= 0x4C)
                            792 ;	genAnd
   0185 74 4C               793 	mov	a,#0x4C
   0187 55 DB               794 	anl	a,_CCAPM1
   0189 FA                  795 	mov	r2,a
                            796 ;	genCmpEq
                            797 ;	gencjneshort
   018A BA 4C 02            798 	cjne	r2,#0x4C,00151$
                            799 ;	Peephole 112.b	changed ljmp to sjmp
   018D 80 0F               800 	sjmp	00111$
   018F                     801 00151$:
                            802 ;	main.c:77: putstr("Configuring PCA module 1 in High Speed mode. Check P1.4 \n\r");
                            803 ;	genCall
                            804 ;	Peephole 182.a	used 16 bit load of DPTR
   018F 90 05 9F            805 	mov	dptr,#__str_8
   0192 75 F0 80            806 	mov	b,#0x80
   0195 12 04 13            807 	lcall	_putstr
                            808 ;	main.c:78: HighSpeed_configure();
                            809 ;	genCall
   0198 12 02 BC            810 	lcall	_HighSpeed_configure
   019B 02 00 BB            811 	ljmp	00131$
   019E                     812 00111$:
                            813 ;	main.c:82: putstr("High Speed mode is already configured. Check P1.4 \n\r");
                            814 ;	genCall
                            815 ;	Peephole 182.a	used 16 bit load of DPTR
   019E 90 05 DA            816 	mov	dptr,#__str_9
   01A1 75 F0 80            817 	mov	b,#0x80
   01A4 12 04 13            818 	lcall	_putstr
                            819 ;	main.c:84: break;
   01A7 02 00 BB            820 	ljmp	00131$
                            821 ;	main.c:85: case 'S' :
   01AA                     822 00113$:
                            823 ;	main.c:86: if((CCAPM1 & 0x0C)==0x0C)
                            824 ;	genAnd
   01AA 74 0C               825 	mov	a,#0x0C
   01AC 55 DB               826 	anl	a,_CCAPM1
   01AE FA                  827 	mov	r2,a
                            828 ;	genCmpEq
                            829 ;	gencjneshort
                            830 ;	Peephole 112.b	changed ljmp to sjmp
                            831 ;	Peephole 198.b	optimized misc jump sequence
   01AF BA 0C 0F            832 	cjne	r2,#0x0C,00115$
                            833 ;	Peephole 200.b	removed redundant sjmp
                            834 ;	Peephole 300	removed redundant label 00152$
                            835 ;	Peephole 300	removed redundant label 00153$
                            836 ;	main.c:88: putstr("Disabling the High Speed mode on P1.4 \n\r");
                            837 ;	genCall
                            838 ;	Peephole 182.a	used 16 bit load of DPTR
   01B2 90 06 0F            839 	mov	dptr,#__str_10
   01B5 75 F0 80            840 	mov	b,#0x80
   01B8 12 04 13            841 	lcall	_putstr
                            842 ;	main.c:89: HighSpeed_disable();
                            843 ;	genCall
   01BB 12 02 DB            844 	lcall	_HighSpeed_disable
   01BE 02 00 BB            845 	ljmp	00131$
   01C1                     846 00115$:
                            847 ;	main.c:93: putstr("High Speed mode on P1.4 is already disabled \n\r");
                            848 ;	genCall
                            849 ;	Peephole 182.a	used 16 bit load of DPTR
   01C1 90 06 38            850 	mov	dptr,#__str_11
   01C4 75 F0 80            851 	mov	b,#0x80
   01C7 12 04 13            852 	lcall	_putstr
                            853 ;	main.c:95: break;
   01CA 02 00 BB            854 	ljmp	00131$
                            855 ;	main.c:96: case '>' :
   01CD                     856 00117$:
                            857 ;	main.c:97: help_menu();
                            858 ;	genCall
   01CD 12 02 E3            859 	lcall	_help_menu
                            860 ;	main.c:98: break;
   01D0 02 00 BB            861 	ljmp	00131$
                            862 ;	main.c:99: case 'I' :
   01D3                     863 00118$:
                            864 ;	main.c:100: if((PCON &0x01)!=0x01)
                            865 ;	genAnd
   01D3 74 01               866 	mov	a,#0x01
   01D5 55 87               867 	anl	a,_PCON
   01D7 FA                  868 	mov	r2,a
                            869 ;	genCmpEq
                            870 ;	gencjneshort
   01D8 BA 01 03            871 	cjne	r2,#0x01,00154$
   01DB 02 00 BB            872 	ljmp	00131$
   01DE                     873 00154$:
                            874 ;	main.c:102: putstr("Entering into Idle Mode. Press any key or generate external interrupt0 to come out of it \n\r");
                            875 ;	genCall
                            876 ;	Peephole 182.a	used 16 bit load of DPTR
   01DE 90 06 67            877 	mov	dptr,#__str_12
   01E1 75 F0 80            878 	mov	b,#0x80
   01E4 12 04 13            879 	lcall	_putstr
                            880 ;	main.c:103: Idle_mode();
                            881 ;	genCall
   01E7 12 02 A8            882 	lcall	_Idle_mode
                            883 ;	main.c:104: putstr("Came out of Idle mode\n\r");
                            884 ;	genCall
                            885 ;	Peephole 182.a	used 16 bit load of DPTR
   01EA 90 06 C3            886 	mov	dptr,#__str_13
   01ED 75 F0 80            887 	mov	b,#0x80
   01F0 12 04 13            888 	lcall	_putstr
                            889 ;	main.c:106: break;
   01F3 02 00 BB            890 	ljmp	00131$
                            891 ;	main.c:107: case 'P' :
   01F6                     892 00121$:
                            893 ;	main.c:108: if((PCON & 0x02)!= 0x02)
                            894 ;	genAnd
   01F6 74 02               895 	mov	a,#0x02
   01F8 55 87               896 	anl	a,_PCON
   01FA FA                  897 	mov	r2,a
                            898 ;	genCmpEq
                            899 ;	gencjneshort
   01FB BA 02 03            900 	cjne	r2,#0x02,00155$
   01FE 02 00 BB            901 	ljmp	00131$
   0201                     902 00155$:
                            903 ;	main.c:110: putstr("Entering Power-down mode. Generate external interrupt0 to come out of it \n\r");
                            904 ;	genCall
                            905 ;	Peephole 182.a	used 16 bit load of DPTR
   0201 90 06 DB            906 	mov	dptr,#__str_14
   0204 75 F0 80            907 	mov	b,#0x80
   0207 12 04 13            908 	lcall	_putstr
                            909 ;	main.c:111: Power_down();
                            910 ;	genCall
   020A 12 02 B2            911 	lcall	_Power_down
                            912 ;	main.c:112: putstr("Came out of power down mode\n\r");
                            913 ;	genCall
                            914 ;	Peephole 182.a	used 16 bit load of DPTR
   020D 90 07 27            915 	mov	dptr,#__str_15
   0210 75 F0 80            916 	mov	b,#0x80
   0213 12 04 13            917 	lcall	_putstr
                            918 ;	main.c:114: break;
   0216 02 00 BB            919 	ljmp	00131$
                            920 ;	main.c:115: case 'C' :
   0219                     921 00124$:
                            922 ;	main.c:116: putstr("Changing the F(clkperiph) to minimum frequency\n\r");
                            923 ;	genCall
                            924 ;	Peephole 182.a	used 16 bit load of DPTR
   0219 90 07 45            925 	mov	dptr,#__str_16
   021C 75 F0 80            926 	mov	b,#0x80
   021F 12 04 13            927 	lcall	_putstr
                            928 ;	main.c:117: Clock_frequency_min();
                            929 ;	genCall
   0222 12 02 72            930 	lcall	_Clock_frequency_min
                            931 ;	main.c:118: break;
   0225 02 00 BB            932 	ljmp	00131$
                            933 ;	main.c:119: case 'N' :
   0228                     934 00125$:
                            935 ;	main.c:120: Clock_frequency_max();
                            936 ;	genCall
   0228 12 02 76            937 	lcall	_Clock_frequency_max
                            938 ;	main.c:121: break;
   022B 02 00 BB            939 	ljmp	00131$
                            940 ;	main.c:122: case 'W' :
   022E                     941 00126$:
                            942 ;	main.c:123: putstr("Watchdog timer is set. Controller will be reset\n\r");
                            943 ;	genCall
                            944 ;	Peephole 182.a	used 16 bit load of DPTR
   022E 90 07 76            945 	mov	dptr,#__str_17
   0231 75 F0 80            946 	mov	b,#0x80
   0234 12 04 13            947 	lcall	_putstr
                            948 ;	main.c:124: watch_dog();
                            949 ;	genCall
   0237 12 02 7A            950 	lcall	_watch_dog
                            951 ;	main.c:125: break;
   023A 02 00 BB            952 	ljmp	00131$
                            953 ;	main.c:126: case 'F' :
   023D                     954 00127$:
                            955 ;	main.c:127: putstr("Going to minimum frequency mode\n\r");
                            956 ;	genCall
                            957 ;	Peephole 182.a	used 16 bit load of DPTR
   023D 90 07 A8            958 	mov	dptr,#__str_18
   0240 75 F0 80            959 	mov	b,#0x80
   0243 12 04 13            960 	lcall	_putstr
                            961 ;	main.c:128: Clock_frequency();
                            962 ;	genCall
   0246 12 04 69            963 	lcall	_Clock_frequency
                            964 ;	main.c:129: putstr("Coming back to maximum frequency mode\n\r");
                            965 ;	genCall
                            966 ;	Peephole 182.a	used 16 bit load of DPTR
   0249 90 07 CA            967 	mov	dptr,#__str_19
   024C 75 F0 80            968 	mov	b,#0x80
   024F 12 04 13            969 	lcall	_putstr
                            970 ;	main.c:130: break;
   0252 02 00 BB            971 	ljmp	00131$
                            972 ;	main.c:131: default :
   0255                     973 00128$:
                            974 ;	main.c:132: putstr("\n\rEnter valid input\n\r");
                            975 ;	genCall
                            976 ;	Peephole 182.a	used 16 bit load of DPTR
   0255 90 07 F2            977 	mov	dptr,#__str_20
   0258 75 F0 80            978 	mov	b,#0x80
   025B 12 04 13            979 	lcall	_putstr
                            980 ;	main.c:133: putstr("Press '>' for help menu\n\r");
                            981 ;	genCall
                            982 ;	Peephole 182.a	used 16 bit load of DPTR
   025E 90 04 D3            983 	mov	dptr,#__str_2
   0261 75 F0 80            984 	mov	b,#0x80
   0264 12 04 13            985 	lcall	_putstr
                            986 ;	main.c:134: }
   0267 02 00 BB            987 	ljmp	00131$
                            988 ;	Peephole 259.b	removed redundant label 00133$ and ret
                            989 ;
                            990 ;------------------------------------------------------------
                            991 ;Allocation info for local variables in function 'serial0'
                            992 ;------------------------------------------------------------
                            993 ;da                        Allocated with name '_serial0_da_1_1'
                            994 ;------------------------------------------------------------
                            995 ;	main.c:138: void serial0(void) interrupt 4
                            996 ;	-----------------------------------------
                            997 ;	 function serial0
                            998 ;	-----------------------------------------
   026A                     999 _serial0:
   026A C0 E0              1000 	push	acc
                           1001 ;	main.c:141: da = SBUF;
                           1002 ;	genDummyRead
   026C E5 99              1003 	mov	a,_SBUF
                           1004 ;	Peephole 300	removed redundant label 00101$
   026E D0 E0              1005 	pop	acc
   0270 32                 1006 	reti
                           1007 ;	eliminated unneeded push/pop psw
                           1008 ;	eliminated unneeded push/pop dpl
                           1009 ;	eliminated unneeded push/pop dph
                           1010 ;	eliminated unneeded push/pop b
                           1011 ;------------------------------------------------------------
                           1012 ;Allocation info for local variables in function 'ISR_ex0'
                           1013 ;------------------------------------------------------------
                           1014 ;------------------------------------------------------------
                           1015 ;	main.c:145: void ISR_ex0(void) interrupt 0
                           1016 ;	-----------------------------------------
                           1017 ;	 function ISR_ex0
                           1018 ;	-----------------------------------------
   0271                    1019 _ISR_ex0:
                           1020 ;	main.c:148: }
                           1021 ;	Peephole 300	removed redundant label 00101$
   0271 32                 1022 	reti
                           1023 ;	eliminated unneeded push/pop psw
                           1024 ;	eliminated unneeded push/pop dpl
                           1025 ;	eliminated unneeded push/pop dph
                           1026 ;	eliminated unneeded push/pop b
                           1027 ;	eliminated unneeded push/pop acc
                           1028 ;------------------------------------------------------------
                           1029 ;Allocation info for local variables in function 'Clock_frequency_min'
                           1030 ;------------------------------------------------------------
                           1031 ;------------------------------------------------------------
                           1032 ;	main.c:150: void Clock_frequency_min()
                           1033 ;	-----------------------------------------
                           1034 ;	 function Clock_frequency_min
                           1035 ;	-----------------------------------------
   0272                    1036 _Clock_frequency_min:
                           1037 ;	main.c:152: CKRL = 0x00;
                           1038 ;	genAssign
   0272 75 97 00           1039 	mov	_CKRL,#0x00
                           1040 ;	Peephole 300	removed redundant label 00101$
   0275 22                 1041 	ret
                           1042 ;------------------------------------------------------------
                           1043 ;Allocation info for local variables in function 'Clock_frequency_max'
                           1044 ;------------------------------------------------------------
                           1045 ;------------------------------------------------------------
                           1046 ;	main.c:155: void Clock_frequency_max()
                           1047 ;	-----------------------------------------
                           1048 ;	 function Clock_frequency_max
                           1049 ;	-----------------------------------------
   0276                    1050 _Clock_frequency_max:
                           1051 ;	main.c:157: CKRL = 0xFF;
                           1052 ;	genAssign
   0276 75 97 FF           1053 	mov	_CKRL,#0xFF
                           1054 ;	Peephole 300	removed redundant label 00101$
   0279 22                 1055 	ret
                           1056 ;------------------------------------------------------------
                           1057 ;Allocation info for local variables in function 'watch_dog'
                           1058 ;------------------------------------------------------------
                           1059 ;------------------------------------------------------------
                           1060 ;	main.c:160: void watch_dog()
                           1061 ;	-----------------------------------------
                           1062 ;	 function watch_dog
                           1063 ;	-----------------------------------------
   027A                    1064 _watch_dog:
                           1065 ;	main.c:162: CR=0;
                           1066 ;	genAssign
   027A C2 DE              1067 	clr	_CR
                           1068 ;	main.c:163: CH=0;
                           1069 ;	genAssign
   027C 75 F9 00           1070 	mov	_CH,#0x00
                           1071 ;	main.c:164: CL=0;
                           1072 ;	genAssign
   027F 75 E9 00           1073 	mov	_CL,#0x00
                           1074 ;	main.c:165: CCAP4L = 0xFF;
                           1075 ;	genAssign
   0282 75 EE FF           1076 	mov	_CCAP4L,#0xFF
                           1077 ;	main.c:166: CCAP4H = 0xFF;
                           1078 ;	genAssign
   0285 75 FE FF           1079 	mov	_CCAP4H,#0xFF
                           1080 ;	main.c:167: CCAPM4 = 0x4C;
                           1081 ;	genAssign
   0288 75 DE 4C           1082 	mov	_CCAPM4,#0x4C
                           1083 ;	main.c:168: CMOD = CMOD | 0x40;
                           1084 ;	genOr
   028B 43 D9 40           1085 	orl	_CMOD,#0x40
                           1086 ;	main.c:169: CR=1;
                           1087 ;	genAssign
   028E D2 DE              1088 	setb	_CR
                           1089 ;	Peephole 300	removed redundant label 00101$
   0290 22                 1090 	ret
                           1091 ;------------------------------------------------------------
                           1092 ;Allocation info for local variables in function 'PWM_configure'
                           1093 ;------------------------------------------------------------
                           1094 ;------------------------------------------------------------
                           1095 ;	main.c:172: void PWM_configure()
                           1096 ;	-----------------------------------------
                           1097 ;	 function PWM_configure
                           1098 ;	-----------------------------------------
   0291                    1099 _PWM_configure:
                           1100 ;	main.c:174: CR=0;
                           1101 ;	genAssign
   0291 C2 DE              1102 	clr	_CR
                           1103 ;	main.c:175: CMOD |= 0x02;
                           1104 ;	genOr
   0293 43 D9 02           1105 	orl	_CMOD,#0x02
                           1106 ;	main.c:176: CH = 0x00;
                           1107 ;	genAssign
   0296 75 F9 00           1108 	mov	_CH,#0x00
                           1109 ;	main.c:177: CL = 0x00;
                           1110 ;	genAssign
   0299 75 E9 00           1111 	mov	_CL,#0x00
                           1112 ;	main.c:178: CCAP0L = 0x4D;
                           1113 ;	genAssign
   029C 75 EA 4D           1114 	mov	_CCAP0L,#0x4D
                           1115 ;	main.c:179: CCAP0H = 0X4D;
                           1116 ;	genAssign
   029F 75 FA 4D           1117 	mov	_CCAP0H,#0x4D
                           1118 ;	main.c:180: CCAPM0 |= 0x42;
                           1119 ;	genOr
   02A2 43 DA 42           1120 	orl	_CCAPM0,#0x42
                           1121 ;	main.c:181: CR=1;
                           1122 ;	genAssign
   02A5 D2 DE              1123 	setb	_CR
                           1124 ;	Peephole 300	removed redundant label 00101$
   02A7 22                 1125 	ret
                           1126 ;------------------------------------------------------------
                           1127 ;Allocation info for local variables in function 'Idle_mode'
                           1128 ;------------------------------------------------------------
                           1129 ;------------------------------------------------------------
                           1130 ;	main.c:185: void Idle_mode()
                           1131 ;	-----------------------------------------
                           1132 ;	 function Idle_mode
                           1133 ;	-----------------------------------------
   02A8                    1134 _Idle_mode:
                           1135 ;	main.c:187: IE |= 0x91;
                           1136 ;	genOr
   02A8 43 A8 91           1137 	orl	_IE,#0x91
                           1138 ;	main.c:188: PCON |= 0x01;
                           1139 ;	genOr
   02AB 43 87 01           1140 	orl	_PCON,#0x01
                           1141 ;	main.c:189: IE = 0x00;
                           1142 ;	genAssign
   02AE 75 A8 00           1143 	mov	_IE,#0x00
                           1144 ;	Peephole 300	removed redundant label 00101$
   02B1 22                 1145 	ret
                           1146 ;------------------------------------------------------------
                           1147 ;Allocation info for local variables in function 'Power_down'
                           1148 ;------------------------------------------------------------
                           1149 ;------------------------------------------------------------
                           1150 ;	main.c:193: void Power_down()
                           1151 ;	-----------------------------------------
                           1152 ;	 function Power_down
                           1153 ;	-----------------------------------------
   02B2                    1154 _Power_down:
                           1155 ;	main.c:195: IE |= 0x81;
                           1156 ;	genOr
   02B2 43 A8 81           1157 	orl	_IE,#0x81
                           1158 ;	main.c:196: PCON |= 0x02;
                           1159 ;	genOr
   02B5 43 87 02           1160 	orl	_PCON,#0x02
                           1161 ;	main.c:197: IE = 0x00;
                           1162 ;	genAssign
   02B8 75 A8 00           1163 	mov	_IE,#0x00
                           1164 ;	Peephole 300	removed redundant label 00101$
   02BB 22                 1165 	ret
                           1166 ;------------------------------------------------------------
                           1167 ;Allocation info for local variables in function 'HighSpeed_configure'
                           1168 ;------------------------------------------------------------
                           1169 ;------------------------------------------------------------
                           1170 ;	main.c:200: void HighSpeed_configure()
                           1171 ;	-----------------------------------------
                           1172 ;	 function HighSpeed_configure
                           1173 ;	-----------------------------------------
   02BC                    1174 _HighSpeed_configure:
                           1175 ;	main.c:202: CR=0;
                           1176 ;	genAssign
   02BC C2 DE              1177 	clr	_CR
                           1178 ;	main.c:203: CMOD |= 0x02;
                           1179 ;	genOr
   02BE 43 D9 02           1180 	orl	_CMOD,#0x02
                           1181 ;	main.c:204: CH = 0x00;
                           1182 ;	genAssign
   02C1 75 F9 00           1183 	mov	_CH,#0x00
                           1184 ;	main.c:205: CL = 0x00;
                           1185 ;	genAssign
   02C4 75 E9 00           1186 	mov	_CL,#0x00
                           1187 ;	main.c:206: CCAP1H = 0x00;
                           1188 ;	genAssign
   02C7 75 FB 00           1189 	mov	_CCAP1H,#0x00
                           1190 ;	main.c:207: CCAP1L = 0x55;
                           1191 ;	genAssign
   02CA 75 EB 55           1192 	mov	_CCAP1L,#0x55
                           1193 ;	main.c:208: CCAPM1 = 0x4C;
                           1194 ;	genAssign
   02CD 75 DB 4C           1195 	mov	_CCAPM1,#0x4C
                           1196 ;	main.c:209: CR=1;
                           1197 ;	genAssign
   02D0 D2 DE              1198 	setb	_CR
                           1199 ;	Peephole 300	removed redundant label 00101$
   02D2 22                 1200 	ret
                           1201 ;------------------------------------------------------------
                           1202 ;Allocation info for local variables in function 'PWM_disable'
                           1203 ;------------------------------------------------------------
                           1204 ;------------------------------------------------------------
                           1205 ;	main.c:212: void PWM_disable()
                           1206 ;	-----------------------------------------
                           1207 ;	 function PWM_disable
                           1208 ;	-----------------------------------------
   02D3                    1209 _PWM_disable:
                           1210 ;	main.c:214: CR=0;
                           1211 ;	genAssign
   02D3 C2 DE              1212 	clr	_CR
                           1213 ;	main.c:215: CCAPM0 &= 0xFD;
                           1214 ;	genAnd
   02D5 53 DA FD           1215 	anl	_CCAPM0,#0xFD
                           1216 ;	main.c:216: CR=1;
                           1217 ;	genAssign
   02D8 D2 DE              1218 	setb	_CR
                           1219 ;	Peephole 300	removed redundant label 00101$
   02DA 22                 1220 	ret
                           1221 ;------------------------------------------------------------
                           1222 ;Allocation info for local variables in function 'HighSpeed_disable'
                           1223 ;------------------------------------------------------------
                           1224 ;------------------------------------------------------------
                           1225 ;	main.c:219: void HighSpeed_disable()
                           1226 ;	-----------------------------------------
                           1227 ;	 function HighSpeed_disable
                           1228 ;	-----------------------------------------
   02DB                    1229 _HighSpeed_disable:
                           1230 ;	main.c:221: CR=0;
                           1231 ;	genAssign
   02DB C2 DE              1232 	clr	_CR
                           1233 ;	main.c:222: CCAPM1 &= 0xF3;
                           1234 ;	genAnd
   02DD 53 DB F3           1235 	anl	_CCAPM1,#0xF3
                           1236 ;	main.c:223: CR=1;
                           1237 ;	genAssign
   02E0 D2 DE              1238 	setb	_CR
                           1239 ;	Peephole 300	removed redundant label 00101$
   02E2 22                 1240 	ret
                           1241 ;------------------------------------------------------------
                           1242 ;Allocation info for local variables in function 'help_menu'
                           1243 ;------------------------------------------------------------
                           1244 ;------------------------------------------------------------
                           1245 ;	main.c:226: void help_menu()
                           1246 ;	-----------------------------------------
                           1247 ;	 function help_menu
                           1248 ;	-----------------------------------------
   02E3                    1249 _help_menu:
                           1250 ;	main.c:228: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           1251 ;	genCall
                           1252 ;	Peephole 182.a	used 16 bit load of DPTR
   02E3 90 08 08           1253 	mov	dptr,#__str_21
   02E6 75 F0 80           1254 	mov	b,#0x80
   02E9 12 04 13           1255 	lcall	_putstr
                           1256 ;	main.c:229: putstr("Welcome to the Help Menu\n\r");
                           1257 ;	genCall
                           1258 ;	Peephole 182.a	used 16 bit load of DPTR
   02EC 90 08 79           1259 	mov	dptr,#__str_22
   02EF 75 F0 80           1260 	mov	b,#0x80
   02F2 12 04 13           1261 	lcall	_putstr
                           1262 ;	main.c:230: putstr("Press 'E' for enabling the PWM output on P1.3\n\r");
                           1263 ;	genCall
                           1264 ;	Peephole 182.a	used 16 bit load of DPTR
   02F5 90 08 94           1265 	mov	dptr,#__str_23
   02F8 75 F0 80           1266 	mov	b,#0x80
   02FB 12 04 13           1267 	lcall	_putstr
                           1268 ;	main.c:231: putstr("Press 'D' for disabling the PWM output on P1.3\n\r");
                           1269 ;	genCall
                           1270 ;	Peephole 182.a	used 16 bit load of DPTR
   02FE 90 08 C4           1271 	mov	dptr,#__str_24
   0301 75 F0 80           1272 	mov	b,#0x80
   0304 12 04 13           1273 	lcall	_putstr
                           1274 ;	main.c:232: putstr("Press 'H' for enabling the High Speed output on P1.4\n\r");
                           1275 ;	genCall
                           1276 ;	Peephole 182.a	used 16 bit load of DPTR
   0307 90 08 F5           1277 	mov	dptr,#__str_25
   030A 75 F0 80           1278 	mov	b,#0x80
   030D 12 04 13           1279 	lcall	_putstr
                           1280 ;	main.c:233: putstr("Press 'S' for disabling the High Speed output on P1.4\n\r");
                           1281 ;	genCall
                           1282 ;	Peephole 182.a	used 16 bit load of DPTR
   0310 90 09 2C           1283 	mov	dptr,#__str_26
   0313 75 F0 80           1284 	mov	b,#0x80
   0316 12 04 13           1285 	lcall	_putstr
                           1286 ;	main.c:234: putstr("Press 'I' to enter into IDLE mode\n\r");
                           1287 ;	genCall
                           1288 ;	Peephole 182.a	used 16 bit load of DPTR
   0319 90 09 64           1289 	mov	dptr,#__str_27
   031C 75 F0 80           1290 	mov	b,#0x80
   031F 12 04 13           1291 	lcall	_putstr
                           1292 ;	main.c:235: putstr("Press 'P' to enter into Power down mode\n\r");
                           1293 ;	genCall
                           1294 ;	Peephole 182.a	used 16 bit load of DPTR
   0322 90 09 88           1295 	mov	dptr,#__str_28
   0325 75 F0 80           1296 	mov	b,#0x80
   0328 12 04 13           1297 	lcall	_putstr
                           1298 ;	main.c:236: putstr("Press 'C' to Set F(clkperiph) at the minimum frequency supported by the CKRL register\n\r");
                           1299 ;	genCall
                           1300 ;	Peephole 182.a	used 16 bit load of DPTR
   032B 90 09 B2           1301 	mov	dptr,#__str_29
   032E 75 F0 80           1302 	mov	b,#0x80
   0331 12 04 13           1303 	lcall	_putstr
                           1304 ;	main.c:237: putstr("Press 'N' to Set F(clkperiph) at the maximum frequency supported by the CKRL register\n\r");
                           1305 ;	genCall
                           1306 ;	Peephole 182.a	used 16 bit load of DPTR
   0334 90 0A 0A           1307 	mov	dptr,#__str_30
   0337 75 F0 80           1308 	mov	b,#0x80
   033A 12 04 13           1309 	lcall	_putstr
                           1310 ;	main.c:238: putstr("Press 'F' to go into the minimum frequency for short amount of period and then come back to normal\n\r");
                           1311 ;	genCall
                           1312 ;	Peephole 182.a	used 16 bit load of DPTR
   033D 90 0A 62           1313 	mov	dptr,#__str_31
   0340 75 F0 80           1314 	mov	b,#0x80
   0343 12 04 13           1315 	lcall	_putstr
                           1316 ;	main.c:239: putstr("Press 'W' to enable the watchdog timer\n\r");
                           1317 ;	genCall
                           1318 ;	Peephole 182.a	used 16 bit load of DPTR
   0346 90 0A C7           1319 	mov	dptr,#__str_32
   0349 75 F0 80           1320 	mov	b,#0x80
   034C 12 04 13           1321 	lcall	_putstr
                           1322 ;	main.c:240: putstr("\n\r");
                           1323 ;	genCall
                           1324 ;	Peephole 182.a	used 16 bit load of DPTR
   034F 90 04 C7           1325 	mov	dptr,#__str_0
   0352 75 F0 80           1326 	mov	b,#0x80
   0355 12 04 13           1327 	lcall	_putstr
                           1328 ;	main.c:241: putstr("Note: \n\r");
                           1329 ;	genCall
                           1330 ;	Peephole 182.a	used 16 bit load of DPTR
   0358 90 0A F0           1331 	mov	dptr,#__str_33
   035B 75 F0 80           1332 	mov	b,#0x80
   035E 12 04 13           1333 	lcall	_putstr
                           1334 ;	main.c:242: putstr("1. To come out of IDLE mode, press any key on the keyboard or generate external interrupt0\n\r");
                           1335 ;	genCall
                           1336 ;	Peephole 182.a	used 16 bit load of DPTR
   0361 90 0A F9           1337 	mov	dptr,#__str_34
   0364 75 F0 80           1338 	mov	b,#0x80
   0367 12 04 13           1339 	lcall	_putstr
                           1340 ;	main.c:243: putstr("2. To come out of Power down mode, generate external interrupt0\n\r");
                           1341 ;	genCall
                           1342 ;	Peephole 182.a	used 16 bit load of DPTR
   036A 90 0B 56           1343 	mov	dptr,#__str_35
   036D 75 F0 80           1344 	mov	b,#0x80
   0370 12 04 13           1345 	lcall	_putstr
                           1346 ;	main.c:244: putstr("3. When you change F(clkperiph) to minimum, the baud rate changes. Hence set-\n\r");
                           1347 ;	genCall
                           1348 ;	Peephole 182.a	used 16 bit load of DPTR
   0373 90 0B 98           1349 	mov	dptr,#__str_36
   0376 75 F0 80           1350 	mov	b,#0x80
   0379 12 04 13           1351 	lcall	_putstr
                           1352 ;	main.c:245: putstr("   -the baud rate as '18' in your serial terminal. And, when you come back to-\n\r");
                           1353 ;	genCall
                           1354 ;	Peephole 182.a	used 16 bit load of DPTR
   037C 90 0B E8           1355 	mov	dptr,#__str_37
   037F 75 F0 80           1356 	mov	b,#0x80
   0382 12 04 13           1357 	lcall	_putstr
                           1358 ;	main.c:246: putstr("   -normal(maximum) frequency, change the baud rate to normal\n\r");
                           1359 ;	genCall
                           1360 ;	Peephole 182.a	used 16 bit load of DPTR
   0385 90 0C 39           1361 	mov	dptr,#__str_38
   0388 75 F0 80           1362 	mov	b,#0x80
   038B 12 04 13           1363 	lcall	_putstr
                           1364 ;	main.c:247: putstr("4. Once you enable the watchdog timer, the controller gets reset within seconds\n\r");
                           1365 ;	genCall
                           1366 ;	Peephole 182.a	used 16 bit load of DPTR
   038E 90 0C 79           1367 	mov	dptr,#__str_39
   0391 75 F0 80           1368 	mov	b,#0x80
   0394 12 04 13           1369 	lcall	_putstr
                           1370 ;	main.c:248: putstr("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           1371 ;	genCall
                           1372 ;	Peephole 182.a	used 16 bit load of DPTR
   0397 90 08 08           1373 	mov	dptr,#__str_21
   039A 75 F0 80           1374 	mov	b,#0x80
                           1375 ;	Peephole 253.b	replaced lcall/ret with ljmp
   039D 02 04 13           1376 	ljmp	_putstr
                           1377 ;
                           1378 ;------------------------------------------------------------
                           1379 ;Allocation info for local variables in function 'ReadValue'
                           1380 ;------------------------------------------------------------
                           1381 ;------------------------------------------------------------
                           1382 ;	main.c:252: uint8_t ReadValue()
                           1383 ;	-----------------------------------------
                           1384 ;	 function ReadValue
                           1385 ;	-----------------------------------------
   03A0                    1386 _ReadValue:
                           1387 ;	main.c:254: do
   03A0                    1388 00110$:
                           1389 ;	main.c:256: i=getchar();
                           1390 ;	genCall
   03A0 12 04 83           1391 	lcall	_getchar
   03A3 AA 82              1392 	mov	r2,dpl
                           1393 ;	genAssign
   03A5 90 00 00           1394 	mov	dptr,#_i
   03A8 EA                 1395 	mov	a,r2
   03A9 F0                 1396 	movx	@dptr,a
                           1397 ;	main.c:257: if(i>='0'&&i<='9')          /* works only if its a number */
                           1398 ;	genCmpLt
                           1399 ;	genCmp
   03AA BA 30 00           1400 	cjne	r2,#0x30,00121$
   03AD                    1401 00121$:
                           1402 ;	genIfxJump
                           1403 ;	Peephole 112.b	changed ljmp to sjmp
                           1404 ;	Peephole 160.a	removed sjmp by inverse jump logic
   03AD 40 2A              1405 	jc	00107$
                           1406 ;	Peephole 300	removed redundant label 00122$
                           1407 ;	genCmpGt
                           1408 ;	genCmp
                           1409 ;	genIfxJump
                           1410 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   03AF EA                 1411 	mov	a,r2
   03B0 24 C6              1412 	add	a,#0xff - 0x39
                           1413 ;	Peephole 112.b	changed ljmp to sjmp
                           1414 ;	Peephole 160.a	removed sjmp by inverse jump logic
   03B2 40 25              1415 	jc	00107$
                           1416 ;	Peephole 300	removed redundant label 00123$
                           1417 ;	main.c:259: value=((value*10)+(i-'0')); /* Convert character to integers */
                           1418 ;	genAssign
   03B4 90 00 08           1419 	mov	dptr,#_value
   03B7 E0                 1420 	movx	a,@dptr
                           1421 ;	genMult
                           1422 ;	genMultOneByte
   03B8 FB                 1423 	mov	r3,a
                           1424 ;	Peephole 105	removed redundant mov
   03B9 75 F0 0A           1425 	mov	b,#0x0A
   03BC A4                 1426 	mul	ab
   03BD FB                 1427 	mov	r3,a
                           1428 ;	genMinus
   03BE EA                 1429 	mov	a,r2
   03BF 24 D0              1430 	add	a,#0xd0
                           1431 ;	genPlus
   03C1 90 00 08           1432 	mov	dptr,#_value
                           1433 ;	Peephole 236.a	used r3 instead of ar3
   03C4 2B                 1434 	add	a,r3
   03C5 F0                 1435 	movx	@dptr,a
                           1436 ;	main.c:260: putchar(i);
                           1437 ;	genCall
   03C6 8A 82              1438 	mov	dpl,r2
   03C8 12 04 95           1439 	lcall	_putchar
                           1440 ;	main.c:261: j++;
                           1441 ;	genAssign
   03CB 90 00 01           1442 	mov	dptr,#_j
   03CE E0                 1443 	movx	a,@dptr
   03CF FA                 1444 	mov	r2,a
                           1445 ;	genPlus
   03D0 90 00 01           1446 	mov	dptr,#_j
                           1447 ;     genPlusIncr
   03D3 74 01              1448 	mov	a,#0x01
                           1449 ;	Peephole 236.a	used r2 instead of ar2
   03D5 2A                 1450 	add	a,r2
   03D6 F0                 1451 	movx	@dptr,a
                           1452 ;	Peephole 112.b	changed ljmp to sjmp
   03D7 80 26              1453 	sjmp	00111$
   03D9                    1454 00107$:
                           1455 ;	main.c:263: else if(i==127)
                           1456 ;	genAssign
   03D9 90 00 00           1457 	mov	dptr,#_i
   03DC E0                 1458 	movx	a,@dptr
   03DD FA                 1459 	mov	r2,a
                           1460 ;	genCmpEq
                           1461 ;	gencjneshort
                           1462 ;	Peephole 112.b	changed ljmp to sjmp
                           1463 ;	Peephole 198.b	optimized misc jump sequence
   03DE BA 7F 10           1464 	cjne	r2,#0x7F,00104$
                           1465 ;	Peephole 200.b	removed redundant sjmp
                           1466 ;	Peephole 300	removed redundant label 00124$
                           1467 ;	Peephole 300	removed redundant label 00125$
                           1468 ;	main.c:265: value=value/10;
                           1469 ;	genAssign
   03E1 90 00 08           1470 	mov	dptr,#_value
   03E4 E0                 1471 	movx	a,@dptr
   03E5 FB                 1472 	mov	r3,a
                           1473 ;	genDiv
   03E6 90 00 08           1474 	mov	dptr,#_value
                           1475 ;     genDivOneByte
   03E9 75 F0 0A           1476 	mov	b,#0x0A
   03EC EB                 1477 	mov	a,r3
   03ED 84                 1478 	div	ab
   03EE F0                 1479 	movx	@dptr,a
                           1480 ;	Peephole 112.b	changed ljmp to sjmp
   03EF 80 0E              1481 	sjmp	00111$
   03F1                    1482 00104$:
                           1483 ;	main.c:267: else if(i!=13)
                           1484 ;	genCmpEq
                           1485 ;	gencjneshort
   03F1 BA 0D 02           1486 	cjne	r2,#0x0D,00126$
                           1487 ;	Peephole 112.b	changed ljmp to sjmp
   03F4 80 09              1488 	sjmp	00111$
   03F6                    1489 00126$:
                           1490 ;	main.c:268: {putstr("\n\rEnter Numbers\n\r");}
                           1491 ;	genCall
                           1492 ;	Peephole 182.a	used 16 bit load of DPTR
   03F6 90 0C CB           1493 	mov	dptr,#__str_40
   03F9 75 F0 80           1494 	mov	b,#0x80
   03FC 12 04 13           1495 	lcall	_putstr
   03FF                    1496 00111$:
                           1497 ;	main.c:269: }while(i!=13);
                           1498 ;	genAssign
   03FF 90 00 00           1499 	mov	dptr,#_i
   0402 E0                 1500 	movx	a,@dptr
   0403 FA                 1501 	mov	r2,a
                           1502 ;	genCmpEq
                           1503 ;	gencjneshort
                           1504 ;	Peephole 112.b	changed ljmp to sjmp
                           1505 ;	Peephole 198.b	optimized misc jump sequence
   0404 BA 0D 99           1506 	cjne	r2,#0x0D,00110$
                           1507 ;	Peephole 200.b	removed redundant sjmp
                           1508 ;	Peephole 300	removed redundant label 00127$
                           1509 ;	Peephole 300	removed redundant label 00128$
                           1510 ;	main.c:270: j=0;
                           1511 ;	genAssign
   0407 90 00 01           1512 	mov	dptr,#_j
                           1513 ;	Peephole 181	changed mov to clr
   040A E4                 1514 	clr	a
   040B F0                 1515 	movx	@dptr,a
                           1516 ;	main.c:271: return value;
                           1517 ;	genAssign
   040C 90 00 08           1518 	mov	dptr,#_value
   040F E0                 1519 	movx	a,@dptr
                           1520 ;	genRet
                           1521 ;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
   0410 F5 82              1522 	mov	dpl,a
                           1523 ;	Peephole 300	removed redundant label 00113$
   0412 22                 1524 	ret
                           1525 ;------------------------------------------------------------
                           1526 ;Allocation info for local variables in function 'putstr'
                           1527 ;------------------------------------------------------------
                           1528 ;ptr                       Allocated with name '_putstr_ptr_1_1'
                           1529 ;------------------------------------------------------------
                           1530 ;	main.c:274: void putstr(unsigned char *ptr)
                           1531 ;	-----------------------------------------
                           1532 ;	 function putstr
                           1533 ;	-----------------------------------------
   0413                    1534 _putstr:
                           1535 ;	genReceive
   0413 AA F0              1536 	mov	r2,b
   0415 AB 83              1537 	mov	r3,dph
   0417 E5 82              1538 	mov	a,dpl
   0419 90 00 03           1539 	mov	dptr,#_putstr_ptr_1_1
   041C F0                 1540 	movx	@dptr,a
   041D A3                 1541 	inc	dptr
   041E EB                 1542 	mov	a,r3
   041F F0                 1543 	movx	@dptr,a
   0420 A3                 1544 	inc	dptr
   0421 EA                 1545 	mov	a,r2
   0422 F0                 1546 	movx	@dptr,a
                           1547 ;	main.c:276: while(*ptr)
                           1548 ;	genAssign
   0423 90 00 03           1549 	mov	dptr,#_putstr_ptr_1_1
   0426 E0                 1550 	movx	a,@dptr
   0427 FA                 1551 	mov	r2,a
   0428 A3                 1552 	inc	dptr
   0429 E0                 1553 	movx	a,@dptr
   042A FB                 1554 	mov	r3,a
   042B A3                 1555 	inc	dptr
   042C E0                 1556 	movx	a,@dptr
   042D FC                 1557 	mov	r4,a
   042E                    1558 00101$:
                           1559 ;	genPointerGet
                           1560 ;	genGenPointerGet
   042E 8A 82              1561 	mov	dpl,r2
   0430 8B 83              1562 	mov	dph,r3
   0432 8C F0              1563 	mov	b,r4
   0434 12 04 A7           1564 	lcall	__gptrget
                           1565 ;	genIfx
   0437 FD                 1566 	mov	r5,a
                           1567 ;	Peephole 105	removed redundant mov
                           1568 ;	genIfxJump
                           1569 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0438 60 23              1570 	jz	00108$
                           1571 ;	Peephole 300	removed redundant label 00109$
                           1572 ;	main.c:278: putchar(*ptr);
                           1573 ;	genCall
   043A 8D 82              1574 	mov	dpl,r5
   043C C0 02              1575 	push	ar2
   043E C0 03              1576 	push	ar3
   0440 C0 04              1577 	push	ar4
   0442 12 04 95           1578 	lcall	_putchar
   0445 D0 04              1579 	pop	ar4
   0447 D0 03              1580 	pop	ar3
   0449 D0 02              1581 	pop	ar2
                           1582 ;	main.c:279: ptr++;
                           1583 ;	genPlus
                           1584 ;     genPlusIncr
   044B 0A                 1585 	inc	r2
   044C BA 00 01           1586 	cjne	r2,#0x00,00110$
   044F 0B                 1587 	inc	r3
   0450                    1588 00110$:
                           1589 ;	genAssign
   0450 90 00 03           1590 	mov	dptr,#_putstr_ptr_1_1
   0453 EA                 1591 	mov	a,r2
   0454 F0                 1592 	movx	@dptr,a
   0455 A3                 1593 	inc	dptr
   0456 EB                 1594 	mov	a,r3
   0457 F0                 1595 	movx	@dptr,a
   0458 A3                 1596 	inc	dptr
   0459 EC                 1597 	mov	a,r4
   045A F0                 1598 	movx	@dptr,a
                           1599 ;	Peephole 112.b	changed ljmp to sjmp
   045B 80 D1              1600 	sjmp	00101$
   045D                    1601 00108$:
                           1602 ;	genAssign
   045D 90 00 03           1603 	mov	dptr,#_putstr_ptr_1_1
   0460 EA                 1604 	mov	a,r2
   0461 F0                 1605 	movx	@dptr,a
   0462 A3                 1606 	inc	dptr
   0463 EB                 1607 	mov	a,r3
   0464 F0                 1608 	movx	@dptr,a
   0465 A3                 1609 	inc	dptr
   0466 EC                 1610 	mov	a,r4
   0467 F0                 1611 	movx	@dptr,a
                           1612 ;	Peephole 300	removed redundant label 00104$
   0468 22                 1613 	ret
                           1614 ;------------------------------------------------------------
                           1615 ;Allocation info for local variables in function 'Clock_frequency'
                           1616 ;------------------------------------------------------------
                           1617 ;i                         Allocated with name '_Clock_frequency_i_1_1'
                           1618 ;------------------------------------------------------------
                           1619 ;	main.c:284: void Clock_frequency()
                           1620 ;	-----------------------------------------
                           1621 ;	 function Clock_frequency
                           1622 ;	-----------------------------------------
   0469                    1623 _Clock_frequency:
                           1624 ;	main.c:287: CKRL=0x00;
                           1625 ;	genAssign
   0469 75 97 00           1626 	mov	_CKRL,#0x00
                           1627 ;	main.c:288: for(i=0; i<250; i++);
                           1628 ;	genAssign
   046C 7A FA              1629 	mov	r2,#0xFA
   046E                    1630 00103$:
                           1631 ;	genDjnz
                           1632 ;	Peephole 112.b	changed ljmp to sjmp
                           1633 ;	Peephole 205	optimized misc jump sequence
   046E DA FE              1634 	djnz	r2,00103$
                           1635 ;	Peephole 300	removed redundant label 00109$
                           1636 ;	Peephole 300	removed redundant label 00110$
                           1637 ;	main.c:289: CKRL = 0xFF;
                           1638 ;	genAssign
   0470 75 97 FF           1639 	mov	_CKRL,#0xFF
                           1640 ;	Peephole 300	removed redundant label 00104$
   0473 22                 1641 	ret
                           1642 ;------------------------------------------------------------
                           1643 ;Allocation info for local variables in function 'SerialInitialize'
                           1644 ;------------------------------------------------------------
                           1645 ;------------------------------------------------------------
                           1646 ;	main.c:291: void SerialInitialize()
                           1647 ;	-----------------------------------------
                           1648 ;	 function SerialInitialize
                           1649 ;	-----------------------------------------
   0474                    1650 _SerialInitialize:
                           1651 ;	main.c:293: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
                           1652 ;	genAssign
   0474 75 89 20           1653 	mov	_TMOD,#0x20
                           1654 ;	main.c:294: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
                           1655 ;	genAssign
   0477 75 8D FD           1656 	mov	_TH1,#0xFD
                           1657 ;	main.c:295: SCON=0x50;
                           1658 ;	genAssign
   047A 75 98 50           1659 	mov	_SCON,#0x50
                           1660 ;	main.c:296: TR1=1;  // to start timer
                           1661 ;	genAssign
   047D D2 8E              1662 	setb	_TR1
                           1663 ;	main.c:297: IE=0x90; // to make serial interrupt interrupt enable
                           1664 ;	genAssign
   047F 75 A8 90           1665 	mov	_IE,#0x90
                           1666 ;	Peephole 300	removed redundant label 00101$
   0482 22                 1667 	ret
                           1668 ;------------------------------------------------------------
                           1669 ;Allocation info for local variables in function 'getchar'
                           1670 ;------------------------------------------------------------
                           1671 ;rec                       Allocated with name '_getchar_rec_1_1'
                           1672 ;------------------------------------------------------------
                           1673 ;	main.c:301: unsigned char getchar()
                           1674 ;	-----------------------------------------
                           1675 ;	 function getchar
                           1676 ;	-----------------------------------------
   0483                    1677 _getchar:
                           1678 ;	main.c:305: while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
   0483                    1679 00101$:
                           1680 ;	genIfx
                           1681 ;	genIfxJump
                           1682 ;	Peephole 108.d	removed ljmp by inverse jump logic
   0483 30 98 FD           1683 	jnb	_RI,00101$
                           1684 ;	Peephole 300	removed redundant label 00108$
                           1685 ;	main.c:306: rec=SBUF;   // data is received in SBUF register present in controller during receiving
                           1686 ;	genAssign
   0486 90 00 06           1687 	mov	dptr,#_getchar_rec_1_1
   0489 E5 99              1688 	mov	a,_SBUF
   048B F0                 1689 	movx	@dptr,a
                           1690 ;	main.c:307: RI=0;  // make RI bit to 0 so that next data is received
                           1691 ;	genAssign
   048C C2 98              1692 	clr	_RI
                           1693 ;	main.c:308: return rec;  // return rec where function is called
                           1694 ;	genAssign
   048E 90 00 06           1695 	mov	dptr,#_getchar_rec_1_1
   0491 E0                 1696 	movx	a,@dptr
                           1697 ;	genRet
                           1698 ;	Peephole 234.a	loading dpl directly from a(ccumulator), r2 not set
   0492 F5 82              1699 	mov	dpl,a
                           1700 ;	Peephole 300	removed redundant label 00104$
   0494 22                 1701 	ret
                           1702 ;------------------------------------------------------------
                           1703 ;Allocation info for local variables in function 'putchar'
                           1704 ;------------------------------------------------------------
                           1705 ;x                         Allocated with name '_putchar_x_1_1'
                           1706 ;------------------------------------------------------------
                           1707 ;	main.c:311: void putchar(uint8_t x)
                           1708 ;	-----------------------------------------
                           1709 ;	 function putchar
                           1710 ;	-----------------------------------------
   0495                    1711 _putchar:
                           1712 ;	genReceive
   0495 E5 82              1713 	mov	a,dpl
   0497 90 00 07           1714 	mov	dptr,#_putchar_x_1_1
   049A F0                 1715 	movx	@dptr,a
                           1716 ;	main.c:313: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
                           1717 ;	genAssign
   049B 90 00 07           1718 	mov	dptr,#_putchar_x_1_1
   049E E0                 1719 	movx	a,@dptr
   049F F5 99              1720 	mov	_SBUF,a
                           1721 ;	main.c:314: while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
   04A1                    1722 00101$:
                           1723 ;	genIfx
                           1724 ;	genIfxJump
                           1725 ;	Peephole 108.d	removed ljmp by inverse jump logic
                           1726 ;	main.c:315: TI=0; // make TI bit to zero for next transmission
                           1727 ;	genAssign
                           1728 ;	Peephole 250.a	using atomic test and clear
   04A1 10 99 02           1729 	jbc	_TI,00108$
   04A4 80 FB              1730 	sjmp	00101$
   04A6                    1731 00108$:
                           1732 ;	Peephole 300	removed redundant label 00104$
   04A6 22                 1733 	ret
                           1734 	.area CSEG    (CODE)
                           1735 	.area CONST   (CODE)
   04C7                    1736 __str_0:
   04C7 0A                 1737 	.db 0x0A
   04C8 0D                 1738 	.db 0x0D
   04C9 00                 1739 	.db 0x00
   04CA                    1740 __str_1:
   04CA 48 65 6C 6C 6F 20  1741 	.ascii "Hello "
   04D0 0A                 1742 	.db 0x0A
   04D1 0D                 1743 	.db 0x0D
   04D2 00                 1744 	.db 0x00
   04D3                    1745 __str_2:
   04D3 50 72 65 73 73 20  1746 	.ascii "Press '>' for help menu"
        27 3E 27 20 66 6F
        72 20 68 65 6C 70
        20 6D 65 6E 75
   04EA 0A                 1747 	.db 0x0A
   04EB 0D                 1748 	.db 0x0D
   04EC 00                 1749 	.db 0x00
   04ED                    1750 __str_3:
   04ED 45 6E 74 65 72 20  1751 	.ascii "Enter the character "
        74 68 65 20 63 68
        61 72 61 63 74 65
        72 20
   0501 0A                 1752 	.db 0x0A
   0502 0D                 1753 	.db 0x0D
   0503 00                 1754 	.db 0x00
   0504                    1755 __str_4:
   0504 43 6F 6E 66 69 67  1756 	.ascii "Configuring PCA module 0 in PWM mode. Check P1.3 "
        75 72 69 6E 67 20
        50 43 41 20 6D 6F
        64 75 6C 65 20 30
        20 69 6E 20 50 57
        4D 20 6D 6F 64 65
        2E 20 43 68 65 63
        6B 20 50 31 2E 33
        20
   0535 0A                 1757 	.db 0x0A
   0536 0D                 1758 	.db 0x0D
   0537 00                 1759 	.db 0x00
   0538                    1760 __str_5:
   0538 50 57 4D 20 61 6C  1761 	.ascii "PWM already configured on P1.3 "
        72 65 61 64 79 20
        63 6F 6E 66 69 67
        75 72 65 64 20 6F
        6E 20 50 31 2E 33
        20
   0557 0A                 1762 	.db 0x0A
   0558 0D                 1763 	.db 0x0D
   0559 00                 1764 	.db 0x00
   055A                    1765 __str_6:
   055A 44 69 73 61 62 6C  1766 	.ascii "Disabling PWM mode on P1.3 "
        69 6E 67 20 50 57
        4D 20 6D 6F 64 65
        20 6F 6E 20 50 31
        2E 33 20
   0575 0A                 1767 	.db 0x0A
   0576 0D                 1768 	.db 0x0D
   0577 00                 1769 	.db 0x00
   0578                    1770 __str_7:
   0578 50 57 4D 20 6D 6F  1771 	.ascii "PWM mode on P1.3 is already disabled"
        64 65 20 6F 6E 20
        50 31 2E 33 20 69
        73 20 61 6C 72 65
        61 64 79 20 64 69
        73 61 62 6C 65 64
   059C 0A                 1772 	.db 0x0A
   059D 0D                 1773 	.db 0x0D
   059E 00                 1774 	.db 0x00
   059F                    1775 __str_8:
   059F 43 6F 6E 66 69 67  1776 	.ascii "Configuring PCA module 1 in High Speed mode. Check P1.4 "
        75 72 69 6E 67 20
        50 43 41 20 6D 6F
        64 75 6C 65 20 31
        20 69 6E 20 48 69
        67 68 20 53 70 65
        65 64 20 6D 6F 64
        65 2E 20 43 68 65
        63 6B 20 50 31 2E
        34 20
   05D7 0A                 1777 	.db 0x0A
   05D8 0D                 1778 	.db 0x0D
   05D9 00                 1779 	.db 0x00
   05DA                    1780 __str_9:
   05DA 48 69 67 68 20 53  1781 	.ascii "High Speed mode is already configured. Check P1.4 "
        70 65 65 64 20 6D
        6F 64 65 20 69 73
        20 61 6C 72 65 61
        64 79 20 63 6F 6E
        66 69 67 75 72 65
        64 2E 20 43 68 65
        63 6B 20 50 31 2E
        34 20
   060C 0A                 1782 	.db 0x0A
   060D 0D                 1783 	.db 0x0D
   060E 00                 1784 	.db 0x00
   060F                    1785 __str_10:
   060F 44 69 73 61 62 6C  1786 	.ascii "Disabling the High Speed mode on P1.4 "
        69 6E 67 20 74 68
        65 20 48 69 67 68
        20 53 70 65 65 64
        20 6D 6F 64 65 20
        6F 6E 20 50 31 2E
        34 20
   0635 0A                 1787 	.db 0x0A
   0636 0D                 1788 	.db 0x0D
   0637 00                 1789 	.db 0x00
   0638                    1790 __str_11:
   0638 48 69 67 68 20 53  1791 	.ascii "High Speed mode on P1.4 is already disabled "
        70 65 65 64 20 6D
        6F 64 65 20 6F 6E
        20 50 31 2E 34 20
        69 73 20 61 6C 72
        65 61 64 79 20 64
        69 73 61 62 6C 65
        64 20
   0664 0A                 1792 	.db 0x0A
   0665 0D                 1793 	.db 0x0D
   0666 00                 1794 	.db 0x00
   0667                    1795 __str_12:
   0667 45 6E 74 65 72 69  1796 	.ascii "Entering into Idle Mode. Press any key or generate external "
        6E 67 20 69 6E 74
        6F 20 49 64 6C 65
        20 4D 6F 64 65 2E
        20 50 72 65 73 73
        20 61 6E 79 20 6B
        65 79 20 6F 72 20
        67 65 6E 65 72 61
        74 65 20 65 78 74
        65 72 6E 61 6C 20
   06A3 69 6E 74 65 72 72  1797 	.ascii "interrupt0 to come out of it "
        75 70 74 30 20 74
        6F 20 63 6F 6D 65
        20 6F 75 74 20 6F
        66 20 69 74 20
   06C0 0A                 1798 	.db 0x0A
   06C1 0D                 1799 	.db 0x0D
   06C2 00                 1800 	.db 0x00
   06C3                    1801 __str_13:
   06C3 43 61 6D 65 20 6F  1802 	.ascii "Came out of Idle mode"
        75 74 20 6F 66 20
        49 64 6C 65 20 6D
        6F 64 65
   06D8 0A                 1803 	.db 0x0A
   06D9 0D                 1804 	.db 0x0D
   06DA 00                 1805 	.db 0x00
   06DB                    1806 __str_14:
   06DB 45 6E 74 65 72 69  1807 	.ascii "Entering Power-down mode. Generate external interrupt0 to co"
        6E 67 20 50 6F 77
        65 72 2D 64 6F 77
        6E 20 6D 6F 64 65
        2E 20 47 65 6E 65
        72 61 74 65 20 65
        78 74 65 72 6E 61
        6C 20 69 6E 74 65
        72 72 75 70 74 30
        20 74 6F 20 63 6F
   0717 6D 65 20 6F 75 74  1808 	.ascii "me out of it "
        20 6F 66 20 69 74
        20
   0724 0A                 1809 	.db 0x0A
   0725 0D                 1810 	.db 0x0D
   0726 00                 1811 	.db 0x00
   0727                    1812 __str_15:
   0727 43 61 6D 65 20 6F  1813 	.ascii "Came out of power down mode"
        75 74 20 6F 66 20
        70 6F 77 65 72 20
        64 6F 77 6E 20 6D
        6F 64 65
   0742 0A                 1814 	.db 0x0A
   0743 0D                 1815 	.db 0x0D
   0744 00                 1816 	.db 0x00
   0745                    1817 __str_16:
   0745 43 68 61 6E 67 69  1818 	.ascii "Changing the F(clkperiph) to minimum frequency"
        6E 67 20 74 68 65
        20 46 28 63 6C 6B
        70 65 72 69 70 68
        29 20 74 6F 20 6D
        69 6E 69 6D 75 6D
        20 66 72 65 71 75
        65 6E 63 79
   0773 0A                 1819 	.db 0x0A
   0774 0D                 1820 	.db 0x0D
   0775 00                 1821 	.db 0x00
   0776                    1822 __str_17:
   0776 57 61 74 63 68 64  1823 	.ascii "Watchdog timer is set. Controller will be reset"
        6F 67 20 74 69 6D
        65 72 20 69 73 20
        73 65 74 2E 20 43
        6F 6E 74 72 6F 6C
        6C 65 72 20 77 69
        6C 6C 20 62 65 20
        72 65 73 65 74
   07A5 0A                 1824 	.db 0x0A
   07A6 0D                 1825 	.db 0x0D
   07A7 00                 1826 	.db 0x00
   07A8                    1827 __str_18:
   07A8 47 6F 69 6E 67 20  1828 	.ascii "Going to minimum frequency mode"
        74 6F 20 6D 69 6E
        69 6D 75 6D 20 66
        72 65 71 75 65 6E
        63 79 20 6D 6F 64
        65
   07C7 0A                 1829 	.db 0x0A
   07C8 0D                 1830 	.db 0x0D
   07C9 00                 1831 	.db 0x00
   07CA                    1832 __str_19:
   07CA 43 6F 6D 69 6E 67  1833 	.ascii "Coming back to maximum frequency mode"
        20 62 61 63 6B 20
        74 6F 20 6D 61 78
        69 6D 75 6D 20 66
        72 65 71 75 65 6E
        63 79 20 6D 6F 64
        65
   07EF 0A                 1834 	.db 0x0A
   07F0 0D                 1835 	.db 0x0D
   07F1 00                 1836 	.db 0x00
   07F2                    1837 __str_20:
   07F2 0A                 1838 	.db 0x0A
   07F3 0D                 1839 	.db 0x0D
   07F4 45 6E 74 65 72 20  1840 	.ascii "Enter valid input"
        76 61 6C 69 64 20
        69 6E 70 75 74
   0805 0A                 1841 	.db 0x0A
   0806 0D                 1842 	.db 0x0D
   0807 00                 1843 	.db 0x00
   0808                    1844 __str_21:
   0808 0A                 1845 	.db 0x0A
   0809 0D                 1846 	.db 0x0D
   080A 7E 7E 7E 7E 7E 7E  1847 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E
   0844 7E 7E 7E 7E 7E 7E  1848 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E
   0876 0A                 1849 	.db 0x0A
   0877 0D                 1850 	.db 0x0D
   0878 00                 1851 	.db 0x00
   0879                    1852 __str_22:
   0879 57 65 6C 63 6F 6D  1853 	.ascii "Welcome to the Help Menu"
        65 20 74 6F 20 74
        68 65 20 48 65 6C
        70 20 4D 65 6E 75
   0891 0A                 1854 	.db 0x0A
   0892 0D                 1855 	.db 0x0D
   0893 00                 1856 	.db 0x00
   0894                    1857 __str_23:
   0894 50 72 65 73 73 20  1858 	.ascii "Press 'E' for enabling the PWM output on P1.3"
        27 45 27 20 66 6F
        72 20 65 6E 61 62
        6C 69 6E 67 20 74
        68 65 20 50 57 4D
        20 6F 75 74 70 75
        74 20 6F 6E 20 50
        31 2E 33
   08C1 0A                 1859 	.db 0x0A
   08C2 0D                 1860 	.db 0x0D
   08C3 00                 1861 	.db 0x00
   08C4                    1862 __str_24:
   08C4 50 72 65 73 73 20  1863 	.ascii "Press 'D' for disabling the PWM output on P1.3"
        27 44 27 20 66 6F
        72 20 64 69 73 61
        62 6C 69 6E 67 20
        74 68 65 20 50 57
        4D 20 6F 75 74 70
        75 74 20 6F 6E 20
        50 31 2E 33
   08F2 0A                 1864 	.db 0x0A
   08F3 0D                 1865 	.db 0x0D
   08F4 00                 1866 	.db 0x00
   08F5                    1867 __str_25:
   08F5 50 72 65 73 73 20  1868 	.ascii "Press 'H' for enabling the High Speed output on P1.4"
        27 48 27 20 66 6F
        72 20 65 6E 61 62
        6C 69 6E 67 20 74
        68 65 20 48 69 67
        68 20 53 70 65 65
        64 20 6F 75 74 70
        75 74 20 6F 6E 20
        50 31 2E 34
   0929 0A                 1869 	.db 0x0A
   092A 0D                 1870 	.db 0x0D
   092B 00                 1871 	.db 0x00
   092C                    1872 __str_26:
   092C 50 72 65 73 73 20  1873 	.ascii "Press 'S' for disabling the High Speed output on P1.4"
        27 53 27 20 66 6F
        72 20 64 69 73 61
        62 6C 69 6E 67 20
        74 68 65 20 48 69
        67 68 20 53 70 65
        65 64 20 6F 75 74
        70 75 74 20 6F 6E
        20 50 31 2E 34
   0961 0A                 1874 	.db 0x0A
   0962 0D                 1875 	.db 0x0D
   0963 00                 1876 	.db 0x00
   0964                    1877 __str_27:
   0964 50 72 65 73 73 20  1878 	.ascii "Press 'I' to enter into IDLE mode"
        27 49 27 20 74 6F
        20 65 6E 74 65 72
        20 69 6E 74 6F 20
        49 44 4C 45 20 6D
        6F 64 65
   0985 0A                 1879 	.db 0x0A
   0986 0D                 1880 	.db 0x0D
   0987 00                 1881 	.db 0x00
   0988                    1882 __str_28:
   0988 50 72 65 73 73 20  1883 	.ascii "Press 'P' to enter into Power down mode"
        27 50 27 20 74 6F
        20 65 6E 74 65 72
        20 69 6E 74 6F 20
        50 6F 77 65 72 20
        64 6F 77 6E 20 6D
        6F 64 65
   09AF 0A                 1884 	.db 0x0A
   09B0 0D                 1885 	.db 0x0D
   09B1 00                 1886 	.db 0x00
   09B2                    1887 __str_29:
   09B2 50 72 65 73 73 20  1888 	.ascii "Press 'C' to Set F(clkperiph) at the minimum frequency suppo"
        27 43 27 20 74 6F
        20 53 65 74 20 46
        28 63 6C 6B 70 65
        72 69 70 68 29 20
        61 74 20 74 68 65
        20 6D 69 6E 69 6D
        75 6D 20 66 72 65
        71 75 65 6E 63 79
        20 73 75 70 70 6F
   09EE 72 74 65 64 20 62  1889 	.ascii "rted by the CKRL register"
        79 20 74 68 65 20
        43 4B 52 4C 20 72
        65 67 69 73 74 65
        72
   0A07 0A                 1890 	.db 0x0A
   0A08 0D                 1891 	.db 0x0D
   0A09 00                 1892 	.db 0x00
   0A0A                    1893 __str_30:
   0A0A 50 72 65 73 73 20  1894 	.ascii "Press 'N' to Set F(clkperiph) at the maximum frequency suppo"
        27 4E 27 20 74 6F
        20 53 65 74 20 46
        28 63 6C 6B 70 65
        72 69 70 68 29 20
        61 74 20 74 68 65
        20 6D 61 78 69 6D
        75 6D 20 66 72 65
        71 75 65 6E 63 79
        20 73 75 70 70 6F
   0A46 72 74 65 64 20 62  1895 	.ascii "rted by the CKRL register"
        79 20 74 68 65 20
        43 4B 52 4C 20 72
        65 67 69 73 74 65
        72
   0A5F 0A                 1896 	.db 0x0A
   0A60 0D                 1897 	.db 0x0D
   0A61 00                 1898 	.db 0x00
   0A62                    1899 __str_31:
   0A62 50 72 65 73 73 20  1900 	.ascii "Press 'F' to go into the minimum frequency for short amount "
        27 46 27 20 74 6F
        20 67 6F 20 69 6E
        74 6F 20 74 68 65
        20 6D 69 6E 69 6D
        75 6D 20 66 72 65
        71 75 65 6E 63 79
        20 66 6F 72 20 73
        68 6F 72 74 20 61
        6D 6F 75 6E 74 20
   0A9E 6F 66 20 70 65 72  1901 	.ascii "of period and then come back to normal"
        69 6F 64 20 61 6E
        64 20 74 68 65 6E
        20 63 6F 6D 65 20
        62 61 63 6B 20 74
        6F 20 6E 6F 72 6D
        61 6C
   0AC4 0A                 1902 	.db 0x0A
   0AC5 0D                 1903 	.db 0x0D
   0AC6 00                 1904 	.db 0x00
   0AC7                    1905 __str_32:
   0AC7 50 72 65 73 73 20  1906 	.ascii "Press 'W' to enable the watchdog timer"
        27 57 27 20 74 6F
        20 65 6E 61 62 6C
        65 20 74 68 65 20
        77 61 74 63 68 64
        6F 67 20 74 69 6D
        65 72
   0AED 0A                 1907 	.db 0x0A
   0AEE 0D                 1908 	.db 0x0D
   0AEF 00                 1909 	.db 0x00
   0AF0                    1910 __str_33:
   0AF0 4E 6F 74 65 3A 20  1911 	.ascii "Note: "
   0AF6 0A                 1912 	.db 0x0A
   0AF7 0D                 1913 	.db 0x0D
   0AF8 00                 1914 	.db 0x00
   0AF9                    1915 __str_34:
   0AF9 31 2E 20 54 6F 20  1916 	.ascii "1. To come out of IDLE mode, press any key on the keyboard o"
        63 6F 6D 65 20 6F
        75 74 20 6F 66 20
        49 44 4C 45 20 6D
        6F 64 65 2C 20 70
        72 65 73 73 20 61
        6E 79 20 6B 65 79
        20 6F 6E 20 74 68
        65 20 6B 65 79 62
        6F 61 72 64 20 6F
   0B35 72 20 67 65 6E 65  1917 	.ascii "r generate external interrupt0"
        72 61 74 65 20 65
        78 74 65 72 6E 61
        6C 20 69 6E 74 65
        72 72 75 70 74 30
   0B53 0A                 1918 	.db 0x0A
   0B54 0D                 1919 	.db 0x0D
   0B55 00                 1920 	.db 0x00
   0B56                    1921 __str_35:
   0B56 32 2E 20 54 6F 20  1922 	.ascii "2. To come out of Power down mode, generate external interru"
        63 6F 6D 65 20 6F
        75 74 20 6F 66 20
        50 6F 77 65 72 20
        64 6F 77 6E 20 6D
        6F 64 65 2C 20 67
        65 6E 65 72 61 74
        65 20 65 78 74 65
        72 6E 61 6C 20 69
        6E 74 65 72 72 75
   0B92 70 74 30           1923 	.ascii "pt0"
   0B95 0A                 1924 	.db 0x0A
   0B96 0D                 1925 	.db 0x0D
   0B97 00                 1926 	.db 0x00
   0B98                    1927 __str_36:
   0B98 33 2E 20 57 68 65  1928 	.ascii "3. When you change F(clkperiph) to minimum, the baud rate ch"
        6E 20 79 6F 75 20
        63 68 61 6E 67 65
        20 46 28 63 6C 6B
        70 65 72 69 70 68
        29 20 74 6F 20 6D
        69 6E 69 6D 75 6D
        2C 20 74 68 65 20
        62 61 75 64 20 72
        61 74 65 20 63 68
   0BD4 61 6E 67 65 73 2E  1929 	.ascii "anges. Hence set-"
        20 48 65 6E 63 65
        20 73 65 74 2D
   0BE5 0A                 1930 	.db 0x0A
   0BE6 0D                 1931 	.db 0x0D
   0BE7 00                 1932 	.db 0x00
   0BE8                    1933 __str_37:
   0BE8 20 20 20 2D 74 68  1934 	.ascii "   -the baud rate as '18' in your serial terminal. And, when"
        65 20 62 61 75 64
        20 72 61 74 65 20
        61 73 20 27 31 38
        27 20 69 6E 20 79
        6F 75 72 20 73 65
        72 69 61 6C 20 74
        65 72 6D 69 6E 61
        6C 2E 20 41 6E 64
        2C 20 77 68 65 6E
   0C24 20 79 6F 75 20 63  1935 	.ascii " you come back to-"
        6F 6D 65 20 62 61
        63 6B 20 74 6F 2D
   0C36 0A                 1936 	.db 0x0A
   0C37 0D                 1937 	.db 0x0D
   0C38 00                 1938 	.db 0x00
   0C39                    1939 __str_38:
   0C39 20 20 20 2D 6E 6F  1940 	.ascii "   -normal(maximum) frequency, change the baud rate to norma"
        72 6D 61 6C 28 6D
        61 78 69 6D 75 6D
        29 20 66 72 65 71
        75 65 6E 63 79 2C
        20 63 68 61 6E 67
        65 20 74 68 65 20
        62 61 75 64 20 72
        61 74 65 20 74 6F
        20 6E 6F 72 6D 61
   0C75 6C                 1941 	.ascii "l"
   0C76 0A                 1942 	.db 0x0A
   0C77 0D                 1943 	.db 0x0D
   0C78 00                 1944 	.db 0x00
   0C79                    1945 __str_39:
   0C79 34 2E 20 4F 6E 63  1946 	.ascii "4. Once you enable the watchdog timer, the controller gets r"
        65 20 79 6F 75 20
        65 6E 61 62 6C 65
        20 74 68 65 20 77
        61 74 63 68 64 6F
        67 20 74 69 6D 65
        72 2C 20 74 68 65
        20 63 6F 6E 74 72
        6F 6C 6C 65 72 20
        67 65 74 73 20 72
   0CB5 65 73 65 74 20 77  1947 	.ascii "eset within seconds"
        69 74 68 69 6E 20
        73 65 63 6F 6E 64
        73
   0CC8 0A                 1948 	.db 0x0A
   0CC9 0D                 1949 	.db 0x0D
   0CCA 00                 1950 	.db 0x00
   0CCB                    1951 __str_40:
   0CCB 0A                 1952 	.db 0x0A
   0CCC 0D                 1953 	.db 0x0D
   0CCD 45 6E 74 65 72 20  1954 	.ascii "Enter Numbers"
        4E 75 6D 62 65 72
        73
   0CDA 0A                 1955 	.db 0x0A
   0CDB 0D                 1956 	.db 0x0D
   0CDC 00                 1957 	.db 0x00
                           1958 	.area XINIT   (CODE)
   0CDD                    1959 __xinit__value:
   0CDD 00                 1960 	.db #0x00
