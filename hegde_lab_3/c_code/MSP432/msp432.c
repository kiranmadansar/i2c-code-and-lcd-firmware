/*
 * UNIVERSITY OF COLORADO BOULDER
 *
 * SUBMITTED AS PART OF THE LAB3 SUPPLEMENTAL ELEMENT SUBMISSION FOR ECEN_5613
 *
 * THE PROGRAM CONTAINS 4 MOULES.
 * 1. BLINK AN LED USING TIMER INTERRUPT
 * 2. CONTROL THE LED USING INTERRUPT FROM BUTTON
 * 3. UART COMMUNICATION
 * 4. TEMPERATURE CALCULATION USING IN-BUILT TEMPERATURE SENSOR
 * */

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "msp.h"
#include <stdlib.h>
int c=0;
uint8_t TXData = 1;     /*gLOBAL DECLARATION OF VARIABLES */
volatile long temp;
volatile float IntDegF;   /*USED IN TEMPERATURE CALCULAION*/
volatile float IntDegC;
char buffer[20]="\0";

main(void) {
    volatile uint32_t i;

    // Variables to store the ADC temperature reference calibration value
    int32_t adcRefTempCal_1_2v_30;
    int32_t adcRefTempCal_1_2v_85;

    WDT_A->CTL = WDT_A_CTL_PW |             // Stop WDT
            WDT_A_CTL_HOLD;

    // Read the ADC temperature reference calibration value
        adcRefTempCal_1_2v_30 = TLV->ADC14_REF1P2V_TS30C;
        adcRefTempCal_1_2v_85 = TLV->ADC14_REF1P2V_TS85C;

    // Configure GPIO
    P1->DIR |= BIT0;
    P1->OUT |= BIT0;

    P1->IE |= BIT1;
    P1->REN |= BIT1;
    P1->IFG &= ~BIT1;

    P2->DIR |= BIT0;
    P2->OUT |= BIT0;
    P2->OUT ^= BIT0;

    CS->KEY = CS_KEY_VAL;                   // Unlock CS module for register access
        CS->CTL0 = 0;                           // Reset tuning parameters
        CS->CTL0 = CS_CTL0_DCORSEL_3;           // Set DCO to 12MHz (nominal, center of 8-16MHz range)
        CS->CTL1 = CS_CTL1_SELA_2 |             // Select ACLK = REFO
                CS_CTL1_SELS_3 |                // SMCLK = DCO
                CS_CTL1_SELM_3;                 // MCLK = DCO
        CS->KEY = 0;

        P1->SEL0 |= BIT2 | BIT3;                // set 2-UART pin as secondary function

            // Configure UART
            EUSCI_A0->CTLW0 |= EUSCI_A_CTLW0_SWRST; // Put eUSCI in reset
            EUSCI_A0->CTLW0 = EUSCI_A_CTLW0_SWRST | // Remain eUSCI in reset
                    EUSCI_B_CTLW0_SSEL__SMCLK;      // Configure eUSCI clock source for SMCLK
            // Baud Rate calculation
            // 12000000/(16*9600) = 78.125
            // Fractional portion = 0.125
            // User's Guide Table 21-4: UCBRSx = 0x10
            // UCBRFx = int ( (78.125-78)*16) = 2
            EUSCI_A0->BRW = 78;                     // 12000000/16/9600
            EUSCI_A0->MCTLW = (2 << EUSCI_A_MCTLW_BRF_OFS) |
                    EUSCI_A_MCTLW_OS16;

            EUSCI_A0->CTLW0 &= ~EUSCI_A_CTLW0_SWRST; // Initialize eUSCI
            EUSCI_A0->IFG &= ~EUSCI_A_IFG_RXIFG;    // Clear eUSCI RX interrupt flag
            EUSCI_A0->IE |= EUSCI_A_IE_RXIE;        // Enable USCI_A0 RX interrupt

            // Enable sleep on exit from ISR
            //SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;



            // Initialize the shared reference module
                // By default, REFMSTR=1 => REFCTL is used to configure the internal reference
                while(REF_A->CTL0 & REF_A_CTL0_GENBUSY);// If ref generator busy, WAIT
                REF_A->CTL0 |= REF_A_CTL0_VSEL_0 |      // Enable internal 1.2V reference
                        REF_A_CTL0_ON;                  // Turn reference on

                REF_A->CTL0 &= ~REF_A_CTL0_TCOFF;       // Enable temperature sensor

                // Configure ADC - Pulse sample mode; ADC14_CTL0_SC trigger
                ADC14->CTL0 |= ADC14_CTL0_SHT0_6 |      // ADC ON,temperature sample period>5us
                        ADC14_CTL0_ON |
                        ADC14_CTL0_SHP;
                ADC14->CTL1 |= ADC14_CTL1_TCMAP;        // Enable internal temperature sensor
                ADC14->MCTL[0] = ADC14_MCTLN_VRSEL_1 |  // ADC input ch A22 => temp sense
                        ADC14_MCTLN_INCH_22;
                ADC14->IER0 = 0x0001;                   // ADC_IFG upon conv result-ADCMEM0

                // Wait for reference generator to settle
                while(!(REF_A->CTL0 & REF_A_CTL0_GENRDY));

                ADC14->CTL0 |= ADC14_CTL0_ENC;



        // Enable global interrupt

    //SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;    // Enable sleep on exit from ISR

    // Enable global interrupt
    __enable_irq();

    NVIC->ISER[0] = 1 << ((TA0_0_IRQn) & 31);   /*ENABLE TIMER INTERRUPT*/
    NVIC->ISER[1]= 1 << ((PORT1_IRQn) & 31);    /*PORT1 INTERRUPT ENABLE*/
    NVIC->ISER[0] = 1 << ((EUSCIA0_IRQn) & 31); /*SERIAL INTERRUPT ENABLE*/
    NVIC->ISER[0] = 1 << ((ADC14_IRQn) & 31);// Enable ADC interrupt in NVIC module

    TIMER_A0->CCTL[0] &= ~TIMER_A_CCTLN_CCIFG;
    TIMER_A0->CCTL[0] = TIMER_A_CCTLN_CCIE; // TACCR0 interrupt enabled
    TIMER_A0->CCR[0] = 0xFFFF;
    TIMER_A0->CTL = TIMER_A_CTL_SSEL__SMCLK | // SMCLK, UP mode
            TIMER_A_CTL_MC__UP;

    while(1)
    {
        //ADC14->CTL0 |= ADC14_CTL0_SC;       // Sampling and conversion start
        //__sleep();
               //__no_operation();                   // Only for debugger

               // Temperature in Celsius
               // The temperature (Temp, C)=
               IntDegC = (((float) temp - adcRefTempCal_1_2v_30) * (85 - 30)) /
                       (adcRefTempCal_1_2v_85 - adcRefTempCal_1_2v_30) + 30.0f;

               // Temperature in Fahrenheit
               // Tf = (9/5)*Tc | 32
               IntDegF = ((9 * IntDegC) / 5) + 32;
               ltoa(IntDegF, buffer);
               __no_operation();                   // Only for debugger
    }

}
// Timer A0_0 interrupt service routine
void PORT1_IRQHandler(void){
    //if(P1->4 == 1)
        P2->OUT ^= BIT0;
        P1->IFG &= ~BIT1;
        P1->IES ^= BIT1;
}
void TA0_0_IRQHandler(void) {
    // Clear the compare interrupt flag
    TIMER_A0->CCTL[0] &= ~TIMER_A_CCTLN_CCIFG;
    c++;
    if(c==20)
        {P1->OUT ^= BIT0;
            c = 0; }// Toggle P1.0 LED
        }

void EUSCIA0_IRQHandler(void)
{
    if (EUSCI_A0->IFG & EUSCI_A_IFG_RXIFG)
    {
        if((EUSCI_A0->RXBUF)==0x1B)     /*IF ESC IS PRESSED PRINT TEMPERATURE*/
        {ADC14->CTL0 |= ADC14_CTL0_SC;
            // Check if the TX buffer is empty first
        while(!(EUSCI_A0->IFG & EUSCI_A_IFG_TXIFG));

        // Echo the received character back
        EUSCI_A0->TXBUF = buffer[0];
        while(!(EUSCI_A0->IFG & EUSCI_A_IFG_TXIFG));

                // Echo the received character back
                EUSCI_A0->TXBUF = buffer[1];
        }
        else
        {while(!(EUSCI_A0->IFG & EUSCI_A_IFG_TXIFG));  /*ELSE, PRINT THE INCOMING CHARACTER*/

                // Echo the received character back
                EUSCI_A0->TXBUF = EUSCI_A0->RXBUF;}

    }
}

void ADC14_IRQHandler(void)
{
    if (ADC14->IFGR0 & ADC14_IFGR0_IFG0)        /* IRQ HANDLER TO READ TEMPERATURE FROM INBUILT SENSOR*/
    {
        temp = ADC14->MEM[0];
    }
}
