                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : FreeWare ANSI-C Compiler
                              3 ; Version 2.6.0 #4309 (Jul 28 2006)
                              4 ; This file generated Sun Oct 22 10:03:12 2017
                              5 ;--------------------------------------------------------
                              6 	.module main
                              7 	.optsdcc -mmcs51 --model-large
                              8 	
                              9 ;--------------------------------------------------------
                             10 ; Public variables in this module
                             11 ;--------------------------------------------------------
                             12 	.globl _main
                             13 	.globl __sdcc_external_startup
                             14 	.globl _CY
                             15 	.globl _AC
                             16 	.globl _F0
                             17 	.globl _RS1
                             18 	.globl _RS0
                             19 	.globl _OV
                             20 	.globl _F1
                             21 	.globl _P
                             22 	.globl _PS
                             23 	.globl _PT1
                             24 	.globl _PX1
                             25 	.globl _PT0
                             26 	.globl _PX0
                             27 	.globl _RD
                             28 	.globl _WR
                             29 	.globl _T1
                             30 	.globl _T0
                             31 	.globl _INT1
                             32 	.globl _INT0
                             33 	.globl _TXD
                             34 	.globl _RXD
                             35 	.globl _P3_7
                             36 	.globl _P3_6
                             37 	.globl _P3_5
                             38 	.globl _P3_4
                             39 	.globl _P3_3
                             40 	.globl _P3_2
                             41 	.globl _P3_1
                             42 	.globl _P3_0
                             43 	.globl _EA
                             44 	.globl _ES
                             45 	.globl _ET1
                             46 	.globl _EX1
                             47 	.globl _ET0
                             48 	.globl _EX0
                             49 	.globl _P2_7
                             50 	.globl _P2_6
                             51 	.globl _P2_5
                             52 	.globl _P2_4
                             53 	.globl _P2_3
                             54 	.globl _P2_2
                             55 	.globl _P2_1
                             56 	.globl _P2_0
                             57 	.globl _SM0
                             58 	.globl _SM1
                             59 	.globl _SM2
                             60 	.globl _REN
                             61 	.globl _TB8
                             62 	.globl _RB8
                             63 	.globl _TI
                             64 	.globl _RI
                             65 	.globl _P1_7
                             66 	.globl _P1_6
                             67 	.globl _P1_5
                             68 	.globl _P1_4
                             69 	.globl _P1_3
                             70 	.globl _P1_2
                             71 	.globl _P1_1
                             72 	.globl _P1_0
                             73 	.globl _TF1
                             74 	.globl _TR1
                             75 	.globl _TF0
                             76 	.globl _TR0
                             77 	.globl _IE1
                             78 	.globl _IT1
                             79 	.globl _IE0
                             80 	.globl _IT0
                             81 	.globl _P0_7
                             82 	.globl _P0_6
                             83 	.globl _P0_5
                             84 	.globl _P0_4
                             85 	.globl _P0_3
                             86 	.globl _P0_2
                             87 	.globl _P0_1
                             88 	.globl _P0_0
                             89 	.globl _TXD0
                             90 	.globl _RXD0
                             91 	.globl _BREG_F7
                             92 	.globl _BREG_F6
                             93 	.globl _BREG_F5
                             94 	.globl _BREG_F4
                             95 	.globl _BREG_F3
                             96 	.globl _BREG_F2
                             97 	.globl _BREG_F1
                             98 	.globl _BREG_F0
                             99 	.globl _P5_7
                            100 	.globl _P5_6
                            101 	.globl _P5_5
                            102 	.globl _P5_4
                            103 	.globl _P5_3
                            104 	.globl _P5_2
                            105 	.globl _P5_1
                            106 	.globl _P5_0
                            107 	.globl _P4_7
                            108 	.globl _P4_6
                            109 	.globl _P4_5
                            110 	.globl _P4_4
                            111 	.globl _P4_3
                            112 	.globl _P4_2
                            113 	.globl _P4_1
                            114 	.globl _P4_0
                            115 	.globl _PX0L
                            116 	.globl _PT0L
                            117 	.globl _PX1L
                            118 	.globl _PT1L
                            119 	.globl _PLS
                            120 	.globl _PT2L
                            121 	.globl _PPCL
                            122 	.globl _EC
                            123 	.globl _CCF0
                            124 	.globl _CCF1
                            125 	.globl _CCF2
                            126 	.globl _CCF3
                            127 	.globl _CCF4
                            128 	.globl _CR
                            129 	.globl _CF
                            130 	.globl _TF2
                            131 	.globl _EXF2
                            132 	.globl _RCLK
                            133 	.globl _TCLK
                            134 	.globl _EXEN2
                            135 	.globl _TR2
                            136 	.globl _C_T2
                            137 	.globl _CP_RL2
                            138 	.globl _T2CON_7
                            139 	.globl _T2CON_6
                            140 	.globl _T2CON_5
                            141 	.globl _T2CON_4
                            142 	.globl _T2CON_3
                            143 	.globl _T2CON_2
                            144 	.globl _T2CON_1
                            145 	.globl _T2CON_0
                            146 	.globl _PT2
                            147 	.globl _ET2
                            148 	.globl _B
                            149 	.globl _ACC
                            150 	.globl _PSW
                            151 	.globl _IP
                            152 	.globl _P3
                            153 	.globl _IE
                            154 	.globl _P2
                            155 	.globl _SBUF
                            156 	.globl _SCON
                            157 	.globl _P1
                            158 	.globl _TH1
                            159 	.globl _TH0
                            160 	.globl _TL1
                            161 	.globl _TL0
                            162 	.globl _TMOD
                            163 	.globl _TCON
                            164 	.globl _PCON
                            165 	.globl _DPH
                            166 	.globl _DPL
                            167 	.globl _SP
                            168 	.globl _P0
                            169 	.globl _SBUF0
                            170 	.globl _DP0L
                            171 	.globl _DP0H
                            172 	.globl _EECON
                            173 	.globl _KBF
                            174 	.globl _KBE
                            175 	.globl _KBLS
                            176 	.globl _BRL
                            177 	.globl _BDRCON
                            178 	.globl _T2MOD
                            179 	.globl _SPDAT
                            180 	.globl _SPSTA
                            181 	.globl _SPCON
                            182 	.globl _SADEN
                            183 	.globl _SADDR
                            184 	.globl _WDTPRG
                            185 	.globl _WDTRST
                            186 	.globl _P5
                            187 	.globl _P4
                            188 	.globl _IPH1
                            189 	.globl _IPL1
                            190 	.globl _IPH0
                            191 	.globl _IPL0
                            192 	.globl _IEN1
                            193 	.globl _IEN0
                            194 	.globl _CMOD
                            195 	.globl _CL
                            196 	.globl _CH
                            197 	.globl _CCON
                            198 	.globl _CCAPM4
                            199 	.globl _CCAPM3
                            200 	.globl _CCAPM2
                            201 	.globl _CCAPM1
                            202 	.globl _CCAPM0
                            203 	.globl _CCAP4L
                            204 	.globl _CCAP3L
                            205 	.globl _CCAP2L
                            206 	.globl _CCAP1L
                            207 	.globl _CCAP0L
                            208 	.globl _CCAP4H
                            209 	.globl _CCAP3H
                            210 	.globl _CCAP2H
                            211 	.globl _CCAP1H
                            212 	.globl _CCAP0H
                            213 	.globl _CKCKON1
                            214 	.globl _CKCKON0
                            215 	.globl _CKRL
                            216 	.globl _AUXR1
                            217 	.globl _AUXR
                            218 	.globl _TH2
                            219 	.globl _TL2
                            220 	.globl _RCAP2H
                            221 	.globl _RCAP2L
                            222 	.globl _T2CON
                            223 	.globl _i
                            224 	.globl _res
                            225 	.globl _value
                            226 	.globl _ptr
                            227 	.globl _g
                            228 	.globl _j
                            229 	.globl _buff_val
                            230 	.globl _temp
                            231 	.globl _rec
                            232 	.globl _bufflen
                            233 	.globl _bptr1
                            234 	.globl _bptr0
                            235 	.globl _buff2ptr
                            236 	.globl _buff1ptr
                            237 	.globl _buffer
                            238 	.globl _heap
                            239 	.globl _BufferInitialize
                            240 	.globl _ReadValue
                            241 	.globl _newbuffer
                            242 	.globl _help_menu
                            243 	.globl _deletebuffer
                            244 	.globl _fillbuffer0
                            245 	.globl _fillbuffer1
                            246 	.globl _freeEverything
                            247 	.globl _displaydelete
                            248 	.globl _display
                            249 	.globl _putstrhex
                            250 	.globl _putstrbuff
                            251 	.globl _putstr
                            252 	.globl _SerialInitialize
                            253 	.globl _getchar
                            254 	.globl _putchar
                            255 ;--------------------------------------------------------
                            256 ; special function registers
                            257 ;--------------------------------------------------------
                            258 	.area RSEG    (DATA)
                    00C8    259 _T2CON	=	0x00c8
                    00CA    260 _RCAP2L	=	0x00ca
                    00CB    261 _RCAP2H	=	0x00cb
                    00CC    262 _TL2	=	0x00cc
                    00CD    263 _TH2	=	0x00cd
                    008E    264 _AUXR	=	0x008e
                    00A2    265 _AUXR1	=	0x00a2
                    0097    266 _CKRL	=	0x0097
                    008F    267 _CKCKON0	=	0x008f
                    008F    268 _CKCKON1	=	0x008f
                    00FA    269 _CCAP0H	=	0x00fa
                    00FB    270 _CCAP1H	=	0x00fb
                    00FC    271 _CCAP2H	=	0x00fc
                    00FD    272 _CCAP3H	=	0x00fd
                    00FE    273 _CCAP4H	=	0x00fe
                    00EA    274 _CCAP0L	=	0x00ea
                    00EB    275 _CCAP1L	=	0x00eb
                    00EC    276 _CCAP2L	=	0x00ec
                    00ED    277 _CCAP3L	=	0x00ed
                    00EE    278 _CCAP4L	=	0x00ee
                    00DA    279 _CCAPM0	=	0x00da
                    00DB    280 _CCAPM1	=	0x00db
                    00DC    281 _CCAPM2	=	0x00dc
                    00DD    282 _CCAPM3	=	0x00dd
                    00DE    283 _CCAPM4	=	0x00de
                    00D8    284 _CCON	=	0x00d8
                    00F9    285 _CH	=	0x00f9
                    00E9    286 _CL	=	0x00e9
                    00D9    287 _CMOD	=	0x00d9
                    00A8    288 _IEN0	=	0x00a8
                    00B1    289 _IEN1	=	0x00b1
                    00B8    290 _IPL0	=	0x00b8
                    00B7    291 _IPH0	=	0x00b7
                    00B2    292 _IPL1	=	0x00b2
                    00B3    293 _IPH1	=	0x00b3
                    00C0    294 _P4	=	0x00c0
                    00D8    295 _P5	=	0x00d8
                    00A6    296 _WDTRST	=	0x00a6
                    00A7    297 _WDTPRG	=	0x00a7
                    00A9    298 _SADDR	=	0x00a9
                    00B9    299 _SADEN	=	0x00b9
                    00C3    300 _SPCON	=	0x00c3
                    00C4    301 _SPSTA	=	0x00c4
                    00C5    302 _SPDAT	=	0x00c5
                    00C9    303 _T2MOD	=	0x00c9
                    009B    304 _BDRCON	=	0x009b
                    009A    305 _BRL	=	0x009a
                    009C    306 _KBLS	=	0x009c
                    009D    307 _KBE	=	0x009d
                    009E    308 _KBF	=	0x009e
                    00D2    309 _EECON	=	0x00d2
                    0083    310 _DP0H	=	0x0083
                    0082    311 _DP0L	=	0x0082
                    0099    312 _SBUF0	=	0x0099
                    0080    313 _P0	=	0x0080
                    0081    314 _SP	=	0x0081
                    0082    315 _DPL	=	0x0082
                    0083    316 _DPH	=	0x0083
                    0087    317 _PCON	=	0x0087
                    0088    318 _TCON	=	0x0088
                    0089    319 _TMOD	=	0x0089
                    008A    320 _TL0	=	0x008a
                    008B    321 _TL1	=	0x008b
                    008C    322 _TH0	=	0x008c
                    008D    323 _TH1	=	0x008d
                    0090    324 _P1	=	0x0090
                    0098    325 _SCON	=	0x0098
                    0099    326 _SBUF	=	0x0099
                    00A0    327 _P2	=	0x00a0
                    00A8    328 _IE	=	0x00a8
                    00B0    329 _P3	=	0x00b0
                    00B8    330 _IP	=	0x00b8
                    00D0    331 _PSW	=	0x00d0
                    00E0    332 _ACC	=	0x00e0
                    00F0    333 _B	=	0x00f0
                            334 ;--------------------------------------------------------
                            335 ; special function bits
                            336 ;--------------------------------------------------------
                            337 	.area RSEG    (DATA)
                    00AD    338 _ET2	=	0x00ad
                    00BD    339 _PT2	=	0x00bd
                    00C8    340 _T2CON_0	=	0x00c8
                    00C9    341 _T2CON_1	=	0x00c9
                    00CA    342 _T2CON_2	=	0x00ca
                    00CB    343 _T2CON_3	=	0x00cb
                    00CC    344 _T2CON_4	=	0x00cc
                    00CD    345 _T2CON_5	=	0x00cd
                    00CE    346 _T2CON_6	=	0x00ce
                    00CF    347 _T2CON_7	=	0x00cf
                    00C8    348 _CP_RL2	=	0x00c8
                    00C9    349 _C_T2	=	0x00c9
                    00CA    350 _TR2	=	0x00ca
                    00CB    351 _EXEN2	=	0x00cb
                    00CC    352 _TCLK	=	0x00cc
                    00CD    353 _RCLK	=	0x00cd
                    00CE    354 _EXF2	=	0x00ce
                    00CF    355 _TF2	=	0x00cf
                    00DF    356 _CF	=	0x00df
                    00DE    357 _CR	=	0x00de
                    00DC    358 _CCF4	=	0x00dc
                    00DB    359 _CCF3	=	0x00db
                    00DA    360 _CCF2	=	0x00da
                    00D9    361 _CCF1	=	0x00d9
                    00D8    362 _CCF0	=	0x00d8
                    00AE    363 _EC	=	0x00ae
                    00BE    364 _PPCL	=	0x00be
                    00BD    365 _PT2L	=	0x00bd
                    00BC    366 _PLS	=	0x00bc
                    00BB    367 _PT1L	=	0x00bb
                    00BA    368 _PX1L	=	0x00ba
                    00B9    369 _PT0L	=	0x00b9
                    00B8    370 _PX0L	=	0x00b8
                    00C0    371 _P4_0	=	0x00c0
                    00C1    372 _P4_1	=	0x00c1
                    00C2    373 _P4_2	=	0x00c2
                    00C3    374 _P4_3	=	0x00c3
                    00C4    375 _P4_4	=	0x00c4
                    00C5    376 _P4_5	=	0x00c5
                    00C6    377 _P4_6	=	0x00c6
                    00C7    378 _P4_7	=	0x00c7
                    00D8    379 _P5_0	=	0x00d8
                    00D9    380 _P5_1	=	0x00d9
                    00DA    381 _P5_2	=	0x00da
                    00DB    382 _P5_3	=	0x00db
                    00DC    383 _P5_4	=	0x00dc
                    00DD    384 _P5_5	=	0x00dd
                    00DE    385 _P5_6	=	0x00de
                    00DF    386 _P5_7	=	0x00df
                    00F0    387 _BREG_F0	=	0x00f0
                    00F1    388 _BREG_F1	=	0x00f1
                    00F2    389 _BREG_F2	=	0x00f2
                    00F3    390 _BREG_F3	=	0x00f3
                    00F4    391 _BREG_F4	=	0x00f4
                    00F5    392 _BREG_F5	=	0x00f5
                    00F6    393 _BREG_F6	=	0x00f6
                    00F7    394 _BREG_F7	=	0x00f7
                    00B0    395 _RXD0	=	0x00b0
                    00B1    396 _TXD0	=	0x00b1
                    0080    397 _P0_0	=	0x0080
                    0081    398 _P0_1	=	0x0081
                    0082    399 _P0_2	=	0x0082
                    0083    400 _P0_3	=	0x0083
                    0084    401 _P0_4	=	0x0084
                    0085    402 _P0_5	=	0x0085
                    0086    403 _P0_6	=	0x0086
                    0087    404 _P0_7	=	0x0087
                    0088    405 _IT0	=	0x0088
                    0089    406 _IE0	=	0x0089
                    008A    407 _IT1	=	0x008a
                    008B    408 _IE1	=	0x008b
                    008C    409 _TR0	=	0x008c
                    008D    410 _TF0	=	0x008d
                    008E    411 _TR1	=	0x008e
                    008F    412 _TF1	=	0x008f
                    0090    413 _P1_0	=	0x0090
                    0091    414 _P1_1	=	0x0091
                    0092    415 _P1_2	=	0x0092
                    0093    416 _P1_3	=	0x0093
                    0094    417 _P1_4	=	0x0094
                    0095    418 _P1_5	=	0x0095
                    0096    419 _P1_6	=	0x0096
                    0097    420 _P1_7	=	0x0097
                    0098    421 _RI	=	0x0098
                    0099    422 _TI	=	0x0099
                    009A    423 _RB8	=	0x009a
                    009B    424 _TB8	=	0x009b
                    009C    425 _REN	=	0x009c
                    009D    426 _SM2	=	0x009d
                    009E    427 _SM1	=	0x009e
                    009F    428 _SM0	=	0x009f
                    00A0    429 _P2_0	=	0x00a0
                    00A1    430 _P2_1	=	0x00a1
                    00A2    431 _P2_2	=	0x00a2
                    00A3    432 _P2_3	=	0x00a3
                    00A4    433 _P2_4	=	0x00a4
                    00A5    434 _P2_5	=	0x00a5
                    00A6    435 _P2_6	=	0x00a6
                    00A7    436 _P2_7	=	0x00a7
                    00A8    437 _EX0	=	0x00a8
                    00A9    438 _ET0	=	0x00a9
                    00AA    439 _EX1	=	0x00aa
                    00AB    440 _ET1	=	0x00ab
                    00AC    441 _ES	=	0x00ac
                    00AF    442 _EA	=	0x00af
                    00B0    443 _P3_0	=	0x00b0
                    00B1    444 _P3_1	=	0x00b1
                    00B2    445 _P3_2	=	0x00b2
                    00B3    446 _P3_3	=	0x00b3
                    00B4    447 _P3_4	=	0x00b4
                    00B5    448 _P3_5	=	0x00b5
                    00B6    449 _P3_6	=	0x00b6
                    00B7    450 _P3_7	=	0x00b7
                    00B0    451 _RXD	=	0x00b0
                    00B1    452 _TXD	=	0x00b1
                    00B2    453 _INT0	=	0x00b2
                    00B3    454 _INT1	=	0x00b3
                    00B4    455 _T0	=	0x00b4
                    00B5    456 _T1	=	0x00b5
                    00B6    457 _WR	=	0x00b6
                    00B7    458 _RD	=	0x00b7
                    00B8    459 _PX0	=	0x00b8
                    00B9    460 _PT0	=	0x00b9
                    00BA    461 _PX1	=	0x00ba
                    00BB    462 _PT1	=	0x00bb
                    00BC    463 _PS	=	0x00bc
                    00D0    464 _P	=	0x00d0
                    00D1    465 _F1	=	0x00d1
                    00D2    466 _OV	=	0x00d2
                    00D3    467 _RS0	=	0x00d3
                    00D4    468 _RS1	=	0x00d4
                    00D5    469 _F0	=	0x00d5
                    00D6    470 _AC	=	0x00d6
                    00D7    471 _CY	=	0x00d7
                            472 ;--------------------------------------------------------
                            473 ; overlayable register banks
                            474 ;--------------------------------------------------------
                            475 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     476 	.ds 8
                            477 ;--------------------------------------------------------
                            478 ; internal ram data
                            479 ;--------------------------------------------------------
                            480 	.area DSEG    (DATA)
   0008                     481 _putstrhex_sloc1_1_0:
   0008                     482 	.ds 2
   000A                     483 _putstrhex_sloc3_1_0:
   000A                     484 	.ds 2
                            485 ;--------------------------------------------------------
                            486 ; overlayable items in internal ram 
                            487 ;--------------------------------------------------------
                            488 	.area	OSEG    (OVR,DATA)
   0012                     489 _fillbuffer0_sloc1_1_0::
   0012                     490 	.ds 2
   0014                     491 _fillbuffer0_sloc2_1_0::
   0014                     492 	.ds 3
                            493 	.area	OSEG    (OVR,DATA)
   0012                     494 _fillbuffer1_sloc1_1_0::
   0012                     495 	.ds 2
   0014                     496 _fillbuffer1_sloc2_1_0::
   0014                     497 	.ds 3
                            498 ;--------------------------------------------------------
                            499 ; Stack segment in internal ram 
                            500 ;--------------------------------------------------------
                            501 	.area	SSEG	(DATA)
   0022                     502 __start__stack:
   0022                     503 	.ds	1
                            504 
                            505 ;--------------------------------------------------------
                            506 ; indirectly addressable internal ram data
                            507 ;--------------------------------------------------------
                            508 	.area ISEG    (DATA)
                            509 ;--------------------------------------------------------
                            510 ; bit data
                            511 ;--------------------------------------------------------
                            512 	.area BSEG    (BIT)
                            513 ;--------------------------------------------------------
                            514 ; paged external ram data
                            515 ;--------------------------------------------------------
                            516 	.area PSEG    (PAG,XDATA)
                            517 ;--------------------------------------------------------
                            518 ; external ram data
                            519 ;--------------------------------------------------------
                            520 	.area XSEG    (XDATA)
   0000                     521 _heap::
   0000                     522 	.ds 3000
   0BB8                     523 _buffer_count:
   0BB8                     524 	.ds 2
   0BBA                     525 _stor_count:
   0BBA                     526 	.ds 2
   0BBC                     527 _char_count:
   0BBC                     528 	.ds 2
   0BBE                     529 _buffer_number:
   0BBE                     530 	.ds 2
   0BC0                     531 _lastchar_count:
   0BC0                     532 	.ds 2
   0BC2                     533 _buffer::
   0BC2                     534 	.ds 512
   0DC2                     535 _buff1ptr::
   0DC2                     536 	.ds 3
   0DC5                     537 _buff2ptr::
   0DC5                     538 	.ds 3
   0DC8                     539 _bptr0::
   0DC8                     540 	.ds 3
   0DCB                     541 _bptr1::
   0DCB                     542 	.ds 3
   0DCE                     543 _bufflen::
   0DCE                     544 	.ds 512
   0FCE                     545 _rec::
   0FCE                     546 	.ds 1
   0FCF                     547 _temp::
   0FCF                     548 	.ds 1
   0FD0                     549 _buff_val::
   0FD0                     550 	.ds 1
   0FD1                     551 _j::
   0FD1                     552 	.ds 1
   0FD2                     553 _g::
   0FD2                     554 	.ds 2
   0FD4                     555 _ptr::
   0FD4                     556 	.ds 3
   0FD7                     557 _value::
   0FD7                     558 	.ds 2
   0FD9                     559 _res::
   0FD9                     560 	.ds 2
   0FDB                     561 _fillbuffer0_buff_val_1_1:
   0FDB                     562 	.ds 1
   0FDC                     563 _fillbuffer1_buff_val_1_1:
   0FDC                     564 	.ds 1
   0FDD                     565 _displaydelete_charnum0_1_1:
   0FDD                     566 	.ds 2
   0FDF                     567 _displaydelete_charnum1_1_1:
   0FDF                     568 	.ds 2
   0FE1                     569 _putstrhex_g_1_1:
   0FE1                     570 	.ds 2
   0FE3                     571 _putstrhex_length_1_1:
   0FE3                     572 	.ds 2
   0FE5                     573 _putstrbuff_ptr_1_1:
   0FE5                     574 	.ds 3
   0FE8                     575 _putstrbuff_count_1_1:
   0FE8                     576 	.ds 2
   0FEA                     577 _putstr_ptr_1_1:
   0FEA                     578 	.ds 3
   0FED                     579 _putchar_x_1_1:
   0FED                     580 	.ds 2
                            581 ;--------------------------------------------------------
                            582 ; external initialized ram data
                            583 ;--------------------------------------------------------
                            584 	.area XISEG   (XDATA)
   1042                     585 _i::
   1042                     586 	.ds 1
                            587 	.area HOME    (CODE)
                            588 	.area GSINIT0 (CODE)
                            589 	.area GSINIT1 (CODE)
                            590 	.area GSINIT2 (CODE)
                            591 	.area GSINIT3 (CODE)
                            592 	.area GSINIT4 (CODE)
                            593 	.area GSINIT5 (CODE)
                            594 	.area GSINIT  (CODE)
                            595 	.area GSFINAL (CODE)
                            596 	.area CSEG    (CODE)
                            597 ;--------------------------------------------------------
                            598 ; interrupt vector 
                            599 ;--------------------------------------------------------
                            600 	.area HOME    (CODE)
   0000                     601 __interrupt_vect:
   0000 02 00 03            602 	ljmp	__sdcc_gsinit_startup
                            603 ;--------------------------------------------------------
                            604 ; global & static initialisations
                            605 ;--------------------------------------------------------
                            606 	.area HOME    (CODE)
                            607 	.area GSINIT  (CODE)
                            608 	.area GSFINAL (CODE)
                            609 	.area GSINIT  (CODE)
                            610 	.globl __sdcc_gsinit_startup
                            611 	.globl __sdcc_program_startup
                            612 	.globl __start__stack
                            613 	.globl __mcs51_genXINIT
                            614 	.globl __mcs51_genXRAMCLEAR
                            615 	.globl __mcs51_genRAMCLEAR
                            616 	.area GSFINAL (CODE)
   005C 02 00 5F            617 	ljmp	__sdcc_program_startup
                            618 ;--------------------------------------------------------
                            619 ; Home
                            620 ;--------------------------------------------------------
                            621 	.area HOME    (CODE)
                            622 	.area CSEG    (CODE)
   005F                     623 __sdcc_program_startup:
   005F 12 00 6B            624 	lcall	_main
                            625 ;	return from main will lock up
   0062 80 FE               626 	sjmp .
                            627 ;--------------------------------------------------------
                            628 ; code
                            629 ;--------------------------------------------------------
                            630 	.area CSEG    (CODE)
                            631 ;------------------------------------------------------------
                            632 ;Allocation info for local variables in function '_sdcc_external_startup'
                            633 ;------------------------------------------------------------
                            634 ;------------------------------------------------------------
                            635 ;	main.c:43: _sdcc_external_startup()
                            636 ;	-----------------------------------------
                            637 ;	 function _sdcc_external_startup
                            638 ;	-----------------------------------------
   0064                     639 __sdcc_external_startup:
                    0002    640 	ar2 = 0x02
                    0003    641 	ar3 = 0x03
                    0004    642 	ar4 = 0x04
                    0005    643 	ar5 = 0x05
                    0006    644 	ar6 = 0x06
                    0007    645 	ar7 = 0x07
                    0000    646 	ar0 = 0x00
                    0001    647 	ar1 = 0x01
                            648 ;	main.c:45: AUXR |= 0x0C;
                            649 ;	genOr
   0064 43 8E 0C            650 	orl	_AUXR,#0x0C
                            651 ;	main.c:46: return 0;
                            652 ;	genRet
                            653 ;	Peephole 182.b	used 16 bit load of dptr
   0067 90 00 00            654 	mov	dptr,#0x0000
                            655 ;	Peephole 300	removed redundant label 00101$
   006A 22                  656 	ret
                            657 ;------------------------------------------------------------
                            658 ;Allocation info for local variables in function 'main'
                            659 ;------------------------------------------------------------
                            660 ;------------------------------------------------------------
                            661 ;	main.c:74: void main()
                            662 ;	-----------------------------------------
                            663 ;	 function main
                            664 ;	-----------------------------------------
   006B                     665 _main:
                            666 ;	main.c:77: SerialInitialize();  /*  call the serial initialize function */
                            667 ;	genCall
   006B 12 0F 73            668 	lcall	_SerialInitialize
                            669 ;	main.c:79: init_dynamic_memory((MEMHEADER xdata *)heap, HEAP_SIZE); /* Heap allocation */
                            670 ;	genAssign
   006E 90 10 00            671 	mov	dptr,#_init_dynamic_memory_PARM_2
   0071 74 B8               672 	mov	a,#0xB8
   0073 F0                  673 	movx	@dptr,a
   0074 A3                  674 	inc	dptr
   0075 74 0B               675 	mov	a,#0x0B
   0077 F0                  676 	movx	@dptr,a
                            677 ;	genCall
                            678 ;	Peephole 182.a	used 16 bit load of DPTR
   0078 90 00 00            679 	mov	dptr,#_heap
   007B 12 11 0F            680 	lcall	_init_dynamic_memory
                            681 ;	main.c:80: here: lastchar_count=0; /*Number of characters since last '?' */
   007E                     682 00101$:
                            683 ;	genAssign
   007E 90 0B C0            684 	mov	dptr,#_lastchar_count
   0081 E4                  685 	clr	a
   0082 F0                  686 	movx	@dptr,a
   0083 A3                  687 	inc	dptr
   0084 F0                  688 	movx	@dptr,a
                            689 ;	main.c:81: char_count=0; /*Total number of characters received */
                            690 ;	genAssign
   0085 90 0B BC            691 	mov	dptr,#_char_count
   0088 E4                  692 	clr	a
   0089 F0                  693 	movx	@dptr,a
   008A A3                  694 	inc	dptr
   008B F0                  695 	movx	@dptr,a
                            696 ;	main.c:82: stor_count=0; /* Total number of storage characters received */
                            697 ;	genAssign
   008C 90 0B BA            698 	mov	dptr,#_stor_count
   008F E4                  699 	clr	a
   0090 F0                  700 	movx	@dptr,a
   0091 A3                  701 	inc	dptr
   0092 F0                  702 	movx	@dptr,a
                            703 ;	main.c:83: buffer_count=2; /*buffer count to allocate new buffers */
                            704 ;	genAssign
   0093 90 0B B8            705 	mov	dptr,#_buffer_count
   0096 74 02               706 	mov	a,#0x02
   0098 F0                  707 	movx	@dptr,a
   0099 E4                  708 	clr	a
   009A A3                  709 	inc	dptr
   009B F0                  710 	movx	@dptr,a
                            711 ;	main.c:84: buffer_number=0; /*Total number of buffers active */
                            712 ;	genAssign
   009C 90 0B BE            713 	mov	dptr,#_buffer_number
   009F E4                  714 	clr	a
   00A0 F0                  715 	movx	@dptr,a
   00A1 A3                  716 	inc	dptr
   00A2 F0                  717 	movx	@dptr,a
                            718 ;	main.c:86: BufferInitialize(); /*Buffer initilisation */
                            719 ;	genCall
   00A3 12 01 FF            720 	lcall	_BufferInitialize
                            721 ;	main.c:88: putstr("\n\rEnter the character\n\r");
                            722 ;	genCall
                            723 ;	Peephole 182.a	used 16 bit load of DPTR
   00A6 90 1D DF            724 	mov	dptr,#__str_0
   00A9 75 F0 80            725 	mov	b,#0x80
   00AC 12 0F 16            726 	lcall	_putstr
                            727 ;	main.c:89: putstr("Press '>' for help menu\n\r");
                            728 ;	genCall
                            729 ;	Peephole 182.a	used 16 bit load of DPTR
   00AF 90 1D F7            730 	mov	dptr,#__str_1
   00B2 75 F0 80            731 	mov	b,#0x80
   00B5 12 0F 16            732 	lcall	_putstr
                            733 ;	main.c:90: while(1)
   00B8                     734 00128$:
                            735 ;	main.c:92: temp = getchar(); /* getting character from user input */
                            736 ;	genCall
   00B8 12 0F 82            737 	lcall	_getchar
   00BB AA 82               738 	mov	r2,dpl
                            739 ;	genAssign
   00BD 90 0F CF            740 	mov	dptr,#_temp
   00C0 EA                  741 	mov	a,r2
   00C1 F0                  742 	movx	@dptr,a
                            743 ;	main.c:93: putchar(temp);  /* put character back on the screen */
                            744 ;	genAssign
                            745 ;	genCast
   00C2 7B 00               746 	mov	r3,#0x00
                            747 ;	genCall
   00C4 8A 82               748 	mov	dpl,r2
   00C6 8B 83               749 	mov	dph,r3
   00C8 12 0F 91            750 	lcall	_putchar
                            751 ;	main.c:94: putstr(" ");
                            752 ;	genCall
                            753 ;	Peephole 182.a	used 16 bit load of DPTR
   00CB 90 1E 11            754 	mov	dptr,#__str_2
   00CE 75 F0 80            755 	mov	b,#0x80
   00D1 12 0F 16            756 	lcall	_putstr
                            757 ;	main.c:95: if (temp>='A' && temp<='Z') /* Check if storage character*/
                            758 ;	genAssign
   00D4 90 0F CF            759 	mov	dptr,#_temp
   00D7 E0                  760 	movx	a,@dptr
   00D8 FA                  761 	mov	r2,a
                            762 ;	genCmpLt
                            763 ;	genCmp
   00D9 BA 41 00            764 	cjne	r2,#0x41,00144$
   00DC                     765 00144$:
                            766 ;	genIfxJump
                            767 ;	Peephole 112.b	changed ljmp to sjmp
                            768 ;	Peephole 160.a	removed sjmp by inverse jump logic
   00DC 40 32               769 	jc	00124$
                            770 ;	Peephole 300	removed redundant label 00145$
                            771 ;	genCmpGt
                            772 ;	genCmp
                            773 ;	genIfxJump
                            774 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   00DE EA                  775 	mov	a,r2
   00DF 24 A5               776 	add	a,#0xff - 0x5A
                            777 ;	Peephole 112.b	changed ljmp to sjmp
                            778 ;	Peephole 160.a	removed sjmp by inverse jump logic
   00E1 40 2D               779 	jc	00124$
                            780 ;	Peephole 300	removed redundant label 00146$
                            781 ;	main.c:97: stor_count++;
                            782 ;	genAssign
   00E3 90 0B BA            783 	mov	dptr,#_stor_count
   00E6 E0                  784 	movx	a,@dptr
   00E7 FB                  785 	mov	r3,a
   00E8 A3                  786 	inc	dptr
   00E9 E0                  787 	movx	a,@dptr
   00EA FC                  788 	mov	r4,a
                            789 ;	genPlus
   00EB 90 0B BA            790 	mov	dptr,#_stor_count
                            791 ;     genPlusIncr
   00EE 74 01               792 	mov	a,#0x01
                            793 ;	Peephole 236.a	used r3 instead of ar3
   00F0 2B                  794 	add	a,r3
   00F1 F0                  795 	movx	@dptr,a
                            796 ;	Peephole 181	changed mov to clr
   00F2 E4                  797 	clr	a
                            798 ;	Peephole 236.b	used r4 instead of ar4
   00F3 3C                  799 	addc	a,r4
   00F4 A3                  800 	inc	dptr
   00F5 F0                  801 	movx	@dptr,a
                            802 ;	main.c:98: char_count++;  /* for character count */
                            803 ;	genAssign
   00F6 90 0B BC            804 	mov	dptr,#_char_count
   00F9 E0                  805 	movx	a,@dptr
   00FA FB                  806 	mov	r3,a
   00FB A3                  807 	inc	dptr
   00FC E0                  808 	movx	a,@dptr
   00FD FC                  809 	mov	r4,a
                            810 ;	genPlus
   00FE 90 0B BC            811 	mov	dptr,#_char_count
                            812 ;     genPlusIncr
   0101 74 01               813 	mov	a,#0x01
                            814 ;	Peephole 236.a	used r3 instead of ar3
   0103 2B                  815 	add	a,r3
   0104 F0                  816 	movx	@dptr,a
                            817 ;	Peephole 181	changed mov to clr
   0105 E4                  818 	clr	a
                            819 ;	Peephole 236.b	used r4 instead of ar4
   0106 3C                  820 	addc	a,r4
   0107 A3                  821 	inc	dptr
   0108 F0                  822 	movx	@dptr,a
                            823 ;	main.c:99: fillbuffer0(temp); /* Send data to the buffer*/
                            824 ;	genCall
   0109 8A 82               825 	mov	dpl,r2
   010B 12 06 8D            826 	lcall	_fillbuffer0
                            827 ;	Peephole 112.b	changed ljmp to sjmp
   010E 80 A8               828 	sjmp	00128$
   0110                     829 00124$:
                            830 ;	main.c:101: else if (temp>='0' && temp<='9')
                            831 ;	genAssign
   0110 90 0F CF            832 	mov	dptr,#_temp
   0113 E0                  833 	movx	a,@dptr
   0114 FA                  834 	mov	r2,a
                            835 ;	genCmpLt
                            836 ;	genCmp
   0115 BA 30 00            837 	cjne	r2,#0x30,00147$
   0118                     838 00147$:
                            839 ;	genIfxJump
                            840 ;	Peephole 112.b	changed ljmp to sjmp
                            841 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0118 40 33               842 	jc	00120$
                            843 ;	Peephole 300	removed redundant label 00148$
                            844 ;	genCmpGt
                            845 ;	genCmp
                            846 ;	genIfxJump
                            847 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   011A EA                  848 	mov	a,r2
   011B 24 C6               849 	add	a,#0xff - 0x39
                            850 ;	Peephole 112.b	changed ljmp to sjmp
                            851 ;	Peephole 160.a	removed sjmp by inverse jump logic
   011D 40 2E               852 	jc	00120$
                            853 ;	Peephole 300	removed redundant label 00149$
                            854 ;	main.c:103: stor_count++;
                            855 ;	genAssign
   011F 90 0B BA            856 	mov	dptr,#_stor_count
   0122 E0                  857 	movx	a,@dptr
   0123 FB                  858 	mov	r3,a
   0124 A3                  859 	inc	dptr
   0125 E0                  860 	movx	a,@dptr
   0126 FC                  861 	mov	r4,a
                            862 ;	genPlus
   0127 90 0B BA            863 	mov	dptr,#_stor_count
                            864 ;     genPlusIncr
   012A 74 01               865 	mov	a,#0x01
                            866 ;	Peephole 236.a	used r3 instead of ar3
   012C 2B                  867 	add	a,r3
   012D F0                  868 	movx	@dptr,a
                            869 ;	Peephole 181	changed mov to clr
   012E E4                  870 	clr	a
                            871 ;	Peephole 236.b	used r4 instead of ar4
   012F 3C                  872 	addc	a,r4
   0130 A3                  873 	inc	dptr
   0131 F0                  874 	movx	@dptr,a
                            875 ;	main.c:104: char_count++;
                            876 ;	genAssign
   0132 90 0B BC            877 	mov	dptr,#_char_count
   0135 E0                  878 	movx	a,@dptr
   0136 FB                  879 	mov	r3,a
   0137 A3                  880 	inc	dptr
   0138 E0                  881 	movx	a,@dptr
   0139 FC                  882 	mov	r4,a
                            883 ;	genPlus
   013A 90 0B BC            884 	mov	dptr,#_char_count
                            885 ;     genPlusIncr
   013D 74 01               886 	mov	a,#0x01
                            887 ;	Peephole 236.a	used r3 instead of ar3
   013F 2B                  888 	add	a,r3
   0140 F0                  889 	movx	@dptr,a
                            890 ;	Peephole 181	changed mov to clr
   0141 E4                  891 	clr	a
                            892 ;	Peephole 236.b	used r4 instead of ar4
   0142 3C                  893 	addc	a,r4
   0143 A3                  894 	inc	dptr
   0144 F0                  895 	movx	@dptr,a
                            896 ;	main.c:105: fillbuffer1(temp);
                            897 ;	genCall
   0145 8A 82               898 	mov	dpl,r2
   0147 12 07 40            899 	lcall	_fillbuffer1
   014A 02 00 B8            900 	ljmp	00128$
   014D                     901 00120$:
                            902 ;	main.c:107: else if (temp=='+')
                            903 ;	genAssign
   014D 90 0F CF            904 	mov	dptr,#_temp
   0150 E0                  905 	movx	a,@dptr
   0151 FA                  906 	mov	r2,a
                            907 ;	genCmpEq
                            908 ;	gencjneshort
                            909 ;	Peephole 112.b	changed ljmp to sjmp
                            910 ;	Peephole 198.b	optimized misc jump sequence
   0152 BA 2B 19            911 	cjne	r2,#0x2B,00117$
                            912 ;	Peephole 200.b	removed redundant sjmp
                            913 ;	Peephole 300	removed redundant label 00150$
                            914 ;	Peephole 300	removed redundant label 00151$
                            915 ;	main.c:109: newbuffer();  /* allocate new buffer */
                            916 ;	genCall
   0155 12 04 38            917 	lcall	_newbuffer
                            918 ;	main.c:110: char_count++;
                            919 ;	genAssign
   0158 90 0B BC            920 	mov	dptr,#_char_count
   015B E0                  921 	movx	a,@dptr
   015C FB                  922 	mov	r3,a
   015D A3                  923 	inc	dptr
   015E E0                  924 	movx	a,@dptr
   015F FC                  925 	mov	r4,a
                            926 ;	genPlus
   0160 90 0B BC            927 	mov	dptr,#_char_count
                            928 ;     genPlusIncr
   0163 74 01               929 	mov	a,#0x01
                            930 ;	Peephole 236.a	used r3 instead of ar3
   0165 2B                  931 	add	a,r3
   0166 F0                  932 	movx	@dptr,a
                            933 ;	Peephole 181	changed mov to clr
   0167 E4                  934 	clr	a
                            935 ;	Peephole 236.b	used r4 instead of ar4
   0168 3C                  936 	addc	a,r4
   0169 A3                  937 	inc	dptr
   016A F0                  938 	movx	@dptr,a
   016B 02 00 B8            939 	ljmp	00128$
   016E                     940 00117$:
                            941 ;	main.c:112: else if (temp=='-')
                            942 ;	genCmpEq
                            943 ;	gencjneshort
                            944 ;	Peephole 112.b	changed ljmp to sjmp
                            945 ;	Peephole 198.b	optimized misc jump sequence
   016E BA 2D 19            946 	cjne	r2,#0x2D,00114$
                            947 ;	Peephole 200.b	removed redundant sjmp
                            948 ;	Peephole 300	removed redundant label 00152$
                            949 ;	Peephole 300	removed redundant label 00153$
                            950 ;	main.c:114: char_count++;
                            951 ;	genAssign
   0171 90 0B BC            952 	mov	dptr,#_char_count
   0174 E0                  953 	movx	a,@dptr
   0175 FB                  954 	mov	r3,a
   0176 A3                  955 	inc	dptr
   0177 E0                  956 	movx	a,@dptr
   0178 FC                  957 	mov	r4,a
                            958 ;	genPlus
   0179 90 0B BC            959 	mov	dptr,#_char_count
                            960 ;     genPlusIncr
   017C 74 01               961 	mov	a,#0x01
                            962 ;	Peephole 236.a	used r3 instead of ar3
   017E 2B                  963 	add	a,r3
   017F F0                  964 	movx	@dptr,a
                            965 ;	Peephole 181	changed mov to clr
   0180 E4                  966 	clr	a
                            967 ;	Peephole 236.b	used r4 instead of ar4
   0181 3C                  968 	addc	a,r4
   0182 A3                  969 	inc	dptr
   0183 F0                  970 	movx	@dptr,a
                            971 ;	main.c:115: deletebuffer();   /* delete buffer*/
                            972 ;	genCall
   0184 12 05 DD            973 	lcall	_deletebuffer
   0187 02 00 B8            974 	ljmp	00128$
   018A                     975 00114$:
                            976 ;	main.c:117: else if (temp=='?')
                            977 ;	genCmpEq
                            978 ;	gencjneshort
                            979 ;	Peephole 112.b	changed ljmp to sjmp
                            980 ;	Peephole 198.b	optimized misc jump sequence
   018A BA 3F 19            981 	cjne	r2,#0x3F,00111$
                            982 ;	Peephole 200.b	removed redundant sjmp
                            983 ;	Peephole 300	removed redundant label 00154$
                            984 ;	Peephole 300	removed redundant label 00155$
                            985 ;	main.c:119: char_count++;
                            986 ;	genAssign
   018D 90 0B BC            987 	mov	dptr,#_char_count
   0190 E0                  988 	movx	a,@dptr
   0191 FB                  989 	mov	r3,a
   0192 A3                  990 	inc	dptr
   0193 E0                  991 	movx	a,@dptr
   0194 FC                  992 	mov	r4,a
                            993 ;	genPlus
   0195 90 0B BC            994 	mov	dptr,#_char_count
                            995 ;     genPlusIncr
   0198 74 01               996 	mov	a,#0x01
                            997 ;	Peephole 236.a	used r3 instead of ar3
   019A 2B                  998 	add	a,r3
   019B F0                  999 	movx	@dptr,a
                           1000 ;	Peephole 181	changed mov to clr
   019C E4                 1001 	clr	a
                           1002 ;	Peephole 236.b	used r4 instead of ar4
   019D 3C                 1003 	addc	a,r4
   019E A3                 1004 	inc	dptr
   019F F0                 1005 	movx	@dptr,a
                           1006 ;	main.c:120: displaydelete(); /*display buffer content, detailed report and empty buffer */
                           1007 ;	genCall
   01A0 12 08 59           1008 	lcall	_displaydelete
   01A3 02 00 B8           1009 	ljmp	00128$
   01A6                    1010 00111$:
                           1011 ;	main.c:122: else if(temp=='=')
                           1012 ;	genCmpEq
                           1013 ;	gencjneshort
                           1014 ;	Peephole 112.b	changed ljmp to sjmp
                           1015 ;	Peephole 198.b	optimized misc jump sequence
   01A6 BA 3D 19           1016 	cjne	r2,#0x3D,00108$
                           1017 ;	Peephole 200.b	removed redundant sjmp
                           1018 ;	Peephole 300	removed redundant label 00156$
                           1019 ;	Peephole 300	removed redundant label 00157$
                           1020 ;	main.c:124: char_count++;
                           1021 ;	genAssign
   01A9 90 0B BC           1022 	mov	dptr,#_char_count
   01AC E0                 1023 	movx	a,@dptr
   01AD FB                 1024 	mov	r3,a
   01AE A3                 1025 	inc	dptr
   01AF E0                 1026 	movx	a,@dptr
   01B0 FC                 1027 	mov	r4,a
                           1028 ;	genPlus
   01B1 90 0B BC           1029 	mov	dptr,#_char_count
                           1030 ;     genPlusIncr
   01B4 74 01              1031 	mov	a,#0x01
                           1032 ;	Peephole 236.a	used r3 instead of ar3
   01B6 2B                 1033 	add	a,r3
   01B7 F0                 1034 	movx	@dptr,a
                           1035 ;	Peephole 181	changed mov to clr
   01B8 E4                 1036 	clr	a
                           1037 ;	Peephole 236.b	used r4 instead of ar4
   01B9 3C                 1038 	addc	a,r4
   01BA A3                 1039 	inc	dptr
   01BB F0                 1040 	movx	@dptr,a
                           1041 ;	main.c:125: display(); /* just display the buffer content in a nice table structure */
                           1042 ;	genCall
   01BC 12 0C 96           1043 	lcall	_display
   01BF 02 00 B8           1044 	ljmp	00128$
   01C2                    1045 00108$:
                           1046 ;	main.c:127: else if(temp=='>')
                           1047 ;	genCmpEq
                           1048 ;	gencjneshort
                           1049 ;	Peephole 112.b	changed ljmp to sjmp
                           1050 ;	Peephole 198.b	optimized misc jump sequence
   01C2 BA 3E 19           1051 	cjne	r2,#0x3E,00105$
                           1052 ;	Peephole 200.b	removed redundant sjmp
                           1053 ;	Peephole 300	removed redundant label 00158$
                           1054 ;	Peephole 300	removed redundant label 00159$
                           1055 ;	main.c:129: char_count++;
                           1056 ;	genAssign
   01C5 90 0B BC           1057 	mov	dptr,#_char_count
   01C8 E0                 1058 	movx	a,@dptr
   01C9 FB                 1059 	mov	r3,a
   01CA A3                 1060 	inc	dptr
   01CB E0                 1061 	movx	a,@dptr
   01CC FC                 1062 	mov	r4,a
                           1063 ;	genPlus
   01CD 90 0B BC           1064 	mov	dptr,#_char_count
                           1065 ;     genPlusIncr
   01D0 74 01              1066 	mov	a,#0x01
                           1067 ;	Peephole 236.a	used r3 instead of ar3
   01D2 2B                 1068 	add	a,r3
   01D3 F0                 1069 	movx	@dptr,a
                           1070 ;	Peephole 181	changed mov to clr
   01D4 E4                 1071 	clr	a
                           1072 ;	Peephole 236.b	used r4 instead of ar4
   01D5 3C                 1073 	addc	a,r4
   01D6 A3                 1074 	inc	dptr
   01D7 F0                 1075 	movx	@dptr,a
                           1076 ;	main.c:130: help_menu();  /* de-allocate every buffer*/
                           1077 ;	genCall
   01D8 12 05 46           1078 	lcall	_help_menu
   01DB 02 00 B8           1079 	ljmp	00128$
   01DE                    1080 00105$:
                           1081 ;	main.c:132: else if(temp=='@')
                           1082 ;	genCmpEq
                           1083 ;	gencjneshort
   01DE BA 40 02           1084 	cjne	r2,#0x40,00160$
   01E1 80 03              1085 	sjmp	00161$
   01E3                    1086 00160$:
   01E3 02 00 B8           1087 	ljmp	00128$
   01E6                    1088 00161$:
                           1089 ;	main.c:134: char_count++;
                           1090 ;	genAssign
   01E6 90 0B BC           1091 	mov	dptr,#_char_count
   01E9 E0                 1092 	movx	a,@dptr
   01EA FA                 1093 	mov	r2,a
   01EB A3                 1094 	inc	dptr
   01EC E0                 1095 	movx	a,@dptr
   01ED FB                 1096 	mov	r3,a
                           1097 ;	genPlus
   01EE 90 0B BC           1098 	mov	dptr,#_char_count
                           1099 ;     genPlusIncr
   01F1 74 01              1100 	mov	a,#0x01
                           1101 ;	Peephole 236.a	used r2 instead of ar2
   01F3 2A                 1102 	add	a,r2
   01F4 F0                 1103 	movx	@dptr,a
                           1104 ;	Peephole 181	changed mov to clr
   01F5 E4                 1105 	clr	a
                           1106 ;	Peephole 236.b	used r3 instead of ar3
   01F6 3B                 1107 	addc	a,r3
   01F7 A3                 1108 	inc	dptr
   01F8 F0                 1109 	movx	@dptr,a
                           1110 ;	main.c:135: freeEverything();  /* de-allocate every buffer*/
                           1111 ;	genCall
   01F9 12 07 F3           1112 	lcall	_freeEverything
                           1113 ;	main.c:136: break;
                           1114 ;	main.c:141: goto here;
   01FC 02 00 7E           1115 	ljmp	00101$
                           1116 ;	Peephole 259.b	removed redundant label 00130$ and ret
                           1117 ;
                           1118 ;------------------------------------------------------------
                           1119 ;Allocation info for local variables in function 'BufferInitialize'
                           1120 ;------------------------------------------------------------
                           1121 ;------------------------------------------------------------
                           1122 ;	main.c:144: void BufferInitialize()
                           1123 ;	-----------------------------------------
                           1124 ;	 function BufferInitialize
                           1125 ;	-----------------------------------------
   01FF                    1126 _BufferInitialize:
                           1127 ;	main.c:146: buffer[0]=0;
                           1128 ;	genPointerSet
                           1129 ;     genFarPointerSet
   01FF 90 0B C2           1130 	mov	dptr,#_buffer
                           1131 ;	Peephole 181	changed mov to clr
   0202 E4                 1132 	clr	a
   0203 F0                 1133 	movx	@dptr,a
   0204 A3                 1134 	inc	dptr
                           1135 ;	Peephole 101	removed redundant mov
   0205 F0                 1136 	movx	@dptr,a
                           1137 ;	main.c:147: buffer[1]=0;
                           1138 ;	genPointerSet
                           1139 ;     genFarPointerSet
   0206 90 0B C4           1140 	mov	dptr,#(_buffer + 0x0002)
                           1141 ;	Peephole 181	changed mov to clr
   0209 E4                 1142 	clr	a
   020A F0                 1143 	movx	@dptr,a
   020B A3                 1144 	inc	dptr
                           1145 ;	Peephole 101	removed redundant mov
   020C F0                 1146 	movx	@dptr,a
                           1147 ;	main.c:148: bptr0=NULL;
                           1148 ;	genAssign
   020D 90 0D C8           1149 	mov	dptr,#_bptr0
   0210 E4                 1150 	clr	a
   0211 F0                 1151 	movx	@dptr,a
   0212 A3                 1152 	inc	dptr
   0213 F0                 1153 	movx	@dptr,a
   0214 A3                 1154 	inc	dptr
   0215 F0                 1155 	movx	@dptr,a
                           1156 ;	main.c:149: bptr1=NULL;
                           1157 ;	genAssign
   0216 90 0D CB           1158 	mov	dptr,#_bptr1
   0219 E4                 1159 	clr	a
   021A F0                 1160 	movx	@dptr,a
   021B A3                 1161 	inc	dptr
   021C F0                 1162 	movx	@dptr,a
   021D A3                 1163 	inc	dptr
   021E F0                 1164 	movx	@dptr,a
                           1165 ;	main.c:150: putstr("\n\r");
                           1166 ;	genCall
                           1167 ;	Peephole 182.a	used 16 bit load of DPTR
   021F 90 1E 13           1168 	mov	dptr,#__str_3
   0222 75 F0 80           1169 	mov	b,#0x80
   0225 12 0F 16           1170 	lcall	_putstr
                           1171 ;	main.c:151: putstr("Input buffer size between 32 and 2800 bytes, divisible by 16\n\r");
                           1172 ;	genCall
                           1173 ;	Peephole 182.a	used 16 bit load of DPTR
   0228 90 1E 16           1174 	mov	dptr,#__str_4
   022B 75 F0 80           1175 	mov	b,#0x80
   022E 12 0F 16           1176 	lcall	_putstr
                           1177 ;	main.c:152: do
   0231                    1178 00113$:
                           1179 ;	main.c:154: res=ReadValue();
                           1180 ;	genCall
   0231 12 03 75           1181 	lcall	_ReadValue
   0234 AA 82              1182 	mov	r2,dpl
   0236 AB 83              1183 	mov	r3,dph
                           1184 ;	genAssign
   0238 90 0F D9           1185 	mov	dptr,#_res
   023B EA                 1186 	mov	a,r2
   023C F0                 1187 	movx	@dptr,a
   023D A3                 1188 	inc	dptr
   023E EB                 1189 	mov	a,r3
   023F F0                 1190 	movx	@dptr,a
                           1191 ;	main.c:155: if(res>31 && res<2801)
                           1192 ;	genAssign
   0240 8A 04              1193 	mov	ar4,r2
   0242 8B 05              1194 	mov	ar5,r3
                           1195 ;	genCmpGt
                           1196 ;	genCmp
   0244 C3                 1197 	clr	c
   0245 74 1F              1198 	mov	a,#0x1F
   0247 9C                 1199 	subb	a,r4
                           1200 ;	Peephole 181	changed mov to clr
   0248 E4                 1201 	clr	a
   0249 9D                 1202 	subb	a,r5
                           1203 ;	genIfxJump
   024A 40 03              1204 	jc	00126$
   024C 02 02 DB           1205 	ljmp	00109$
   024F                    1206 00126$:
                           1207 ;	genAssign
   024F 8A 04              1208 	mov	ar4,r2
   0251 8B 05              1209 	mov	ar5,r3
                           1210 ;	genCmpLt
                           1211 ;	genCmp
   0253 C3                 1212 	clr	c
   0254 EC                 1213 	mov	a,r4
   0255 94 F1              1214 	subb	a,#0xF1
   0257 ED                 1215 	mov	a,r5
   0258 94 0A              1216 	subb	a,#0x0A
                           1217 ;	genIfxJump
   025A 40 03              1218 	jc	00127$
   025C 02 02 DB           1219 	ljmp	00109$
   025F                    1220 00127$:
                           1221 ;	main.c:157: if(res%16==0)
                           1222 ;	genAssign
   025F 8A 04              1223 	mov	ar4,r2
   0261 8B 05              1224 	mov	ar5,r3
                           1225 ;	genAnd
   0263 EC                 1226 	mov	a,r4
   0264 54 0F              1227 	anl	a,#0x0F
                           1228 ;	Peephole 160.c	removed sjmp by inverse jump logic
   0266 60 02              1229 	jz	00129$
                           1230 ;	Peephole 300	removed redundant label 00128$
                           1231 ;	Peephole 112.b	changed ljmp to sjmp
   0268 80 66              1232 	sjmp	00106$
   026A                    1233 00129$:
                           1234 ;	main.c:159: if ((buffer[0] = malloc(res)) == 0)  //allocate buffer0
                           1235 ;	genCall
   026A 8A 82              1236 	mov	dpl,r2
   026C 8B 83              1237 	mov	dph,r3
   026E 12 11 90           1238 	lcall	_malloc
   0271 AA 82              1239 	mov	r2,dpl
   0273 AB 83              1240 	mov	r3,dph
                           1241 ;	genPointerSet
                           1242 ;     genFarPointerSet
   0275 90 0B C2           1243 	mov	dptr,#_buffer
   0278 EA                 1244 	mov	a,r2
   0279 F0                 1245 	movx	@dptr,a
   027A A3                 1246 	inc	dptr
   027B EB                 1247 	mov	a,r3
   027C F0                 1248 	movx	@dptr,a
                           1249 ;	genIfx
   027D EA                 1250 	mov	a,r2
   027E 4B                 1251 	orl	a,r3
                           1252 ;	genIfxJump
                           1253 ;	Peephole 108.b	removed ljmp by inverse jump logic
   027F 70 09              1254 	jnz	00102$
                           1255 ;	Peephole 300	removed redundant label 00130$
                           1256 ;	main.c:160: {putstr("malloc buffer0 failed\n\r");}
                           1257 ;	genCall
                           1258 ;	Peephole 182.a	used 16 bit load of DPTR
   0281 90 1E 55           1259 	mov	dptr,#__str_5
   0284 75 F0 80           1260 	mov	b,#0x80
   0287 12 0F 16           1261 	lcall	_putstr
   028A                    1262 00102$:
                           1263 ;	main.c:161: if ((buffer[1] = malloc(res)) == 0)         //allocate buffer1
                           1264 ;	genAssign
   028A 90 0F D9           1265 	mov	dptr,#_res
   028D E0                 1266 	movx	a,@dptr
   028E FA                 1267 	mov	r2,a
   028F A3                 1268 	inc	dptr
   0290 E0                 1269 	movx	a,@dptr
   0291 FB                 1270 	mov	r3,a
                           1271 ;	genCall
   0292 8A 82              1272 	mov	dpl,r2
   0294 8B 83              1273 	mov	dph,r3
   0296 12 11 90           1274 	lcall	_malloc
   0299 AA 82              1275 	mov	r2,dpl
   029B AB 83              1276 	mov	r3,dph
                           1277 ;	genPointerSet
                           1278 ;     genFarPointerSet
   029D 90 0B C4           1279 	mov	dptr,#(_buffer + 0x0002)
   02A0 EA                 1280 	mov	a,r2
   02A1 F0                 1281 	movx	@dptr,a
   02A2 A3                 1282 	inc	dptr
   02A3 EB                 1283 	mov	a,r3
   02A4 F0                 1284 	movx	@dptr,a
                           1285 ;	genIfx
   02A5 EA                 1286 	mov	a,r2
   02A6 4B                 1287 	orl	a,r3
                           1288 ;	genIfxJump
                           1289 ;	Peephole 108.b	removed ljmp by inverse jump logic
   02A7 70 3B              1290 	jnz	00114$
                           1291 ;	Peephole 300	removed redundant label 00131$
                           1292 ;	main.c:163: putstr("malloc buffer1 failed\n\r");
                           1293 ;	genCall
                           1294 ;	Peephole 182.a	used 16 bit load of DPTR
   02A9 90 1E 6D           1295 	mov	dptr,#__str_6
   02AC 75 F0 80           1296 	mov	b,#0x80
   02AF 12 0F 16           1297 	lcall	_putstr
                           1298 ;	main.c:164: putstr("Enter smaller size for buffer\n\r");
                           1299 ;	genCall
                           1300 ;	Peephole 182.a	used 16 bit load of DPTR
   02B2 90 1E 85           1301 	mov	dptr,#__str_7
   02B5 75 F0 80           1302 	mov	b,#0x80
   02B8 12 0F 16           1303 	lcall	_putstr
                           1304 ;	main.c:165: free (buffer[0]);  // if buffer1 malloc fails, free buffer 0
                           1305 ;	genPointerGet
                           1306 ;	genFarPointerGet
   02BB 90 0B C2           1307 	mov	dptr,#_buffer
   02BE E0                 1308 	movx	a,@dptr
   02BF FA                 1309 	mov	r2,a
   02C0 A3                 1310 	inc	dptr
   02C1 E0                 1311 	movx	a,@dptr
   02C2 FB                 1312 	mov	r3,a
                           1313 ;	genCast
   02C3 7C 00              1314 	mov	r4,#0x0
                           1315 ;	genCall
   02C5 8A 82              1316 	mov	dpl,r2
   02C7 8B 83              1317 	mov	dph,r3
   02C9 8C F0              1318 	mov	b,r4
   02CB 12 10 21           1319 	lcall	_free
                           1320 ;	Peephole 112.b	changed ljmp to sjmp
   02CE 80 14              1321 	sjmp	00114$
   02D0                    1322 00106$:
                           1323 ;	main.c:169: putstr("\n\rEnter the correct buffer size value divisible by 16\n\r");
                           1324 ;	genCall
                           1325 ;	Peephole 182.a	used 16 bit load of DPTR
   02D0 90 1E A5           1326 	mov	dptr,#__str_8
   02D3 75 F0 80           1327 	mov	b,#0x80
   02D6 12 0F 16           1328 	lcall	_putstr
                           1329 ;	Peephole 112.b	changed ljmp to sjmp
   02D9 80 09              1330 	sjmp	00114$
   02DB                    1331 00109$:
                           1332 ;	main.c:172: putstr("\n\rEnter the correct buffer size between 32 and 2800\n\r");
                           1333 ;	genCall
                           1334 ;	Peephole 182.a	used 16 bit load of DPTR
   02DB 90 1E DD           1335 	mov	dptr,#__str_9
   02DE 75 F0 80           1336 	mov	b,#0x80
   02E1 12 0F 16           1337 	lcall	_putstr
   02E4                    1338 00114$:
                           1339 ;	main.c:173: } while((buffer[0] == 0)||(buffer[1] ==0));
                           1340 ;	genPointerGet
                           1341 ;	genFarPointerGet
   02E4 90 0B C2           1342 	mov	dptr,#_buffer
   02E7 E0                 1343 	movx	a,@dptr
   02E8 FA                 1344 	mov	r2,a
   02E9 A3                 1345 	inc	dptr
   02EA E0                 1346 	movx	a,@dptr
                           1347 ;	genIfx
   02EB FB                 1348 	mov	r3,a
                           1349 ;	Peephole 135	removed redundant mov
   02EC 4A                 1350 	orl	a,r2
                           1351 ;	genIfxJump
   02ED 70 03              1352 	jnz	00132$
   02EF 02 02 31           1353 	ljmp	00113$
   02F2                    1354 00132$:
                           1355 ;	genPointerGet
                           1356 ;	genFarPointerGet
   02F2 90 0B C4           1357 	mov	dptr,#(_buffer + 0x0002)
   02F5 E0                 1358 	movx	a,@dptr
   02F6 FA                 1359 	mov	r2,a
   02F7 A3                 1360 	inc	dptr
   02F8 E0                 1361 	movx	a,@dptr
                           1362 ;	genIfx
   02F9 FB                 1363 	mov	r3,a
                           1364 ;	Peephole 135	removed redundant mov
   02FA 4A                 1365 	orl	a,r2
                           1366 ;	genIfxJump
   02FB 70 03              1367 	jnz	00133$
   02FD 02 02 31           1368 	ljmp	00113$
   0300                    1369 00133$:
                           1370 ;	main.c:174: putstr("\n\rBuffer allocation successful\n\r");
                           1371 ;	genCall
                           1372 ;	Peephole 182.a	used 16 bit load of DPTR
   0300 90 1F 13           1373 	mov	dptr,#__str_10
   0303 75 F0 80           1374 	mov	b,#0x80
   0306 12 0F 16           1375 	lcall	_putstr
                           1376 ;	main.c:175: buffer_number=buffer_number+2;
                           1377 ;	genAssign
   0309 90 0B BE           1378 	mov	dptr,#_buffer_number
   030C E0                 1379 	movx	a,@dptr
   030D FA                 1380 	mov	r2,a
   030E A3                 1381 	inc	dptr
   030F E0                 1382 	movx	a,@dptr
   0310 FB                 1383 	mov	r3,a
                           1384 ;	genPlus
   0311 90 0B BE           1385 	mov	dptr,#_buffer_number
                           1386 ;     genPlusIncr
   0314 74 02              1387 	mov	a,#0x02
                           1388 ;	Peephole 236.a	used r2 instead of ar2
   0316 2A                 1389 	add	a,r2
   0317 F0                 1390 	movx	@dptr,a
                           1391 ;	Peephole 181	changed mov to clr
   0318 E4                 1392 	clr	a
                           1393 ;	Peephole 236.b	used r3 instead of ar3
   0319 3B                 1394 	addc	a,r3
   031A A3                 1395 	inc	dptr
   031B F0                 1396 	movx	@dptr,a
                           1397 ;	main.c:176: bufflen[0]=res;
                           1398 ;	genAssign
   031C 90 0F D9           1399 	mov	dptr,#_res
   031F E0                 1400 	movx	a,@dptr
   0320 FA                 1401 	mov	r2,a
   0321 A3                 1402 	inc	dptr
   0322 E0                 1403 	movx	a,@dptr
   0323 FB                 1404 	mov	r3,a
                           1405 ;	genPointerSet
                           1406 ;     genFarPointerSet
   0324 90 0D CE           1407 	mov	dptr,#_bufflen
   0327 EA                 1408 	mov	a,r2
   0328 F0                 1409 	movx	@dptr,a
   0329 A3                 1410 	inc	dptr
   032A EB                 1411 	mov	a,r3
   032B F0                 1412 	movx	@dptr,a
                           1413 ;	main.c:177: bufflen[1]=res;         /* buffer lengths */
                           1414 ;	genPointerSet
                           1415 ;     genFarPointerSet
   032C 90 0D D0           1416 	mov	dptr,#(_bufflen + 0x0002)
   032F EA                 1417 	mov	a,r2
   0330 F0                 1418 	movx	@dptr,a
   0331 A3                 1419 	inc	dptr
   0332 EB                 1420 	mov	a,r3
   0333 F0                 1421 	movx	@dptr,a
                           1422 ;	main.c:178: buff1ptr=buffer[0];     /* assign pointers to buffer to do certain actions */
                           1423 ;	genPointerGet
                           1424 ;	genFarPointerGet
   0334 90 0B C2           1425 	mov	dptr,#_buffer
   0337 E0                 1426 	movx	a,@dptr
   0338 FA                 1427 	mov	r2,a
   0339 A3                 1428 	inc	dptr
   033A E0                 1429 	movx	a,@dptr
   033B FB                 1430 	mov	r3,a
                           1431 ;	genCast
   033C 90 0D C2           1432 	mov	dptr,#_buff1ptr
   033F EA                 1433 	mov	a,r2
   0340 F0                 1434 	movx	@dptr,a
   0341 A3                 1435 	inc	dptr
   0342 EB                 1436 	mov	a,r3
   0343 F0                 1437 	movx	@dptr,a
   0344 A3                 1438 	inc	dptr
   0345 74 00              1439 	mov	a,#0x0
   0347 F0                 1440 	movx	@dptr,a
                           1441 ;	main.c:179: buff2ptr=buffer[1];
                           1442 ;	genPointerGet
                           1443 ;	genFarPointerGet
   0348 90 0B C4           1444 	mov	dptr,#(_buffer + 0x0002)
   034B E0                 1445 	movx	a,@dptr
   034C FC                 1446 	mov	r4,a
   034D A3                 1447 	inc	dptr
   034E E0                 1448 	movx	a,@dptr
   034F FD                 1449 	mov	r5,a
                           1450 ;	genCast
   0350 90 0D C5           1451 	mov	dptr,#_buff2ptr
   0353 EC                 1452 	mov	a,r4
   0354 F0                 1453 	movx	@dptr,a
   0355 A3                 1454 	inc	dptr
   0356 ED                 1455 	mov	a,r5
   0357 F0                 1456 	movx	@dptr,a
   0358 A3                 1457 	inc	dptr
   0359 74 00              1458 	mov	a,#0x0
   035B F0                 1459 	movx	@dptr,a
                           1460 ;	main.c:180: bptr0=buffer[0];
                           1461 ;	genAssign
                           1462 ;	genCast
   035C 90 0D C8           1463 	mov	dptr,#_bptr0
   035F EA                 1464 	mov	a,r2
   0360 F0                 1465 	movx	@dptr,a
   0361 A3                 1466 	inc	dptr
   0362 EB                 1467 	mov	a,r3
   0363 F0                 1468 	movx	@dptr,a
   0364 A3                 1469 	inc	dptr
   0365 74 00              1470 	mov	a,#0x0
   0367 F0                 1471 	movx	@dptr,a
                           1472 ;	main.c:181: bptr1=buffer[1];
                           1473 ;	genAssign
                           1474 ;	genCast
   0368 90 0D CB           1475 	mov	dptr,#_bptr1
   036B EC                 1476 	mov	a,r4
   036C F0                 1477 	movx	@dptr,a
   036D A3                 1478 	inc	dptr
   036E ED                 1479 	mov	a,r5
   036F F0                 1480 	movx	@dptr,a
   0370 A3                 1481 	inc	dptr
   0371 74 00              1482 	mov	a,#0x0
   0373 F0                 1483 	movx	@dptr,a
                           1484 ;	Peephole 300	removed redundant label 00116$
   0374 22                 1485 	ret
                           1486 ;------------------------------------------------------------
                           1487 ;Allocation info for local variables in function 'ReadValue'
                           1488 ;------------------------------------------------------------
                           1489 ;------------------------------------------------------------
                           1490 ;	main.c:185: int ReadValue()
                           1491 ;	-----------------------------------------
                           1492 ;	 function ReadValue
                           1493 ;	-----------------------------------------
   0375                    1494 _ReadValue:
                           1495 ;	main.c:187: value=0;
                           1496 ;	genAssign
   0375 90 0F D7           1497 	mov	dptr,#_value
   0378 E4                 1498 	clr	a
   0379 F0                 1499 	movx	@dptr,a
   037A A3                 1500 	inc	dptr
   037B F0                 1501 	movx	@dptr,a
                           1502 ;	main.c:188: do
   037C                    1503 00110$:
                           1504 ;	main.c:190: i=getchar();
                           1505 ;	genCall
   037C 12 0F 82           1506 	lcall	_getchar
   037F AA 82              1507 	mov	r2,dpl
                           1508 ;	genAssign
   0381 90 10 42           1509 	mov	dptr,#_i
   0384 EA                 1510 	mov	a,r2
   0385 F0                 1511 	movx	@dptr,a
                           1512 ;	main.c:191: if(i>='0'&&i<='9')          /* works only if its a number */
                           1513 ;	genAssign
   0386 8A 03              1514 	mov	ar3,r2
                           1515 ;	genCmpLt
                           1516 ;	genCmp
   0388 BB 30 00           1517 	cjne	r3,#0x30,00121$
   038B                    1518 00121$:
                           1519 ;	genIfxJump
                           1520 ;	Peephole 112.b	changed ljmp to sjmp
                           1521 ;	Peephole 160.a	removed sjmp by inverse jump logic
   038B 40 50              1522 	jc	00107$
                           1523 ;	Peephole 300	removed redundant label 00122$
                           1524 ;	genAssign
   038D 8A 03              1525 	mov	ar3,r2
                           1526 ;	genCmpGt
                           1527 ;	genCmp
                           1528 ;	genIfxJump
                           1529 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   038F EB                 1530 	mov	a,r3
   0390 24 C6              1531 	add	a,#0xff - 0x39
                           1532 ;	Peephole 112.b	changed ljmp to sjmp
                           1533 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0392 40 49              1534 	jc	00107$
                           1535 ;	Peephole 300	removed redundant label 00123$
                           1536 ;	main.c:193: value=((value*10)+(i-'0')); /* Convert character to integers */
                           1537 ;	genAssign
   0394 90 0F D7           1538 	mov	dptr,#_value
   0397 E0                 1539 	movx	a,@dptr
   0398 FB                 1540 	mov	r3,a
   0399 A3                 1541 	inc	dptr
   039A E0                 1542 	movx	a,@dptr
   039B FC                 1543 	mov	r4,a
                           1544 ;	genAssign
   039C 90 10 0D           1545 	mov	dptr,#__mulint_PARM_2
   039F 74 0A              1546 	mov	a,#0x0A
   03A1 F0                 1547 	movx	@dptr,a
   03A2 E4                 1548 	clr	a
   03A3 A3                 1549 	inc	dptr
   03A4 F0                 1550 	movx	@dptr,a
                           1551 ;	genCall
   03A5 8B 82              1552 	mov	dpl,r3
   03A7 8C 83              1553 	mov	dph,r4
   03A9 C0 02              1554 	push	ar2
   03AB 12 13 ED           1555 	lcall	__mulint
   03AE AB 82              1556 	mov	r3,dpl
   03B0 AC 83              1557 	mov	r4,dph
   03B2 D0 02              1558 	pop	ar2
                           1559 ;	genAssign
                           1560 ;	genCast
   03B4 7D 00              1561 	mov	r5,#0x00
                           1562 ;	genMinus
   03B6 EA                 1563 	mov	a,r2
   03B7 24 D0              1564 	add	a,#0xd0
   03B9 FE                 1565 	mov	r6,a
   03BA ED                 1566 	mov	a,r5
   03BB 34 FF              1567 	addc	a,#0xff
   03BD FF                 1568 	mov	r7,a
                           1569 ;	genPlus
   03BE 90 0F D7           1570 	mov	dptr,#_value
                           1571 ;	Peephole 236.g	used r6 instead of ar6
   03C1 EE                 1572 	mov	a,r6
                           1573 ;	Peephole 236.a	used r3 instead of ar3
   03C2 2B                 1574 	add	a,r3
   03C3 F0                 1575 	movx	@dptr,a
                           1576 ;	Peephole 236.g	used r7 instead of ar7
   03C4 EF                 1577 	mov	a,r7
                           1578 ;	Peephole 236.b	used r4 instead of ar4
   03C5 3C                 1579 	addc	a,r4
   03C6 A3                 1580 	inc	dptr
   03C7 F0                 1581 	movx	@dptr,a
                           1582 ;	main.c:194: putchar(i);
                           1583 ;	genCall
   03C8 8A 82              1584 	mov	dpl,r2
   03CA 8D 83              1585 	mov	dph,r5
   03CC 12 0F 91           1586 	lcall	_putchar
                           1587 ;	main.c:195: j++;
                           1588 ;	genAssign
   03CF 90 0F D1           1589 	mov	dptr,#_j
   03D2 E0                 1590 	movx	a,@dptr
   03D3 FA                 1591 	mov	r2,a
                           1592 ;	genPlus
   03D4 90 0F D1           1593 	mov	dptr,#_j
                           1594 ;     genPlusIncr
   03D7 74 01              1595 	mov	a,#0x01
                           1596 ;	Peephole 236.a	used r2 instead of ar2
   03D9 2A                 1597 	add	a,r2
   03DA F0                 1598 	movx	@dptr,a
                           1599 ;	Peephole 112.b	changed ljmp to sjmp
   03DB 80 3D              1600 	sjmp	00111$
   03DD                    1601 00107$:
                           1602 ;	main.c:197: else if(i==127)
                           1603 ;	genAssign
   03DD 90 10 42           1604 	mov	dptr,#_i
   03E0 E0                 1605 	movx	a,@dptr
   03E1 FA                 1606 	mov	r2,a
                           1607 ;	genCmpEq
                           1608 ;	gencjneshort
                           1609 ;	Peephole 112.b	changed ljmp to sjmp
                           1610 ;	Peephole 198.b	optimized misc jump sequence
   03E2 BA 7F 27           1611 	cjne	r2,#0x7F,00104$
                           1612 ;	Peephole 200.b	removed redundant sjmp
                           1613 ;	Peephole 300	removed redundant label 00124$
                           1614 ;	Peephole 300	removed redundant label 00125$
                           1615 ;	main.c:199: value=value/10;
                           1616 ;	genAssign
   03E5 90 0F D7           1617 	mov	dptr,#_value
   03E8 E0                 1618 	movx	a,@dptr
   03E9 FB                 1619 	mov	r3,a
   03EA A3                 1620 	inc	dptr
   03EB E0                 1621 	movx	a,@dptr
   03EC FC                 1622 	mov	r4,a
                           1623 ;	genAssign
   03ED 90 0F F8           1624 	mov	dptr,#__divuint_PARM_2
   03F0 74 0A              1625 	mov	a,#0x0A
   03F2 F0                 1626 	movx	@dptr,a
   03F3 E4                 1627 	clr	a
   03F4 A3                 1628 	inc	dptr
   03F5 F0                 1629 	movx	@dptr,a
                           1630 ;	genCall
   03F6 8B 82              1631 	mov	dpl,r3
   03F8 8C 83              1632 	mov	dph,r4
   03FA 12 10 74           1633 	lcall	__divuint
   03FD E5 82              1634 	mov	a,dpl
   03FF 85 83 F0           1635 	mov	b,dph
                           1636 ;	genAssign
   0402 90 0F D7           1637 	mov	dptr,#_value
   0405 F0                 1638 	movx	@dptr,a
   0406 A3                 1639 	inc	dptr
   0407 E5 F0              1640 	mov	a,b
   0409 F0                 1641 	movx	@dptr,a
                           1642 ;	Peephole 112.b	changed ljmp to sjmp
   040A 80 0E              1643 	sjmp	00111$
   040C                    1644 00104$:
                           1645 ;	main.c:201: else if(i!=13)
                           1646 ;	genCmpEq
                           1647 ;	gencjneshort
   040C BA 0D 02           1648 	cjne	r2,#0x0D,00126$
                           1649 ;	Peephole 112.b	changed ljmp to sjmp
   040F 80 09              1650 	sjmp	00111$
   0411                    1651 00126$:
                           1652 ;	main.c:202: {putstr("\n\rEnter Numbers\n\r");}
                           1653 ;	genCall
                           1654 ;	Peephole 182.a	used 16 bit load of DPTR
   0411 90 1F 34           1655 	mov	dptr,#__str_11
   0414 75 F0 80           1656 	mov	b,#0x80
   0417 12 0F 16           1657 	lcall	_putstr
   041A                    1658 00111$:
                           1659 ;	main.c:203: }while(i!=13);
                           1660 ;	genAssign
   041A 90 10 42           1661 	mov	dptr,#_i
   041D E0                 1662 	movx	a,@dptr
   041E FA                 1663 	mov	r2,a
                           1664 ;	genCmpEq
                           1665 ;	gencjneshort
   041F BA 0D 02           1666 	cjne	r2,#0x0D,00127$
   0422 80 03              1667 	sjmp	00128$
   0424                    1668 00127$:
   0424 02 03 7C           1669 	ljmp	00110$
   0427                    1670 00128$:
                           1671 ;	main.c:204: j=0;
                           1672 ;	genAssign
   0427 90 0F D1           1673 	mov	dptr,#_j
                           1674 ;	Peephole 181	changed mov to clr
   042A E4                 1675 	clr	a
   042B F0                 1676 	movx	@dptr,a
                           1677 ;	main.c:205: return value;
                           1678 ;	genAssign
   042C 90 0F D7           1679 	mov	dptr,#_value
   042F E0                 1680 	movx	a,@dptr
   0430 FA                 1681 	mov	r2,a
   0431 A3                 1682 	inc	dptr
   0432 E0                 1683 	movx	a,@dptr
                           1684 ;	genRet
                           1685 ;	Peephole 234.b	loading dph directly from a(ccumulator), r3 not set
   0433 8A 82              1686 	mov	dpl,r2
   0435 F5 83              1687 	mov	dph,a
                           1688 ;	Peephole 300	removed redundant label 00113$
   0437 22                 1689 	ret
                           1690 ;------------------------------------------------------------
                           1691 ;Allocation info for local variables in function 'newbuffer'
                           1692 ;------------------------------------------------------------
                           1693 ;------------------------------------------------------------
                           1694 ;	main.c:208: void newbuffer()
                           1695 ;	-----------------------------------------
                           1696 ;	 function newbuffer
                           1697 ;	-----------------------------------------
   0438                    1698 _newbuffer:
                           1699 ;	main.c:210: putstr("\n\r");
                           1700 ;	genCall
                           1701 ;	Peephole 182.a	used 16 bit load of DPTR
   0438 90 1E 13           1702 	mov	dptr,#__str_3
   043B 75 F0 80           1703 	mov	b,#0x80
   043E 12 0F 16           1704 	lcall	_putstr
                           1705 ;	main.c:211: putstr("\n\rEnter the buffer size between 20 and 400\n\r");
                           1706 ;	genCall
                           1707 ;	Peephole 182.a	used 16 bit load of DPTR
   0441 90 1F 46           1708 	mov	dptr,#__str_12
   0444 75 F0 80           1709 	mov	b,#0x80
   0447 12 0F 16           1710 	lcall	_putstr
                           1711 ;	main.c:213: res = ReadValue();
                           1712 ;	genCall
   044A 12 03 75           1713 	lcall	_ReadValue
   044D AA 82              1714 	mov	r2,dpl
   044F AB 83              1715 	mov	r3,dph
                           1716 ;	genAssign
   0451 90 0F D9           1717 	mov	dptr,#_res
   0454 EA                 1718 	mov	a,r2
   0455 F0                 1719 	movx	@dptr,a
   0456 A3                 1720 	inc	dptr
   0457 EB                 1721 	mov	a,r3
   0458 F0                 1722 	movx	@dptr,a
                           1723 ;	main.c:214: if(res>=20 && res<=400)     /* if size is in between the mentioned size*/
                           1724 ;	genAssign
   0459 8A 04              1725 	mov	ar4,r2
   045B 8B 05              1726 	mov	ar5,r3
                           1727 ;	genCmpLt
                           1728 ;	genCmp
   045D C3                 1729 	clr	c
   045E EC                 1730 	mov	a,r4
   045F 94 14              1731 	subb	a,#0x14
   0461 ED                 1732 	mov	a,r5
   0462 94 00              1733 	subb	a,#0x00
                           1734 ;	genIfxJump
   0464 50 03              1735 	jnc	00113$
   0466 02 05 16           1736 	ljmp	00105$
   0469                    1737 00113$:
                           1738 ;	genAssign
   0469 8A 04              1739 	mov	ar4,r2
   046B 8B 05              1740 	mov	ar5,r3
                           1741 ;	genCmpGt
                           1742 ;	genCmp
   046D C3                 1743 	clr	c
   046E 74 90              1744 	mov	a,#0x90
   0470 9C                 1745 	subb	a,r4
   0471 74 01              1746 	mov	a,#0x01
   0473 9D                 1747 	subb	a,r5
                           1748 ;	genIfxJump
   0474 50 03              1749 	jnc	00114$
   0476 02 05 16           1750 	ljmp	00105$
   0479                    1751 00114$:
                           1752 ;	main.c:216: if((buffer[buffer_count]=malloc(res))==0)       /* allocate buffer */
                           1753 ;	genAssign
   0479 90 0B B8           1754 	mov	dptr,#_buffer_count
   047C E0                 1755 	movx	a,@dptr
   047D FC                 1756 	mov	r4,a
   047E A3                 1757 	inc	dptr
   047F E0                 1758 	movx	a,@dptr
                           1759 ;	genLeftShift
                           1760 ;	genLeftShiftLiteral
                           1761 ;	genlshTwo
   0480 FD                 1762 	mov	r5,a
                           1763 ;	Peephole 105	removed redundant mov
   0481 CC                 1764 	xch	a,r4
   0482 25 E0              1765 	add	a,acc
   0484 CC                 1766 	xch	a,r4
   0485 33                 1767 	rlc	a
   0486 FD                 1768 	mov	r5,a
                           1769 ;	genPlus
                           1770 ;	Peephole 236.g	used r4 instead of ar4
   0487 EC                 1771 	mov	a,r4
   0488 24 C2              1772 	add	a,#_buffer
   048A FC                 1773 	mov	r4,a
                           1774 ;	Peephole 236.g	used r5 instead of ar5
   048B ED                 1775 	mov	a,r5
   048C 34 0B              1776 	addc	a,#(_buffer >> 8)
   048E FD                 1777 	mov	r5,a
                           1778 ;	genCall
   048F 8A 82              1779 	mov	dpl,r2
   0491 8B 83              1780 	mov	dph,r3
   0493 C0 04              1781 	push	ar4
   0495 C0 05              1782 	push	ar5
   0497 12 11 90           1783 	lcall	_malloc
   049A AA 82              1784 	mov	r2,dpl
   049C AB 83              1785 	mov	r3,dph
   049E D0 05              1786 	pop	ar5
   04A0 D0 04              1787 	pop	ar4
                           1788 ;	genPointerSet
                           1789 ;     genFarPointerSet
   04A2 8C 82              1790 	mov	dpl,r4
   04A4 8D 83              1791 	mov	dph,r5
   04A6 EA                 1792 	mov	a,r2
   04A7 F0                 1793 	movx	@dptr,a
   04A8 A3                 1794 	inc	dptr
   04A9 EB                 1795 	mov	a,r3
   04AA F0                 1796 	movx	@dptr,a
                           1797 ;	genIfx
   04AB EA                 1798 	mov	a,r2
   04AC 4B                 1799 	orl	a,r3
                           1800 ;	genIfxJump
                           1801 ;	Peephole 108.b	removed ljmp by inverse jump logic
   04AD 70 0B              1802 	jnz	00102$
                           1803 ;	Peephole 300	removed redundant label 00115$
                           1804 ;	main.c:217: putstr("\n\rBuffer allocation failed\n\r");
                           1805 ;	genCall
                           1806 ;	Peephole 182.a	used 16 bit load of DPTR
   04AF 90 1F 73           1807 	mov	dptr,#__str_13
   04B2 75 F0 80           1808 	mov	b,#0x80
   04B5 12 0F 16           1809 	lcall	_putstr
                           1810 ;	Peephole 112.b	changed ljmp to sjmp
   04B8 80 65              1811 	sjmp	00106$
   04BA                    1812 00102$:
                           1813 ;	main.c:219: {putstr("\n\rBuffer Allocated\n\r");
                           1814 ;	genCall
                           1815 ;	Peephole 182.a	used 16 bit load of DPTR
   04BA 90 1F 90           1816 	mov	dptr,#__str_14
   04BD 75 F0 80           1817 	mov	b,#0x80
   04C0 12 0F 16           1818 	lcall	_putstr
                           1819 ;	main.c:220: bufflen[buffer_count]=res;                  /* take down buffer length */
                           1820 ;	genAssign
   04C3 90 0B B8           1821 	mov	dptr,#_buffer_count
   04C6 E0                 1822 	movx	a,@dptr
   04C7 FA                 1823 	mov	r2,a
   04C8 A3                 1824 	inc	dptr
   04C9 E0                 1825 	movx	a,@dptr
                           1826 ;	genLeftShift
                           1827 ;	genLeftShiftLiteral
                           1828 ;	genlshTwo
   04CA FB                 1829 	mov	r3,a
   04CB 8A 04              1830 	mov	ar4,r2
                           1831 ;	Peephole 177.d	removed redundant move
   04CD CC                 1832 	xch	a,r4
   04CE 25 E0              1833 	add	a,acc
   04D0 CC                 1834 	xch	a,r4
   04D1 33                 1835 	rlc	a
   04D2 FD                 1836 	mov	r5,a
                           1837 ;	genPlus
                           1838 ;	Peephole 236.g	used r4 instead of ar4
   04D3 EC                 1839 	mov	a,r4
   04D4 24 CE              1840 	add	a,#_bufflen
   04D6 FE                 1841 	mov	r6,a
                           1842 ;	Peephole 236.g	used r5 instead of ar5
   04D7 ED                 1843 	mov	a,r5
   04D8 34 0D              1844 	addc	a,#(_bufflen >> 8)
   04DA FF                 1845 	mov	r7,a
                           1846 ;	genAssign
   04DB 90 0F D9           1847 	mov	dptr,#_res
   04DE E0                 1848 	movx	a,@dptr
   04DF F8                 1849 	mov	r0,a
   04E0 A3                 1850 	inc	dptr
   04E1 E0                 1851 	movx	a,@dptr
   04E2 F9                 1852 	mov	r1,a
                           1853 ;	genPointerSet
                           1854 ;     genFarPointerSet
   04E3 8E 82              1855 	mov	dpl,r6
   04E5 8F 83              1856 	mov	dph,r7
   04E7 E8                 1857 	mov	a,r0
   04E8 F0                 1858 	movx	@dptr,a
   04E9 A3                 1859 	inc	dptr
   04EA E9                 1860 	mov	a,r1
   04EB F0                 1861 	movx	@dptr,a
                           1862 ;	main.c:221: printf_tiny("Buffer address and buffer number 0x%x, %d\n\r", (unsigned int) buffer[buffer_count], buffer_count);
                           1863 ;	genPlus
                           1864 ;	Peephole 236.g	used r4 instead of ar4
   04EC EC                 1865 	mov	a,r4
   04ED 24 C2              1866 	add	a,#_buffer
   04EF F5 82              1867 	mov	dpl,a
                           1868 ;	Peephole 236.g	used r5 instead of ar5
   04F1 ED                 1869 	mov	a,r5
   04F2 34 0B              1870 	addc	a,#(_buffer >> 8)
   04F4 F5 83              1871 	mov	dph,a
                           1872 ;	genPointerGet
                           1873 ;	genFarPointerGet
   04F6 E0                 1874 	movx	a,@dptr
   04F7 FC                 1875 	mov	r4,a
   04F8 A3                 1876 	inc	dptr
   04F9 E0                 1877 	movx	a,@dptr
   04FA FD                 1878 	mov	r5,a
                           1879 ;	genCast
                           1880 ;	genIpush
   04FB C0 02              1881 	push	ar2
   04FD C0 03              1882 	push	ar3
                           1883 ;	genIpush
   04FF C0 04              1884 	push	ar4
   0501 C0 05              1885 	push	ar5
                           1886 ;	genIpush
   0503 74 A5              1887 	mov	a,#__str_15
   0505 C0 E0              1888 	push	acc
   0507 74 1F              1889 	mov	a,#(__str_15 >> 8)
   0509 C0 E0              1890 	push	acc
                           1891 ;	genCall
   050B 12 12 E5           1892 	lcall	_printf_tiny
   050E E5 81              1893 	mov	a,sp
   0510 24 FA              1894 	add	a,#0xfa
   0512 F5 81              1895 	mov	sp,a
                           1896 ;	Peephole 112.b	changed ljmp to sjmp
   0514 80 09              1897 	sjmp	00106$
   0516                    1898 00105$:
                           1899 ;	main.c:225: putstr("\n\rNot a valid buffer size\n\r");
                           1900 ;	genCall
                           1901 ;	Peephole 182.a	used 16 bit load of DPTR
   0516 90 1F D1           1902 	mov	dptr,#__str_16
   0519 75 F0 80           1903 	mov	b,#0x80
   051C 12 0F 16           1904 	lcall	_putstr
   051F                    1905 00106$:
                           1906 ;	main.c:226: buffer_count++;         /* increment buffer number and buffer count */
                           1907 ;	genAssign
   051F 90 0B B8           1908 	mov	dptr,#_buffer_count
   0522 E0                 1909 	movx	a,@dptr
   0523 FA                 1910 	mov	r2,a
   0524 A3                 1911 	inc	dptr
   0525 E0                 1912 	movx	a,@dptr
   0526 FB                 1913 	mov	r3,a
                           1914 ;	genPlus
   0527 90 0B B8           1915 	mov	dptr,#_buffer_count
                           1916 ;     genPlusIncr
   052A 74 01              1917 	mov	a,#0x01
                           1918 ;	Peephole 236.a	used r2 instead of ar2
   052C 2A                 1919 	add	a,r2
   052D F0                 1920 	movx	@dptr,a
                           1921 ;	Peephole 181	changed mov to clr
   052E E4                 1922 	clr	a
                           1923 ;	Peephole 236.b	used r3 instead of ar3
   052F 3B                 1924 	addc	a,r3
   0530 A3                 1925 	inc	dptr
   0531 F0                 1926 	movx	@dptr,a
                           1927 ;	main.c:227: buffer_number++;
                           1928 ;	genAssign
   0532 90 0B BE           1929 	mov	dptr,#_buffer_number
   0535 E0                 1930 	movx	a,@dptr
   0536 FA                 1931 	mov	r2,a
   0537 A3                 1932 	inc	dptr
   0538 E0                 1933 	movx	a,@dptr
   0539 FB                 1934 	mov	r3,a
                           1935 ;	genPlus
   053A 90 0B BE           1936 	mov	dptr,#_buffer_number
                           1937 ;     genPlusIncr
   053D 74 01              1938 	mov	a,#0x01
                           1939 ;	Peephole 236.a	used r2 instead of ar2
   053F 2A                 1940 	add	a,r2
   0540 F0                 1941 	movx	@dptr,a
                           1942 ;	Peephole 181	changed mov to clr
   0541 E4                 1943 	clr	a
                           1944 ;	Peephole 236.b	used r3 instead of ar3
   0542 3B                 1945 	addc	a,r3
   0543 A3                 1946 	inc	dptr
   0544 F0                 1947 	movx	@dptr,a
                           1948 ;	Peephole 300	removed redundant label 00108$
   0545 22                 1949 	ret
                           1950 ;------------------------------------------------------------
                           1951 ;Allocation info for local variables in function 'help_menu'
                           1952 ;------------------------------------------------------------
                           1953 ;------------------------------------------------------------
                           1954 ;	main.c:230: void help_menu()
                           1955 ;	-----------------------------------------
                           1956 ;	 function help_menu
                           1957 ;	-----------------------------------------
   0546                    1958 _help_menu:
                           1959 ;	main.c:232: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HELP MENU~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           1960 ;	genIpush
   0546 74 ED              1961 	mov	a,#__str_17
   0548 C0 E0              1962 	push	acc
   054A 74 1F              1963 	mov	a,#(__str_17 >> 8)
   054C C0 E0              1964 	push	acc
                           1965 ;	genCall
   054E 12 12 E5           1966 	lcall	_printf_tiny
   0551 15 81              1967 	dec	sp
   0553 15 81              1968 	dec	sp
                           1969 ;	main.c:233: printf_tiny("\n\r '+' Allocate new buffer\n\r");
                           1970 ;	genIpush
   0555 74 49              1971 	mov	a,#__str_18
   0557 C0 E0              1972 	push	acc
   0559 74 20              1973 	mov	a,#(__str_18 >> 8)
   055B C0 E0              1974 	push	acc
                           1975 ;	genCall
   055D 12 12 E5           1976 	lcall	_printf_tiny
   0560 15 81              1977 	dec	sp
   0562 15 81              1978 	dec	sp
                           1979 ;	main.c:234: printf_tiny(" '-' Delete a buffer\n\r");
                           1980 ;	genIpush
   0564 74 66              1981 	mov	a,#__str_19
   0566 C0 E0              1982 	push	acc
   0568 74 20              1983 	mov	a,#(__str_19 >> 8)
   056A C0 E0              1984 	push	acc
                           1985 ;	genCall
   056C 12 12 E5           1986 	lcall	_printf_tiny
   056F 15 81              1987 	dec	sp
   0571 15 81              1988 	dec	sp
                           1989 ;	main.c:235: printf_tiny(" '@' Start over from allocating heap\n\r");
                           1990 ;	genIpush
   0573 74 7D              1991 	mov	a,#__str_20
   0575 C0 E0              1992 	push	acc
   0577 74 20              1993 	mov	a,#(__str_20 >> 8)
   0579 C0 E0              1994 	push	acc
                           1995 ;	genCall
   057B 12 12 E5           1996 	lcall	_printf_tiny
   057E 15 81              1997 	dec	sp
   0580 15 81              1998 	dec	sp
                           1999 ;	main.c:236: printf_tiny(" '?' Display all buffer details and buffer0, buffer1 contents and erase it \n\r");
                           2000 ;	genIpush
   0582 74 A4              2001 	mov	a,#__str_21
   0584 C0 E0              2002 	push	acc
   0586 74 20              2003 	mov	a,#(__str_21 >> 8)
   0588 C0 E0              2004 	push	acc
                           2005 ;	genCall
   058A 12 12 E5           2006 	lcall	_printf_tiny
   058D 15 81              2007 	dec	sp
   058F 15 81              2008 	dec	sp
                           2009 ;	main.c:237: printf_tiny(" '=' Display the contents of the buffer\n\r");
                           2010 ;	genIpush
   0591 74 F2              2011 	mov	a,#__str_22
   0593 C0 E0              2012 	push	acc
   0595 74 20              2013 	mov	a,#(__str_22 >> 8)
   0597 C0 E0              2014 	push	acc
                           2015 ;	genCall
   0599 12 12 E5           2016 	lcall	_printf_tiny
   059C 15 81              2017 	dec	sp
   059E 15 81              2018 	dec	sp
                           2019 ;	main.c:238: printf_tiny(" Enter Capital alphabets to fill buffer0\n\r");
                           2020 ;	genIpush
   05A0 74 1C              2021 	mov	a,#__str_23
   05A2 C0 E0              2022 	push	acc
   05A4 74 21              2023 	mov	a,#(__str_23 >> 8)
   05A6 C0 E0              2024 	push	acc
                           2025 ;	genCall
   05A8 12 12 E5           2026 	lcall	_printf_tiny
   05AB 15 81              2027 	dec	sp
   05AD 15 81              2028 	dec	sp
                           2029 ;	main.c:239: printf_tiny(" Enter Numerical values to fill buffer1\n\r");
                           2030 ;	genIpush
   05AF 74 47              2031 	mov	a,#__str_24
   05B1 C0 E0              2032 	push	acc
   05B3 74 21              2033 	mov	a,#(__str_24 >> 8)
   05B5 C0 E0              2034 	push	acc
                           2035 ;	genCall
   05B7 12 12 E5           2036 	lcall	_printf_tiny
   05BA 15 81              2037 	dec	sp
   05BC 15 81              2038 	dec	sp
                           2039 ;	main.c:240: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HELP MENU~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           2040 ;	genIpush
   05BE 74 ED              2041 	mov	a,#__str_17
   05C0 C0 E0              2042 	push	acc
   05C2 74 1F              2043 	mov	a,#(__str_17 >> 8)
   05C4 C0 E0              2044 	push	acc
                           2045 ;	genCall
   05C6 12 12 E5           2046 	lcall	_printf_tiny
   05C9 15 81              2047 	dec	sp
   05CB 15 81              2048 	dec	sp
                           2049 ;	main.c:241: printf_tiny("\n\r\n\r Enter the option or Character\n\r");
                           2050 ;	genIpush
   05CD 74 71              2051 	mov	a,#__str_25
   05CF C0 E0              2052 	push	acc
   05D1 74 21              2053 	mov	a,#(__str_25 >> 8)
   05D3 C0 E0              2054 	push	acc
                           2055 ;	genCall
   05D5 12 12 E5           2056 	lcall	_printf_tiny
   05D8 15 81              2057 	dec	sp
   05DA 15 81              2058 	dec	sp
                           2059 ;	Peephole 300	removed redundant label 00101$
   05DC 22                 2060 	ret
                           2061 ;------------------------------------------------------------
                           2062 ;Allocation info for local variables in function 'deletebuffer'
                           2063 ;------------------------------------------------------------
                           2064 ;------------------------------------------------------------
                           2065 ;	main.c:246: void deletebuffer()
                           2066 ;	-----------------------------------------
                           2067 ;	 function deletebuffer
                           2068 ;	-----------------------------------------
   05DD                    2069 _deletebuffer:
                           2070 ;	main.c:248: putstr("\n\r");
                           2071 ;	genCall
                           2072 ;	Peephole 182.a	used 16 bit load of DPTR
   05DD 90 1E 13           2073 	mov	dptr,#__str_3
   05E0 75 F0 80           2074 	mov	b,#0x80
   05E3 12 0F 16           2075 	lcall	_putstr
                           2076 ;	main.c:249: putstr("\n\rEnter the buffer number which has to be deleted\n\r");
                           2077 ;	genCall
                           2078 ;	Peephole 182.a	used 16 bit load of DPTR
   05E6 90 21 96           2079 	mov	dptr,#__str_26
   05E9 75 F0 80           2080 	mov	b,#0x80
   05EC 12 0F 16           2081 	lcall	_putstr
                           2082 ;	main.c:250: res=ReadValue();
                           2083 ;	genCall
   05EF 12 03 75           2084 	lcall	_ReadValue
   05F2 AA 82              2085 	mov	r2,dpl
   05F4 AB 83              2086 	mov	r3,dph
                           2087 ;	genAssign
   05F6 90 0F D9           2088 	mov	dptr,#_res
   05F9 EA                 2089 	mov	a,r2
   05FA F0                 2090 	movx	@dptr,a
   05FB A3                 2091 	inc	dptr
   05FC EB                 2092 	mov	a,r3
   05FD F0                 2093 	movx	@dptr,a
                           2094 ;	main.c:251: if(res>buffer_count || res==1 || res==0)    /* check if buffer number to be deleted is proper*/
                           2095 ;	genAssign
   05FE 8A 04              2096 	mov	ar4,r2
   0600 8B 05              2097 	mov	ar5,r3
                           2098 ;	genAssign
   0602 90 0B B8           2099 	mov	dptr,#_buffer_count
   0605 E0                 2100 	movx	a,@dptr
   0606 FE                 2101 	mov	r6,a
   0607 A3                 2102 	inc	dptr
   0608 E0                 2103 	movx	a,@dptr
   0609 FF                 2104 	mov	r7,a
                           2105 ;	genCmpGt
                           2106 ;	genCmp
   060A C3                 2107 	clr	c
   060B EE                 2108 	mov	a,r6
   060C 9C                 2109 	subb	a,r4
   060D EF                 2110 	mov	a,r7
   060E 9D                 2111 	subb	a,r5
                           2112 ;	genIfxJump
                           2113 ;	Peephole 112.b	changed ljmp to sjmp
                           2114 ;	Peephole 160.a	removed sjmp by inverse jump logic
   060F 40 0C              2115 	jc	00104$
                           2116 ;	Peephole 300	removed redundant label 00114$
                           2117 ;	genCmpEq
                           2118 ;	gencjneshort
   0611 BA 01 05           2119 	cjne	r2,#0x01,00115$
   0614 BB 00 02           2120 	cjne	r3,#0x00,00115$
                           2121 ;	Peephole 112.b	changed ljmp to sjmp
   0617 80 04              2122 	sjmp	00104$
   0619                    2123 00115$:
                           2124 ;	genIfx
   0619 EA                 2125 	mov	a,r2
   061A 4B                 2126 	orl	a,r3
                           2127 ;	genIfxJump
                           2128 ;	Peephole 108.b	removed ljmp by inverse jump logic
   061B 70 09              2129 	jnz	00105$
                           2130 ;	Peephole 300	removed redundant label 00116$
   061D                    2131 00104$:
                           2132 ;	main.c:252: putstr("Enter valid buffer number next time\n\r");
                           2133 ;	genCall
                           2134 ;	Peephole 182.a	used 16 bit load of DPTR
   061D 90 21 CA           2135 	mov	dptr,#__str_27
   0620 75 F0 80           2136 	mov	b,#0x80
                           2137 ;	Peephole 112.b	changed ljmp to sjmp
                           2138 ;	Peephole 251.b	replaced sjmp to ret with ret
                           2139 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0623 02 0F 16           2140 	ljmp	_putstr
   0626                    2141 00105$:
                           2142 ;	main.c:253: else if (!buffer[res])                      /* check if buffer exists */
                           2143 ;	genLeftShift
                           2144 ;	genLeftShiftLiteral
                           2145 ;	genlshTwo
   0626 EB                 2146 	mov	a,r3
   0627 CA                 2147 	xch	a,r2
   0628 25 E0              2148 	add	a,acc
   062A CA                 2149 	xch	a,r2
   062B 33                 2150 	rlc	a
   062C FB                 2151 	mov	r3,a
                           2152 ;	genPlus
                           2153 ;	Peephole 236.g	used r2 instead of ar2
   062D EA                 2154 	mov	a,r2
   062E 24 C2              2155 	add	a,#_buffer
   0630 F5 82              2156 	mov	dpl,a
                           2157 ;	Peephole 236.g	used r3 instead of ar3
   0632 EB                 2158 	mov	a,r3
   0633 34 0B              2159 	addc	a,#(_buffer >> 8)
   0635 F5 83              2160 	mov	dph,a
                           2161 ;	genPointerGet
                           2162 ;	genFarPointerGet
   0637 E0                 2163 	movx	a,@dptr
   0638 FA                 2164 	mov	r2,a
   0639 A3                 2165 	inc	dptr
   063A E0                 2166 	movx	a,@dptr
                           2167 ;	genIfx
   063B FB                 2168 	mov	r3,a
                           2169 ;	Peephole 135	removed redundant mov
   063C 4A                 2170 	orl	a,r2
                           2171 ;	genIfxJump
                           2172 ;	Peephole 108.b	removed ljmp by inverse jump logic
   063D 70 09              2173 	jnz	00102$
                           2174 ;	Peephole 300	removed redundant label 00117$
                           2175 ;	main.c:254: putstr("Enter valid buffer number next time\n\r");
                           2176 ;	genCall
                           2177 ;	Peephole 182.a	used 16 bit load of DPTR
   063F 90 21 CA           2178 	mov	dptr,#__str_27
   0642 75 F0 80           2179 	mov	b,#0x80
                           2180 ;	Peephole 112.b	changed ljmp to sjmp
                           2181 ;	Peephole 251.b	replaced sjmp to ret with ret
                           2182 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0645 02 0F 16           2183 	ljmp	_putstr
   0648                    2184 00102$:
                           2185 ;	main.c:257: free (buffer[res]);             /* free buffer */
                           2186 ;	genAssign
                           2187 ;	genCast
   0648 7C 00              2188 	mov	r4,#0x0
                           2189 ;	genCall
   064A 8A 82              2190 	mov	dpl,r2
   064C 8B 83              2191 	mov	dph,r3
   064E 8C F0              2192 	mov	b,r4
   0650 12 10 21           2193 	lcall	_free
                           2194 ;	main.c:258: buffer[res]=0;
                           2195 ;	genAssign
   0653 90 0F D9           2196 	mov	dptr,#_res
   0656 E0                 2197 	movx	a,@dptr
   0657 FA                 2198 	mov	r2,a
   0658 A3                 2199 	inc	dptr
   0659 E0                 2200 	movx	a,@dptr
                           2201 ;	genLeftShift
                           2202 ;	genLeftShiftLiteral
                           2203 ;	genlshTwo
   065A FB                 2204 	mov	r3,a
                           2205 ;	Peephole 105	removed redundant mov
   065B CA                 2206 	xch	a,r2
   065C 25 E0              2207 	add	a,acc
   065E CA                 2208 	xch	a,r2
   065F 33                 2209 	rlc	a
   0660 FB                 2210 	mov	r3,a
                           2211 ;	genPlus
                           2212 ;	Peephole 236.g	used r2 instead of ar2
   0661 EA                 2213 	mov	a,r2
   0662 24 C2              2214 	add	a,#_buffer
   0664 F5 82              2215 	mov	dpl,a
                           2216 ;	Peephole 236.g	used r3 instead of ar3
   0666 EB                 2217 	mov	a,r3
   0667 34 0B              2218 	addc	a,#(_buffer >> 8)
   0669 F5 83              2219 	mov	dph,a
                           2220 ;	genPointerSet
                           2221 ;     genFarPointerSet
                           2222 ;	Peephole 181	changed mov to clr
   066B E4                 2223 	clr	a
   066C F0                 2224 	movx	@dptr,a
   066D A3                 2225 	inc	dptr
                           2226 ;	Peephole 101	removed redundant mov
   066E F0                 2227 	movx	@dptr,a
                           2228 ;	main.c:259: buffer_number--;                /* decrement the number of buffers that are active */
                           2229 ;	genAssign
   066F 90 0B BE           2230 	mov	dptr,#_buffer_number
   0672 E0                 2231 	movx	a,@dptr
   0673 FA                 2232 	mov	r2,a
   0674 A3                 2233 	inc	dptr
   0675 E0                 2234 	movx	a,@dptr
   0676 FB                 2235 	mov	r3,a
                           2236 ;	genMinus
                           2237 ;	genMinusDec
   0677 1A                 2238 	dec	r2
   0678 BA FF 01           2239 	cjne	r2,#0xff,00118$
   067B 1B                 2240 	dec	r3
   067C                    2241 00118$:
                           2242 ;	genAssign
   067C 90 0B BE           2243 	mov	dptr,#_buffer_number
   067F EA                 2244 	mov	a,r2
   0680 F0                 2245 	movx	@dptr,a
   0681 A3                 2246 	inc	dptr
   0682 EB                 2247 	mov	a,r3
   0683 F0                 2248 	movx	@dptr,a
                           2249 ;	main.c:260: putstr("Buffer deleted\n\r");
                           2250 ;	genCall
                           2251 ;	Peephole 182.a	used 16 bit load of DPTR
   0684 90 21 F0           2252 	mov	dptr,#__str_28
   0687 75 F0 80           2253 	mov	b,#0x80
                           2254 ;	Peephole 253.b	replaced lcall/ret with ljmp
   068A 02 0F 16           2255 	ljmp	_putstr
                           2256 ;
                           2257 ;------------------------------------------------------------
                           2258 ;Allocation info for local variables in function 'fillbuffer0'
                           2259 ;------------------------------------------------------------
                           2260 ;buff_val                  Allocated with name '_fillbuffer0_buff_val_1_1'
                           2261 ;sloc0                     Allocated with name '_fillbuffer0_sloc0_1_0'
                           2262 ;sloc1                     Allocated with name '_fillbuffer0_sloc1_1_0'
                           2263 ;sloc2                     Allocated with name '_fillbuffer0_sloc2_1_0'
                           2264 ;------------------------------------------------------------
                           2265 ;	main.c:266: void fillbuffer0(unsigned char buff_val)
                           2266 ;	-----------------------------------------
                           2267 ;	 function fillbuffer0
                           2268 ;	-----------------------------------------
   068D                    2269 _fillbuffer0:
                           2270 ;	genReceive
   068D E5 82              2271 	mov	a,dpl
   068F 90 0F DB           2272 	mov	dptr,#_fillbuffer0_buff_val_1_1
   0692 F0                 2273 	movx	@dptr,a
                           2274 ;	main.c:268: if((unsigned int)bptr0<((unsigned int)buff1ptr+(int)bufflen[0]-1)) /*check if buffer is full */
                           2275 ;	genAssign
   0693 90 0D C8           2276 	mov	dptr,#_bptr0
   0696 E0                 2277 	movx	a,@dptr
   0697 FA                 2278 	mov	r2,a
   0698 A3                 2279 	inc	dptr
   0699 E0                 2280 	movx	a,@dptr
   069A FB                 2281 	mov	r3,a
   069B A3                 2282 	inc	dptr
   069C E0                 2283 	movx	a,@dptr
   069D FC                 2284 	mov	r4,a
                           2285 ;	genCast
   069E 8A 07              2286 	mov	ar7,r2
   06A0 8B 00              2287 	mov	ar0,r3
                           2288 ;	genAssign
   06A2 90 0D C2           2289 	mov	dptr,#_buff1ptr
   06A5 E0                 2290 	movx	a,@dptr
   06A6 F5 14              2291 	mov	_fillbuffer0_sloc2_1_0,a
   06A8 A3                 2292 	inc	dptr
   06A9 E0                 2293 	movx	a,@dptr
   06AA F5 15              2294 	mov	(_fillbuffer0_sloc2_1_0 + 1),a
   06AC A3                 2295 	inc	dptr
   06AD E0                 2296 	movx	a,@dptr
   06AE F5 16              2297 	mov	(_fillbuffer0_sloc2_1_0 + 2),a
                           2298 ;	genCast
   06B0 AD 14              2299 	mov	r5,_fillbuffer0_sloc2_1_0
   06B2 AE 15              2300 	mov	r6,(_fillbuffer0_sloc2_1_0 + 1)
                           2301 ;	genPointerGet
                           2302 ;	genFarPointerGet
   06B4 90 0D CE           2303 	mov	dptr,#_bufflen
   06B7 E0                 2304 	movx	a,@dptr
   06B8 F5 12              2305 	mov	_fillbuffer0_sloc1_1_0,a
   06BA A3                 2306 	inc	dptr
   06BB E0                 2307 	movx	a,@dptr
   06BC F5 13              2308 	mov	(_fillbuffer0_sloc1_1_0 + 1),a
                           2309 ;	genPlus
   06BE E5 12              2310 	mov	a,_fillbuffer0_sloc1_1_0
                           2311 ;	Peephole 236.a	used r5 instead of ar5
   06C0 2D                 2312 	add	a,r5
   06C1 FD                 2313 	mov	r5,a
   06C2 E5 13              2314 	mov	a,(_fillbuffer0_sloc1_1_0 + 1)
                           2315 ;	Peephole 236.b	used r6 instead of ar6
   06C4 3E                 2316 	addc	a,r6
   06C5 FE                 2317 	mov	r6,a
                           2318 ;	genMinus
                           2319 ;	genMinusDec
   06C6 1D                 2320 	dec	r5
   06C7 BD FF 01           2321 	cjne	r5,#0xff,00110$
   06CA 1E                 2322 	dec	r6
   06CB                    2323 00110$:
                           2324 ;	genCmpLt
                           2325 ;	genCmp
   06CB C3                 2326 	clr	c
   06CC EF                 2327 	mov	a,r7
   06CD 9D                 2328 	subb	a,r5
   06CE E8                 2329 	mov	a,r0
   06CF 9E                 2330 	subb	a,r6
                           2331 ;	genIfxJump
                           2332 ;	Peephole 108.a	removed ljmp by inverse jump logic
   06D0 50 1D              2333 	jnc	00104$
                           2334 ;	Peephole 300	removed redundant label 00111$
                           2335 ;	main.c:270: *bptr0 = buff_val;
                           2336 ;	genAssign
   06D2 90 0F DB           2337 	mov	dptr,#_fillbuffer0_buff_val_1_1
   06D5 E0                 2338 	movx	a,@dptr
                           2339 ;	genPointerSet
                           2340 ;	genGenPointerSet
   06D6 FD                 2341 	mov	r5,a
   06D7 8A 82              2342 	mov	dpl,r2
   06D9 8B 83              2343 	mov	dph,r3
   06DB 8C F0              2344 	mov	b,r4
                           2345 ;	Peephole 191	removed redundant mov
   06DD 12 12 CC           2346 	lcall	__gptrput
                           2347 ;	main.c:271: bptr0++;
                           2348 ;	genPlus
   06E0 90 0D C8           2349 	mov	dptr,#_bptr0
                           2350 ;     genPlusIncr
   06E3 74 01              2351 	mov	a,#0x01
                           2352 ;	Peephole 236.a	used r2 instead of ar2
   06E5 2A                 2353 	add	a,r2
   06E6 F0                 2354 	movx	@dptr,a
                           2355 ;	Peephole 181	changed mov to clr
   06E7 E4                 2356 	clr	a
                           2357 ;	Peephole 236.b	used r3 instead of ar3
   06E8 3B                 2358 	addc	a,r3
   06E9 A3                 2359 	inc	dptr
   06EA F0                 2360 	movx	@dptr,a
   06EB A3                 2361 	inc	dptr
   06EC EC                 2362 	mov	a,r4
   06ED F0                 2363 	movx	@dptr,a
                           2364 ;	Peephole 112.b	changed ljmp to sjmp
                           2365 ;	Peephole 251.b	replaced sjmp to ret with ret
   06EE 22                 2366 	ret
   06EF                    2367 00104$:
                           2368 ;	main.c:273: else if((unsigned int)bptr0==((unsigned int)buff1ptr+(int)bufflen[0]-1))
                           2369 ;	genAssign
   06EF 8A 05              2370 	mov	ar5,r2
   06F1 8B 06              2371 	mov	ar6,r3
                           2372 ;	genCast
                           2373 ;	genAssign
                           2374 ;	peephole 177.f	removed redundant move
   06F3 AF 14              2375 	mov	r7,_fillbuffer0_sloc2_1_0
   06F5 A8 15              2376 	mov	r0,(_fillbuffer0_sloc2_1_0 + 1)
   06F7 A9 16              2377 	mov	r1,(_fillbuffer0_sloc2_1_0 + 2)
                           2378 ;	genCast
                           2379 ;	genPlus
   06F9 E5 12              2380 	mov	a,_fillbuffer0_sloc1_1_0
                           2381 ;	Peephole 236.a	used r7 instead of ar7
   06FB 2F                 2382 	add	a,r7
   06FC FF                 2383 	mov	r7,a
   06FD E5 13              2384 	mov	a,(_fillbuffer0_sloc1_1_0 + 1)
                           2385 ;	Peephole 236.b	used r0 instead of ar0
   06FF 38                 2386 	addc	a,r0
   0700 F8                 2387 	mov	r0,a
                           2388 ;	genMinus
                           2389 ;	genMinusDec
   0701 1F                 2390 	dec	r7
   0702 BF FF 01           2391 	cjne	r7,#0xff,00112$
   0705 18                 2392 	dec	r0
   0706                    2393 00112$:
                           2394 ;	genCmpEq
                           2395 ;	gencjneshort
   0706 ED                 2396 	mov	a,r5
                           2397 ;	Peephole 112.b	changed ljmp to sjmp
                           2398 ;	Peephole 197.b	optimized misc jump sequence
   0707 B5 07 35           2399 	cjne	a,ar7,00106$
   070A EE                 2400 	mov	a,r6
   070B B5 00 31           2401 	cjne	a,ar0,00106$
                           2402 ;	Peephole 200.b	removed redundant sjmp
                           2403 ;	Peephole 300	removed redundant label 00113$
                           2404 ;	Peephole 300	removed redundant label 00114$
                           2405 ;	main.c:275: *bptr0=buff_val;
                           2406 ;	genAssign
   070E 90 0F DB           2407 	mov	dptr,#_fillbuffer0_buff_val_1_1
   0711 E0                 2408 	movx	a,@dptr
                           2409 ;	genPointerSet
                           2410 ;	genGenPointerSet
   0712 FD                 2411 	mov	r5,a
   0713 8A 82              2412 	mov	dpl,r2
   0715 8B 83              2413 	mov	dph,r3
   0717 8C F0              2414 	mov	b,r4
                           2415 ;	Peephole 191	removed redundant mov
   0719 12 12 CC           2416 	lcall	__gptrput
                           2417 ;	main.c:276: bptr0++;
                           2418 ;	genPlus
   071C 90 0D C8           2419 	mov	dptr,#_bptr0
                           2420 ;     genPlusIncr
   071F 74 01              2421 	mov	a,#0x01
                           2422 ;	Peephole 236.a	used r2 instead of ar2
   0721 2A                 2423 	add	a,r2
   0722 F0                 2424 	movx	@dptr,a
                           2425 ;	Peephole 181	changed mov to clr
   0723 E4                 2426 	clr	a
                           2427 ;	Peephole 236.b	used r3 instead of ar3
   0724 3B                 2428 	addc	a,r3
   0725 A3                 2429 	inc	dptr
   0726 F0                 2430 	movx	@dptr,a
   0727 A3                 2431 	inc	dptr
   0728 EC                 2432 	mov	a,r4
   0729 F0                 2433 	movx	@dptr,a
                           2434 ;	main.c:277: *bptr0='\0';
                           2435 ;	genAssign
   072A 90 0D C8           2436 	mov	dptr,#_bptr0
   072D E0                 2437 	movx	a,@dptr
   072E FA                 2438 	mov	r2,a
   072F A3                 2439 	inc	dptr
   0730 E0                 2440 	movx	a,@dptr
   0731 FB                 2441 	mov	r3,a
   0732 A3                 2442 	inc	dptr
   0733 E0                 2443 	movx	a,@dptr
   0734 FC                 2444 	mov	r4,a
                           2445 ;	genPointerSet
                           2446 ;	genGenPointerSet
   0735 8A 82              2447 	mov	dpl,r2
   0737 8B 83              2448 	mov	dph,r3
   0739 8C F0              2449 	mov	b,r4
                           2450 ;	Peephole 181	changed mov to clr
   073B E4                 2451 	clr	a
                           2452 ;	Peephole 253.c	replaced lcall with ljmp
   073C 02 12 CC           2453 	ljmp	__gptrput
   073F                    2454 00106$:
   073F 22                 2455 	ret
                           2456 ;------------------------------------------------------------
                           2457 ;Allocation info for local variables in function 'fillbuffer1'
                           2458 ;------------------------------------------------------------
                           2459 ;buff_val                  Allocated with name '_fillbuffer1_buff_val_1_1'
                           2460 ;sloc0                     Allocated with name '_fillbuffer1_sloc0_1_0'
                           2461 ;sloc1                     Allocated with name '_fillbuffer1_sloc1_1_0'
                           2462 ;sloc2                     Allocated with name '_fillbuffer1_sloc2_1_0'
                           2463 ;------------------------------------------------------------
                           2464 ;	main.c:281: void fillbuffer1(unsigned char buff_val)
                           2465 ;	-----------------------------------------
                           2466 ;	 function fillbuffer1
                           2467 ;	-----------------------------------------
   0740                    2468 _fillbuffer1:
                           2469 ;	genReceive
   0740 E5 82              2470 	mov	a,dpl
   0742 90 0F DC           2471 	mov	dptr,#_fillbuffer1_buff_val_1_1
   0745 F0                 2472 	movx	@dptr,a
                           2473 ;	main.c:283: if((unsigned int)bptr1<((unsigned int)buff2ptr+(int)bufflen[1]-1))      /*checking if buffer is full */
                           2474 ;	genAssign
   0746 90 0D CB           2475 	mov	dptr,#_bptr1
   0749 E0                 2476 	movx	a,@dptr
   074A FA                 2477 	mov	r2,a
   074B A3                 2478 	inc	dptr
   074C E0                 2479 	movx	a,@dptr
   074D FB                 2480 	mov	r3,a
   074E A3                 2481 	inc	dptr
   074F E0                 2482 	movx	a,@dptr
   0750 FC                 2483 	mov	r4,a
                           2484 ;	genCast
   0751 8A 07              2485 	mov	ar7,r2
   0753 8B 00              2486 	mov	ar0,r3
                           2487 ;	genAssign
   0755 90 0D C5           2488 	mov	dptr,#_buff2ptr
   0758 E0                 2489 	movx	a,@dptr
   0759 F5 14              2490 	mov	_fillbuffer1_sloc2_1_0,a
   075B A3                 2491 	inc	dptr
   075C E0                 2492 	movx	a,@dptr
   075D F5 15              2493 	mov	(_fillbuffer1_sloc2_1_0 + 1),a
   075F A3                 2494 	inc	dptr
   0760 E0                 2495 	movx	a,@dptr
   0761 F5 16              2496 	mov	(_fillbuffer1_sloc2_1_0 + 2),a
                           2497 ;	genCast
   0763 AD 14              2498 	mov	r5,_fillbuffer1_sloc2_1_0
   0765 AE 15              2499 	mov	r6,(_fillbuffer1_sloc2_1_0 + 1)
                           2500 ;	genPointerGet
                           2501 ;	genFarPointerGet
   0767 90 0D D0           2502 	mov	dptr,#(_bufflen + 0x0002)
   076A E0                 2503 	movx	a,@dptr
   076B F5 12              2504 	mov	_fillbuffer1_sloc1_1_0,a
   076D A3                 2505 	inc	dptr
   076E E0                 2506 	movx	a,@dptr
   076F F5 13              2507 	mov	(_fillbuffer1_sloc1_1_0 + 1),a
                           2508 ;	genPlus
   0771 E5 12              2509 	mov	a,_fillbuffer1_sloc1_1_0
                           2510 ;	Peephole 236.a	used r5 instead of ar5
   0773 2D                 2511 	add	a,r5
   0774 FD                 2512 	mov	r5,a
   0775 E5 13              2513 	mov	a,(_fillbuffer1_sloc1_1_0 + 1)
                           2514 ;	Peephole 236.b	used r6 instead of ar6
   0777 3E                 2515 	addc	a,r6
   0778 FE                 2516 	mov	r6,a
                           2517 ;	genMinus
                           2518 ;	genMinusDec
   0779 1D                 2519 	dec	r5
   077A BD FF 01           2520 	cjne	r5,#0xff,00110$
   077D 1E                 2521 	dec	r6
   077E                    2522 00110$:
                           2523 ;	genCmpLt
                           2524 ;	genCmp
   077E C3                 2525 	clr	c
   077F EF                 2526 	mov	a,r7
   0780 9D                 2527 	subb	a,r5
   0781 E8                 2528 	mov	a,r0
   0782 9E                 2529 	subb	a,r6
                           2530 ;	genIfxJump
                           2531 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0783 50 1D              2532 	jnc	00104$
                           2533 ;	Peephole 300	removed redundant label 00111$
                           2534 ;	main.c:285: *bptr1 = buff_val;
                           2535 ;	genAssign
   0785 90 0F DC           2536 	mov	dptr,#_fillbuffer1_buff_val_1_1
   0788 E0                 2537 	movx	a,@dptr
                           2538 ;	genPointerSet
                           2539 ;	genGenPointerSet
   0789 FD                 2540 	mov	r5,a
   078A 8A 82              2541 	mov	dpl,r2
   078C 8B 83              2542 	mov	dph,r3
   078E 8C F0              2543 	mov	b,r4
                           2544 ;	Peephole 191	removed redundant mov
   0790 12 12 CC           2545 	lcall	__gptrput
                           2546 ;	main.c:286: bptr1++;
                           2547 ;	genPlus
   0793 90 0D CB           2548 	mov	dptr,#_bptr1
                           2549 ;     genPlusIncr
   0796 74 01              2550 	mov	a,#0x01
                           2551 ;	Peephole 236.a	used r2 instead of ar2
   0798 2A                 2552 	add	a,r2
   0799 F0                 2553 	movx	@dptr,a
                           2554 ;	Peephole 181	changed mov to clr
   079A E4                 2555 	clr	a
                           2556 ;	Peephole 236.b	used r3 instead of ar3
   079B 3B                 2557 	addc	a,r3
   079C A3                 2558 	inc	dptr
   079D F0                 2559 	movx	@dptr,a
   079E A3                 2560 	inc	dptr
   079F EC                 2561 	mov	a,r4
   07A0 F0                 2562 	movx	@dptr,a
                           2563 ;	Peephole 112.b	changed ljmp to sjmp
                           2564 ;	Peephole 251.b	replaced sjmp to ret with ret
   07A1 22                 2565 	ret
   07A2                    2566 00104$:
                           2567 ;	main.c:288: else if((unsigned int)bptr1==((unsigned int)buff2ptr+(int)bufflen[1]-1))
                           2568 ;	genAssign
   07A2 8A 05              2569 	mov	ar5,r2
   07A4 8B 06              2570 	mov	ar6,r3
                           2571 ;	genCast
                           2572 ;	genAssign
                           2573 ;	peephole 177.f	removed redundant move
   07A6 AF 14              2574 	mov	r7,_fillbuffer1_sloc2_1_0
   07A8 A8 15              2575 	mov	r0,(_fillbuffer1_sloc2_1_0 + 1)
   07AA A9 16              2576 	mov	r1,(_fillbuffer1_sloc2_1_0 + 2)
                           2577 ;	genCast
                           2578 ;	genPlus
   07AC E5 12              2579 	mov	a,_fillbuffer1_sloc1_1_0
                           2580 ;	Peephole 236.a	used r7 instead of ar7
   07AE 2F                 2581 	add	a,r7
   07AF FF                 2582 	mov	r7,a
   07B0 E5 13              2583 	mov	a,(_fillbuffer1_sloc1_1_0 + 1)
                           2584 ;	Peephole 236.b	used r0 instead of ar0
   07B2 38                 2585 	addc	a,r0
   07B3 F8                 2586 	mov	r0,a
                           2587 ;	genMinus
                           2588 ;	genMinusDec
   07B4 1F                 2589 	dec	r7
   07B5 BF FF 01           2590 	cjne	r7,#0xff,00112$
   07B8 18                 2591 	dec	r0
   07B9                    2592 00112$:
                           2593 ;	genCmpEq
                           2594 ;	gencjneshort
   07B9 ED                 2595 	mov	a,r5
                           2596 ;	Peephole 112.b	changed ljmp to sjmp
                           2597 ;	Peephole 197.b	optimized misc jump sequence
   07BA B5 07 35           2598 	cjne	a,ar7,00106$
   07BD EE                 2599 	mov	a,r6
   07BE B5 00 31           2600 	cjne	a,ar0,00106$
                           2601 ;	Peephole 200.b	removed redundant sjmp
                           2602 ;	Peephole 300	removed redundant label 00113$
                           2603 ;	Peephole 300	removed redundant label 00114$
                           2604 ;	main.c:290: *bptr1=buff_val;
                           2605 ;	genAssign
   07C1 90 0F DC           2606 	mov	dptr,#_fillbuffer1_buff_val_1_1
   07C4 E0                 2607 	movx	a,@dptr
                           2608 ;	genPointerSet
                           2609 ;	genGenPointerSet
   07C5 FD                 2610 	mov	r5,a
   07C6 8A 82              2611 	mov	dpl,r2
   07C8 8B 83              2612 	mov	dph,r3
   07CA 8C F0              2613 	mov	b,r4
                           2614 ;	Peephole 191	removed redundant mov
   07CC 12 12 CC           2615 	lcall	__gptrput
                           2616 ;	main.c:291: bptr1++;
                           2617 ;	genPlus
   07CF 90 0D CB           2618 	mov	dptr,#_bptr1
                           2619 ;     genPlusIncr
   07D2 74 01              2620 	mov	a,#0x01
                           2621 ;	Peephole 236.a	used r2 instead of ar2
   07D4 2A                 2622 	add	a,r2
   07D5 F0                 2623 	movx	@dptr,a
                           2624 ;	Peephole 181	changed mov to clr
   07D6 E4                 2625 	clr	a
                           2626 ;	Peephole 236.b	used r3 instead of ar3
   07D7 3B                 2627 	addc	a,r3
   07D8 A3                 2628 	inc	dptr
   07D9 F0                 2629 	movx	@dptr,a
   07DA A3                 2630 	inc	dptr
   07DB EC                 2631 	mov	a,r4
   07DC F0                 2632 	movx	@dptr,a
                           2633 ;	main.c:292: *bptr1='\0';
                           2634 ;	genAssign
   07DD 90 0D CB           2635 	mov	dptr,#_bptr1
   07E0 E0                 2636 	movx	a,@dptr
   07E1 FA                 2637 	mov	r2,a
   07E2 A3                 2638 	inc	dptr
   07E3 E0                 2639 	movx	a,@dptr
   07E4 FB                 2640 	mov	r3,a
   07E5 A3                 2641 	inc	dptr
   07E6 E0                 2642 	movx	a,@dptr
   07E7 FC                 2643 	mov	r4,a
                           2644 ;	genPointerSet
                           2645 ;	genGenPointerSet
   07E8 8A 82              2646 	mov	dpl,r2
   07EA 8B 83              2647 	mov	dph,r3
   07EC 8C F0              2648 	mov	b,r4
                           2649 ;	Peephole 181	changed mov to clr
   07EE E4                 2650 	clr	a
                           2651 ;	Peephole 253.c	replaced lcall with ljmp
   07EF 02 12 CC           2652 	ljmp	__gptrput
   07F2                    2653 00106$:
   07F2 22                 2654 	ret
                           2655 ;------------------------------------------------------------
                           2656 ;Allocation info for local variables in function 'freeEverything'
                           2657 ;------------------------------------------------------------
                           2658 ;counter                   Allocated with name '_freeEverything_counter_1_1'
                           2659 ;------------------------------------------------------------
                           2660 ;	main.c:298: void freeEverything()
                           2661 ;	-----------------------------------------
                           2662 ;	 function freeEverything
                           2663 ;	-----------------------------------------
   07F3                    2664 _freeEverything:
                           2665 ;	main.c:301: for (counter=0; counter<buffer_count; counter++)
                           2666 ;	genAssign
   07F3 7A 00              2667 	mov	r2,#0x00
   07F5 7B 00              2668 	mov	r3,#0x00
   07F7                    2669 00103$:
                           2670 ;	genAssign
   07F7 90 0B B8           2671 	mov	dptr,#_buffer_count
   07FA E0                 2672 	movx	a,@dptr
   07FB FC                 2673 	mov	r4,a
   07FC A3                 2674 	inc	dptr
   07FD E0                 2675 	movx	a,@dptr
   07FE FD                 2676 	mov	r5,a
                           2677 ;	genCmpLt
                           2678 ;	genCmp
   07FF C3                 2679 	clr	c
   0800 EA                 2680 	mov	a,r2
   0801 9C                 2681 	subb	a,r4
   0802 EB                 2682 	mov	a,r3
   0803 9D                 2683 	subb	a,r5
                           2684 ;	genIfxJump
                           2685 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0804 50 52              2686 	jnc	00107$
                           2687 ;	Peephole 300	removed redundant label 00113$
                           2688 ;	main.c:303: if(buffer[counter])
                           2689 ;	genLeftShift
                           2690 ;	genLeftShiftLiteral
                           2691 ;	genlshTwo
   0806 8A 04              2692 	mov	ar4,r2
   0808 EB                 2693 	mov	a,r3
   0809 CC                 2694 	xch	a,r4
   080A 25 E0              2695 	add	a,acc
   080C CC                 2696 	xch	a,r4
   080D 33                 2697 	rlc	a
   080E FD                 2698 	mov	r5,a
                           2699 ;	genPlus
                           2700 ;	Peephole 236.g	used r4 instead of ar4
   080F EC                 2701 	mov	a,r4
   0810 24 C2              2702 	add	a,#_buffer
   0812 F5 82              2703 	mov	dpl,a
                           2704 ;	Peephole 236.g	used r5 instead of ar5
   0814 ED                 2705 	mov	a,r5
   0815 34 0B              2706 	addc	a,#(_buffer >> 8)
   0817 F5 83              2707 	mov	dph,a
                           2708 ;	genPointerGet
                           2709 ;	genFarPointerGet
   0819 E0                 2710 	movx	a,@dptr
   081A FC                 2711 	mov	r4,a
   081B A3                 2712 	inc	dptr
   081C E0                 2713 	movx	a,@dptr
                           2714 ;	genIfx
   081D FD                 2715 	mov	r5,a
                           2716 ;	Peephole 135	removed redundant mov
   081E 4C                 2717 	orl	a,r4
                           2718 ;	genIfxJump
                           2719 ;	Peephole 108.c	removed ljmp by inverse jump logic
   081F 60 30              2720 	jz	00105$
                           2721 ;	Peephole 300	removed redundant label 00114$
                           2722 ;	main.c:305: free (buffer[counter]);                 /*Delete Every buffer */
                           2723 ;	genAssign
                           2724 ;	genCast
   0821 7E 00              2725 	mov	r6,#0x0
                           2726 ;	genCall
   0823 8C 82              2727 	mov	dpl,r4
   0825 8D 83              2728 	mov	dph,r5
   0827 8E F0              2729 	mov	b,r6
   0829 C0 02              2730 	push	ar2
   082B C0 03              2731 	push	ar3
   082D 12 10 21           2732 	lcall	_free
   0830 D0 03              2733 	pop	ar3
   0832 D0 02              2734 	pop	ar2
                           2735 ;	main.c:306: printf_tiny("Deleted %d buffer\n\r",counter);
                           2736 ;	genIpush
   0834 C0 02              2737 	push	ar2
   0836 C0 03              2738 	push	ar3
   0838 C0 02              2739 	push	ar2
   083A C0 03              2740 	push	ar3
                           2741 ;	genIpush
   083C 74 01              2742 	mov	a,#__str_29
   083E C0 E0              2743 	push	acc
   0840 74 22              2744 	mov	a,#(__str_29 >> 8)
   0842 C0 E0              2745 	push	acc
                           2746 ;	genCall
   0844 12 12 E5           2747 	lcall	_printf_tiny
   0847 E5 81              2748 	mov	a,sp
   0849 24 FC              2749 	add	a,#0xfc
   084B F5 81              2750 	mov	sp,a
   084D D0 03              2751 	pop	ar3
   084F D0 02              2752 	pop	ar2
   0851                    2753 00105$:
                           2754 ;	main.c:301: for (counter=0; counter<buffer_count; counter++)
                           2755 ;	genPlus
                           2756 ;     genPlusIncr
   0851 0A                 2757 	inc	r2
                           2758 ;	Peephole 112.b	changed ljmp to sjmp
                           2759 ;	Peephole 243	avoided branch to sjmp
   0852 BA 00 A2           2760 	cjne	r2,#0x00,00103$
   0855 0B                 2761 	inc	r3
                           2762 ;	Peephole 300	removed redundant label 00115$
   0856 80 9F              2763 	sjmp	00103$
   0858                    2764 00107$:
   0858 22                 2765 	ret
                           2766 ;------------------------------------------------------------
                           2767 ;Allocation info for local variables in function 'displaydelete'
                           2768 ;------------------------------------------------------------
                           2769 ;len                       Allocated with name '_displaydelete_len_1_1'
                           2770 ;charnum0                  Allocated with name '_displaydelete_charnum0_1_1'
                           2771 ;charnum1                  Allocated with name '_displaydelete_charnum1_1_1'
                           2772 ;------------------------------------------------------------
                           2773 ;	main.c:312: void displaydelete()
                           2774 ;	-----------------------------------------
                           2775 ;	 function displaydelete
                           2776 ;	-----------------------------------------
   0859                    2777 _displaydelete:
                           2778 ;	main.c:314: unsigned int len=0, charnum0=0, charnum1=0 ;
                           2779 ;	genAssign
   0859 90 0F DD           2780 	mov	dptr,#_displaydelete_charnum0_1_1
   085C E4                 2781 	clr	a
   085D F0                 2782 	movx	@dptr,a
   085E A3                 2783 	inc	dptr
   085F F0                 2784 	movx	@dptr,a
                           2785 ;	genAssign
   0860 90 0F DF           2786 	mov	dptr,#_displaydelete_charnum1_1_1
   0863 E4                 2787 	clr	a
   0864 F0                 2788 	movx	@dptr,a
   0865 A3                 2789 	inc	dptr
   0866 F0                 2790 	movx	@dptr,a
                           2791 ;	main.c:315: putstr("\n\r");
                           2792 ;	genCall
                           2793 ;	Peephole 182.a	used 16 bit load of DPTR
   0867 90 1E 13           2794 	mov	dptr,#__str_3
   086A 75 F0 80           2795 	mov	b,#0x80
   086D 12 0F 16           2796 	lcall	_putstr
                           2797 ;	main.c:316: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYING DETAILS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           2798 ;	genIpush
   0870 74 15              2799 	mov	a,#__str_30
   0872 C0 E0              2800 	push	acc
   0874 74 22              2801 	mov	a,#(__str_30 >> 8)
   0876 C0 E0              2802 	push	acc
                           2803 ;	genCall
   0878 12 12 E5           2804 	lcall	_printf_tiny
   087B 15 81              2805 	dec	sp
   087D 15 81              2806 	dec	sp
                           2807 ;	main.c:317: printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           2808 ;	genIpush
   087F 74 71              2809 	mov	a,#__str_31
   0881 C0 E0              2810 	push	acc
   0883 74 22              2811 	mov	a,#(__str_31 >> 8)
   0885 C0 E0              2812 	push	acc
                           2813 ;	genCall
   0887 12 12 E5           2814 	lcall	_printf_tiny
   088A 15 81              2815 	dec	sp
   088C 15 81              2816 	dec	sp
                           2817 ;	main.c:318: printf_tiny("\n\rTotal Number of Buffer = %d\n\r", buffer_number);
                           2818 ;	genIpush
   088E 90 0B BE           2819 	mov	dptr,#_buffer_number
   0891 E0                 2820 	movx	a,@dptr
   0892 C0 E0              2821 	push	acc
   0894 A3                 2822 	inc	dptr
   0895 E0                 2823 	movx	a,@dptr
   0896 C0 E0              2824 	push	acc
                           2825 ;	genIpush
   0898 74 CB              2826 	mov	a,#__str_32
   089A C0 E0              2827 	push	acc
   089C 74 22              2828 	mov	a,#(__str_32 >> 8)
   089E C0 E0              2829 	push	acc
                           2830 ;	genCall
   08A0 12 12 E5           2831 	lcall	_printf_tiny
   08A3 E5 81              2832 	mov	a,sp
   08A5 24 FC              2833 	add	a,#0xfc
   08A7 F5 81              2834 	mov	sp,a
                           2835 ;	main.c:319: putstr("\n\r");
                           2836 ;	genCall
                           2837 ;	Peephole 182.a	used 16 bit load of DPTR
   08A9 90 1E 13           2838 	mov	dptr,#__str_3
   08AC 75 F0 80           2839 	mov	b,#0x80
   08AF 12 0F 16           2840 	lcall	_putstr
                           2841 ;	main.c:320: printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           2842 ;	genIpush
   08B2 74 71              2843 	mov	a,#__str_31
   08B4 C0 E0              2844 	push	acc
   08B6 74 22              2845 	mov	a,#(__str_31 >> 8)
   08B8 C0 E0              2846 	push	acc
                           2847 ;	genCall
   08BA 12 12 E5           2848 	lcall	_printf_tiny
   08BD 15 81              2849 	dec	sp
   08BF 15 81              2850 	dec	sp
                           2851 ;	main.c:321: printf_tiny("\n\rTotal number of characters received = %d\n\r", char_count);
                           2852 ;	genIpush
   08C1 90 0B BC           2853 	mov	dptr,#_char_count
   08C4 E0                 2854 	movx	a,@dptr
   08C5 C0 E0              2855 	push	acc
   08C7 A3                 2856 	inc	dptr
   08C8 E0                 2857 	movx	a,@dptr
   08C9 C0 E0              2858 	push	acc
                           2859 ;	genIpush
   08CB 74 EB              2860 	mov	a,#__str_33
   08CD C0 E0              2861 	push	acc
   08CF 74 22              2862 	mov	a,#(__str_33 >> 8)
   08D1 C0 E0              2863 	push	acc
                           2864 ;	genCall
   08D3 12 12 E5           2865 	lcall	_printf_tiny
   08D6 E5 81              2866 	mov	a,sp
   08D8 24 FC              2867 	add	a,#0xfc
   08DA F5 81              2868 	mov	sp,a
                           2869 ;	main.c:322: printf_tiny("Total number of storage characters received = %d\n\r", stor_count);
                           2870 ;	genIpush
   08DC 90 0B BA           2871 	mov	dptr,#_stor_count
   08DF E0                 2872 	movx	a,@dptr
   08E0 C0 E0              2873 	push	acc
   08E2 A3                 2874 	inc	dptr
   08E3 E0                 2875 	movx	a,@dptr
   08E4 C0 E0              2876 	push	acc
                           2877 ;	genIpush
   08E6 74 18              2878 	mov	a,#__str_34
   08E8 C0 E0              2879 	push	acc
   08EA 74 23              2880 	mov	a,#(__str_34 >> 8)
   08EC C0 E0              2881 	push	acc
                           2882 ;	genCall
   08EE 12 12 E5           2883 	lcall	_printf_tiny
   08F1 E5 81              2884 	mov	a,sp
   08F3 24 FC              2885 	add	a,#0xfc
   08F5 F5 81              2886 	mov	sp,a
                           2887 ;	main.c:323: printf_tiny("Total number of characters received since last '?' = %d\n\r", (char_count-lastchar_count));
                           2888 ;	genAssign
   08F7 90 0B C0           2889 	mov	dptr,#_lastchar_count
   08FA E0                 2890 	movx	a,@dptr
   08FB FA                 2891 	mov	r2,a
   08FC A3                 2892 	inc	dptr
   08FD E0                 2893 	movx	a,@dptr
   08FE FB                 2894 	mov	r3,a
                           2895 ;	genAssign
   08FF 90 0B BC           2896 	mov	dptr,#_char_count
   0902 E0                 2897 	movx	a,@dptr
   0903 FC                 2898 	mov	r4,a
   0904 A3                 2899 	inc	dptr
   0905 E0                 2900 	movx	a,@dptr
   0906 FD                 2901 	mov	r5,a
                           2902 ;	genMinus
   0907 EC                 2903 	mov	a,r4
   0908 C3                 2904 	clr	c
                           2905 ;	Peephole 236.l	used r2 instead of ar2
   0909 9A                 2906 	subb	a,r2
   090A FA                 2907 	mov	r2,a
   090B ED                 2908 	mov	a,r5
                           2909 ;	Peephole 236.l	used r3 instead of ar3
   090C 9B                 2910 	subb	a,r3
   090D FB                 2911 	mov	r3,a
                           2912 ;	genIpush
   090E C0 02              2913 	push	ar2
   0910 C0 03              2914 	push	ar3
                           2915 ;	genIpush
   0912 74 4B              2916 	mov	a,#__str_35
   0914 C0 E0              2917 	push	acc
   0916 74 23              2918 	mov	a,#(__str_35 >> 8)
   0918 C0 E0              2919 	push	acc
                           2920 ;	genCall
   091A 12 12 E5           2921 	lcall	_printf_tiny
   091D E5 81              2922 	mov	a,sp
   091F 24 FC              2923 	add	a,#0xfc
   0921 F5 81              2924 	mov	sp,a
                           2925 ;	main.c:324: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           2926 ;	genIpush
   0923 74 85              2927 	mov	a,#__str_36
   0925 C0 E0              2928 	push	acc
   0927 74 23              2929 	mov	a,#(__str_36 >> 8)
   0929 C0 E0              2930 	push	acc
                           2931 ;	genCall
   092B 12 12 E5           2932 	lcall	_printf_tiny
   092E 15 81              2933 	dec	sp
   0930 15 81              2934 	dec	sp
                           2935 ;	main.c:325: putstr("\n\r");
                           2936 ;	genCall
                           2937 ;	Peephole 182.a	used 16 bit load of DPTR
   0932 90 1E 13           2938 	mov	dptr,#__str_3
   0935 75 F0 80           2939 	mov	b,#0x80
   0938 12 0F 16           2940 	lcall	_putstr
                           2941 ;	main.c:326: lastchar_count=char_count;
                           2942 ;	genAssign
   093B 90 0B BC           2943 	mov	dptr,#_char_count
   093E E0                 2944 	movx	a,@dptr
   093F FA                 2945 	mov	r2,a
   0940 A3                 2946 	inc	dptr
   0941 E0                 2947 	movx	a,@dptr
   0942 FB                 2948 	mov	r3,a
                           2949 ;	genAssign
   0943 90 0B C0           2950 	mov	dptr,#_lastchar_count
   0946 EA                 2951 	mov	a,r2
   0947 F0                 2952 	movx	@dptr,a
   0948 A3                 2953 	inc	dptr
   0949 EB                 2954 	mov	a,r3
   094A F0                 2955 	movx	@dptr,a
                           2956 ;	main.c:327: while((unsigned char)*buff1ptr!='\0')           /* calculate number of characters in buffer0*/
                           2957 ;	genAssign
   094B 7A 00              2958 	mov	r2,#0x00
   094D 7B 00              2959 	mov	r3,#0x00
   094F                    2960 00101$:
                           2961 ;	genAssign
   094F 90 0D C2           2962 	mov	dptr,#_buff1ptr
   0952 E0                 2963 	movx	a,@dptr
   0953 FC                 2964 	mov	r4,a
   0954 A3                 2965 	inc	dptr
   0955 E0                 2966 	movx	a,@dptr
   0956 FD                 2967 	mov	r5,a
   0957 A3                 2968 	inc	dptr
   0958 E0                 2969 	movx	a,@dptr
   0959 FE                 2970 	mov	r6,a
                           2971 ;	genPointerGet
                           2972 ;	genGenPointerGet
   095A 8C 82              2973 	mov	dpl,r4
   095C 8D 83              2974 	mov	dph,r5
   095E 8E F0              2975 	mov	b,r6
   0960 12 1D A6           2976 	lcall	__gptrget
                           2977 ;	genCmpEq
                           2978 ;	gencjneshort
                           2979 ;	Peephole 112.b	changed ljmp to sjmp
   0963 FF                 2980 	mov	r7,a
                           2981 ;	Peephole 115.b	jump optimization
   0964 60 1D              2982 	jz	00122$
                           2983 ;	Peephole 300	removed redundant label 00125$
                           2984 ;	main.c:329: charnum0++;
                           2985 ;	genPlus
                           2986 ;     genPlusIncr
   0966 0A                 2987 	inc	r2
   0967 BA 00 01           2988 	cjne	r2,#0x00,00126$
   096A 0B                 2989 	inc	r3
   096B                    2990 00126$:
                           2991 ;	genAssign
   096B 90 0F DD           2992 	mov	dptr,#_displaydelete_charnum0_1_1
   096E EA                 2993 	mov	a,r2
   096F F0                 2994 	movx	@dptr,a
   0970 A3                 2995 	inc	dptr
   0971 EB                 2996 	mov	a,r3
   0972 F0                 2997 	movx	@dptr,a
                           2998 ;	main.c:330: (unsigned char)buff1ptr++;
                           2999 ;	genPlus
   0973 90 0D C2           3000 	mov	dptr,#_buff1ptr
                           3001 ;     genPlusIncr
   0976 74 01              3002 	mov	a,#0x01
                           3003 ;	Peephole 236.a	used r4 instead of ar4
   0978 2C                 3004 	add	a,r4
   0979 F0                 3005 	movx	@dptr,a
                           3006 ;	Peephole 181	changed mov to clr
   097A E4                 3007 	clr	a
                           3008 ;	Peephole 236.b	used r5 instead of ar5
   097B 3D                 3009 	addc	a,r5
   097C A3                 3010 	inc	dptr
   097D F0                 3011 	movx	@dptr,a
   097E A3                 3012 	inc	dptr
   097F EE                 3013 	mov	a,r6
   0980 F0                 3014 	movx	@dptr,a
                           3015 ;	Peephole 112.b	changed ljmp to sjmp
   0981 80 CC              3016 	sjmp	00101$
   0983                    3017 00122$:
                           3018 ;	genAssign
   0983 90 0F DD           3019 	mov	dptr,#_displaydelete_charnum0_1_1
   0986 EA                 3020 	mov	a,r2
   0987 F0                 3021 	movx	@dptr,a
   0988 A3                 3022 	inc	dptr
   0989 EB                 3023 	mov	a,r3
   098A F0                 3024 	movx	@dptr,a
                           3025 ;	main.c:332: printf_tiny("\n\rNumber of characters in buffer0 = %d\n\r", charnum0);
                           3026 ;	genIpush
   098B C0 02              3027 	push	ar2
   098D C0 03              3028 	push	ar3
   098F C0 02              3029 	push	ar2
   0991 C0 03              3030 	push	ar3
                           3031 ;	genIpush
   0993 74 E1              3032 	mov	a,#__str_37
   0995 C0 E0              3033 	push	acc
   0997 74 23              3034 	mov	a,#(__str_37 >> 8)
   0999 C0 E0              3035 	push	acc
                           3036 ;	genCall
   099B 12 12 E5           3037 	lcall	_printf_tiny
   099E E5 81              3038 	mov	a,sp
   09A0 24 FC              3039 	add	a,#0xfc
   09A2 F5 81              3040 	mov	sp,a
   09A4 D0 03              3041 	pop	ar3
   09A6 D0 02              3042 	pop	ar2
                           3043 ;	main.c:333: printf_tiny("Number of free spaces in buffer0 = %d\n\r", (bufflen[0]-charnum0));
                           3044 ;	genPointerGet
                           3045 ;	genFarPointerGet
   09A8 90 0D CE           3046 	mov	dptr,#_bufflen
   09AB E0                 3047 	movx	a,@dptr
   09AC FC                 3048 	mov	r4,a
   09AD A3                 3049 	inc	dptr
   09AE E0                 3050 	movx	a,@dptr
   09AF FD                 3051 	mov	r5,a
                           3052 ;	genMinus
   09B0 EC                 3053 	mov	a,r4
   09B1 C3                 3054 	clr	c
                           3055 ;	Peephole 236.l	used r2 instead of ar2
   09B2 9A                 3056 	subb	a,r2
   09B3 FC                 3057 	mov	r4,a
   09B4 ED                 3058 	mov	a,r5
                           3059 ;	Peephole 236.l	used r3 instead of ar3
   09B5 9B                 3060 	subb	a,r3
   09B6 FD                 3061 	mov	r5,a
                           3062 ;	genIpush
   09B7 C0 02              3063 	push	ar2
   09B9 C0 03              3064 	push	ar3
   09BB C0 04              3065 	push	ar4
   09BD C0 05              3066 	push	ar5
                           3067 ;	genIpush
   09BF 74 0A              3068 	mov	a,#__str_38
   09C1 C0 E0              3069 	push	acc
   09C3 74 24              3070 	mov	a,#(__str_38 >> 8)
   09C5 C0 E0              3071 	push	acc
                           3072 ;	genCall
   09C7 12 12 E5           3073 	lcall	_printf_tiny
   09CA E5 81              3074 	mov	a,sp
   09CC 24 FC              3075 	add	a,#0xfc
   09CE F5 81              3076 	mov	sp,a
   09D0 D0 03              3077 	pop	ar3
   09D2 D0 02              3078 	pop	ar2
                           3079 ;	main.c:334: buff1ptr=buff1ptr-charnum0;
                           3080 ;	genAssign
   09D4 90 0D C2           3081 	mov	dptr,#_buff1ptr
   09D7 E0                 3082 	movx	a,@dptr
   09D8 FC                 3083 	mov	r4,a
   09D9 A3                 3084 	inc	dptr
   09DA E0                 3085 	movx	a,@dptr
   09DB FD                 3086 	mov	r5,a
   09DC A3                 3087 	inc	dptr
   09DD E0                 3088 	movx	a,@dptr
   09DE FE                 3089 	mov	r6,a
                           3090 ;	genMinus
   09DF 90 0D C2           3091 	mov	dptr,#_buff1ptr
   09E2 EC                 3092 	mov	a,r4
   09E3 C3                 3093 	clr	c
                           3094 ;	Peephole 236.l	used r2 instead of ar2
   09E4 9A                 3095 	subb	a,r2
   09E5 F0                 3096 	movx	@dptr,a
   09E6 ED                 3097 	mov	a,r5
                           3098 ;	Peephole 236.l	used r3 instead of ar3
   09E7 9B                 3099 	subb	a,r3
   09E8 A3                 3100 	inc	dptr
   09E9 F0                 3101 	movx	@dptr,a
   09EA A3                 3102 	inc	dptr
   09EB EE                 3103 	mov	a,r6
   09EC F0                 3104 	movx	@dptr,a
                           3105 ;	main.c:335: while((unsigned char)*buff2ptr!='\0')           /* calculate number of characters in buffer1*/
                           3106 ;	genAssign
   09ED 7A 00              3107 	mov	r2,#0x00
   09EF 7B 00              3108 	mov	r3,#0x00
   09F1                    3109 00104$:
                           3110 ;	genAssign
   09F1 90 0D C5           3111 	mov	dptr,#_buff2ptr
   09F4 E0                 3112 	movx	a,@dptr
   09F5 FC                 3113 	mov	r4,a
   09F6 A3                 3114 	inc	dptr
   09F7 E0                 3115 	movx	a,@dptr
   09F8 FD                 3116 	mov	r5,a
   09F9 A3                 3117 	inc	dptr
   09FA E0                 3118 	movx	a,@dptr
   09FB FE                 3119 	mov	r6,a
                           3120 ;	genPointerGet
                           3121 ;	genGenPointerGet
   09FC 8C 82              3122 	mov	dpl,r4
   09FE 8D 83              3123 	mov	dph,r5
   0A00 8E F0              3124 	mov	b,r6
   0A02 12 1D A6           3125 	lcall	__gptrget
                           3126 ;	genCmpEq
                           3127 ;	gencjneshort
                           3128 ;	Peephole 112.b	changed ljmp to sjmp
   0A05 FF                 3129 	mov	r7,a
                           3130 ;	Peephole 115.b	jump optimization
   0A06 60 1D              3131 	jz	00123$
                           3132 ;	Peephole 300	removed redundant label 00127$
                           3133 ;	main.c:337: charnum1++;
                           3134 ;	genPlus
                           3135 ;     genPlusIncr
   0A08 0A                 3136 	inc	r2
   0A09 BA 00 01           3137 	cjne	r2,#0x00,00128$
   0A0C 0B                 3138 	inc	r3
   0A0D                    3139 00128$:
                           3140 ;	genAssign
   0A0D 90 0F DF           3141 	mov	dptr,#_displaydelete_charnum1_1_1
   0A10 EA                 3142 	mov	a,r2
   0A11 F0                 3143 	movx	@dptr,a
   0A12 A3                 3144 	inc	dptr
   0A13 EB                 3145 	mov	a,r3
   0A14 F0                 3146 	movx	@dptr,a
                           3147 ;	main.c:338: (unsigned char)buff2ptr++;
                           3148 ;	genPlus
   0A15 90 0D C5           3149 	mov	dptr,#_buff2ptr
                           3150 ;     genPlusIncr
   0A18 74 01              3151 	mov	a,#0x01
                           3152 ;	Peephole 236.a	used r4 instead of ar4
   0A1A 2C                 3153 	add	a,r4
   0A1B F0                 3154 	movx	@dptr,a
                           3155 ;	Peephole 181	changed mov to clr
   0A1C E4                 3156 	clr	a
                           3157 ;	Peephole 236.b	used r5 instead of ar5
   0A1D 3D                 3158 	addc	a,r5
   0A1E A3                 3159 	inc	dptr
   0A1F F0                 3160 	movx	@dptr,a
   0A20 A3                 3161 	inc	dptr
   0A21 EE                 3162 	mov	a,r6
   0A22 F0                 3163 	movx	@dptr,a
                           3164 ;	Peephole 112.b	changed ljmp to sjmp
   0A23 80 CC              3165 	sjmp	00104$
   0A25                    3166 00123$:
                           3167 ;	genAssign
   0A25 90 0F DF           3168 	mov	dptr,#_displaydelete_charnum1_1_1
   0A28 EA                 3169 	mov	a,r2
   0A29 F0                 3170 	movx	@dptr,a
   0A2A A3                 3171 	inc	dptr
   0A2B EB                 3172 	mov	a,r3
   0A2C F0                 3173 	movx	@dptr,a
                           3174 ;	main.c:340: printf_tiny("Number of characters in buffer1 = %d\n\r", charnum1);
                           3175 ;	genIpush
   0A2D C0 02              3176 	push	ar2
   0A2F C0 03              3177 	push	ar3
   0A31 C0 02              3178 	push	ar2
   0A33 C0 03              3179 	push	ar3
                           3180 ;	genIpush
   0A35 74 32              3181 	mov	a,#__str_39
   0A37 C0 E0              3182 	push	acc
   0A39 74 24              3183 	mov	a,#(__str_39 >> 8)
   0A3B C0 E0              3184 	push	acc
                           3185 ;	genCall
   0A3D 12 12 E5           3186 	lcall	_printf_tiny
   0A40 E5 81              3187 	mov	a,sp
   0A42 24 FC              3188 	add	a,#0xfc
   0A44 F5 81              3189 	mov	sp,a
   0A46 D0 03              3190 	pop	ar3
   0A48 D0 02              3191 	pop	ar2
                           3192 ;	main.c:341: printf_tiny("Number of free spaces in buffer0 = %d\n\r", (bufflen[1]-charnum1));
                           3193 ;	genPointerGet
                           3194 ;	genFarPointerGet
   0A4A 90 0D D0           3195 	mov	dptr,#(_bufflen + 0x0002)
   0A4D E0                 3196 	movx	a,@dptr
   0A4E FC                 3197 	mov	r4,a
   0A4F A3                 3198 	inc	dptr
   0A50 E0                 3199 	movx	a,@dptr
   0A51 FD                 3200 	mov	r5,a
                           3201 ;	genMinus
   0A52 EC                 3202 	mov	a,r4
   0A53 C3                 3203 	clr	c
                           3204 ;	Peephole 236.l	used r2 instead of ar2
   0A54 9A                 3205 	subb	a,r2
   0A55 FC                 3206 	mov	r4,a
   0A56 ED                 3207 	mov	a,r5
                           3208 ;	Peephole 236.l	used r3 instead of ar3
   0A57 9B                 3209 	subb	a,r3
   0A58 FD                 3210 	mov	r5,a
                           3211 ;	genIpush
   0A59 C0 02              3212 	push	ar2
   0A5B C0 03              3213 	push	ar3
   0A5D C0 04              3214 	push	ar4
   0A5F C0 05              3215 	push	ar5
                           3216 ;	genIpush
   0A61 74 0A              3217 	mov	a,#__str_38
   0A63 C0 E0              3218 	push	acc
   0A65 74 24              3219 	mov	a,#(__str_38 >> 8)
   0A67 C0 E0              3220 	push	acc
                           3221 ;	genCall
   0A69 12 12 E5           3222 	lcall	_printf_tiny
   0A6C E5 81              3223 	mov	a,sp
   0A6E 24 FC              3224 	add	a,#0xfc
   0A70 F5 81              3225 	mov	sp,a
   0A72 D0 03              3226 	pop	ar3
   0A74 D0 02              3227 	pop	ar2
                           3228 ;	main.c:342: buff2ptr=buff2ptr-charnum1;
                           3229 ;	genAssign
   0A76 90 0D C5           3230 	mov	dptr,#_buff2ptr
   0A79 E0                 3231 	movx	a,@dptr
   0A7A FC                 3232 	mov	r4,a
   0A7B A3                 3233 	inc	dptr
   0A7C E0                 3234 	movx	a,@dptr
   0A7D FD                 3235 	mov	r5,a
   0A7E A3                 3236 	inc	dptr
   0A7F E0                 3237 	movx	a,@dptr
   0A80 FE                 3238 	mov	r6,a
                           3239 ;	genMinus
   0A81 90 0D C5           3240 	mov	dptr,#_buff2ptr
   0A84 EC                 3241 	mov	a,r4
   0A85 C3                 3242 	clr	c
                           3243 ;	Peephole 236.l	used r2 instead of ar2
   0A86 9A                 3244 	subb	a,r2
   0A87 F0                 3245 	movx	@dptr,a
   0A88 ED                 3246 	mov	a,r5
                           3247 ;	Peephole 236.l	used r3 instead of ar3
   0A89 9B                 3248 	subb	a,r3
   0A8A A3                 3249 	inc	dptr
   0A8B F0                 3250 	movx	@dptr,a
   0A8C A3                 3251 	inc	dptr
   0A8D EE                 3252 	mov	a,r6
   0A8E F0                 3253 	movx	@dptr,a
                           3254 ;	main.c:343: putstr("\n\r");
                           3255 ;	genCall
                           3256 ;	Peephole 182.a	used 16 bit load of DPTR
   0A8F 90 1E 13           3257 	mov	dptr,#__str_3
   0A92 75 F0 80           3258 	mov	b,#0x80
   0A95 12 0F 16           3259 	lcall	_putstr
                           3260 ;	main.c:344: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           3261 ;	genIpush
   0A98 74 85              3262 	mov	a,#__str_36
   0A9A C0 E0              3263 	push	acc
   0A9C 74 23              3264 	mov	a,#(__str_36 >> 8)
   0A9E C0 E0              3265 	push	acc
                           3266 ;	genCall
   0AA0 12 12 E5           3267 	lcall	_printf_tiny
   0AA3 15 81              3268 	dec	sp
   0AA5 15 81              3269 	dec	sp
                           3270 ;	main.c:345: putstr("\n\r");
                           3271 ;	genCall
                           3272 ;	Peephole 182.a	used 16 bit load of DPTR
   0AA7 90 1E 13           3273 	mov	dptr,#__str_3
   0AAA 75 F0 80           3274 	mov	b,#0x80
   0AAD 12 0F 16           3275 	lcall	_putstr
                           3276 ;	main.c:346: for(len=0; len<buffer_count; len++)
                           3277 ;	genAssign
   0AB0 7A 00              3278 	mov	r2,#0x00
   0AB2 7B 00              3279 	mov	r3,#0x00
   0AB4                    3280 00109$:
                           3281 ;	genAssign
   0AB4 90 0B B8           3282 	mov	dptr,#_buffer_count
   0AB7 E0                 3283 	movx	a,@dptr
   0AB8 FC                 3284 	mov	r4,a
   0AB9 A3                 3285 	inc	dptr
   0ABA E0                 3286 	movx	a,@dptr
   0ABB FD                 3287 	mov	r5,a
                           3288 ;	genCmpLt
                           3289 ;	genCmp
   0ABC C3                 3290 	clr	c
   0ABD EA                 3291 	mov	a,r2
   0ABE 9C                 3292 	subb	a,r4
   0ABF EB                 3293 	mov	a,r3
   0AC0 9D                 3294 	subb	a,r5
                           3295 ;	genIfxJump
   0AC1 40 03              3296 	jc	00129$
   0AC3 02 0B CB           3297 	ljmp	00112$
   0AC6                    3298 00129$:
                           3299 ;	main.c:348: if(buffer[len])
                           3300 ;	genLeftShift
                           3301 ;	genLeftShiftLiteral
                           3302 ;	genlshTwo
   0AC6 8A 04              3303 	mov	ar4,r2
   0AC8 EB                 3304 	mov	a,r3
   0AC9 CC                 3305 	xch	a,r4
   0ACA 25 E0              3306 	add	a,acc
   0ACC CC                 3307 	xch	a,r4
   0ACD 33                 3308 	rlc	a
   0ACE FD                 3309 	mov	r5,a
                           3310 ;	genPlus
                           3311 ;	Peephole 236.g	used r4 instead of ar4
   0ACF EC                 3312 	mov	a,r4
   0AD0 24 C2              3313 	add	a,#_buffer
   0AD2 F5 82              3314 	mov	dpl,a
                           3315 ;	Peephole 236.g	used r5 instead of ar5
   0AD4 ED                 3316 	mov	a,r5
   0AD5 34 0B              3317 	addc	a,#(_buffer >> 8)
   0AD7 F5 83              3318 	mov	dph,a
                           3319 ;	genPointerGet
                           3320 ;	genFarPointerGet
   0AD9 E0                 3321 	movx	a,@dptr
   0ADA FE                 3322 	mov	r6,a
   0ADB A3                 3323 	inc	dptr
   0ADC E0                 3324 	movx	a,@dptr
                           3325 ;	genIfx
   0ADD FF                 3326 	mov	r7,a
                           3327 ;	Peephole 135	removed redundant mov
   0ADE 4E                 3328 	orl	a,r6
                           3329 ;	genIfxJump
   0ADF 70 03              3330 	jnz	00130$
   0AE1 02 0B C3           3331 	ljmp	00111$
   0AE4                    3332 00130$:
                           3333 ;	main.c:350: printf_tiny("Buffer %d Information\n\r", len);
                           3334 ;	genIpush
   0AE4 C0 02              3335 	push	ar2
   0AE6 C0 03              3336 	push	ar3
   0AE8 C0 04              3337 	push	ar4
   0AEA C0 05              3338 	push	ar5
   0AEC C0 02              3339 	push	ar2
   0AEE C0 03              3340 	push	ar3
                           3341 ;	genIpush
   0AF0 74 59              3342 	mov	a,#__str_40
   0AF2 C0 E0              3343 	push	acc
   0AF4 74 24              3344 	mov	a,#(__str_40 >> 8)
   0AF6 C0 E0              3345 	push	acc
                           3346 ;	genCall
   0AF8 12 12 E5           3347 	lcall	_printf_tiny
   0AFB E5 81              3348 	mov	a,sp
   0AFD 24 FC              3349 	add	a,#0xfc
   0AFF F5 81              3350 	mov	sp,a
   0B01 D0 05              3351 	pop	ar5
   0B03 D0 04              3352 	pop	ar4
   0B05 D0 03              3353 	pop	ar3
   0B07 D0 02              3354 	pop	ar2
                           3355 ;	main.c:351: printf_tiny("Starting address = 0x%x\n\r",(unsigned int)buffer[len]);
                           3356 ;	genPlus
                           3357 ;	Peephole 236.g	used r4 instead of ar4
   0B09 EC                 3358 	mov	a,r4
   0B0A 24 C2              3359 	add	a,#_buffer
   0B0C F5 82              3360 	mov	dpl,a
                           3361 ;	Peephole 236.g	used r5 instead of ar5
   0B0E ED                 3362 	mov	a,r5
   0B0F 34 0B              3363 	addc	a,#(_buffer >> 8)
   0B11 F5 83              3364 	mov	dph,a
                           3365 ;	genPointerGet
                           3366 ;	genFarPointerGet
   0B13 E0                 3367 	movx	a,@dptr
   0B14 FE                 3368 	mov	r6,a
   0B15 A3                 3369 	inc	dptr
   0B16 E0                 3370 	movx	a,@dptr
   0B17 FF                 3371 	mov	r7,a
                           3372 ;	genCast
                           3373 ;	genIpush
   0B18 C0 02              3374 	push	ar2
   0B1A C0 03              3375 	push	ar3
   0B1C C0 04              3376 	push	ar4
   0B1E C0 05              3377 	push	ar5
   0B20 C0 06              3378 	push	ar6
   0B22 C0 07              3379 	push	ar7
                           3380 ;	genIpush
   0B24 74 71              3381 	mov	a,#__str_41
   0B26 C0 E0              3382 	push	acc
   0B28 74 24              3383 	mov	a,#(__str_41 >> 8)
   0B2A C0 E0              3384 	push	acc
                           3385 ;	genCall
   0B2C 12 12 E5           3386 	lcall	_printf_tiny
   0B2F E5 81              3387 	mov	a,sp
   0B31 24 FC              3388 	add	a,#0xfc
   0B33 F5 81              3389 	mov	sp,a
   0B35 D0 05              3390 	pop	ar5
   0B37 D0 04              3391 	pop	ar4
   0B39 D0 03              3392 	pop	ar3
   0B3B D0 02              3393 	pop	ar2
                           3394 ;	main.c:352: printf_tiny("Ending address = 0x%x\n\r",((unsigned int)buffer[len]+(unsigned int)bufflen[len]));
                           3395 ;	genPlus
                           3396 ;	Peephole 236.g	used r4 instead of ar4
   0B3D EC                 3397 	mov	a,r4
   0B3E 24 C2              3398 	add	a,#_buffer
   0B40 F5 82              3399 	mov	dpl,a
                           3400 ;	Peephole 236.g	used r5 instead of ar5
   0B42 ED                 3401 	mov	a,r5
   0B43 34 0B              3402 	addc	a,#(_buffer >> 8)
   0B45 F5 83              3403 	mov	dph,a
                           3404 ;	genPointerGet
                           3405 ;	genFarPointerGet
   0B47 E0                 3406 	movx	a,@dptr
   0B48 FE                 3407 	mov	r6,a
   0B49 A3                 3408 	inc	dptr
   0B4A E0                 3409 	movx	a,@dptr
   0B4B FF                 3410 	mov	r7,a
                           3411 ;	genCast
                           3412 ;	genPlus
                           3413 ;	Peephole 236.g	used r4 instead of ar4
   0B4C EC                 3414 	mov	a,r4
   0B4D 24 CE              3415 	add	a,#_bufflen
   0B4F F5 82              3416 	mov	dpl,a
                           3417 ;	Peephole 236.g	used r5 instead of ar5
   0B51 ED                 3418 	mov	a,r5
   0B52 34 0D              3419 	addc	a,#(_bufflen >> 8)
   0B54 F5 83              3420 	mov	dph,a
                           3421 ;	genPointerGet
                           3422 ;	genFarPointerGet
   0B56 E0                 3423 	movx	a,@dptr
   0B57 F8                 3424 	mov	r0,a
   0B58 A3                 3425 	inc	dptr
   0B59 E0                 3426 	movx	a,@dptr
   0B5A F9                 3427 	mov	r1,a
                           3428 ;	genPlus
                           3429 ;	Peephole 236.g	used r0 instead of ar0
   0B5B E8                 3430 	mov	a,r0
                           3431 ;	Peephole 236.a	used r6 instead of ar6
   0B5C 2E                 3432 	add	a,r6
   0B5D FE                 3433 	mov	r6,a
                           3434 ;	Peephole 236.g	used r1 instead of ar1
   0B5E E9                 3435 	mov	a,r1
                           3436 ;	Peephole 236.b	used r7 instead of ar7
   0B5F 3F                 3437 	addc	a,r7
   0B60 FF                 3438 	mov	r7,a
                           3439 ;	genIpush
   0B61 C0 02              3440 	push	ar2
   0B63 C0 03              3441 	push	ar3
   0B65 C0 04              3442 	push	ar4
   0B67 C0 05              3443 	push	ar5
   0B69 C0 06              3444 	push	ar6
   0B6B C0 07              3445 	push	ar7
                           3446 ;	genIpush
   0B6D 74 8B              3447 	mov	a,#__str_42
   0B6F C0 E0              3448 	push	acc
   0B71 74 24              3449 	mov	a,#(__str_42 >> 8)
   0B73 C0 E0              3450 	push	acc
                           3451 ;	genCall
   0B75 12 12 E5           3452 	lcall	_printf_tiny
   0B78 E5 81              3453 	mov	a,sp
   0B7A 24 FC              3454 	add	a,#0xfc
   0B7C F5 81              3455 	mov	sp,a
   0B7E D0 05              3456 	pop	ar5
   0B80 D0 04              3457 	pop	ar4
   0B82 D0 03              3458 	pop	ar3
   0B84 D0 02              3459 	pop	ar2
                           3460 ;	main.c:353: printf_tiny("Allocated size = %d\n\r", bufflen[len]);
                           3461 ;	genPlus
                           3462 ;	Peephole 236.g	used r4 instead of ar4
   0B86 EC                 3463 	mov	a,r4
   0B87 24 CE              3464 	add	a,#_bufflen
   0B89 F5 82              3465 	mov	dpl,a
                           3466 ;	Peephole 236.g	used r5 instead of ar5
   0B8B ED                 3467 	mov	a,r5
   0B8C 34 0D              3468 	addc	a,#(_bufflen >> 8)
   0B8E F5 83              3469 	mov	dph,a
                           3470 ;	genPointerGet
                           3471 ;	genFarPointerGet
   0B90 E0                 3472 	movx	a,@dptr
   0B91 FC                 3473 	mov	r4,a
   0B92 A3                 3474 	inc	dptr
   0B93 E0                 3475 	movx	a,@dptr
   0B94 FD                 3476 	mov	r5,a
                           3477 ;	genIpush
   0B95 C0 02              3478 	push	ar2
   0B97 C0 03              3479 	push	ar3
   0B99 C0 04              3480 	push	ar4
   0B9B C0 05              3481 	push	ar5
                           3482 ;	genIpush
   0B9D 74 A3              3483 	mov	a,#__str_43
   0B9F C0 E0              3484 	push	acc
   0BA1 74 24              3485 	mov	a,#(__str_43 >> 8)
   0BA3 C0 E0              3486 	push	acc
                           3487 ;	genCall
   0BA5 12 12 E5           3488 	lcall	_printf_tiny
   0BA8 E5 81              3489 	mov	a,sp
   0BAA 24 FC              3490 	add	a,#0xfc
   0BAC F5 81              3491 	mov	sp,a
   0BAE D0 03              3492 	pop	ar3
   0BB0 D0 02              3493 	pop	ar2
                           3494 ;	main.c:354: putstr("\n\r");
                           3495 ;	genCall
                           3496 ;	Peephole 182.a	used 16 bit load of DPTR
   0BB2 90 1E 13           3497 	mov	dptr,#__str_3
   0BB5 75 F0 80           3498 	mov	b,#0x80
   0BB8 C0 02              3499 	push	ar2
   0BBA C0 03              3500 	push	ar3
   0BBC 12 0F 16           3501 	lcall	_putstr
   0BBF D0 03              3502 	pop	ar3
   0BC1 D0 02              3503 	pop	ar2
   0BC3                    3504 00111$:
                           3505 ;	main.c:346: for(len=0; len<buffer_count; len++)
                           3506 ;	genPlus
                           3507 ;     genPlusIncr
   0BC3 0A                 3508 	inc	r2
   0BC4 BA 00 01           3509 	cjne	r2,#0x00,00131$
   0BC7 0B                 3510 	inc	r3
   0BC8                    3511 00131$:
   0BC8 02 0A B4           3512 	ljmp	00109$
   0BCB                    3513 00112$:
                           3514 ;	main.c:357: printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           3515 ;	genIpush
   0BCB 74 71              3516 	mov	a,#__str_31
   0BCD C0 E0              3517 	push	acc
   0BCF 74 22              3518 	mov	a,#(__str_31 >> 8)
   0BD1 C0 E0              3519 	push	acc
                           3520 ;	genCall
   0BD3 12 12 E5           3521 	lcall	_printf_tiny
   0BD6 15 81              3522 	dec	sp
   0BD8 15 81              3523 	dec	sp
                           3524 ;	main.c:358: putstr("\n\rContents of Buffer0\n\r");
                           3525 ;	genCall
                           3526 ;	Peephole 182.a	used 16 bit load of DPTR
   0BDA 90 24 B9           3527 	mov	dptr,#__str_44
   0BDD 75 F0 80           3528 	mov	b,#0x80
   0BE0 12 0F 16           3529 	lcall	_putstr
                           3530 ;	main.c:359: putstrbuff(buff1ptr);               /* Displays content */
                           3531 ;	genAssign
   0BE3 90 0D C2           3532 	mov	dptr,#_buff1ptr
   0BE6 E0                 3533 	movx	a,@dptr
   0BE7 FA                 3534 	mov	r2,a
   0BE8 A3                 3535 	inc	dptr
   0BE9 E0                 3536 	movx	a,@dptr
   0BEA FB                 3537 	mov	r3,a
   0BEB A3                 3538 	inc	dptr
   0BEC E0                 3539 	movx	a,@dptr
   0BED FC                 3540 	mov	r4,a
                           3541 ;	genCall
   0BEE 8A 82              3542 	mov	dpl,r2
   0BF0 8B 83              3543 	mov	dph,r3
   0BF2 8C F0              3544 	mov	b,r4
   0BF4 12 0E 57           3545 	lcall	_putstrbuff
                           3546 ;	main.c:360: bptr0=bptr0-charnum0;               /*Move back the pointer to original after erasing buffer*/
                           3547 ;	genAssign
   0BF7 90 0F DD           3548 	mov	dptr,#_displaydelete_charnum0_1_1
   0BFA E0                 3549 	movx	a,@dptr
   0BFB FA                 3550 	mov	r2,a
   0BFC A3                 3551 	inc	dptr
   0BFD E0                 3552 	movx	a,@dptr
   0BFE FB                 3553 	mov	r3,a
                           3554 ;	genAssign
   0BFF 90 0D C8           3555 	mov	dptr,#_bptr0
   0C02 E0                 3556 	movx	a,@dptr
   0C03 FC                 3557 	mov	r4,a
   0C04 A3                 3558 	inc	dptr
   0C05 E0                 3559 	movx	a,@dptr
   0C06 FD                 3560 	mov	r5,a
   0C07 A3                 3561 	inc	dptr
   0C08 E0                 3562 	movx	a,@dptr
   0C09 FE                 3563 	mov	r6,a
                           3564 ;	genMinus
   0C0A 90 0D C8           3565 	mov	dptr,#_bptr0
   0C0D EC                 3566 	mov	a,r4
   0C0E C3                 3567 	clr	c
                           3568 ;	Peephole 236.l	used r2 instead of ar2
   0C0F 9A                 3569 	subb	a,r2
   0C10 F0                 3570 	movx	@dptr,a
   0C11 ED                 3571 	mov	a,r5
                           3572 ;	Peephole 236.l	used r3 instead of ar3
   0C12 9B                 3573 	subb	a,r3
   0C13 A3                 3574 	inc	dptr
   0C14 F0                 3575 	movx	@dptr,a
   0C15 A3                 3576 	inc	dptr
   0C16 EE                 3577 	mov	a,r6
   0C17 F0                 3578 	movx	@dptr,a
                           3579 ;	main.c:361: putstr("\n\r");
                           3580 ;	genCall
                           3581 ;	Peephole 182.a	used 16 bit load of DPTR
   0C18 90 1E 13           3582 	mov	dptr,#__str_3
   0C1B 75 F0 80           3583 	mov	b,#0x80
   0C1E 12 0F 16           3584 	lcall	_putstr
                           3585 ;	main.c:362: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           3586 ;	genIpush
   0C21 74 85              3587 	mov	a,#__str_36
   0C23 C0 E0              3588 	push	acc
   0C25 74 23              3589 	mov	a,#(__str_36 >> 8)
   0C27 C0 E0              3590 	push	acc
                           3591 ;	genCall
   0C29 12 12 E5           3592 	lcall	_printf_tiny
   0C2C 15 81              3593 	dec	sp
   0C2E 15 81              3594 	dec	sp
                           3595 ;	main.c:363: putstr("\n\rContents of Buffer1\n\r");
                           3596 ;	genCall
                           3597 ;	Peephole 182.a	used 16 bit load of DPTR
   0C30 90 24 D1           3598 	mov	dptr,#__str_45
   0C33 75 F0 80           3599 	mov	b,#0x80
   0C36 12 0F 16           3600 	lcall	_putstr
                           3601 ;	main.c:364: putstrbuff(buff2ptr);               /*Displays content*/
                           3602 ;	genAssign
   0C39 90 0D C5           3603 	mov	dptr,#_buff2ptr
   0C3C E0                 3604 	movx	a,@dptr
   0C3D FA                 3605 	mov	r2,a
   0C3E A3                 3606 	inc	dptr
   0C3F E0                 3607 	movx	a,@dptr
   0C40 FB                 3608 	mov	r3,a
   0C41 A3                 3609 	inc	dptr
   0C42 E0                 3610 	movx	a,@dptr
   0C43 FC                 3611 	mov	r4,a
                           3612 ;	genCall
   0C44 8A 82              3613 	mov	dpl,r2
   0C46 8B 83              3614 	mov	dph,r3
   0C48 8C F0              3615 	mov	b,r4
   0C4A 12 0E 57           3616 	lcall	_putstrbuff
                           3617 ;	main.c:365: bptr1=bptr1-charnum1;               /*Move back the pointer to original after erasing buffer*/
                           3618 ;	genAssign
   0C4D 90 0F DF           3619 	mov	dptr,#_displaydelete_charnum1_1_1
   0C50 E0                 3620 	movx	a,@dptr
   0C51 FA                 3621 	mov	r2,a
   0C52 A3                 3622 	inc	dptr
   0C53 E0                 3623 	movx	a,@dptr
   0C54 FB                 3624 	mov	r3,a
                           3625 ;	genAssign
   0C55 90 0D CB           3626 	mov	dptr,#_bptr1
   0C58 E0                 3627 	movx	a,@dptr
   0C59 FC                 3628 	mov	r4,a
   0C5A A3                 3629 	inc	dptr
   0C5B E0                 3630 	movx	a,@dptr
   0C5C FD                 3631 	mov	r5,a
   0C5D A3                 3632 	inc	dptr
   0C5E E0                 3633 	movx	a,@dptr
   0C5F FE                 3634 	mov	r6,a
                           3635 ;	genMinus
   0C60 90 0D CB           3636 	mov	dptr,#_bptr1
   0C63 EC                 3637 	mov	a,r4
   0C64 C3                 3638 	clr	c
                           3639 ;	Peephole 236.l	used r2 instead of ar2
   0C65 9A                 3640 	subb	a,r2
   0C66 F0                 3641 	movx	@dptr,a
   0C67 ED                 3642 	mov	a,r5
                           3643 ;	Peephole 236.l	used r3 instead of ar3
   0C68 9B                 3644 	subb	a,r3
   0C69 A3                 3645 	inc	dptr
   0C6A F0                 3646 	movx	@dptr,a
   0C6B A3                 3647 	inc	dptr
   0C6C EE                 3648 	mov	a,r6
   0C6D F0                 3649 	movx	@dptr,a
                           3650 ;	main.c:366: putstr("\n\r");
                           3651 ;	genCall
                           3652 ;	Peephole 182.a	used 16 bit load of DPTR
   0C6E 90 1E 13           3653 	mov	dptr,#__str_3
   0C71 75 F0 80           3654 	mov	b,#0x80
   0C74 12 0F 16           3655 	lcall	_putstr
                           3656 ;	main.c:367: printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           3657 ;	genIpush
   0C77 74 85              3658 	mov	a,#__str_36
   0C79 C0 E0              3659 	push	acc
   0C7B 74 23              3660 	mov	a,#(__str_36 >> 8)
   0C7D C0 E0              3661 	push	acc
                           3662 ;	genCall
   0C7F 12 12 E5           3663 	lcall	_printf_tiny
   0C82 15 81              3664 	dec	sp
   0C84 15 81              3665 	dec	sp
                           3666 ;	main.c:368: printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYED ALL DETAILS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
                           3667 ;	genIpush
   0C86 74 E9              3668 	mov	a,#__str_46
   0C88 C0 E0              3669 	push	acc
   0C8A 74 24              3670 	mov	a,#(__str_46 >> 8)
   0C8C C0 E0              3671 	push	acc
                           3672 ;	genCall
   0C8E 12 12 E5           3673 	lcall	_printf_tiny
   0C91 15 81              3674 	dec	sp
   0C93 15 81              3675 	dec	sp
                           3676 ;	Peephole 300	removed redundant label 00113$
   0C95 22                 3677 	ret
                           3678 ;------------------------------------------------------------
                           3679 ;Allocation info for local variables in function 'display'
                           3680 ;------------------------------------------------------------
                           3681 ;------------------------------------------------------------
                           3682 ;	main.c:373: void display()
                           3683 ;	-----------------------------------------
                           3684 ;	 function display
                           3685 ;	-----------------------------------------
   0C96                    3686 _display:
                           3687 ;	main.c:375: putstr("\n\r");
                           3688 ;	genCall
                           3689 ;	Peephole 182.a	used 16 bit load of DPTR
   0C96 90 1E 13           3690 	mov	dptr,#__str_3
   0C99 75 F0 80           3691 	mov	b,#0x80
   0C9C 12 0F 16           3692 	lcall	_putstr
                           3693 ;	main.c:376: putstr("\n\rContents of Buffer0\n\r");
                           3694 ;	genCall
                           3695 ;	Peephole 182.a	used 16 bit load of DPTR
   0C9F 90 24 B9           3696 	mov	dptr,#__str_44
   0CA2 75 F0 80           3697 	mov	b,#0x80
   0CA5 12 0F 16           3698 	lcall	_putstr
                           3699 ;	main.c:377: putstrhex(0);
                           3700 ;	genCall
                           3701 ;	Peephole 182.b	used 16 bit load of dptr
   0CA8 90 00 00           3702 	mov	dptr,#0x0000
   0CAB 12 0C CF           3703 	lcall	_putstrhex
                           3704 ;	main.c:378: putstr("\n\r");
                           3705 ;	genCall
                           3706 ;	Peephole 182.a	used 16 bit load of DPTR
   0CAE 90 1E 13           3707 	mov	dptr,#__str_3
   0CB1 75 F0 80           3708 	mov	b,#0x80
   0CB4 12 0F 16           3709 	lcall	_putstr
                           3710 ;	main.c:379: putstr("Contents of Buffer1\n\r");
                           3711 ;	genCall
                           3712 ;	Peephole 182.a	used 16 bit load of DPTR
   0CB7 90 25 43           3713 	mov	dptr,#__str_47
   0CBA 75 F0 80           3714 	mov	b,#0x80
   0CBD 12 0F 16           3715 	lcall	_putstr
                           3716 ;	main.c:380: putstrhex(1);
                           3717 ;	genCall
                           3718 ;	Peephole 182.b	used 16 bit load of dptr
   0CC0 90 00 01           3719 	mov	dptr,#0x0001
   0CC3 12 0C CF           3720 	lcall	_putstrhex
                           3721 ;	main.c:381: putstr("\n\r");
                           3722 ;	genCall
                           3723 ;	Peephole 182.a	used 16 bit load of DPTR
   0CC6 90 1E 13           3724 	mov	dptr,#__str_3
   0CC9 75 F0 80           3725 	mov	b,#0x80
                           3726 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0CCC 02 0F 16           3727 	ljmp	_putstr
                           3728 ;
                           3729 ;------------------------------------------------------------
                           3730 ;Allocation info for local variables in function 'putstrhex'
                           3731 ;------------------------------------------------------------
                           3732 ;sloc0                     Allocated with name '_putstrhex_sloc0_1_0'
                           3733 ;sloc1                     Allocated with name '_putstrhex_sloc1_1_0'
                           3734 ;sloc2                     Allocated with name '_putstrhex_sloc2_1_0'
                           3735 ;sloc3                     Allocated with name '_putstrhex_sloc3_1_0'
                           3736 ;g                         Allocated with name '_putstrhex_g_1_1'
                           3737 ;count                     Allocated with name '_putstrhex_count_1_1'
                           3738 ;length                    Allocated with name '_putstrhex_length_1_1'
                           3739 ;------------------------------------------------------------
                           3740 ;	main.c:384: void putstrhex(unsigned int g)
                           3741 ;	-----------------------------------------
                           3742 ;	 function putstrhex
                           3743 ;	-----------------------------------------
   0CCF                    3744 _putstrhex:
                           3745 ;	genReceive
   0CCF AA 83              3746 	mov	r2,dph
   0CD1 E5 82              3747 	mov	a,dpl
   0CD3 90 0F E1           3748 	mov	dptr,#_putstrhex_g_1_1
   0CD6 F0                 3749 	movx	@dptr,a
   0CD7 A3                 3750 	inc	dptr
   0CD8 EA                 3751 	mov	a,r2
   0CD9 F0                 3752 	movx	@dptr,a
                           3753 ;	main.c:387: unsigned int length=0;
                           3754 ;	genAssign
   0CDA 90 0F E3           3755 	mov	dptr,#_putstrhex_length_1_1
   0CDD E4                 3756 	clr	a
   0CDE F0                 3757 	movx	@dptr,a
   0CDF A3                 3758 	inc	dptr
   0CE0 F0                 3759 	movx	@dptr,a
                           3760 ;	main.c:388: while(*buffer[g])
                           3761 ;	genAssign
   0CE1 90 0F E1           3762 	mov	dptr,#_putstrhex_g_1_1
   0CE4 E0                 3763 	movx	a,@dptr
   0CE5 FA                 3764 	mov	r2,a
   0CE6 A3                 3765 	inc	dptr
   0CE7 E0                 3766 	movx	a,@dptr
                           3767 ;	genLeftShift
                           3768 ;	genLeftShiftLiteral
                           3769 ;	genlshTwo
   0CE8 FB                 3770 	mov	r3,a
                           3771 ;	Peephole 105	removed redundant mov
   0CE9 CA                 3772 	xch	a,r2
   0CEA 25 E0              3773 	add	a,acc
   0CEC CA                 3774 	xch	a,r2
   0CED 33                 3775 	rlc	a
   0CEE FB                 3776 	mov	r3,a
                           3777 ;	genPlus
                           3778 ;	Peephole 236.g	used r2 instead of ar2
   0CEF EA                 3779 	mov	a,r2
   0CF0 24 C2              3780 	add	a,#_buffer
   0CF2 F5 0A              3781 	mov	_putstrhex_sloc3_1_0,a
                           3782 ;	Peephole 236.g	used r3 instead of ar3
   0CF4 EB                 3783 	mov	a,r3
   0CF5 34 0B              3784 	addc	a,#(_buffer >> 8)
   0CF7 F5 0B              3785 	mov	(_putstrhex_sloc3_1_0 + 1),a
                           3786 ;	genAssign
   0CF9 AE 0A              3787 	mov	r6,_putstrhex_sloc3_1_0
   0CFB AF 0B              3788 	mov	r7,(_putstrhex_sloc3_1_0 + 1)
   0CFD                    3789 00108$:
                           3790 ;	genPointerGet
                           3791 ;	genFarPointerGet
   0CFD 8E 82              3792 	mov	dpl,r6
   0CFF 8F 83              3793 	mov	dph,r7
   0D01 E0                 3794 	movx	a,@dptr
   0D02 F8                 3795 	mov	r0,a
   0D03 A3                 3796 	inc	dptr
   0D04 E0                 3797 	movx	a,@dptr
   0D05 F9                 3798 	mov	r1,a
                           3799 ;	genPointerGet
                           3800 ;	genFarPointerGet
   0D06 88 82              3801 	mov	dpl,r0
   0D08 89 83              3802 	mov	dph,r1
   0D0A E0                 3803 	movx	a,@dptr
                           3804 ;	genIfxJump
   0D0B 70 03              3805 	jnz	00119$
   0D0D 02 0E 36           3806 	ljmp	00110$
   0D10                    3807 00119$:
                           3808 ;	main.c:390: printf("0x%04x:  ",((unsigned int)buffer[g]));          /*Displaying address. hex value of 16 characters in a line*/
                           3809 ;	genIpush
   0D10 C0 06              3810 	push	ar6
   0D12 C0 07              3811 	push	ar7
                           3812 ;	genAssign
   0D14 88 06              3813 	mov	ar6,r0
   0D16 89 07              3814 	mov	ar7,r1
                           3815 ;	genCast
                           3816 ;	genIpush
   0D18 C0 02              3817 	push	ar2
   0D1A C0 03              3818 	push	ar3
   0D1C C0 06              3819 	push	ar6
   0D1E C0 07              3820 	push	ar7
   0D20 C0 06              3821 	push	ar6
   0D22 C0 07              3822 	push	ar7
                           3823 ;	genIpush
   0D24 74 59              3824 	mov	a,#__str_48
   0D26 C0 E0              3825 	push	acc
   0D28 74 25              3826 	mov	a,#(__str_48 >> 8)
   0D2A C0 E0              3827 	push	acc
   0D2C 74 80              3828 	mov	a,#0x80
   0D2E C0 E0              3829 	push	acc
                           3830 ;	genCall
   0D30 12 15 07           3831 	lcall	_printf
   0D33 E5 81              3832 	mov	a,sp
   0D35 24 FB              3833 	add	a,#0xfb
   0D37 F5 81              3834 	mov	sp,a
   0D39 D0 07              3835 	pop	ar7
   0D3B D0 06              3836 	pop	ar6
   0D3D D0 03              3837 	pop	ar3
   0D3F D0 02              3838 	pop	ar2
                           3839 ;	main.c:408: putstr("\n\r");
                           3840 ;	genIpop
   0D41 D0 07              3841 	pop	ar7
   0D43 D0 06              3842 	pop	ar6
                           3843 ;	main.c:391: for(count=1; ((count%17)!=0); count++)
                           3844 ;	genAssign
   0D45 90 0F E3           3845 	mov	dptr,#_putstrhex_length_1_1
   0D48 E0                 3846 	movx	a,@dptr
   0D49 F8                 3847 	mov	r0,a
   0D4A A3                 3848 	inc	dptr
   0D4B E0                 3849 	movx	a,@dptr
   0D4C F9                 3850 	mov	r1,a
                           3851 ;	genAssign
   0D4D 75 08 01           3852 	mov	_putstrhex_sloc1_1_0,#0x01
   0D50 E4                 3853 	clr	a
   0D51 F5 09              3854 	mov	(_putstrhex_sloc1_1_0 + 1),a
   0D53                    3855 00104$:
                           3856 ;	genIpush
   0D53 C0 06              3857 	push	ar6
   0D55 C0 07              3858 	push	ar7
                           3859 ;	genAssign
   0D57 90 10 0F           3860 	mov	dptr,#__moduint_PARM_2
   0D5A 74 11              3861 	mov	a,#0x11
   0D5C F0                 3862 	movx	@dptr,a
   0D5D E4                 3863 	clr	a
   0D5E A3                 3864 	inc	dptr
   0D5F F0                 3865 	movx	@dptr,a
                           3866 ;	genCall
   0D60 85 08 82           3867 	mov	dpl,_putstrhex_sloc1_1_0
   0D63 85 09 83           3868 	mov	dph,(_putstrhex_sloc1_1_0 + 1)
   0D66 C0 02              3869 	push	ar2
   0D68 C0 03              3870 	push	ar3
   0D6A C0 00              3871 	push	ar0
   0D6C C0 01              3872 	push	ar1
   0D6E 12 14 0D           3873 	lcall	__moduint
   0D71 AE 82              3874 	mov	r6,dpl
   0D73 AF 83              3875 	mov	r7,dph
   0D75 D0 01              3876 	pop	ar1
   0D77 D0 00              3877 	pop	ar0
   0D79 D0 03              3878 	pop	ar3
   0D7B D0 02              3879 	pop	ar2
                           3880 ;	genCmpEq
                           3881 ;	gencjne
                           3882 ;	gencjneshort
                           3883 ;	Peephole 241.c	optimized compare
   0D7D E4                 3884 	clr	a
   0D7E BE 00 04           3885 	cjne	r6,#0x00,00120$
   0D81 BF 00 01           3886 	cjne	r7,#0x00,00120$
   0D84 04                 3887 	inc	a
   0D85                    3888 00120$:
                           3889 ;	Peephole 300	removed redundant label 00121$
                           3890 ;	genIpop
   0D85 D0 07              3891 	pop	ar7
   0D87 D0 06              3892 	pop	ar6
                           3893 ;	genIfx
                           3894 ;	genIfxJump
   0D89 60 03              3895 	jz	00122$
   0D8B 02 0E 12           3896 	ljmp	00118$
   0D8E                    3897 00122$:
                           3898 ;	main.c:393: if(*buffer[g])
                           3899 ;	genIpush
   0D8E C0 06              3900 	push	ar6
   0D90 C0 07              3901 	push	ar7
                           3902 ;	genPointerGet
                           3903 ;	genFarPointerGet
   0D92 85 0A 82           3904 	mov	dpl,_putstrhex_sloc3_1_0
   0D95 85 0B 83           3905 	mov	dph,(_putstrhex_sloc3_1_0 + 1)
   0D98 E0                 3906 	movx	a,@dptr
   0D99 FE                 3907 	mov	r6,a
   0D9A A3                 3908 	inc	dptr
   0D9B E0                 3909 	movx	a,@dptr
   0D9C FF                 3910 	mov	r7,a
                           3911 ;	genPointerGet
                           3912 ;	genFarPointerGet
   0D9D 8E 82              3913 	mov	dpl,r6
   0D9F 8F 83              3914 	mov	dph,r7
   0DA1 E0                 3915 	movx	a,@dptr
   0DA2 FC                 3916 	mov	r4,a
                           3917 ;	genIpop
   0DA3 D0 07              3918 	pop	ar7
   0DA5 D0 06              3919 	pop	ar6
                           3920 ;	genIfx
   0DA7 EC                 3921 	mov	a,r4
                           3922 ;	genIfxJump
                           3923 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0DA8 60 68              3924 	jz	00118$
                           3925 ;	Peephole 300	removed redundant label 00123$
                           3926 ;	main.c:395: printf_tiny("%x ", *buffer[g]);
                           3927 ;	genIpush
   0DAA C0 06              3928 	push	ar6
   0DAC C0 07              3929 	push	ar7
                           3930 ;	genCast
   0DAE 8C 06              3931 	mov	ar6,r4
   0DB0 7F 00              3932 	mov	r7,#0x00
                           3933 ;	genIpush
   0DB2 C0 02              3934 	push	ar2
   0DB4 C0 03              3935 	push	ar3
   0DB6 C0 06              3936 	push	ar6
   0DB8 C0 07              3937 	push	ar7
   0DBA C0 00              3938 	push	ar0
   0DBC C0 01              3939 	push	ar1
   0DBE C0 06              3940 	push	ar6
   0DC0 C0 07              3941 	push	ar7
                           3942 ;	genIpush
   0DC2 74 63              3943 	mov	a,#__str_49
   0DC4 C0 E0              3944 	push	acc
   0DC6 74 25              3945 	mov	a,#(__str_49 >> 8)
   0DC8 C0 E0              3946 	push	acc
                           3947 ;	genCall
   0DCA 12 12 E5           3948 	lcall	_printf_tiny
   0DCD E5 81              3949 	mov	a,sp
   0DCF 24 FC              3950 	add	a,#0xfc
   0DD1 F5 81              3951 	mov	sp,a
   0DD3 D0 01              3952 	pop	ar1
   0DD5 D0 00              3953 	pop	ar0
   0DD7 D0 07              3954 	pop	ar7
   0DD9 D0 06              3955 	pop	ar6
   0DDB D0 03              3956 	pop	ar3
   0DDD D0 02              3957 	pop	ar2
                           3958 ;	main.c:396: buffer[g]++;
                           3959 ;	genPlus
                           3960 ;	Peephole 236.g	used r2 instead of ar2
   0DDF EA                 3961 	mov	a,r2
   0DE0 24 C2              3962 	add	a,#_buffer
   0DE2 FE                 3963 	mov	r6,a
                           3964 ;	Peephole 236.g	used r3 instead of ar3
   0DE3 EB                 3965 	mov	a,r3
   0DE4 34 0B              3966 	addc	a,#(_buffer >> 8)
   0DE6 FF                 3967 	mov	r7,a
                           3968 ;	genPointerGet
                           3969 ;	genFarPointerGet
   0DE7 8E 82              3970 	mov	dpl,r6
   0DE9 8F 83              3971 	mov	dph,r7
   0DEB E0                 3972 	movx	a,@dptr
   0DEC FC                 3973 	mov	r4,a
   0DED A3                 3974 	inc	dptr
   0DEE E0                 3975 	movx	a,@dptr
   0DEF FD                 3976 	mov	r5,a
                           3977 ;	genPlus
                           3978 ;     genPlusIncr
   0DF0 0C                 3979 	inc	r4
   0DF1 BC 00 01           3980 	cjne	r4,#0x00,00124$
   0DF4 0D                 3981 	inc	r5
   0DF5                    3982 00124$:
                           3983 ;	genPointerSet
                           3984 ;     genFarPointerSet
   0DF5 8E 82              3985 	mov	dpl,r6
   0DF7 8F 83              3986 	mov	dph,r7
   0DF9 EC                 3987 	mov	a,r4
   0DFA F0                 3988 	movx	@dptr,a
   0DFB A3                 3989 	inc	dptr
   0DFC ED                 3990 	mov	a,r5
   0DFD F0                 3991 	movx	@dptr,a
                           3992 ;	main.c:397: length++;
                           3993 ;	genPlus
                           3994 ;     genPlusIncr
   0DFE 08                 3995 	inc	r0
   0DFF B8 00 01           3996 	cjne	r0,#0x00,00125$
   0E02 09                 3997 	inc	r1
   0E03                    3998 00125$:
                           3999 ;	main.c:391: for(count=1; ((count%17)!=0); count++)
                           4000 ;	genPlus
                           4001 ;     genPlusIncr
   0E03 05 08              4002 	inc	_putstrhex_sloc1_1_0
   0E05 E4                 4003 	clr	a
   0E06 B5 08 02           4004 	cjne	a,_putstrhex_sloc1_1_0,00126$
   0E09 05 09              4005 	inc	(_putstrhex_sloc1_1_0 + 1)
   0E0B                    4006 00126$:
                           4007 ;	genIpop
   0E0B D0 07              4008 	pop	ar7
   0E0D D0 06              4009 	pop	ar6
   0E0F 02 0D 53           4010 	ljmp	00104$
   0E12                    4011 00118$:
                           4012 ;	genAssign
   0E12 90 0F E3           4013 	mov	dptr,#_putstrhex_length_1_1
   0E15 E8                 4014 	mov	a,r0
   0E16 F0                 4015 	movx	@dptr,a
   0E17 A3                 4016 	inc	dptr
   0E18 E9                 4017 	mov	a,r1
   0E19 F0                 4018 	movx	@dptr,a
                           4019 ;	main.c:404: putstr("\n\r");
                           4020 ;	genCall
                           4021 ;	Peephole 182.a	used 16 bit load of DPTR
   0E1A 90 1E 13           4022 	mov	dptr,#__str_3
   0E1D 75 F0 80           4023 	mov	b,#0x80
   0E20 C0 02              4024 	push	ar2
   0E22 C0 03              4025 	push	ar3
   0E24 C0 06              4026 	push	ar6
   0E26 C0 07              4027 	push	ar7
   0E28 12 0F 16           4028 	lcall	_putstr
   0E2B D0 07              4029 	pop	ar7
   0E2D D0 06              4030 	pop	ar6
   0E2F D0 03              4031 	pop	ar3
   0E31 D0 02              4032 	pop	ar2
   0E33 02 0C FD           4033 	ljmp	00108$
   0E36                    4034 00110$:
                           4035 ;	main.c:407: buffer[g]=buffer[g]-length;
                           4036 ;	genAssign
   0E36 90 0F E3           4037 	mov	dptr,#_putstrhex_length_1_1
   0E39 E0                 4038 	movx	a,@dptr
   0E3A FA                 4039 	mov	r2,a
   0E3B A3                 4040 	inc	dptr
   0E3C E0                 4041 	movx	a,@dptr
   0E3D FB                 4042 	mov	r3,a
                           4043 ;	genMinus
   0E3E E8                 4044 	mov	a,r0
   0E3F C3                 4045 	clr	c
                           4046 ;	Peephole 236.l	used r2 instead of ar2
   0E40 9A                 4047 	subb	a,r2
   0E41 F8                 4048 	mov	r0,a
   0E42 E9                 4049 	mov	a,r1
                           4050 ;	Peephole 236.l	used r3 instead of ar3
   0E43 9B                 4051 	subb	a,r3
   0E44 F9                 4052 	mov	r1,a
                           4053 ;	genPointerSet
                           4054 ;     genFarPointerSet
   0E45 8E 82              4055 	mov	dpl,r6
   0E47 8F 83              4056 	mov	dph,r7
   0E49 E8                 4057 	mov	a,r0
   0E4A F0                 4058 	movx	@dptr,a
   0E4B A3                 4059 	inc	dptr
   0E4C E9                 4060 	mov	a,r1
   0E4D F0                 4061 	movx	@dptr,a
                           4062 ;	main.c:408: putstr("\n\r");
                           4063 ;	genCall
                           4064 ;	Peephole 182.a	used 16 bit load of DPTR
   0E4E 90 1E 13           4065 	mov	dptr,#__str_3
   0E51 75 F0 80           4066 	mov	b,#0x80
                           4067 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0E54 02 0F 16           4068 	ljmp	_putstr
                           4069 ;
                           4070 ;------------------------------------------------------------
                           4071 ;Allocation info for local variables in function 'putstrbuff'
                           4072 ;------------------------------------------------------------
                           4073 ;ptr                       Allocated with name '_putstrbuff_ptr_1_1'
                           4074 ;count                     Allocated with name '_putstrbuff_count_1_1'
                           4075 ;------------------------------------------------------------
                           4076 ;	main.c:413: void putstrbuff(char *ptr)
                           4077 ;	-----------------------------------------
                           4078 ;	 function putstrbuff
                           4079 ;	-----------------------------------------
   0E57                    4080 _putstrbuff:
                           4081 ;	genReceive
   0E57 AA F0              4082 	mov	r2,b
   0E59 AB 83              4083 	mov	r3,dph
   0E5B E5 82              4084 	mov	a,dpl
   0E5D 90 0F E5           4085 	mov	dptr,#_putstrbuff_ptr_1_1
   0E60 F0                 4086 	movx	@dptr,a
   0E61 A3                 4087 	inc	dptr
   0E62 EB                 4088 	mov	a,r3
   0E63 F0                 4089 	movx	@dptr,a
   0E64 A3                 4090 	inc	dptr
   0E65 EA                 4091 	mov	a,r2
   0E66 F0                 4092 	movx	@dptr,a
                           4093 ;	main.c:415: unsigned int count=0;
                           4094 ;	genAssign
   0E67 90 0F E8           4095 	mov	dptr,#_putstrbuff_count_1_1
   0E6A E4                 4096 	clr	a
   0E6B F0                 4097 	movx	@dptr,a
   0E6C A3                 4098 	inc	dptr
   0E6D F0                 4099 	movx	@dptr,a
                           4100 ;	main.c:416: while(*ptr)
                           4101 ;	genAssign
   0E6E 90 0F E5           4102 	mov	dptr,#_putstrbuff_ptr_1_1
   0E71 E0                 4103 	movx	a,@dptr
   0E72 FA                 4104 	mov	r2,a
   0E73 A3                 4105 	inc	dptr
   0E74 E0                 4106 	movx	a,@dptr
   0E75 FB                 4107 	mov	r3,a
   0E76 A3                 4108 	inc	dptr
   0E77 E0                 4109 	movx	a,@dptr
   0E78 FC                 4110 	mov	r4,a
   0E79                    4111 00103$:
                           4112 ;	genPointerGet
                           4113 ;	genGenPointerGet
   0E79 8A 82              4114 	mov	dpl,r2
   0E7B 8B 83              4115 	mov	dph,r3
   0E7D 8C F0              4116 	mov	b,r4
   0E7F 12 1D A6           4117 	lcall	__gptrget
                           4118 ;	genIfxJump
   0E82 70 03              4119 	jnz	00112$
   0E84 02 0F 0A           4120 	ljmp	00111$
   0E87                    4121 00112$:
                           4122 ;	main.c:418: putchar(*ptr);
                           4123 ;	genPointerGet
                           4124 ;	genGenPointerGet
   0E87 8A 82              4125 	mov	dpl,r2
   0E89 8B 83              4126 	mov	dph,r3
   0E8B 8C F0              4127 	mov	b,r4
   0E8D 12 1D A6           4128 	lcall	__gptrget
                           4129 ;	genCast
   0E90 FD                 4130 	mov	r5,a
                           4131 ;	Peephole 105	removed redundant mov
   0E91 33                 4132 	rlc	a
   0E92 95 E0              4133 	subb	a,acc
   0E94 FE                 4134 	mov	r6,a
                           4135 ;	genCall
   0E95 8D 82              4136 	mov	dpl,r5
   0E97 8E 83              4137 	mov	dph,r6
   0E99 C0 02              4138 	push	ar2
   0E9B C0 03              4139 	push	ar3
   0E9D C0 04              4140 	push	ar4
   0E9F 12 0F 91           4141 	lcall	_putchar
   0EA2 D0 04              4142 	pop	ar4
   0EA4 D0 03              4143 	pop	ar3
   0EA6 D0 02              4144 	pop	ar2
                           4145 ;	main.c:419: count++;
                           4146 ;	genAssign
   0EA8 90 0F E8           4147 	mov	dptr,#_putstrbuff_count_1_1
   0EAB E0                 4148 	movx	a,@dptr
   0EAC FD                 4149 	mov	r5,a
   0EAD A3                 4150 	inc	dptr
   0EAE E0                 4151 	movx	a,@dptr
   0EAF FE                 4152 	mov	r6,a
                           4153 ;	genPlus
   0EB0 90 0F E8           4154 	mov	dptr,#_putstrbuff_count_1_1
                           4155 ;     genPlusIncr
   0EB3 74 01              4156 	mov	a,#0x01
                           4157 ;	Peephole 236.a	used r5 instead of ar5
   0EB5 2D                 4158 	add	a,r5
   0EB6 F0                 4159 	movx	@dptr,a
                           4160 ;	Peephole 181	changed mov to clr
   0EB7 E4                 4161 	clr	a
                           4162 ;	Peephole 236.b	used r6 instead of ar6
   0EB8 3E                 4163 	addc	a,r6
   0EB9 A3                 4164 	inc	dptr
   0EBA F0                 4165 	movx	@dptr,a
                           4166 ;	main.c:420: *ptr='\0';
                           4167 ;	genPointerSet
                           4168 ;	genGenPointerSet
   0EBB 8A 82              4169 	mov	dpl,r2
   0EBD 8B 83              4170 	mov	dph,r3
   0EBF 8C F0              4171 	mov	b,r4
                           4172 ;	Peephole 181	changed mov to clr
   0EC1 E4                 4173 	clr	a
   0EC2 12 12 CC           4174 	lcall	__gptrput
                           4175 ;	main.c:421: if (count>63)               /* 64 characters in  a line*/
                           4176 ;	genAssign
   0EC5 90 0F E8           4177 	mov	dptr,#_putstrbuff_count_1_1
   0EC8 E0                 4178 	movx	a,@dptr
   0EC9 FD                 4179 	mov	r5,a
   0ECA A3                 4180 	inc	dptr
   0ECB E0                 4181 	movx	a,@dptr
   0ECC FE                 4182 	mov	r6,a
                           4183 ;	genCmpGt
                           4184 ;	genCmp
   0ECD C3                 4185 	clr	c
   0ECE 74 3F              4186 	mov	a,#0x3F
   0ED0 9D                 4187 	subb	a,r5
                           4188 ;	Peephole 181	changed mov to clr
   0ED1 E4                 4189 	clr	a
   0ED2 9E                 4190 	subb	a,r6
                           4191 ;	genIfxJump
                           4192 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0ED3 50 22              4193 	jnc	00102$
                           4194 ;	Peephole 300	removed redundant label 00113$
                           4195 ;	main.c:422: {count=0;
                           4196 ;	genAssign
   0ED5 90 0F E8           4197 	mov	dptr,#_putstrbuff_count_1_1
   0ED8 E4                 4198 	clr	a
   0ED9 F0                 4199 	movx	@dptr,a
   0EDA A3                 4200 	inc	dptr
   0EDB F0                 4201 	movx	@dptr,a
                           4202 ;	main.c:423: printf_tiny("\n\r");}
                           4203 ;	genIpush
   0EDC C0 02              4204 	push	ar2
   0EDE C0 03              4205 	push	ar3
   0EE0 C0 04              4206 	push	ar4
   0EE2 74 13              4207 	mov	a,#__str_3
   0EE4 C0 E0              4208 	push	acc
   0EE6 74 1E              4209 	mov	a,#(__str_3 >> 8)
   0EE8 C0 E0              4210 	push	acc
                           4211 ;	genCall
   0EEA 12 12 E5           4212 	lcall	_printf_tiny
   0EED 15 81              4213 	dec	sp
   0EEF 15 81              4214 	dec	sp
   0EF1 D0 04              4215 	pop	ar4
   0EF3 D0 03              4216 	pop	ar3
   0EF5 D0 02              4217 	pop	ar2
   0EF7                    4218 00102$:
                           4219 ;	main.c:424: ptr++;
                           4220 ;	genPlus
                           4221 ;     genPlusIncr
   0EF7 0A                 4222 	inc	r2
   0EF8 BA 00 01           4223 	cjne	r2,#0x00,00114$
   0EFB 0B                 4224 	inc	r3
   0EFC                    4225 00114$:
                           4226 ;	genAssign
   0EFC 90 0F E5           4227 	mov	dptr,#_putstrbuff_ptr_1_1
   0EFF EA                 4228 	mov	a,r2
   0F00 F0                 4229 	movx	@dptr,a
   0F01 A3                 4230 	inc	dptr
   0F02 EB                 4231 	mov	a,r3
   0F03 F0                 4232 	movx	@dptr,a
   0F04 A3                 4233 	inc	dptr
   0F05 EC                 4234 	mov	a,r4
   0F06 F0                 4235 	movx	@dptr,a
   0F07 02 0E 79           4236 	ljmp	00103$
   0F0A                    4237 00111$:
                           4238 ;	genAssign
   0F0A 90 0F E5           4239 	mov	dptr,#_putstrbuff_ptr_1_1
   0F0D EA                 4240 	mov	a,r2
   0F0E F0                 4241 	movx	@dptr,a
   0F0F A3                 4242 	inc	dptr
   0F10 EB                 4243 	mov	a,r3
   0F11 F0                 4244 	movx	@dptr,a
   0F12 A3                 4245 	inc	dptr
   0F13 EC                 4246 	mov	a,r4
   0F14 F0                 4247 	movx	@dptr,a
                           4248 ;	Peephole 300	removed redundant label 00106$
   0F15 22                 4249 	ret
                           4250 ;------------------------------------------------------------
                           4251 ;Allocation info for local variables in function 'putstr'
                           4252 ;------------------------------------------------------------
                           4253 ;ptr                       Allocated with name '_putstr_ptr_1_1'
                           4254 ;------------------------------------------------------------
                           4255 ;	main.c:429: void putstr(char *ptr)
                           4256 ;	-----------------------------------------
                           4257 ;	 function putstr
                           4258 ;	-----------------------------------------
   0F16                    4259 _putstr:
                           4260 ;	genReceive
   0F16 AA F0              4261 	mov	r2,b
   0F18 AB 83              4262 	mov	r3,dph
   0F1A E5 82              4263 	mov	a,dpl
   0F1C 90 0F EA           4264 	mov	dptr,#_putstr_ptr_1_1
   0F1F F0                 4265 	movx	@dptr,a
   0F20 A3                 4266 	inc	dptr
   0F21 EB                 4267 	mov	a,r3
   0F22 F0                 4268 	movx	@dptr,a
   0F23 A3                 4269 	inc	dptr
   0F24 EA                 4270 	mov	a,r2
   0F25 F0                 4271 	movx	@dptr,a
                           4272 ;	main.c:431: while(*ptr)
                           4273 ;	genAssign
   0F26 90 0F EA           4274 	mov	dptr,#_putstr_ptr_1_1
   0F29 E0                 4275 	movx	a,@dptr
   0F2A FA                 4276 	mov	r2,a
   0F2B A3                 4277 	inc	dptr
   0F2C E0                 4278 	movx	a,@dptr
   0F2D FB                 4279 	mov	r3,a
   0F2E A3                 4280 	inc	dptr
   0F2F E0                 4281 	movx	a,@dptr
   0F30 FC                 4282 	mov	r4,a
   0F31                    4283 00101$:
                           4284 ;	genPointerGet
                           4285 ;	genGenPointerGet
   0F31 8A 82              4286 	mov	dpl,r2
   0F33 8B 83              4287 	mov	dph,r3
   0F35 8C F0              4288 	mov	b,r4
   0F37 12 1D A6           4289 	lcall	__gptrget
                           4290 ;	genIfx
   0F3A FD                 4291 	mov	r5,a
                           4292 ;	Peephole 105	removed redundant mov
                           4293 ;	genIfxJump
                           4294 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0F3B 60 2A              4295 	jz	00108$
                           4296 ;	Peephole 300	removed redundant label 00109$
                           4297 ;	main.c:433: putchar(*ptr);
                           4298 ;	genCast
   0F3D ED                 4299 	mov	a,r5
   0F3E 33                 4300 	rlc	a
   0F3F 95 E0              4301 	subb	a,acc
   0F41 FE                 4302 	mov	r6,a
                           4303 ;	genCall
   0F42 8D 82              4304 	mov	dpl,r5
   0F44 8E 83              4305 	mov	dph,r6
   0F46 C0 02              4306 	push	ar2
   0F48 C0 03              4307 	push	ar3
   0F4A C0 04              4308 	push	ar4
   0F4C 12 0F 91           4309 	lcall	_putchar
   0F4F D0 04              4310 	pop	ar4
   0F51 D0 03              4311 	pop	ar3
   0F53 D0 02              4312 	pop	ar2
                           4313 ;	main.c:434: ptr++;
                           4314 ;	genPlus
                           4315 ;     genPlusIncr
   0F55 0A                 4316 	inc	r2
   0F56 BA 00 01           4317 	cjne	r2,#0x00,00110$
   0F59 0B                 4318 	inc	r3
   0F5A                    4319 00110$:
                           4320 ;	genAssign
   0F5A 90 0F EA           4321 	mov	dptr,#_putstr_ptr_1_1
   0F5D EA                 4322 	mov	a,r2
   0F5E F0                 4323 	movx	@dptr,a
   0F5F A3                 4324 	inc	dptr
   0F60 EB                 4325 	mov	a,r3
   0F61 F0                 4326 	movx	@dptr,a
   0F62 A3                 4327 	inc	dptr
   0F63 EC                 4328 	mov	a,r4
   0F64 F0                 4329 	movx	@dptr,a
                           4330 ;	Peephole 112.b	changed ljmp to sjmp
   0F65 80 CA              4331 	sjmp	00101$
   0F67                    4332 00108$:
                           4333 ;	genAssign
   0F67 90 0F EA           4334 	mov	dptr,#_putstr_ptr_1_1
   0F6A EA                 4335 	mov	a,r2
   0F6B F0                 4336 	movx	@dptr,a
   0F6C A3                 4337 	inc	dptr
   0F6D EB                 4338 	mov	a,r3
   0F6E F0                 4339 	movx	@dptr,a
   0F6F A3                 4340 	inc	dptr
   0F70 EC                 4341 	mov	a,r4
   0F71 F0                 4342 	movx	@dptr,a
                           4343 ;	Peephole 300	removed redundant label 00104$
   0F72 22                 4344 	ret
                           4345 ;------------------------------------------------------------
                           4346 ;Allocation info for local variables in function 'SerialInitialize'
                           4347 ;------------------------------------------------------------
                           4348 ;------------------------------------------------------------
                           4349 ;	main.c:439: void SerialInitialize()
                           4350 ;	-----------------------------------------
                           4351 ;	 function SerialInitialize
                           4352 ;	-----------------------------------------
   0F73                    4353 _SerialInitialize:
                           4354 ;	main.c:441: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
                           4355 ;	genAssign
   0F73 75 89 20           4356 	mov	_TMOD,#0x20
                           4357 ;	main.c:442: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
                           4358 ;	genAssign
   0F76 75 8D FD           4359 	mov	_TH1,#0xFD
                           4360 ;	main.c:443: SCON=0x50;
                           4361 ;	genAssign
   0F79 75 98 50           4362 	mov	_SCON,#0x50
                           4363 ;	main.c:444: TR1=1;  // to start timer
                           4364 ;	genAssign
   0F7C D2 8E              4365 	setb	_TR1
                           4366 ;	main.c:445: IE=0x90; // to make serial interrupt interrupt enable
                           4367 ;	genAssign
   0F7E 75 A8 90           4368 	mov	_IE,#0x90
                           4369 ;	Peephole 300	removed redundant label 00101$
   0F81 22                 4370 	ret
                           4371 ;------------------------------------------------------------
                           4372 ;Allocation info for local variables in function 'getchar'
                           4373 ;------------------------------------------------------------
                           4374 ;------------------------------------------------------------
                           4375 ;	main.c:449: char getchar()
                           4376 ;	-----------------------------------------
                           4377 ;	 function getchar
                           4378 ;	-----------------------------------------
   0F82                    4379 _getchar:
                           4380 ;	main.c:452: while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
   0F82                    4381 00101$:
                           4382 ;	genIfx
                           4383 ;	genIfxJump
                           4384 ;	Peephole 108.d	removed ljmp by inverse jump logic
   0F82 30 98 FD           4385 	jnb	_RI,00101$
                           4386 ;	Peephole 300	removed redundant label 00108$
                           4387 ;	main.c:453: rec=SBUF;   // data is received in SBUF register present in controller during receiving
                           4388 ;	genAssign
   0F85 AA 99              4389 	mov	r2,_SBUF
                           4390 ;	genAssign
   0F87 90 0F CE           4391 	mov	dptr,#_rec
   0F8A EA                 4392 	mov	a,r2
   0F8B F0                 4393 	movx	@dptr,a
                           4394 ;	main.c:454: RI=0;  // make RI bit to 0 so that next data is received
                           4395 ;	genAssign
   0F8C C2 98              4396 	clr	_RI
                           4397 ;	main.c:455: return rec;  // return rec where function is called
                           4398 ;	genRet
   0F8E 8A 82              4399 	mov	dpl,r2
                           4400 ;	Peephole 300	removed redundant label 00104$
   0F90 22                 4401 	ret
                           4402 ;------------------------------------------------------------
                           4403 ;Allocation info for local variables in function 'putchar'
                           4404 ;------------------------------------------------------------
                           4405 ;x                         Allocated with name '_putchar_x_1_1'
                           4406 ;------------------------------------------------------------
                           4407 ;	main.c:458: void putchar(int x)
                           4408 ;	-----------------------------------------
                           4409 ;	 function putchar
                           4410 ;	-----------------------------------------
   0F91                    4411 _putchar:
                           4412 ;	genReceive
   0F91 AA 83              4413 	mov	r2,dph
   0F93 E5 82              4414 	mov	a,dpl
   0F95 90 0F ED           4415 	mov	dptr,#_putchar_x_1_1
   0F98 F0                 4416 	movx	@dptr,a
   0F99 A3                 4417 	inc	dptr
   0F9A EA                 4418 	mov	a,r2
   0F9B F0                 4419 	movx	@dptr,a
                           4420 ;	main.c:460: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
                           4421 ;	genAssign
   0F9C 90 0F ED           4422 	mov	dptr,#_putchar_x_1_1
   0F9F E0                 4423 	movx	a,@dptr
   0FA0 FA                 4424 	mov	r2,a
   0FA1 A3                 4425 	inc	dptr
   0FA2 E0                 4426 	movx	a,@dptr
   0FA3 FB                 4427 	mov	r3,a
                           4428 ;	genCast
   0FA4 8A 99              4429 	mov	_SBUF,r2
                           4430 ;	main.c:461: while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
   0FA6                    4431 00101$:
                           4432 ;	genIfx
                           4433 ;	genIfxJump
                           4434 ;	Peephole 108.d	removed ljmp by inverse jump logic
                           4435 ;	main.c:462: TI=0; // make TI bit to zero for next transmission
                           4436 ;	genAssign
                           4437 ;	Peephole 250.a	using atomic test and clear
   0FA6 10 99 02           4438 	jbc	_TI,00108$
   0FA9 80 FB              4439 	sjmp	00101$
   0FAB                    4440 00108$:
                           4441 ;	Peephole 300	removed redundant label 00104$
   0FAB 22                 4442 	ret
                           4443 	.area CSEG    (CODE)
                           4444 	.area CONST   (CODE)
   1DDF                    4445 __str_0:
   1DDF 0A                 4446 	.db 0x0A
   1DE0 0D                 4447 	.db 0x0D
   1DE1 45 6E 74 65 72 20  4448 	.ascii "Enter the character"
        74 68 65 20 63 68
        61 72 61 63 74 65
        72
   1DF4 0A                 4449 	.db 0x0A
   1DF5 0D                 4450 	.db 0x0D
   1DF6 00                 4451 	.db 0x00
   1DF7                    4452 __str_1:
   1DF7 50 72 65 73 73 20  4453 	.ascii "Press '>' for help menu"
        27 3E 27 20 66 6F
        72 20 68 65 6C 70
        20 6D 65 6E 75
   1E0E 0A                 4454 	.db 0x0A
   1E0F 0D                 4455 	.db 0x0D
   1E10 00                 4456 	.db 0x00
   1E11                    4457 __str_2:
   1E11 20                 4458 	.ascii " "
   1E12 00                 4459 	.db 0x00
   1E13                    4460 __str_3:
   1E13 0A                 4461 	.db 0x0A
   1E14 0D                 4462 	.db 0x0D
   1E15 00                 4463 	.db 0x00
   1E16                    4464 __str_4:
   1E16 49 6E 70 75 74 20  4465 	.ascii "Input buffer size between 32 and 2800 bytes, divisible by 16"
        62 75 66 66 65 72
        20 73 69 7A 65 20
        62 65 74 77 65 65
        6E 20 33 32 20 61
        6E 64 20 32 38 30
        30 20 62 79 74 65
        73 2C 20 64 69 76
        69 73 69 62 6C 65
        20 62 79 20 31 36
   1E52 0A                 4466 	.db 0x0A
   1E53 0D                 4467 	.db 0x0D
   1E54 00                 4468 	.db 0x00
   1E55                    4469 __str_5:
   1E55 6D 61 6C 6C 6F 63  4470 	.ascii "malloc buffer0 failed"
        20 62 75 66 66 65
        72 30 20 66 61 69
        6C 65 64
   1E6A 0A                 4471 	.db 0x0A
   1E6B 0D                 4472 	.db 0x0D
   1E6C 00                 4473 	.db 0x00
   1E6D                    4474 __str_6:
   1E6D 6D 61 6C 6C 6F 63  4475 	.ascii "malloc buffer1 failed"
        20 62 75 66 66 65
        72 31 20 66 61 69
        6C 65 64
   1E82 0A                 4476 	.db 0x0A
   1E83 0D                 4477 	.db 0x0D
   1E84 00                 4478 	.db 0x00
   1E85                    4479 __str_7:
   1E85 45 6E 74 65 72 20  4480 	.ascii "Enter smaller size for buffer"
        73 6D 61 6C 6C 65
        72 20 73 69 7A 65
        20 66 6F 72 20 62
        75 66 66 65 72
   1EA2 0A                 4481 	.db 0x0A
   1EA3 0D                 4482 	.db 0x0D
   1EA4 00                 4483 	.db 0x00
   1EA5                    4484 __str_8:
   1EA5 0A                 4485 	.db 0x0A
   1EA6 0D                 4486 	.db 0x0D
   1EA7 45 6E 74 65 72 20  4487 	.ascii "Enter the correct buffer size value divisible by 16"
        74 68 65 20 63 6F
        72 72 65 63 74 20
        62 75 66 66 65 72
        20 73 69 7A 65 20
        76 61 6C 75 65 20
        64 69 76 69 73 69
        62 6C 65 20 62 79
        20 31 36
   1EDA 0A                 4488 	.db 0x0A
   1EDB 0D                 4489 	.db 0x0D
   1EDC 00                 4490 	.db 0x00
   1EDD                    4491 __str_9:
   1EDD 0A                 4492 	.db 0x0A
   1EDE 0D                 4493 	.db 0x0D
   1EDF 45 6E 74 65 72 20  4494 	.ascii "Enter the correct buffer size between 32 and 2800"
        74 68 65 20 63 6F
        72 72 65 63 74 20
        62 75 66 66 65 72
        20 73 69 7A 65 20
        62 65 74 77 65 65
        6E 20 33 32 20 61
        6E 64 20 32 38 30
        30
   1F10 0A                 4495 	.db 0x0A
   1F11 0D                 4496 	.db 0x0D
   1F12 00                 4497 	.db 0x00
   1F13                    4498 __str_10:
   1F13 0A                 4499 	.db 0x0A
   1F14 0D                 4500 	.db 0x0D
   1F15 42 75 66 66 65 72  4501 	.ascii "Buffer allocation successful"
        20 61 6C 6C 6F 63
        61 74 69 6F 6E 20
        73 75 63 63 65 73
        73 66 75 6C
   1F31 0A                 4502 	.db 0x0A
   1F32 0D                 4503 	.db 0x0D
   1F33 00                 4504 	.db 0x00
   1F34                    4505 __str_11:
   1F34 0A                 4506 	.db 0x0A
   1F35 0D                 4507 	.db 0x0D
   1F36 45 6E 74 65 72 20  4508 	.ascii "Enter Numbers"
        4E 75 6D 62 65 72
        73
   1F43 0A                 4509 	.db 0x0A
   1F44 0D                 4510 	.db 0x0D
   1F45 00                 4511 	.db 0x00
   1F46                    4512 __str_12:
   1F46 0A                 4513 	.db 0x0A
   1F47 0D                 4514 	.db 0x0D
   1F48 45 6E 74 65 72 20  4515 	.ascii "Enter the buffer size between 20 and 400"
        74 68 65 20 62 75
        66 66 65 72 20 73
        69 7A 65 20 62 65
        74 77 65 65 6E 20
        32 30 20 61 6E 64
        20 34 30 30
   1F70 0A                 4516 	.db 0x0A
   1F71 0D                 4517 	.db 0x0D
   1F72 00                 4518 	.db 0x00
   1F73                    4519 __str_13:
   1F73 0A                 4520 	.db 0x0A
   1F74 0D                 4521 	.db 0x0D
   1F75 42 75 66 66 65 72  4522 	.ascii "Buffer allocation failed"
        20 61 6C 6C 6F 63
        61 74 69 6F 6E 20
        66 61 69 6C 65 64
   1F8D 0A                 4523 	.db 0x0A
   1F8E 0D                 4524 	.db 0x0D
   1F8F 00                 4525 	.db 0x00
   1F90                    4526 __str_14:
   1F90 0A                 4527 	.db 0x0A
   1F91 0D                 4528 	.db 0x0D
   1F92 42 75 66 66 65 72  4529 	.ascii "Buffer Allocated"
        20 41 6C 6C 6F 63
        61 74 65 64
   1FA2 0A                 4530 	.db 0x0A
   1FA3 0D                 4531 	.db 0x0D
   1FA4 00                 4532 	.db 0x00
   1FA5                    4533 __str_15:
   1FA5 42 75 66 66 65 72  4534 	.ascii "Buffer address and buffer number 0x%x, %d"
        20 61 64 64 72 65
        73 73 20 61 6E 64
        20 62 75 66 66 65
        72 20 6E 75 6D 62
        65 72 20 30 78 25
        78 2C 20 25 64
   1FCE 0A                 4535 	.db 0x0A
   1FCF 0D                 4536 	.db 0x0D
   1FD0 00                 4537 	.db 0x00
   1FD1                    4538 __str_16:
   1FD1 0A                 4539 	.db 0x0A
   1FD2 0D                 4540 	.db 0x0D
   1FD3 4E 6F 74 20 61 20  4541 	.ascii "Not a valid buffer size"
        76 61 6C 69 64 20
        62 75 66 66 65 72
        20 73 69 7A 65
   1FEA 0A                 4542 	.db 0x0A
   1FEB 0D                 4543 	.db 0x0D
   1FEC 00                 4544 	.db 0x00
   1FED                    4545 __str_17:
   1FED 0A                 4546 	.db 0x0A
   1FEE 0D                 4547 	.db 0x0D
   1FEF 7E 7E 7E 7E 7E 7E  4548 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HELP MENU~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 48 45 4C
        50 20 4D 45 4E 55
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E
   2029 7E 7E 7E 7E 7E 7E  4549 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E
   2046 0A                 4550 	.db 0x0A
   2047 0D                 4551 	.db 0x0D
   2048 00                 4552 	.db 0x00
   2049                    4553 __str_18:
   2049 0A                 4554 	.db 0x0A
   204A 0D                 4555 	.db 0x0D
   204B 20 27 2B 27 20 41  4556 	.ascii " '+' Allocate new buffer"
        6C 6C 6F 63 61 74
        65 20 6E 65 77 20
        62 75 66 66 65 72
   2063 0A                 4557 	.db 0x0A
   2064 0D                 4558 	.db 0x0D
   2065 00                 4559 	.db 0x00
   2066                    4560 __str_19:
   2066 20 27 2D 27 20 44  4561 	.ascii " '-' Delete a buffer"
        65 6C 65 74 65 20
        61 20 62 75 66 66
        65 72
   207A 0A                 4562 	.db 0x0A
   207B 0D                 4563 	.db 0x0D
   207C 00                 4564 	.db 0x00
   207D                    4565 __str_20:
   207D 20 27 40 27 20 53  4566 	.ascii " '@' Start over from allocating heap"
        74 61 72 74 20 6F
        76 65 72 20 66 72
        6F 6D 20 61 6C 6C
        6F 63 61 74 69 6E
        67 20 68 65 61 70
   20A1 0A                 4567 	.db 0x0A
   20A2 0D                 4568 	.db 0x0D
   20A3 00                 4569 	.db 0x00
   20A4                    4570 __str_21:
   20A4 20 27 3F 27 20 44  4571 	.ascii " '?' Display all buffer details and buffer0, buffer1 content"
        69 73 70 6C 61 79
        20 61 6C 6C 20 62
        75 66 66 65 72 20
        64 65 74 61 69 6C
        73 20 61 6E 64 20
        62 75 66 66 65 72
        30 2C 20 62 75 66
        66 65 72 31 20 63
        6F 6E 74 65 6E 74
   20E0 73 20 61 6E 64 20  4572 	.ascii "s and erase it "
        65 72 61 73 65 20
        69 74 20
   20EF 0A                 4573 	.db 0x0A
   20F0 0D                 4574 	.db 0x0D
   20F1 00                 4575 	.db 0x00
   20F2                    4576 __str_22:
   20F2 20 27 3D 27 20 44  4577 	.ascii " '=' Display the contents of the buffer"
        69 73 70 6C 61 79
        20 74 68 65 20 63
        6F 6E 74 65 6E 74
        73 20 6F 66 20 74
        68 65 20 62 75 66
        66 65 72
   2119 0A                 4578 	.db 0x0A
   211A 0D                 4579 	.db 0x0D
   211B 00                 4580 	.db 0x00
   211C                    4581 __str_23:
   211C 20 45 6E 74 65 72  4582 	.ascii " Enter Capital alphabets to fill buffer0"
        20 43 61 70 69 74
        61 6C 20 61 6C 70
        68 61 62 65 74 73
        20 74 6F 20 66 69
        6C 6C 20 62 75 66
        66 65 72 30
   2144 0A                 4583 	.db 0x0A
   2145 0D                 4584 	.db 0x0D
   2146 00                 4585 	.db 0x00
   2147                    4586 __str_24:
   2147 20 45 6E 74 65 72  4587 	.ascii " Enter Numerical values to fill buffer1"
        20 4E 75 6D 65 72
        69 63 61 6C 20 76
        61 6C 75 65 73 20
        74 6F 20 66 69 6C
        6C 20 62 75 66 66
        65 72 31
   216E 0A                 4588 	.db 0x0A
   216F 0D                 4589 	.db 0x0D
   2170 00                 4590 	.db 0x00
   2171                    4591 __str_25:
   2171 0A                 4592 	.db 0x0A
   2172 0D                 4593 	.db 0x0D
   2173 0A                 4594 	.db 0x0A
   2174 0D                 4595 	.db 0x0D
   2175 20 45 6E 74 65 72  4596 	.ascii " Enter the option or Character"
        20 74 68 65 20 6F
        70 74 69 6F 6E 20
        6F 72 20 43 68 61
        72 61 63 74 65 72
   2193 0A                 4597 	.db 0x0A
   2194 0D                 4598 	.db 0x0D
   2195 00                 4599 	.db 0x00
   2196                    4600 __str_26:
   2196 0A                 4601 	.db 0x0A
   2197 0D                 4602 	.db 0x0D
   2198 45 6E 74 65 72 20  4603 	.ascii "Enter the buffer number which has to be deleted"
        74 68 65 20 62 75
        66 66 65 72 20 6E
        75 6D 62 65 72 20
        77 68 69 63 68 20
        68 61 73 20 74 6F
        20 62 65 20 64 65
        6C 65 74 65 64
   21C7 0A                 4604 	.db 0x0A
   21C8 0D                 4605 	.db 0x0D
   21C9 00                 4606 	.db 0x00
   21CA                    4607 __str_27:
   21CA 45 6E 74 65 72 20  4608 	.ascii "Enter valid buffer number next time"
        76 61 6C 69 64 20
        62 75 66 66 65 72
        20 6E 75 6D 62 65
        72 20 6E 65 78 74
        20 74 69 6D 65
   21ED 0A                 4609 	.db 0x0A
   21EE 0D                 4610 	.db 0x0D
   21EF 00                 4611 	.db 0x00
   21F0                    4612 __str_28:
   21F0 42 75 66 66 65 72  4613 	.ascii "Buffer deleted"
        20 64 65 6C 65 74
        65 64
   21FE 0A                 4614 	.db 0x0A
   21FF 0D                 4615 	.db 0x0D
   2200 00                 4616 	.db 0x00
   2201                    4617 __str_29:
   2201 44 65 6C 65 74 65  4618 	.ascii "Deleted %d buffer"
        64 20 25 64 20 62
        75 66 66 65 72
   2212 0A                 4619 	.db 0x0A
   2213 0D                 4620 	.db 0x0D
   2214 00                 4621 	.db 0x00
   2215                    4622 __str_30:
   2215 0A                 4623 	.db 0x0A
   2216 0D                 4624 	.db 0x0D
   2217 7E 7E 7E 7E 7E 7E  4625 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYING DETAILS~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 44 49 53 50 4C
        41 59 49 4E 47 20
        44 45 54 41 49 4C
        53 7E 7E 7E 7E 7E
        7E 7E 7E 7E
   2251 7E 7E 7E 7E 7E 7E  4626 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E
   226E 0A                 4627 	.db 0x0A
   226F 0D                 4628 	.db 0x0D
   2270 00                 4629 	.db 0x00
   2271                    4630 __str_31:
   2271 7E 7E 7E 7E 7E 7E  4631 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
   22AD 7E 7E 7E 7E 7E 7E  4632 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E
   22C8 0A                 4633 	.db 0x0A
   22C9 0D                 4634 	.db 0x0D
   22CA 00                 4635 	.db 0x00
   22CB                    4636 __str_32:
   22CB 0A                 4637 	.db 0x0A
   22CC 0D                 4638 	.db 0x0D
   22CD 54 6F 74 61 6C 20  4639 	.ascii "Total Number of Buffer = %d"
        4E 75 6D 62 65 72
        20 6F 66 20 42 75
        66 66 65 72 20 3D
        20 25 64
   22E8 0A                 4640 	.db 0x0A
   22E9 0D                 4641 	.db 0x0D
   22EA 00                 4642 	.db 0x00
   22EB                    4643 __str_33:
   22EB 0A                 4644 	.db 0x0A
   22EC 0D                 4645 	.db 0x0D
   22ED 54 6F 74 61 6C 20  4646 	.ascii "Total number of characters received = %d"
        6E 75 6D 62 65 72
        20 6F 66 20 63 68
        61 72 61 63 74 65
        72 73 20 72 65 63
        65 69 76 65 64 20
        3D 20 25 64
   2315 0A                 4647 	.db 0x0A
   2316 0D                 4648 	.db 0x0D
   2317 00                 4649 	.db 0x00
   2318                    4650 __str_34:
   2318 54 6F 74 61 6C 20  4651 	.ascii "Total number of storage characters received = %d"
        6E 75 6D 62 65 72
        20 6F 66 20 73 74
        6F 72 61 67 65 20
        63 68 61 72 61 63
        74 65 72 73 20 72
        65 63 65 69 76 65
        64 20 3D 20 25 64
   2348 0A                 4652 	.db 0x0A
   2349 0D                 4653 	.db 0x0D
   234A 00                 4654 	.db 0x00
   234B                    4655 __str_35:
   234B 54 6F 74 61 6C 20  4656 	.ascii "Total number of characters received since last '?' = %d"
        6E 75 6D 62 65 72
        20 6F 66 20 63 68
        61 72 61 63 74 65
        72 73 20 72 65 63
        65 69 76 65 64 20
        73 69 6E 63 65 20
        6C 61 73 74 20 27
        3F 27 20 3D 20 25
        64
   2382 0A                 4657 	.db 0x0A
   2383 0D                 4658 	.db 0x0D
   2384 00                 4659 	.db 0x00
   2385                    4660 __str_36:
   2385 0A                 4661 	.db 0x0A
   2386 0D                 4662 	.db 0x0D
   2387 7E 7E 7E 7E 7E 7E  4663 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E
   23C1 7E 7E 7E 7E 7E 7E  4664 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E
   23DE 0A                 4665 	.db 0x0A
   23DF 0D                 4666 	.db 0x0D
   23E0 00                 4667 	.db 0x00
   23E1                    4668 __str_37:
   23E1 0A                 4669 	.db 0x0A
   23E2 0D                 4670 	.db 0x0D
   23E3 4E 75 6D 62 65 72  4671 	.ascii "Number of characters in buffer0 = %d"
        20 6F 66 20 63 68
        61 72 61 63 74 65
        72 73 20 69 6E 20
        62 75 66 66 65 72
        30 20 3D 20 25 64
   2407 0A                 4672 	.db 0x0A
   2408 0D                 4673 	.db 0x0D
   2409 00                 4674 	.db 0x00
   240A                    4675 __str_38:
   240A 4E 75 6D 62 65 72  4676 	.ascii "Number of free spaces in buffer0 = %d"
        20 6F 66 20 66 72
        65 65 20 73 70 61
        63 65 73 20 69 6E
        20 62 75 66 66 65
        72 30 20 3D 20 25
        64
   242F 0A                 4677 	.db 0x0A
   2430 0D                 4678 	.db 0x0D
   2431 00                 4679 	.db 0x00
   2432                    4680 __str_39:
   2432 4E 75 6D 62 65 72  4681 	.ascii "Number of characters in buffer1 = %d"
        20 6F 66 20 63 68
        61 72 61 63 74 65
        72 73 20 69 6E 20
        62 75 66 66 65 72
        31 20 3D 20 25 64
   2456 0A                 4682 	.db 0x0A
   2457 0D                 4683 	.db 0x0D
   2458 00                 4684 	.db 0x00
   2459                    4685 __str_40:
   2459 42 75 66 66 65 72  4686 	.ascii "Buffer %d Information"
        20 25 64 20 49 6E
        66 6F 72 6D 61 74
        69 6F 6E
   246E 0A                 4687 	.db 0x0A
   246F 0D                 4688 	.db 0x0D
   2470 00                 4689 	.db 0x00
   2471                    4690 __str_41:
   2471 53 74 61 72 74 69  4691 	.ascii "Starting address = 0x%x"
        6E 67 20 61 64 64
        72 65 73 73 20 3D
        20 30 78 25 78
   2488 0A                 4692 	.db 0x0A
   2489 0D                 4693 	.db 0x0D
   248A 00                 4694 	.db 0x00
   248B                    4695 __str_42:
   248B 45 6E 64 69 6E 67  4696 	.ascii "Ending address = 0x%x"
        20 61 64 64 72 65
        73 73 20 3D 20 30
        78 25 78
   24A0 0A                 4697 	.db 0x0A
   24A1 0D                 4698 	.db 0x0D
   24A2 00                 4699 	.db 0x00
   24A3                    4700 __str_43:
   24A3 41 6C 6C 6F 63 61  4701 	.ascii "Allocated size = %d"
        74 65 64 20 73 69
        7A 65 20 3D 20 25
        64
   24B6 0A                 4702 	.db 0x0A
   24B7 0D                 4703 	.db 0x0D
   24B8 00                 4704 	.db 0x00
   24B9                    4705 __str_44:
   24B9 0A                 4706 	.db 0x0A
   24BA 0D                 4707 	.db 0x0D
   24BB 43 6F 6E 74 65 6E  4708 	.ascii "Contents of Buffer0"
        74 73 20 6F 66 20
        42 75 66 66 65 72
        30
   24CE 0A                 4709 	.db 0x0A
   24CF 0D                 4710 	.db 0x0D
   24D0 00                 4711 	.db 0x00
   24D1                    4712 __str_45:
   24D1 0A                 4713 	.db 0x0A
   24D2 0D                 4714 	.db 0x0D
   24D3 43 6F 6E 74 65 6E  4715 	.ascii "Contents of Buffer1"
        74 73 20 6F 66 20
        42 75 66 66 65 72
        31
   24E6 0A                 4716 	.db 0x0A
   24E7 0D                 4717 	.db 0x0D
   24E8 00                 4718 	.db 0x00
   24E9                    4719 __str_46:
   24E9 7E 7E 7E 7E 7E 7E  4720 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYED ALL DETAILS~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 44 49 53 50
        4C 41 59 45 44 20
        41 4C 4C 20 44 45
        54 41 49 4C 53 7E
        7E 7E 7E 7E 7E 7E
   2525 7E 7E 7E 7E 7E 7E  4721 	.ascii "~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E 7E 7E 7E
        7E 7E 7E
   2540 0A                 4722 	.db 0x0A
   2541 0D                 4723 	.db 0x0D
   2542 00                 4724 	.db 0x00
   2543                    4725 __str_47:
   2543 43 6F 6E 74 65 6E  4726 	.ascii "Contents of Buffer1"
        74 73 20 6F 66 20
        42 75 66 66 65 72
        31
   2556 0A                 4727 	.db 0x0A
   2557 0D                 4728 	.db 0x0D
   2558 00                 4729 	.db 0x00
   2559                    4730 __str_48:
   2559 30 78 25 30 34 78  4731 	.ascii "0x%04x:  "
        3A 20 20
   2562 00                 4732 	.db 0x00
   2563                    4733 __str_49:
   2563 25 78 20           4734 	.ascii "%x "
   2566 00                 4735 	.db 0x00
                           4736 	.area XINIT   (CODE)
   2572                    4737 __xinit__i:
   2572 00                 4738 	.db #0x00
