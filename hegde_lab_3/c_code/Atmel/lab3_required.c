/*

UNIVERSITY OF COLORADO BOULDER

Author: Kiran Narayana Hegde (kihe6592@colorado.edu)

C Program is submitted as Lab3 required element submission for ECEN_5613
The program mainly deals with buffer allocation, filling and removing character from buffers and
displaying the details.
*/


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


 /*Including all the header files needed */
#include <at89c51ed2.h>
#include <mcs51reg.h>
#include <mcs51/8051.h>
#include <malloc.h>
#include<stdio.h>

#define HEAP_SIZE 3000   // size must be smaller than available XRAM

#undef DEBUG

#ifdef DEBUG
#define DEBUGPORT(x) (*(xdata unsigned char *)0xFFFF)=x;        /*debug port macro function*/
#else
#define DEBUGPORT(x)
#endif

unsigned char xdata heap[HEAP_SIZE];

char getchar();     // initialize controller to work as serial communication
void SerialInitialize();  // receive data to controller via rxd pin to computer,s hyper terminal
void putchar(int x);  // transmit data to computer,s hyper terminal from controller via txd pin
void putstr(char *);
void putstrbuff(char *);


_sdcc_external_startup()
{
    AUXR |= 0x0C;
    return 0;
}

/* Declaring all the global variables and function declaration */
static unsigned int buffer_count, stor_count, char_count;
static unsigned int buffer_number, lastchar_count;
xdata unsigned char * buffer[256];
unsigned char *buff1ptr, *buff2ptr, *bptr0, *bptr1;
unsigned int bufflen[256];
unsigned char rec;  // gloable variable
unsigned char temp, buff_val, j, i=0;
unsigned int g;
char *ptr;
void delay(int x);
void fillbuffer0(unsigned char);
void fillbuffer1(unsigned char);
void newbuffer();
void deletebuffer();
int ReadValue();
void displaydelete();
unsigned int value, res;
void BufferInitialize();
void display();
void putstrhex(unsigned int );
void freeEverything();
void help_menu();


void main()
{

    SerialInitialize();  /*  call the serial initialize function */

    init_dynamic_memory((MEMHEADER xdata *)heap, HEAP_SIZE); /* Heap allocation */
here: lastchar_count=0; /*Number of characters since last '?' */
    char_count=0; /*Total number of characters received */
    stor_count=0; /* Total number of storage characters received */
    buffer_count=2; /*buffer count to allocate new buffers */
    buffer_number=0; /*Total number of buffers active */

    BufferInitialize(); /*Buffer initilisation */

putstr("\n\rEnter the character\n\r");
    while(1)
    {
        temp = getchar(); /* getting character from user input */
        putchar(temp);  /* put character back on the screen */
        putstr(" ");
        if (temp>='A' && temp<='Z') /* Check if storage character*/
        {
            stor_count++;
            char_count++;  /* for character count */
            fillbuffer0(temp); /* Send data to the buffer*/
        }
        else if (temp>='0' && temp<='9')
        {
            stor_count++;
            char_count++;
            fillbuffer1(temp);
        }
        else if (temp=='+')
        {
            newbuffer();  /* allocate new buffer */
            char_count++;
        }
        else if (temp=='-')
        {
            char_count++;
            deletebuffer();   /* delete buffer*/
        }
        else if (temp=='?')
        {
            char_count++;
            displaydelete(); /*display buffer content, detailed report and empty buffer */
        }
        else if(temp=='=')
        {
                char_count++;
                display(); /* just display the buffer content in a nice table structure */
        }
        else if(temp=='>')
        {
                char_count++;
                help_menu();  /* de-allocate every buffer*/
        }
        else if(temp=='@')
        {
                char_count++;
                freeEverything();  /* de-allocate every buffer*/
                break;
        }
        else
            ;
    }
    goto here;
}

void BufferInitialize()
{
    buffer[0]=0;
    buffer[1]=0;
    bptr0=NULL;
    bptr1=NULL;
    putstr("\n\r");
    putstr("Input buffer size between 32 and 2800 bytes, divisible by 16\n\r");
	do
    {
        res=ReadValue();
        if(res>31 && res<2801)
        {
            if(res%16==0)
            {
                if ((buffer[0] = malloc(res)) == 0)  //allocate buffer0
                    {putstr("malloc buffer0 failed\n\r");}
                if ((buffer[1] = malloc(res)) == 0)         //allocate buffer1
                {
                    putstr("malloc buffer1 failed\n\r");
                    putstr("Enter smaller size for buffer\n\r");
                    free (buffer[0]);  // if buffer1 malloc fails, free buffer 0
                }
            }
            else
                putstr("\n\rEnter the correct buffer size value divisible by 16\n\r");
        }
        else
            putstr("\n\rEnter the correct buffer size between 32 and 2800\n\r");
    } while((buffer[0] == 0)||(buffer[1] ==0));
    putstr("\n\rBuffer allocation successful\n\r");
    buffer_number=buffer_number+2;
    bufflen[0]=res;
    bufflen[1]=res;         /* buffer lengths */
    buff1ptr=buffer[0];     /* assign pointers to buffer to do certain actions */
    buff2ptr=buffer[1];
    bptr0=buffer[0];
    bptr1=buffer[1];
}

/* Getting user input and converting it into integer value */
int ReadValue()
{
    value=0;
    do
    {
        i=getchar();
        if(i>='0'&&i<='9')          /* works only if its a number */
        {
            value=((value*10)+(i-'0')); /* Convert character to integers */
            putchar(i);
            j++;
        }
        else if(i==127)
        {
            value=value/10;
        }
        else if(i!=13)
            {putstr("\n\rEnter Numbers\n\r");}
    }while(i!=13);
    j=0;
    return value;
}

void newbuffer()
{
    putstr("\n\r");
    putstr("\n\rEnter the buffer size between 20 and 400\n\r");
    DEBUGPORT(12)
    res = ReadValue();
    if(res>=20 && res<=400)     /* if size is in between the mentioned size*/
    {
        if((buffer[buffer_count]=malloc(res))==0)       /* allocate buffer */
            putstr("\n\rBuffer allocation failed\n\r");
        else
            {putstr("\n\rBuffer Allocated\n\r");
            bufflen[buffer_count]=res;                  /* take down buffer length */
            printf_tiny("Buffer address and buffer number 0x%x, %d\n\r", (unsigned int) buffer[buffer_count], buffer_count);
            }
    }
    else
        putstr("\n\rNot a valid buffer size\n\r");
    buffer_count++;         /* increment buffer number and buffer count */
    buffer_number++;
}

void help_menu()
{
    printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HELP MENU~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    printf_tiny("\n\r '+' Allocate new buffer\n\r");
    printf_tiny(" '-' Delete a buffer\n\r");
    printf_tiny(" '@' Start over from allocating heap\n\r");
    printf_tiny(" '?' Display all buffer details and buffer0, buffer1 contents and erase it \n\r");
    printf_tiny(" '=' Display the contents of the buffer\n\r");
    printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~HELP MENU~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    printf_tiny("\n\r\n\r Enter the option or Character\n\r");

}

/*de-allocates the buffer, buffer number is user given */
void deletebuffer()
{
    putstr("\n\r");
    putstr("\n\rEnter the buffer number which has to be deleted\n\r");
    res=ReadValue();
        if(res>buffer_count || res==1 || res==0)    /* check if buffer number to be deleted is proper*/
            putstr("Enter valid buffer number next time\n\r");
        else if (!buffer[res])                      /* check if buffer exists */
            putstr("Enter valid buffer number next time\n\r");
        else
        {
            free (buffer[res]);             /* free buffer */
            buffer[res]=0;
            buffer_number--;                /* decrement the number of buffers that are active */
            putstr("Buffer deleted\n\r");
        }

}

/* fill data into buffer 0 */
void fillbuffer0(unsigned char buff_val)
{
    if((unsigned int)bptr0<((unsigned int)buff1ptr+(int)bufflen[0]-1)) /*check if buffer is full */
    {
        *bptr0 = buff_val;
        bptr0++;
    }
    else if((unsigned int)bptr0==((unsigned int)buff1ptr+(int)bufflen[0]-1))
        {
            *bptr0=buff_val;
            bptr0++;
            *bptr0='\0';
        }
}

void fillbuffer1(unsigned char buff_val)
{
    if((unsigned int)bptr1<((unsigned int)buff2ptr+(int)bufflen[1]-1))      /*checking if buffer is full */
    {
        *bptr1 = buff_val;
        bptr1++;
    }
    else if((unsigned int)bptr1==((unsigned int)buff2ptr+(int)bufflen[1]-1))
        {
            *bptr1=buff_val;
            bptr1++;
            *bptr1='\0';
        }

}

/* de-allocate all buffers */
void freeEverything()
{
    unsigned int counter;
    for (counter=0; counter<buffer_count; counter++)
    {
        if(buffer[counter])
        {
            free (buffer[counter]);                 /*Delete Every buffer */
            printf_tiny("Deleted %d buffer\n\r",counter);
        }
    }
}

/* Displays all buffer report and contents. After that erases the buffer 0 and buffer 1 */
void displaydelete()
{
    unsigned int len=0, charnum0=0, charnum1=0 ;
    putstr("\n\r");
    printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYING DETAILS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    printf_tiny("\n\rTotal Number of Buffer = %d\n\r", buffer_number);
    putstr("\n\r");
    printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    printf_tiny("\n\rTotal number of characters received = %d\n\r", char_count);
    printf_tiny("Total number of storage characters received = %d\n\r", stor_count);
    printf_tiny("Total number of characters received since last '?' = %d\n\r", (char_count-lastchar_count));
    printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    putstr("\n\r");
    lastchar_count=char_count;
    while((unsigned char)*buff1ptr!='\0')           /* calculate number of characters in buffer0*/
    {
        charnum0++;
        (unsigned char)buff1ptr++;
    }
    printf_tiny("\n\rNumber of characters in buffer0 = %d\n\r", charnum0);
    printf_tiny("Number of free spaces in buffer0 = %d\n\r", (bufflen[0]-charnum0));
    buff1ptr=buff1ptr-charnum0;
    while((unsigned char)*buff2ptr!='\0')           /* calculate number of characters in buffer1*/
    {
        charnum1++;
        (unsigned char)buff2ptr++;
    }
    printf_tiny("Number of characters in buffer1 = %d\n\r", charnum1);
    printf_tiny("Number of free spaces in buffer0 = %d\n\r", (bufflen[1]-charnum1));
    buff2ptr=buff2ptr-charnum1;
    putstr("\n\r");
    printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    putstr("\n\r");
    for(len=0; len<buffer_count; len++)
    {
        if(buffer[len])
        {
            printf_tiny("Buffer %d Information\n\r", len);
            printf_tiny("Starting address = 0x%x\n\r",(unsigned int)buffer[len]);
            printf_tiny("Ending address = 0x%x\n\r",((unsigned int)buffer[len]+(unsigned int)bufflen[len]));
            printf_tiny("Allocated size = %d\n\r", bufflen[len]);
            putstr("\n\r");
        }
    }
    printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    putstr("\n\rContents of Buffer0\n\r");
    putstrbuff(buff1ptr);               /* Displays content */
    bptr0=bptr0-charnum0;               /*Move back the pointer to original after erasing buffer*/
    putstr("\n\r");
    printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    putstr("\n\rContents of Buffer1\n\r");
    putstrbuff(buff2ptr);               /*Displays content*/
    bptr1=bptr1-charnum1;               /*Move back the pointer to original after erasing buffer*/
    putstr("\n\r");
    printf_tiny("\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");
    printf_tiny("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DISPLAYED ALL DETAILS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\r");

}

/* Displays hex value contents of buffer 0 and buffer 1 with 16 elements in a line*/
void display()
{
    putstr("\n\r");
    putstr("\n\rContents of Buffer0\n\r");
    putstrhex(0);
    putstr("\n\r");
    putstr("Contents of Buffer1\n\r");
    putstrhex(1);
    putstr("\n\r");
}

void putstrhex(unsigned int g)
{
    unsigned int count=0;
    unsigned int length=0;
        while(*buffer[g])
        {
            printf("0x%04x:  ",((unsigned int)buffer[g]));          /*Displaying address. hex value of 16 characters in a line*/
            for(count=1; ((count%17)!=0); count++)
            {
                if(*buffer[g])
                {
                    printf_tiny("%x ", *buffer[g]);
                    buffer[g]++;
                    length++;
                }
                else
                    break;

            }
            count=1;
            putstr("\n\r");

        }
        buffer[g]=buffer[g]-length;
        putstr("\n\r");

}

/* Transmit string of characters until a null character is occurred */
void putstrbuff(char *ptr)
{
    unsigned int count=0;
        while(*ptr)
        {
            putchar(*ptr);
            count++;
            *ptr='\0';
            if (count>63)               /* 64 characters in  a line*/
            {count=0;
                printf_tiny("\n\r");}
            ptr++;
        }

}

void putstr(char *ptr)
{
        while(*ptr)
        {
            putchar(*ptr);
            ptr++;
        }

}

void SerialInitialize()
{
    TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
    TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
    SCON=0x50;
    TR1=1;  // to start timer
    IE=0x90; // to make serial interrupt interrupt enable
}


char getchar()
{
    /* RI bit is present in SCON register*/
    while(RI==0);  // wait until hole 8 bit data is received completely. RI bit become 1 when reception is completed
    rec=SBUF;   // data is received in SBUF register present in controller during receiving
    RI=0;  // make RI bit to 0 so that next data is received
    return rec;  // return rec where function is called
}

void putchar(int x)
{
    SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
    while(TI==0); // wait until transmission is completed. when transmission is completed TI bit become 1
    TI=0; // make TI bit to zero for next transmission
}
