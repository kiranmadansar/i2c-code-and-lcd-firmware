/*
 */
#include <at89c51ed2.h>
#include <mcs51reg.h>
#include <mcs51/8051.h>
#include <malloc.h>
#include<stdio.h>

#define HEAP_SIZE 0x3000   // size must be smaller than available XRAM

unsigned char xdata heap[HEAP_SIZE];

char getchar();     // initialize controller to work as serial communication
void SerialInitialize();  // receive data to controller via rxd pin to computer,s hyper terminal
void putchar(int x);  // transmit data to computer,s hyper terminal from controller via txd pin
void putstr(char *);
void putstrbuff(char *);


_sdcc_external_startup()
{
    AUXR |= 0x0C;
    //init_dynamic_memory((MEMHEADER xdata *)heap, HEAP_SIZE);
    return 0;
}

static unsigned int buffer_count, stor_count, char_count;
static unsigned int buffer_number, lastchar_count;
xdata unsigned char * buffer[256];
unsigned char *buff1ptr, *buff2ptr;
unsigned int bufflen[256];
unsigned char rec;  // gloable variable
unsigned char temp, buff_val, j, i=0;
char *ptr;
void delay(int x);
void fillbuffer0(unsigned char);
void fillbuffer1(unsigned char);
void newbuffer();
void deletebuffer();
int ReadValue();
void displaydelete();
int value, res;
void BufferInitialize();
void display();
void putstrhex(char *);
void freeEverything();
unsigned char *bptr0;
unsigned char *bptr1;

void main()
{
    lastchar_count=0;
    char_count=0;
    stor_count=0;
    buffer_count=2;
    buffer_number=0;

    SerialInitialize();  //  call the initialize function
here:    BufferInitialize();

    while(1)
    {
        putstr("\n\rEnter the character\n\r");
        temp = getchar();
        putchar(temp);
        putstr("\n\r");
        if (temp>='A' && temp<='Z')
        {
            stor_count++;
            char_count++;
            fillbuffer0(temp);
        }
        else if (temp>='0' && temp<='9')
        {
            stor_count++;
            char_count++;
            fillbuffer1(temp);
        }
        else if (temp=='+')
        {
            newbuffer();
            char_count++;
        }
        else if (temp=='-')
        {
            char_count++;
            deletebuffer();
        }
        else if (temp=='?')
        {
            char_count++;
            displaydelete();
        }
        else if(temp=='=')
        {
                char_count++;
                display();
        }
        else if(temp=='@')
        {
                char_count++;
                freeEverything();
                break;
        }
        else if (temp=='del')
            {break;}
        else
            ;

       // else if (temp==0x7f)
        //    break;
        //putchar(temp);  //  call transmit function
    }
    buffer_count=2;
    buffer_number=0;
    goto here;
}

void BufferInitialize()
{
    unsigned char z;
    z=2;
    init_dynamic_memory((MEMHEADER xdata *)heap, HEAP_SIZE);
	putstr("Input buffer size between 32 and 2800 bytes, divisible by 16\n\r");
	do
    {
        res=ReadValue();
        if(res>31 && res<2801)
        {
            if(res%16==0)
            {
                if ((buffer[0] = malloc((unsigned int)z * res)) == 0)  //allocate buffer0
                    {putstr("malloc buffer0 failed\n\r");}
                if ((buffer[1] = malloc((unsigned int)z * res)) == 0)         //allocate buffer1
                {
                    putstr("malloc buffer1 failed\n\r");
                    free (buffer[0]);  // if buffer1 malloc fails, free buffer 0
                }
            }
            else
                putstr("Enter the correct buffer size value divisible by 16\n\r");
        }
        else
            putstr("Enter the correct buffer size between 32 and 2800\n\r");
    } while((buffer[0] == 0)||(buffer[1] ==0));
    putstr("Buffer allocation successful\n\r");
    buffer_number=buffer_number+2;
    bufflen[0]=res;
    bufflen[1]=res;
    printf_tiny("len1 = %d\n\r", bufflen[0]);
    printf_tiny("len2 = %d\n\r", bufflen[1]);
    buff1ptr=buffer[0];
    buff2ptr=buffer[1];
    bptr0=buffer[0];
    bptr1=buffer[1];
    printf_tiny("Buffer0 address 0x%x\n\r", (unsigned int) buffer[0]);
    printf_tiny("Buffer1 address 0x%x\n\r", (unsigned int) buffer[1]);
}

int ReadValue()
{
    value=0;
    while(j<4)
    {
        i=getchar();
        value=((value*10)+(i-'0'));
        putchar(i);
        j++;
    }
    j=0;
    return value;
}

void newbuffer()
{
    putstr("Enter the buffer size between 20 and 400\n\r");
    do
    {
        res = ReadValue();
        if(res>20 && res<400)
        {
            if((buffer[buffer_count]=malloc(sizeof(int)*res))==0)
                putstr("\n\rBuffer allocation failed\n\r");
            else
                {putstr("Buffer Allocated\n\r");
                bufflen[buffer_count]=res;
                printf_tiny("Buffer address and buffer number 0x%x, %d\n\r", (unsigned int) buffer[buffer_count], buffer_count);
                }

        }
        else
            putstr("Enter valid buffer size\n\r");
    } while(buffer[buffer_count]==0);
    buffer_count++;
    buffer_number++;
}

void deletebuffer()
{
    putstr("Enter the buffer number which has to be deleted\n\r");
    res=ReadValue();
        if(res>buffer_count || res==1 || res==0)
            putstr("Enter valid buffer number next time\n\r");
        else if (!buffer[res])
            putstr("Enter valid buffer number next time\n\r");
        else
        {
            free (buffer[res]);
            buffer[res]=0;
            buffer_number--;
            putstr("Buffer deleted\n\r");
        }

}

void fillbuffer0(unsigned char buff_val)
{


    if((unsigned int)buffer[0]<((unsigned int)buff1ptr+(int)bufflen[0]))
    {
        *bptr0 = buff_val;
        bptr0++;
    }
}

void fillbuffer1(unsigned char buff_val)
{
    if((unsigned int)buffer[1]<((unsigned int)buff2ptr+(int)bufflen[1]))
    {
        *bptr1 = buff_val;
        bptr1++;
    }
}


void freeEverything()
{
    unsigned int counter;
    for (counter=0; counter<buffer_count; counter++)
    {
        if(buffer[counter])
        {
            free (buffer[counter]);
            printf_tiny("Freeing %d buffer\n\r",counter);
        }
    }
}
void displaydelete()
{
    unsigned int len=0, charnum0=0, charnum1=0 ;
    printf_tiny("---------------------------------------------------------------------------------------\n\r");
    printf_tiny("\n\rrTotal Number of Buffer = %d\n\r", buffer_number);
    putstr("\n\r");
    printf_tiny("---------------------------------------------------------------------------------------\n\r");
    printf_tiny("\n\rTotal number of characters received = %d\n\r", char_count);
    printf_tiny("Total number of storage characters received = %d\n\r", stor_count);
    printf_tiny("Total number of characters received since last '?' = %d\n\r", (char_count-lastchar_count));
    printf_tiny("\n\r---------------------------------------------------------------------------------------\n\r");
    putstr("\n\r");
    lastchar_count=char_count;
    while((unsigned char)*buff1ptr!='\0')
    {
        charnum0++;
        (unsigned char)buff1ptr++;
    }
    printf_tiny("\n\rNumber of characters in buffer0 = %d\n\r", charnum0);
    buff1ptr=buff1ptr-charnum0;
    while((unsigned char)*buff2ptr!='\0')
    {
        charnum1++;
        (unsigned char)buff2ptr++;
    }
    printf_tiny("Number of characters in buffer1 = %d\n\r", charnum1);
    buff2ptr=buff2ptr-charnum1;
    printf_tiny("\n\r---------------------------------------------------------------------------------------\n\r");
    for(len=0; len<buffer_count; len++)
    {
        if(buffer[len])
        {
            printf_tiny("---------------------------------------------------------------------------------------\n\r");
            printf_tiny("Buffer %d Information\n\r", len);
            printf_tiny("Starting address = 0x%x\n\r",(unsigned int)buffer[len]);
            printf_tiny("Ending address = 0x%x\n\r",((unsigned int)buffer[len]+(unsigned int)bufflen[len]));
            printf_tiny("Allocated size = %d\n\r", bufflen[len]);
            printf_tiny("---------------------------------------------------------------------------------------\n\r");
            putstr("\n\r");
        }
    }
    printf_tiny("---------------------------------------------------------------------------------------\n\r");
    putstr("\n\rContents of Buffer0\n\r");
    putstrbuff(buff1ptr);
    //buff1ptr=buff1ptr-charnum0;
    bptr0=bptr0-charnum0;
    putstr("\n\r");
    printf_tiny("---------------------------------------------------------------------------------------\n\r");
    putstr("\n\rContents of Buffer1\n\r");
    putstrbuff(buff2ptr);
    //buff2ptr=buff2ptr-charnum1;
    bptr1=bptr1-charnum1;
    putstr("\n\r");
    printf_tiny("---------------------------------------------------------------------------------------\n\r");

}

void display()
{
    putstr("\n\rContents of Buffer0\n\r");
    putstrhex(buff1ptr);
    //buff1ptr=buff1ptr-charnum0;
    putstr("\n\r");
    putstr("Contents of Buffer1\n\r");
    putstrhex(buff2ptr);
    //buff2ptr=buff2ptr-charnum1;
    putstr("\n\r");
}

void putstrhex(char *ptr)
{
    unsigned int count=0;
    printf_tiny("0x%x:  ",&(ptr));
        while(*ptr)
        {
            if (count==15)
            {
                printf_tiny("0x%x:  ",&(ptr));
            }
            printf_tiny("%x ",*ptr);
            ptr++;
            if (count>15)
            {
                printf_tiny("\n\r");
                count=0;
            }
            count++;
        }

}

/* Transmit string of characters until a null character is occurred */
void putstrbuff(char *ptr)
{
    unsigned int count=0;
        while(*ptr)
        {
            putchar(*ptr);
            count++;
            *ptr='\0';
            if (count>63)
            {count=0;
                printf_tiny("\n\r");}
            ptr++;
        }

}

void putstr(char *ptr)
{
        while(*ptr)
        {
            putchar(*ptr);
            ptr++;
        }

}

void SerialInitialize()
{
    TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
    TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
    SCON=0x50;
    TR1=1;  // to start timer
    IE=0x90; // to make serial interrupt interrupt enable
}

char getchar()
{
    /* RI bit is present in SCON register*/
    while(RI==0);  // wait untill hole 8 bit data is received completely. RI bit become 1 when reception is compleated
    rec=SBUF;   // data is received in SBUF register present in controller during receiving
    RI=0;  // make RI bit to 0 so that next data is received
    return rec;  // return rec where funtion is called
}

void putchar(int x)
{
    SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
    while(TI==0); // wait untill transmission is compleated. when transmittion is compleated TI bit become 1
    TI=0; // make TI bit to zero for next transmittion
}

void delay(int x)
{
    int i;
	for(i=0;i<x;i++);
}
