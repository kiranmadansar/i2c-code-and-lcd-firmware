                              1 ;--------------------------------------------------------
                              2 ; File Created by SDCC : FreeWare ANSI-C Compiler
                              3 ; Version 2.6.0 #4309 (Jul 28 2006)
                              4 ; This file generated Fri Oct 20 23:58:12 2017
                              5 ;--------------------------------------------------------
                              6 	.module main
                              7 	.optsdcc -mmcs51 --model-large
                              8 	
                              9 ;--------------------------------------------------------
                             10 ; Public variables in this module
                             11 ;--------------------------------------------------------
                             12 	.globl _main
                             13 	.globl __sdcc_external_startup
                             14 	.globl _CY
                             15 	.globl _AC
                             16 	.globl _F0
                             17 	.globl _RS1
                             18 	.globl _RS0
                             19 	.globl _OV
                             20 	.globl _F1
                             21 	.globl _P
                             22 	.globl _PS
                             23 	.globl _PT1
                             24 	.globl _PX1
                             25 	.globl _PT0
                             26 	.globl _PX0
                             27 	.globl _RD
                             28 	.globl _WR
                             29 	.globl _T1
                             30 	.globl _T0
                             31 	.globl _INT1
                             32 	.globl _INT0
                             33 	.globl _TXD
                             34 	.globl _RXD
                             35 	.globl _P3_7
                             36 	.globl _P3_6
                             37 	.globl _P3_5
                             38 	.globl _P3_4
                             39 	.globl _P3_3
                             40 	.globl _P3_2
                             41 	.globl _P3_1
                             42 	.globl _P3_0
                             43 	.globl _EA
                             44 	.globl _ES
                             45 	.globl _ET1
                             46 	.globl _EX1
                             47 	.globl _ET0
                             48 	.globl _EX0
                             49 	.globl _P2_7
                             50 	.globl _P2_6
                             51 	.globl _P2_5
                             52 	.globl _P2_4
                             53 	.globl _P2_3
                             54 	.globl _P2_2
                             55 	.globl _P2_1
                             56 	.globl _P2_0
                             57 	.globl _SM0
                             58 	.globl _SM1
                             59 	.globl _SM2
                             60 	.globl _REN
                             61 	.globl _TB8
                             62 	.globl _RB8
                             63 	.globl _TI
                             64 	.globl _RI
                             65 	.globl _P1_7
                             66 	.globl _P1_6
                             67 	.globl _P1_5
                             68 	.globl _P1_4
                             69 	.globl _P1_3
                             70 	.globl _P1_2
                             71 	.globl _P1_1
                             72 	.globl _P1_0
                             73 	.globl _TF1
                             74 	.globl _TR1
                             75 	.globl _TF0
                             76 	.globl _TR0
                             77 	.globl _IE1
                             78 	.globl _IT1
                             79 	.globl _IE0
                             80 	.globl _IT0
                             81 	.globl _P0_7
                             82 	.globl _P0_6
                             83 	.globl _P0_5
                             84 	.globl _P0_4
                             85 	.globl _P0_3
                             86 	.globl _P0_2
                             87 	.globl _P0_1
                             88 	.globl _P0_0
                             89 	.globl _TXD0
                             90 	.globl _RXD0
                             91 	.globl _BREG_F7
                             92 	.globl _BREG_F6
                             93 	.globl _BREG_F5
                             94 	.globl _BREG_F4
                             95 	.globl _BREG_F3
                             96 	.globl _BREG_F2
                             97 	.globl _BREG_F1
                             98 	.globl _BREG_F0
                             99 	.globl _P5_7
                            100 	.globl _P5_6
                            101 	.globl _P5_5
                            102 	.globl _P5_4
                            103 	.globl _P5_3
                            104 	.globl _P5_2
                            105 	.globl _P5_1
                            106 	.globl _P5_0
                            107 	.globl _P4_7
                            108 	.globl _P4_6
                            109 	.globl _P4_5
                            110 	.globl _P4_4
                            111 	.globl _P4_3
                            112 	.globl _P4_2
                            113 	.globl _P4_1
                            114 	.globl _P4_0
                            115 	.globl _PX0L
                            116 	.globl _PT0L
                            117 	.globl _PX1L
                            118 	.globl _PT1L
                            119 	.globl _PLS
                            120 	.globl _PT2L
                            121 	.globl _PPCL
                            122 	.globl _EC
                            123 	.globl _CCF0
                            124 	.globl _CCF1
                            125 	.globl _CCF2
                            126 	.globl _CCF3
                            127 	.globl _CCF4
                            128 	.globl _CR
                            129 	.globl _CF
                            130 	.globl _TF2
                            131 	.globl _EXF2
                            132 	.globl _RCLK
                            133 	.globl _TCLK
                            134 	.globl _EXEN2
                            135 	.globl _TR2
                            136 	.globl _C_T2
                            137 	.globl _CP_RL2
                            138 	.globl _T2CON_7
                            139 	.globl _T2CON_6
                            140 	.globl _T2CON_5
                            141 	.globl _T2CON_4
                            142 	.globl _T2CON_3
                            143 	.globl _T2CON_2
                            144 	.globl _T2CON_1
                            145 	.globl _T2CON_0
                            146 	.globl _PT2
                            147 	.globl _ET2
                            148 	.globl _B
                            149 	.globl _ACC
                            150 	.globl _PSW
                            151 	.globl _IP
                            152 	.globl _P3
                            153 	.globl _IE
                            154 	.globl _P2
                            155 	.globl _SBUF
                            156 	.globl _SCON
                            157 	.globl _P1
                            158 	.globl _TH1
                            159 	.globl _TH0
                            160 	.globl _TL1
                            161 	.globl _TL0
                            162 	.globl _TMOD
                            163 	.globl _TCON
                            164 	.globl _PCON
                            165 	.globl _DPH
                            166 	.globl _DPL
                            167 	.globl _SP
                            168 	.globl _P0
                            169 	.globl _SBUF0
                            170 	.globl _DP0L
                            171 	.globl _DP0H
                            172 	.globl _EECON
                            173 	.globl _KBF
                            174 	.globl _KBE
                            175 	.globl _KBLS
                            176 	.globl _BRL
                            177 	.globl _BDRCON
                            178 	.globl _T2MOD
                            179 	.globl _SPDAT
                            180 	.globl _SPSTA
                            181 	.globl _SPCON
                            182 	.globl _SADEN
                            183 	.globl _SADDR
                            184 	.globl _WDTPRG
                            185 	.globl _WDTRST
                            186 	.globl _P5
                            187 	.globl _P4
                            188 	.globl _IPH1
                            189 	.globl _IPL1
                            190 	.globl _IPH0
                            191 	.globl _IPL0
                            192 	.globl _IEN1
                            193 	.globl _IEN0
                            194 	.globl _CMOD
                            195 	.globl _CL
                            196 	.globl _CH
                            197 	.globl _CCON
                            198 	.globl _CCAPM4
                            199 	.globl _CCAPM3
                            200 	.globl _CCAPM2
                            201 	.globl _CCAPM1
                            202 	.globl _CCAPM0
                            203 	.globl _CCAP4L
                            204 	.globl _CCAP3L
                            205 	.globl _CCAP2L
                            206 	.globl _CCAP1L
                            207 	.globl _CCAP0L
                            208 	.globl _CCAP4H
                            209 	.globl _CCAP3H
                            210 	.globl _CCAP2H
                            211 	.globl _CCAP1H
                            212 	.globl _CCAP0H
                            213 	.globl _CKCKON1
                            214 	.globl _CKCKON0
                            215 	.globl _CKRL
                            216 	.globl _AUXR1
                            217 	.globl _AUXR
                            218 	.globl _TH2
                            219 	.globl _TL2
                            220 	.globl _RCAP2H
                            221 	.globl _RCAP2L
                            222 	.globl _T2CON
                            223 	.globl _i
                            224 	.globl _bptr1
                            225 	.globl _bptr0
                            226 	.globl _res
                            227 	.globl _value
                            228 	.globl _ptr
                            229 	.globl _j
                            230 	.globl _buff_val
                            231 	.globl _temp
                            232 	.globl _rec
                            233 	.globl _bufflen
                            234 	.globl _buff2ptr
                            235 	.globl _buff1ptr
                            236 	.globl _buffer
                            237 	.globl _heap
                            238 	.globl _BufferInitialize
                            239 	.globl _ReadValue
                            240 	.globl _newbuffer
                            241 	.globl _deletebuffer
                            242 	.globl _fillbuffer0
                            243 	.globl _fillbuffer1
                            244 	.globl _freeEverything
                            245 	.globl _displaydelete
                            246 	.globl _display
                            247 	.globl _putstrhex
                            248 	.globl _putstrbuff
                            249 	.globl _putstr
                            250 	.globl _SerialInitialize
                            251 	.globl _getchar
                            252 	.globl _putchar
                            253 	.globl _delay
                            254 ;--------------------------------------------------------
                            255 ; special function registers
                            256 ;--------------------------------------------------------
                            257 	.area RSEG    (DATA)
                    00C8    258 _T2CON	=	0x00c8
                    00CA    259 _RCAP2L	=	0x00ca
                    00CB    260 _RCAP2H	=	0x00cb
                    00CC    261 _TL2	=	0x00cc
                    00CD    262 _TH2	=	0x00cd
                    008E    263 _AUXR	=	0x008e
                    00A2    264 _AUXR1	=	0x00a2
                    0097    265 _CKRL	=	0x0097
                    008F    266 _CKCKON0	=	0x008f
                    008F    267 _CKCKON1	=	0x008f
                    00FA    268 _CCAP0H	=	0x00fa
                    00FB    269 _CCAP1H	=	0x00fb
                    00FC    270 _CCAP2H	=	0x00fc
                    00FD    271 _CCAP3H	=	0x00fd
                    00FE    272 _CCAP4H	=	0x00fe
                    00EA    273 _CCAP0L	=	0x00ea
                    00EB    274 _CCAP1L	=	0x00eb
                    00EC    275 _CCAP2L	=	0x00ec
                    00ED    276 _CCAP3L	=	0x00ed
                    00EE    277 _CCAP4L	=	0x00ee
                    00DA    278 _CCAPM0	=	0x00da
                    00DB    279 _CCAPM1	=	0x00db
                    00DC    280 _CCAPM2	=	0x00dc
                    00DD    281 _CCAPM3	=	0x00dd
                    00DE    282 _CCAPM4	=	0x00de
                    00D8    283 _CCON	=	0x00d8
                    00F9    284 _CH	=	0x00f9
                    00E9    285 _CL	=	0x00e9
                    00D9    286 _CMOD	=	0x00d9
                    00A8    287 _IEN0	=	0x00a8
                    00B1    288 _IEN1	=	0x00b1
                    00B8    289 _IPL0	=	0x00b8
                    00B7    290 _IPH0	=	0x00b7
                    00B2    291 _IPL1	=	0x00b2
                    00B3    292 _IPH1	=	0x00b3
                    00C0    293 _P4	=	0x00c0
                    00D8    294 _P5	=	0x00d8
                    00A6    295 _WDTRST	=	0x00a6
                    00A7    296 _WDTPRG	=	0x00a7
                    00A9    297 _SADDR	=	0x00a9
                    00B9    298 _SADEN	=	0x00b9
                    00C3    299 _SPCON	=	0x00c3
                    00C4    300 _SPSTA	=	0x00c4
                    00C5    301 _SPDAT	=	0x00c5
                    00C9    302 _T2MOD	=	0x00c9
                    009B    303 _BDRCON	=	0x009b
                    009A    304 _BRL	=	0x009a
                    009C    305 _KBLS	=	0x009c
                    009D    306 _KBE	=	0x009d
                    009E    307 _KBF	=	0x009e
                    00D2    308 _EECON	=	0x00d2
                    0083    309 _DP0H	=	0x0083
                    0082    310 _DP0L	=	0x0082
                    0099    311 _SBUF0	=	0x0099
                    0080    312 _P0	=	0x0080
                    0081    313 _SP	=	0x0081
                    0082    314 _DPL	=	0x0082
                    0083    315 _DPH	=	0x0083
                    0087    316 _PCON	=	0x0087
                    0088    317 _TCON	=	0x0088
                    0089    318 _TMOD	=	0x0089
                    008A    319 _TL0	=	0x008a
                    008B    320 _TL1	=	0x008b
                    008C    321 _TH0	=	0x008c
                    008D    322 _TH1	=	0x008d
                    0090    323 _P1	=	0x0090
                    0098    324 _SCON	=	0x0098
                    0099    325 _SBUF	=	0x0099
                    00A0    326 _P2	=	0x00a0
                    00A8    327 _IE	=	0x00a8
                    00B0    328 _P3	=	0x00b0
                    00B8    329 _IP	=	0x00b8
                    00D0    330 _PSW	=	0x00d0
                    00E0    331 _ACC	=	0x00e0
                    00F0    332 _B	=	0x00f0
                            333 ;--------------------------------------------------------
                            334 ; special function bits
                            335 ;--------------------------------------------------------
                            336 	.area RSEG    (DATA)
                    00AD    337 _ET2	=	0x00ad
                    00BD    338 _PT2	=	0x00bd
                    00C8    339 _T2CON_0	=	0x00c8
                    00C9    340 _T2CON_1	=	0x00c9
                    00CA    341 _T2CON_2	=	0x00ca
                    00CB    342 _T2CON_3	=	0x00cb
                    00CC    343 _T2CON_4	=	0x00cc
                    00CD    344 _T2CON_5	=	0x00cd
                    00CE    345 _T2CON_6	=	0x00ce
                    00CF    346 _T2CON_7	=	0x00cf
                    00C8    347 _CP_RL2	=	0x00c8
                    00C9    348 _C_T2	=	0x00c9
                    00CA    349 _TR2	=	0x00ca
                    00CB    350 _EXEN2	=	0x00cb
                    00CC    351 _TCLK	=	0x00cc
                    00CD    352 _RCLK	=	0x00cd
                    00CE    353 _EXF2	=	0x00ce
                    00CF    354 _TF2	=	0x00cf
                    00DF    355 _CF	=	0x00df
                    00DE    356 _CR	=	0x00de
                    00DC    357 _CCF4	=	0x00dc
                    00DB    358 _CCF3	=	0x00db
                    00DA    359 _CCF2	=	0x00da
                    00D9    360 _CCF1	=	0x00d9
                    00D8    361 _CCF0	=	0x00d8
                    00AE    362 _EC	=	0x00ae
                    00BE    363 _PPCL	=	0x00be
                    00BD    364 _PT2L	=	0x00bd
                    00BC    365 _PLS	=	0x00bc
                    00BB    366 _PT1L	=	0x00bb
                    00BA    367 _PX1L	=	0x00ba
                    00B9    368 _PT0L	=	0x00b9
                    00B8    369 _PX0L	=	0x00b8
                    00C0    370 _P4_0	=	0x00c0
                    00C1    371 _P4_1	=	0x00c1
                    00C2    372 _P4_2	=	0x00c2
                    00C3    373 _P4_3	=	0x00c3
                    00C4    374 _P4_4	=	0x00c4
                    00C5    375 _P4_5	=	0x00c5
                    00C6    376 _P4_6	=	0x00c6
                    00C7    377 _P4_7	=	0x00c7
                    00D8    378 _P5_0	=	0x00d8
                    00D9    379 _P5_1	=	0x00d9
                    00DA    380 _P5_2	=	0x00da
                    00DB    381 _P5_3	=	0x00db
                    00DC    382 _P5_4	=	0x00dc
                    00DD    383 _P5_5	=	0x00dd
                    00DE    384 _P5_6	=	0x00de
                    00DF    385 _P5_7	=	0x00df
                    00F0    386 _BREG_F0	=	0x00f0
                    00F1    387 _BREG_F1	=	0x00f1
                    00F2    388 _BREG_F2	=	0x00f2
                    00F3    389 _BREG_F3	=	0x00f3
                    00F4    390 _BREG_F4	=	0x00f4
                    00F5    391 _BREG_F5	=	0x00f5
                    00F6    392 _BREG_F6	=	0x00f6
                    00F7    393 _BREG_F7	=	0x00f7
                    00B0    394 _RXD0	=	0x00b0
                    00B1    395 _TXD0	=	0x00b1
                    0080    396 _P0_0	=	0x0080
                    0081    397 _P0_1	=	0x0081
                    0082    398 _P0_2	=	0x0082
                    0083    399 _P0_3	=	0x0083
                    0084    400 _P0_4	=	0x0084
                    0085    401 _P0_5	=	0x0085
                    0086    402 _P0_6	=	0x0086
                    0087    403 _P0_7	=	0x0087
                    0088    404 _IT0	=	0x0088
                    0089    405 _IE0	=	0x0089
                    008A    406 _IT1	=	0x008a
                    008B    407 _IE1	=	0x008b
                    008C    408 _TR0	=	0x008c
                    008D    409 _TF0	=	0x008d
                    008E    410 _TR1	=	0x008e
                    008F    411 _TF1	=	0x008f
                    0090    412 _P1_0	=	0x0090
                    0091    413 _P1_1	=	0x0091
                    0092    414 _P1_2	=	0x0092
                    0093    415 _P1_3	=	0x0093
                    0094    416 _P1_4	=	0x0094
                    0095    417 _P1_5	=	0x0095
                    0096    418 _P1_6	=	0x0096
                    0097    419 _P1_7	=	0x0097
                    0098    420 _RI	=	0x0098
                    0099    421 _TI	=	0x0099
                    009A    422 _RB8	=	0x009a
                    009B    423 _TB8	=	0x009b
                    009C    424 _REN	=	0x009c
                    009D    425 _SM2	=	0x009d
                    009E    426 _SM1	=	0x009e
                    009F    427 _SM0	=	0x009f
                    00A0    428 _P2_0	=	0x00a0
                    00A1    429 _P2_1	=	0x00a1
                    00A2    430 _P2_2	=	0x00a2
                    00A3    431 _P2_3	=	0x00a3
                    00A4    432 _P2_4	=	0x00a4
                    00A5    433 _P2_5	=	0x00a5
                    00A6    434 _P2_6	=	0x00a6
                    00A7    435 _P2_7	=	0x00a7
                    00A8    436 _EX0	=	0x00a8
                    00A9    437 _ET0	=	0x00a9
                    00AA    438 _EX1	=	0x00aa
                    00AB    439 _ET1	=	0x00ab
                    00AC    440 _ES	=	0x00ac
                    00AF    441 _EA	=	0x00af
                    00B0    442 _P3_0	=	0x00b0
                    00B1    443 _P3_1	=	0x00b1
                    00B2    444 _P3_2	=	0x00b2
                    00B3    445 _P3_3	=	0x00b3
                    00B4    446 _P3_4	=	0x00b4
                    00B5    447 _P3_5	=	0x00b5
                    00B6    448 _P3_6	=	0x00b6
                    00B7    449 _P3_7	=	0x00b7
                    00B0    450 _RXD	=	0x00b0
                    00B1    451 _TXD	=	0x00b1
                    00B2    452 _INT0	=	0x00b2
                    00B3    453 _INT1	=	0x00b3
                    00B4    454 _T0	=	0x00b4
                    00B5    455 _T1	=	0x00b5
                    00B6    456 _WR	=	0x00b6
                    00B7    457 _RD	=	0x00b7
                    00B8    458 _PX0	=	0x00b8
                    00B9    459 _PT0	=	0x00b9
                    00BA    460 _PX1	=	0x00ba
                    00BB    461 _PT1	=	0x00bb
                    00BC    462 _PS	=	0x00bc
                    00D0    463 _P	=	0x00d0
                    00D1    464 _F1	=	0x00d1
                    00D2    465 _OV	=	0x00d2
                    00D3    466 _RS0	=	0x00d3
                    00D4    467 _RS1	=	0x00d4
                    00D5    468 _F0	=	0x00d5
                    00D6    469 _AC	=	0x00d6
                    00D7    470 _CY	=	0x00d7
                            471 ;--------------------------------------------------------
                            472 ; overlayable register banks
                            473 ;--------------------------------------------------------
                            474 	.area REG_BANK_0	(REL,OVR,DATA)
   0000                     475 	.ds 8
                            476 ;--------------------------------------------------------
                            477 ; internal ram data
                            478 ;--------------------------------------------------------
                            479 	.area DSEG    (DATA)
                            480 ;--------------------------------------------------------
                            481 ; overlayable items in internal ram 
                            482 ;--------------------------------------------------------
                            483 	.area OSEG    (OVR,DATA)
                            484 ;--------------------------------------------------------
                            485 ; Stack segment in internal ram 
                            486 ;--------------------------------------------------------
                            487 	.area	SSEG	(DATA)
   000B                     488 __start__stack:
   000B                     489 	.ds	1
                            490 
                            491 ;--------------------------------------------------------
                            492 ; indirectly addressable internal ram data
                            493 ;--------------------------------------------------------
                            494 	.area ISEG    (DATA)
                            495 ;--------------------------------------------------------
                            496 ; bit data
                            497 ;--------------------------------------------------------
                            498 	.area BSEG    (BIT)
                            499 ;--------------------------------------------------------
                            500 ; paged external ram data
                            501 ;--------------------------------------------------------
                            502 	.area PSEG    (PAG,XDATA)
                            503 ;--------------------------------------------------------
                            504 ; external ram data
                            505 ;--------------------------------------------------------
                            506 	.area XSEG    (XDATA)
   0000                     507 _heap::
   0000                     508 	.ds 12288
   3000                     509 _buffer_count:
   3000                     510 	.ds 2
   3002                     511 _stor_count:
   3002                     512 	.ds 2
   3004                     513 _char_count:
   3004                     514 	.ds 2
   3006                     515 _buffer_number:
   3006                     516 	.ds 2
   3008                     517 _lastchar_count:
   3008                     518 	.ds 2
   300A                     519 _buffer::
   300A                     520 	.ds 512
   320A                     521 _buff1ptr::
   320A                     522 	.ds 3
   320D                     523 _buff2ptr::
   320D                     524 	.ds 3
   3210                     525 _bufflen::
   3210                     526 	.ds 512
   3410                     527 _rec::
   3410                     528 	.ds 1
   3411                     529 _temp::
   3411                     530 	.ds 1
   3412                     531 _buff_val::
   3412                     532 	.ds 1
   3413                     533 _j::
   3413                     534 	.ds 1
   3414                     535 _ptr::
   3414                     536 	.ds 3
   3417                     537 _value::
   3417                     538 	.ds 2
   3419                     539 _res::
   3419                     540 	.ds 2
   341B                     541 _bptr0::
   341B                     542 	.ds 3
   341E                     543 _bptr1::
   341E                     544 	.ds 3
   3421                     545 _fillbuffer0_buff_val_1_1:
   3421                     546 	.ds 1
   3422                     547 _fillbuffer1_buff_val_1_1:
   3422                     548 	.ds 1
   3423                     549 _displaydelete_charnum0_1_1:
   3423                     550 	.ds 2
   3425                     551 _displaydelete_charnum1_1_1:
   3425                     552 	.ds 2
   3427                     553 _putstrhex_ptr_1_1:
   3427                     554 	.ds 3
   342A                     555 _putstrhex_count_1_1:
   342A                     556 	.ds 2
   342C                     557 _putstrbuff_ptr_1_1:
   342C                     558 	.ds 3
   342F                     559 _putstrbuff_count_1_1:
   342F                     560 	.ds 2
   3431                     561 _putstr_ptr_1_1:
   3431                     562 	.ds 3
   3434                     563 _putchar_x_1_1:
   3434                     564 	.ds 2
   3436                     565 _delay_x_1_1:
   3436                     566 	.ds 2
                            567 ;--------------------------------------------------------
                            568 ; external initialized ram data
                            569 ;--------------------------------------------------------
                            570 	.area XISEG   (XDATA)
   345B                     571 _i::
   345B                     572 	.ds 1
                            573 	.area HOME    (CODE)
                            574 	.area GSINIT0 (CODE)
                            575 	.area GSINIT1 (CODE)
                            576 	.area GSINIT2 (CODE)
                            577 	.area GSINIT3 (CODE)
                            578 	.area GSINIT4 (CODE)
                            579 	.area GSINIT5 (CODE)
                            580 	.area GSINIT  (CODE)
                            581 	.area GSFINAL (CODE)
                            582 	.area CSEG    (CODE)
                            583 ;--------------------------------------------------------
                            584 ; interrupt vector 
                            585 ;--------------------------------------------------------
                            586 	.area HOME    (CODE)
   0000                     587 __interrupt_vect:
   0000 02 00 03            588 	ljmp	__sdcc_gsinit_startup
                            589 ;--------------------------------------------------------
                            590 ; global & static initialisations
                            591 ;--------------------------------------------------------
                            592 	.area HOME    (CODE)
                            593 	.area GSINIT  (CODE)
                            594 	.area GSFINAL (CODE)
                            595 	.area GSINIT  (CODE)
                            596 	.globl __sdcc_gsinit_startup
                            597 	.globl __sdcc_program_startup
                            598 	.globl __start__stack
                            599 	.globl __mcs51_genXINIT
                            600 	.globl __mcs51_genXRAMCLEAR
                            601 	.globl __mcs51_genRAMCLEAR
                            602 	.area GSFINAL (CODE)
   005C 02 00 5F            603 	ljmp	__sdcc_program_startup
                            604 ;--------------------------------------------------------
                            605 ; Home
                            606 ;--------------------------------------------------------
                            607 	.area HOME    (CODE)
                            608 	.area CSEG    (CODE)
   005F                     609 __sdcc_program_startup:
   005F 12 00 7A            610 	lcall	_main
                            611 ;	return from main will lock up
   0062 80 FE               612 	sjmp .
                            613 ;--------------------------------------------------------
                            614 ; code
                            615 ;--------------------------------------------------------
                            616 	.area CSEG    (CODE)
                            617 ;------------------------------------------------------------
                            618 ;Allocation info for local variables in function '_sdcc_external_startup'
                            619 ;------------------------------------------------------------
                            620 ;------------------------------------------------------------
                            621 ;	main.c:21: _sdcc_external_startup()
                            622 ;	-----------------------------------------
                            623 ;	 function _sdcc_external_startup
                            624 ;	-----------------------------------------
   0064                     625 __sdcc_external_startup:
                    0002    626 	ar2 = 0x02
                    0003    627 	ar3 = 0x03
                    0004    628 	ar4 = 0x04
                    0005    629 	ar5 = 0x05
                    0006    630 	ar6 = 0x06
                    0007    631 	ar7 = 0x07
                    0000    632 	ar0 = 0x00
                    0001    633 	ar1 = 0x01
                            634 ;	main.c:23: AUXR |= 0x0C;
                            635 ;	genOr
   0064 43 8E 0C            636 	orl	_AUXR,#0x0C
                            637 ;	main.c:24: init_dynamic_memory((MEMHEADER xdata *)heap, HEAP_SIZE);
                            638 ;	genAssign
   0067 90 34 43            639 	mov	dptr,#_init_dynamic_memory_PARM_2
                            640 ;	Peephole 181	changed mov to clr
   006A E4                  641 	clr	a
   006B F0                  642 	movx	@dptr,a
   006C A3                  643 	inc	dptr
   006D 74 30               644 	mov	a,#0x30
   006F F0                  645 	movx	@dptr,a
                            646 ;	genCall
                            647 ;	Peephole 182.a	used 16 bit load of DPTR
   0070 90 00 00            648 	mov	dptr,#_heap
   0073 12 0E 84            649 	lcall	_init_dynamic_memory
                            650 ;	main.c:25: return 0;
                            651 ;	genRet
                            652 ;	Peephole 182.b	used 16 bit load of dptr
   0076 90 00 00            653 	mov	dptr,#0x0000
                            654 ;	Peephole 300	removed redundant label 00101$
   0079 22                  655 	ret
                            656 ;------------------------------------------------------------
                            657 ;Allocation info for local variables in function 'main'
                            658 ;------------------------------------------------------------
                            659 ;------------------------------------------------------------
                            660 ;	main.c:51: void main()
                            661 ;	-----------------------------------------
                            662 ;	 function main
                            663 ;	-----------------------------------------
   007A                     664 _main:
                            665 ;	main.c:53: lastchar_count=0;
                            666 ;	genAssign
   007A 90 30 08            667 	mov	dptr,#_lastchar_count
   007D E4                  668 	clr	a
   007E F0                  669 	movx	@dptr,a
   007F A3                  670 	inc	dptr
   0080 F0                  671 	movx	@dptr,a
                            672 ;	main.c:54: char_count=0;
                            673 ;	genAssign
   0081 90 30 04            674 	mov	dptr,#_char_count
   0084 E4                  675 	clr	a
   0085 F0                  676 	movx	@dptr,a
   0086 A3                  677 	inc	dptr
   0087 F0                  678 	movx	@dptr,a
                            679 ;	main.c:55: stor_count=0;
                            680 ;	genAssign
   0088 90 30 02            681 	mov	dptr,#_stor_count
   008B E4                  682 	clr	a
   008C F0                  683 	movx	@dptr,a
   008D A3                  684 	inc	dptr
   008E F0                  685 	movx	@dptr,a
                            686 ;	main.c:56: buffer_count=2;
                            687 ;	genAssign
   008F 90 30 00            688 	mov	dptr,#_buffer_count
   0092 74 02               689 	mov	a,#0x02
   0094 F0                  690 	movx	@dptr,a
   0095 E4                  691 	clr	a
   0096 A3                  692 	inc	dptr
   0097 F0                  693 	movx	@dptr,a
                            694 ;	main.c:57: buffer_number=0;
                            695 ;	genAssign
   0098 90 30 06            696 	mov	dptr,#_buffer_number
   009B E4                  697 	clr	a
   009C F0                  698 	movx	@dptr,a
   009D A3                  699 	inc	dptr
   009E F0                  700 	movx	@dptr,a
                            701 ;	main.c:59: SerialInitialize();  //  call the initialize function
                            702 ;	genCall
   009F 12 0D 55            703 	lcall	_SerialInitialize
                            704 ;	main.c:60: here:    BufferInitialize();
   00A2                     705 00101$:
                            706 ;	genCall
   00A2 12 01 EE            707 	lcall	_BufferInitialize
                            708 ;	main.c:62: while(1)
   00A5                     709 00128$:
                            710 ;	main.c:64: putstr("\n\rEnter the character\n\r");
                            711 ;	genCall
                            712 ;	Peephole 182.a	used 16 bit load of DPTR
   00A5 90 12 BA            713 	mov	dptr,#__str_0
   00A8 75 F0 80            714 	mov	b,#0x80
   00AB 12 0C F8            715 	lcall	_putstr
                            716 ;	main.c:65: temp = getchar();
                            717 ;	genCall
   00AE 12 0D 64            718 	lcall	_getchar
   00B1 AA 82               719 	mov	r2,dpl
                            720 ;	genAssign
   00B3 90 34 11            721 	mov	dptr,#_temp
   00B6 EA                  722 	mov	a,r2
   00B7 F0                  723 	movx	@dptr,a
                            724 ;	main.c:66: putchar(temp);
                            725 ;	genAssign
                            726 ;	genCast
   00B8 7B 00               727 	mov	r3,#0x00
                            728 ;	genCall
   00BA 8A 82               729 	mov	dpl,r2
   00BC 8B 83               730 	mov	dph,r3
   00BE 12 0D 73            731 	lcall	_putchar
                            732 ;	main.c:67: putstr("\n\r");
                            733 ;	genCall
                            734 ;	Peephole 182.a	used 16 bit load of DPTR
   00C1 90 12 D2            735 	mov	dptr,#__str_1
   00C4 75 F0 80            736 	mov	b,#0x80
   00C7 12 0C F8            737 	lcall	_putstr
                            738 ;	main.c:68: if (temp>='A' && temp<='Z')
                            739 ;	genAssign
   00CA 90 34 11            740 	mov	dptr,#_temp
   00CD E0                  741 	movx	a,@dptr
   00CE FA                  742 	mov	r2,a
                            743 ;	genCmpLt
                            744 ;	genCmp
   00CF BA 41 00            745 	cjne	r2,#0x41,00143$
   00D2                     746 00143$:
                            747 ;	genIfxJump
                            748 ;	Peephole 112.b	changed ljmp to sjmp
                            749 ;	Peephole 160.a	removed sjmp by inverse jump logic
   00D2 40 32               750 	jc	00124$
                            751 ;	Peephole 300	removed redundant label 00144$
                            752 ;	genCmpGt
                            753 ;	genCmp
                            754 ;	genIfxJump
                            755 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   00D4 EA                  756 	mov	a,r2
   00D5 24 A5               757 	add	a,#0xff - 0x5A
                            758 ;	Peephole 112.b	changed ljmp to sjmp
                            759 ;	Peephole 160.a	removed sjmp by inverse jump logic
   00D7 40 2D               760 	jc	00124$
                            761 ;	Peephole 300	removed redundant label 00145$
                            762 ;	main.c:70: stor_count++;
                            763 ;	genAssign
   00D9 90 30 02            764 	mov	dptr,#_stor_count
   00DC E0                  765 	movx	a,@dptr
   00DD FB                  766 	mov	r3,a
   00DE A3                  767 	inc	dptr
   00DF E0                  768 	movx	a,@dptr
   00E0 FC                  769 	mov	r4,a
                            770 ;	genPlus
   00E1 90 30 02            771 	mov	dptr,#_stor_count
                            772 ;     genPlusIncr
   00E4 74 01               773 	mov	a,#0x01
                            774 ;	Peephole 236.a	used r3 instead of ar3
   00E6 2B                  775 	add	a,r3
   00E7 F0                  776 	movx	@dptr,a
                            777 ;	Peephole 181	changed mov to clr
   00E8 E4                  778 	clr	a
                            779 ;	Peephole 236.b	used r4 instead of ar4
   00E9 3C                  780 	addc	a,r4
   00EA A3                  781 	inc	dptr
   00EB F0                  782 	movx	@dptr,a
                            783 ;	main.c:71: char_count++;
                            784 ;	genAssign
   00EC 90 30 04            785 	mov	dptr,#_char_count
   00EF E0                  786 	movx	a,@dptr
   00F0 FB                  787 	mov	r3,a
   00F1 A3                  788 	inc	dptr
   00F2 E0                  789 	movx	a,@dptr
   00F3 FC                  790 	mov	r4,a
                            791 ;	genPlus
   00F4 90 30 04            792 	mov	dptr,#_char_count
                            793 ;     genPlusIncr
   00F7 74 01               794 	mov	a,#0x01
                            795 ;	Peephole 236.a	used r3 instead of ar3
   00F9 2B                  796 	add	a,r3
   00FA F0                  797 	movx	@dptr,a
                            798 ;	Peephole 181	changed mov to clr
   00FB E4                  799 	clr	a
                            800 ;	Peephole 236.b	used r4 instead of ar4
   00FC 3C                  801 	addc	a,r4
   00FD A3                  802 	inc	dptr
   00FE F0                  803 	movx	@dptr,a
                            804 ;	main.c:72: fillbuffer0(temp);
                            805 ;	genCall
   00FF 8A 82               806 	mov	dpl,r2
   0101 12 06 02            807 	lcall	_fillbuffer0
                            808 ;	Peephole 112.b	changed ljmp to sjmp
   0104 80 9F               809 	sjmp	00128$
   0106                     810 00124$:
                            811 ;	main.c:74: else if (temp>='0' && temp<='9')
                            812 ;	genAssign
   0106 90 34 11            813 	mov	dptr,#_temp
   0109 E0                  814 	movx	a,@dptr
   010A FA                  815 	mov	r2,a
                            816 ;	genCmpLt
                            817 ;	genCmp
   010B BA 30 00            818 	cjne	r2,#0x30,00146$
   010E                     819 00146$:
                            820 ;	genIfxJump
                            821 ;	Peephole 112.b	changed ljmp to sjmp
                            822 ;	Peephole 160.a	removed sjmp by inverse jump logic
   010E 40 33               823 	jc	00120$
                            824 ;	Peephole 300	removed redundant label 00147$
                            825 ;	genCmpGt
                            826 ;	genCmp
                            827 ;	genIfxJump
                            828 ;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
   0110 EA                  829 	mov	a,r2
   0111 24 C6               830 	add	a,#0xff - 0x39
                            831 ;	Peephole 112.b	changed ljmp to sjmp
                            832 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0113 40 2E               833 	jc	00120$
                            834 ;	Peephole 300	removed redundant label 00148$
                            835 ;	main.c:76: stor_count++;
                            836 ;	genAssign
   0115 90 30 02            837 	mov	dptr,#_stor_count
   0118 E0                  838 	movx	a,@dptr
   0119 FB                  839 	mov	r3,a
   011A A3                  840 	inc	dptr
   011B E0                  841 	movx	a,@dptr
   011C FC                  842 	mov	r4,a
                            843 ;	genPlus
   011D 90 30 02            844 	mov	dptr,#_stor_count
                            845 ;     genPlusIncr
   0120 74 01               846 	mov	a,#0x01
                            847 ;	Peephole 236.a	used r3 instead of ar3
   0122 2B                  848 	add	a,r3
   0123 F0                  849 	movx	@dptr,a
                            850 ;	Peephole 181	changed mov to clr
   0124 E4                  851 	clr	a
                            852 ;	Peephole 236.b	used r4 instead of ar4
   0125 3C                  853 	addc	a,r4
   0126 A3                  854 	inc	dptr
   0127 F0                  855 	movx	@dptr,a
                            856 ;	main.c:77: char_count++;
                            857 ;	genAssign
   0128 90 30 04            858 	mov	dptr,#_char_count
   012B E0                  859 	movx	a,@dptr
   012C FB                  860 	mov	r3,a
   012D A3                  861 	inc	dptr
   012E E0                  862 	movx	a,@dptr
   012F FC                  863 	mov	r4,a
                            864 ;	genPlus
   0130 90 30 04            865 	mov	dptr,#_char_count
                            866 ;     genPlusIncr
   0133 74 01               867 	mov	a,#0x01
                            868 ;	Peephole 236.a	used r3 instead of ar3
   0135 2B                  869 	add	a,r3
   0136 F0                  870 	movx	@dptr,a
                            871 ;	Peephole 181	changed mov to clr
   0137 E4                  872 	clr	a
                            873 ;	Peephole 236.b	used r4 instead of ar4
   0138 3C                  874 	addc	a,r4
   0139 A3                  875 	inc	dptr
   013A F0                  876 	movx	@dptr,a
                            877 ;	main.c:78: fillbuffer1(temp);
                            878 ;	genCall
   013B 8A 82               879 	mov	dpl,r2
   013D 12 06 58            880 	lcall	_fillbuffer1
   0140 02 00 A5            881 	ljmp	00128$
   0143                     882 00120$:
                            883 ;	main.c:80: else if (temp=='+')
                            884 ;	genAssign
   0143 90 34 11            885 	mov	dptr,#_temp
   0146 E0                  886 	movx	a,@dptr
   0147 FA                  887 	mov	r2,a
                            888 ;	genCmpEq
                            889 ;	gencjneshort
                            890 ;	Peephole 112.b	changed ljmp to sjmp
                            891 ;	Peephole 198.b	optimized misc jump sequence
   0148 BA 2B 19            892 	cjne	r2,#0x2B,00117$
                            893 ;	Peephole 200.b	removed redundant sjmp
                            894 ;	Peephole 300	removed redundant label 00149$
                            895 ;	Peephole 300	removed redundant label 00150$
                            896 ;	main.c:82: newbuffer();
                            897 ;	genCall
   014B 12 04 32            898 	lcall	_newbuffer
                            899 ;	main.c:83: char_count++;
                            900 ;	genAssign
   014E 90 30 04            901 	mov	dptr,#_char_count
   0151 E0                  902 	movx	a,@dptr
   0152 FB                  903 	mov	r3,a
   0153 A3                  904 	inc	dptr
   0154 E0                  905 	movx	a,@dptr
   0155 FC                  906 	mov	r4,a
                            907 ;	genPlus
   0156 90 30 04            908 	mov	dptr,#_char_count
                            909 ;     genPlusIncr
   0159 74 01               910 	mov	a,#0x01
                            911 ;	Peephole 236.a	used r3 instead of ar3
   015B 2B                  912 	add	a,r3
   015C F0                  913 	movx	@dptr,a
                            914 ;	Peephole 181	changed mov to clr
   015D E4                  915 	clr	a
                            916 ;	Peephole 236.b	used r4 instead of ar4
   015E 3C                  917 	addc	a,r4
   015F A3                  918 	inc	dptr
   0160 F0                  919 	movx	@dptr,a
   0161 02 00 A5            920 	ljmp	00128$
   0164                     921 00117$:
                            922 ;	main.c:85: else if (temp=='-')
                            923 ;	genCmpEq
                            924 ;	gencjneshort
                            925 ;	Peephole 112.b	changed ljmp to sjmp
                            926 ;	Peephole 198.b	optimized misc jump sequence
   0164 BA 2D 19            927 	cjne	r2,#0x2D,00114$
                            928 ;	Peephole 200.b	removed redundant sjmp
                            929 ;	Peephole 300	removed redundant label 00151$
                            930 ;	Peephole 300	removed redundant label 00152$
                            931 ;	main.c:87: char_count++;
                            932 ;	genAssign
   0167 90 30 04            933 	mov	dptr,#_char_count
   016A E0                  934 	movx	a,@dptr
   016B FB                  935 	mov	r3,a
   016C A3                  936 	inc	dptr
   016D E0                  937 	movx	a,@dptr
   016E FC                  938 	mov	r4,a
                            939 ;	genPlus
   016F 90 30 04            940 	mov	dptr,#_char_count
                            941 ;     genPlusIncr
   0172 74 01               942 	mov	a,#0x01
                            943 ;	Peephole 236.a	used r3 instead of ar3
   0174 2B                  944 	add	a,r3
   0175 F0                  945 	movx	@dptr,a
                            946 ;	Peephole 181	changed mov to clr
   0176 E4                  947 	clr	a
                            948 ;	Peephole 236.b	used r4 instead of ar4
   0177 3C                  949 	addc	a,r4
   0178 A3                  950 	inc	dptr
   0179 F0                  951 	movx	@dptr,a
                            952 ;	main.c:88: deletebuffer();
                            953 ;	genCall
   017A 12 05 5B            954 	lcall	_deletebuffer
   017D 02 00 A5            955 	ljmp	00128$
   0180                     956 00114$:
                            957 ;	main.c:90: else if (temp=='?')
                            958 ;	genCmpEq
                            959 ;	gencjneshort
                            960 ;	Peephole 112.b	changed ljmp to sjmp
                            961 ;	Peephole 198.b	optimized misc jump sequence
   0180 BA 3F 19            962 	cjne	r2,#0x3F,00111$
                            963 ;	Peephole 200.b	removed redundant sjmp
                            964 ;	Peephole 300	removed redundant label 00153$
                            965 ;	Peephole 300	removed redundant label 00154$
                            966 ;	main.c:92: char_count++;
                            967 ;	genAssign
   0183 90 30 04            968 	mov	dptr,#_char_count
   0186 E0                  969 	movx	a,@dptr
   0187 FB                  970 	mov	r3,a
   0188 A3                  971 	inc	dptr
   0189 E0                  972 	movx	a,@dptr
   018A FC                  973 	mov	r4,a
                            974 ;	genPlus
   018B 90 30 04            975 	mov	dptr,#_char_count
                            976 ;     genPlusIncr
   018E 74 01               977 	mov	a,#0x01
                            978 ;	Peephole 236.a	used r3 instead of ar3
   0190 2B                  979 	add	a,r3
   0191 F0                  980 	movx	@dptr,a
                            981 ;	Peephole 181	changed mov to clr
   0192 E4                  982 	clr	a
                            983 ;	Peephole 236.b	used r4 instead of ar4
   0193 3C                  984 	addc	a,r4
   0194 A3                  985 	inc	dptr
   0195 F0                  986 	movx	@dptr,a
                            987 ;	main.c:93: displaydelete();
                            988 ;	genCall
   0196 12 07 14            989 	lcall	_displaydelete
   0199 02 00 A5            990 	ljmp	00128$
   019C                     991 00111$:
                            992 ;	main.c:95: else if(temp=='=')
                            993 ;	genCmpEq
                            994 ;	gencjneshort
                            995 ;	Peephole 112.b	changed ljmp to sjmp
                            996 ;	Peephole 198.b	optimized misc jump sequence
   019C BA 3D 19            997 	cjne	r2,#0x3D,00108$
                            998 ;	Peephole 200.b	removed redundant sjmp
                            999 ;	Peephole 300	removed redundant label 00155$
                           1000 ;	Peephole 300	removed redundant label 00156$
                           1001 ;	main.c:97: char_count++;
                           1002 ;	genAssign
   019F 90 30 04           1003 	mov	dptr,#_char_count
   01A2 E0                 1004 	movx	a,@dptr
   01A3 FB                 1005 	mov	r3,a
   01A4 A3                 1006 	inc	dptr
   01A5 E0                 1007 	movx	a,@dptr
   01A6 FC                 1008 	mov	r4,a
                           1009 ;	genPlus
   01A7 90 30 04           1010 	mov	dptr,#_char_count
                           1011 ;     genPlusIncr
   01AA 74 01              1012 	mov	a,#0x01
                           1013 ;	Peephole 236.a	used r3 instead of ar3
   01AC 2B                 1014 	add	a,r3
   01AD F0                 1015 	movx	@dptr,a
                           1016 ;	Peephole 181	changed mov to clr
   01AE E4                 1017 	clr	a
                           1018 ;	Peephole 236.b	used r4 instead of ar4
   01AF 3C                 1019 	addc	a,r4
   01B0 A3                 1020 	inc	dptr
   01B1 F0                 1021 	movx	@dptr,a
                           1022 ;	main.c:98: display();
                           1023 ;	genCall
   01B2 12 0A F6           1024 	lcall	_display
   01B5 02 00 A5           1025 	ljmp	00128$
   01B8                    1026 00108$:
                           1027 ;	main.c:100: else if(temp=='@')
                           1028 ;	genCmpEq
                           1029 ;	gencjneshort
                           1030 ;	Peephole 112.b	changed ljmp to sjmp
                           1031 ;	Peephole 198.b	optimized misc jump sequence
   01B8 BA 40 18           1032 	cjne	r2,#0x40,00105$
                           1033 ;	Peephole 200.b	removed redundant sjmp
                           1034 ;	Peephole 300	removed redundant label 00157$
                           1035 ;	Peephole 300	removed redundant label 00158$
                           1036 ;	main.c:102: char_count++;
                           1037 ;	genAssign
   01BB 90 30 04           1038 	mov	dptr,#_char_count
   01BE E0                 1039 	movx	a,@dptr
   01BF FB                 1040 	mov	r3,a
   01C0 A3                 1041 	inc	dptr
   01C1 E0                 1042 	movx	a,@dptr
   01C2 FC                 1043 	mov	r4,a
                           1044 ;	genPlus
   01C3 90 30 04           1045 	mov	dptr,#_char_count
                           1046 ;     genPlusIncr
   01C6 74 01              1047 	mov	a,#0x01
                           1048 ;	Peephole 236.a	used r3 instead of ar3
   01C8 2B                 1049 	add	a,r3
   01C9 F0                 1050 	movx	@dptr,a
                           1051 ;	Peephole 181	changed mov to clr
   01CA E4                 1052 	clr	a
                           1053 ;	Peephole 236.b	used r4 instead of ar4
   01CB 3C                 1054 	addc	a,r4
   01CC A3                 1055 	inc	dptr
   01CD F0                 1056 	movx	@dptr,a
                           1057 ;	main.c:103: freeEverything();
                           1058 ;	genCall
   01CE 12 06 AE           1059 	lcall	_freeEverything
                           1060 ;	main.c:104: break;
                           1061 ;	Peephole 112.b	changed ljmp to sjmp
   01D1 80 08              1062 	sjmp	00129$
   01D3                    1063 00105$:
                           1064 ;	main.c:106: else if (temp=='del')
                           1065 ;	genCmpEq
                           1066 ;	gencjneshort
   01D3 BA 64 02           1067 	cjne	r2,#0x64,00159$
   01D6 80 03              1068 	sjmp	00160$
   01D8                    1069 00159$:
   01D8 02 00 A5           1070 	ljmp	00128$
   01DB                    1071 00160$:
                           1072 ;	main.c:107: {break;}
   01DB                    1073 00129$:
                           1074 ;	main.c:115: buffer_count=2;
                           1075 ;	genAssign
   01DB 90 30 00           1076 	mov	dptr,#_buffer_count
   01DE 74 02              1077 	mov	a,#0x02
   01E0 F0                 1078 	movx	@dptr,a
   01E1 E4                 1079 	clr	a
   01E2 A3                 1080 	inc	dptr
   01E3 F0                 1081 	movx	@dptr,a
                           1082 ;	main.c:116: buffer_number=0;
                           1083 ;	genAssign
   01E4 90 30 06           1084 	mov	dptr,#_buffer_number
   01E7 E4                 1085 	clr	a
   01E8 F0                 1086 	movx	@dptr,a
   01E9 A3                 1087 	inc	dptr
   01EA F0                 1088 	movx	@dptr,a
                           1089 ;	main.c:117: goto here;
   01EB 02 00 A2           1090 	ljmp	00101$
                           1091 ;	Peephole 259.b	removed redundant label 00130$ and ret
                           1092 ;
                           1093 ;------------------------------------------------------------
                           1094 ;Allocation info for local variables in function 'BufferInitialize'
                           1095 ;------------------------------------------------------------
                           1096 ;z                         Allocated with name '_BufferInitialize_z_1_1'
                           1097 ;------------------------------------------------------------
                           1098 ;	main.c:120: void BufferInitialize()
                           1099 ;	-----------------------------------------
                           1100 ;	 function BufferInitialize
                           1101 ;	-----------------------------------------
   01EE                    1102 _BufferInitialize:
                           1103 ;	main.c:125: putstr("Input buffer size between 32 and 2800 bytes, divisible by 16\n\r");
                           1104 ;	genCall
                           1105 ;	Peephole 182.a	used 16 bit load of DPTR
   01EE 90 12 D5           1106 	mov	dptr,#__str_2
   01F1 75 F0 80           1107 	mov	b,#0x80
   01F4 12 0C F8           1108 	lcall	_putstr
                           1109 ;	main.c:126: do
   01F7                    1110 00113$:
                           1111 ;	main.c:128: res=ReadValue();
                           1112 ;	genCall
   01F7 12 03 B9           1113 	lcall	_ReadValue
   01FA AA 82              1114 	mov	r2,dpl
   01FC AB 83              1115 	mov	r3,dph
                           1116 ;	genAssign
   01FE 90 34 19           1117 	mov	dptr,#_res
   0201 EA                 1118 	mov	a,r2
   0202 F0                 1119 	movx	@dptr,a
   0203 A3                 1120 	inc	dptr
   0204 EB                 1121 	mov	a,r3
   0205 F0                 1122 	movx	@dptr,a
                           1123 ;	main.c:131: if(res>31 && res<2801)
                           1124 ;	genCmpGt
                           1125 ;	genCmp
   0206 C3                 1126 	clr	c
   0207 74 1F              1127 	mov	a,#0x1F
   0209 9A                 1128 	subb	a,r2
                           1129 ;	Peephole 159	avoided xrl during execution
   020A 74 80              1130 	mov	a,#(0x00 ^ 0x80)
   020C 8B F0              1131 	mov	b,r3
   020E 63 F0 80           1132 	xrl	b,#0x80
   0211 95 F0              1133 	subb	a,b
                           1134 ;	genIfxJump
   0213 40 03              1135 	jc	00126$
   0215 02 02 AF           1136 	ljmp	00109$
   0218                    1137 00126$:
                           1138 ;	genCmpLt
                           1139 ;	genCmp
   0218 C3                 1140 	clr	c
   0219 EA                 1141 	mov	a,r2
   021A 94 F1              1142 	subb	a,#0xF1
   021C EB                 1143 	mov	a,r3
   021D 64 80              1144 	xrl	a,#0x80
   021F 94 8A              1145 	subb	a,#0x8a
                           1146 ;	genIfxJump
   0221 40 03              1147 	jc	00127$
   0223 02 02 AF           1148 	ljmp	00109$
   0226                    1149 00127$:
                           1150 ;	main.c:133: if(res%16==0)
                           1151 ;	genAssign
   0226 90 34 52           1152 	mov	dptr,#__modsint_PARM_2
   0229 74 10              1153 	mov	a,#0x10
   022B F0                 1154 	movx	@dptr,a
   022C E4                 1155 	clr	a
   022D A3                 1156 	inc	dptr
   022E F0                 1157 	movx	@dptr,a
                           1158 ;	genCall
   022F 8A 82              1159 	mov	dpl,r2
   0231 8B 83              1160 	mov	dph,r3
   0233 C0 02              1161 	push	ar2
   0235 C0 03              1162 	push	ar3
   0237 12 11 82           1163 	lcall	__modsint
   023A E5 82              1164 	mov	a,dpl
   023C 85 83 F0           1165 	mov	b,dph
   023F D0 03              1166 	pop	ar3
   0241 D0 02              1167 	pop	ar2
                           1168 ;	genIfx
   0243 45 F0              1169 	orl	a,b
                           1170 ;	genIfxJump
                           1171 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0245 70 5D              1172 	jnz	00106$
                           1173 ;	Peephole 300	removed redundant label 00128$
                           1174 ;	main.c:135: if ((buffer[0] = malloc((unsigned int)z * res)) == 0)  //allocate buffer0
                           1175 ;	genCall
   0247 8A 82              1176 	mov	dpl,r2
   0249 8B 83              1177 	mov	dph,r3
   024B 12 0F 05           1178 	lcall	_malloc
   024E AA 82              1179 	mov	r2,dpl
   0250 AB 83              1180 	mov	r3,dph
                           1181 ;	genPointerSet
                           1182 ;     genFarPointerSet
   0252 90 30 0A           1183 	mov	dptr,#_buffer
   0255 EA                 1184 	mov	a,r2
   0256 F0                 1185 	movx	@dptr,a
   0257 A3                 1186 	inc	dptr
   0258 EB                 1187 	mov	a,r3
   0259 F0                 1188 	movx	@dptr,a
                           1189 ;	genIfx
   025A EA                 1190 	mov	a,r2
   025B 4B                 1191 	orl	a,r3
                           1192 ;	genIfxJump
                           1193 ;	Peephole 108.b	removed ljmp by inverse jump logic
   025C 70 09              1194 	jnz	00102$
                           1195 ;	Peephole 300	removed redundant label 00129$
                           1196 ;	main.c:136: {putstr("malloc buffer0 failed\n\r");}
                           1197 ;	genCall
                           1198 ;	Peephole 182.a	used 16 bit load of DPTR
   025E 90 13 14           1199 	mov	dptr,#__str_3
   0261 75 F0 80           1200 	mov	b,#0x80
   0264 12 0C F8           1201 	lcall	_putstr
   0267                    1202 00102$:
                           1203 ;	main.c:137: if ((buffer[1] = malloc((unsigned int)z * res)) == 0)         //allocate buffer1
                           1204 ;	genAssign
   0267 90 34 19           1205 	mov	dptr,#_res
   026A E0                 1206 	movx	a,@dptr
   026B FA                 1207 	mov	r2,a
   026C A3                 1208 	inc	dptr
   026D E0                 1209 	movx	a,@dptr
   026E FB                 1210 	mov	r3,a
                           1211 ;	genCall
   026F 8A 82              1212 	mov	dpl,r2
   0271 8B 83              1213 	mov	dph,r3
   0273 12 0F 05           1214 	lcall	_malloc
   0276 AA 82              1215 	mov	r2,dpl
   0278 AB 83              1216 	mov	r3,dph
                           1217 ;	genPointerSet
                           1218 ;     genFarPointerSet
   027A 90 30 0C           1219 	mov	dptr,#(_buffer + 0x0002)
   027D EA                 1220 	mov	a,r2
   027E F0                 1221 	movx	@dptr,a
   027F A3                 1222 	inc	dptr
   0280 EB                 1223 	mov	a,r3
   0281 F0                 1224 	movx	@dptr,a
                           1225 ;	genIfx
   0282 EA                 1226 	mov	a,r2
   0283 4B                 1227 	orl	a,r3
                           1228 ;	genIfxJump
                           1229 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0284 70 32              1230 	jnz	00114$
                           1231 ;	Peephole 300	removed redundant label 00130$
                           1232 ;	main.c:139: putstr("malloc buffer1 failed\n\r");
                           1233 ;	genCall
                           1234 ;	Peephole 182.a	used 16 bit load of DPTR
   0286 90 13 2C           1235 	mov	dptr,#__str_4
   0289 75 F0 80           1236 	mov	b,#0x80
   028C 12 0C F8           1237 	lcall	_putstr
                           1238 ;	main.c:140: free (buffer[0]);  // if buffer1 malloc fails, free buffer 0
                           1239 ;	genPointerGet
                           1240 ;	genFarPointerGet
   028F 90 30 0A           1241 	mov	dptr,#_buffer
   0292 E0                 1242 	movx	a,@dptr
   0293 FA                 1243 	mov	r2,a
   0294 A3                 1244 	inc	dptr
   0295 E0                 1245 	movx	a,@dptr
   0296 FB                 1246 	mov	r3,a
                           1247 ;	genCast
   0297 7C 00              1248 	mov	r4,#0x0
                           1249 ;	genCall
   0299 8A 82              1250 	mov	dpl,r2
   029B 8B 83              1251 	mov	dph,r3
   029D 8C F0              1252 	mov	b,r4
   029F 12 0E 31           1253 	lcall	_free
                           1254 ;	Peephole 112.b	changed ljmp to sjmp
   02A2 80 14              1255 	sjmp	00114$
   02A4                    1256 00106$:
                           1257 ;	main.c:144: putstr("Enter the correct buffer size value divisible by 16\n\r");
                           1258 ;	genCall
                           1259 ;	Peephole 182.a	used 16 bit load of DPTR
   02A4 90 13 44           1260 	mov	dptr,#__str_5
   02A7 75 F0 80           1261 	mov	b,#0x80
   02AA 12 0C F8           1262 	lcall	_putstr
                           1263 ;	Peephole 112.b	changed ljmp to sjmp
   02AD 80 09              1264 	sjmp	00114$
   02AF                    1265 00109$:
                           1266 ;	main.c:147: putstr("Enter the correct buffer size between 32 and 2800\n\r");
                           1267 ;	genCall
                           1268 ;	Peephole 182.a	used 16 bit load of DPTR
   02AF 90 13 7A           1269 	mov	dptr,#__str_6
   02B2 75 F0 80           1270 	mov	b,#0x80
   02B5 12 0C F8           1271 	lcall	_putstr
   02B8                    1272 00114$:
                           1273 ;	main.c:148: } while((buffer[0] == 0)||(buffer[1] ==0));
                           1274 ;	genPointerGet
                           1275 ;	genFarPointerGet
   02B8 90 30 0A           1276 	mov	dptr,#_buffer
   02BB E0                 1277 	movx	a,@dptr
   02BC FA                 1278 	mov	r2,a
   02BD A3                 1279 	inc	dptr
   02BE E0                 1280 	movx	a,@dptr
                           1281 ;	genIfx
   02BF FB                 1282 	mov	r3,a
                           1283 ;	Peephole 135	removed redundant mov
   02C0 4A                 1284 	orl	a,r2
                           1285 ;	genIfxJump
   02C1 70 03              1286 	jnz	00131$
   02C3 02 01 F7           1287 	ljmp	00113$
   02C6                    1288 00131$:
                           1289 ;	genPointerGet
                           1290 ;	genFarPointerGet
   02C6 90 30 0C           1291 	mov	dptr,#(_buffer + 0x0002)
   02C9 E0                 1292 	movx	a,@dptr
   02CA FA                 1293 	mov	r2,a
   02CB A3                 1294 	inc	dptr
   02CC E0                 1295 	movx	a,@dptr
                           1296 ;	genIfx
   02CD FB                 1297 	mov	r3,a
                           1298 ;	Peephole 135	removed redundant mov
   02CE 4A                 1299 	orl	a,r2
                           1300 ;	genIfxJump
   02CF 70 03              1301 	jnz	00132$
   02D1 02 01 F7           1302 	ljmp	00113$
   02D4                    1303 00132$:
                           1304 ;	main.c:149: putstr("Buffer allocation successful\n\r");
                           1305 ;	genCall
                           1306 ;	Peephole 182.a	used 16 bit load of DPTR
   02D4 90 13 AE           1307 	mov	dptr,#__str_7
   02D7 75 F0 80           1308 	mov	b,#0x80
   02DA 12 0C F8           1309 	lcall	_putstr
                           1310 ;	main.c:150: buffer_number=buffer_number+2;
                           1311 ;	genAssign
   02DD 90 30 06           1312 	mov	dptr,#_buffer_number
   02E0 E0                 1313 	movx	a,@dptr
   02E1 FA                 1314 	mov	r2,a
   02E2 A3                 1315 	inc	dptr
   02E3 E0                 1316 	movx	a,@dptr
   02E4 FB                 1317 	mov	r3,a
                           1318 ;	genPlus
   02E5 90 30 06           1319 	mov	dptr,#_buffer_number
                           1320 ;     genPlusIncr
   02E8 74 02              1321 	mov	a,#0x02
                           1322 ;	Peephole 236.a	used r2 instead of ar2
   02EA 2A                 1323 	add	a,r2
   02EB F0                 1324 	movx	@dptr,a
                           1325 ;	Peephole 181	changed mov to clr
   02EC E4                 1326 	clr	a
                           1327 ;	Peephole 236.b	used r3 instead of ar3
   02ED 3B                 1328 	addc	a,r3
   02EE A3                 1329 	inc	dptr
   02EF F0                 1330 	movx	@dptr,a
                           1331 ;	main.c:151: bufflen[0]=res;
                           1332 ;	genAssign
   02F0 90 34 19           1333 	mov	dptr,#_res
   02F3 E0                 1334 	movx	a,@dptr
   02F4 FA                 1335 	mov	r2,a
   02F5 A3                 1336 	inc	dptr
   02F6 E0                 1337 	movx	a,@dptr
   02F7 FB                 1338 	mov	r3,a
                           1339 ;	genPointerSet
                           1340 ;     genFarPointerSet
   02F8 90 32 10           1341 	mov	dptr,#_bufflen
   02FB EA                 1342 	mov	a,r2
   02FC F0                 1343 	movx	@dptr,a
   02FD A3                 1344 	inc	dptr
   02FE EB                 1345 	mov	a,r3
   02FF F0                 1346 	movx	@dptr,a
                           1347 ;	main.c:152: bufflen[1]=res;
                           1348 ;	genPointerSet
                           1349 ;     genFarPointerSet
   0300 90 32 12           1350 	mov	dptr,#(_bufflen + 0x0002)
   0303 EA                 1351 	mov	a,r2
   0304 F0                 1352 	movx	@dptr,a
   0305 A3                 1353 	inc	dptr
   0306 EB                 1354 	mov	a,r3
   0307 F0                 1355 	movx	@dptr,a
                           1356 ;	main.c:153: printf_tiny("len1 = %d\n\r", bufflen[0]);
                           1357 ;	genPointerGet
                           1358 ;	genFarPointerGet
   0308 90 32 10           1359 	mov	dptr,#_bufflen
   030B E0                 1360 	movx	a,@dptr
   030C FA                 1361 	mov	r2,a
   030D A3                 1362 	inc	dptr
   030E E0                 1363 	movx	a,@dptr
   030F FB                 1364 	mov	r3,a
                           1365 ;	genIpush
   0310 C0 02              1366 	push	ar2
   0312 C0 03              1367 	push	ar3
                           1368 ;	genIpush
   0314 74 CD              1369 	mov	a,#__str_8
   0316 C0 E0              1370 	push	acc
   0318 74 13              1371 	mov	a,#(__str_8 >> 8)
   031A C0 E0              1372 	push	acc
                           1373 ;	genCall
   031C 12 10 5A           1374 	lcall	_printf_tiny
   031F E5 81              1375 	mov	a,sp
   0321 24 FC              1376 	add	a,#0xfc
   0323 F5 81              1377 	mov	sp,a
                           1378 ;	main.c:154: printf_tiny("len2 = %d\n\r", bufflen[1]);
                           1379 ;	genPointerGet
                           1380 ;	genFarPointerGet
   0325 90 32 12           1381 	mov	dptr,#(_bufflen + 0x0002)
   0328 E0                 1382 	movx	a,@dptr
   0329 FA                 1383 	mov	r2,a
   032A A3                 1384 	inc	dptr
   032B E0                 1385 	movx	a,@dptr
   032C FB                 1386 	mov	r3,a
                           1387 ;	genIpush
   032D C0 02              1388 	push	ar2
   032F C0 03              1389 	push	ar3
                           1390 ;	genIpush
   0331 74 D9              1391 	mov	a,#__str_9
   0333 C0 E0              1392 	push	acc
   0335 74 13              1393 	mov	a,#(__str_9 >> 8)
   0337 C0 E0              1394 	push	acc
                           1395 ;	genCall
   0339 12 10 5A           1396 	lcall	_printf_tiny
   033C E5 81              1397 	mov	a,sp
   033E 24 FC              1398 	add	a,#0xfc
   0340 F5 81              1399 	mov	sp,a
                           1400 ;	main.c:155: buff1ptr=buffer[0];
                           1401 ;	genPointerGet
                           1402 ;	genFarPointerGet
   0342 90 30 0A           1403 	mov	dptr,#_buffer
   0345 E0                 1404 	movx	a,@dptr
   0346 FA                 1405 	mov	r2,a
   0347 A3                 1406 	inc	dptr
   0348 E0                 1407 	movx	a,@dptr
   0349 FB                 1408 	mov	r3,a
                           1409 ;	genCast
   034A 90 32 0A           1410 	mov	dptr,#_buff1ptr
   034D EA                 1411 	mov	a,r2
   034E F0                 1412 	movx	@dptr,a
   034F A3                 1413 	inc	dptr
   0350 EB                 1414 	mov	a,r3
   0351 F0                 1415 	movx	@dptr,a
   0352 A3                 1416 	inc	dptr
   0353 74 00              1417 	mov	a,#0x0
   0355 F0                 1418 	movx	@dptr,a
                           1419 ;	main.c:156: buff2ptr=buffer[1];
                           1420 ;	genPointerGet
                           1421 ;	genFarPointerGet
   0356 90 30 0C           1422 	mov	dptr,#(_buffer + 0x0002)
   0359 E0                 1423 	movx	a,@dptr
   035A FC                 1424 	mov	r4,a
   035B A3                 1425 	inc	dptr
   035C E0                 1426 	movx	a,@dptr
   035D FD                 1427 	mov	r5,a
                           1428 ;	genCast
   035E 90 32 0D           1429 	mov	dptr,#_buff2ptr
   0361 EC                 1430 	mov	a,r4
   0362 F0                 1431 	movx	@dptr,a
   0363 A3                 1432 	inc	dptr
   0364 ED                 1433 	mov	a,r5
   0365 F0                 1434 	movx	@dptr,a
   0366 A3                 1435 	inc	dptr
   0367 74 00              1436 	mov	a,#0x0
   0369 F0                 1437 	movx	@dptr,a
                           1438 ;	main.c:157: bptr0=buffer[0];
                           1439 ;	genAssign
   036A 8A 06              1440 	mov	ar6,r2
   036C 8B 07              1441 	mov	ar7,r3
                           1442 ;	genCast
   036E 90 34 1B           1443 	mov	dptr,#_bptr0
   0371 EE                 1444 	mov	a,r6
   0372 F0                 1445 	movx	@dptr,a
   0373 A3                 1446 	inc	dptr
   0374 EF                 1447 	mov	a,r7
   0375 F0                 1448 	movx	@dptr,a
   0376 A3                 1449 	inc	dptr
   0377 74 00              1450 	mov	a,#0x0
   0379 F0                 1451 	movx	@dptr,a
                           1452 ;	main.c:158: bptr1=buffer[1];
                           1453 ;	genAssign
                           1454 ;	genCast
   037A 90 34 1E           1455 	mov	dptr,#_bptr1
   037D EC                 1456 	mov	a,r4
   037E F0                 1457 	movx	@dptr,a
   037F A3                 1458 	inc	dptr
   0380 ED                 1459 	mov	a,r5
   0381 F0                 1460 	movx	@dptr,a
   0382 A3                 1461 	inc	dptr
   0383 74 00              1462 	mov	a,#0x0
   0385 F0                 1463 	movx	@dptr,a
                           1464 ;	main.c:159: printf_tiny("Buffer0 address 0x%x\n\r", (unsigned int) buffer[0]);
                           1465 ;	genAssign
                           1466 ;	genCast
                           1467 ;	genIpush
   0386 C0 02              1468 	push	ar2
   0388 C0 03              1469 	push	ar3
                           1470 ;	genIpush
   038A 74 E5              1471 	mov	a,#__str_10
   038C C0 E0              1472 	push	acc
   038E 74 13              1473 	mov	a,#(__str_10 >> 8)
   0390 C0 E0              1474 	push	acc
                           1475 ;	genCall
   0392 12 10 5A           1476 	lcall	_printf_tiny
   0395 E5 81              1477 	mov	a,sp
   0397 24 FC              1478 	add	a,#0xfc
   0399 F5 81              1479 	mov	sp,a
                           1480 ;	main.c:160: printf_tiny("Buffer1 address 0x%x\n\r", (unsigned int) buffer[1]);
                           1481 ;	genPointerGet
                           1482 ;	genFarPointerGet
   039B 90 30 0C           1483 	mov	dptr,#(_buffer + 0x0002)
   039E E0                 1484 	movx	a,@dptr
   039F FA                 1485 	mov	r2,a
   03A0 A3                 1486 	inc	dptr
   03A1 E0                 1487 	movx	a,@dptr
   03A2 FB                 1488 	mov	r3,a
                           1489 ;	genCast
                           1490 ;	genIpush
   03A3 C0 02              1491 	push	ar2
   03A5 C0 03              1492 	push	ar3
                           1493 ;	genIpush
   03A7 74 FC              1494 	mov	a,#__str_11
   03A9 C0 E0              1495 	push	acc
   03AB 74 13              1496 	mov	a,#(__str_11 >> 8)
   03AD C0 E0              1497 	push	acc
                           1498 ;	genCall
   03AF 12 10 5A           1499 	lcall	_printf_tiny
   03B2 E5 81              1500 	mov	a,sp
   03B4 24 FC              1501 	add	a,#0xfc
   03B6 F5 81              1502 	mov	sp,a
                           1503 ;	Peephole 300	removed redundant label 00116$
   03B8 22                 1504 	ret
                           1505 ;------------------------------------------------------------
                           1506 ;Allocation info for local variables in function 'ReadValue'
                           1507 ;------------------------------------------------------------
                           1508 ;------------------------------------------------------------
                           1509 ;	main.c:163: int ReadValue()
                           1510 ;	-----------------------------------------
                           1511 ;	 function ReadValue
                           1512 ;	-----------------------------------------
   03B9                    1513 _ReadValue:
                           1514 ;	main.c:165: value=0;
                           1515 ;	genAssign
   03B9 90 34 17           1516 	mov	dptr,#_value
   03BC E4                 1517 	clr	a
   03BD F0                 1518 	movx	@dptr,a
   03BE A3                 1519 	inc	dptr
   03BF F0                 1520 	movx	@dptr,a
                           1521 ;	main.c:166: while(j<4)
   03C0                    1522 00101$:
                           1523 ;	genAssign
   03C0 90 34 13           1524 	mov	dptr,#_j
   03C3 E0                 1525 	movx	a,@dptr
   03C4 FA                 1526 	mov	r2,a
                           1527 ;	genCmpLt
                           1528 ;	genCmp
   03C5 BA 04 00           1529 	cjne	r2,#0x04,00108$
   03C8                    1530 00108$:
                           1531 ;	genIfxJump
                           1532 ;	Peephole 108.a	removed ljmp by inverse jump logic
   03C8 50 57              1533 	jnc	00103$
                           1534 ;	Peephole 300	removed redundant label 00109$
                           1535 ;	main.c:168: i=getchar();
                           1536 ;	genCall
   03CA 12 0D 64           1537 	lcall	_getchar
   03CD AA 82              1538 	mov	r2,dpl
                           1539 ;	genAssign
   03CF 90 34 5B           1540 	mov	dptr,#_i
   03D2 EA                 1541 	mov	a,r2
   03D3 F0                 1542 	movx	@dptr,a
                           1543 ;	main.c:169: value=((value*10)+(i-'0'));
                           1544 ;	genAssign
   03D4 90 34 17           1545 	mov	dptr,#_value
   03D7 E0                 1546 	movx	a,@dptr
   03D8 FB                 1547 	mov	r3,a
   03D9 A3                 1548 	inc	dptr
   03DA E0                 1549 	movx	a,@dptr
   03DB FC                 1550 	mov	r4,a
                           1551 ;	genAssign
   03DC 90 34 50           1552 	mov	dptr,#__mulint_PARM_2
   03DF 74 0A              1553 	mov	a,#0x0A
   03E1 F0                 1554 	movx	@dptr,a
   03E2 E4                 1555 	clr	a
   03E3 A3                 1556 	inc	dptr
   03E4 F0                 1557 	movx	@dptr,a
                           1558 ;	genCall
   03E5 8B 82              1559 	mov	dpl,r3
   03E7 8C 83              1560 	mov	dph,r4
   03E9 C0 02              1561 	push	ar2
   03EB 12 11 62           1562 	lcall	__mulint
   03EE AB 82              1563 	mov	r3,dpl
   03F0 AC 83              1564 	mov	r4,dph
   03F2 D0 02              1565 	pop	ar2
                           1566 ;	genAssign
   03F4 8A 05              1567 	mov	ar5,r2
                           1568 ;	genCast
   03F6 7E 00              1569 	mov	r6,#0x00
                           1570 ;	genMinus
   03F8 ED                 1571 	mov	a,r5
   03F9 24 D0              1572 	add	a,#0xd0
   03FB FD                 1573 	mov	r5,a
   03FC EE                 1574 	mov	a,r6
   03FD 34 FF              1575 	addc	a,#0xff
   03FF FE                 1576 	mov	r6,a
                           1577 ;	genPlus
   0400 90 34 17           1578 	mov	dptr,#_value
                           1579 ;	Peephole 236.g	used r5 instead of ar5
   0403 ED                 1580 	mov	a,r5
                           1581 ;	Peephole 236.a	used r3 instead of ar3
   0404 2B                 1582 	add	a,r3
   0405 F0                 1583 	movx	@dptr,a
                           1584 ;	Peephole 236.g	used r6 instead of ar6
   0406 EE                 1585 	mov	a,r6
                           1586 ;	Peephole 236.b	used r4 instead of ar4
   0407 3C                 1587 	addc	a,r4
   0408 A3                 1588 	inc	dptr
   0409 F0                 1589 	movx	@dptr,a
                           1590 ;	main.c:170: putchar(i);
                           1591 ;	genAssign
                           1592 ;	genCast
   040A 7B 00              1593 	mov	r3,#0x00
                           1594 ;	genCall
   040C 8A 82              1595 	mov	dpl,r2
   040E 8B 83              1596 	mov	dph,r3
   0410 12 0D 73           1597 	lcall	_putchar
                           1598 ;	main.c:171: j++;
                           1599 ;	genAssign
   0413 90 34 13           1600 	mov	dptr,#_j
   0416 E0                 1601 	movx	a,@dptr
   0417 FA                 1602 	mov	r2,a
                           1603 ;	genPlus
   0418 90 34 13           1604 	mov	dptr,#_j
                           1605 ;     genPlusIncr
   041B 74 01              1606 	mov	a,#0x01
                           1607 ;	Peephole 236.a	used r2 instead of ar2
   041D 2A                 1608 	add	a,r2
   041E F0                 1609 	movx	@dptr,a
                           1610 ;	Peephole 112.b	changed ljmp to sjmp
   041F 80 9F              1611 	sjmp	00101$
   0421                    1612 00103$:
                           1613 ;	main.c:173: j=0;
                           1614 ;	genAssign
   0421 90 34 13           1615 	mov	dptr,#_j
                           1616 ;	Peephole 181	changed mov to clr
   0424 E4                 1617 	clr	a
   0425 F0                 1618 	movx	@dptr,a
                           1619 ;	main.c:174: return value;
                           1620 ;	genAssign
   0426 90 34 17           1621 	mov	dptr,#_value
   0429 E0                 1622 	movx	a,@dptr
   042A FA                 1623 	mov	r2,a
   042B A3                 1624 	inc	dptr
   042C E0                 1625 	movx	a,@dptr
                           1626 ;	genRet
                           1627 ;	Peephole 234.b	loading dph directly from a(ccumulator), r3 not set
   042D 8A 82              1628 	mov	dpl,r2
   042F F5 83              1629 	mov	dph,a
                           1630 ;	Peephole 300	removed redundant label 00104$
   0431 22                 1631 	ret
                           1632 ;------------------------------------------------------------
                           1633 ;Allocation info for local variables in function 'newbuffer'
                           1634 ;------------------------------------------------------------
                           1635 ;------------------------------------------------------------
                           1636 ;	main.c:177: void newbuffer()
                           1637 ;	-----------------------------------------
                           1638 ;	 function newbuffer
                           1639 ;	-----------------------------------------
   0432                    1640 _newbuffer:
                           1641 ;	main.c:179: putstr("Enter the buffer size between 20 and 400\n\r");
                           1642 ;	genCall
                           1643 ;	Peephole 182.a	used 16 bit load of DPTR
   0432 90 14 13           1644 	mov	dptr,#__str_12
   0435 75 F0 80           1645 	mov	b,#0x80
   0438 12 0C F8           1646 	lcall	_putstr
                           1647 ;	main.c:180: do
   043B                    1648 00108$:
                           1649 ;	main.c:182: res = ReadValue();
                           1650 ;	genCall
   043B 12 03 B9           1651 	lcall	_ReadValue
   043E AA 82              1652 	mov	r2,dpl
   0440 AB 83              1653 	mov	r3,dph
                           1654 ;	genAssign
   0442 90 34 19           1655 	mov	dptr,#_res
   0445 EA                 1656 	mov	a,r2
   0446 F0                 1657 	movx	@dptr,a
   0447 A3                 1658 	inc	dptr
   0448 EB                 1659 	mov	a,r3
   0449 F0                 1660 	movx	@dptr,a
                           1661 ;	main.c:183: if(res>20 && res<400)
                           1662 ;	genCmpGt
                           1663 ;	genCmp
   044A C3                 1664 	clr	c
   044B 74 14              1665 	mov	a,#0x14
   044D 9A                 1666 	subb	a,r2
                           1667 ;	Peephole 159	avoided xrl during execution
   044E 74 80              1668 	mov	a,#(0x00 ^ 0x80)
   0450 8B F0              1669 	mov	b,r3
   0452 63 F0 80           1670 	xrl	b,#0x80
   0455 95 F0              1671 	subb	a,b
                           1672 ;	genIfxJump
   0457 40 03              1673 	jc	00118$
   0459 02 05 0E           1674 	ljmp	00105$
   045C                    1675 00118$:
                           1676 ;	genCmpLt
                           1677 ;	genCmp
   045C C3                 1678 	clr	c
   045D EA                 1679 	mov	a,r2
   045E 94 90              1680 	subb	a,#0x90
   0460 EB                 1681 	mov	a,r3
   0461 64 80              1682 	xrl	a,#0x80
   0463 94 81              1683 	subb	a,#0x81
                           1684 ;	genIfxJump
   0465 40 03              1685 	jc	00119$
   0467 02 05 0E           1686 	ljmp	00105$
   046A                    1687 00119$:
                           1688 ;	main.c:185: if((buffer[buffer_count]=malloc(sizeof(int)*res))==0)
                           1689 ;	genAssign
   046A 90 30 00           1690 	mov	dptr,#_buffer_count
   046D E0                 1691 	movx	a,@dptr
   046E FC                 1692 	mov	r4,a
   046F A3                 1693 	inc	dptr
   0470 E0                 1694 	movx	a,@dptr
                           1695 ;	genLeftShift
                           1696 ;	genLeftShiftLiteral
                           1697 ;	genlshTwo
   0471 FD                 1698 	mov	r5,a
                           1699 ;	Peephole 105	removed redundant mov
   0472 CC                 1700 	xch	a,r4
   0473 25 E0              1701 	add	a,acc
   0475 CC                 1702 	xch	a,r4
   0476 33                 1703 	rlc	a
   0477 FD                 1704 	mov	r5,a
                           1705 ;	genPlus
                           1706 ;	Peephole 236.g	used r4 instead of ar4
   0478 EC                 1707 	mov	a,r4
   0479 24 0A              1708 	add	a,#_buffer
   047B FC                 1709 	mov	r4,a
                           1710 ;	Peephole 236.g	used r5 instead of ar5
   047C ED                 1711 	mov	a,r5
   047D 34 30              1712 	addc	a,#(_buffer >> 8)
   047F FD                 1713 	mov	r5,a
                           1714 ;	genLeftShift
                           1715 ;	genLeftShiftLiteral
                           1716 ;	genlshTwo
   0480 EB                 1717 	mov	a,r3
   0481 CA                 1718 	xch	a,r2
   0482 25 E0              1719 	add	a,acc
   0484 CA                 1720 	xch	a,r2
   0485 33                 1721 	rlc	a
   0486 FB                 1722 	mov	r3,a
                           1723 ;	genCall
   0487 8A 82              1724 	mov	dpl,r2
   0489 8B 83              1725 	mov	dph,r3
   048B C0 04              1726 	push	ar4
   048D C0 05              1727 	push	ar5
   048F 12 0F 05           1728 	lcall	_malloc
   0492 AA 82              1729 	mov	r2,dpl
   0494 AB 83              1730 	mov	r3,dph
   0496 D0 05              1731 	pop	ar5
   0498 D0 04              1732 	pop	ar4
                           1733 ;	genPointerSet
                           1734 ;     genFarPointerSet
   049A 8C 82              1735 	mov	dpl,r4
   049C 8D 83              1736 	mov	dph,r5
   049E EA                 1737 	mov	a,r2
   049F F0                 1738 	movx	@dptr,a
   04A0 A3                 1739 	inc	dptr
   04A1 EB                 1740 	mov	a,r3
   04A2 F0                 1741 	movx	@dptr,a
                           1742 ;	genIfx
   04A3 EA                 1743 	mov	a,r2
   04A4 4B                 1744 	orl	a,r3
                           1745 ;	genIfxJump
                           1746 ;	Peephole 108.b	removed ljmp by inverse jump logic
   04A5 70 0B              1747 	jnz	00102$
                           1748 ;	Peephole 300	removed redundant label 00120$
                           1749 ;	main.c:186: putstr("\n\rBuffer allocation failed\n\r");
                           1750 ;	genCall
                           1751 ;	Peephole 182.a	used 16 bit load of DPTR
   04A7 90 14 3E           1752 	mov	dptr,#__str_13
   04AA 75 F0 80           1753 	mov	b,#0x80
   04AD 12 0C F8           1754 	lcall	_putstr
                           1755 ;	Peephole 112.b	changed ljmp to sjmp
   04B0 80 65              1756 	sjmp	00109$
   04B2                    1757 00102$:
                           1758 ;	main.c:188: {putstr("Buffer Allocated\n\r");
                           1759 ;	genCall
                           1760 ;	Peephole 182.a	used 16 bit load of DPTR
   04B2 90 14 5B           1761 	mov	dptr,#__str_14
   04B5 75 F0 80           1762 	mov	b,#0x80
   04B8 12 0C F8           1763 	lcall	_putstr
                           1764 ;	main.c:189: bufflen[buffer_count]=res;
                           1765 ;	genAssign
   04BB 90 30 00           1766 	mov	dptr,#_buffer_count
   04BE E0                 1767 	movx	a,@dptr
   04BF FA                 1768 	mov	r2,a
   04C0 A3                 1769 	inc	dptr
   04C1 E0                 1770 	movx	a,@dptr
                           1771 ;	genLeftShift
                           1772 ;	genLeftShiftLiteral
                           1773 ;	genlshTwo
   04C2 FB                 1774 	mov	r3,a
   04C3 8A 04              1775 	mov	ar4,r2
                           1776 ;	Peephole 177.d	removed redundant move
   04C5 CC                 1777 	xch	a,r4
   04C6 25 E0              1778 	add	a,acc
   04C8 CC                 1779 	xch	a,r4
   04C9 33                 1780 	rlc	a
   04CA FD                 1781 	mov	r5,a
                           1782 ;	genPlus
                           1783 ;	Peephole 236.g	used r4 instead of ar4
   04CB EC                 1784 	mov	a,r4
   04CC 24 10              1785 	add	a,#_bufflen
   04CE FE                 1786 	mov	r6,a
                           1787 ;	Peephole 236.g	used r5 instead of ar5
   04CF ED                 1788 	mov	a,r5
   04D0 34 32              1789 	addc	a,#(_bufflen >> 8)
   04D2 FF                 1790 	mov	r7,a
                           1791 ;	genAssign
   04D3 90 34 19           1792 	mov	dptr,#_res
   04D6 E0                 1793 	movx	a,@dptr
   04D7 F8                 1794 	mov	r0,a
   04D8 A3                 1795 	inc	dptr
   04D9 E0                 1796 	movx	a,@dptr
   04DA F9                 1797 	mov	r1,a
                           1798 ;	genPointerSet
                           1799 ;     genFarPointerSet
   04DB 8E 82              1800 	mov	dpl,r6
   04DD 8F 83              1801 	mov	dph,r7
   04DF E8                 1802 	mov	a,r0
   04E0 F0                 1803 	movx	@dptr,a
   04E1 A3                 1804 	inc	dptr
   04E2 E9                 1805 	mov	a,r1
   04E3 F0                 1806 	movx	@dptr,a
                           1807 ;	main.c:190: printf_tiny("Buffer address and buffer number 0x%x, %d\n\r", (unsigned int) buffer[buffer_count], buffer_count);
                           1808 ;	genPlus
                           1809 ;	Peephole 236.g	used r4 instead of ar4
   04E4 EC                 1810 	mov	a,r4
   04E5 24 0A              1811 	add	a,#_buffer
   04E7 F5 82              1812 	mov	dpl,a
                           1813 ;	Peephole 236.g	used r5 instead of ar5
   04E9 ED                 1814 	mov	a,r5
   04EA 34 30              1815 	addc	a,#(_buffer >> 8)
   04EC F5 83              1816 	mov	dph,a
                           1817 ;	genPointerGet
                           1818 ;	genFarPointerGet
   04EE E0                 1819 	movx	a,@dptr
   04EF FC                 1820 	mov	r4,a
   04F0 A3                 1821 	inc	dptr
   04F1 E0                 1822 	movx	a,@dptr
   04F2 FD                 1823 	mov	r5,a
                           1824 ;	genCast
                           1825 ;	genIpush
   04F3 C0 02              1826 	push	ar2
   04F5 C0 03              1827 	push	ar3
                           1828 ;	genIpush
   04F7 C0 04              1829 	push	ar4
   04F9 C0 05              1830 	push	ar5
                           1831 ;	genIpush
   04FB 74 6E              1832 	mov	a,#__str_15
   04FD C0 E0              1833 	push	acc
   04FF 74 14              1834 	mov	a,#(__str_15 >> 8)
   0501 C0 E0              1835 	push	acc
                           1836 ;	genCall
   0503 12 10 5A           1837 	lcall	_printf_tiny
   0506 E5 81              1838 	mov	a,sp
   0508 24 FA              1839 	add	a,#0xfa
   050A F5 81              1840 	mov	sp,a
                           1841 ;	Peephole 112.b	changed ljmp to sjmp
   050C 80 09              1842 	sjmp	00109$
   050E                    1843 00105$:
                           1844 ;	main.c:194: putstr("Enter valid buffer size\n\r");
                           1845 ;	genCall
                           1846 ;	Peephole 182.a	used 16 bit load of DPTR
   050E 90 14 9A           1847 	mov	dptr,#__str_16
   0511 75 F0 80           1848 	mov	b,#0x80
   0514 12 0C F8           1849 	lcall	_putstr
   0517                    1850 00109$:
                           1851 ;	main.c:195: } while(buffer[buffer_count]==0);
                           1852 ;	genAssign
   0517 90 30 00           1853 	mov	dptr,#_buffer_count
   051A E0                 1854 	movx	a,@dptr
   051B FA                 1855 	mov	r2,a
   051C A3                 1856 	inc	dptr
   051D E0                 1857 	movx	a,@dptr
                           1858 ;	genLeftShift
                           1859 ;	genLeftShiftLiteral
                           1860 ;	genlshTwo
   051E FB                 1861 	mov	r3,a
   051F 8A 04              1862 	mov	ar4,r2
                           1863 ;	Peephole 177.d	removed redundant move
   0521 CC                 1864 	xch	a,r4
   0522 25 E0              1865 	add	a,acc
   0524 CC                 1866 	xch	a,r4
   0525 33                 1867 	rlc	a
   0526 FD                 1868 	mov	r5,a
                           1869 ;	genPlus
                           1870 ;	Peephole 236.g	used r4 instead of ar4
   0527 EC                 1871 	mov	a,r4
   0528 24 0A              1872 	add	a,#_buffer
   052A F5 82              1873 	mov	dpl,a
                           1874 ;	Peephole 236.g	used r5 instead of ar5
   052C ED                 1875 	mov	a,r5
   052D 34 30              1876 	addc	a,#(_buffer >> 8)
   052F F5 83              1877 	mov	dph,a
                           1878 ;	genPointerGet
                           1879 ;	genFarPointerGet
   0531 E0                 1880 	movx	a,@dptr
   0532 FC                 1881 	mov	r4,a
   0533 A3                 1882 	inc	dptr
   0534 E0                 1883 	movx	a,@dptr
                           1884 ;	genIfx
   0535 FD                 1885 	mov	r5,a
                           1886 ;	Peephole 135	removed redundant mov
   0536 4C                 1887 	orl	a,r4
                           1888 ;	genIfxJump
   0537 70 03              1889 	jnz	00121$
   0539 02 04 3B           1890 	ljmp	00108$
   053C                    1891 00121$:
                           1892 ;	main.c:196: buffer_count++;
                           1893 ;	genPlus
   053C 90 30 00           1894 	mov	dptr,#_buffer_count
                           1895 ;     genPlusIncr
   053F 74 01              1896 	mov	a,#0x01
                           1897 ;	Peephole 236.a	used r2 instead of ar2
   0541 2A                 1898 	add	a,r2
   0542 F0                 1899 	movx	@dptr,a
                           1900 ;	Peephole 181	changed mov to clr
   0543 E4                 1901 	clr	a
                           1902 ;	Peephole 236.b	used r3 instead of ar3
   0544 3B                 1903 	addc	a,r3
   0545 A3                 1904 	inc	dptr
   0546 F0                 1905 	movx	@dptr,a
                           1906 ;	main.c:197: buffer_number++;
                           1907 ;	genAssign
   0547 90 30 06           1908 	mov	dptr,#_buffer_number
   054A E0                 1909 	movx	a,@dptr
   054B FA                 1910 	mov	r2,a
   054C A3                 1911 	inc	dptr
   054D E0                 1912 	movx	a,@dptr
   054E FB                 1913 	mov	r3,a
                           1914 ;	genPlus
   054F 90 30 06           1915 	mov	dptr,#_buffer_number
                           1916 ;     genPlusIncr
   0552 74 01              1917 	mov	a,#0x01
                           1918 ;	Peephole 236.a	used r2 instead of ar2
   0554 2A                 1919 	add	a,r2
   0555 F0                 1920 	movx	@dptr,a
                           1921 ;	Peephole 181	changed mov to clr
   0556 E4                 1922 	clr	a
                           1923 ;	Peephole 236.b	used r3 instead of ar3
   0557 3B                 1924 	addc	a,r3
   0558 A3                 1925 	inc	dptr
   0559 F0                 1926 	movx	@dptr,a
                           1927 ;	Peephole 300	removed redundant label 00111$
   055A 22                 1928 	ret
                           1929 ;------------------------------------------------------------
                           1930 ;Allocation info for local variables in function 'deletebuffer'
                           1931 ;------------------------------------------------------------
                           1932 ;------------------------------------------------------------
                           1933 ;	main.c:200: void deletebuffer()
                           1934 ;	-----------------------------------------
                           1935 ;	 function deletebuffer
                           1936 ;	-----------------------------------------
   055B                    1937 _deletebuffer:
                           1938 ;	main.c:202: putstr("Enter the buffer number which has to be deleted\n\r");
                           1939 ;	genCall
                           1940 ;	Peephole 182.a	used 16 bit load of DPTR
   055B 90 14 B4           1941 	mov	dptr,#__str_17
   055E 75 F0 80           1942 	mov	b,#0x80
   0561 12 0C F8           1943 	lcall	_putstr
                           1944 ;	main.c:203: res=ReadValue();
                           1945 ;	genCall
   0564 12 03 B9           1946 	lcall	_ReadValue
   0567 AA 82              1947 	mov	r2,dpl
   0569 AB 83              1948 	mov	r3,dph
                           1949 ;	genAssign
   056B 90 34 19           1950 	mov	dptr,#_res
   056E EA                 1951 	mov	a,r2
   056F F0                 1952 	movx	@dptr,a
   0570 A3                 1953 	inc	dptr
   0571 EB                 1954 	mov	a,r3
   0572 F0                 1955 	movx	@dptr,a
                           1956 ;	main.c:204: if(res>buffer_count || res==1 || res==0)
                           1957 ;	genAssign
   0573 90 30 00           1958 	mov	dptr,#_buffer_count
   0576 E0                 1959 	movx	a,@dptr
   0577 FC                 1960 	mov	r4,a
   0578 A3                 1961 	inc	dptr
   0579 E0                 1962 	movx	a,@dptr
   057A FD                 1963 	mov	r5,a
                           1964 ;	genAssign
   057B 8A 06              1965 	mov	ar6,r2
   057D 8B 07              1966 	mov	ar7,r3
                           1967 ;	genCmpGt
                           1968 ;	genCmp
   057F C3                 1969 	clr	c
   0580 EC                 1970 	mov	a,r4
   0581 9E                 1971 	subb	a,r6
   0582 ED                 1972 	mov	a,r5
   0583 9F                 1973 	subb	a,r7
                           1974 ;	genIfxJump
                           1975 ;	Peephole 112.b	changed ljmp to sjmp
                           1976 ;	Peephole 160.a	removed sjmp by inverse jump logic
   0584 40 0C              1977 	jc	00104$
                           1978 ;	Peephole 300	removed redundant label 00114$
                           1979 ;	genCmpEq
                           1980 ;	gencjneshort
   0586 BA 01 05           1981 	cjne	r2,#0x01,00115$
   0589 BB 00 02           1982 	cjne	r3,#0x00,00115$
                           1983 ;	Peephole 112.b	changed ljmp to sjmp
   058C 80 04              1984 	sjmp	00104$
   058E                    1985 00115$:
                           1986 ;	genIfx
   058E EA                 1987 	mov	a,r2
   058F 4B                 1988 	orl	a,r3
                           1989 ;	genIfxJump
                           1990 ;	Peephole 108.b	removed ljmp by inverse jump logic
   0590 70 09              1991 	jnz	00105$
                           1992 ;	Peephole 300	removed redundant label 00116$
   0592                    1993 00104$:
                           1994 ;	main.c:205: putstr("Enter valid buffer number next time\n\r");
                           1995 ;	genCall
                           1996 ;	Peephole 182.a	used 16 bit load of DPTR
   0592 90 14 E6           1997 	mov	dptr,#__str_18
   0595 75 F0 80           1998 	mov	b,#0x80
                           1999 ;	Peephole 112.b	changed ljmp to sjmp
                           2000 ;	Peephole 251.b	replaced sjmp to ret with ret
                           2001 ;	Peephole 253.a	replaced lcall/ret with ljmp
   0598 02 0C F8           2002 	ljmp	_putstr
   059B                    2003 00105$:
                           2004 ;	main.c:206: else if (!buffer[res])
                           2005 ;	genLeftShift
                           2006 ;	genLeftShiftLiteral
                           2007 ;	genlshTwo
   059B EB                 2008 	mov	a,r3
   059C CA                 2009 	xch	a,r2
   059D 25 E0              2010 	add	a,acc
   059F CA                 2011 	xch	a,r2
   05A0 33                 2012 	rlc	a
   05A1 FB                 2013 	mov	r3,a
                           2014 ;	genPlus
                           2015 ;	Peephole 236.g	used r2 instead of ar2
   05A2 EA                 2016 	mov	a,r2
   05A3 24 0A              2017 	add	a,#_buffer
   05A5 F5 82              2018 	mov	dpl,a
                           2019 ;	Peephole 236.g	used r3 instead of ar3
   05A7 EB                 2020 	mov	a,r3
   05A8 34 30              2021 	addc	a,#(_buffer >> 8)
   05AA F5 83              2022 	mov	dph,a
                           2023 ;	genPointerGet
                           2024 ;	genFarPointerGet
   05AC E0                 2025 	movx	a,@dptr
   05AD FA                 2026 	mov	r2,a
   05AE A3                 2027 	inc	dptr
   05AF E0                 2028 	movx	a,@dptr
                           2029 ;	genIfx
   05B0 FB                 2030 	mov	r3,a
                           2031 ;	Peephole 135	removed redundant mov
   05B1 4A                 2032 	orl	a,r2
                           2033 ;	genIfxJump
                           2034 ;	Peephole 108.b	removed ljmp by inverse jump logic
   05B2 70 09              2035 	jnz	00102$
                           2036 ;	Peephole 300	removed redundant label 00117$
                           2037 ;	main.c:207: putstr("Enter valid buffer number next time\n\r");
                           2038 ;	genCall
                           2039 ;	Peephole 182.a	used 16 bit load of DPTR
   05B4 90 14 E6           2040 	mov	dptr,#__str_18
   05B7 75 F0 80           2041 	mov	b,#0x80
                           2042 ;	Peephole 112.b	changed ljmp to sjmp
                           2043 ;	Peephole 251.b	replaced sjmp to ret with ret
                           2044 ;	Peephole 253.a	replaced lcall/ret with ljmp
   05BA 02 0C F8           2045 	ljmp	_putstr
   05BD                    2046 00102$:
                           2047 ;	main.c:210: free (buffer[res]);
                           2048 ;	genAssign
                           2049 ;	genCast
   05BD 7C 00              2050 	mov	r4,#0x0
                           2051 ;	genCall
   05BF 8A 82              2052 	mov	dpl,r2
   05C1 8B 83              2053 	mov	dph,r3
   05C3 8C F0              2054 	mov	b,r4
   05C5 12 0E 31           2055 	lcall	_free
                           2056 ;	main.c:211: buffer[res]=0;
                           2057 ;	genAssign
   05C8 90 34 19           2058 	mov	dptr,#_res
   05CB E0                 2059 	movx	a,@dptr
   05CC FA                 2060 	mov	r2,a
   05CD A3                 2061 	inc	dptr
   05CE E0                 2062 	movx	a,@dptr
                           2063 ;	genLeftShift
                           2064 ;	genLeftShiftLiteral
                           2065 ;	genlshTwo
   05CF FB                 2066 	mov	r3,a
                           2067 ;	Peephole 105	removed redundant mov
   05D0 CA                 2068 	xch	a,r2
   05D1 25 E0              2069 	add	a,acc
   05D3 CA                 2070 	xch	a,r2
   05D4 33                 2071 	rlc	a
   05D5 FB                 2072 	mov	r3,a
                           2073 ;	genPlus
                           2074 ;	Peephole 236.g	used r2 instead of ar2
   05D6 EA                 2075 	mov	a,r2
   05D7 24 0A              2076 	add	a,#_buffer
   05D9 F5 82              2077 	mov	dpl,a
                           2078 ;	Peephole 236.g	used r3 instead of ar3
   05DB EB                 2079 	mov	a,r3
   05DC 34 30              2080 	addc	a,#(_buffer >> 8)
   05DE F5 83              2081 	mov	dph,a
                           2082 ;	genPointerSet
                           2083 ;     genFarPointerSet
                           2084 ;	Peephole 181	changed mov to clr
   05E0 E4                 2085 	clr	a
   05E1 F0                 2086 	movx	@dptr,a
   05E2 A3                 2087 	inc	dptr
                           2088 ;	Peephole 101	removed redundant mov
   05E3 F0                 2089 	movx	@dptr,a
                           2090 ;	main.c:212: buffer_number--;
                           2091 ;	genAssign
   05E4 90 30 06           2092 	mov	dptr,#_buffer_number
   05E7 E0                 2093 	movx	a,@dptr
   05E8 FA                 2094 	mov	r2,a
   05E9 A3                 2095 	inc	dptr
   05EA E0                 2096 	movx	a,@dptr
   05EB FB                 2097 	mov	r3,a
                           2098 ;	genMinus
                           2099 ;	genMinusDec
   05EC 1A                 2100 	dec	r2
   05ED BA FF 01           2101 	cjne	r2,#0xff,00118$
   05F0 1B                 2102 	dec	r3
   05F1                    2103 00118$:
                           2104 ;	genAssign
   05F1 90 30 06           2105 	mov	dptr,#_buffer_number
   05F4 EA                 2106 	mov	a,r2
   05F5 F0                 2107 	movx	@dptr,a
   05F6 A3                 2108 	inc	dptr
   05F7 EB                 2109 	mov	a,r3
   05F8 F0                 2110 	movx	@dptr,a
                           2111 ;	main.c:213: putstr("Buffer deleted\n\r");
                           2112 ;	genCall
                           2113 ;	Peephole 182.a	used 16 bit load of DPTR
   05F9 90 15 0C           2114 	mov	dptr,#__str_19
   05FC 75 F0 80           2115 	mov	b,#0x80
                           2116 ;	Peephole 253.b	replaced lcall/ret with ljmp
   05FF 02 0C F8           2117 	ljmp	_putstr
                           2118 ;
                           2119 ;------------------------------------------------------------
                           2120 ;Allocation info for local variables in function 'fillbuffer0'
                           2121 ;------------------------------------------------------------
                           2122 ;buff_val                  Allocated with name '_fillbuffer0_buff_val_1_1'
                           2123 ;------------------------------------------------------------
                           2124 ;	main.c:218: void fillbuffer0(unsigned char buff_val)
                           2125 ;	-----------------------------------------
                           2126 ;	 function fillbuffer0
                           2127 ;	-----------------------------------------
   0602                    2128 _fillbuffer0:
                           2129 ;	genReceive
   0602 E5 82              2130 	mov	a,dpl
   0604 90 34 21           2131 	mov	dptr,#_fillbuffer0_buff_val_1_1
   0607 F0                 2132 	movx	@dptr,a
                           2133 ;	main.c:222: if((unsigned int)buffer[0]<((unsigned int)buff1ptr+(int)bufflen[0]))
                           2134 ;	genPointerGet
                           2135 ;	genFarPointerGet
   0608 90 30 0A           2136 	mov	dptr,#_buffer
   060B E0                 2137 	movx	a,@dptr
   060C FA                 2138 	mov	r2,a
   060D A3                 2139 	inc	dptr
   060E E0                 2140 	movx	a,@dptr
   060F FB                 2141 	mov	r3,a
                           2142 ;	genCast
                           2143 ;	genAssign
   0610 90 32 0A           2144 	mov	dptr,#_buff1ptr
   0613 E0                 2145 	movx	a,@dptr
   0614 FC                 2146 	mov	r4,a
   0615 A3                 2147 	inc	dptr
   0616 E0                 2148 	movx	a,@dptr
   0617 FD                 2149 	mov	r5,a
   0618 A3                 2150 	inc	dptr
   0619 E0                 2151 	movx	a,@dptr
   061A FE                 2152 	mov	r6,a
                           2153 ;	genCast
                           2154 ;	genPointerGet
                           2155 ;	genFarPointerGet
   061B 90 32 10           2156 	mov	dptr,#_bufflen
   061E E0                 2157 	movx	a,@dptr
   061F FE                 2158 	mov	r6,a
   0620 A3                 2159 	inc	dptr
   0621 E0                 2160 	movx	a,@dptr
   0622 FF                 2161 	mov	r7,a
                           2162 ;	genPlus
                           2163 ;	Peephole 236.g	used r6 instead of ar6
   0623 EE                 2164 	mov	a,r6
                           2165 ;	Peephole 236.a	used r4 instead of ar4
   0624 2C                 2166 	add	a,r4
   0625 FC                 2167 	mov	r4,a
                           2168 ;	Peephole 236.g	used r7 instead of ar7
   0626 EF                 2169 	mov	a,r7
                           2170 ;	Peephole 236.b	used r5 instead of ar5
   0627 3D                 2171 	addc	a,r5
   0628 FD                 2172 	mov	r5,a
                           2173 ;	genCmpLt
                           2174 ;	genCmp
   0629 C3                 2175 	clr	c
   062A EA                 2176 	mov	a,r2
   062B 9C                 2177 	subb	a,r4
   062C EB                 2178 	mov	a,r3
   062D 9D                 2179 	subb	a,r5
                           2180 ;	genIfxJump
                           2181 ;	Peephole 108.a	removed ljmp by inverse jump logic
   062E 50 27              2182 	jnc	00103$
                           2183 ;	Peephole 300	removed redundant label 00106$
                           2184 ;	main.c:224: *bptr0 = buff_val;
                           2185 ;	genAssign
   0630 90 34 1B           2186 	mov	dptr,#_bptr0
   0633 E0                 2187 	movx	a,@dptr
   0634 FA                 2188 	mov	r2,a
   0635 A3                 2189 	inc	dptr
   0636 E0                 2190 	movx	a,@dptr
   0637 FB                 2191 	mov	r3,a
   0638 A3                 2192 	inc	dptr
   0639 E0                 2193 	movx	a,@dptr
   063A FC                 2194 	mov	r4,a
                           2195 ;	genAssign
   063B 90 34 21           2196 	mov	dptr,#_fillbuffer0_buff_val_1_1
   063E E0                 2197 	movx	a,@dptr
                           2198 ;	genPointerSet
                           2199 ;	genGenPointerSet
   063F FD                 2200 	mov	r5,a
   0640 8A 82              2201 	mov	dpl,r2
   0642 8B 83              2202 	mov	dph,r3
   0644 8C F0              2203 	mov	b,r4
                           2204 ;	Peephole 191	removed redundant mov
   0646 12 10 41           2205 	lcall	__gptrput
                           2206 ;	main.c:225: bptr0++;
                           2207 ;	genPlus
   0649 90 34 1B           2208 	mov	dptr,#_bptr0
                           2209 ;     genPlusIncr
   064C 74 01              2210 	mov	a,#0x01
                           2211 ;	Peephole 236.a	used r2 instead of ar2
   064E 2A                 2212 	add	a,r2
   064F F0                 2213 	movx	@dptr,a
                           2214 ;	Peephole 181	changed mov to clr
   0650 E4                 2215 	clr	a
                           2216 ;	Peephole 236.b	used r3 instead of ar3
   0651 3B                 2217 	addc	a,r3
   0652 A3                 2218 	inc	dptr
   0653 F0                 2219 	movx	@dptr,a
   0654 A3                 2220 	inc	dptr
   0655 EC                 2221 	mov	a,r4
   0656 F0                 2222 	movx	@dptr,a
   0657                    2223 00103$:
   0657 22                 2224 	ret
                           2225 ;------------------------------------------------------------
                           2226 ;Allocation info for local variables in function 'fillbuffer1'
                           2227 ;------------------------------------------------------------
                           2228 ;buff_val                  Allocated with name '_fillbuffer1_buff_val_1_1'
                           2229 ;------------------------------------------------------------
                           2230 ;	main.c:229: void fillbuffer1(unsigned char buff_val)
                           2231 ;	-----------------------------------------
                           2232 ;	 function fillbuffer1
                           2233 ;	-----------------------------------------
   0658                    2234 _fillbuffer1:
                           2235 ;	genReceive
   0658 E5 82              2236 	mov	a,dpl
   065A 90 34 22           2237 	mov	dptr,#_fillbuffer1_buff_val_1_1
   065D F0                 2238 	movx	@dptr,a
                           2239 ;	main.c:231: if((unsigned int)buffer[1]<((unsigned int)buff2ptr+(int)bufflen[1]))
                           2240 ;	genPointerGet
                           2241 ;	genFarPointerGet
   065E 90 30 0C           2242 	mov	dptr,#(_buffer + 0x0002)
   0661 E0                 2243 	movx	a,@dptr
   0662 FA                 2244 	mov	r2,a
   0663 A3                 2245 	inc	dptr
   0664 E0                 2246 	movx	a,@dptr
   0665 FB                 2247 	mov	r3,a
                           2248 ;	genCast
                           2249 ;	genAssign
   0666 90 32 0D           2250 	mov	dptr,#_buff2ptr
   0669 E0                 2251 	movx	a,@dptr
   066A FC                 2252 	mov	r4,a
   066B A3                 2253 	inc	dptr
   066C E0                 2254 	movx	a,@dptr
   066D FD                 2255 	mov	r5,a
   066E A3                 2256 	inc	dptr
   066F E0                 2257 	movx	a,@dptr
   0670 FE                 2258 	mov	r6,a
                           2259 ;	genCast
                           2260 ;	genPointerGet
                           2261 ;	genFarPointerGet
   0671 90 32 12           2262 	mov	dptr,#(_bufflen + 0x0002)
   0674 E0                 2263 	movx	a,@dptr
   0675 FE                 2264 	mov	r6,a
   0676 A3                 2265 	inc	dptr
   0677 E0                 2266 	movx	a,@dptr
   0678 FF                 2267 	mov	r7,a
                           2268 ;	genPlus
                           2269 ;	Peephole 236.g	used r6 instead of ar6
   0679 EE                 2270 	mov	a,r6
                           2271 ;	Peephole 236.a	used r4 instead of ar4
   067A 2C                 2272 	add	a,r4
   067B FC                 2273 	mov	r4,a
                           2274 ;	Peephole 236.g	used r7 instead of ar7
   067C EF                 2275 	mov	a,r7
                           2276 ;	Peephole 236.b	used r5 instead of ar5
   067D 3D                 2277 	addc	a,r5
   067E FD                 2278 	mov	r5,a
                           2279 ;	genCmpLt
                           2280 ;	genCmp
   067F C3                 2281 	clr	c
   0680 EA                 2282 	mov	a,r2
   0681 9C                 2283 	subb	a,r4
   0682 EB                 2284 	mov	a,r3
   0683 9D                 2285 	subb	a,r5
                           2286 ;	genIfxJump
                           2287 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0684 50 27              2288 	jnc	00103$
                           2289 ;	Peephole 300	removed redundant label 00106$
                           2290 ;	main.c:233: *bptr1 = buff_val;
                           2291 ;	genAssign
   0686 90 34 1E           2292 	mov	dptr,#_bptr1
   0689 E0                 2293 	movx	a,@dptr
   068A FA                 2294 	mov	r2,a
   068B A3                 2295 	inc	dptr
   068C E0                 2296 	movx	a,@dptr
   068D FB                 2297 	mov	r3,a
   068E A3                 2298 	inc	dptr
   068F E0                 2299 	movx	a,@dptr
   0690 FC                 2300 	mov	r4,a
                           2301 ;	genAssign
   0691 90 34 22           2302 	mov	dptr,#_fillbuffer1_buff_val_1_1
   0694 E0                 2303 	movx	a,@dptr
                           2304 ;	genPointerSet
                           2305 ;	genGenPointerSet
   0695 FD                 2306 	mov	r5,a
   0696 8A 82              2307 	mov	dpl,r2
   0698 8B 83              2308 	mov	dph,r3
   069A 8C F0              2309 	mov	b,r4
                           2310 ;	Peephole 191	removed redundant mov
   069C 12 10 41           2311 	lcall	__gptrput
                           2312 ;	main.c:234: bptr1++;
                           2313 ;	genPlus
   069F 90 34 1E           2314 	mov	dptr,#_bptr1
                           2315 ;     genPlusIncr
   06A2 74 01              2316 	mov	a,#0x01
                           2317 ;	Peephole 236.a	used r2 instead of ar2
   06A4 2A                 2318 	add	a,r2
   06A5 F0                 2319 	movx	@dptr,a
                           2320 ;	Peephole 181	changed mov to clr
   06A6 E4                 2321 	clr	a
                           2322 ;	Peephole 236.b	used r3 instead of ar3
   06A7 3B                 2323 	addc	a,r3
   06A8 A3                 2324 	inc	dptr
   06A9 F0                 2325 	movx	@dptr,a
   06AA A3                 2326 	inc	dptr
   06AB EC                 2327 	mov	a,r4
   06AC F0                 2328 	movx	@dptr,a
   06AD                    2329 00103$:
   06AD 22                 2330 	ret
                           2331 ;------------------------------------------------------------
                           2332 ;Allocation info for local variables in function 'freeEverything'
                           2333 ;------------------------------------------------------------
                           2334 ;counter                   Allocated with name '_freeEverything_counter_1_1'
                           2335 ;------------------------------------------------------------
                           2336 ;	main.c:239: void freeEverything()
                           2337 ;	-----------------------------------------
                           2338 ;	 function freeEverything
                           2339 ;	-----------------------------------------
   06AE                    2340 _freeEverything:
                           2341 ;	main.c:242: for (counter=0; counter<buffer_count; counter++)
                           2342 ;	genAssign
   06AE 7A 00              2343 	mov	r2,#0x00
   06B0 7B 00              2344 	mov	r3,#0x00
   06B2                    2345 00103$:
                           2346 ;	genAssign
   06B2 90 30 00           2347 	mov	dptr,#_buffer_count
   06B5 E0                 2348 	movx	a,@dptr
   06B6 FC                 2349 	mov	r4,a
   06B7 A3                 2350 	inc	dptr
   06B8 E0                 2351 	movx	a,@dptr
   06B9 FD                 2352 	mov	r5,a
                           2353 ;	genCmpLt
                           2354 ;	genCmp
   06BA C3                 2355 	clr	c
   06BB EA                 2356 	mov	a,r2
   06BC 9C                 2357 	subb	a,r4
   06BD EB                 2358 	mov	a,r3
   06BE 9D                 2359 	subb	a,r5
                           2360 ;	genIfxJump
                           2361 ;	Peephole 108.a	removed ljmp by inverse jump logic
   06BF 50 52              2362 	jnc	00107$
                           2363 ;	Peephole 300	removed redundant label 00113$
                           2364 ;	main.c:244: if(buffer[counter])
                           2365 ;	genLeftShift
                           2366 ;	genLeftShiftLiteral
                           2367 ;	genlshTwo
   06C1 8A 04              2368 	mov	ar4,r2
   06C3 EB                 2369 	mov	a,r3
   06C4 CC                 2370 	xch	a,r4
   06C5 25 E0              2371 	add	a,acc
   06C7 CC                 2372 	xch	a,r4
   06C8 33                 2373 	rlc	a
   06C9 FD                 2374 	mov	r5,a
                           2375 ;	genPlus
                           2376 ;	Peephole 236.g	used r4 instead of ar4
   06CA EC                 2377 	mov	a,r4
   06CB 24 0A              2378 	add	a,#_buffer
   06CD F5 82              2379 	mov	dpl,a
                           2380 ;	Peephole 236.g	used r5 instead of ar5
   06CF ED                 2381 	mov	a,r5
   06D0 34 30              2382 	addc	a,#(_buffer >> 8)
   06D2 F5 83              2383 	mov	dph,a
                           2384 ;	genPointerGet
                           2385 ;	genFarPointerGet
   06D4 E0                 2386 	movx	a,@dptr
   06D5 FC                 2387 	mov	r4,a
   06D6 A3                 2388 	inc	dptr
   06D7 E0                 2389 	movx	a,@dptr
                           2390 ;	genIfx
   06D8 FD                 2391 	mov	r5,a
                           2392 ;	Peephole 135	removed redundant mov
   06D9 4C                 2393 	orl	a,r4
                           2394 ;	genIfxJump
                           2395 ;	Peephole 108.c	removed ljmp by inverse jump logic
   06DA 60 30              2396 	jz	00105$
                           2397 ;	Peephole 300	removed redundant label 00114$
                           2398 ;	main.c:246: free (buffer[counter]);
                           2399 ;	genAssign
                           2400 ;	genCast
   06DC 7E 00              2401 	mov	r6,#0x0
                           2402 ;	genCall
   06DE 8C 82              2403 	mov	dpl,r4
   06E0 8D 83              2404 	mov	dph,r5
   06E2 8E F0              2405 	mov	b,r6
   06E4 C0 02              2406 	push	ar2
   06E6 C0 03              2407 	push	ar3
   06E8 12 0E 31           2408 	lcall	_free
   06EB D0 03              2409 	pop	ar3
   06ED D0 02              2410 	pop	ar2
                           2411 ;	main.c:247: printf_tiny("Freeing %d buffer\n\r",counter);
                           2412 ;	genIpush
   06EF C0 02              2413 	push	ar2
   06F1 C0 03              2414 	push	ar3
   06F3 C0 02              2415 	push	ar2
   06F5 C0 03              2416 	push	ar3
                           2417 ;	genIpush
   06F7 74 1D              2418 	mov	a,#__str_20
   06F9 C0 E0              2419 	push	acc
   06FB 74 15              2420 	mov	a,#(__str_20 >> 8)
   06FD C0 E0              2421 	push	acc
                           2422 ;	genCall
   06FF 12 10 5A           2423 	lcall	_printf_tiny
   0702 E5 81              2424 	mov	a,sp
   0704 24 FC              2425 	add	a,#0xfc
   0706 F5 81              2426 	mov	sp,a
   0708 D0 03              2427 	pop	ar3
   070A D0 02              2428 	pop	ar2
   070C                    2429 00105$:
                           2430 ;	main.c:242: for (counter=0; counter<buffer_count; counter++)
                           2431 ;	genPlus
                           2432 ;     genPlusIncr
   070C 0A                 2433 	inc	r2
                           2434 ;	Peephole 112.b	changed ljmp to sjmp
                           2435 ;	Peephole 243	avoided branch to sjmp
   070D BA 00 A2           2436 	cjne	r2,#0x00,00103$
   0710 0B                 2437 	inc	r3
                           2438 ;	Peephole 300	removed redundant label 00115$
   0711 80 9F              2439 	sjmp	00103$
   0713                    2440 00107$:
   0713 22                 2441 	ret
                           2442 ;------------------------------------------------------------
                           2443 ;Allocation info for local variables in function 'displaydelete'
                           2444 ;------------------------------------------------------------
                           2445 ;len                       Allocated with name '_displaydelete_len_1_1'
                           2446 ;charnum0                  Allocated with name '_displaydelete_charnum0_1_1'
                           2447 ;charnum1                  Allocated with name '_displaydelete_charnum1_1_1'
                           2448 ;------------------------------------------------------------
                           2449 ;	main.c:251: void displaydelete()
                           2450 ;	-----------------------------------------
                           2451 ;	 function displaydelete
                           2452 ;	-----------------------------------------
   0714                    2453 _displaydelete:
                           2454 ;	main.c:253: unsigned int len=0, charnum0=0, charnum1=0 ;
                           2455 ;	genAssign
   0714 90 34 23           2456 	mov	dptr,#_displaydelete_charnum0_1_1
   0717 E4                 2457 	clr	a
   0718 F0                 2458 	movx	@dptr,a
   0719 A3                 2459 	inc	dptr
   071A F0                 2460 	movx	@dptr,a
                           2461 ;	genAssign
   071B 90 34 25           2462 	mov	dptr,#_displaydelete_charnum1_1_1
   071E E4                 2463 	clr	a
   071F F0                 2464 	movx	@dptr,a
   0720 A3                 2465 	inc	dptr
   0721 F0                 2466 	movx	@dptr,a
                           2467 ;	main.c:254: printf_tiny("---------------------------------------------------------------------------------------\n\r");
                           2468 ;	genIpush
   0722 74 31              2469 	mov	a,#__str_21
   0724 C0 E0              2470 	push	acc
   0726 74 15              2471 	mov	a,#(__str_21 >> 8)
   0728 C0 E0              2472 	push	acc
                           2473 ;	genCall
   072A 12 10 5A           2474 	lcall	_printf_tiny
   072D 15 81              2475 	dec	sp
   072F 15 81              2476 	dec	sp
                           2477 ;	main.c:255: printf_tiny("\n\rrTotal Number of Buffer = %d\n\r", buffer_number);
                           2478 ;	genIpush
   0731 90 30 06           2479 	mov	dptr,#_buffer_number
   0734 E0                 2480 	movx	a,@dptr
   0735 C0 E0              2481 	push	acc
   0737 A3                 2482 	inc	dptr
   0738 E0                 2483 	movx	a,@dptr
   0739 C0 E0              2484 	push	acc
                           2485 ;	genIpush
   073B 74 8B              2486 	mov	a,#__str_22
   073D C0 E0              2487 	push	acc
   073F 74 15              2488 	mov	a,#(__str_22 >> 8)
   0741 C0 E0              2489 	push	acc
                           2490 ;	genCall
   0743 12 10 5A           2491 	lcall	_printf_tiny
   0746 E5 81              2492 	mov	a,sp
   0748 24 FC              2493 	add	a,#0xfc
   074A F5 81              2494 	mov	sp,a
                           2495 ;	main.c:256: putstr("\n\r");
                           2496 ;	genCall
                           2497 ;	Peephole 182.a	used 16 bit load of DPTR
   074C 90 12 D2           2498 	mov	dptr,#__str_1
   074F 75 F0 80           2499 	mov	b,#0x80
   0752 12 0C F8           2500 	lcall	_putstr
                           2501 ;	main.c:257: printf_tiny("---------------------------------------------------------------------------------------\n\r");
                           2502 ;	genIpush
   0755 74 31              2503 	mov	a,#__str_21
   0757 C0 E0              2504 	push	acc
   0759 74 15              2505 	mov	a,#(__str_21 >> 8)
   075B C0 E0              2506 	push	acc
                           2507 ;	genCall
   075D 12 10 5A           2508 	lcall	_printf_tiny
   0760 15 81              2509 	dec	sp
   0762 15 81              2510 	dec	sp
                           2511 ;	main.c:258: printf_tiny("\n\rTotal number of characters received = %d\n\r", char_count);
                           2512 ;	genIpush
   0764 90 30 04           2513 	mov	dptr,#_char_count
   0767 E0                 2514 	movx	a,@dptr
   0768 C0 E0              2515 	push	acc
   076A A3                 2516 	inc	dptr
   076B E0                 2517 	movx	a,@dptr
   076C C0 E0              2518 	push	acc
                           2519 ;	genIpush
   076E 74 AC              2520 	mov	a,#__str_23
   0770 C0 E0              2521 	push	acc
   0772 74 15              2522 	mov	a,#(__str_23 >> 8)
   0774 C0 E0              2523 	push	acc
                           2524 ;	genCall
   0776 12 10 5A           2525 	lcall	_printf_tiny
   0779 E5 81              2526 	mov	a,sp
   077B 24 FC              2527 	add	a,#0xfc
   077D F5 81              2528 	mov	sp,a
                           2529 ;	main.c:259: printf_tiny("Total number of storage characters received = %d\n\r", stor_count);
                           2530 ;	genIpush
   077F 90 30 02           2531 	mov	dptr,#_stor_count
   0782 E0                 2532 	movx	a,@dptr
   0783 C0 E0              2533 	push	acc
   0785 A3                 2534 	inc	dptr
   0786 E0                 2535 	movx	a,@dptr
   0787 C0 E0              2536 	push	acc
                           2537 ;	genIpush
   0789 74 D9              2538 	mov	a,#__str_24
   078B C0 E0              2539 	push	acc
   078D 74 15              2540 	mov	a,#(__str_24 >> 8)
   078F C0 E0              2541 	push	acc
                           2542 ;	genCall
   0791 12 10 5A           2543 	lcall	_printf_tiny
   0794 E5 81              2544 	mov	a,sp
   0796 24 FC              2545 	add	a,#0xfc
   0798 F5 81              2546 	mov	sp,a
                           2547 ;	main.c:260: printf_tiny("Total number of characters received since last '?' = %d\n\r", (char_count-lastchar_count));
                           2548 ;	genAssign
   079A 90 30 08           2549 	mov	dptr,#_lastchar_count
   079D E0                 2550 	movx	a,@dptr
   079E FA                 2551 	mov	r2,a
   079F A3                 2552 	inc	dptr
   07A0 E0                 2553 	movx	a,@dptr
   07A1 FB                 2554 	mov	r3,a
                           2555 ;	genAssign
   07A2 90 30 04           2556 	mov	dptr,#_char_count
   07A5 E0                 2557 	movx	a,@dptr
   07A6 FC                 2558 	mov	r4,a
   07A7 A3                 2559 	inc	dptr
   07A8 E0                 2560 	movx	a,@dptr
   07A9 FD                 2561 	mov	r5,a
                           2562 ;	genMinus
   07AA EC                 2563 	mov	a,r4
   07AB C3                 2564 	clr	c
                           2565 ;	Peephole 236.l	used r2 instead of ar2
   07AC 9A                 2566 	subb	a,r2
   07AD FA                 2567 	mov	r2,a
   07AE ED                 2568 	mov	a,r5
                           2569 ;	Peephole 236.l	used r3 instead of ar3
   07AF 9B                 2570 	subb	a,r3
   07B0 FB                 2571 	mov	r3,a
                           2572 ;	genIpush
   07B1 C0 02              2573 	push	ar2
   07B3 C0 03              2574 	push	ar3
                           2575 ;	genIpush
   07B5 74 0C              2576 	mov	a,#__str_25
   07B7 C0 E0              2577 	push	acc
   07B9 74 16              2578 	mov	a,#(__str_25 >> 8)
   07BB C0 E0              2579 	push	acc
                           2580 ;	genCall
   07BD 12 10 5A           2581 	lcall	_printf_tiny
   07C0 E5 81              2582 	mov	a,sp
   07C2 24 FC              2583 	add	a,#0xfc
   07C4 F5 81              2584 	mov	sp,a
                           2585 ;	main.c:261: printf_tiny("\n\r---------------------------------------------------------------------------------------\n\r");
                           2586 ;	genIpush
   07C6 74 46              2587 	mov	a,#__str_26
   07C8 C0 E0              2588 	push	acc
   07CA 74 16              2589 	mov	a,#(__str_26 >> 8)
   07CC C0 E0              2590 	push	acc
                           2591 ;	genCall
   07CE 12 10 5A           2592 	lcall	_printf_tiny
   07D1 15 81              2593 	dec	sp
   07D3 15 81              2594 	dec	sp
                           2595 ;	main.c:262: putstr("\n\r");
                           2596 ;	genCall
                           2597 ;	Peephole 182.a	used 16 bit load of DPTR
   07D5 90 12 D2           2598 	mov	dptr,#__str_1
   07D8 75 F0 80           2599 	mov	b,#0x80
   07DB 12 0C F8           2600 	lcall	_putstr
                           2601 ;	main.c:263: lastchar_count=char_count;
                           2602 ;	genAssign
   07DE 90 30 04           2603 	mov	dptr,#_char_count
   07E1 E0                 2604 	movx	a,@dptr
   07E2 FA                 2605 	mov	r2,a
   07E3 A3                 2606 	inc	dptr
   07E4 E0                 2607 	movx	a,@dptr
   07E5 FB                 2608 	mov	r3,a
                           2609 ;	genAssign
   07E6 90 30 08           2610 	mov	dptr,#_lastchar_count
   07E9 EA                 2611 	mov	a,r2
   07EA F0                 2612 	movx	@dptr,a
   07EB A3                 2613 	inc	dptr
   07EC EB                 2614 	mov	a,r3
   07ED F0                 2615 	movx	@dptr,a
                           2616 ;	main.c:264: while((unsigned char)*buff1ptr!='\0')
                           2617 ;	genAssign
   07EE 7A 00              2618 	mov	r2,#0x00
   07F0 7B 00              2619 	mov	r3,#0x00
   07F2                    2620 00101$:
                           2621 ;	genAssign
   07F2 90 32 0A           2622 	mov	dptr,#_buff1ptr
   07F5 E0                 2623 	movx	a,@dptr
   07F6 FC                 2624 	mov	r4,a
   07F7 A3                 2625 	inc	dptr
   07F8 E0                 2626 	movx	a,@dptr
   07F9 FD                 2627 	mov	r5,a
   07FA A3                 2628 	inc	dptr
   07FB E0                 2629 	movx	a,@dptr
   07FC FE                 2630 	mov	r6,a
                           2631 ;	genPointerGet
                           2632 ;	genGenPointerGet
   07FD 8C 82              2633 	mov	dpl,r4
   07FF 8D 83              2634 	mov	dph,r5
   0801 8E F0              2635 	mov	b,r6
   0803 12 12 9E           2636 	lcall	__gptrget
                           2637 ;	genCmpEq
                           2638 ;	gencjneshort
                           2639 ;	Peephole 112.b	changed ljmp to sjmp
   0806 FF                 2640 	mov	r7,a
                           2641 ;	Peephole 115.b	jump optimization
   0807 60 1D              2642 	jz	00122$
                           2643 ;	Peephole 300	removed redundant label 00125$
                           2644 ;	main.c:266: charnum0++;
                           2645 ;	genPlus
                           2646 ;     genPlusIncr
   0809 0A                 2647 	inc	r2
   080A BA 00 01           2648 	cjne	r2,#0x00,00126$
   080D 0B                 2649 	inc	r3
   080E                    2650 00126$:
                           2651 ;	genAssign
   080E 90 34 23           2652 	mov	dptr,#_displaydelete_charnum0_1_1
   0811 EA                 2653 	mov	a,r2
   0812 F0                 2654 	movx	@dptr,a
   0813 A3                 2655 	inc	dptr
   0814 EB                 2656 	mov	a,r3
   0815 F0                 2657 	movx	@dptr,a
                           2658 ;	main.c:267: (unsigned char)buff1ptr++;
                           2659 ;	genPlus
   0816 90 32 0A           2660 	mov	dptr,#_buff1ptr
                           2661 ;     genPlusIncr
   0819 74 01              2662 	mov	a,#0x01
                           2663 ;	Peephole 236.a	used r4 instead of ar4
   081B 2C                 2664 	add	a,r4
   081C F0                 2665 	movx	@dptr,a
                           2666 ;	Peephole 181	changed mov to clr
   081D E4                 2667 	clr	a
                           2668 ;	Peephole 236.b	used r5 instead of ar5
   081E 3D                 2669 	addc	a,r5
   081F A3                 2670 	inc	dptr
   0820 F0                 2671 	movx	@dptr,a
   0821 A3                 2672 	inc	dptr
   0822 EE                 2673 	mov	a,r6
   0823 F0                 2674 	movx	@dptr,a
                           2675 ;	Peephole 112.b	changed ljmp to sjmp
   0824 80 CC              2676 	sjmp	00101$
   0826                    2677 00122$:
                           2678 ;	genAssign
   0826 90 34 23           2679 	mov	dptr,#_displaydelete_charnum0_1_1
   0829 EA                 2680 	mov	a,r2
   082A F0                 2681 	movx	@dptr,a
   082B A3                 2682 	inc	dptr
   082C EB                 2683 	mov	a,r3
   082D F0                 2684 	movx	@dptr,a
                           2685 ;	main.c:269: printf_tiny("\n\rNumber of characters in buffer0 = %d\n\r", charnum0);
                           2686 ;	genIpush
   082E C0 02              2687 	push	ar2
   0830 C0 03              2688 	push	ar3
   0832 C0 02              2689 	push	ar2
   0834 C0 03              2690 	push	ar3
                           2691 ;	genIpush
   0836 74 A2              2692 	mov	a,#__str_27
   0838 C0 E0              2693 	push	acc
   083A 74 16              2694 	mov	a,#(__str_27 >> 8)
   083C C0 E0              2695 	push	acc
                           2696 ;	genCall
   083E 12 10 5A           2697 	lcall	_printf_tiny
   0841 E5 81              2698 	mov	a,sp
   0843 24 FC              2699 	add	a,#0xfc
   0845 F5 81              2700 	mov	sp,a
   0847 D0 03              2701 	pop	ar3
   0849 D0 02              2702 	pop	ar2
                           2703 ;	main.c:270: buff1ptr=buff1ptr-charnum0;
                           2704 ;	genAssign
   084B 90 32 0A           2705 	mov	dptr,#_buff1ptr
   084E E0                 2706 	movx	a,@dptr
   084F FC                 2707 	mov	r4,a
   0850 A3                 2708 	inc	dptr
   0851 E0                 2709 	movx	a,@dptr
   0852 FD                 2710 	mov	r5,a
   0853 A3                 2711 	inc	dptr
   0854 E0                 2712 	movx	a,@dptr
   0855 FE                 2713 	mov	r6,a
                           2714 ;	genMinus
   0856 90 32 0A           2715 	mov	dptr,#_buff1ptr
   0859 EC                 2716 	mov	a,r4
   085A C3                 2717 	clr	c
                           2718 ;	Peephole 236.l	used r2 instead of ar2
   085B 9A                 2719 	subb	a,r2
   085C F0                 2720 	movx	@dptr,a
   085D ED                 2721 	mov	a,r5
                           2722 ;	Peephole 236.l	used r3 instead of ar3
   085E 9B                 2723 	subb	a,r3
   085F A3                 2724 	inc	dptr
   0860 F0                 2725 	movx	@dptr,a
   0861 A3                 2726 	inc	dptr
   0862 EE                 2727 	mov	a,r6
   0863 F0                 2728 	movx	@dptr,a
                           2729 ;	main.c:271: while((unsigned char)*buff2ptr!='\0')
                           2730 ;	genAssign
   0864 7A 00              2731 	mov	r2,#0x00
   0866 7B 00              2732 	mov	r3,#0x00
   0868                    2733 00104$:
                           2734 ;	genAssign
   0868 90 32 0D           2735 	mov	dptr,#_buff2ptr
   086B E0                 2736 	movx	a,@dptr
   086C FC                 2737 	mov	r4,a
   086D A3                 2738 	inc	dptr
   086E E0                 2739 	movx	a,@dptr
   086F FD                 2740 	mov	r5,a
   0870 A3                 2741 	inc	dptr
   0871 E0                 2742 	movx	a,@dptr
   0872 FE                 2743 	mov	r6,a
                           2744 ;	genPointerGet
                           2745 ;	genGenPointerGet
   0873 8C 82              2746 	mov	dpl,r4
   0875 8D 83              2747 	mov	dph,r5
   0877 8E F0              2748 	mov	b,r6
   0879 12 12 9E           2749 	lcall	__gptrget
                           2750 ;	genCmpEq
                           2751 ;	gencjneshort
                           2752 ;	Peephole 112.b	changed ljmp to sjmp
   087C FF                 2753 	mov	r7,a
                           2754 ;	Peephole 115.b	jump optimization
   087D 60 1D              2755 	jz	00123$
                           2756 ;	Peephole 300	removed redundant label 00127$
                           2757 ;	main.c:273: charnum1++;
                           2758 ;	genPlus
                           2759 ;     genPlusIncr
   087F 0A                 2760 	inc	r2
   0880 BA 00 01           2761 	cjne	r2,#0x00,00128$
   0883 0B                 2762 	inc	r3
   0884                    2763 00128$:
                           2764 ;	genAssign
   0884 90 34 25           2765 	mov	dptr,#_displaydelete_charnum1_1_1
   0887 EA                 2766 	mov	a,r2
   0888 F0                 2767 	movx	@dptr,a
   0889 A3                 2768 	inc	dptr
   088A EB                 2769 	mov	a,r3
   088B F0                 2770 	movx	@dptr,a
                           2771 ;	main.c:274: (unsigned char)buff2ptr++;
                           2772 ;	genPlus
   088C 90 32 0D           2773 	mov	dptr,#_buff2ptr
                           2774 ;     genPlusIncr
   088F 74 01              2775 	mov	a,#0x01
                           2776 ;	Peephole 236.a	used r4 instead of ar4
   0891 2C                 2777 	add	a,r4
   0892 F0                 2778 	movx	@dptr,a
                           2779 ;	Peephole 181	changed mov to clr
   0893 E4                 2780 	clr	a
                           2781 ;	Peephole 236.b	used r5 instead of ar5
   0894 3D                 2782 	addc	a,r5
   0895 A3                 2783 	inc	dptr
   0896 F0                 2784 	movx	@dptr,a
   0897 A3                 2785 	inc	dptr
   0898 EE                 2786 	mov	a,r6
   0899 F0                 2787 	movx	@dptr,a
                           2788 ;	Peephole 112.b	changed ljmp to sjmp
   089A 80 CC              2789 	sjmp	00104$
   089C                    2790 00123$:
                           2791 ;	genAssign
   089C 90 34 25           2792 	mov	dptr,#_displaydelete_charnum1_1_1
   089F EA                 2793 	mov	a,r2
   08A0 F0                 2794 	movx	@dptr,a
   08A1 A3                 2795 	inc	dptr
   08A2 EB                 2796 	mov	a,r3
   08A3 F0                 2797 	movx	@dptr,a
                           2798 ;	main.c:276: printf_tiny("Number of characters in buffer1 = %d\n\r", charnum1);
                           2799 ;	genIpush
   08A4 C0 02              2800 	push	ar2
   08A6 C0 03              2801 	push	ar3
   08A8 C0 02              2802 	push	ar2
   08AA C0 03              2803 	push	ar3
                           2804 ;	genIpush
   08AC 74 CB              2805 	mov	a,#__str_28
   08AE C0 E0              2806 	push	acc
   08B0 74 16              2807 	mov	a,#(__str_28 >> 8)
   08B2 C0 E0              2808 	push	acc
                           2809 ;	genCall
   08B4 12 10 5A           2810 	lcall	_printf_tiny
   08B7 E5 81              2811 	mov	a,sp
   08B9 24 FC              2812 	add	a,#0xfc
   08BB F5 81              2813 	mov	sp,a
   08BD D0 03              2814 	pop	ar3
   08BF D0 02              2815 	pop	ar2
                           2816 ;	main.c:277: buff2ptr=buff2ptr-charnum1;
                           2817 ;	genAssign
   08C1 90 32 0D           2818 	mov	dptr,#_buff2ptr
   08C4 E0                 2819 	movx	a,@dptr
   08C5 FC                 2820 	mov	r4,a
   08C6 A3                 2821 	inc	dptr
   08C7 E0                 2822 	movx	a,@dptr
   08C8 FD                 2823 	mov	r5,a
   08C9 A3                 2824 	inc	dptr
   08CA E0                 2825 	movx	a,@dptr
   08CB FE                 2826 	mov	r6,a
                           2827 ;	genMinus
   08CC 90 32 0D           2828 	mov	dptr,#_buff2ptr
   08CF EC                 2829 	mov	a,r4
   08D0 C3                 2830 	clr	c
                           2831 ;	Peephole 236.l	used r2 instead of ar2
   08D1 9A                 2832 	subb	a,r2
   08D2 F0                 2833 	movx	@dptr,a
   08D3 ED                 2834 	mov	a,r5
                           2835 ;	Peephole 236.l	used r3 instead of ar3
   08D4 9B                 2836 	subb	a,r3
   08D5 A3                 2837 	inc	dptr
   08D6 F0                 2838 	movx	@dptr,a
   08D7 A3                 2839 	inc	dptr
   08D8 EE                 2840 	mov	a,r6
   08D9 F0                 2841 	movx	@dptr,a
                           2842 ;	main.c:278: printf_tiny("\n\r---------------------------------------------------------------------------------------\n\r");
                           2843 ;	genIpush
   08DA 74 46              2844 	mov	a,#__str_26
   08DC C0 E0              2845 	push	acc
   08DE 74 16              2846 	mov	a,#(__str_26 >> 8)
   08E0 C0 E0              2847 	push	acc
                           2848 ;	genCall
   08E2 12 10 5A           2849 	lcall	_printf_tiny
   08E5 15 81              2850 	dec	sp
   08E7 15 81              2851 	dec	sp
                           2852 ;	main.c:279: for(len=0; len<buffer_count; len++)
                           2853 ;	genAssign
   08E9 7A 00              2854 	mov	r2,#0x00
   08EB 7B 00              2855 	mov	r3,#0x00
   08ED                    2856 00109$:
                           2857 ;	genAssign
   08ED 90 30 00           2858 	mov	dptr,#_buffer_count
   08F0 E0                 2859 	movx	a,@dptr
   08F1 FC                 2860 	mov	r4,a
   08F2 A3                 2861 	inc	dptr
   08F3 E0                 2862 	movx	a,@dptr
   08F4 FD                 2863 	mov	r5,a
                           2864 ;	genCmpLt
                           2865 ;	genCmp
   08F5 C3                 2866 	clr	c
   08F6 EA                 2867 	mov	a,r2
   08F7 9C                 2868 	subb	a,r4
   08F8 EB                 2869 	mov	a,r3
   08F9 9D                 2870 	subb	a,r5
                           2871 ;	genIfxJump
   08FA 40 03              2872 	jc	00129$
   08FC 02 0A 3A           2873 	ljmp	00112$
   08FF                    2874 00129$:
                           2875 ;	main.c:281: if(buffer[len])
                           2876 ;	genLeftShift
                           2877 ;	genLeftShiftLiteral
                           2878 ;	genlshTwo
   08FF 8A 04              2879 	mov	ar4,r2
   0901 EB                 2880 	mov	a,r3
   0902 CC                 2881 	xch	a,r4
   0903 25 E0              2882 	add	a,acc
   0905 CC                 2883 	xch	a,r4
   0906 33                 2884 	rlc	a
   0907 FD                 2885 	mov	r5,a
                           2886 ;	genPlus
                           2887 ;	Peephole 236.g	used r4 instead of ar4
   0908 EC                 2888 	mov	a,r4
   0909 24 0A              2889 	add	a,#_buffer
   090B F5 82              2890 	mov	dpl,a
                           2891 ;	Peephole 236.g	used r5 instead of ar5
   090D ED                 2892 	mov	a,r5
   090E 34 30              2893 	addc	a,#(_buffer >> 8)
   0910 F5 83              2894 	mov	dph,a
                           2895 ;	genPointerGet
                           2896 ;	genFarPointerGet
   0912 E0                 2897 	movx	a,@dptr
   0913 FE                 2898 	mov	r6,a
   0914 A3                 2899 	inc	dptr
   0915 E0                 2900 	movx	a,@dptr
                           2901 ;	genIfx
   0916 FF                 2902 	mov	r7,a
                           2903 ;	Peephole 135	removed redundant mov
   0917 4E                 2904 	orl	a,r6
                           2905 ;	genIfxJump
   0918 70 03              2906 	jnz	00130$
   091A 02 0A 32           2907 	ljmp	00111$
   091D                    2908 00130$:
                           2909 ;	main.c:283: printf_tiny("---------------------------------------------------------------------------------------\n\r");
                           2910 ;	genIpush
   091D C0 02              2911 	push	ar2
   091F C0 03              2912 	push	ar3
   0921 C0 04              2913 	push	ar4
   0923 C0 05              2914 	push	ar5
   0925 74 31              2915 	mov	a,#__str_21
   0927 C0 E0              2916 	push	acc
   0929 74 15              2917 	mov	a,#(__str_21 >> 8)
   092B C0 E0              2918 	push	acc
                           2919 ;	genCall
   092D 12 10 5A           2920 	lcall	_printf_tiny
   0930 15 81              2921 	dec	sp
   0932 15 81              2922 	dec	sp
   0934 D0 05              2923 	pop	ar5
   0936 D0 04              2924 	pop	ar4
   0938 D0 03              2925 	pop	ar3
   093A D0 02              2926 	pop	ar2
                           2927 ;	main.c:284: printf_tiny("Buffer %d Information\n\r", len);
                           2928 ;	genIpush
   093C C0 02              2929 	push	ar2
   093E C0 03              2930 	push	ar3
   0940 C0 04              2931 	push	ar4
   0942 C0 05              2932 	push	ar5
   0944 C0 02              2933 	push	ar2
   0946 C0 03              2934 	push	ar3
                           2935 ;	genIpush
   0948 74 F2              2936 	mov	a,#__str_29
   094A C0 E0              2937 	push	acc
   094C 74 16              2938 	mov	a,#(__str_29 >> 8)
   094E C0 E0              2939 	push	acc
                           2940 ;	genCall
   0950 12 10 5A           2941 	lcall	_printf_tiny
   0953 E5 81              2942 	mov	a,sp
   0955 24 FC              2943 	add	a,#0xfc
   0957 F5 81              2944 	mov	sp,a
   0959 D0 05              2945 	pop	ar5
   095B D0 04              2946 	pop	ar4
   095D D0 03              2947 	pop	ar3
   095F D0 02              2948 	pop	ar2
                           2949 ;	main.c:285: printf_tiny("Starting address = 0x%x\n\r",(unsigned int)buffer[len]);
                           2950 ;	genPlus
                           2951 ;	Peephole 236.g	used r4 instead of ar4
   0961 EC                 2952 	mov	a,r4
   0962 24 0A              2953 	add	a,#_buffer
   0964 F5 82              2954 	mov	dpl,a
                           2955 ;	Peephole 236.g	used r5 instead of ar5
   0966 ED                 2956 	mov	a,r5
   0967 34 30              2957 	addc	a,#(_buffer >> 8)
   0969 F5 83              2958 	mov	dph,a
                           2959 ;	genPointerGet
                           2960 ;	genFarPointerGet
   096B E0                 2961 	movx	a,@dptr
   096C FE                 2962 	mov	r6,a
   096D A3                 2963 	inc	dptr
   096E E0                 2964 	movx	a,@dptr
   096F FF                 2965 	mov	r7,a
                           2966 ;	genCast
                           2967 ;	genIpush
   0970 C0 02              2968 	push	ar2
   0972 C0 03              2969 	push	ar3
   0974 C0 04              2970 	push	ar4
   0976 C0 05              2971 	push	ar5
   0978 C0 06              2972 	push	ar6
   097A C0 07              2973 	push	ar7
                           2974 ;	genIpush
   097C 74 0A              2975 	mov	a,#__str_30
   097E C0 E0              2976 	push	acc
   0980 74 17              2977 	mov	a,#(__str_30 >> 8)
   0982 C0 E0              2978 	push	acc
                           2979 ;	genCall
   0984 12 10 5A           2980 	lcall	_printf_tiny
   0987 E5 81              2981 	mov	a,sp
   0989 24 FC              2982 	add	a,#0xfc
   098B F5 81              2983 	mov	sp,a
   098D D0 05              2984 	pop	ar5
   098F D0 04              2985 	pop	ar4
   0991 D0 03              2986 	pop	ar3
   0993 D0 02              2987 	pop	ar2
                           2988 ;	main.c:286: printf_tiny("Ending address = 0x%x\n\r",((unsigned int)buffer[len]+(unsigned int)bufflen[len]));
                           2989 ;	genPlus
                           2990 ;	Peephole 236.g	used r4 instead of ar4
   0995 EC                 2991 	mov	a,r4
   0996 24 0A              2992 	add	a,#_buffer
   0998 F5 82              2993 	mov	dpl,a
                           2994 ;	Peephole 236.g	used r5 instead of ar5
   099A ED                 2995 	mov	a,r5
   099B 34 30              2996 	addc	a,#(_buffer >> 8)
   099D F5 83              2997 	mov	dph,a
                           2998 ;	genPointerGet
                           2999 ;	genFarPointerGet
   099F E0                 3000 	movx	a,@dptr
   09A0 FE                 3001 	mov	r6,a
   09A1 A3                 3002 	inc	dptr
   09A2 E0                 3003 	movx	a,@dptr
   09A3 FF                 3004 	mov	r7,a
                           3005 ;	genCast
                           3006 ;	genPlus
                           3007 ;	Peephole 236.g	used r4 instead of ar4
   09A4 EC                 3008 	mov	a,r4
   09A5 24 10              3009 	add	a,#_bufflen
   09A7 F5 82              3010 	mov	dpl,a
                           3011 ;	Peephole 236.g	used r5 instead of ar5
   09A9 ED                 3012 	mov	a,r5
   09AA 34 32              3013 	addc	a,#(_bufflen >> 8)
   09AC F5 83              3014 	mov	dph,a
                           3015 ;	genPointerGet
                           3016 ;	genFarPointerGet
   09AE E0                 3017 	movx	a,@dptr
   09AF F8                 3018 	mov	r0,a
   09B0 A3                 3019 	inc	dptr
   09B1 E0                 3020 	movx	a,@dptr
   09B2 F9                 3021 	mov	r1,a
                           3022 ;	genPlus
                           3023 ;	Peephole 236.g	used r0 instead of ar0
   09B3 E8                 3024 	mov	a,r0
                           3025 ;	Peephole 236.a	used r6 instead of ar6
   09B4 2E                 3026 	add	a,r6
   09B5 FE                 3027 	mov	r6,a
                           3028 ;	Peephole 236.g	used r1 instead of ar1
   09B6 E9                 3029 	mov	a,r1
                           3030 ;	Peephole 236.b	used r7 instead of ar7
   09B7 3F                 3031 	addc	a,r7
   09B8 FF                 3032 	mov	r7,a
                           3033 ;	genIpush
   09B9 C0 02              3034 	push	ar2
   09BB C0 03              3035 	push	ar3
   09BD C0 04              3036 	push	ar4
   09BF C0 05              3037 	push	ar5
   09C1 C0 06              3038 	push	ar6
   09C3 C0 07              3039 	push	ar7
                           3040 ;	genIpush
   09C5 74 24              3041 	mov	a,#__str_31
   09C7 C0 E0              3042 	push	acc
   09C9 74 17              3043 	mov	a,#(__str_31 >> 8)
   09CB C0 E0              3044 	push	acc
                           3045 ;	genCall
   09CD 12 10 5A           3046 	lcall	_printf_tiny
   09D0 E5 81              3047 	mov	a,sp
   09D2 24 FC              3048 	add	a,#0xfc
   09D4 F5 81              3049 	mov	sp,a
   09D6 D0 05              3050 	pop	ar5
   09D8 D0 04              3051 	pop	ar4
   09DA D0 03              3052 	pop	ar3
   09DC D0 02              3053 	pop	ar2
                           3054 ;	main.c:287: printf_tiny("Allocated size = %d\n\r", bufflen[len]);
                           3055 ;	genPlus
                           3056 ;	Peephole 236.g	used r4 instead of ar4
   09DE EC                 3057 	mov	a,r4
   09DF 24 10              3058 	add	a,#_bufflen
   09E1 F5 82              3059 	mov	dpl,a
                           3060 ;	Peephole 236.g	used r5 instead of ar5
   09E3 ED                 3061 	mov	a,r5
   09E4 34 32              3062 	addc	a,#(_bufflen >> 8)
   09E6 F5 83              3063 	mov	dph,a
                           3064 ;	genPointerGet
                           3065 ;	genFarPointerGet
   09E8 E0                 3066 	movx	a,@dptr
   09E9 FC                 3067 	mov	r4,a
   09EA A3                 3068 	inc	dptr
   09EB E0                 3069 	movx	a,@dptr
   09EC FD                 3070 	mov	r5,a
                           3071 ;	genIpush
   09ED C0 02              3072 	push	ar2
   09EF C0 03              3073 	push	ar3
   09F1 C0 04              3074 	push	ar4
   09F3 C0 05              3075 	push	ar5
                           3076 ;	genIpush
   09F5 74 3C              3077 	mov	a,#__str_32
   09F7 C0 E0              3078 	push	acc
   09F9 74 17              3079 	mov	a,#(__str_32 >> 8)
   09FB C0 E0              3080 	push	acc
                           3081 ;	genCall
   09FD 12 10 5A           3082 	lcall	_printf_tiny
   0A00 E5 81              3083 	mov	a,sp
   0A02 24 FC              3084 	add	a,#0xfc
   0A04 F5 81              3085 	mov	sp,a
   0A06 D0 03              3086 	pop	ar3
   0A08 D0 02              3087 	pop	ar2
                           3088 ;	main.c:288: printf_tiny("---------------------------------------------------------------------------------------\n\r");
                           3089 ;	genIpush
   0A0A C0 02              3090 	push	ar2
   0A0C C0 03              3091 	push	ar3
   0A0E 74 31              3092 	mov	a,#__str_21
   0A10 C0 E0              3093 	push	acc
   0A12 74 15              3094 	mov	a,#(__str_21 >> 8)
   0A14 C0 E0              3095 	push	acc
                           3096 ;	genCall
   0A16 12 10 5A           3097 	lcall	_printf_tiny
   0A19 15 81              3098 	dec	sp
   0A1B 15 81              3099 	dec	sp
   0A1D D0 03              3100 	pop	ar3
   0A1F D0 02              3101 	pop	ar2
                           3102 ;	main.c:289: putstr("\n\r");
                           3103 ;	genCall
                           3104 ;	Peephole 182.a	used 16 bit load of DPTR
   0A21 90 12 D2           3105 	mov	dptr,#__str_1
   0A24 75 F0 80           3106 	mov	b,#0x80
   0A27 C0 02              3107 	push	ar2
   0A29 C0 03              3108 	push	ar3
   0A2B 12 0C F8           3109 	lcall	_putstr
   0A2E D0 03              3110 	pop	ar3
   0A30 D0 02              3111 	pop	ar2
   0A32                    3112 00111$:
                           3113 ;	main.c:279: for(len=0; len<buffer_count; len++)
                           3114 ;	genPlus
                           3115 ;     genPlusIncr
   0A32 0A                 3116 	inc	r2
   0A33 BA 00 01           3117 	cjne	r2,#0x00,00131$
   0A36 0B                 3118 	inc	r3
   0A37                    3119 00131$:
   0A37 02 08 ED           3120 	ljmp	00109$
   0A3A                    3121 00112$:
                           3122 ;	main.c:292: printf_tiny("---------------------------------------------------------------------------------------\n\r");
                           3123 ;	genIpush
   0A3A 74 31              3124 	mov	a,#__str_21
   0A3C C0 E0              3125 	push	acc
   0A3E 74 15              3126 	mov	a,#(__str_21 >> 8)
   0A40 C0 E0              3127 	push	acc
                           3128 ;	genCall
   0A42 12 10 5A           3129 	lcall	_printf_tiny
   0A45 15 81              3130 	dec	sp
   0A47 15 81              3131 	dec	sp
                           3132 ;	main.c:293: putstr("\n\rContents of Buffer0\n\r");
                           3133 ;	genCall
                           3134 ;	Peephole 182.a	used 16 bit load of DPTR
   0A49 90 17 52           3135 	mov	dptr,#__str_33
   0A4C 75 F0 80           3136 	mov	b,#0x80
   0A4F 12 0C F8           3137 	lcall	_putstr
                           3138 ;	main.c:294: putstrbuff(buff1ptr);
                           3139 ;	genAssign
   0A52 90 32 0A           3140 	mov	dptr,#_buff1ptr
   0A55 E0                 3141 	movx	a,@dptr
   0A56 FA                 3142 	mov	r2,a
   0A57 A3                 3143 	inc	dptr
   0A58 E0                 3144 	movx	a,@dptr
   0A59 FB                 3145 	mov	r3,a
   0A5A A3                 3146 	inc	dptr
   0A5B E0                 3147 	movx	a,@dptr
   0A5C FC                 3148 	mov	r4,a
                           3149 ;	genCall
   0A5D 8A 82              3150 	mov	dpl,r2
   0A5F 8B 83              3151 	mov	dph,r3
   0A61 8C F0              3152 	mov	b,r4
   0A63 12 0C 39           3153 	lcall	_putstrbuff
                           3154 ;	main.c:296: bptr0=bptr0-charnum0;
                           3155 ;	genAssign
   0A66 90 34 23           3156 	mov	dptr,#_displaydelete_charnum0_1_1
   0A69 E0                 3157 	movx	a,@dptr
   0A6A FA                 3158 	mov	r2,a
   0A6B A3                 3159 	inc	dptr
   0A6C E0                 3160 	movx	a,@dptr
   0A6D FB                 3161 	mov	r3,a
                           3162 ;	genAssign
   0A6E 90 34 1B           3163 	mov	dptr,#_bptr0
   0A71 E0                 3164 	movx	a,@dptr
   0A72 FC                 3165 	mov	r4,a
   0A73 A3                 3166 	inc	dptr
   0A74 E0                 3167 	movx	a,@dptr
   0A75 FD                 3168 	mov	r5,a
   0A76 A3                 3169 	inc	dptr
   0A77 E0                 3170 	movx	a,@dptr
   0A78 FE                 3171 	mov	r6,a
                           3172 ;	genMinus
   0A79 90 34 1B           3173 	mov	dptr,#_bptr0
   0A7C EC                 3174 	mov	a,r4
   0A7D C3                 3175 	clr	c
                           3176 ;	Peephole 236.l	used r2 instead of ar2
   0A7E 9A                 3177 	subb	a,r2
   0A7F F0                 3178 	movx	@dptr,a
   0A80 ED                 3179 	mov	a,r5
                           3180 ;	Peephole 236.l	used r3 instead of ar3
   0A81 9B                 3181 	subb	a,r3
   0A82 A3                 3182 	inc	dptr
   0A83 F0                 3183 	movx	@dptr,a
   0A84 A3                 3184 	inc	dptr
   0A85 EE                 3185 	mov	a,r6
   0A86 F0                 3186 	movx	@dptr,a
                           3187 ;	main.c:297: putstr("\n\r");
                           3188 ;	genCall
                           3189 ;	Peephole 182.a	used 16 bit load of DPTR
   0A87 90 12 D2           3190 	mov	dptr,#__str_1
   0A8A 75 F0 80           3191 	mov	b,#0x80
   0A8D 12 0C F8           3192 	lcall	_putstr
                           3193 ;	main.c:298: printf_tiny("---------------------------------------------------------------------------------------\n\r");
                           3194 ;	genIpush
   0A90 74 31              3195 	mov	a,#__str_21
   0A92 C0 E0              3196 	push	acc
   0A94 74 15              3197 	mov	a,#(__str_21 >> 8)
   0A96 C0 E0              3198 	push	acc
                           3199 ;	genCall
   0A98 12 10 5A           3200 	lcall	_printf_tiny
   0A9B 15 81              3201 	dec	sp
   0A9D 15 81              3202 	dec	sp
                           3203 ;	main.c:299: putstr("\n\rContents of Buffer1\n\r");
                           3204 ;	genCall
                           3205 ;	Peephole 182.a	used 16 bit load of DPTR
   0A9F 90 17 6A           3206 	mov	dptr,#__str_34
   0AA2 75 F0 80           3207 	mov	b,#0x80
   0AA5 12 0C F8           3208 	lcall	_putstr
                           3209 ;	main.c:300: putstrbuff(buff2ptr);
                           3210 ;	genAssign
   0AA8 90 32 0D           3211 	mov	dptr,#_buff2ptr
   0AAB E0                 3212 	movx	a,@dptr
   0AAC FA                 3213 	mov	r2,a
   0AAD A3                 3214 	inc	dptr
   0AAE E0                 3215 	movx	a,@dptr
   0AAF FB                 3216 	mov	r3,a
   0AB0 A3                 3217 	inc	dptr
   0AB1 E0                 3218 	movx	a,@dptr
   0AB2 FC                 3219 	mov	r4,a
                           3220 ;	genCall
   0AB3 8A 82              3221 	mov	dpl,r2
   0AB5 8B 83              3222 	mov	dph,r3
   0AB7 8C F0              3223 	mov	b,r4
   0AB9 12 0C 39           3224 	lcall	_putstrbuff
                           3225 ;	main.c:302: bptr1=bptr1-charnum1;
                           3226 ;	genAssign
   0ABC 90 34 25           3227 	mov	dptr,#_displaydelete_charnum1_1_1
   0ABF E0                 3228 	movx	a,@dptr
   0AC0 FA                 3229 	mov	r2,a
   0AC1 A3                 3230 	inc	dptr
   0AC2 E0                 3231 	movx	a,@dptr
   0AC3 FB                 3232 	mov	r3,a
                           3233 ;	genAssign
   0AC4 90 34 1E           3234 	mov	dptr,#_bptr1
   0AC7 E0                 3235 	movx	a,@dptr
   0AC8 FC                 3236 	mov	r4,a
   0AC9 A3                 3237 	inc	dptr
   0ACA E0                 3238 	movx	a,@dptr
   0ACB FD                 3239 	mov	r5,a
   0ACC A3                 3240 	inc	dptr
   0ACD E0                 3241 	movx	a,@dptr
   0ACE FE                 3242 	mov	r6,a
                           3243 ;	genMinus
   0ACF 90 34 1E           3244 	mov	dptr,#_bptr1
   0AD2 EC                 3245 	mov	a,r4
   0AD3 C3                 3246 	clr	c
                           3247 ;	Peephole 236.l	used r2 instead of ar2
   0AD4 9A                 3248 	subb	a,r2
   0AD5 F0                 3249 	movx	@dptr,a
   0AD6 ED                 3250 	mov	a,r5
                           3251 ;	Peephole 236.l	used r3 instead of ar3
   0AD7 9B                 3252 	subb	a,r3
   0AD8 A3                 3253 	inc	dptr
   0AD9 F0                 3254 	movx	@dptr,a
   0ADA A3                 3255 	inc	dptr
   0ADB EE                 3256 	mov	a,r6
   0ADC F0                 3257 	movx	@dptr,a
                           3258 ;	main.c:303: putstr("\n\r");
                           3259 ;	genCall
                           3260 ;	Peephole 182.a	used 16 bit load of DPTR
   0ADD 90 12 D2           3261 	mov	dptr,#__str_1
   0AE0 75 F0 80           3262 	mov	b,#0x80
   0AE3 12 0C F8           3263 	lcall	_putstr
                           3264 ;	main.c:304: printf_tiny("---------------------------------------------------------------------------------------\n\r");
                           3265 ;	genIpush
   0AE6 74 31              3266 	mov	a,#__str_21
   0AE8 C0 E0              3267 	push	acc
   0AEA 74 15              3268 	mov	a,#(__str_21 >> 8)
   0AEC C0 E0              3269 	push	acc
                           3270 ;	genCall
   0AEE 12 10 5A           3271 	lcall	_printf_tiny
   0AF1 15 81              3272 	dec	sp
   0AF3 15 81              3273 	dec	sp
                           3274 ;	Peephole 300	removed redundant label 00113$
   0AF5 22                 3275 	ret
                           3276 ;------------------------------------------------------------
                           3277 ;Allocation info for local variables in function 'display'
                           3278 ;------------------------------------------------------------
                           3279 ;------------------------------------------------------------
                           3280 ;	main.c:308: void display()
                           3281 ;	-----------------------------------------
                           3282 ;	 function display
                           3283 ;	-----------------------------------------
   0AF6                    3284 _display:
                           3285 ;	main.c:310: putstr("\n\rContents of Buffer0\n\r");
                           3286 ;	genCall
                           3287 ;	Peephole 182.a	used 16 bit load of DPTR
   0AF6 90 17 52           3288 	mov	dptr,#__str_33
   0AF9 75 F0 80           3289 	mov	b,#0x80
   0AFC 12 0C F8           3290 	lcall	_putstr
                           3291 ;	main.c:311: putstrhex(buff1ptr);
                           3292 ;	genAssign
   0AFF 90 32 0A           3293 	mov	dptr,#_buff1ptr
   0B02 E0                 3294 	movx	a,@dptr
   0B03 FA                 3295 	mov	r2,a
   0B04 A3                 3296 	inc	dptr
   0B05 E0                 3297 	movx	a,@dptr
   0B06 FB                 3298 	mov	r3,a
   0B07 A3                 3299 	inc	dptr
   0B08 E0                 3300 	movx	a,@dptr
   0B09 FC                 3301 	mov	r4,a
                           3302 ;	genCall
   0B0A 8A 82              3303 	mov	dpl,r2
   0B0C 8B 83              3304 	mov	dph,r3
   0B0E 8C F0              3305 	mov	b,r4
   0B10 12 0B 42           3306 	lcall	_putstrhex
                           3307 ;	main.c:313: putstr("\n\r");
                           3308 ;	genCall
                           3309 ;	Peephole 182.a	used 16 bit load of DPTR
   0B13 90 12 D2           3310 	mov	dptr,#__str_1
   0B16 75 F0 80           3311 	mov	b,#0x80
   0B19 12 0C F8           3312 	lcall	_putstr
                           3313 ;	main.c:314: putstr("Contents of Buffer1\n\r");
                           3314 ;	genCall
                           3315 ;	Peephole 182.a	used 16 bit load of DPTR
   0B1C 90 17 82           3316 	mov	dptr,#__str_35
   0B1F 75 F0 80           3317 	mov	b,#0x80
   0B22 12 0C F8           3318 	lcall	_putstr
                           3319 ;	main.c:315: putstrhex(buff2ptr);
                           3320 ;	genAssign
   0B25 90 32 0D           3321 	mov	dptr,#_buff2ptr
   0B28 E0                 3322 	movx	a,@dptr
   0B29 FA                 3323 	mov	r2,a
   0B2A A3                 3324 	inc	dptr
   0B2B E0                 3325 	movx	a,@dptr
   0B2C FB                 3326 	mov	r3,a
   0B2D A3                 3327 	inc	dptr
   0B2E E0                 3328 	movx	a,@dptr
   0B2F FC                 3329 	mov	r4,a
                           3330 ;	genCall
   0B30 8A 82              3331 	mov	dpl,r2
   0B32 8B 83              3332 	mov	dph,r3
   0B34 8C F0              3333 	mov	b,r4
   0B36 12 0B 42           3334 	lcall	_putstrhex
                           3335 ;	main.c:317: putstr("\n\r");
                           3336 ;	genCall
                           3337 ;	Peephole 182.a	used 16 bit load of DPTR
   0B39 90 12 D2           3338 	mov	dptr,#__str_1
   0B3C 75 F0 80           3339 	mov	b,#0x80
                           3340 ;	Peephole 253.b	replaced lcall/ret with ljmp
   0B3F 02 0C F8           3341 	ljmp	_putstr
                           3342 ;
                           3343 ;------------------------------------------------------------
                           3344 ;Allocation info for local variables in function 'putstrhex'
                           3345 ;------------------------------------------------------------
                           3346 ;ptr                       Allocated with name '_putstrhex_ptr_1_1'
                           3347 ;count                     Allocated with name '_putstrhex_count_1_1'
                           3348 ;------------------------------------------------------------
                           3349 ;	main.c:320: void putstrhex(char *ptr)
                           3350 ;	-----------------------------------------
                           3351 ;	 function putstrhex
                           3352 ;	-----------------------------------------
   0B42                    3353 _putstrhex:
                           3354 ;	genReceive
   0B42 AA F0              3355 	mov	r2,b
   0B44 AB 83              3356 	mov	r3,dph
   0B46 E5 82              3357 	mov	a,dpl
   0B48 90 34 27           3358 	mov	dptr,#_putstrhex_ptr_1_1
   0B4B F0                 3359 	movx	@dptr,a
   0B4C A3                 3360 	inc	dptr
   0B4D EB                 3361 	mov	a,r3
   0B4E F0                 3362 	movx	@dptr,a
   0B4F A3                 3363 	inc	dptr
   0B50 EA                 3364 	mov	a,r2
   0B51 F0                 3365 	movx	@dptr,a
                           3366 ;	main.c:322: unsigned int count=0;
                           3367 ;	genAssign
   0B52 90 34 2A           3368 	mov	dptr,#_putstrhex_count_1_1
   0B55 E4                 3369 	clr	a
   0B56 F0                 3370 	movx	@dptr,a
   0B57 A3                 3371 	inc	dptr
   0B58 F0                 3372 	movx	@dptr,a
                           3373 ;	main.c:323: printf_tiny("0x%x:  ",&(ptr));
                           3374 ;	genIpush
   0B59 74 27              3375 	mov	a,#_putstrhex_ptr_1_1
   0B5B C0 E0              3376 	push	acc
   0B5D 74 34              3377 	mov	a,#(_putstrhex_ptr_1_1 >> 8)
   0B5F C0 E0              3378 	push	acc
                           3379 ;	Peephole 181	changed mov to clr
   0B61 E4                 3380 	clr	a
   0B62 C0 E0              3381 	push	acc
                           3382 ;	genIpush
   0B64 74 98              3383 	mov	a,#__str_36
   0B66 C0 E0              3384 	push	acc
   0B68 74 17              3385 	mov	a,#(__str_36 >> 8)
   0B6A C0 E0              3386 	push	acc
                           3387 ;	genCall
   0B6C 12 10 5A           3388 	lcall	_printf_tiny
   0B6F E5 81              3389 	mov	a,sp
   0B71 24 FB              3390 	add	a,#0xfb
   0B73 F5 81              3391 	mov	sp,a
                           3392 ;	main.c:324: while(*ptr)
   0B75                    3393 00105$:
                           3394 ;	genAssign
   0B75 90 34 27           3395 	mov	dptr,#_putstrhex_ptr_1_1
   0B78 E0                 3396 	movx	a,@dptr
   0B79 FA                 3397 	mov	r2,a
   0B7A A3                 3398 	inc	dptr
   0B7B E0                 3399 	movx	a,@dptr
   0B7C FB                 3400 	mov	r3,a
   0B7D A3                 3401 	inc	dptr
   0B7E E0                 3402 	movx	a,@dptr
   0B7F FC                 3403 	mov	r4,a
                           3404 ;	genPointerGet
                           3405 ;	genGenPointerGet
   0B80 8A 82              3406 	mov	dpl,r2
   0B82 8B 83              3407 	mov	dph,r3
   0B84 8C F0              3408 	mov	b,r4
   0B86 12 12 9E           3409 	lcall	__gptrget
                           3410 ;	genIfxJump
   0B89 70 01              3411 	jnz	00114$
                           3412 ;	Peephole 251.a	replaced ljmp to ret with ret
   0B8B 22                 3413 	ret
   0B8C                    3414 00114$:
                           3415 ;	main.c:326: if (count==15)
                           3416 ;	genAssign
   0B8C 90 34 2A           3417 	mov	dptr,#_putstrhex_count_1_1
   0B8F E0                 3418 	movx	a,@dptr
   0B90 FA                 3419 	mov	r2,a
   0B91 A3                 3420 	inc	dptr
   0B92 E0                 3421 	movx	a,@dptr
   0B93 FB                 3422 	mov	r3,a
                           3423 ;	genCmpEq
                           3424 ;	gencjneshort
                           3425 ;	Peephole 112.b	changed ljmp to sjmp
                           3426 ;	Peephole 198.a	optimized misc jump sequence
   0B94 BA 0F 1F           3427 	cjne	r2,#0x0F,00102$
   0B97 BB 00 1C           3428 	cjne	r3,#0x00,00102$
                           3429 ;	Peephole 200.b	removed redundant sjmp
                           3430 ;	Peephole 300	removed redundant label 00115$
                           3431 ;	Peephole 300	removed redundant label 00116$
                           3432 ;	main.c:328: printf_tiny("0x%x:  ",&(ptr));
                           3433 ;	genIpush
   0B9A 74 27              3434 	mov	a,#_putstrhex_ptr_1_1
   0B9C C0 E0              3435 	push	acc
   0B9E 74 34              3436 	mov	a,#(_putstrhex_ptr_1_1 >> 8)
   0BA0 C0 E0              3437 	push	acc
                           3438 ;	Peephole 181	changed mov to clr
   0BA2 E4                 3439 	clr	a
   0BA3 C0 E0              3440 	push	acc
                           3441 ;	genIpush
   0BA5 74 98              3442 	mov	a,#__str_36
   0BA7 C0 E0              3443 	push	acc
   0BA9 74 17              3444 	mov	a,#(__str_36 >> 8)
   0BAB C0 E0              3445 	push	acc
                           3446 ;	genCall
   0BAD 12 10 5A           3447 	lcall	_printf_tiny
   0BB0 E5 81              3448 	mov	a,sp
   0BB2 24 FB              3449 	add	a,#0xfb
   0BB4 F5 81              3450 	mov	sp,a
   0BB6                    3451 00102$:
                           3452 ;	main.c:330: printf_tiny("%x ",*ptr);
                           3453 ;	genAssign
   0BB6 90 34 27           3454 	mov	dptr,#_putstrhex_ptr_1_1
   0BB9 E0                 3455 	movx	a,@dptr
   0BBA FA                 3456 	mov	r2,a
   0BBB A3                 3457 	inc	dptr
   0BBC E0                 3458 	movx	a,@dptr
   0BBD FB                 3459 	mov	r3,a
   0BBE A3                 3460 	inc	dptr
   0BBF E0                 3461 	movx	a,@dptr
   0BC0 FC                 3462 	mov	r4,a
                           3463 ;	genPointerGet
                           3464 ;	genGenPointerGet
   0BC1 8A 82              3465 	mov	dpl,r2
   0BC3 8B 83              3466 	mov	dph,r3
   0BC5 8C F0              3467 	mov	b,r4
   0BC7 12 12 9E           3468 	lcall	__gptrget
                           3469 ;	genCast
   0BCA FA                 3470 	mov	r2,a
                           3471 ;	Peephole 105	removed redundant mov
   0BCB 33                 3472 	rlc	a
   0BCC 95 E0              3473 	subb	a,acc
   0BCE FB                 3474 	mov	r3,a
                           3475 ;	genIpush
   0BCF C0 02              3476 	push	ar2
   0BD1 C0 03              3477 	push	ar3
                           3478 ;	genIpush
   0BD3 74 A0              3479 	mov	a,#__str_37
   0BD5 C0 E0              3480 	push	acc
   0BD7 74 17              3481 	mov	a,#(__str_37 >> 8)
   0BD9 C0 E0              3482 	push	acc
                           3483 ;	genCall
   0BDB 12 10 5A           3484 	lcall	_printf_tiny
   0BDE E5 81              3485 	mov	a,sp
   0BE0 24 FC              3486 	add	a,#0xfc
   0BE2 F5 81              3487 	mov	sp,a
                           3488 ;	main.c:331: ptr++;
                           3489 ;	genAssign
   0BE4 90 34 27           3490 	mov	dptr,#_putstrhex_ptr_1_1
   0BE7 E0                 3491 	movx	a,@dptr
   0BE8 FA                 3492 	mov	r2,a
   0BE9 A3                 3493 	inc	dptr
   0BEA E0                 3494 	movx	a,@dptr
   0BEB FB                 3495 	mov	r3,a
   0BEC A3                 3496 	inc	dptr
   0BED E0                 3497 	movx	a,@dptr
   0BEE FC                 3498 	mov	r4,a
                           3499 ;	genPlus
   0BEF 90 34 27           3500 	mov	dptr,#_putstrhex_ptr_1_1
                           3501 ;     genPlusIncr
   0BF2 74 01              3502 	mov	a,#0x01
                           3503 ;	Peephole 236.a	used r2 instead of ar2
   0BF4 2A                 3504 	add	a,r2
   0BF5 F0                 3505 	movx	@dptr,a
                           3506 ;	Peephole 181	changed mov to clr
   0BF6 E4                 3507 	clr	a
                           3508 ;	Peephole 236.b	used r3 instead of ar3
   0BF7 3B                 3509 	addc	a,r3
   0BF8 A3                 3510 	inc	dptr
   0BF9 F0                 3511 	movx	@dptr,a
   0BFA A3                 3512 	inc	dptr
   0BFB EC                 3513 	mov	a,r4
   0BFC F0                 3514 	movx	@dptr,a
                           3515 ;	main.c:332: if (count>15)
                           3516 ;	genAssign
   0BFD 90 34 2A           3517 	mov	dptr,#_putstrhex_count_1_1
   0C00 E0                 3518 	movx	a,@dptr
   0C01 FA                 3519 	mov	r2,a
   0C02 A3                 3520 	inc	dptr
   0C03 E0                 3521 	movx	a,@dptr
   0C04 FB                 3522 	mov	r3,a
                           3523 ;	genCmpGt
                           3524 ;	genCmp
   0C05 C3                 3525 	clr	c
   0C06 74 0F              3526 	mov	a,#0x0F
   0C08 9A                 3527 	subb	a,r2
                           3528 ;	Peephole 181	changed mov to clr
   0C09 E4                 3529 	clr	a
   0C0A 9B                 3530 	subb	a,r3
                           3531 ;	genIfxJump
                           3532 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0C0B 50 16              3533 	jnc	00104$
                           3534 ;	Peephole 300	removed redundant label 00117$
                           3535 ;	main.c:334: printf_tiny("\n\r");
                           3536 ;	genIpush
   0C0D 74 D2              3537 	mov	a,#__str_1
   0C0F C0 E0              3538 	push	acc
   0C11 74 12              3539 	mov	a,#(__str_1 >> 8)
   0C13 C0 E0              3540 	push	acc
                           3541 ;	genCall
   0C15 12 10 5A           3542 	lcall	_printf_tiny
   0C18 15 81              3543 	dec	sp
   0C1A 15 81              3544 	dec	sp
                           3545 ;	main.c:335: count=0;
                           3546 ;	genAssign
   0C1C 90 34 2A           3547 	mov	dptr,#_putstrhex_count_1_1
   0C1F E4                 3548 	clr	a
   0C20 F0                 3549 	movx	@dptr,a
   0C21 A3                 3550 	inc	dptr
   0C22 F0                 3551 	movx	@dptr,a
   0C23                    3552 00104$:
                           3553 ;	main.c:337: count++;
                           3554 ;	genAssign
   0C23 90 34 2A           3555 	mov	dptr,#_putstrhex_count_1_1
   0C26 E0                 3556 	movx	a,@dptr
   0C27 FA                 3557 	mov	r2,a
   0C28 A3                 3558 	inc	dptr
   0C29 E0                 3559 	movx	a,@dptr
   0C2A FB                 3560 	mov	r3,a
                           3561 ;	genPlus
   0C2B 90 34 2A           3562 	mov	dptr,#_putstrhex_count_1_1
                           3563 ;     genPlusIncr
   0C2E 74 01              3564 	mov	a,#0x01
                           3565 ;	Peephole 236.a	used r2 instead of ar2
   0C30 2A                 3566 	add	a,r2
   0C31 F0                 3567 	movx	@dptr,a
                           3568 ;	Peephole 181	changed mov to clr
   0C32 E4                 3569 	clr	a
                           3570 ;	Peephole 236.b	used r3 instead of ar3
   0C33 3B                 3571 	addc	a,r3
   0C34 A3                 3572 	inc	dptr
   0C35 F0                 3573 	movx	@dptr,a
   0C36 02 0B 75           3574 	ljmp	00105$
                           3575 ;	Peephole 259.b	removed redundant label 00108$ and ret
                           3576 ;
                           3577 ;------------------------------------------------------------
                           3578 ;Allocation info for local variables in function 'putstrbuff'
                           3579 ;------------------------------------------------------------
                           3580 ;ptr                       Allocated with name '_putstrbuff_ptr_1_1'
                           3581 ;count                     Allocated with name '_putstrbuff_count_1_1'
                           3582 ;------------------------------------------------------------
                           3583 ;	main.c:343: void putstrbuff(char *ptr)
                           3584 ;	-----------------------------------------
                           3585 ;	 function putstrbuff
                           3586 ;	-----------------------------------------
   0C39                    3587 _putstrbuff:
                           3588 ;	genReceive
   0C39 AA F0              3589 	mov	r2,b
   0C3B AB 83              3590 	mov	r3,dph
   0C3D E5 82              3591 	mov	a,dpl
   0C3F 90 34 2C           3592 	mov	dptr,#_putstrbuff_ptr_1_1
   0C42 F0                 3593 	movx	@dptr,a
   0C43 A3                 3594 	inc	dptr
   0C44 EB                 3595 	mov	a,r3
   0C45 F0                 3596 	movx	@dptr,a
   0C46 A3                 3597 	inc	dptr
   0C47 EA                 3598 	mov	a,r2
   0C48 F0                 3599 	movx	@dptr,a
                           3600 ;	main.c:345: unsigned int count=0;
                           3601 ;	genAssign
   0C49 90 34 2F           3602 	mov	dptr,#_putstrbuff_count_1_1
   0C4C E4                 3603 	clr	a
   0C4D F0                 3604 	movx	@dptr,a
   0C4E A3                 3605 	inc	dptr
   0C4F F0                 3606 	movx	@dptr,a
                           3607 ;	main.c:346: while(*ptr)
                           3608 ;	genAssign
   0C50 90 34 2C           3609 	mov	dptr,#_putstrbuff_ptr_1_1
   0C53 E0                 3610 	movx	a,@dptr
   0C54 FA                 3611 	mov	r2,a
   0C55 A3                 3612 	inc	dptr
   0C56 E0                 3613 	movx	a,@dptr
   0C57 FB                 3614 	mov	r3,a
   0C58 A3                 3615 	inc	dptr
   0C59 E0                 3616 	movx	a,@dptr
   0C5A FC                 3617 	mov	r4,a
   0C5B                    3618 00103$:
                           3619 ;	genPointerGet
                           3620 ;	genGenPointerGet
   0C5B 8A 82              3621 	mov	dpl,r2
   0C5D 8B 83              3622 	mov	dph,r3
   0C5F 8C F0              3623 	mov	b,r4
   0C61 12 12 9E           3624 	lcall	__gptrget
                           3625 ;	genIfxJump
   0C64 70 03              3626 	jnz	00112$
   0C66 02 0C EC           3627 	ljmp	00111$
   0C69                    3628 00112$:
                           3629 ;	main.c:348: putchar(*ptr);
                           3630 ;	genPointerGet
                           3631 ;	genGenPointerGet
   0C69 8A 82              3632 	mov	dpl,r2
   0C6B 8B 83              3633 	mov	dph,r3
   0C6D 8C F0              3634 	mov	b,r4
   0C6F 12 12 9E           3635 	lcall	__gptrget
                           3636 ;	genCast
   0C72 FD                 3637 	mov	r5,a
                           3638 ;	Peephole 105	removed redundant mov
   0C73 33                 3639 	rlc	a
   0C74 95 E0              3640 	subb	a,acc
   0C76 FE                 3641 	mov	r6,a
                           3642 ;	genCall
   0C77 8D 82              3643 	mov	dpl,r5
   0C79 8E 83              3644 	mov	dph,r6
   0C7B C0 02              3645 	push	ar2
   0C7D C0 03              3646 	push	ar3
   0C7F C0 04              3647 	push	ar4
   0C81 12 0D 73           3648 	lcall	_putchar
   0C84 D0 04              3649 	pop	ar4
   0C86 D0 03              3650 	pop	ar3
   0C88 D0 02              3651 	pop	ar2
                           3652 ;	main.c:349: count++;
                           3653 ;	genAssign
   0C8A 90 34 2F           3654 	mov	dptr,#_putstrbuff_count_1_1
   0C8D E0                 3655 	movx	a,@dptr
   0C8E FD                 3656 	mov	r5,a
   0C8F A3                 3657 	inc	dptr
   0C90 E0                 3658 	movx	a,@dptr
   0C91 FE                 3659 	mov	r6,a
                           3660 ;	genPlus
   0C92 90 34 2F           3661 	mov	dptr,#_putstrbuff_count_1_1
                           3662 ;     genPlusIncr
   0C95 74 01              3663 	mov	a,#0x01
                           3664 ;	Peephole 236.a	used r5 instead of ar5
   0C97 2D                 3665 	add	a,r5
   0C98 F0                 3666 	movx	@dptr,a
                           3667 ;	Peephole 181	changed mov to clr
   0C99 E4                 3668 	clr	a
                           3669 ;	Peephole 236.b	used r6 instead of ar6
   0C9A 3E                 3670 	addc	a,r6
   0C9B A3                 3671 	inc	dptr
   0C9C F0                 3672 	movx	@dptr,a
                           3673 ;	main.c:350: *ptr='\0';
                           3674 ;	genPointerSet
                           3675 ;	genGenPointerSet
   0C9D 8A 82              3676 	mov	dpl,r2
   0C9F 8B 83              3677 	mov	dph,r3
   0CA1 8C F0              3678 	mov	b,r4
                           3679 ;	Peephole 181	changed mov to clr
   0CA3 E4                 3680 	clr	a
   0CA4 12 10 41           3681 	lcall	__gptrput
                           3682 ;	main.c:351: if (count>63)
                           3683 ;	genAssign
   0CA7 90 34 2F           3684 	mov	dptr,#_putstrbuff_count_1_1
   0CAA E0                 3685 	movx	a,@dptr
   0CAB FD                 3686 	mov	r5,a
   0CAC A3                 3687 	inc	dptr
   0CAD E0                 3688 	movx	a,@dptr
   0CAE FE                 3689 	mov	r6,a
                           3690 ;	genCmpGt
                           3691 ;	genCmp
   0CAF C3                 3692 	clr	c
   0CB0 74 3F              3693 	mov	a,#0x3F
   0CB2 9D                 3694 	subb	a,r5
                           3695 ;	Peephole 181	changed mov to clr
   0CB3 E4                 3696 	clr	a
   0CB4 9E                 3697 	subb	a,r6
                           3698 ;	genIfxJump
                           3699 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0CB5 50 22              3700 	jnc	00102$
                           3701 ;	Peephole 300	removed redundant label 00113$
                           3702 ;	main.c:352: {count=0;
                           3703 ;	genAssign
   0CB7 90 34 2F           3704 	mov	dptr,#_putstrbuff_count_1_1
   0CBA E4                 3705 	clr	a
   0CBB F0                 3706 	movx	@dptr,a
   0CBC A3                 3707 	inc	dptr
   0CBD F0                 3708 	movx	@dptr,a
                           3709 ;	main.c:353: printf_tiny("\n\r");}
                           3710 ;	genIpush
   0CBE C0 02              3711 	push	ar2
   0CC0 C0 03              3712 	push	ar3
   0CC2 C0 04              3713 	push	ar4
   0CC4 74 D2              3714 	mov	a,#__str_1
   0CC6 C0 E0              3715 	push	acc
   0CC8 74 12              3716 	mov	a,#(__str_1 >> 8)
   0CCA C0 E0              3717 	push	acc
                           3718 ;	genCall
   0CCC 12 10 5A           3719 	lcall	_printf_tiny
   0CCF 15 81              3720 	dec	sp
   0CD1 15 81              3721 	dec	sp
   0CD3 D0 04              3722 	pop	ar4
   0CD5 D0 03              3723 	pop	ar3
   0CD7 D0 02              3724 	pop	ar2
   0CD9                    3725 00102$:
                           3726 ;	main.c:354: ptr++;
                           3727 ;	genPlus
                           3728 ;     genPlusIncr
   0CD9 0A                 3729 	inc	r2
   0CDA BA 00 01           3730 	cjne	r2,#0x00,00114$
   0CDD 0B                 3731 	inc	r3
   0CDE                    3732 00114$:
                           3733 ;	genAssign
   0CDE 90 34 2C           3734 	mov	dptr,#_putstrbuff_ptr_1_1
   0CE1 EA                 3735 	mov	a,r2
   0CE2 F0                 3736 	movx	@dptr,a
   0CE3 A3                 3737 	inc	dptr
   0CE4 EB                 3738 	mov	a,r3
   0CE5 F0                 3739 	movx	@dptr,a
   0CE6 A3                 3740 	inc	dptr
   0CE7 EC                 3741 	mov	a,r4
   0CE8 F0                 3742 	movx	@dptr,a
   0CE9 02 0C 5B           3743 	ljmp	00103$
   0CEC                    3744 00111$:
                           3745 ;	genAssign
   0CEC 90 34 2C           3746 	mov	dptr,#_putstrbuff_ptr_1_1
   0CEF EA                 3747 	mov	a,r2
   0CF0 F0                 3748 	movx	@dptr,a
   0CF1 A3                 3749 	inc	dptr
   0CF2 EB                 3750 	mov	a,r3
   0CF3 F0                 3751 	movx	@dptr,a
   0CF4 A3                 3752 	inc	dptr
   0CF5 EC                 3753 	mov	a,r4
   0CF6 F0                 3754 	movx	@dptr,a
                           3755 ;	Peephole 300	removed redundant label 00106$
   0CF7 22                 3756 	ret
                           3757 ;------------------------------------------------------------
                           3758 ;Allocation info for local variables in function 'putstr'
                           3759 ;------------------------------------------------------------
                           3760 ;ptr                       Allocated with name '_putstr_ptr_1_1'
                           3761 ;------------------------------------------------------------
                           3762 ;	main.c:359: void putstr(char *ptr)
                           3763 ;	-----------------------------------------
                           3764 ;	 function putstr
                           3765 ;	-----------------------------------------
   0CF8                    3766 _putstr:
                           3767 ;	genReceive
   0CF8 AA F0              3768 	mov	r2,b
   0CFA AB 83              3769 	mov	r3,dph
   0CFC E5 82              3770 	mov	a,dpl
   0CFE 90 34 31           3771 	mov	dptr,#_putstr_ptr_1_1
   0D01 F0                 3772 	movx	@dptr,a
   0D02 A3                 3773 	inc	dptr
   0D03 EB                 3774 	mov	a,r3
   0D04 F0                 3775 	movx	@dptr,a
   0D05 A3                 3776 	inc	dptr
   0D06 EA                 3777 	mov	a,r2
   0D07 F0                 3778 	movx	@dptr,a
                           3779 ;	main.c:361: while(*ptr)
                           3780 ;	genAssign
   0D08 90 34 31           3781 	mov	dptr,#_putstr_ptr_1_1
   0D0B E0                 3782 	movx	a,@dptr
   0D0C FA                 3783 	mov	r2,a
   0D0D A3                 3784 	inc	dptr
   0D0E E0                 3785 	movx	a,@dptr
   0D0F FB                 3786 	mov	r3,a
   0D10 A3                 3787 	inc	dptr
   0D11 E0                 3788 	movx	a,@dptr
   0D12 FC                 3789 	mov	r4,a
   0D13                    3790 00101$:
                           3791 ;	genPointerGet
                           3792 ;	genGenPointerGet
   0D13 8A 82              3793 	mov	dpl,r2
   0D15 8B 83              3794 	mov	dph,r3
   0D17 8C F0              3795 	mov	b,r4
   0D19 12 12 9E           3796 	lcall	__gptrget
                           3797 ;	genIfx
   0D1C FD                 3798 	mov	r5,a
                           3799 ;	Peephole 105	removed redundant mov
                           3800 ;	genIfxJump
                           3801 ;	Peephole 108.c	removed ljmp by inverse jump logic
   0D1D 60 2A              3802 	jz	00108$
                           3803 ;	Peephole 300	removed redundant label 00109$
                           3804 ;	main.c:363: putchar(*ptr);
                           3805 ;	genCast
   0D1F ED                 3806 	mov	a,r5
   0D20 33                 3807 	rlc	a
   0D21 95 E0              3808 	subb	a,acc
   0D23 FE                 3809 	mov	r6,a
                           3810 ;	genCall
   0D24 8D 82              3811 	mov	dpl,r5
   0D26 8E 83              3812 	mov	dph,r6
   0D28 C0 02              3813 	push	ar2
   0D2A C0 03              3814 	push	ar3
   0D2C C0 04              3815 	push	ar4
   0D2E 12 0D 73           3816 	lcall	_putchar
   0D31 D0 04              3817 	pop	ar4
   0D33 D0 03              3818 	pop	ar3
   0D35 D0 02              3819 	pop	ar2
                           3820 ;	main.c:364: ptr++;
                           3821 ;	genPlus
                           3822 ;     genPlusIncr
   0D37 0A                 3823 	inc	r2
   0D38 BA 00 01           3824 	cjne	r2,#0x00,00110$
   0D3B 0B                 3825 	inc	r3
   0D3C                    3826 00110$:
                           3827 ;	genAssign
   0D3C 90 34 31           3828 	mov	dptr,#_putstr_ptr_1_1
   0D3F EA                 3829 	mov	a,r2
   0D40 F0                 3830 	movx	@dptr,a
   0D41 A3                 3831 	inc	dptr
   0D42 EB                 3832 	mov	a,r3
   0D43 F0                 3833 	movx	@dptr,a
   0D44 A3                 3834 	inc	dptr
   0D45 EC                 3835 	mov	a,r4
   0D46 F0                 3836 	movx	@dptr,a
                           3837 ;	Peephole 112.b	changed ljmp to sjmp
   0D47 80 CA              3838 	sjmp	00101$
   0D49                    3839 00108$:
                           3840 ;	genAssign
   0D49 90 34 31           3841 	mov	dptr,#_putstr_ptr_1_1
   0D4C EA                 3842 	mov	a,r2
   0D4D F0                 3843 	movx	@dptr,a
   0D4E A3                 3844 	inc	dptr
   0D4F EB                 3845 	mov	a,r3
   0D50 F0                 3846 	movx	@dptr,a
   0D51 A3                 3847 	inc	dptr
   0D52 EC                 3848 	mov	a,r4
   0D53 F0                 3849 	movx	@dptr,a
                           3850 ;	Peephole 300	removed redundant label 00104$
   0D54 22                 3851 	ret
                           3852 ;------------------------------------------------------------
                           3853 ;Allocation info for local variables in function 'SerialInitialize'
                           3854 ;------------------------------------------------------------
                           3855 ;------------------------------------------------------------
                           3856 ;	main.c:369: void SerialInitialize()
                           3857 ;	-----------------------------------------
                           3858 ;	 function SerialInitialize
                           3859 ;	-----------------------------------------
   0D55                    3860 _SerialInitialize:
                           3861 ;	main.c:371: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
                           3862 ;	genAssign
   0D55 75 89 20           3863 	mov	_TMOD,#0x20
                           3864 ;	main.c:372: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
                           3865 ;	genAssign
   0D58 75 8D FD           3866 	mov	_TH1,#0xFD
                           3867 ;	main.c:373: SCON=0x50;
                           3868 ;	genAssign
   0D5B 75 98 50           3869 	mov	_SCON,#0x50
                           3870 ;	main.c:374: TR1=1;  // to start timer
                           3871 ;	genAssign
   0D5E D2 8E              3872 	setb	_TR1
                           3873 ;	main.c:375: IE=0x90; // to make serial interrupt interrupt enable
                           3874 ;	genAssign
   0D60 75 A8 90           3875 	mov	_IE,#0x90
                           3876 ;	Peephole 300	removed redundant label 00101$
   0D63 22                 3877 	ret
                           3878 ;------------------------------------------------------------
                           3879 ;Allocation info for local variables in function 'getchar'
                           3880 ;------------------------------------------------------------
                           3881 ;------------------------------------------------------------
                           3882 ;	main.c:378: char getchar()
                           3883 ;	-----------------------------------------
                           3884 ;	 function getchar
                           3885 ;	-----------------------------------------
   0D64                    3886 _getchar:
                           3887 ;	main.c:381: while(RI==0);  // wait untill hole 8 bit data is received completely. RI bit become 1 when reception is compleated
   0D64                    3888 00101$:
                           3889 ;	genIfx
                           3890 ;	genIfxJump
                           3891 ;	Peephole 108.d	removed ljmp by inverse jump logic
   0D64 30 98 FD           3892 	jnb	_RI,00101$
                           3893 ;	Peephole 300	removed redundant label 00108$
                           3894 ;	main.c:382: rec=SBUF;   // data is received in SBUF register present in controller during receiving
                           3895 ;	genAssign
   0D67 AA 99              3896 	mov	r2,_SBUF
                           3897 ;	genAssign
   0D69 90 34 10           3898 	mov	dptr,#_rec
   0D6C EA                 3899 	mov	a,r2
   0D6D F0                 3900 	movx	@dptr,a
                           3901 ;	main.c:383: RI=0;  // make RI bit to 0 so that next data is received
                           3902 ;	genAssign
   0D6E C2 98              3903 	clr	_RI
                           3904 ;	main.c:384: return rec;  // return rec where funtion is called
                           3905 ;	genRet
   0D70 8A 82              3906 	mov	dpl,r2
                           3907 ;	Peephole 300	removed redundant label 00104$
   0D72 22                 3908 	ret
                           3909 ;------------------------------------------------------------
                           3910 ;Allocation info for local variables in function 'putchar'
                           3911 ;------------------------------------------------------------
                           3912 ;x                         Allocated with name '_putchar_x_1_1'
                           3913 ;------------------------------------------------------------
                           3914 ;	main.c:387: void putchar(int x)
                           3915 ;	-----------------------------------------
                           3916 ;	 function putchar
                           3917 ;	-----------------------------------------
   0D73                    3918 _putchar:
                           3919 ;	genReceive
   0D73 AA 83              3920 	mov	r2,dph
   0D75 E5 82              3921 	mov	a,dpl
   0D77 90 34 34           3922 	mov	dptr,#_putchar_x_1_1
   0D7A F0                 3923 	movx	@dptr,a
   0D7B A3                 3924 	inc	dptr
   0D7C EA                 3925 	mov	a,r2
   0D7D F0                 3926 	movx	@dptr,a
                           3927 ;	main.c:389: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
                           3928 ;	genAssign
   0D7E 90 34 34           3929 	mov	dptr,#_putchar_x_1_1
   0D81 E0                 3930 	movx	a,@dptr
   0D82 FA                 3931 	mov	r2,a
   0D83 A3                 3932 	inc	dptr
   0D84 E0                 3933 	movx	a,@dptr
   0D85 FB                 3934 	mov	r3,a
                           3935 ;	genCast
   0D86 8A 99              3936 	mov	_SBUF,r2
                           3937 ;	main.c:390: while(TI==0); // wait untill transmission is compleated. when transmittion is compleated TI bit become 1
   0D88                    3938 00101$:
                           3939 ;	genIfx
                           3940 ;	genIfxJump
                           3941 ;	Peephole 108.d	removed ljmp by inverse jump logic
                           3942 ;	main.c:391: TI=0; // make TI bit to zero for next transmittion
                           3943 ;	genAssign
                           3944 ;	Peephole 250.a	using atomic test and clear
   0D88 10 99 02           3945 	jbc	_TI,00108$
   0D8B 80 FB              3946 	sjmp	00101$
   0D8D                    3947 00108$:
                           3948 ;	Peephole 300	removed redundant label 00104$
   0D8D 22                 3949 	ret
                           3950 ;------------------------------------------------------------
                           3951 ;Allocation info for local variables in function 'delay'
                           3952 ;------------------------------------------------------------
                           3953 ;x                         Allocated with name '_delay_x_1_1'
                           3954 ;i                         Allocated with name '_delay_i_1_1'
                           3955 ;------------------------------------------------------------
                           3956 ;	main.c:394: void delay(int x)
                           3957 ;	-----------------------------------------
                           3958 ;	 function delay
                           3959 ;	-----------------------------------------
   0D8E                    3960 _delay:
                           3961 ;	genReceive
   0D8E AA 83              3962 	mov	r2,dph
   0D90 E5 82              3963 	mov	a,dpl
   0D92 90 34 36           3964 	mov	dptr,#_delay_x_1_1
   0D95 F0                 3965 	movx	@dptr,a
   0D96 A3                 3966 	inc	dptr
   0D97 EA                 3967 	mov	a,r2
   0D98 F0                 3968 	movx	@dptr,a
                           3969 ;	main.c:397: for(i=0;i<x;i++);
                           3970 ;	genAssign
   0D99 90 34 36           3971 	mov	dptr,#_delay_x_1_1
   0D9C E0                 3972 	movx	a,@dptr
   0D9D FA                 3973 	mov	r2,a
   0D9E A3                 3974 	inc	dptr
   0D9F E0                 3975 	movx	a,@dptr
   0DA0 FB                 3976 	mov	r3,a
                           3977 ;	genAssign
   0DA1 7C 00              3978 	mov	r4,#0x00
   0DA3 7D 00              3979 	mov	r5,#0x00
   0DA5                    3980 00101$:
                           3981 ;	genCmpLt
                           3982 ;	genCmp
   0DA5 C3                 3983 	clr	c
   0DA6 EC                 3984 	mov	a,r4
   0DA7 9A                 3985 	subb	a,r2
   0DA8 ED                 3986 	mov	a,r5
   0DA9 64 80              3987 	xrl	a,#0x80
   0DAB 8B F0              3988 	mov	b,r3
   0DAD 63 F0 80           3989 	xrl	b,#0x80
   0DB0 95 F0              3990 	subb	a,b
                           3991 ;	genIfxJump
                           3992 ;	Peephole 108.a	removed ljmp by inverse jump logic
   0DB2 50 07              3993 	jnc	00105$
                           3994 ;	Peephole 300	removed redundant label 00110$
                           3995 ;	genPlus
                           3996 ;     genPlusIncr
                           3997 ;	tail increment optimized (range 3)
   0DB4 0C                 3998 	inc	r4
   0DB5 BC 00 ED           3999 	cjne	r4,#0x00,00101$
   0DB8 0D                 4000 	inc	r5
                           4001 ;	Peephole 112.b	changed ljmp to sjmp
   0DB9 80 EA              4002 	sjmp	00101$
   0DBB                    4003 00105$:
   0DBB 22                 4004 	ret
                           4005 	.area CSEG    (CODE)
                           4006 	.area CONST   (CODE)
   12BA                    4007 __str_0:
   12BA 0A                 4008 	.db 0x0A
   12BB 0D                 4009 	.db 0x0D
   12BC 45 6E 74 65 72 20  4010 	.ascii "Enter the character"
        74 68 65 20 63 68
        61 72 61 63 74 65
        72
   12CF 0A                 4011 	.db 0x0A
   12D0 0D                 4012 	.db 0x0D
   12D1 00                 4013 	.db 0x00
   12D2                    4014 __str_1:
   12D2 0A                 4015 	.db 0x0A
   12D3 0D                 4016 	.db 0x0D
   12D4 00                 4017 	.db 0x00
   12D5                    4018 __str_2:
   12D5 49 6E 70 75 74 20  4019 	.ascii "Input buffer size between 32 and 2800 bytes, divisible by 16"
        62 75 66 66 65 72
        20 73 69 7A 65 20
        62 65 74 77 65 65
        6E 20 33 32 20 61
        6E 64 20 32 38 30
        30 20 62 79 74 65
        73 2C 20 64 69 76
        69 73 69 62 6C 65
        20 62 79 20 31 36
   1311 0A                 4020 	.db 0x0A
   1312 0D                 4021 	.db 0x0D
   1313 00                 4022 	.db 0x00
   1314                    4023 __str_3:
   1314 6D 61 6C 6C 6F 63  4024 	.ascii "malloc buffer0 failed"
        20 62 75 66 66 65
        72 30 20 66 61 69
        6C 65 64
   1329 0A                 4025 	.db 0x0A
   132A 0D                 4026 	.db 0x0D
   132B 00                 4027 	.db 0x00
   132C                    4028 __str_4:
   132C 6D 61 6C 6C 6F 63  4029 	.ascii "malloc buffer1 failed"
        20 62 75 66 66 65
        72 31 20 66 61 69
        6C 65 64
   1341 0A                 4030 	.db 0x0A
   1342 0D                 4031 	.db 0x0D
   1343 00                 4032 	.db 0x00
   1344                    4033 __str_5:
   1344 45 6E 74 65 72 20  4034 	.ascii "Enter the correct buffer size value divisible by 16"
        74 68 65 20 63 6F
        72 72 65 63 74 20
        62 75 66 66 65 72
        20 73 69 7A 65 20
        76 61 6C 75 65 20
        64 69 76 69 73 69
        62 6C 65 20 62 79
        20 31 36
   1377 0A                 4035 	.db 0x0A
   1378 0D                 4036 	.db 0x0D
   1379 00                 4037 	.db 0x00
   137A                    4038 __str_6:
   137A 45 6E 74 65 72 20  4039 	.ascii "Enter the correct buffer size between 32 and 2800"
        74 68 65 20 63 6F
        72 72 65 63 74 20
        62 75 66 66 65 72
        20 73 69 7A 65 20
        62 65 74 77 65 65
        6E 20 33 32 20 61
        6E 64 20 32 38 30
        30
   13AB 0A                 4040 	.db 0x0A
   13AC 0D                 4041 	.db 0x0D
   13AD 00                 4042 	.db 0x00
   13AE                    4043 __str_7:
   13AE 42 75 66 66 65 72  4044 	.ascii "Buffer allocation successful"
        20 61 6C 6C 6F 63
        61 74 69 6F 6E 20
        73 75 63 63 65 73
        73 66 75 6C
   13CA 0A                 4045 	.db 0x0A
   13CB 0D                 4046 	.db 0x0D
   13CC 00                 4047 	.db 0x00
   13CD                    4048 __str_8:
   13CD 6C 65 6E 31 20 3D  4049 	.ascii "len1 = %d"
        20 25 64
   13D6 0A                 4050 	.db 0x0A
   13D7 0D                 4051 	.db 0x0D
   13D8 00                 4052 	.db 0x00
   13D9                    4053 __str_9:
   13D9 6C 65 6E 32 20 3D  4054 	.ascii "len2 = %d"
        20 25 64
   13E2 0A                 4055 	.db 0x0A
   13E3 0D                 4056 	.db 0x0D
   13E4 00                 4057 	.db 0x00
   13E5                    4058 __str_10:
   13E5 42 75 66 66 65 72  4059 	.ascii "Buffer0 address 0x%x"
        30 20 61 64 64 72
        65 73 73 20 30 78
        25 78
   13F9 0A                 4060 	.db 0x0A
   13FA 0D                 4061 	.db 0x0D
   13FB 00                 4062 	.db 0x00
   13FC                    4063 __str_11:
   13FC 42 75 66 66 65 72  4064 	.ascii "Buffer1 address 0x%x"
        31 20 61 64 64 72
        65 73 73 20 30 78
        25 78
   1410 0A                 4065 	.db 0x0A
   1411 0D                 4066 	.db 0x0D
   1412 00                 4067 	.db 0x00
   1413                    4068 __str_12:
   1413 45 6E 74 65 72 20  4069 	.ascii "Enter the buffer size between 20 and 400"
        74 68 65 20 62 75
        66 66 65 72 20 73
        69 7A 65 20 62 65
        74 77 65 65 6E 20
        32 30 20 61 6E 64
        20 34 30 30
   143B 0A                 4070 	.db 0x0A
   143C 0D                 4071 	.db 0x0D
   143D 00                 4072 	.db 0x00
   143E                    4073 __str_13:
   143E 0A                 4074 	.db 0x0A
   143F 0D                 4075 	.db 0x0D
   1440 42 75 66 66 65 72  4076 	.ascii "Buffer allocation failed"
        20 61 6C 6C 6F 63
        61 74 69 6F 6E 20
        66 61 69 6C 65 64
   1458 0A                 4077 	.db 0x0A
   1459 0D                 4078 	.db 0x0D
   145A 00                 4079 	.db 0x00
   145B                    4080 __str_14:
   145B 42 75 66 66 65 72  4081 	.ascii "Buffer Allocated"
        20 41 6C 6C 6F 63
        61 74 65 64
   146B 0A                 4082 	.db 0x0A
   146C 0D                 4083 	.db 0x0D
   146D 00                 4084 	.db 0x00
   146E                    4085 __str_15:
   146E 42 75 66 66 65 72  4086 	.ascii "Buffer address and buffer number 0x%x, %d"
        20 61 64 64 72 65
        73 73 20 61 6E 64
        20 62 75 66 66 65
        72 20 6E 75 6D 62
        65 72 20 30 78 25
        78 2C 20 25 64
   1497 0A                 4087 	.db 0x0A
   1498 0D                 4088 	.db 0x0D
   1499 00                 4089 	.db 0x00
   149A                    4090 __str_16:
   149A 45 6E 74 65 72 20  4091 	.ascii "Enter valid buffer size"
        76 61 6C 69 64 20
        62 75 66 66 65 72
        20 73 69 7A 65
   14B1 0A                 4092 	.db 0x0A
   14B2 0D                 4093 	.db 0x0D
   14B3 00                 4094 	.db 0x00
   14B4                    4095 __str_17:
   14B4 45 6E 74 65 72 20  4096 	.ascii "Enter the buffer number which has to be deleted"
        74 68 65 20 62 75
        66 66 65 72 20 6E
        75 6D 62 65 72 20
        77 68 69 63 68 20
        68 61 73 20 74 6F
        20 62 65 20 64 65
        6C 65 74 65 64
   14E3 0A                 4097 	.db 0x0A
   14E4 0D                 4098 	.db 0x0D
   14E5 00                 4099 	.db 0x00
   14E6                    4100 __str_18:
   14E6 45 6E 74 65 72 20  4101 	.ascii "Enter valid buffer number next time"
        76 61 6C 69 64 20
        62 75 66 66 65 72
        20 6E 75 6D 62 65
        72 20 6E 65 78 74
        20 74 69 6D 65
   1509 0A                 4102 	.db 0x0A
   150A 0D                 4103 	.db 0x0D
   150B 00                 4104 	.db 0x00
   150C                    4105 __str_19:
   150C 42 75 66 66 65 72  4106 	.ascii "Buffer deleted"
        20 64 65 6C 65 74
        65 64
   151A 0A                 4107 	.db 0x0A
   151B 0D                 4108 	.db 0x0D
   151C 00                 4109 	.db 0x00
   151D                    4110 __str_20:
   151D 46 72 65 65 69 6E  4111 	.ascii "Freeing %d buffer"
        67 20 25 64 20 62
        75 66 66 65 72
   152E 0A                 4112 	.db 0x0A
   152F 0D                 4113 	.db 0x0D
   1530 00                 4114 	.db 0x00
   1531                    4115 __str_21:
   1531 2D 2D 2D 2D 2D 2D  4116 	.ascii "------------------------------------------------------------"
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
   156D 2D 2D 2D 2D 2D 2D  4117 	.ascii "---------------------------"
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D
   1588 0A                 4118 	.db 0x0A
   1589 0D                 4119 	.db 0x0D
   158A 00                 4120 	.db 0x00
   158B                    4121 __str_22:
   158B 0A                 4122 	.db 0x0A
   158C 0D                 4123 	.db 0x0D
   158D 72 54 6F 74 61 6C  4124 	.ascii "rTotal Number of Buffer = %d"
        20 4E 75 6D 62 65
        72 20 6F 66 20 42
        75 66 66 65 72 20
        3D 20 25 64
   15A9 0A                 4125 	.db 0x0A
   15AA 0D                 4126 	.db 0x0D
   15AB 00                 4127 	.db 0x00
   15AC                    4128 __str_23:
   15AC 0A                 4129 	.db 0x0A
   15AD 0D                 4130 	.db 0x0D
   15AE 54 6F 74 61 6C 20  4131 	.ascii "Total number of characters received = %d"
        6E 75 6D 62 65 72
        20 6F 66 20 63 68
        61 72 61 63 74 65
        72 73 20 72 65 63
        65 69 76 65 64 20
        3D 20 25 64
   15D6 0A                 4132 	.db 0x0A
   15D7 0D                 4133 	.db 0x0D
   15D8 00                 4134 	.db 0x00
   15D9                    4135 __str_24:
   15D9 54 6F 74 61 6C 20  4136 	.ascii "Total number of storage characters received = %d"
        6E 75 6D 62 65 72
        20 6F 66 20 73 74
        6F 72 61 67 65 20
        63 68 61 72 61 63
        74 65 72 73 20 72
        65 63 65 69 76 65
        64 20 3D 20 25 64
   1609 0A                 4137 	.db 0x0A
   160A 0D                 4138 	.db 0x0D
   160B 00                 4139 	.db 0x00
   160C                    4140 __str_25:
   160C 54 6F 74 61 6C 20  4141 	.ascii "Total number of characters received since last '?' = %d"
        6E 75 6D 62 65 72
        20 6F 66 20 63 68
        61 72 61 63 74 65
        72 73 20 72 65 63
        65 69 76 65 64 20
        73 69 6E 63 65 20
        6C 61 73 74 20 27
        3F 27 20 3D 20 25
        64
   1643 0A                 4142 	.db 0x0A
   1644 0D                 4143 	.db 0x0D
   1645 00                 4144 	.db 0x00
   1646                    4145 __str_26:
   1646 0A                 4146 	.db 0x0A
   1647 0D                 4147 	.db 0x0D
   1648 2D 2D 2D 2D 2D 2D  4148 	.ascii "----------------------------------------------------------"
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D
   1682 2D 2D 2D 2D 2D 2D  4149 	.ascii "-----------------------------"
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D 2D
        2D 2D 2D 2D 2D
   169F 0A                 4150 	.db 0x0A
   16A0 0D                 4151 	.db 0x0D
   16A1 00                 4152 	.db 0x00
   16A2                    4153 __str_27:
   16A2 0A                 4154 	.db 0x0A
   16A3 0D                 4155 	.db 0x0D
   16A4 4E 75 6D 62 65 72  4156 	.ascii "Number of characters in buffer0 = %d"
        20 6F 66 20 63 68
        61 72 61 63 74 65
        72 73 20 69 6E 20
        62 75 66 66 65 72
        30 20 3D 20 25 64
   16C8 0A                 4157 	.db 0x0A
   16C9 0D                 4158 	.db 0x0D
   16CA 00                 4159 	.db 0x00
   16CB                    4160 __str_28:
   16CB 4E 75 6D 62 65 72  4161 	.ascii "Number of characters in buffer1 = %d"
        20 6F 66 20 63 68
        61 72 61 63 74 65
        72 73 20 69 6E 20
        62 75 66 66 65 72
        31 20 3D 20 25 64
   16EF 0A                 4162 	.db 0x0A
   16F0 0D                 4163 	.db 0x0D
   16F1 00                 4164 	.db 0x00
   16F2                    4165 __str_29:
   16F2 42 75 66 66 65 72  4166 	.ascii "Buffer %d Information"
        20 25 64 20 49 6E
        66 6F 72 6D 61 74
        69 6F 6E
   1707 0A                 4167 	.db 0x0A
   1708 0D                 4168 	.db 0x0D
   1709 00                 4169 	.db 0x00
   170A                    4170 __str_30:
   170A 53 74 61 72 74 69  4171 	.ascii "Starting address = 0x%x"
        6E 67 20 61 64 64
        72 65 73 73 20 3D
        20 30 78 25 78
   1721 0A                 4172 	.db 0x0A
   1722 0D                 4173 	.db 0x0D
   1723 00                 4174 	.db 0x00
   1724                    4175 __str_31:
   1724 45 6E 64 69 6E 67  4176 	.ascii "Ending address = 0x%x"
        20 61 64 64 72 65
        73 73 20 3D 20 30
        78 25 78
   1739 0A                 4177 	.db 0x0A
   173A 0D                 4178 	.db 0x0D
   173B 00                 4179 	.db 0x00
   173C                    4180 __str_32:
   173C 41 6C 6C 6F 63 61  4181 	.ascii "Allocated size = %d"
        74 65 64 20 73 69
        7A 65 20 3D 20 25
        64
   174F 0A                 4182 	.db 0x0A
   1750 0D                 4183 	.db 0x0D
   1751 00                 4184 	.db 0x00
   1752                    4185 __str_33:
   1752 0A                 4186 	.db 0x0A
   1753 0D                 4187 	.db 0x0D
   1754 43 6F 6E 74 65 6E  4188 	.ascii "Contents of Buffer0"
        74 73 20 6F 66 20
        42 75 66 66 65 72
        30
   1767 0A                 4189 	.db 0x0A
   1768 0D                 4190 	.db 0x0D
   1769 00                 4191 	.db 0x00
   176A                    4192 __str_34:
   176A 0A                 4193 	.db 0x0A
   176B 0D                 4194 	.db 0x0D
   176C 43 6F 6E 74 65 6E  4195 	.ascii "Contents of Buffer1"
        74 73 20 6F 66 20
        42 75 66 66 65 72
        31
   177F 0A                 4196 	.db 0x0A
   1780 0D                 4197 	.db 0x0D
   1781 00                 4198 	.db 0x00
   1782                    4199 __str_35:
   1782 43 6F 6E 74 65 6E  4200 	.ascii "Contents of Buffer1"
        74 73 20 6F 66 20
        42 75 66 66 65 72
        31
   1795 0A                 4201 	.db 0x0A
   1796 0D                 4202 	.db 0x0D
   1797 00                 4203 	.db 0x00
   1798                    4204 __str_36:
   1798 30 78 25 78 3A 20  4205 	.ascii "0x%x:  "
        20
   179F 00                 4206 	.db 0x00
   17A0                    4207 __str_37:
   17A0 25 78 20           4208 	.ascii "%x "
   17A3 00                 4209 	.db 0x00
                           4210 	.area XINIT   (CODE)
   17A4                    4211 __xinit__i:
   17A4 00                 4212 	.db #0x00
