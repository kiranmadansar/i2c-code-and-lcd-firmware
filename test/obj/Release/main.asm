;--------------------------------------------------------
; File Created by SDCC : FreeWare ANSI-C Compiler
; Version 2.6.0 #4309 (Jul 28 2006)
; This file generated Fri Oct 20 23:58:12 2017
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl __sdcc_external_startup
	.globl _CY
	.globl _AC
	.globl _F0
	.globl _RS1
	.globl _RS0
	.globl _OV
	.globl _F1
	.globl _P
	.globl _PS
	.globl _PT1
	.globl _PX1
	.globl _PT0
	.globl _PX0
	.globl _RD
	.globl _WR
	.globl _T1
	.globl _T0
	.globl _INT1
	.globl _INT0
	.globl _TXD
	.globl _RXD
	.globl _P3_7
	.globl _P3_6
	.globl _P3_5
	.globl _P3_4
	.globl _P3_3
	.globl _P3_2
	.globl _P3_1
	.globl _P3_0
	.globl _EA
	.globl _ES
	.globl _ET1
	.globl _EX1
	.globl _ET0
	.globl _EX0
	.globl _P2_7
	.globl _P2_6
	.globl _P2_5
	.globl _P2_4
	.globl _P2_3
	.globl _P2_2
	.globl _P2_1
	.globl _P2_0
	.globl _SM0
	.globl _SM1
	.globl _SM2
	.globl _REN
	.globl _TB8
	.globl _RB8
	.globl _TI
	.globl _RI
	.globl _P1_7
	.globl _P1_6
	.globl _P1_5
	.globl _P1_4
	.globl _P1_3
	.globl _P1_2
	.globl _P1_1
	.globl _P1_0
	.globl _TF1
	.globl _TR1
	.globl _TF0
	.globl _TR0
	.globl _IE1
	.globl _IT1
	.globl _IE0
	.globl _IT0
	.globl _P0_7
	.globl _P0_6
	.globl _P0_5
	.globl _P0_4
	.globl _P0_3
	.globl _P0_2
	.globl _P0_1
	.globl _P0_0
	.globl _TXD0
	.globl _RXD0
	.globl _BREG_F7
	.globl _BREG_F6
	.globl _BREG_F5
	.globl _BREG_F4
	.globl _BREG_F3
	.globl _BREG_F2
	.globl _BREG_F1
	.globl _BREG_F0
	.globl _P5_7
	.globl _P5_6
	.globl _P5_5
	.globl _P5_4
	.globl _P5_3
	.globl _P5_2
	.globl _P5_1
	.globl _P5_0
	.globl _P4_7
	.globl _P4_6
	.globl _P4_5
	.globl _P4_4
	.globl _P4_3
	.globl _P4_2
	.globl _P4_1
	.globl _P4_0
	.globl _PX0L
	.globl _PT0L
	.globl _PX1L
	.globl _PT1L
	.globl _PLS
	.globl _PT2L
	.globl _PPCL
	.globl _EC
	.globl _CCF0
	.globl _CCF1
	.globl _CCF2
	.globl _CCF3
	.globl _CCF4
	.globl _CR
	.globl _CF
	.globl _TF2
	.globl _EXF2
	.globl _RCLK
	.globl _TCLK
	.globl _EXEN2
	.globl _TR2
	.globl _C_T2
	.globl _CP_RL2
	.globl _T2CON_7
	.globl _T2CON_6
	.globl _T2CON_5
	.globl _T2CON_4
	.globl _T2CON_3
	.globl _T2CON_2
	.globl _T2CON_1
	.globl _T2CON_0
	.globl _PT2
	.globl _ET2
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _IP
	.globl _P3
	.globl _IE
	.globl _P2
	.globl _SBUF
	.globl _SCON
	.globl _P1
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _P0
	.globl _SBUF0
	.globl _DP0L
	.globl _DP0H
	.globl _EECON
	.globl _KBF
	.globl _KBE
	.globl _KBLS
	.globl _BRL
	.globl _BDRCON
	.globl _T2MOD
	.globl _SPDAT
	.globl _SPSTA
	.globl _SPCON
	.globl _SADEN
	.globl _SADDR
	.globl _WDTPRG
	.globl _WDTRST
	.globl _P5
	.globl _P4
	.globl _IPH1
	.globl _IPL1
	.globl _IPH0
	.globl _IPL0
	.globl _IEN1
	.globl _IEN0
	.globl _CMOD
	.globl _CL
	.globl _CH
	.globl _CCON
	.globl _CCAPM4
	.globl _CCAPM3
	.globl _CCAPM2
	.globl _CCAPM1
	.globl _CCAPM0
	.globl _CCAP4L
	.globl _CCAP3L
	.globl _CCAP2L
	.globl _CCAP1L
	.globl _CCAP0L
	.globl _CCAP4H
	.globl _CCAP3H
	.globl _CCAP2H
	.globl _CCAP1H
	.globl _CCAP0H
	.globl _CKCKON1
	.globl _CKCKON0
	.globl _CKRL
	.globl _AUXR1
	.globl _AUXR
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T2CON
	.globl _i
	.globl _bptr1
	.globl _bptr0
	.globl _res
	.globl _value
	.globl _ptr
	.globl _j
	.globl _buff_val
	.globl _temp
	.globl _rec
	.globl _bufflen
	.globl _buff2ptr
	.globl _buff1ptr
	.globl _buffer
	.globl _heap
	.globl _BufferInitialize
	.globl _ReadValue
	.globl _newbuffer
	.globl _deletebuffer
	.globl _fillbuffer0
	.globl _fillbuffer1
	.globl _freeEverything
	.globl _displaydelete
	.globl _display
	.globl _putstrhex
	.globl _putstrbuff
	.globl _putstr
	.globl _SerialInitialize
	.globl _getchar
	.globl _putchar
	.globl _delay
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (DATA)
_T2CON	=	0x00c8
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_TL2	=	0x00cc
_TH2	=	0x00cd
_AUXR	=	0x008e
_AUXR1	=	0x00a2
_CKRL	=	0x0097
_CKCKON0	=	0x008f
_CKCKON1	=	0x008f
_CCAP0H	=	0x00fa
_CCAP1H	=	0x00fb
_CCAP2H	=	0x00fc
_CCAP3H	=	0x00fd
_CCAP4H	=	0x00fe
_CCAP0L	=	0x00ea
_CCAP1L	=	0x00eb
_CCAP2L	=	0x00ec
_CCAP3L	=	0x00ed
_CCAP4L	=	0x00ee
_CCAPM0	=	0x00da
_CCAPM1	=	0x00db
_CCAPM2	=	0x00dc
_CCAPM3	=	0x00dd
_CCAPM4	=	0x00de
_CCON	=	0x00d8
_CH	=	0x00f9
_CL	=	0x00e9
_CMOD	=	0x00d9
_IEN0	=	0x00a8
_IEN1	=	0x00b1
_IPL0	=	0x00b8
_IPH0	=	0x00b7
_IPL1	=	0x00b2
_IPH1	=	0x00b3
_P4	=	0x00c0
_P5	=	0x00d8
_WDTRST	=	0x00a6
_WDTPRG	=	0x00a7
_SADDR	=	0x00a9
_SADEN	=	0x00b9
_SPCON	=	0x00c3
_SPSTA	=	0x00c4
_SPDAT	=	0x00c5
_T2MOD	=	0x00c9
_BDRCON	=	0x009b
_BRL	=	0x009a
_KBLS	=	0x009c
_KBE	=	0x009d
_KBF	=	0x009e
_EECON	=	0x00d2
_DP0H	=	0x0083
_DP0L	=	0x0082
_SBUF0	=	0x0099
_P0	=	0x0080
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_PCON	=	0x0087
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_P1	=	0x0090
_SCON	=	0x0098
_SBUF	=	0x0099
_P2	=	0x00a0
_IE	=	0x00a8
_P3	=	0x00b0
_IP	=	0x00b8
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (DATA)
_ET2	=	0x00ad
_PT2	=	0x00bd
_T2CON_0	=	0x00c8
_T2CON_1	=	0x00c9
_T2CON_2	=	0x00ca
_T2CON_3	=	0x00cb
_T2CON_4	=	0x00cc
_T2CON_5	=	0x00cd
_T2CON_6	=	0x00ce
_T2CON_7	=	0x00cf
_CP_RL2	=	0x00c8
_C_T2	=	0x00c9
_TR2	=	0x00ca
_EXEN2	=	0x00cb
_TCLK	=	0x00cc
_RCLK	=	0x00cd
_EXF2	=	0x00ce
_TF2	=	0x00cf
_CF	=	0x00df
_CR	=	0x00de
_CCF4	=	0x00dc
_CCF3	=	0x00db
_CCF2	=	0x00da
_CCF1	=	0x00d9
_CCF0	=	0x00d8
_EC	=	0x00ae
_PPCL	=	0x00be
_PT2L	=	0x00bd
_PLS	=	0x00bc
_PT1L	=	0x00bb
_PX1L	=	0x00ba
_PT0L	=	0x00b9
_PX0L	=	0x00b8
_P4_0	=	0x00c0
_P4_1	=	0x00c1
_P4_2	=	0x00c2
_P4_3	=	0x00c3
_P4_4	=	0x00c4
_P4_5	=	0x00c5
_P4_6	=	0x00c6
_P4_7	=	0x00c7
_P5_0	=	0x00d8
_P5_1	=	0x00d9
_P5_2	=	0x00da
_P5_3	=	0x00db
_P5_4	=	0x00dc
_P5_5	=	0x00dd
_P5_6	=	0x00de
_P5_7	=	0x00df
_BREG_F0	=	0x00f0
_BREG_F1	=	0x00f1
_BREG_F2	=	0x00f2
_BREG_F3	=	0x00f3
_BREG_F4	=	0x00f4
_BREG_F5	=	0x00f5
_BREG_F6	=	0x00f6
_BREG_F7	=	0x00f7
_RXD0	=	0x00b0
_TXD0	=	0x00b1
_P0_0	=	0x0080
_P0_1	=	0x0081
_P0_2	=	0x0082
_P0_3	=	0x0083
_P0_4	=	0x0084
_P0_5	=	0x0085
_P0_6	=	0x0086
_P0_7	=	0x0087
_IT0	=	0x0088
_IE0	=	0x0089
_IT1	=	0x008a
_IE1	=	0x008b
_TR0	=	0x008c
_TF0	=	0x008d
_TR1	=	0x008e
_TF1	=	0x008f
_P1_0	=	0x0090
_P1_1	=	0x0091
_P1_2	=	0x0092
_P1_3	=	0x0093
_P1_4	=	0x0094
_P1_5	=	0x0095
_P1_6	=	0x0096
_P1_7	=	0x0097
_RI	=	0x0098
_TI	=	0x0099
_RB8	=	0x009a
_TB8	=	0x009b
_REN	=	0x009c
_SM2	=	0x009d
_SM1	=	0x009e
_SM0	=	0x009f
_P2_0	=	0x00a0
_P2_1	=	0x00a1
_P2_2	=	0x00a2
_P2_3	=	0x00a3
_P2_4	=	0x00a4
_P2_5	=	0x00a5
_P2_6	=	0x00a6
_P2_7	=	0x00a7
_EX0	=	0x00a8
_ET0	=	0x00a9
_EX1	=	0x00aa
_ET1	=	0x00ab
_ES	=	0x00ac
_EA	=	0x00af
_P3_0	=	0x00b0
_P3_1	=	0x00b1
_P3_2	=	0x00b2
_P3_3	=	0x00b3
_P3_4	=	0x00b4
_P3_5	=	0x00b5
_P3_6	=	0x00b6
_P3_7	=	0x00b7
_RXD	=	0x00b0
_TXD	=	0x00b1
_INT0	=	0x00b2
_INT1	=	0x00b3
_T0	=	0x00b4
_T1	=	0x00b5
_WR	=	0x00b6
_RD	=	0x00b7
_PX0	=	0x00b8
_PT0	=	0x00b9
_PX1	=	0x00ba
_PT1	=	0x00bb
_PS	=	0x00bc
_P	=	0x00d0
_F1	=	0x00d1
_OV	=	0x00d2
_RS0	=	0x00d3
_RS1	=	0x00d4
_F0	=	0x00d5
_AC	=	0x00d6
_CY	=	0x00d7
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_heap::
	.ds 12288
_buffer_count:
	.ds 2
_stor_count:
	.ds 2
_char_count:
	.ds 2
_buffer_number:
	.ds 2
_lastchar_count:
	.ds 2
_buffer::
	.ds 512
_buff1ptr::
	.ds 3
_buff2ptr::
	.ds 3
_bufflen::
	.ds 512
_rec::
	.ds 1
_temp::
	.ds 1
_buff_val::
	.ds 1
_j::
	.ds 1
_ptr::
	.ds 3
_value::
	.ds 2
_res::
	.ds 2
_bptr0::
	.ds 3
_bptr1::
	.ds 3
_fillbuffer0_buff_val_1_1:
	.ds 1
_fillbuffer1_buff_val_1_1:
	.ds 1
_displaydelete_charnum0_1_1:
	.ds 2
_displaydelete_charnum1_1_1:
	.ds 2
_putstrhex_ptr_1_1:
	.ds 3
_putstrhex_count_1_1:
	.ds 2
_putstrbuff_ptr_1_1:
	.ds 3
_putstrbuff_count_1_1:
	.ds 2
_putstr_ptr_1_1:
	.ds 3
_putchar_x_1_1:
	.ds 2
_delay_x_1_1:
	.ds 2
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
_i::
	.ds 1
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area CSEG    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function '_sdcc_external_startup'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:21: _sdcc_external_startup()
;	-----------------------------------------
;	 function _sdcc_external_startup
;	-----------------------------------------
__sdcc_external_startup:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
;	main.c:23: AUXR |= 0x0C;
;	genOr
	orl	_AUXR,#0x0C
;	main.c:24: init_dynamic_memory((MEMHEADER xdata *)heap, HEAP_SIZE);
;	genAssign
	mov	dptr,#_init_dynamic_memory_PARM_2
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
	inc	dptr
	mov	a,#0x30
	movx	@dptr,a
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#_heap
	lcall	_init_dynamic_memory
;	main.c:25: return 0;
;	genRet
;	Peephole 182.b	used 16 bit load of dptr
	mov	dptr,#0x0000
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:51: void main()
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	main.c:53: lastchar_count=0;
;	genAssign
	mov	dptr,#_lastchar_count
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:54: char_count=0;
;	genAssign
	mov	dptr,#_char_count
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:55: stor_count=0;
;	genAssign
	mov	dptr,#_stor_count
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:56: buffer_count=2;
;	genAssign
	mov	dptr,#_buffer_count
	mov	a,#0x02
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	main.c:57: buffer_number=0;
;	genAssign
	mov	dptr,#_buffer_number
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:59: SerialInitialize();  //  call the initialize function
;	genCall
	lcall	_SerialInitialize
;	main.c:60: here:    BufferInitialize();
00101$:
;	genCall
	lcall	_BufferInitialize
;	main.c:62: while(1)
00128$:
;	main.c:64: putstr("\n\rEnter the character\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_0
	mov	b,#0x80
	lcall	_putstr
;	main.c:65: temp = getchar();
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	genAssign
	mov	dptr,#_temp
	mov	a,r2
	movx	@dptr,a
;	main.c:66: putchar(temp);
;	genAssign
;	genCast
	mov	r3,#0x00
;	genCall
	mov	dpl,r2
	mov	dph,r3
	lcall	_putchar
;	main.c:67: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	lcall	_putstr
;	main.c:68: if (temp>='A' && temp<='Z')
;	genAssign
	mov	dptr,#_temp
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x41,00143$
00143$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00124$
;	Peephole 300	removed redundant label 00144$
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r2
	add	a,#0xff - 0x5A
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00124$
;	Peephole 300	removed redundant label 00145$
;	main.c:70: stor_count++;
;	genAssign
	mov	dptr,#_stor_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_stor_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:71: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:72: fillbuffer0(temp);
;	genCall
	mov	dpl,r2
	lcall	_fillbuffer0
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00128$
00124$:
;	main.c:74: else if (temp>='0' && temp<='9')
;	genAssign
	mov	dptr,#_temp
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x30,00146$
00146$:
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00120$
;	Peephole 300	removed redundant label 00147$
;	genCmpGt
;	genCmp
;	genIfxJump
;	Peephole 132.b	optimized genCmpGt by inverse logic (acc differs)
	mov	a,r2
	add	a,#0xff - 0x39
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00120$
;	Peephole 300	removed redundant label 00148$
;	main.c:76: stor_count++;
;	genAssign
	mov	dptr,#_stor_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_stor_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:77: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:78: fillbuffer1(temp);
;	genCall
	mov	dpl,r2
	lcall	_fillbuffer1
	ljmp	00128$
00120$:
;	main.c:80: else if (temp=='+')
;	genAssign
	mov	dptr,#_temp
	movx	a,@dptr
	mov	r2,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x2B,00117$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00149$
;	Peephole 300	removed redundant label 00150$
;	main.c:82: newbuffer();
;	genCall
	lcall	_newbuffer
;	main.c:83: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
	ljmp	00128$
00117$:
;	main.c:85: else if (temp=='-')
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x2D,00114$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00151$
;	Peephole 300	removed redundant label 00152$
;	main.c:87: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:88: deletebuffer();
;	genCall
	lcall	_deletebuffer
	ljmp	00128$
00114$:
;	main.c:90: else if (temp=='?')
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x3F,00111$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00153$
;	Peephole 300	removed redundant label 00154$
;	main.c:92: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:93: displaydelete();
;	genCall
	lcall	_displaydelete
	ljmp	00128$
00111$:
;	main.c:95: else if(temp=='=')
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x3D,00108$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00155$
;	Peephole 300	removed redundant label 00156$
;	main.c:97: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:98: display();
;	genCall
	lcall	_display
	ljmp	00128$
00108$:
;	main.c:100: else if(temp=='@')
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.b	optimized misc jump sequence
	cjne	r2,#0x40,00105$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00157$
;	Peephole 300	removed redundant label 00158$
;	main.c:102: char_count++;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_char_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:103: freeEverything();
;	genCall
	lcall	_freeEverything
;	main.c:104: break;
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00129$
00105$:
;	main.c:106: else if (temp=='del')
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x64,00159$
	sjmp	00160$
00159$:
	ljmp	00128$
00160$:
;	main.c:107: {break;}
00129$:
;	main.c:115: buffer_count=2;
;	genAssign
	mov	dptr,#_buffer_count
	mov	a,#0x02
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	main.c:116: buffer_number=0;
;	genAssign
	mov	dptr,#_buffer_number
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:117: goto here;
	ljmp	00101$
;	Peephole 259.b	removed redundant label 00130$ and ret
;
;------------------------------------------------------------
;Allocation info for local variables in function 'BufferInitialize'
;------------------------------------------------------------
;z                         Allocated with name '_BufferInitialize_z_1_1'
;------------------------------------------------------------
;	main.c:120: void BufferInitialize()
;	-----------------------------------------
;	 function BufferInitialize
;	-----------------------------------------
_BufferInitialize:
;	main.c:125: putstr("Input buffer size between 32 and 2800 bytes, divisible by 16\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_2
	mov	b,#0x80
	lcall	_putstr
;	main.c:126: do
00113$:
;	main.c:128: res=ReadValue();
;	genCall
	lcall	_ReadValue
	mov	r2,dpl
	mov	r3,dph
;	genAssign
	mov	dptr,#_res
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:131: if(res>31 && res<2801)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x1F
	subb	a,r2
;	Peephole 159	avoided xrl during execution
	mov	a,#(0x00 ^ 0x80)
	mov	b,r3
	xrl	b,#0x80
	subb	a,b
;	genIfxJump
	jc	00126$
	ljmp	00109$
00126$:
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r2
	subb	a,#0xF1
	mov	a,r3
	xrl	a,#0x80
	subb	a,#0x8a
;	genIfxJump
	jc	00127$
	ljmp	00109$
00127$:
;	main.c:133: if(res%16==0)
;	genAssign
	mov	dptr,#__modsint_PARM_2
	mov	a,#0x10
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar2
	push	ar3
	lcall	__modsint
	mov	a,dpl
	mov	b,dph
	pop	ar3
	pop	ar2
;	genIfx
	orl	a,b
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00106$
;	Peephole 300	removed redundant label 00128$
;	main.c:135: if ((buffer[0] = malloc((unsigned int)z * res)) == 0)  //allocate buffer0
;	genCall
	mov	dpl,r2
	mov	dph,r3
	lcall	_malloc
	mov	r2,dpl
	mov	r3,dph
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_buffer
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00102$
;	Peephole 300	removed redundant label 00129$
;	main.c:136: {putstr("malloc buffer0 failed\n\r");}
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_3
	mov	b,#0x80
	lcall	_putstr
00102$:
;	main.c:137: if ((buffer[1] = malloc((unsigned int)z * res)) == 0)         //allocate buffer1
;	genAssign
	mov	dptr,#_res
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	lcall	_malloc
	mov	r2,dpl
	mov	r3,dph
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_buffer + 0x0002)
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00114$
;	Peephole 300	removed redundant label 00130$
;	main.c:139: putstr("malloc buffer1 failed\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_4
	mov	b,#0x80
	lcall	_putstr
;	main.c:140: free (buffer[0]);  // if buffer1 malloc fails, free buffer 0
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_buffer
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCast
	mov	r4,#0x0
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_free
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00114$
00106$:
;	main.c:144: putstr("Enter the correct buffer size value divisible by 16\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_5
	mov	b,#0x80
	lcall	_putstr
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00114$
00109$:
;	main.c:147: putstr("Enter the correct buffer size between 32 and 2800\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_6
	mov	b,#0x80
	lcall	_putstr
00114$:
;	main.c:148: } while((buffer[0] == 0)||(buffer[1] ==0));
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_buffer
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r3,a
;	Peephole 135	removed redundant mov
	orl	a,r2
;	genIfxJump
	jnz	00131$
	ljmp	00113$
00131$:
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_buffer + 0x0002)
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r3,a
;	Peephole 135	removed redundant mov
	orl	a,r2
;	genIfxJump
	jnz	00132$
	ljmp	00113$
00132$:
;	main.c:149: putstr("Buffer allocation successful\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_7
	mov	b,#0x80
	lcall	_putstr
;	main.c:150: buffer_number=buffer_number+2;
;	genAssign
	mov	dptr,#_buffer_number
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genPlus
	mov	dptr,#_buffer_number
;     genPlusIncr
	mov	a,#0x02
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	main.c:151: bufflen[0]=res;
;	genAssign
	mov	dptr,#_res
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#_bufflen
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:152: bufflen[1]=res;
;	genPointerSet
;     genFarPointerSet
	mov	dptr,#(_bufflen + 0x0002)
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:153: printf_tiny("len1 = %d\n\r", bufflen[0]);
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_bufflen
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_8
	push	acc
	mov	a,#(__str_8 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:154: printf_tiny("len2 = %d\n\r", bufflen[1]);
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_bufflen + 0x0002)
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_9
	push	acc
	mov	a,#(__str_9 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:155: buff1ptr=buffer[0];
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_buffer
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCast
	mov	dptr,#_buff1ptr
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	main.c:156: buff2ptr=buffer[1];
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_buffer + 0x0002)
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genCast
	mov	dptr,#_buff2ptr
	mov	a,r4
	movx	@dptr,a
	inc	dptr
	mov	a,r5
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	main.c:157: bptr0=buffer[0];
;	genAssign
	mov	ar6,r2
	mov	ar7,r3
;	genCast
	mov	dptr,#_bptr0
	mov	a,r6
	movx	@dptr,a
	inc	dptr
	mov	a,r7
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	main.c:158: bptr1=buffer[1];
;	genAssign
;	genCast
	mov	dptr,#_bptr1
	mov	a,r4
	movx	@dptr,a
	inc	dptr
	mov	a,r5
	movx	@dptr,a
	inc	dptr
	mov	a,#0x0
	movx	@dptr,a
;	main.c:159: printf_tiny("Buffer0 address 0x%x\n\r", (unsigned int) buffer[0]);
;	genAssign
;	genCast
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_10
	push	acc
	mov	a,#(__str_10 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:160: printf_tiny("Buffer1 address 0x%x\n\r", (unsigned int) buffer[1]);
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_buffer + 0x0002)
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCast
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_11
	push	acc
	mov	a,#(__str_11 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	Peephole 300	removed redundant label 00116$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'ReadValue'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:163: int ReadValue()
;	-----------------------------------------
;	 function ReadValue
;	-----------------------------------------
_ReadValue:
;	main.c:165: value=0;
;	genAssign
	mov	dptr,#_value
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:166: while(j<4)
00101$:
;	genAssign
	mov	dptr,#_j
	movx	a,@dptr
	mov	r2,a
;	genCmpLt
;	genCmp
	cjne	r2,#0x04,00108$
00108$:
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00103$
;	Peephole 300	removed redundant label 00109$
;	main.c:168: i=getchar();
;	genCall
	lcall	_getchar
	mov	r2,dpl
;	genAssign
	mov	dptr,#_i
	mov	a,r2
	movx	@dptr,a
;	main.c:169: value=((value*10)+(i-'0'));
;	genAssign
	mov	dptr,#_value
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genAssign
	mov	dptr,#__mulint_PARM_2
	mov	a,#0x0A
	movx	@dptr,a
	clr	a
	inc	dptr
	movx	@dptr,a
;	genCall
	mov	dpl,r3
	mov	dph,r4
	push	ar2
	lcall	__mulint
	mov	r3,dpl
	mov	r4,dph
	pop	ar2
;	genAssign
	mov	ar5,r2
;	genCast
	mov	r6,#0x00
;	genMinus
	mov	a,r5
	add	a,#0xd0
	mov	r5,a
	mov	a,r6
	addc	a,#0xff
	mov	r6,a
;	genPlus
	mov	dptr,#_value
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
;	Peephole 236.a	used r3 instead of ar3
	add	a,r3
	movx	@dptr,a
;	Peephole 236.g	used r6 instead of ar6
	mov	a,r6
;	Peephole 236.b	used r4 instead of ar4
	addc	a,r4
	inc	dptr
	movx	@dptr,a
;	main.c:170: putchar(i);
;	genAssign
;	genCast
	mov	r3,#0x00
;	genCall
	mov	dpl,r2
	mov	dph,r3
	lcall	_putchar
;	main.c:171: j++;
;	genAssign
	mov	dptr,#_j
	movx	a,@dptr
	mov	r2,a
;	genPlus
	mov	dptr,#_j
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00103$:
;	main.c:173: j=0;
;	genAssign
	mov	dptr,#_j
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
;	main.c:174: return value;
;	genAssign
	mov	dptr,#_value
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genRet
;	Peephole 234.b	loading dph directly from a(ccumulator), r3 not set
	mov	dpl,r2
	mov	dph,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'newbuffer'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:177: void newbuffer()
;	-----------------------------------------
;	 function newbuffer
;	-----------------------------------------
_newbuffer:
;	main.c:179: putstr("Enter the buffer size between 20 and 400\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_12
	mov	b,#0x80
	lcall	_putstr
;	main.c:180: do
00108$:
;	main.c:182: res = ReadValue();
;	genCall
	lcall	_ReadValue
	mov	r2,dpl
	mov	r3,dph
;	genAssign
	mov	dptr,#_res
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:183: if(res>20 && res<400)
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x14
	subb	a,r2
;	Peephole 159	avoided xrl during execution
	mov	a,#(0x00 ^ 0x80)
	mov	b,r3
	xrl	b,#0x80
	subb	a,b
;	genIfxJump
	jc	00118$
	ljmp	00105$
00118$:
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r2
	subb	a,#0x90
	mov	a,r3
	xrl	a,#0x80
	subb	a,#0x81
;	genIfxJump
	jc	00119$
	ljmp	00105$
00119$:
;	main.c:185: if((buffer[buffer_count]=malloc(sizeof(int)*res))==0)
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r5,a
;	Peephole 105	removed redundant mov
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	r4,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	r5,a
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	a,r3
	xch	a,r2
	add	a,acc
	xch	a,r2
	rlc	a
	mov	r3,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	push	ar4
	push	ar5
	lcall	_malloc
	mov	r2,dpl
	mov	r3,dph
	pop	ar5
	pop	ar4
;	genPointerSet
;     genFarPointerSet
	mov	dpl,r4
	mov	dph,r5
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00102$
;	Peephole 300	removed redundant label 00120$
;	main.c:186: putstr("\n\rBuffer allocation failed\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_13
	mov	b,#0x80
	lcall	_putstr
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00109$
00102$:
;	main.c:188: {putstr("Buffer Allocated\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_14
	mov	b,#0x80
	lcall	_putstr
;	main.c:189: bufflen[buffer_count]=res;
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r3,a
	mov	ar4,r2
;	Peephole 177.d	removed redundant move
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_bufflen
	mov	r6,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_bufflen >> 8)
	mov	r7,a
;	genAssign
	mov	dptr,#_res
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
;	genPointerSet
;     genFarPointerSet
	mov	dpl,r6
	mov	dph,r7
	mov	a,r0
	movx	@dptr,a
	inc	dptr
	mov	a,r1
	movx	@dptr,a
;	main.c:190: printf_tiny("Buffer address and buffer number 0x%x, %d\n\r", (unsigned int) buffer[buffer_count], buffer_count);
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genCast
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	push	ar4
	push	ar5
;	genIpush
	mov	a,#__str_15
	push	acc
	mov	a,#(__str_15 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfa
	mov	sp,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00109$
00105$:
;	main.c:194: putstr("Enter valid buffer size\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_16
	mov	b,#0x80
	lcall	_putstr
00109$:
;	main.c:195: } while(buffer[buffer_count]==0);
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r3,a
	mov	ar4,r2
;	Peephole 177.d	removed redundant move
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r5,a
;	Peephole 135	removed redundant mov
	orl	a,r4
;	genIfxJump
	jnz	00121$
	ljmp	00108$
00121$:
;	main.c:196: buffer_count++;
;	genPlus
	mov	dptr,#_buffer_count
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	main.c:197: buffer_number++;
;	genAssign
	mov	dptr,#_buffer_number
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genPlus
	mov	dptr,#_buffer_number
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
;	Peephole 300	removed redundant label 00111$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'deletebuffer'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:200: void deletebuffer()
;	-----------------------------------------
;	 function deletebuffer
;	-----------------------------------------
_deletebuffer:
;	main.c:202: putstr("Enter the buffer number which has to be deleted\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_17
	mov	b,#0x80
	lcall	_putstr
;	main.c:203: res=ReadValue();
;	genCall
	lcall	_ReadValue
	mov	r2,dpl
	mov	r3,dph
;	genAssign
	mov	dptr,#_res
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:204: if(res>buffer_count || res==1 || res==0)
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genAssign
	mov	ar6,r2
	mov	ar7,r3
;	genCmpGt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,r6
	mov	a,r5
	subb	a,r7
;	genIfxJump
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 160.a	removed sjmp by inverse jump logic
	jc	00104$
;	Peephole 300	removed redundant label 00114$
;	genCmpEq
;	gencjneshort
	cjne	r2,#0x01,00115$
	cjne	r3,#0x00,00115$
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00104$
00115$:
;	genIfx
	mov	a,r2
	orl	a,r3
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00105$
;	Peephole 300	removed redundant label 00116$
00104$:
;	main.c:205: putstr("Enter valid buffer number next time\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_18
	mov	b,#0x80
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_putstr
00105$:
;	main.c:206: else if (!buffer[res])
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	a,r3
	xch	a,r2
	add	a,acc
	xch	a,r2
	rlc	a
	mov	r3,a
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r3,a
;	Peephole 135	removed redundant mov
	orl	a,r2
;	genIfxJump
;	Peephole 108.b	removed ljmp by inverse jump logic
	jnz	00102$
;	Peephole 300	removed redundant label 00117$
;	main.c:207: putstr("Enter valid buffer number next time\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_18
	mov	b,#0x80
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 251.b	replaced sjmp to ret with ret
;	Peephole 253.a	replaced lcall/ret with ljmp
	ljmp	_putstr
00102$:
;	main.c:210: free (buffer[res]);
;	genAssign
;	genCast
	mov	r4,#0x0
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_free
;	main.c:211: buffer[res]=0;
;	genAssign
	mov	dptr,#_res
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	r3,a
;	Peephole 105	removed redundant mov
	xch	a,r2
	add	a,acc
	xch	a,r2
	rlc	a
	mov	r3,a
;	genPlus
;	Peephole 236.g	used r2 instead of ar2
	mov	a,r2
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r3 instead of ar3
	mov	a,r3
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerSet
;     genFarPointerSet
;	Peephole 181	changed mov to clr
	clr	a
	movx	@dptr,a
	inc	dptr
;	Peephole 101	removed redundant mov
	movx	@dptr,a
;	main.c:212: buffer_number--;
;	genAssign
	mov	dptr,#_buffer_number
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genMinus
;	genMinusDec
	dec	r2
	cjne	r2,#0xff,00118$
	dec	r3
00118$:
;	genAssign
	mov	dptr,#_buffer_number
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:213: putstr("Buffer deleted\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_19
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'fillbuffer0'
;------------------------------------------------------------
;buff_val                  Allocated with name '_fillbuffer0_buff_val_1_1'
;------------------------------------------------------------
;	main.c:218: void fillbuffer0(unsigned char buff_val)
;	-----------------------------------------
;	 function fillbuffer0
;	-----------------------------------------
_fillbuffer0:
;	genReceive
	mov	a,dpl
	mov	dptr,#_fillbuffer0_buff_val_1_1
	movx	@dptr,a
;	main.c:222: if((unsigned int)buffer[0]<((unsigned int)buff1ptr+(int)bufflen[0]))
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_buffer
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCast
;	genAssign
	mov	dptr,#_buff1ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genCast
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#_bufflen
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	genPlus
;	Peephole 236.g	used r6 instead of ar6
	mov	a,r6
;	Peephole 236.a	used r4 instead of ar4
	add	a,r4
	mov	r4,a
;	Peephole 236.g	used r7 instead of ar7
	mov	a,r7
;	Peephole 236.b	used r5 instead of ar5
	addc	a,r5
	mov	r5,a
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r2
	subb	a,r4
	mov	a,r3
	subb	a,r5
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00103$
;	Peephole 300	removed redundant label 00106$
;	main.c:224: *bptr0 = buff_val;
;	genAssign
	mov	dptr,#_bptr0
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genAssign
	mov	dptr,#_fillbuffer0_buff_val_1_1
	movx	a,@dptr
;	genPointerSet
;	genGenPointerSet
	mov	r5,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 191	removed redundant mov
	lcall	__gptrput
;	main.c:225: bptr0++;
;	genPlus
	mov	dptr,#_bptr0
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
00103$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'fillbuffer1'
;------------------------------------------------------------
;buff_val                  Allocated with name '_fillbuffer1_buff_val_1_1'
;------------------------------------------------------------
;	main.c:229: void fillbuffer1(unsigned char buff_val)
;	-----------------------------------------
;	 function fillbuffer1
;	-----------------------------------------
_fillbuffer1:
;	genReceive
	mov	a,dpl
	mov	dptr,#_fillbuffer1_buff_val_1_1
	movx	@dptr,a
;	main.c:231: if((unsigned int)buffer[1]<((unsigned int)buff2ptr+(int)bufflen[1]))
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_buffer + 0x0002)
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCast
;	genAssign
	mov	dptr,#_buff2ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genCast
;	genPointerGet
;	genFarPointerGet
	mov	dptr,#(_bufflen + 0x0002)
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	genPlus
;	Peephole 236.g	used r6 instead of ar6
	mov	a,r6
;	Peephole 236.a	used r4 instead of ar4
	add	a,r4
	mov	r4,a
;	Peephole 236.g	used r7 instead of ar7
	mov	a,r7
;	Peephole 236.b	used r5 instead of ar5
	addc	a,r5
	mov	r5,a
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r2
	subb	a,r4
	mov	a,r3
	subb	a,r5
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00103$
;	Peephole 300	removed redundant label 00106$
;	main.c:233: *bptr1 = buff_val;
;	genAssign
	mov	dptr,#_bptr1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genAssign
	mov	dptr,#_fillbuffer1_buff_val_1_1
	movx	a,@dptr
;	genPointerSet
;	genGenPointerSet
	mov	r5,a
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 191	removed redundant mov
	lcall	__gptrput
;	main.c:234: bptr1++;
;	genPlus
	mov	dptr,#_bptr1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
00103$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'freeEverything'
;------------------------------------------------------------
;counter                   Allocated with name '_freeEverything_counter_1_1'
;------------------------------------------------------------
;	main.c:239: void freeEverything()
;	-----------------------------------------
;	 function freeEverything
;	-----------------------------------------
_freeEverything:
;	main.c:242: for (counter=0; counter<buffer_count; counter++)
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
00103$:
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r2
	subb	a,r4
	mov	a,r3
	subb	a,r5
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00107$
;	Peephole 300	removed redundant label 00113$
;	main.c:244: if(buffer[counter])
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	ar4,r2
	mov	a,r3
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r5,a
;	Peephole 135	removed redundant mov
	orl	a,r4
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00105$
;	Peephole 300	removed redundant label 00114$
;	main.c:246: free (buffer[counter]);
;	genAssign
;	genCast
	mov	r6,#0x0
;	genCall
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	push	ar2
	push	ar3
	lcall	_free
	pop	ar3
	pop	ar2
;	main.c:247: printf_tiny("Freeing %d buffer\n\r",counter);
;	genIpush
	push	ar2
	push	ar3
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_20
	push	acc
	mov	a,#(__str_20 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
00105$:
;	main.c:242: for (counter=0; counter<buffer_count; counter++)
;	genPlus
;     genPlusIncr
	inc	r2
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 243	avoided branch to sjmp
	cjne	r2,#0x00,00103$
	inc	r3
;	Peephole 300	removed redundant label 00115$
	sjmp	00103$
00107$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'displaydelete'
;------------------------------------------------------------
;len                       Allocated with name '_displaydelete_len_1_1'
;charnum0                  Allocated with name '_displaydelete_charnum0_1_1'
;charnum1                  Allocated with name '_displaydelete_charnum1_1_1'
;------------------------------------------------------------
;	main.c:251: void displaydelete()
;	-----------------------------------------
;	 function displaydelete
;	-----------------------------------------
_displaydelete:
;	main.c:253: unsigned int len=0, charnum0=0, charnum1=0 ;
;	genAssign
	mov	dptr,#_displaydelete_charnum0_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	genAssign
	mov	dptr,#_displaydelete_charnum1_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:254: printf_tiny("---------------------------------------------------------------------------------------\n\r");
;	genIpush
	mov	a,#__str_21
	push	acc
	mov	a,#(__str_21 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:255: printf_tiny("\n\rrTotal Number of Buffer = %d\n\r", buffer_number);
;	genIpush
	mov	dptr,#_buffer_number
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
;	genIpush
	mov	a,#__str_22
	push	acc
	mov	a,#(__str_22 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:256: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	lcall	_putstr
;	main.c:257: printf_tiny("---------------------------------------------------------------------------------------\n\r");
;	genIpush
	mov	a,#__str_21
	push	acc
	mov	a,#(__str_21 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:258: printf_tiny("\n\rTotal number of characters received = %d\n\r", char_count);
;	genIpush
	mov	dptr,#_char_count
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
;	genIpush
	mov	a,#__str_23
	push	acc
	mov	a,#(__str_23 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:259: printf_tiny("Total number of storage characters received = %d\n\r", stor_count);
;	genIpush
	mov	dptr,#_stor_count
	movx	a,@dptr
	push	acc
	inc	dptr
	movx	a,@dptr
	push	acc
;	genIpush
	mov	a,#__str_24
	push	acc
	mov	a,#(__str_24 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:260: printf_tiny("Total number of characters received since last '?' = %d\n\r", (char_count-lastchar_count));
;	genAssign
	mov	dptr,#_lastchar_count
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genMinus
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	mov	r2,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	mov	r3,a
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_25
	push	acc
	mov	a,#(__str_25 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:261: printf_tiny("\n\r---------------------------------------------------------------------------------------\n\r");
;	genIpush
	mov	a,#__str_26
	push	acc
	mov	a,#(__str_26 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:262: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	lcall	_putstr
;	main.c:263: lastchar_count=char_count;
;	genAssign
	mov	dptr,#_char_count
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_lastchar_count
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:264: while((unsigned char)*buff1ptr!='\0')
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
00101$:
;	genAssign
	mov	dptr,#_buff1ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
	mov	r7,a
;	Peephole 115.b	jump optimization
	jz	00122$
;	Peephole 300	removed redundant label 00125$
;	main.c:266: charnum0++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00126$
	inc	r3
00126$:
;	genAssign
	mov	dptr,#_displaydelete_charnum0_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:267: (unsigned char)buff1ptr++;
;	genPlus
	mov	dptr,#_buff1ptr
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r4 instead of ar4
	add	a,r4
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r5 instead of ar5
	addc	a,r5
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00122$:
;	genAssign
	mov	dptr,#_displaydelete_charnum0_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:269: printf_tiny("\n\rNumber of characters in buffer0 = %d\n\r", charnum0);
;	genIpush
	push	ar2
	push	ar3
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_27
	push	acc
	mov	a,#(__str_27 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
;	main.c:270: buff1ptr=buff1ptr-charnum0;
;	genAssign
	mov	dptr,#_buff1ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genMinus
	mov	dptr,#_buff1ptr
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	movx	@dptr,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	main.c:271: while((unsigned char)*buff2ptr!='\0')
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
00104$:
;	genAssign
	mov	dptr,#_buff2ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
	mov	r7,a
;	Peephole 115.b	jump optimization
	jz	00123$
;	Peephole 300	removed redundant label 00127$
;	main.c:273: charnum1++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00128$
	inc	r3
00128$:
;	genAssign
	mov	dptr,#_displaydelete_charnum1_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:274: (unsigned char)buff2ptr++;
;	genPlus
	mov	dptr,#_buff2ptr
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r4 instead of ar4
	add	a,r4
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r5 instead of ar5
	addc	a,r5
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00104$
00123$:
;	genAssign
	mov	dptr,#_displaydelete_charnum1_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
;	main.c:276: printf_tiny("Number of characters in buffer1 = %d\n\r", charnum1);
;	genIpush
	push	ar2
	push	ar3
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_28
	push	acc
	mov	a,#(__str_28 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
;	main.c:277: buff2ptr=buff2ptr-charnum1;
;	genAssign
	mov	dptr,#_buff2ptr
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genMinus
	mov	dptr,#_buff2ptr
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	movx	@dptr,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	main.c:278: printf_tiny("\n\r---------------------------------------------------------------------------------------\n\r");
;	genIpush
	mov	a,#__str_26
	push	acc
	mov	a,#(__str_26 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:279: for(len=0; len<buffer_count; len++)
;	genAssign
	mov	r2,#0x00
	mov	r3,#0x00
00109$:
;	genAssign
	mov	dptr,#_buffer_count
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r2
	subb	a,r4
	mov	a,r3
	subb	a,r5
;	genIfxJump
	jc	00129$
	ljmp	00112$
00129$:
;	main.c:281: if(buffer[len])
;	genLeftShift
;	genLeftShiftLiteral
;	genlshTwo
	mov	ar4,r2
	mov	a,r3
	xch	a,r4
	add	a,acc
	xch	a,r4
	rlc	a
	mov	r5,a
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
;	genIfx
	mov	r7,a
;	Peephole 135	removed redundant mov
	orl	a,r6
;	genIfxJump
	jnz	00130$
	ljmp	00111$
00130$:
;	main.c:283: printf_tiny("---------------------------------------------------------------------------------------\n\r");
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	mov	a,#__str_21
	push	acc
	mov	a,#(__str_21 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:284: printf_tiny("Buffer %d Information\n\r", len);
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_29
	push	acc
	mov	a,#(__str_29 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:285: printf_tiny("Starting address = 0x%x\n\r",(unsigned int)buffer[len]);
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	genCast
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_30
	push	acc
	mov	a,#(__str_30 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:286: printf_tiny("Ending address = 0x%x\n\r",((unsigned int)buffer[len]+(unsigned int)bufflen[len]));
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_buffer
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_buffer >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r6,a
	inc	dptr
	movx	a,@dptr
	mov	r7,a
;	genCast
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_bufflen
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_bufflen >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r0,a
	inc	dptr
	movx	a,@dptr
	mov	r1,a
;	genPlus
;	Peephole 236.g	used r0 instead of ar0
	mov	a,r0
;	Peephole 236.a	used r6 instead of ar6
	add	a,r6
	mov	r6,a
;	Peephole 236.g	used r1 instead of ar1
	mov	a,r1
;	Peephole 236.b	used r7 instead of ar7
	addc	a,r7
	mov	r7,a
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
	push	ar6
	push	ar7
;	genIpush
	mov	a,#__str_31
	push	acc
	mov	a,#(__str_31 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar5
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:287: printf_tiny("Allocated size = %d\n\r", bufflen[len]);
;	genPlus
;	Peephole 236.g	used r4 instead of ar4
	mov	a,r4
	add	a,#_bufflen
	mov	dpl,a
;	Peephole 236.g	used r5 instead of ar5
	mov	a,r5
	addc	a,#(_bufflen >> 8)
	mov	dph,a
;	genPointerGet
;	genFarPointerGet
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	push	ar5
;	genIpush
	mov	a,#__str_32
	push	acc
	mov	a,#(__str_32 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar3
	pop	ar2
;	main.c:288: printf_tiny("---------------------------------------------------------------------------------------\n\r");
;	genIpush
	push	ar2
	push	ar3
	mov	a,#__str_21
	push	acc
	mov	a,#(__str_21 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
	pop	ar3
	pop	ar2
;	main.c:289: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	push	ar2
	push	ar3
	lcall	_putstr
	pop	ar3
	pop	ar2
00111$:
;	main.c:279: for(len=0; len<buffer_count; len++)
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00131$
	inc	r3
00131$:
	ljmp	00109$
00112$:
;	main.c:292: printf_tiny("---------------------------------------------------------------------------------------\n\r");
;	genIpush
	mov	a,#__str_21
	push	acc
	mov	a,#(__str_21 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:293: putstr("\n\rContents of Buffer0\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_33
	mov	b,#0x80
	lcall	_putstr
;	main.c:294: putstrbuff(buff1ptr);
;	genAssign
	mov	dptr,#_buff1ptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_putstrbuff
;	main.c:296: bptr0=bptr0-charnum0;
;	genAssign
	mov	dptr,#_displaydelete_charnum0_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_bptr0
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genMinus
	mov	dptr,#_bptr0
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	movx	@dptr,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	main.c:297: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	lcall	_putstr
;	main.c:298: printf_tiny("---------------------------------------------------------------------------------------\n\r");
;	genIpush
	mov	a,#__str_21
	push	acc
	mov	a,#(__str_21 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:299: putstr("\n\rContents of Buffer1\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_34
	mov	b,#0x80
	lcall	_putstr
;	main.c:300: putstrbuff(buff2ptr);
;	genAssign
	mov	dptr,#_buff2ptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_putstrbuff
;	main.c:302: bptr1=bptr1-charnum1;
;	genAssign
	mov	dptr,#_displaydelete_charnum1_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	dptr,#_bptr1
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genMinus
	mov	dptr,#_bptr1
	mov	a,r4
	clr	c
;	Peephole 236.l	used r2 instead of ar2
	subb	a,r2
	movx	@dptr,a
	mov	a,r5
;	Peephole 236.l	used r3 instead of ar3
	subb	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r6
	movx	@dptr,a
;	main.c:303: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	lcall	_putstr
;	main.c:304: printf_tiny("---------------------------------------------------------------------------------------\n\r");
;	genIpush
	mov	a,#__str_21
	push	acc
	mov	a,#(__str_21 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	Peephole 300	removed redundant label 00113$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'display'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:308: void display()
;	-----------------------------------------
;	 function display
;	-----------------------------------------
_display:
;	main.c:310: putstr("\n\rContents of Buffer0\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_33
	mov	b,#0x80
	lcall	_putstr
;	main.c:311: putstrhex(buff1ptr);
;	genAssign
	mov	dptr,#_buff1ptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_putstrhex
;	main.c:313: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
	lcall	_putstr
;	main.c:314: putstr("Contents of Buffer1\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_35
	mov	b,#0x80
	lcall	_putstr
;	main.c:315: putstrhex(buff2ptr);
;	genAssign
	mov	dptr,#_buff2ptr
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genCall
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	_putstrhex
;	main.c:317: putstr("\n\r");
;	genCall
;	Peephole 182.a	used 16 bit load of DPTR
	mov	dptr,#__str_1
	mov	b,#0x80
;	Peephole 253.b	replaced lcall/ret with ljmp
	ljmp	_putstr
;
;------------------------------------------------------------
;Allocation info for local variables in function 'putstrhex'
;------------------------------------------------------------
;ptr                       Allocated with name '_putstrhex_ptr_1_1'
;count                     Allocated with name '_putstrhex_count_1_1'
;------------------------------------------------------------
;	main.c:320: void putstrhex(char *ptr)
;	-----------------------------------------
;	 function putstrhex
;	-----------------------------------------
_putstrhex:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_putstrhex_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:322: unsigned int count=0;
;	genAssign
	mov	dptr,#_putstrhex_count_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:323: printf_tiny("0x%x:  ",&(ptr));
;	genIpush
	mov	a,#_putstrhex_ptr_1_1
	push	acc
	mov	a,#(_putstrhex_ptr_1_1 >> 8)
	push	acc
;	Peephole 181	changed mov to clr
	clr	a
	push	acc
;	genIpush
	mov	a,#__str_36
	push	acc
	mov	a,#(__str_36 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
;	main.c:324: while(*ptr)
00105$:
;	genAssign
	mov	dptr,#_putstrhex_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfxJump
	jnz	00114$
;	Peephole 251.a	replaced ljmp to ret with ret
	ret
00114$:
;	main.c:326: if (count==15)
;	genAssign
	mov	dptr,#_putstrhex_count_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpEq
;	gencjneshort
;	Peephole 112.b	changed ljmp to sjmp
;	Peephole 198.a	optimized misc jump sequence
	cjne	r2,#0x0F,00102$
	cjne	r3,#0x00,00102$
;	Peephole 200.b	removed redundant sjmp
;	Peephole 300	removed redundant label 00115$
;	Peephole 300	removed redundant label 00116$
;	main.c:328: printf_tiny("0x%x:  ",&(ptr));
;	genIpush
	mov	a,#_putstrhex_ptr_1_1
	push	acc
	mov	a,#(_putstrhex_ptr_1_1 >> 8)
	push	acc
;	Peephole 181	changed mov to clr
	clr	a
	push	acc
;	genIpush
	mov	a,#__str_36
	push	acc
	mov	a,#(__str_36 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
00102$:
;	main.c:330: printf_tiny("%x ",*ptr);
;	genAssign
	mov	dptr,#_putstrhex_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genCast
	mov	r2,a
;	Peephole 105	removed redundant mov
	rlc	a
	subb	a,acc
	mov	r3,a
;	genIpush
	push	ar2
	push	ar3
;	genIpush
	mov	a,#__str_37
	push	acc
	mov	a,#(__str_37 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	main.c:331: ptr++;
;	genAssign
	mov	dptr,#_putstrhex_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
;	genPlus
	mov	dptr,#_putstrhex_ptr_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	main.c:332: if (count>15)
;	genAssign
	mov	dptr,#_putstrhex_count_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x0F
	subb	a,r2
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r3
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00104$
;	Peephole 300	removed redundant label 00117$
;	main.c:334: printf_tiny("\n\r");
;	genIpush
	mov	a,#__str_1
	push	acc
	mov	a,#(__str_1 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
;	main.c:335: count=0;
;	genAssign
	mov	dptr,#_putstrhex_count_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
00104$:
;	main.c:337: count++;
;	genAssign
	mov	dptr,#_putstrhex_count_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genPlus
	mov	dptr,#_putstrhex_count_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r2 instead of ar2
	add	a,r2
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r3 instead of ar3
	addc	a,r3
	inc	dptr
	movx	@dptr,a
	ljmp	00105$
;	Peephole 259.b	removed redundant label 00108$ and ret
;
;------------------------------------------------------------
;Allocation info for local variables in function 'putstrbuff'
;------------------------------------------------------------
;ptr                       Allocated with name '_putstrbuff_ptr_1_1'
;count                     Allocated with name '_putstrbuff_count_1_1'
;------------------------------------------------------------
;	main.c:343: void putstrbuff(char *ptr)
;	-----------------------------------------
;	 function putstrbuff
;	-----------------------------------------
_putstrbuff:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_putstrbuff_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:345: unsigned int count=0;
;	genAssign
	mov	dptr,#_putstrbuff_count_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:346: while(*ptr)
;	genAssign
	mov	dptr,#_putstrbuff_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00103$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfxJump
	jnz	00112$
	ljmp	00111$
00112$:
;	main.c:348: putchar(*ptr);
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genCast
	mov	r5,a
;	Peephole 105	removed redundant mov
	rlc	a
	subb	a,acc
	mov	r6,a
;	genCall
	mov	dpl,r5
	mov	dph,r6
	push	ar2
	push	ar3
	push	ar4
	lcall	_putchar
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:349: count++;
;	genAssign
	mov	dptr,#_putstrbuff_count_1_1
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genPlus
	mov	dptr,#_putstrbuff_count_1_1
;     genPlusIncr
	mov	a,#0x01
;	Peephole 236.a	used r5 instead of ar5
	add	a,r5
	movx	@dptr,a
;	Peephole 181	changed mov to clr
	clr	a
;	Peephole 236.b	used r6 instead of ar6
	addc	a,r6
	inc	dptr
	movx	@dptr,a
;	main.c:350: *ptr='\0';
;	genPointerSet
;	genGenPointerSet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
;	Peephole 181	changed mov to clr
	clr	a
	lcall	__gptrput
;	main.c:351: if (count>63)
;	genAssign
	mov	dptr,#_putstrbuff_count_1_1
	movx	a,@dptr
	mov	r5,a
	inc	dptr
	movx	a,@dptr
	mov	r6,a
;	genCmpGt
;	genCmp
	clr	c
	mov	a,#0x3F
	subb	a,r5
;	Peephole 181	changed mov to clr
	clr	a
	subb	a,r6
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00102$
;	Peephole 300	removed redundant label 00113$
;	main.c:352: {count=0;
;	genAssign
	mov	dptr,#_putstrbuff_count_1_1
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
;	main.c:353: printf_tiny("\n\r");}
;	genIpush
	push	ar2
	push	ar3
	push	ar4
	mov	a,#__str_1
	push	acc
	mov	a,#(__str_1 >> 8)
	push	acc
;	genCall
	lcall	_printf_tiny
	dec	sp
	dec	sp
	pop	ar4
	pop	ar3
	pop	ar2
00102$:
;	main.c:354: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00114$
	inc	r3
00114$:
;	genAssign
	mov	dptr,#_putstrbuff_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
	ljmp	00103$
00111$:
;	genAssign
	mov	dptr,#_putstrbuff_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00106$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putstr'
;------------------------------------------------------------
;ptr                       Allocated with name '_putstr_ptr_1_1'
;------------------------------------------------------------
;	main.c:359: void putstr(char *ptr)
;	-----------------------------------------
;	 function putstr
;	-----------------------------------------
_putstr:
;	genReceive
	mov	r2,b
	mov	r3,dph
	mov	a,dpl
	mov	dptr,#_putstr_ptr_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:361: while(*ptr)
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	r4,a
00101$:
;	genPointerGet
;	genGenPointerGet
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
;	genIfx
	mov	r5,a
;	Peephole 105	removed redundant mov
;	genIfxJump
;	Peephole 108.c	removed ljmp by inverse jump logic
	jz	00108$
;	Peephole 300	removed redundant label 00109$
;	main.c:363: putchar(*ptr);
;	genCast
	mov	a,r5
	rlc	a
	subb	a,acc
	mov	r6,a
;	genCall
	mov	dpl,r5
	mov	dph,r6
	push	ar2
	push	ar3
	push	ar4
	lcall	_putchar
	pop	ar4
	pop	ar3
	pop	ar2
;	main.c:364: ptr++;
;	genPlus
;     genPlusIncr
	inc	r2
	cjne	r2,#0x00,00110$
	inc	r3
00110$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00108$:
;	genAssign
	mov	dptr,#_putstr_ptr_1_1
	mov	a,r2
	movx	@dptr,a
	inc	dptr
	mov	a,r3
	movx	@dptr,a
	inc	dptr
	mov	a,r4
	movx	@dptr,a
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'SerialInitialize'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:369: void SerialInitialize()
;	-----------------------------------------
;	 function SerialInitialize
;	-----------------------------------------
_SerialInitialize:
;	main.c:371: TMOD=0x20; //timer 1 in mode2 (8-bit auto-reload) to set baud rate
;	genAssign
	mov	_TMOD,#0x20
;	main.c:372: TH1=0xfd;  // to make baud rate of 9600hz , crystal oscillator =11.0592Mhz
;	genAssign
	mov	_TH1,#0xFD
;	main.c:373: SCON=0x50;
;	genAssign
	mov	_SCON,#0x50
;	main.c:374: TR1=1;  // to start timer
;	genAssign
	setb	_TR1
;	main.c:375: IE=0x90; // to make serial interrupt interrupt enable
;	genAssign
	mov	_IE,#0x90
;	Peephole 300	removed redundant label 00101$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'getchar'
;------------------------------------------------------------
;------------------------------------------------------------
;	main.c:378: char getchar()
;	-----------------------------------------
;	 function getchar
;	-----------------------------------------
_getchar:
;	main.c:381: while(RI==0);  // wait untill hole 8 bit data is received completely. RI bit become 1 when reception is compleated
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
	jnb	_RI,00101$
;	Peephole 300	removed redundant label 00108$
;	main.c:382: rec=SBUF;   // data is received in SBUF register present in controller during receiving
;	genAssign
	mov	r2,_SBUF
;	genAssign
	mov	dptr,#_rec
	mov	a,r2
	movx	@dptr,a
;	main.c:383: RI=0;  // make RI bit to 0 so that next data is received
;	genAssign
	clr	_RI
;	main.c:384: return rec;  // return rec where funtion is called
;	genRet
	mov	dpl,r2
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
;x                         Allocated with name '_putchar_x_1_1'
;------------------------------------------------------------
;	main.c:387: void putchar(int x)
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_putchar_x_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:389: SBUF=x;    // 8 bit data is put in SBUF register present in controller to transmit
;	genAssign
	mov	dptr,#_putchar_x_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genCast
	mov	_SBUF,r2
;	main.c:390: while(TI==0); // wait untill transmission is compleated. when transmittion is compleated TI bit become 1
00101$:
;	genIfx
;	genIfxJump
;	Peephole 108.d	removed ljmp by inverse jump logic
;	main.c:391: TI=0; // make TI bit to zero for next transmittion
;	genAssign
;	Peephole 250.a	using atomic test and clear
	jbc	_TI,00108$
	sjmp	00101$
00108$:
;	Peephole 300	removed redundant label 00104$
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'delay'
;------------------------------------------------------------
;x                         Allocated with name '_delay_x_1_1'
;i                         Allocated with name '_delay_i_1_1'
;------------------------------------------------------------
;	main.c:394: void delay(int x)
;	-----------------------------------------
;	 function delay
;	-----------------------------------------
_delay:
;	genReceive
	mov	r2,dph
	mov	a,dpl
	mov	dptr,#_delay_x_1_1
	movx	@dptr,a
	inc	dptr
	mov	a,r2
	movx	@dptr,a
;	main.c:397: for(i=0;i<x;i++);
;	genAssign
	mov	dptr,#_delay_x_1_1
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	mov	r3,a
;	genAssign
	mov	r4,#0x00
	mov	r5,#0x00
00101$:
;	genCmpLt
;	genCmp
	clr	c
	mov	a,r4
	subb	a,r2
	mov	a,r5
	xrl	a,#0x80
	mov	b,r3
	xrl	b,#0x80
	subb	a,b
;	genIfxJump
;	Peephole 108.a	removed ljmp by inverse jump logic
	jnc	00105$
;	Peephole 300	removed redundant label 00110$
;	genPlus
;     genPlusIncr
;	tail increment optimized (range 3)
	inc	r4
	cjne	r4,#0x00,00101$
	inc	r5
;	Peephole 112.b	changed ljmp to sjmp
	sjmp	00101$
00105$:
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
__str_0:
	.db 0x0A
	.db 0x0D
	.ascii "Enter the character"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_1:
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_2:
	.ascii "Input buffer size between 32 and 2800 bytes, divisible by 16"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_3:
	.ascii "malloc buffer0 failed"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_4:
	.ascii "malloc buffer1 failed"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_5:
	.ascii "Enter the correct buffer size value divisible by 16"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_6:
	.ascii "Enter the correct buffer size between 32 and 2800"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_7:
	.ascii "Buffer allocation successful"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_8:
	.ascii "len1 = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_9:
	.ascii "len2 = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_10:
	.ascii "Buffer0 address 0x%x"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_11:
	.ascii "Buffer1 address 0x%x"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_12:
	.ascii "Enter the buffer size between 20 and 400"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_13:
	.db 0x0A
	.db 0x0D
	.ascii "Buffer allocation failed"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_14:
	.ascii "Buffer Allocated"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_15:
	.ascii "Buffer address and buffer number 0x%x, %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_16:
	.ascii "Enter valid buffer size"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_17:
	.ascii "Enter the buffer number which has to be deleted"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_18:
	.ascii "Enter valid buffer number next time"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_19:
	.ascii "Buffer deleted"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_20:
	.ascii "Freeing %d buffer"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_21:
	.ascii "------------------------------------------------------------"
	.ascii "---------------------------"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_22:
	.db 0x0A
	.db 0x0D
	.ascii "rTotal Number of Buffer = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_23:
	.db 0x0A
	.db 0x0D
	.ascii "Total number of characters received = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_24:
	.ascii "Total number of storage characters received = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_25:
	.ascii "Total number of characters received since last '?' = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_26:
	.db 0x0A
	.db 0x0D
	.ascii "----------------------------------------------------------"
	.ascii "-----------------------------"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_27:
	.db 0x0A
	.db 0x0D
	.ascii "Number of characters in buffer0 = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_28:
	.ascii "Number of characters in buffer1 = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_29:
	.ascii "Buffer %d Information"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_30:
	.ascii "Starting address = 0x%x"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_31:
	.ascii "Ending address = 0x%x"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_32:
	.ascii "Allocated size = %d"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_33:
	.db 0x0A
	.db 0x0D
	.ascii "Contents of Buffer0"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_34:
	.db 0x0A
	.db 0x0D
	.ascii "Contents of Buffer1"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_35:
	.ascii "Contents of Buffer1"
	.db 0x0A
	.db 0x0D
	.db 0x00
__str_36:
	.ascii "0x%x:  "
	.db 0x00
__str_37:
	.ascii "%x "
	.db 0x00
	.area XINIT   (CODE)
__xinit__i:
	.db #0x00
